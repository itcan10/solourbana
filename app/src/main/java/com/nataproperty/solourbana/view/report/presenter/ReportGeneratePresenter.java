package com.nataproperty.solourbana.view.report.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.report.adapter.ReportAdapter;
import com.nataproperty.solourbana.view.report.intractor.ReportGenerateInteractor;
import com.nataproperty.solourbana.view.report.model.ResponGenerateReportModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ReportGeneratePresenter implements ReportGenerateInteractor {
    private ReportAdapter view;
    private ServiceRetrofitProject service;

    public ReportGeneratePresenter(ReportAdapter view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GenerateDashboardReportSvc(String projectCode, String dbMasterRef, String projectRef, String memberRef, String userRef,String aspx) {
        Call<ResponGenerateReportModel> call = service.getAPI().GenerateDashboardReportSvc(projectCode,dbMasterRef,projectRef,memberRef,userRef);
        call.enqueue(new Callback<ResponGenerateReportModel>() {
            @Override
            public void onResponse(Call<ResponGenerateReportModel> call, Response<ResponGenerateReportModel> response) {
                view.showGenerateResults(response);
            }

            @Override
            public void onFailure(Call<ResponGenerateReportModel> call, Throwable t) {
                view.showGenerateFailure(t);

            }


        });
    }



}
