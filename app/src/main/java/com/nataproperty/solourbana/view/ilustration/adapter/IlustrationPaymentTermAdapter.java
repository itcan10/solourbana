package com.nataproperty.solourbana.view.ilustration.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.ilustration.model.PaymentTermModel;

import java.util.ArrayList;

/**
 * Created by UserModel on 5/13/2016.
 */
public class IlustrationPaymentTermAdapter extends BaseAdapter {
    public static final String TAG = "PaymentTermAdapter";

    private Context context;
    private ArrayList<PaymentTermModel> list;
    private ListPaymentTermtHolder holder;

    public IlustrationPaymentTermAdapter(Context context, ArrayList<PaymentTermModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_payment,null);
            holder = new ListPaymentTermtHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.discount = (TextView) convertView.findViewById(R.id.txt_disc);
            holder.downPayment = (TextView) convertView.findViewById(R.id.txt_downPayment);
            holder.price = (TextView) convertView.findViewById(R.id.txt_priceInc);

            convertView.setTag(holder);
        }else{
            holder = (ListPaymentTermtHolder) convertView.getTag();
        }

        PaymentTermModel payment = list.get(position);
        holder.name.setText(payment.getTermName());
        holder.discount.setText(payment.getDiscount());
        holder.downPayment.setText(payment.getDownPayment());
        holder.price.setText(payment.getNetPrice());

        Log.d(TAG,"getDownPayment "+payment.getDownPayment());

        return convertView;
    }

    private class ListPaymentTermtHolder {
        TextView name,discount,downPayment,price;
    }
}
