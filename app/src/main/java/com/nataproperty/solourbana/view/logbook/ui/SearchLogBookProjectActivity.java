package com.nataproperty.solourbana.view.logbook.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.adapter.LogBookProjectAdapter;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.presenter.SearchLogBookProjectPresenter;

import java.util.ArrayList;

import retrofit2.Response;

public class SearchLogBookProjectActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private SearchLogBookProjectPresenter presenter;
    ListView listView;
    LogBookProjectAdapter adapter;
    ArrayList<LogBookModel> list = new ArrayList<>();
    Toolbar toolbar;
    TextView title, noData;
    Typeface font;
    SharedPreferences sharedPreferences;
    FloatingActionButton fab;
    String memberRef, keyword = "", projectRef, dbMasterRef, referralRef = "", eventRef = "",
            projectCustomerStatus, projectCustomerStatusName, name, hp1, email1, address, province,
            city, eventName, referralName, color, customerRef;
    ProgressDialog progressDialog;
    private EditText edtSearch;
    private TextView txtClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_log_book_project);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new SearchLogBookProjectPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent i = getIntent();
        dbMasterRef = i.getStringExtra(DBMASTER_REF);
        projectRef = i.getStringExtra(PROJECT_REF);
        projectCustomerStatus = i.getStringExtra("projectCustomerStatus");
        color = i.getStringExtra("color");

        edtSearch.setHint("Search..");
        edtSearch.setHintTextColor(getResources().getColor(R.color.colorText));
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edtSearch.getText().toString().trim().equals("")) {
                        Toast.makeText(getApplicationContext(), "Pencarian Tidak Boleh Kosong", Toast.LENGTH_LONG).show();
                    } else {
                        if (list.size() != 0) {
                            list.clear();
                        }
                        keyword = edtSearch.getText().toString();
                        requestLogBook();
                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                        return true;
                    }
                }
                return false;
            }
        });

        txtClose.setOnClickListener(this);

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        listView = (ListView) findViewById(R.id.list_log_book);
        listView.setOnItemClickListener(this);
        noData = (TextView) findViewById(R.id.txt_no_data);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        txtClose = (TextView) findViewById(R.id.txt_close);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_close:
                onBackPressed();
                break;

        }
    }

    private void requestLogBook() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetContactBankProjectSvc(dbMasterRef, projectRef, memberRef, projectCustomerStatus, eventRef, referralRef, keyword);
    }

    public void showListLogBookResults(Response<ArrayList<LogBookModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            list = response.body();
            if (list.size() > 0) {
                initAdapter();
                noData.setVisibility(View.GONE);
            } else {
                noData.setVisibility(View.VISIBLE);
                listView.setEmptyView(noData);
            }

        }
    }

    public void showListLogBookFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new LogBookProjectAdapter(this, list, "");
        listView.setAdapter(adapter);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == listView) {
            name = list.get(position).getName();
            hp1 = list.get(position).getHp1();
            email1 = list.get(position).getEmail1();
            province = list.get(position).getProvinceName();
            city = list.get(position).getCityName();
            address = list.get(position).getAddress();
            projectCustomerStatusName = list.get(position).getProjectCustomerStatusName();
            eventName = list.get(position).getEventName();
            referralName = list.get(position).getReferralName();
            customerRef = list.get(position).getCustomerRef();
            projectCustomerStatus = list.get(position).getProjectCustomerStatus();

            Intent i = new Intent(SearchLogBookProjectActivity.this, LogBookDetailActivity.class);
            i.putExtra("dbMasterRef", dbMasterRef);
            i.putExtra("projectRef", projectRef);
            i.putExtra("customerRef", customerRef);
            i.putExtra("name", name);
            i.putExtra("hp1", hp1);
            i.putExtra("email1", email1);
            i.putExtra("province", province);
            i.putExtra("city", city);
            i.putExtra("address", address);
            i.putExtra("projectCustomerStatusName", projectCustomerStatusName);
            i.putExtra("eventName", eventName);
            i.putExtra("referralName", referralName);
            i.putExtra("projectCustomerStatus", projectCustomerStatus);
            i.putExtra("color", color);
            startActivity(i);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
