package com.nataproperty.solourbana.view.event.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.event.interactor.PresenterMyTicketInteractor;
import com.nataproperty.solourbana.view.event.model.MyTicketModel;
import com.nataproperty.solourbana.view.event.ui.MyTicketActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyTicketPresenter implements PresenterMyTicketInteractor {
    private MyTicketActivity view;
    private ServiceRetrofitProject service;

    public MyTicketPresenter(MyTicketActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListTiketRsvp(String memberRef, String projectCode) {
        Call<ArrayList<MyTicketModel>> call = service.getAPI().getListTiketRsvp(memberRef,projectCode);
        call.enqueue(new Callback<ArrayList<MyTicketModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MyTicketModel>> call, Response<ArrayList<MyTicketModel>> response) {
                view.showListMyTicketResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<MyTicketModel>> call, Throwable t) {
                view.showListTicketFailure(t);

            }


        });
    }

}
