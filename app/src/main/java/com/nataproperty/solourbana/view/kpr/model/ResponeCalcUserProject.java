package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by Nata on 3/13/2017.
 */

public class ResponeCalcUserProject {
    int status;
    String calcUserProjectRef;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCalcUserProjectRef() {
        return calcUserProjectRef;
    }

    public void setCalcUserProjectRef(String calcUserProjectRef) {
        this.calcUserProjectRef = calcUserProjectRef;
    }
}
