package com.nataproperty.solourbana.view.mynup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mynup.model.NeedAttentionModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class NeedAttentionAdapter extends BaseAdapter {
    private Context context;
    private List<NeedAttentionModel> list;
    private ListUnPaidHolder holder;

    public NeedAttentionAdapter(Context context, List<NeedAttentionModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_paid,null);
            holder = new ListUnPaidHolder();
            holder.orderCode = (TextView) convertView.findViewById(R.id.txt_nup_code);
            holder.qty = (TextView) convertView.findViewById(R.id.txt_qty);
            holder.totalAmt = (TextView) convertView.findViewById(R.id.txt_total_amt);
            holder.projectName = (TextView) convertView.findViewById(R.id.txt_project_name);
            holder.ppNup = (TextView) convertView.findViewById(R.id.txt_pp_nup);
            holder.ppName = (TextView) convertView.findViewById(R.id.txt_pp_name);

            convertView.setTag(holder);
        }else{
            holder = (ListUnPaidHolder) convertView.getTag();
        }

        NeedAttentionModel needAttentionModel = list.get(position);

        holder.orderCode.setText(needAttentionModel.getNupOrderCode());
        holder.qty.setText(needAttentionModel.getQty());
        holder.totalAmt.setText(needAttentionModel.getTotalAmt());
        holder.projectName.setText(needAttentionModel.getProjectName());
        holder.ppName.setText(needAttentionModel.getCustomerName());

        if (needAttentionModel.getNupNo()==null){
            holder.ppNup.setText("-");
        }else {
            holder.ppNup.setText(needAttentionModel.getNupNo());
        }

        //Log.d("cek",""+unPaid.getNupOrderCode()+""+unPaid.getQty()+""+unPaid.getTotalAmt());

        return convertView;
    }

    private class ListUnPaidHolder {
        TextView orderCode,qty,totalAmt,projectName,ppNup,ppName;
    }
}
