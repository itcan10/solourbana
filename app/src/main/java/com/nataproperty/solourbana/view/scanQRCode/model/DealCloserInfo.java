package com.nataproperty.solourbana.view.scanQRCode.model;

/**
 * Created by Nata on 3/15/2017.
 */

public class DealCloserInfo {
    String dealcloserName, dealcloserHp, dealcloserPhone, dealcloserEmail;

    public String getDealcloserName() {
        return dealcloserName;
    }

    public void setDealcloserName(String dealcloserName) {
        this.dealcloserName = dealcloserName;
    }

    public String getDealcloserHp() {
        return dealcloserHp;
    }

    public void setDealcloserHp(String dealcloserHp) {
        this.dealcloserHp = dealcloserHp;
    }

    public String getDealcloserPhone() {
        return dealcloserPhone;
    }

    public void setDealcloserPhone(String dealcloserPhone) {
        this.dealcloserPhone = dealcloserPhone;
    }

    public String getDealcloserEmail() {
        return dealcloserEmail;
    }

    public void setDealcloserEmail(String dealcloserEmail) {
        this.dealcloserEmail = dealcloserEmail;
    }
}
