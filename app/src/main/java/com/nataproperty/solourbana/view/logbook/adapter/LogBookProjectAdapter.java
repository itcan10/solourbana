package com.nataproperty.solourbana.view.logbook.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by UserModel on 4/19/2016.
 */
public class LogBookProjectAdapter extends BaseAdapter {
    public static final String PREF_NAME = "pref";

    private Context context;
    private List<LogBookModel> logbookList = null;
    private ArrayList<LogBookModel> list;
    private ListNotificationHolder holder;
    private String color;

    public LogBookProjectAdapter(Context context, ArrayList<LogBookModel> list, String color) {
        this.context = context;
        this.list = list;
        this.color = color;
    }

    SharedPreferences sharedPreferences;
    private boolean state;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_log_book_project, null);
            holder = new ListNotificationHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.hp = (TextView) convertView.findViewById(R.id.txt_hp);
            holder.email = (TextView) convertView.findViewById(R.id.txt_email);
            holder.address = (TextView) convertView.findViewById(R.id.txt_address);
            holder.projectCustomerStatusName = (TextView) convertView.findViewById(R.id.txt_projectCustomerStatusName);
            holder.eventName = (TextView) convertView.findViewById(R.id.txt_eventName);
            holder.referralName = (TextView) convertView.findViewById(R.id.txt_referralName);
            holder.itemList = (LinearLayout) convertView.findViewById(R.id.item_list);

            convertView.setTag(holder);
        } else {
            holder = (ListNotificationHolder) convertView.getTag();
        }

        LogBookModel logBookModel = list.get(position);
        holder.name.setText(logBookModel.getName());
        holder.hp.setText(logBookModel.getHp1());
        if (logBookModel.getEmail1().equals("")) {
            holder.email.setText("-");
        } else {
            holder.email.setText(logBookModel.getEmail1());
        }
        if (logBookModel.getAddress().trim().equals("")) {
            holder.address.setText(logBookModel.getCityName() + ", " + logBookModel.getProvinceName());
        } else {
            holder.address.setText(logBookModel.getAddress() + ", " + logBookModel.getCityName() + ", " + logBookModel.getProvinceName());
        }
        holder.projectCustomerStatusName.setText(logBookModel.getProjectCustomerStatusName());
        holder.eventName.setText(logBookModel.getEventName());
        holder.referralName.setText(logBookModel.getReferralName());
        holder.itemList.setBackgroundColor(Color.parseColor(logBookModel.getColorStatus()));
        return convertView;
    }

    private class ListNotificationHolder {
        TextView name, hp, email, address, projectCustomerStatusName, eventName, referralName;
        LinearLayout itemList;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        logbookList.clear();
        if (charText.length() == 0) {
            logbookList.addAll(list);
        }
        else
        {
            for ( LogBookModel log : list)
            {
                if (log.getName().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    logbookList.add(log);
                }
            }
        }
        notifyDataSetChanged();
    }

}
