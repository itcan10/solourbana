package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by UserModel on 5/13/2016.
 */
public class PaymentTermModel {
    String termRef,termNo,termName,discount,downPayment,priceIncTerm,netPrice,finType,dpNo;

    public String getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    public String getTermRef() {
        return termRef;
    }

    public void setTermRef(String termRef) {
        this.termRef = termRef;
    }

    public String getTermNo() {
        return termNo;
    }

    public void setTermNo(String termNo) {
        this.termNo = termNo;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(String downPayment) {
        this.downPayment = downPayment;
    }

    public String getPriceIncTerm() {
        return priceIncTerm;
    }

    public void setPriceIncTerm(String priceIncTerm) {
        this.priceIncTerm = priceIncTerm;
    }

    public String getFinType() {
        return finType;
    }

    public void setFinType(String finType) {
        this.finType = finType;
    }

    public String getDpNo() {
        return dpNo;
    }

    public void setDpNo(String dpNo) {
        this.dpNo = dpNo;
    }
}
