package com.nataproperty.solourbana.view.ilustration.ui;

import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.ilustration.adapter.IlustrationProductAdapter;
import com.nataproperty.solourbana.view.ilustration.model.GenerateClusterInfoParam;
import com.nataproperty.solourbana.view.ilustration.model.IlustrationModel;
import com.nataproperty.solourbana.view.ilustration.model.IlustrationStatusModel;
import com.nataproperty.solourbana.view.ilustration.presenter.IlustrationProductPresenter;
import com.nataproperty.solourbana.view.mainmenu.ui.MainMenuActivity;
import com.nataproperty.solourbana.view.project.model.IlustrationProductModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/4/2016.
 */
public class IlustrationProductActivity extends AppCompatActivity implements View.OnClickListener {
    private ServiceRetrofit service;
    private IlustrationProductPresenter presenter;
    private List<IlustrationModel> listIlustration = new ArrayList<IlustrationModel>();
    private IlustrationProductAdapter ilustrationAdapter;
    ProgressDialog progressDialog;
    AlertDialog b;
    ImageView imgLogo;
    TextView txtProjectName, txtChooseIlustration, txtCluster;
    long dbMasterRef;
    String projectRef, categoryRef, clusterRef, productRef, clusterDescription, projectName,
            titleProduct, priceRangeStart, priceRangeEnd, isAllowCalculatePrice, isNUP, isBooking,urlSitePlan,
            isShowAvailableUnit, specialEnquiries, isShowDiagramMaticUnit, isShowSitePlan, imageLogo, isJoin, isWaiting, clusterName, imageCluster, memberRef;
    EditText edtSearch;
    Button btnSearch, btnDiagramMatic, btnSearchFloor, btnSpecialEnquiries, btnSitePlan;
    MyListView listView;
    long unitRef;
    ImageView imgSparator;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Intent intent;

    LinearLayout linearNoData, linearLayoutDiagramMatic;
    Display display;
    Point size;
    Integer width;
    Double result;
    RelativeLayout rPage;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_product);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new IlustrationProductPresenter(this, service);
        intent = getIntent();
        dbMasterRef = intent.getLongExtra(General.DBMASTER_REF, 0);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        categoryRef = intent.getStringExtra(General.CATEGORY_REF);
        clusterRef = intent.getStringExtra(General.CLUSTER_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        productRef = intent.getStringExtra(General.PRODUCT_REF);
        titleProduct = intent.getStringExtra(General.TITLE_PRODUCT);
        isShowAvailableUnit = intent.getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        imageLogo = intent.getStringExtra(General.IMAGE_LOGO);
        clusterName = intent.getStringExtra(General.CLUSTER_NAME);
        imageCluster = intent.getStringExtra(imageCluster);


        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        isWaiting = sharedPreferences.getString("isWaiting", "");
        isJoin = sharedPreferences.getString("isJoin", "");
        memberRef = sharedPreferences.getString("isMemberRef", null);
        initWidget();

        Log.d("cek product ilustrasi", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef + " " + isShowAvailableUnit);
        txtProjectName.setText(projectName);
        txtCluster.setText(clusterName);

        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        btnDiagramMatic.setOnClickListener(this);
        btnSitePlan.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnSearchFloor.setOnClickListener(this);
        retry.setOnClickListener(this);
        initAdapter();

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edtSearch.getText().toString().equals("")) {
                        edtSearch.setError("Keyword empty");
                    } else {
                        listIlustration.clear();
                        //ilustrationAdapter.notifyDataSetChanged();
                        Log.d("count", "" + String.valueOf(listIlustration.size()));
                        requestIlustration();

                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                        return true;
                    }
                }
                return false;
            }
        });

        /**
         * list cari unit
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productRef = listIlustration.get(position).getProductRef();
                unitRef = listIlustration.get(position).getUnitRef();

                Intent intent = new Intent(IlustrationProductActivity.this, IlustrationPaymentTermActivity.class);
                intent.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                intent.putExtra(General.PROJECT_REF, projectRef);
                intent.putExtra(General.CATEGORY_REF, categoryRef);
                intent.putExtra(General.CLUSTER_REF, clusterRef);
                intent.putExtra(General.PROJECT_NAME, projectName);
                intent.putExtra(General.PRODUCT_REF, productRef);
                intent.putExtra(General.UNIT_REF, String.valueOf(unitRef));
                intent.putExtra(General.IS_BOOKING, isBooking);
                startActivity(intent);

            }
        });

        requestClusterInfo();

    }

    private void initAdapter() {
        ilustrationAdapter = new IlustrationProductAdapter(this, listIlustration);
        listView.setAdapter(ilustrationAdapter);
        listView.setExpanded(true);
    }

    private void initWidget() {
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText(getResources().getString(R.string.title_illustrastion));
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        edtSearch = (EditText) findViewById(R.id.txt_keyword_search);
        btnSearch = (Button) findViewById(R.id.btn_search);
        btnSearchFloor = (Button) findViewById(R.id.btn_search_lantai);
        imgSparator = (ImageView) findViewById(R.id.imgSparator);
        linearLayoutDiagramMatic = (LinearLayout) findViewById(R.id.linear_diagrammatic);

        btnDiagramMatic = (Button) findViewById(R.id.btn_diagram_matic);
        btnDiagramMatic.setTypeface(font);
        btnSitePlan = (Button) findViewById(R.id.btn_site_plan);
        btnSitePlan.setTypeface(font);
        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        listView = (MyListView) findViewById(R.id.list_ilustration);
        btnSearch.setTypeface(font);
        btnSearchFloor.setTypeface(font);
        txtChooseIlustration = (TextView) findViewById(R.id.txt_choose_ilustration);
        txtCluster = (TextView) findViewById(R.id.txt_cluster);
    }

    public void requestClusterInfo() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        GenerateClusterInfoParam param = new GenerateClusterInfoParam();
        param.setDbMasterRef(String.valueOf(dbMasterRef));
        param.setProjectRef(projectRef);
        param.setClusterRef(clusterRef);
        param.setMemberRef(memberRef);
        param.setProductRef(productRef);
        presenter.getClusterInfo(param);
        String js = (Utils.modelToJson(param));
        System.out.print("test" + js);
//        presenter.getClusterInfo(String.valueOf(dbMasterRef), projectRef, clusterRef);
    }


    public void requestIlustration() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getIlustration(String.valueOf(dbMasterRef), projectRef, clusterRef, edtSearch.getText().toString(), productRef);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_diagram_matic:
                if (isJoin.equals("0")) {
                    if (isWaiting.equals("1")) {
                        //sudah join tetapi belum di approv
                        dialogNotJoin(getString(R.string.waitingJoinBooking).replace("@projectName", projectName));
                    } else {
                        //belum join
                        dialogNotJoin(getString(R.string.notJoinBooking).replace("@projectName", projectName));
                    }

                } else {
                    //jika dia sudah join
                    Intent intent = new Intent(this, DiagramMaticInflateActivity.class);
                    intent.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                    intent.putExtra(General.PROJECT_REF, projectRef);
                    intent.putExtra(General.CATEGORY_REF, categoryRef);
                    intent.putExtra(General.CLUSTER_REF, clusterRef);
                    intent.putExtra(General.PROJECT_NAME, projectName);
                    intent.putExtra(General.PRODUCT_REF, productRef);
                    intent.putExtra(General.TITLE_PRODUCT, titleProduct);
                    intent.putExtra(General.IS_BOOKING, isBooking);
                    intent.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intent.putExtra(General.CLUSTER_NAME, clusterName);
                    startActivity(intent);
                }
                break;
            case R.id.btn_search:
                String keyword = edtSearch.getText().toString();
                if (isJoin.equals("0")) {
                    if (isWaiting.equals("1")) {
                        //sudah join tetapi belum di approv
                        dialogNotJoin(getString(R.string.waitingJoinBooking).replace("@projectName", projectName));
                    } else {
                        //belum join
                        dialogNotJoin(getString(R.string.notJoinBooking).replace("@projectName", projectName));
                    }
                } else {
                    //jika dia sudah join
                    if (!keyword.isEmpty()) {
                        listIlustration.clear();
                        //ilustrationAdapter.notifyDataSetChanged();
                        requestIlustration();
                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);

                    } else {
                        edtSearch.setError("Harus diisi.");
                    }
                }

                break;
            case R.id.btn_search_lantai:
                if (isJoin.equals("0")) {
                    if (isWaiting.equals("1")) {
                        //sudah join tetapi belum di approv
                        dialogNotJoin(getString(R.string.waitingJoinBooking).replace("@projectName", projectName));
                    } else {
                        //belum join
                        dialogNotJoin(getString(R.string.notJoinBooking).replace("@projectName", projectName));
                    }

                } else {
                    //jika dia sudah join
                    Intent intentFloor = new Intent(IlustrationProductActivity.this, FloorMappingActivity.class);
                    intentFloor.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                    intentFloor.putExtra(General.PROJECT_REF, projectRef);
                    intentFloor.putExtra(General.CATEGORY_REF, categoryRef);
                    intentFloor.putExtra(General.CLUSTER_REF, clusterRef);
                    intentFloor.putExtra(General.PROJECT_NAME, projectName);
                    intentFloor.putExtra(General.IS_BOOKING, isBooking);
                    intentFloor.putExtra(General.PRODUCT_REF, productRef);
                    intentFloor.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentFloor.putExtra(General.TITLE_PRODUCT, titleProduct);
                    startActivity(intentFloor);
                }

                break;
            case R.id.btn_site_plan:
                //jika dia sudah join
                Intent intentFloor = new Intent(IlustrationProductActivity.this, FloorPlanUrlActivity.class);
                intentFloor.putExtra(General.VIEW_FLOOR_PLAN_URL, urlSitePlan);
                intentFloor.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                intentFloor.putExtra(General.PROJECT_REF, projectRef);
                intentFloor.putExtra(General.CATEGORY_REF, categoryRef);
                intentFloor.putExtra(General.CLUSTER_REF, clusterRef);
                intentFloor.putExtra(General.PROJECT_NAME, projectName);
                intentFloor.putExtra(General.IS_BOOKING, isBooking);
                intentFloor.putExtra(General.PRODUCT_REF, productRef);
                startActivity(intentFloor);
                break;
            case R.id.main_retry:
                startActivity(new Intent(IlustrationProductActivity.this, LaunchActivity.class));
                finish();
                break;

        }
    }

    private void dialogNotJoin(String notJoint) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IlustrationProductActivity.this);
        LayoutInflater inflater = IlustrationProductActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView textView = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Notifikasi");
        textView.setText(notJoint);
        dialogBuilder.setPositiveButton("GO TO PROJECT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intentProjectMenu = new Intent(IlustrationProductActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                finish();
            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        b = dialogBuilder.create();
        b.show();
    }

    public void showClusterInfoResults(retrofit2.Response<IlustrationStatusModel> response) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = false;
        snackBarBuatan.setVisibility(View.GONE);
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        isShowDiagramMaticUnit = response.body().getData().getIsShowDiagramMaticUnit();
        isShowSitePlan = response.body().getData().getIsShowSitePlan();

        if (Integer.parseInt(isShowDiagramMaticUnit) == 1) {
            btnDiagramMatic.setVisibility(View.VISIBLE);
        }
        else if (Integer.parseInt(isShowSitePlan) == 1) {
            btnSitePlan.setVisibility(View.VISIBLE);
        }
        else if (Integer.parseInt(isShowDiagramMaticUnit) == 0 && Integer.parseInt(isShowSitePlan) == 0) {
            linearLayoutDiagramMatic.setVisibility(View.GONE);
        }

        if (status == 200) {
            IlustrationProductModel ilustrationProductModel = new IlustrationProductModel();
            ilustrationProductModel.setDbMasterRef(response.body().getData().getDbMasterRef());
            ilustrationProductModel.setProjectRef(response.body().getData().getProjectRef());
            ilustrationProductModel.setCategoryRef(response.body().getData().getCategoryRef());
            ilustrationProductModel.setClusterRef(response.body().getData().getClusterRef());
            ilustrationProductModel.setClusterDescription(response.body().getData().getClusterDescription());
            ilustrationProductModel.setPriceRangeStart(response.body().getData().getPriceRangeStart());
            ilustrationProductModel.setPriceRangeEnd(response.body().getData().getPriceRangeEnd());
            ilustrationProductModel.setIsAllowCalculatePrice(response.body().getData().getIsAllowCalculatePrice());
            ilustrationProductModel.setIsNUP(response.body().getData().getIsNUP());
            ilustrationProductModel.setIsBooking(response.body().getData().getIsBooking());
            ilustrationProductModel.setIsShowAvailableUnit(response.body().getData().getIsShowAvailableUnit());
            ilustrationProductModel.setIsShowDiagramMaticUnit(response.body().getData().getIsShowDiagramMaticUnit());
            ilustrationProductModel.setIsShowSitePlan(response.body().getData().getIsShowSitePlan());
            ilustrationProductModel.setUrlSitePlan(response.body().getData().getUrlSitePlan());

            dbMasterRef = response.body().getData().getDbMasterRef();
            projectRef = response.body().getData().getProjectRef();
            categoryRef = response.body().getData().getCategoryRef();
            clusterRef = response.body().getData().getClusterRef();
            clusterDescription = response.body().getData().getClusterDescription();
            priceRangeStart = response.body().getData().getPriceRangeStart();
            priceRangeEnd = response.body().getData().getPriceRangeEnd();
            isAllowCalculatePrice = response.body().getData().getIsAllowCalculatePrice();
            isNUP = response.body().getData().getIsNUP();
            isBooking = response.body().getData().getIsBooking();
            urlSitePlan = response.body().getData().getUrlSitePlan();

        } else {
            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showClusterInfoFailure(Throwable t) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = true;
        snackBarBuatan.setVisibility(View.VISIBLE);

    }

    public void showIlustrationResults(retrofit2.Response<List<IlustrationModel>> response) {
        progressDialog.dismiss();
        listIlustration = response.body();
        if (listIlustration.size() != 0) {
            linearNoData.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            txtChooseIlustration.setVisibility(View.VISIBLE);
            ilustrationAdapter.notifyDataSetChanged();
            initAdapter();

        } else {
            linearNoData.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            txtChooseIlustration.setVisibility(View.VISIBLE);
        }
    }

    public void showIlustrationFailure(Throwable t) {
        progressDialog.dismiss();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (b != null && b.isShowing()) {
            b.dismiss();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationProductActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
