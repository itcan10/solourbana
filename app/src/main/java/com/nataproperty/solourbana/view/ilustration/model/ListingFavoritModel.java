package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by UserModel on 7/26/2016.
 */
public class ListingFavoritModel {
    long listingRef;
    String statusBookmark;
    String listingTitle,price,subLocationName,cityName,provinceName,
            listingTypeName,priceTypeName,imageCover,memberName,memberRef,agencyCompanyRef;

    double buildArea,landArea,buildDimX,buildDimY,landDimX,landDimY;

    public String getStatusBookmark() {
        return statusBookmark;
    }

    public void setStatusBookmark(String statusBookmark) {
        this.statusBookmark = statusBookmark;
    }

    public long getListingRef() {
        return listingRef;
    }

    public void setListingRef(long listingRef) {
        this.listingRef = listingRef;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubLocationName() {
        return subLocationName;
    }

    public void setSubLocationName(String subLocationName) {
        this.subLocationName = subLocationName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getListingTypeName() {
        return listingTypeName;
    }

    public void setListingTypeName(String listingTypeName) {
        this.listingTypeName = listingTypeName;
    }

    public String getPriceTypeName() {
        return priceTypeName;
    }

    public void setPriceTypeName(String priceTypeName) {
        this.priceTypeName = priceTypeName;
    }

    public String getImageCover() {
        return imageCover;
    }

    public void setImageCover(String imageCover) {
        this.imageCover = imageCover;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberRef() {
        return memberRef;
    }

    public void setMemberRef(String memberRef) {
        this.memberRef = memberRef;
    }

    public String getAgencyCompanyRef() {
        return agencyCompanyRef;
    }

    public void setAgencyCompanyRef(String agencyCompanyRef) {
        this.agencyCompanyRef = agencyCompanyRef;
    }

    public double getBuildArea() {
        return buildArea;
    }

    public void setBuildArea(double buildArea) {
        this.buildArea = buildArea;
    }

    public double getLandArea() {
        return landArea;
    }

    public void setLandArea(double landArea) {
        this.landArea = landArea;
    }

    public double getBuildDimX() {
        return buildDimX;
    }

    public void setBuildDimX(double buildDimX) {
        this.buildDimX = buildDimX;
    }

    public double getBuildDimY() {
        return buildDimY;
    }

    public void setBuildDimY(double buildDimY) {
        this.buildDimY = buildDimY;
    }

    public double getLandDimX() {
        return landDimX;
    }

    public void setLandDimX(double landDimX) {
        this.landDimX = landDimX;
    }

    public double getLandDimY() {
        return landDimY;
    }

    public void setLandDimY(double landDimY) {
        this.landDimY = landDimY;
    }
}
