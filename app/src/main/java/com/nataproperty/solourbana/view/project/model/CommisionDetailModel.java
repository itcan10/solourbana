package com.nataproperty.solourbana.view.project.model;

/**
 * Created by nata on 11/25/2016.
 */

public class CommisionDetailModel {
    long id;
    String commision;
    Double commissionAmt;
    String closingFee;
    String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public Double getCommissionAmt() {
        return commissionAmt;
    }

    public void setCommissionAmt(Double commissionAmt) {
        this.commissionAmt = commissionAmt;
    }

    public String getClosingFee() {
        return closingFee;
    }

    public void setClosingFee(String closingFee) {
        this.closingFee = closingFee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
