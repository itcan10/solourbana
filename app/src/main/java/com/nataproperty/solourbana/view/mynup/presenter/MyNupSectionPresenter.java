package com.nataproperty.solourbana.view.mynup.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.mynup.intractor.PresenterMyNupSectionInteractor;
import com.nataproperty.solourbana.view.mynup.model.MyNupSectionModel;
import com.nataproperty.solourbana.view.mynup.ui.MyNupSectionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyNupSectionPresenter implements PresenterMyNupSectionInteractor {
    private MyNupSectionActivity view;
    private ServiceRetrofitProject service;

    public MyNupSectionPresenter(MyNupSectionActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListNupSection(String memberRef, String projectCode) {
        Call<List<MyNupSectionModel>> call = service.getAPI().getListNupSection(memberRef,projectCode);
        call.enqueue(new Callback<List<MyNupSectionModel>>() {
            @Override
            public void onResponse(Call<List<MyNupSectionModel>> call, Response<List<MyNupSectionModel>> response) {
                view.showListNupSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<MyNupSectionModel>> call, Throwable t) {
                view.showListNupSectionFailure(t);

            }


        });
    }

}
