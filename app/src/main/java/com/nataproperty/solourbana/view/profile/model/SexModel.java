package com.nataproperty.solourbana.view.profile.model;

/**
 * Created by UserModel on 5/15/2016.
 */
public class SexModel {
    String sexRef;
    String sexName;
    String sexSortNo;

    public String getSexRef() {
        return sexRef;
    }

    public void setSexRef(String sexRef) {
        this.sexRef = sexRef;
    }

    public String getSexName() {
        return sexName;
    }

    public void setSexName(String sexName) {
        this.sexName = sexName;
    }

    public String getSexSortNo() {
        return sexSortNo;
    }

    public void setSexSortNo(String sexSortNo) {
        this.sexSortNo = sexSortNo;
    }
}
