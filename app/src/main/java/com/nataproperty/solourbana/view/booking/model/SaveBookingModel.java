package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class SaveBookingModel {
    int status;
    String message;

    public int getStatus() {
        return status;
    }

    String bookingNotification;

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBookingNotification() {
        return bookingNotification;
    }

    public void setBookingNotification(String bookingNotification) {
        this.bookingNotification = bookingNotification;
    }
}
