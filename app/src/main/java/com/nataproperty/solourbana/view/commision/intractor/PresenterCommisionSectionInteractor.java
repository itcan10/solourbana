package com.nataproperty.solourbana.view.commision.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCommisionSectionInteractor {
    void getListCommisionSection(String memberRef,String projectCode);

}
