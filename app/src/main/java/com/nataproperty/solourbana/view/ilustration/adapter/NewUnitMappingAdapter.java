package com.nataproperty.solourbana.view.ilustration.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.ilustration.model.MapingModelNew;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class NewUnitMappingAdapter extends BaseAdapter {
    private Context context;
    private List<MapingModelNew> list;
    private ListUnitMappingHolder holder;

    public NewUnitMappingAdapter(Context context, List<MapingModelNew> list) {
        this.context = context;
        this.list = list;
    }

    MapingModelNew unit;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_unit_mapping, null);
            holder = new ListUnitMappingHolder();
            holder.unitName = (TextView) convertView.findViewById(R.id.txt_unit_name);
            holder.productName = (TextView) convertView.findViewById(R.id.txt_product_name);
            holder.linearItemUnit = (LinearLayout) convertView.findViewById(R.id.linear_item_unit);

            convertView.setTag(holder);
        } else {
            holder = (ListUnitMappingHolder) convertView.getTag();
        }

        unit = list.get(position);
        holder.unitName.setText(unit.getUnitName());
        holder.productName.setText(unit.getProductName());
        holder.linearItemUnit.setBackgroundColor(Color.parseColor(unit.getColor()));

        //color1 = unit.getColor1();
        //color2 = unit.getColor2();

//        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
//        memberRef = sharedPreferences.getString("isMemberRef", null);
//        memberType = sharedPreferences.getString("isMemberType", null);

        //requestDiagramColor();
//        if (!color1.equals("") && !color2.equals("")) {
//            if (unit.getUnitStatus().equals("A")) {
//                holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorGreen));
//            } else {
//                holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
//            }
//
//        } else {
//            if (isShowAvailableUnit.equals("1")) {
//                if (unit.getUnitStatus().equals("A")) {
//                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorGreen));
//                } else {
//                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
//                }
//            }
//        }
//
//        if (!productRef.equals("")) {
//            if (memberType.startsWith("Inhouse")) {
//                if (unit.getUnitStatus().equals("A")) {
//                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorGreen));
//                } else {
//                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
//                }
//            } else {
//                if (unit.getProductRef().equals(productRef)) {
//                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorGray));
//                }
//            }
//        }

        return convertView;
    }

    private class ListUnitMappingHolder {
        TextView unitName, productName;
        LinearLayout linearItemUnit;
    }
}
