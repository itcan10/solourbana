package com.nataproperty.solourbana.view.profile.model;

/**
 * Created by UserModel on 4/25/2016.
 */
public class CountryModel {
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    String countryCode;
    String countryName;

}
