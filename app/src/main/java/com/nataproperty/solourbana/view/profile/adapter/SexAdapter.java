package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.SexModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class SexAdapter extends BaseAdapter {
    private Context context;
    private List<SexModel> list;
    private ListSexHolder holder;

    public SexAdapter(Context context, List<SexModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_region,null);
            holder = new ListSexHolder();
            holder.sexName = (TextView) convertView.findViewById(R.id.txt_region);

            convertView.setTag(holder);
        }else{
            holder = (ListSexHolder) convertView.getTag();
        }
        SexModel nation = list.get(position);
        holder.sexName.setText(nation.getSexName());

        return convertView;
    }

    private class ListSexHolder {
        TextView sexName;
    }
}
