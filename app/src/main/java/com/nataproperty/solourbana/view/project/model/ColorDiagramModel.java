package com.nataproperty.solourbana.view.project.model;

/**
 * Created by herlambang-nata on 11/1/2016.
 */
public class ColorDiagramModel {
    long memberRef;
    int status;
    String color1;
    String color2;

    public long getMemberRef() {
        return memberRef;
    }

    public void setMemberRef(long memberRef) {
        this.memberRef = memberRef;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getColor1() {
        return color1;
    }

    public void setColor1(String color1) {
        this.color1 = color1;
    }

    public String getColor2() {
        return color2;
    }

    public void setColor2(String color2) {
        this.color2 = color2;
    }
}
