package com.nataproperty.solourbana.view.commision.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.commision.model.CommisionDetailModel;

import java.util.List;

/**
 * Created by UserModel on 4/21/2016.
 */
public class CommisionDetailAdapter extends BaseAdapter {
    public static final String TAG = "MyNupAdapter";

    private Context context;
    private List<CommisionDetailModel> list;
    private ListMyNupHolder holder;

    public CommisionDetailAdapter(Context context, List<CommisionDetailModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_commision_detail, null);
            holder = new ListMyNupHolder();
            holder.payOrderCode = (TextView) convertView.findViewById(R.id.txt_payOrderCode);
            holder.comPayNo = (TextView) convertView.findViewById(R.id.txt_comPayNo);
            holder.schedDate = (TextView) convertView.findViewById(R.id.txt_schedDate);
            holder.comAmt = (TextView) convertView.findViewById(R.id.txt_comAmt);
            holder.pphAmt = (TextView) convertView.findViewById(R.id.txt_pphAmt);
            holder.ppnAmt = (TextView) convertView.findViewById(R.id.txt_ppnAmt);
            holder.netPaidAmt = (TextView) convertView.findViewById(R.id.txt_netPaidAmt);
            holder.paidDate = (TextView) convertView.findViewById(R.id.txt_paidDate);

            convertView.setTag(holder);
        } else {
            holder = (ListMyNupHolder) convertView.getTag();
        }

        CommisionDetailModel commisionDetailModel = list.get(position);
        holder.payOrderCode.setText(commisionDetailModel.getPayOrderCode());
        holder.comPayNo.setText(commisionDetailModel.getComPayNo());
        holder.schedDate.setText(commisionDetailModel.getSchedDate());
        holder.comAmt.setText(commisionDetailModel.getComAmt());
        holder.pphAmt.setText(commisionDetailModel.getPphAmt());
        holder.ppnAmt.setText(commisionDetailModel.getPpnAmt());
        holder.netPaidAmt.setText(commisionDetailModel.getNetPaidAmt());
        holder.paidDate.setText(commisionDetailModel.getPaidDate());

        return convertView;
    }

    public class ListMyNupHolder {
        TextView payOrderCode, comPayNo, schedDate, comAmt, pphAmt, ppnAmt, netPaidAmt,paidDate;

    }
}