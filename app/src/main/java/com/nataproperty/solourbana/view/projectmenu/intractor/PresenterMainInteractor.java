package com.nataproperty.solourbana.view.projectmenu.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMainInteractor {
    void sendGCMToken(String GCMToken, String memberRef);

    void CheckAvailableReportSvc(String projectCode, String dbMasterRef, String projectRef, String memberRef);
}
