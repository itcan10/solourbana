package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by nata on 3/27/2018.
 */

public class GenerateKalkulatorKPRParam {
    String dbMasterRef;
    String projectRef;
    String projectCode;
    String clusterRef;
    String productRef;
    String unitRef;
    String termRef;
    String termNo;
    String KPRYear;
    String discRoomPrice;
    String income;
    String tenor;
    String memberRef;

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public String getUnitRef() {
        return unitRef;
    }

    public void setUnitRef(String unitRef) {
        this.unitRef = unitRef;
    }

    public String getTermRef() {
        return termRef;
    }

    public void setTermRef(String termRef) {
        this.termRef = termRef;
    }

    public String getTermNo() {
        return termNo;
    }

    public void setTermNo(String termNo) {
        this.termNo = termNo;
    }

    public String getKPRYear() {
        return KPRYear;
    }

    public void setKPRYear(String KPRYear) {
        this.KPRYear = KPRYear;
    }

    public String getDiscRoomPrice() {
        return discRoomPrice;
    }

    public void setDiscRoomPrice(String discRoomPrice) {
        this.discRoomPrice = discRoomPrice;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getMemberRef() {
        return memberRef;
    }

    public void setMemberRef(String memberRef) {
        this.memberRef = memberRef;
    }
}
