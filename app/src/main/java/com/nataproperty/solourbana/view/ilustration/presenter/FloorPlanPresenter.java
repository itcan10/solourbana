package com.nataproperty.solourbana.view.ilustration.presenter;





import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.ilustration.intractor.FloorPlanInteractor;
import com.nataproperty.solourbana.view.ilustration.model.FloorPlanRespone;
import com.nataproperty.solourbana.view.ilustration.ui.FloorPlanUrlActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class FloorPlanPresenter implements FloorPlanInteractor {
    private FloorPlanUrlActivity view;
    private ServiceRetrofit service;

    public FloorPlanPresenter(FloorPlanUrlActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getUnitStatusSVC(String jsonIn, final String unitRef, final String productRef) {
        Call<FloorPlanRespone> call = service.getAPI().getUnitStatusSVC(jsonIn);
        call.enqueue(new Callback<FloorPlanRespone>() {
            @Override
            public void onResponse(Call<FloorPlanRespone> call, Response<FloorPlanRespone> response) {
                view.showResults(response, unitRef, productRef);
            }

            @Override
            public void onFailure(Call<FloorPlanRespone> call, Throwable t) {
                view.showFailure(t);

            }


        });
    }
}
