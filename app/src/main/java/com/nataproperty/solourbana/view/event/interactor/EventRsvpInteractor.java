package com.nataproperty.solourbana.view.event.interactor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface EventRsvpInteractor {
    void saveRsvpSvc(Map<String, String> fields) ;

}
