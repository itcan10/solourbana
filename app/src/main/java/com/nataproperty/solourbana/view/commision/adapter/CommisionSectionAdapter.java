package com.nataproperty.solourbana.view.commision.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.commision.model.CommisionSectionModel;

import java.util.List;

/**
 * Created by UserModel on 4/21/2016.
 */
public class CommisionSectionAdapter extends BaseAdapter {
    public static final String TAG = "MyNupAdapter";

    private Context context;
    private List<CommisionSectionModel> list;
    private ListMyNupHolder holder;

    public CommisionSectionAdapter(Context context, List<CommisionSectionModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_mynup,null);
            holder = new ListMyNupHolder();
            holder.sectionName = (TextView) convertView.findViewById(R.id.txt_nup_type);
            holder.bookingCount = (TextView) convertView.findViewById(R.id.txt_nup_count);

            convertView.setTag(holder);
        }else{
            holder = (ListMyNupHolder) convertView.getTag();
        }

        CommisionSectionModel commisionSectionModel = list.get(position);
        holder.sectionName.setText(commisionSectionModel.getSectionName());
        holder.bookingCount.setText(commisionSectionModel.getCommissionCount());

        return convertView;
    }

    public class ListMyNupHolder
    {
        TextView sectionName,bookingCount;

    }
}