package com.nataproperty.solourbana.view.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.project.model.LocationModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class LocationAdapter extends BaseAdapter {
    private Context context;
    private List<LocationModel> list;
    private ListFeatureHolder holder;

    public LocationAdapter(Context context, List<LocationModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.spinner_item_project,null);
            holder = new ListFeatureHolder();
            holder.locationName = (TextView) convertView.findViewById(R.id.txt_location_name);

            convertView.setTag(holder);
        }else{
            holder = (ListFeatureHolder) convertView.getTag();
        }

        LocationModel locationModel = list.get(position);
        holder.locationName.setText(locationModel.getLocationName());

       /* Picasso.with(context)
                .load("http://192.168.10.180/andro/wf/support/displayImage.aspx?is=cluster&dr=5&pr=1&cr=1&quot;).into(holder.clusterImg);*/

        return convertView;

    }

    private class ListFeatureHolder {
        TextView locationName;
    }
}
