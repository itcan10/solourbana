package com.nataproperty.solourbana.view.report.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.report.intractor.ReportInteractor;
import com.nataproperty.solourbana.view.report.model.ReportModel;
import com.nataproperty.solourbana.view.report.ui.ReportActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ReportPresenter implements ReportInteractor {
    private ReportActivity view;
    private ServiceRetrofitProject service;

    public ReportPresenter(ReportActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void GetListDashboardReport(String memberRef ) {
        Call<ArrayList<ReportModel>> call = service.getAPI().GetListDashboardReport(memberRef);
        call.enqueue(new Callback<ArrayList<ReportModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ReportModel>> call, Response<ArrayList<ReportModel>> response) {
                view.showListReportResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<ReportModel>> call, Throwable t) {
                view.showListReportFailure(t);

            }


        });
    }
}
