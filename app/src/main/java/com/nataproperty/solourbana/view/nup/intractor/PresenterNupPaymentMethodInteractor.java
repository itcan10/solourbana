package com.nataproperty.solourbana.view.nup.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterNupPaymentMethodInteractor {
    void GetPaymentMethodSvc(String dbMasterRef, String projectRef);

    void updatePaymentTypeOrderSvc (String nupOrderRef, String paymentType,String memberRef);

    void updatePaymentTypeOrderVTSvc (String nupOrderRef,String paymentType, String dbMasterRef, String projectRef,
                                      String memberRef, String nupAmt, String total, String paymentMethod);

}
