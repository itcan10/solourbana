package com.nataproperty.solourbana.view.report.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface ReportGenerateInteractor {
    void GenerateDashboardReportSvc(String projectCode, String dbMasterRef, String projectRef, String memberRef, String userRef, String aspx);

}
