package com.nataproperty.solourbana.view.project.model;

/**
 * Created by nata on 11/25/2016.
 */

public class DetailStatusModel {
    int status;
    String message;
    MenuProjectModel2 data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MenuProjectModel2 getData() {
        return data;
    }

    public void setData(MenuProjectModel2 data) {
        this.data = data;
    }
}
