package com.nataproperty.solourbana.view.booking.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterBookingBankTransferInteractor {

    void getIlustrationPaymentSvc(String dbMasterRef, String projectRef, String clusterRef, String productRef,
                                  String unitRef, String termRef, String termNo);

    void getProjectAccountBank(String dbMasterRef, String projectRef);

    void getListBankSvc();

    void getBookingDetailSvc (String dbMasterRef,String projectRef, String bookingRef);

    void savePaymentBookingBankTransferSvc(String jsonIn);


}
