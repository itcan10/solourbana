package com.nataproperty.solourbana.view.project.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.view.project.model.MyListingModel;
import com.nataproperty.solourbana.view.project.ui.MyListingActivity;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by UserModel on 10/28/2016.
 */
public class RVMyListAdapter extends RecyclerView.Adapter<RVMyListAdapter.ProjectRequestHolder> {
    public static final String PREF_NAME = "pref";
    List<MyListingModel> projectModels;
    private Context context;
    private Display display;
    String memberRef, dbMasterRef, projectRef;


    Integer position = 0; //Need to declare because onbind is executed after oncreate, and idk how to get position on oncreate

    public RVMyListAdapter(Context context, List<MyListingModel> projectModels1, Display display) {
        this.context = context;
        this.display = display;
        this.projectModels = projectModels1;
    }

    public static class ProjectRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cv;
        TextView projectName, locationName, subLocationName;
        ImageView projectImg, btn_project, btn_remove;
        Context context;
        SharedPreferences sharedPreferences;
        private boolean state;

        private String memberRef, dbMasterRef, projectRef, memberType;

        ProjectRequestHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            cv = (CardView) itemView.findViewById(R.id.cv);
            projectName = (TextView) itemView.findViewById(R.id.projectName);
            locationName = (TextView) itemView.findViewById(R.id.locationName);
            subLocationName = (TextView) itemView.findViewById(R.id.subLocationName);
            projectImg = (ImageView) itemView.findViewById(R.id.imgProject);
            btn_project = (ImageView) itemView.findViewById(R.id.btnProject);
            btn_remove = (ImageView) itemView.findViewById(R.id.btnRemove);

        }

        @Override
        public void onClick(final View v) {

        }

    }

    @Override
    public ProjectRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_mylisting, parent, false);
        final ProjectRequestHolder holder = new ProjectRequestHolder(v);
/*        final Context context = holder.header.getContext();*/
        ProjectRequestHolder crh = new ProjectRequestHolder(v);

        return crh;
    }

    @Override
    public void onBindViewHolder(final ProjectRequestHolder holder, final int position) {
        this.position = position + 1;
        holder.sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        holder.state = holder.sharedPreferences.getBoolean("isLogin", false);
        holder.memberRef = holder.sharedPreferences.getString("isMemberRef", null);
        holder.memberType = holder.sharedPreferences.getString("isMemberType", null);
        final MyListingModel project = projectModels.get(position);

        Log.d("onBindViewHolder", memberRef + " " + dbMasterRef + " " + projectRef);

        holder.projectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProjectMenuActivity.class);
                intent.putExtra(General.PROJECT_REF, project.getProjectRef());
                intent.putExtra(General.DBMASTER_REF, project.getDbMasterRef());
                intent.putExtra(General.PROJECT_NAME, project.getProjectName());
                intent.putExtra(General.LOCATION_NAME, project.getLocationName());
                intent.putExtra(General.LOCATION_REF, project.getLocationRef());
                intent.putExtra(General.SUBLOCATION_NAME, project.getSubLocationName());
                intent.putExtra(General.SUBLOCATION_REF, project.getSublocationRef());
                SharedPreferences sharedPreferences = context.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isWaiting", project.getIsWaiting());
                editor.putString("isJoin", project.getIsJoin());
                editor.commit();
                context.startActivity(intent);
            }
        });

        if (holder.memberType.startsWith("Inhouse")) {
            holder.btn_remove.setVisibility(View.GONE);
        } else {
            holder.btn_remove.setVisibility(View.VISIBLE);
        }

        holder.btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbMasterRef = String.valueOf(project.getDbMasterRef());
                projectRef = project.getProjectRef();

                AlertDialog.Builder alertDialogBuilder =
                        new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Apakah anda yakin unjoin project "+project.getProjectName()+"?");
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        unSubscribe(holder.memberRef, dbMasterRef, projectRef);
                        projectModels.remove(position);
                        notifyDataSetChanged();

                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        holder.projectName.setText(project.getProjectName().toUpperCase());
        holder.locationName.setText(project.getLocationName());
        holder.subLocationName.setText(project.getSubLocationName());

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = holder.projectImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        holder.projectImg.setLayoutParams(params);
        holder.projectImg.requestLayout();

    }

    private void unSubscribe(final String memberRef, final String dbMasterRef, final String projectRef) {

        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.unSubscribeProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result detail project", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        //list.notify();
                        Intent intent = new Intent(context, MyListingActivity.class);
                        ((Activity) context).recreate();
                        //context.startActivity(intent);
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("dbMasterRef", dbMasterRef.toString());
                params.put("projectRef", projectRef.toString());

                Log.d("MyProject", memberRef + " " + dbMasterRef + " " + projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "project");

    }


    @Override
    public int getItemCount() {
        return projectModels.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}
