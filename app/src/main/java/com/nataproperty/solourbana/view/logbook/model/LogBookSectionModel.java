package com.nataproperty.solourbana.view.logbook.model;

/**
 * Created by Nata on 2/14/2017.
 */

public class LogBookSectionModel {
    String projectCustomerStatus, projectCustomerStatusName, countStatus, color;

    public String getProjectCustomerStatus() {
        return projectCustomerStatus;
    }

    public void setProjectCustomerStatus(String projectCustomerStatus) {
        this.projectCustomerStatus = projectCustomerStatus;
    }

    public String getProjectCustomerStatusName() {
        return projectCustomerStatusName;
    }

    public void setProjectCustomerStatusName(String projectCustomerStatusName) {
        this.projectCustomerStatusName = projectCustomerStatusName;
    }

    public String getCountStatus() {
        return countStatus;
    }

    public void setCountStatus(String countStatus) {
        this.countStatus = countStatus;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
