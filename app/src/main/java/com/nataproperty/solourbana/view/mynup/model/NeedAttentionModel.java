package com.nataproperty.solourbana.view.mynup.model;

/**
 * Created by UserModel on 6/8/2016.
 */
public class NeedAttentionModel {
    String dbMasterRef,projectRef,nupOrderCode,buyDate,paymentTypeName,qty,ppAmt,totalAmt,nupOrderDetailRef,nupOrderStatus,
            customerName,projectName,nupNo,nupOrderRef;

    public String getNupOrderRef() {
        return nupOrderRef;
    }

    public void setNupOrderRef(String nupOrderRef) {
        this.nupOrderRef = nupOrderRef;
    }

    public String getNupNo() {
        return nupNo;
    }

    public void setNupNo(String nupNo) {
        this.nupNo = nupNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getNupOrderCode() {
        return nupOrderCode;
    }

    public void setNupOrderCode(String nupOrderCode) {
        this.nupOrderCode = nupOrderCode;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(String buyDate) {
        this.buyDate = buyDate;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPpAmt() {
        return ppAmt;
    }

    public void setPpAmt(String ppAmt) {
        this.ppAmt = ppAmt;
    }

    public String getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(String totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getNupOrderDetailRef() {
        return nupOrderDetailRef;
    }

    public void setNupOrderDetailRef(String nupOrderDetailRef) {
        this.nupOrderDetailRef = nupOrderDetailRef;
    }

    public String getNupOrderStatus() {
        return nupOrderStatus;
    }

    public void setNupOrderStatus(String nupOrderStatus) {
        this.nupOrderStatus = nupOrderStatus;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
