package com.nataproperty.solourbana.view.project.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterProjectInteractor {
    void getLocation(String memberRef, String projectCode);
    void getListProject(String memberRef, String locationRef, String projectCode);
}
