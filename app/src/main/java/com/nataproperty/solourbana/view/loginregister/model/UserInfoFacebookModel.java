package com.nataproperty.solourbana.view.loginregister.model;

/**
 * Created by nata on 4/10/2017.
 */

public class UserInfoFacebookModel {
    public String name;

    public String email;

    public String facebookID;

    public String gender;

    public String urlPhotoProfile;
}
