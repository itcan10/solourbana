package com.nataproperty.solourbana.view.nup.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.FixedHoloDatePickerDialog;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by UserModel on 5/18/2016.
 */
public class NupCreateCustomerActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String NUP_AMT = "nupAmt";

    EditText editFullname, editBirthdate, editEmail, editMobil, editPassword, editRetypePassword;
    Button btnRegister;

    String memberCustomerRef;
    String customerName,birthDate;
    private String dbMasterRef, projectRef, projectDescription, nupAmt;

    ProgressDialog progressDialog;
    int status;
    String message;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_create_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Register");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);
        nupAmt = intent.getStringExtra(NUP_AMT);
        customerName = intent.getStringExtra("customerName");
        birthDate = intent.getStringExtra("birthDate");

        editFullname = (EditText) findViewById(R.id.txt_name);
        editBirthdate = (EditText) findViewById(R.id.edit_birthdate);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editMobil = (EditText) findViewById(R.id.txt_mobile);
        editPassword = (EditText) findViewById(R.id.txt_password);
        editRetypePassword = (EditText) findViewById(R.id.txt_retype_password);

        editBirthdate.setInputType(InputType.TYPE_NULL);
        editBirthdate.setTextIsSelectable(true);
        editBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPicker();
            }
        });
        editBirthdate.setFocusable(false);

        if (!customerName.isEmpty()) {
            editFullname.setText(customerName);;
        } else {
            editFullname.setEnabled(true);
        }

        if (!birthDate.isEmpty()) {
            editBirthdate.setText(birthDate);
        } else {
            editBirthdate.setEnabled(true);
        }

        btnRegister = (Button) findViewById(R.id.btn_register);
        btnRegister.setTypeface(font);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    onRegisterFailed();
                    return;
                }

                btnRegister.setEnabled(false);

                String fullname = editFullname.getText().toString().trim();
                String birthdate = editBirthdate.getText().toString().trim();
                String email = editEmail.getText().toString().trim();
                String mobile = editMobil.getText().toString().trim();
                String password = editPassword.getText().toString().trim();
                String retypePassword = editRetypePassword.getText().toString().trim();

                if ((!fullname.isEmpty() && !birthdate.isEmpty() && !email.isEmpty() && !password.isEmpty()
                        && !mobile.isEmpty() && !retypePassword.isEmpty() && password.equals(retypePassword)))
                    registerCustomer();

                else {
                    Toast.makeText(getApplicationContext(), "Please enter your details!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void dialogPicker() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final Context themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog);
        final DatePickerDialog dialog = new FixedHoloDatePickerDialog(
                themedContext,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dialog.show();
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editBirthdate.setText(sdf.format(myCalendar.getTime()));
    }


    public boolean validate() {
        boolean valid = true;

        String fullname = editFullname.getText().toString().trim();
        String birthdate = editBirthdate.getText().toString().trim();
        String email = editEmail.getText().toString().trim();
        String mobile = editMobil.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String retypePassword = editRetypePassword.getText().toString().trim();

        if (fullname.isEmpty()) {
            editFullname.setError("enter a fullname");
            valid = false;
        } else {
            editFullname.setError(null);
        }

        if (birthdate.isEmpty()) {
            editBirthdate.setError("enter a birtdate");
            valid = false;
        } else {
            editBirthdate.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editEmail.setError("enter a valid email address");
            valid = false;
        } else {
            editEmail.setError(null);
        }

        if (mobile.isEmpty()) {
            editMobil.setError("enter a mobile phone");
            valid = false;
        } else {
            editMobil.setError(null);
        }

        if (password.isEmpty()) {
            editPassword.setError("enter a password");
            valid = false;
        } else {
            editPassword.setError(null);
        }

        if (retypePassword.isEmpty()) {
            editRetypePassword.setError("enter a retype password");
            valid = false;
        } else if (!password.equals(retypePassword)) {
            editRetypePassword.setError("password don't match");
            valid = false;
        } else {
            editRetypePassword.setError(null);
        }

        return valid;
    }

    public void onRegisterFailed() {
        // Toast.makeText(getBaseContext(), "Register failed", Toast.LENGTH_LONG).show();

        btnRegister.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnRegister.setEnabled(true);
    }

    public void registerCustomer() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCostumerRegister(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result register", response);
                    status = jo.getInt("status");
                    message = jo.getString("message");

                    if (status == 200) {
                        Log.d("status", "" + message);
                        memberCustomerRef = jo.getString("customerMemberRef");

                        Intent intent = new Intent(NupCreateCustomerActivity.this, NupInformasiCustomerActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        SharedPreferences sharedPreferences = NupCreateCustomerActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isMemberCostumerRef", memberCustomerRef);
                        editor.commit();
                        intent.putExtra("status", "1");
                        startActivity(intent);
                        finish();

                    } else if (status == 202) {
                        btnRegister.setEnabled(true);
                        editEmail.setError(message);

                    } else {
                        String errorMsg = jo.getString("massage");
                        Log.d("status", "" + errorMsg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupCreateCustomerActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupCreateCustomerActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customerName", editFullname.getText().toString());
                params.put("birthDate", editBirthdate.getText().toString());
                params.put("email", editEmail.getText().toString());
                params.put("mobile", editMobil.getText().toString());
                params.put("password", editPassword.getText().toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "creatcustomer");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
