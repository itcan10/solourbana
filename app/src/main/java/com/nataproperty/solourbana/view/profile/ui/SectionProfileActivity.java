package com.nataproperty.solourbana.view.profile.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.profile.adapter.SelectorAdapter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 4/25/2016.
 */
public class SectionProfileActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String KEY_MEMBERREF = "memberRef";

    public static String[] listSectionMenu = {"Profile", "About Me", "Address", "Bank", "Others"};
    private ArrayList<String> listMenu = new ArrayList<>();

    private ListView listSection;
    private SharedPreferences sharedPreferences;

    String memberRef, memberType, name, memberTypeCode;

    String fullname, ktpid, birthPlace, mobile1, mobile2, email1, email2, birthdate, npwp, ktpRef, npwpRef;
    String idCountryCode, idProvinceCode, idCityCode, idAddr, idPostCode, proviceSortNo, citySortNo, countrySortNo,
            proviceSortNoCorres, citySortNoCorres, countrySortNoCorres, corresPostCode, corresAddr, corresCountryCode,
            corresProvinceCode, corresCityCode;
    String aboutMe, quotes;
    String bloodType, fax, passportID, simid;
    String nationSortNo, typeSortNo, regionSortNo, sexSortNo, occupationSortNo, jobTitleSortNo, maritalStatusSortNo;
    String bankRef, bankBranch, accName, accNo;

    static SectionProfileActivity sectionProfileActivity;

    private ImageView imgProfile;
    private TextView memberTypeName, nameProfile;

    private AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_profile));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberType = sharedPreferences.getString("isMemberType", null);
        name = sharedPreferences.getString("isName", null);
        memberTypeCode = sharedPreferences.getString("isMemberTypeCode", "");

        sectionProfileActivity = this;

        listMenu.add(0, "Profile");
        listMenu.add(1, "About Me");
        listMenu.add(2, "Address");
        listMenu.add(3, "Bank");
        listMenu.add(4, "Others");

        if (memberTypeCode.equals(General.customer)) {
            listMenu.add(5, "Change Personal Code");
        }

        //hide bank if agent property
        if (memberType.startsWith("Property")) {
            listMenu.remove(3);
        }

        listSection = (ListView) findViewById(R.id.list_section_profile);
        imgProfile = (ImageView) findViewById(R.id.profileImage);
        memberTypeName = (TextView) findViewById(R.id.memberTypeName);
        nameProfile = (TextView) findViewById(R.id.profilName);

        setProfile();

        //listSection.setAdapter(new SelectorAdapter(this, listSectionMenu));
        listSection.setAdapter(new SelectorAdapter(this, listMenu));
        listSection.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String listName = listMenu.get(position);

                if (listName.equals("Profile")) {
                    intentToEditProfile();
                } else if (listName.equals("About Me")) {
                    Intent intentAboutMe = new Intent(SectionProfileActivity.this, AboutMeActivity.class);
                    intentAboutMe.putExtra("memberRef", memberRef);
                    intentAboutMe.putExtra("aboutMe", aboutMe);
                    intentAboutMe.putExtra("quotes", quotes);
                    startActivity(intentAboutMe);
                } else if (listName.equals("Address")) {
                    Intent intentAddr = new Intent(SectionProfileActivity.this, AddressActivity.class);
                    intentAddr.putExtra("memberRef", memberRef);
                    intentAddr.putExtra("idCountryCode", idCountryCode);
                    intentAddr.putExtra("idProvinceCode", idProvinceCode);
                    intentAddr.putExtra("idCityCode", idCityCode);
                    intentAddr.putExtra("idPostCode", idPostCode);
                    intentAddr.putExtra("idAddr", idAddr);
                    intentAddr.putExtra("provinceSortNo", proviceSortNo);
                    intentAddr.putExtra("citySortNo", citySortNo);
                    intentAddr.putExtra("countrySortNo", countrySortNo);

                    intentAddr.putExtra("corresCountryCode", corresCountryCode);
                    intentAddr.putExtra("corresProvinceCode", corresProvinceCode);
                    intentAddr.putExtra("corresCityCode", corresCityCode);
                    intentAddr.putExtra("corresPostCode", corresPostCode);
                    intentAddr.putExtra("corresAddr", corresAddr);

                    intentAddr.putExtra("proviceSortNoCorres", proviceSortNoCorres);
                    intentAddr.putExtra("citySortNoCorres", citySortNoCorres);
                    intentAddr.putExtra("countrySortNoCorres", countrySortNoCorres);
                    intentAddr.putExtra("corresPostCode", corresPostCode);
                    intentAddr.putExtra("corresAddr", corresAddr);
                    startActivity(intentAddr);
                } else if (listName.equals("Bank")) {
                    Intent intentAccountBank = new Intent(SectionProfileActivity.this, AccountBankActivity.class);
                    intentAccountBank.putExtra("memberRef", memberRef);
                    intentAccountBank.putExtra("bankRef", bankRef);
                    intentAccountBank.putExtra("bankBranch", bankBranch);
                    intentAccountBank.putExtra("accName", accName);
                    intentAccountBank.putExtra("accNo", accNo);
                    startActivity(intentAccountBank);
                } else if (listName.equals("Others")) {
                    Intent intentOthers = new Intent(SectionProfileActivity.this, OthersEditActivity.class);
                    intentOthers.putExtra("memberRef", memberRef);
                    intentOthers.putExtra("bloodType", bloodType);
                    intentOthers.putExtra("fax", fax);
                    intentOthers.putExtra("passportID", passportID);
                    intentOthers.putExtra("simid", simid);

                    intentOthers.putExtra("nationSortNo", nationSortNo);
                    intentOthers.putExtra("typeSortNo", typeSortNo);
                    intentOthers.putExtra("regionSortNo", regionSortNo);
                    intentOthers.putExtra("sexSortNo", sexSortNo);
                    intentOthers.putExtra("occupationSortNo", occupationSortNo);
                    intentOthers.putExtra("jobTitleSortNo", jobTitleSortNo);
                    intentOthers.putExtra("maritalStatusSortNo", maritalStatusSortNo);
                    startActivity(intentOthers);
                } else if (listName.equals("Change Personal Code")){
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SectionProfileActivity.this, R.style.AlertDialogTheme);
                    LayoutInflater inflater = SectionProfileActivity.this.getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.dialog_insert_ps_code, null);
                    dialogBuilder.setView(dialogView);
                    final EditText psCode = dialogView.findViewById(R.id.edt_ps_code);

                    dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (!psCode.getText().toString().isEmpty()) {
                                syncPsCodeSvc(psCode.getText().toString());
                            } else {
                                psCode.setError("Tidak Boleh Kosong");
                            }
                        }
                    });
                    dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    });

                    alertDialog = dialogBuilder.create();
                    alertDialog.show();
                }
                else {

                }
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToEditProfile();
            }
        });

    }

    private void requestPsInfo() {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMemberInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                Log.d("cek", "response psInfo " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        //reuired
                        fullname = jo.getJSONObject("data").getString("name");
                        ktpid = jo.getJSONObject("data").getString("ktpid");
                        birthPlace = jo.getJSONObject("data").getString("birthPlace");
                        mobile1 = jo.getJSONObject("data").getString("hP1");
                        mobile2 = jo.getJSONObject("data").getString("hP2");
                        email1 = jo.getJSONObject("data").getString("email1");
                        email2 = jo.getJSONObject("data").getString("email2");
                        birthdate = jo.getJSONObject("data").getString("birthDate");
                        npwp = jo.getJSONObject("data").getString("npwp");
                        ktpRef = jo.getJSONObject("data").getString("ktpRef");
                        npwpRef = jo.getJSONObject("data").getString("npwpRef");

                        //address
                        idCountryCode = jo.getJSONObject("data").getString("idCountryCode");
                        idProvinceCode = jo.getJSONObject("data").getString("idProvinceCode");
                        idCityCode = jo.getJSONObject("data").getString("idCityCode");
                        idPostCode = jo.getJSONObject("data").getString("idPostCode");
                        idAddr = jo.getJSONObject("data").getString("idAddr");

                        corresCountryCode = jo.getJSONObject("data").getString("corresCountryCode");
                        corresProvinceCode = jo.getJSONObject("data").getString("corresProvinceCode");
                        corresCityCode = jo.getJSONObject("data").getString("corresCityCode");
                        corresAddr = jo.getJSONObject("data").getString("corresAddr");
                        corresPostCode = jo.getJSONObject("data").getString("corresPostCode");

                        proviceSortNo = jo.getJSONObject("data").getString("provinceSortNo");
                        citySortNo = jo.getJSONObject("data").getString("citySortNo");
                        countrySortNo = jo.getJSONObject("data").getString("countrySortNo");
                        proviceSortNoCorres = jo.getJSONObject("data").getString("corresProvinceSortNo");
                        citySortNoCorres = jo.getJSONObject("data").getString("corresCitySortNo");
                        countrySortNoCorres = jo.getJSONObject("data").getString("corresCountrySortNo");

                        //about me
                        aboutMe = jo.getJSONObject("data").getString("aboutMe");
                        quotes = jo.getJSONObject("data").getString("quotes");
                        //others
                        bloodType = jo.getJSONObject("data").getString("bloodType");
                        fax = jo.getJSONObject("data").getString("fax");
                        passportID = jo.getJSONObject("data").getString("passportID");
                        simid = jo.getJSONObject("data").getString("simid");
                        //otherSpinner
                        nationSortNo = jo.getJSONObject("data").getString("nationRef");
                        typeSortNo = jo.getJSONObject("data").getString("typeSortNo");
                        regionSortNo = jo.getJSONObject("data").getString("religionSortNo");
                        sexSortNo = jo.getJSONObject("data").getString("sexSortNo");
                        occupationSortNo = jo.getJSONObject("data").getString("occupationSortNo");
                        jobTitleSortNo = jo.getJSONObject("data").getString("jobSortNo");
                        maritalStatusSortNo = jo.getJSONObject("data").getString("maritalSortNo");

                        //accountBank
                        bankRef = jo.getJSONObject("data").getString("bankRef");
                        bankBranch = jo.getJSONObject("data").getString("bankBranch");
                        accName = jo.getJSONObject("data").getString("accName");
                        accNo = jo.getJSONObject("data").getString("accNo");

                        Log.d("LOG TAG PROFILE", ktpid + " " + birthPlace + " " + birthPlace + " " + mobile1 + " " + mobile2
                                + " " + email1 + " " + email2 + " " + birthdate);

                        Log.d("LOG TAG ADDRESS", idCountryCode + " " + idProvinceCode + " " + idCityCode + " " + idPostCode
                                + " " + idAddr + " " + proviceSortNo);

                        /*SharedPreferences sharedPreferences = SectionProfileActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isName", fullname);
                        editor.commit();*/

                        Log.d("Section profile", message);

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(getIntent());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(SectionProfileActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(SectionProfileActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_MEMBERREF, memberRef);
                Log.d("TAG Member Ref", memberRef);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "psInfo");
    }

    private void syncPsCodeSvc(final String psCode) {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.SyncPsCodeSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                Log.d("cek", "response psInfo " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Toast.makeText(SectionProfileActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(SectionProfileActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SectionProfileActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(SectionProfileActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(SectionProfileActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("projectCode", General.projectCode);
                params.put("memberRef", memberRef);
                params.put("psCode", psCode);
                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "psInfo");
    }

    @Override
    protected void onResume() {
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        name = sharedPreferences.getString("isName", null);
        super.onResume();
        requestPsInfo();
        setProfile();
    }

    private void setProfile() {
        Glide.with(SectionProfileActivity.this).load(WebService.getProfile() + memberRef)
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.drawable.profile_image)
                .placeholder(R.drawable.profile_image)
                .into(imgProfile);

        nameProfile.setText(name);
        memberTypeName.setText(memberType);
    }

    private void intentToEditProfile() {
        Intent intentEditProfile = new Intent(SectionProfileActivity.this, EditProfileActivity.class);
        intentEditProfile.putExtra("memberRef", memberRef);
        intentEditProfile.putExtra("fullname", fullname);
        intentEditProfile.putExtra("ktpid", ktpid);
        intentEditProfile.putExtra("birthPlace", birthPlace);
        intentEditProfile.putExtra("mobile1", mobile1);
        intentEditProfile.putExtra("mobile2", mobile2);
        intentEditProfile.putExtra("email1", email1);
        intentEditProfile.putExtra("email2", email2);
        intentEditProfile.putExtra("birthdate", birthdate);
        intentEditProfile.putExtra("npwp", npwp);
        intentEditProfile.putExtra("ktpRef", ktpRef);
        intentEditProfile.putExtra("npwpRef", npwpRef);
        startActivity(intentEditProfile);
    }

    public static SectionProfileActivity getInstance() {
        return sectionProfileActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(SectionProfileActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}


