package com.nataproperty.solourbana.view.ilustration.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.ilustration.model.ListBlockDiagramModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class FloorMappingAdapter extends BaseAdapter {
    private Context context;
    private List<ListBlockDiagramModel> list;
    private ListBlockMappingHolder holder;

    public FloorMappingAdapter(Context context, List<ListBlockDiagramModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_floor_mapping,null);
            holder = new ListBlockMappingHolder();
            holder.blockName = (TextView) convertView.findViewById(R.id.txt_block_name);

            convertView.setTag(holder);
        }else{
            holder = (ListBlockMappingHolder) convertView.getTag();
        }
        ListBlockDiagramModel block = list.get(position);
        holder.blockName.setText(block.getBlockName());

        return convertView;
    }

    private class ListBlockMappingHolder {
        TextView blockName;
    }
}
