package com.nataproperty.solourbana.view.logbook.model;

/**
 * Created by Nata on 2/9/2017.
 */

public class ListEvent {
    String eventRef, eventName;

    public String getEventRef() {
        return eventRef;
    }

    public void setEventRef(String eventRef) {
        this.eventRef = eventRef;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
