package com.nataproperty.solourbana.view.project.ui;

import android.Manifest;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.project.adapter.DownloadAdapter;
import com.nataproperty.solourbana.view.project.model.DownloadModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/7/2016.
 */
public class DownloadActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String IMAGE_LOGO = "imageLogo";
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final String TAG = "Download Activity";

    DownloadFileAsync dfa = null;
    ImageView imgLogo;
    TextView txtProjectName;

    private ArrayList<DownloadModel> listDownload = new ArrayList<DownloadModel>();
    private DownloadAdapter adapter;

    private ProgressDialog mProgressDialog;

    SharedPreferences sharedPreferences;
    long dbMasterRef;
    private String projectRef, projectName, memberRef, imageLogo;
    private String fileRef, filename, extension;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Download Material");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        projectRef = getIntent().getStringExtra(PROJECT_REF);
        dbMasterRef = getIntent().getLongExtra(DBMASTER_REF, 0);
        projectName = getIntent().getStringExtra(PROJECT_NAME);
        imageLogo = getIntent().getStringExtra(IMAGE_LOGO);

        BaseApplication.showLog(TAG,"cek "+dbMasterRef+" "+projectRef);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + String.valueOf(dbMasterRef) +
                        "&pr=" + projectRef)
                .skipMemoryCache(true)
                .into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        requestDownload();

        MyListView listView = (MyListView) findViewById(R.id.list_download);
        adapter = new DownloadAdapter(this, listDownload);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fileRef = listDownload.get(position).getFileRef();
                filename = listDownload.get(position).getFileName();
                extension = (listDownload.get(position).getExtension()).toLowerCase();

                if (checkWriteExternalPermission()) {
                    String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                            + filename.toString() + "." + extension.toLowerCase().toString();
                    File f = new File(path);
                    if (f.exists()) {
                        if (extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png")) {
                            try {
                                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                                        "/nataproperty/" + filename.toString() + "." + extension.toLowerCase().toString());
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), "image/*");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(DownloadActivity.this, "File tidak bisa dibuka", Toast.LENGTH_SHORT).show();
                            }

                        } else if (extension.equals("pdf")) {
                            try {
                                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                                        "/nataproperty/" + filename.toString() + "." + extension.toLowerCase().toString());
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(DownloadActivity.this, "File tidak bisa dibuka", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(DownloadActivity.this, "File tidak bisa dibuka", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        dialogStartDownload();
                    }
                } else {
                    showDialogCekPermission();
                }

            }
        });

    }

    public void showDialogCekPermission() {
        if (ContextCompat.checkSelfPermission(DownloadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DownloadActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
        } else {
            proceedAfterPermissionDeny();
        }
    }

    private void dialogStartDownload() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DownloadActivity.this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("Apakah anda ingin mengunduh file ini?");
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String url = WebService.URL_DOWNLOAD + "?is=fileDownload" +
                        "&dr=" + String.valueOf(dbMasterRef) +
                        "&pr=" + projectRef.toString() +
                        "&fr=" + fileRef.toString() +
                        "&fn=" + filename.toString().replace(" ", "%20") +
                        "&ex=." + extension.toString();
                Log.d("urld", url);
                startDownload(url);

            }
        });
        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void startDownload(String url) {
        Log.d("Cek", "" + checkWriteExternalPermission());
        if (!url.equals("")) {
//            new DownloadFileAsync().execute(url);
            dfa = new DownloadFileAsync();
            dfa.execute(url);   // Add
        } else {
            Toast.makeText(DownloadActivity.this, "No input url", Toast.LENGTH_LONG).show();
        }
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        BaseApplication.showLog(TAG, "check write external " + res);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dialogStartDownload();
            } else {
                proceedAfterPermissionDeny();
            }
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this,R.style.AlertDialogTheme);
                mProgressDialog.setMessage("Downloading file..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);

                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dfa.cancel(true);   // Add
                        String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                                +filename.toString() + ".pdf";
                        File f = new File(path);

                        String path2 = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                                +filename.toString() + ".jpg";
                        File f2 = new File(path2);

                        String path3 = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                                +filename.toString() + ".jpeg";
                        File f3 = new File(path3);

                        String path4 = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                                +filename.toString() + ".png";
                        File f4 = new File(path4);


                        try{
                            f.delete();
                            f2.delete();
                            f3.delete();
                            f4.delete();
                        }catch (Exception e) {

                        }
                        dialog.dismiss();
                    }
                });

                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }


    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            int count;

            try {

                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                File wallpaperDirectory = new File(android.os.Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(wallpaperDirectory + "/" + filename.toString() + "." + extension.toLowerCase().toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;

                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                    if(isCancelled()) break;// Add

                }

                if (isCancelled()){
                    dfa.cancel(true);
                }

                output.flush();
                output.close();
                input.close();
                if(!isCancelled()){
                    notification();// Add
                }
            } catch (Exception e) {
            }
            return null;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d("ANDRO_ASYNC", values[0]);
            mProgressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

    }

    public void notification() {
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        File file = new File("/" + Environment.getExternalStorageDirectory().getPath()
                + "/nataproperty/" + filename.toString() + "." + extension.toString());

        Log.d("FileUri", Environment.getExternalStorageDirectory().getPath()
                + "/" + filename.toString() + "." + extension.toString());
        if (extension.toLowerCase().equals("pdf")) {
            Log.d("ext", filename.toString() + "." + extension.toString());
            intent.setDataAndType(Uri.fromFile(file), "document/*");
        } else if (extension.toLowerCase().equals("jpg")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        } else if (extension.toLowerCase().equals("jpeg")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        } else if (extension.toLowerCase().equals("png")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        }
        // use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Notification noti = new Notification.Builder(this)
                .setContentTitle("Download " + filename + " Success")
                .setContentText("")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(largeIcon)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();

        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);
    }

    public void requestDownload() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListDownload(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result", response);
                    generateListDownload(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(DownloadActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(DownloadActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("memberRef", memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "category");

    }

    private void generateListDownload(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                DownloadModel download = new DownloadModel();
                download.setDbMasterRef(jo.getString("dbMasterRef"));
                download.setProjectRef(jo.getString("projectRef"));
                download.setFileName(jo.getString("fileName"));
                download.setFileRef(jo.getString("fileRef"));
                download.setExtension(jo.getString("extension"));

                listDownload.add(download);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(DownloadActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
