package com.nataproperty.solourbana.view.loginregister.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.loginregister.adapter.AgenTypeAdapter;
import com.nataproperty.solourbana.view.loginregister.adapter.AgencyCompanyAdapter;
import com.nataproperty.solourbana.view.loginregister.model.AgenTypeModel;
import com.nataproperty.solourbana.view.loginregister.model.CompanyModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/14/2016.
 */
public class RegisterPropertyAgencyActivity extends AppCompatActivity {
    public static final String TAG = "RegisterPropertyAgency";
    public static final String PREF_NAME = "pref";
    public static final String AGENTY_TYPE = "agentType";
    public static final String AGENTY_TYPE_NAME = "agentTypeName";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String MESSAGE = "message";

    private ArrayList<CompanyModel> listAgentcyCompany = new ArrayList<CompanyModel>(); //model
    private AgencyCompanyAdapter companyAdapter;
    private List<AgenTypeModel> listAgentType = new ArrayList<AgenTypeModel>(); //model
    private AgenTypeAdapter adapter; //set adapter

    private EditText txtCompanyName, txtPrincipleName, txtCompanyPhone, txtCompanyMobile,
            txtCompanyAddress, txtCompanyCode;
    private AutoCompleteTextView autoCompleteTextView;
    private Button btnValidate, btnFinish;
    private String fullname, birthDate, phone, email, password;
    private String agencyCompanyRef, companyName, principleName, companyPhone, companyMobile, companyAddress;
    private String getCompanyRef, getCompanyName, getPrincipleName, getCompanyPhone, getCompanyMobile, getCompanyAddress;
    private String agentType, agentTypeName, statusGoogleSignIn;

    private SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;
    String message;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_property_agency);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);

        txtCompanyCode = (EditText) findViewById(R.id.txt_company_code);
        txtPrincipleName = (EditText) findViewById(R.id.txtPrincipleName);
        txtCompanyPhone = (EditText) findViewById(R.id.txtCompanyPhone);
        txtCompanyMobile = (EditText) findViewById(R.id.txtCompanyMobile);
        txtCompanyAddress = (EditText) findViewById(R.id.txtCompanyAddress);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.auto_company_name);

        companyAdapter = new AgencyCompanyAdapter(RegisterPropertyAgencyActivity.this, listAgentcyCompany);
        autoCompleteTextView.setAdapter(companyAdapter);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setTypeface(font);

        btnValidate = (Button) findViewById(R.id.btn_validate);
        btnFinish = (Button) findViewById(R.id.btnFinishRegister);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        fullname = sharedPreferences.getString("isfullname", null);
        birthDate = sharedPreferences.getString("isbirthDate", null);
        phone = sharedPreferences.getString("isphone", null);
        email = sharedPreferences.getString("isemail", null);
        password = sharedPreferences.getString("ispassword", null);
        agentType = sharedPreferences.getString("isagentType", null);
        agentTypeName = sharedPreferences.getString("isagentTypeName", null);
        statusGoogleSignIn = sharedPreferences.getString("statusGoogleSignIn", null);

        title.setText(String.valueOf(agentTypeName));

        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        companyName = intent.getStringExtra("companyName");
        principleName = intent.getStringExtra("principleName");
        companyPhone = intent.getStringExtra("companyPhone");
        companyMobile = intent.getStringExtra("companyMobile");
        companyAddress = intent.getStringExtra("companyAddress");

        Log.d("agencyCompanyRef",""+agencyCompanyRef);

        Log.d("cekProperty", " " + fullname + " " + birthDate + " " + phone + " " +
                email + " " + password + " " + agentType + " " + agentTypeName + " " + agencyCompanyRef + " " +
                companyName + " " + principleName + " " + companyPhone + " " + companyMobile + " " + companyAddress);

        /*adapter*/
        if (agencyCompanyRef != null) {
            autoCompleteTextView.setVisibility(View.VISIBLE);
            txtPrincipleName.setVisibility(View.VISIBLE);
            txtCompanyPhone.setVisibility(View.VISIBLE);
            txtCompanyMobile.setVisibility(View.VISIBLE);
            txtCompanyAddress.setVisibility(View.VISIBLE);
            btnFinish.setVisibility(View.VISIBLE);

            autoCompleteTextView.setText(companyName);
            txtPrincipleName.setText(principleName);
            txtCompanyPhone.setText(companyPhone);
            txtCompanyMobile.setText(companyMobile);
            txtCompanyAddress.setText(companyAddress);

            if (principleName.isEmpty()) {
                txtPrincipleName.setFocusableInTouchMode(true);
                txtPrincipleName.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtPrincipleName, InputMethodManager.SHOW_IMPLICIT);
            } else if (companyPhone.isEmpty()) {
                txtCompanyPhone.setFocusableInTouchMode(true);
                txtCompanyPhone.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtCompanyPhone, InputMethodManager.SHOW_IMPLICIT);
            } else if (companyMobile.isEmpty()) {
                txtCompanyMobile.setFocusableInTouchMode(true);
                txtCompanyMobile.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtCompanyMobile, InputMethodManager.SHOW_IMPLICIT);
            } else if (companyAddress.isEmpty()) {
                txtCompanyAddress.setFocusableInTouchMode(true);
                txtCompanyAddress.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtCompanyAddress, InputMethodManager.SHOW_IMPLICIT);
            } else {
                autoCompleteTextView.setFocusableInTouchMode(true);
                autoCompleteTextView.requestFocus();
            }

            companyAdapter = new AgencyCompanyAdapter(RegisterPropertyAgencyActivity.this, listAgentcyCompany);
            autoCompleteTextView.setAdapter(companyAdapter);
            autoCompleteTextView.setThreshold(1);
            autoCompleteTextView.setTypeface(font);

            requestAgencyCompany();

        }

        btnValidate.setTypeface(font);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekRequestCompanyCode = txtCompanyCode.getText().toString().trim();
                if (cekRequestCompanyCode.equals("")) {
                    agencyCompanyRef = "0";

                    requestAgencyCompany();
                    companyAdapter = new AgencyCompanyAdapter(RegisterPropertyAgencyActivity.this, listAgentcyCompany);
                    autoCompleteTextView.setAdapter(companyAdapter);
                    autoCompleteTextView.setThreshold(1);

                    autoCompleteTextView.setText("");
                    txtPrincipleName.setText("");
                    txtCompanyPhone.setText("");
                    txtCompanyMobile.setText("");
                    txtCompanyAddress.setText("");

                    autoCompleteTextView.setEnabled(true);
                    txtPrincipleName.setEnabled(true);
                    txtCompanyPhone.setEnabled(true);
                    txtCompanyMobile.setEnabled(true);
                    txtCompanyAddress.setEnabled(true);
                } else {
                    requestCompanyCode();
                }

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        });

        btnFinish.setTypeface(font);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String companyName = autoCompleteTextView.getText().toString();

                if (!companyName.isEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterPropertyAgencyActivity.this);
                    alertDialogBuilder.setMessage("Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
                    alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            registerFinish();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialogBuilder.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(RegisterPropertyAgencyActivity.this,RegisterTermActivity.class));
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    autoCompleteTextView.setError(null);
                } else {
                    autoCompleteTextView.requestFocus();
                    autoCompleteTextView.setError("Harus diisi");
                }
            }
        });


    }

    private void requestCompanyCode() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCompanyCode(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d(TAG, "response login " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d(TAG, response);
                    int status = jo.getInt("status");

                    if (status == 200) {
                        String message = jo.getString("message");
                        Toast.makeText(RegisterPropertyAgencyActivity.this, message, Toast.LENGTH_LONG).show();

                        agencyCompanyRef = jo.getJSONObject("data").getString("agencyCompanyRef");
                        getCompanyName = jo.getJSONObject("data").getString("companyName");
                        getPrincipleName = jo.getJSONObject("data").getString("principleName");
                        getCompanyPhone = jo.getJSONObject("data").getString("companyPhone");
                        getCompanyMobile = jo.getJSONObject("data").getString("companyHP");
                        getCompanyAddress = jo.getJSONObject("data").getString("address");

                        Log.d("Cek Validate", getCompanyName + " " + getPrincipleName + " " + getCompanyPhone + " "
                                + getCompanyMobile + " " + getCompanyAddress);

                        autoCompleteTextView.setText(getCompanyName);
                        txtPrincipleName.setText(getPrincipleName);
                        txtCompanyPhone.setText(getCompanyPhone);
                        txtCompanyMobile.setText(getCompanyMobile);
                        txtCompanyAddress.setText(getCompanyAddress);

                        autoCompleteTextView.setVisibility(View.VISIBLE);
                        txtPrincipleName.setVisibility(View.VISIBLE);
                        txtCompanyPhone.setVisibility(View.VISIBLE);
                        txtCompanyMobile.setVisibility(View.VISIBLE);
                        txtCompanyAddress.setVisibility(View.VISIBLE);
                        btnFinish.setVisibility(View.VISIBLE);

                        autoCompleteTextView.setEnabled(false);
                        txtPrincipleName.setEnabled(false);
                        txtCompanyPhone.setEnabled(false);
                        txtCompanyMobile.setEnabled(false);
                        txtCompanyAddress.setEnabled(false);

                    } else {
                        agencyCompanyRef = "0";

                        autoCompleteTextView.setText("");
                        txtPrincipleName.setText("");
                        txtCompanyPhone.setText("");
                        txtCompanyMobile.setText("");
                        txtCompanyAddress.setText("");

                        autoCompleteTextView.setEnabled(true);
                        txtPrincipleName.setEnabled(true);
                        txtCompanyPhone.setEnabled(true);
                        txtCompanyMobile.setEnabled(true);
                        txtCompanyAddress.setEnabled(true);

                        autoCompleteTextView.setVisibility(View.VISIBLE);
                        btnFinish.setVisibility(View.VISIBLE);
                        txtPrincipleName.setVisibility(View.VISIBLE);
                        txtCompanyPhone.setVisibility(View.VISIBLE);
                        txtCompanyMobile.setVisibility(View.VISIBLE);
                        txtCompanyAddress.setVisibility(View.VISIBLE);

                        autoCompleteTextView.setFocusableInTouchMode(true);
                        autoCompleteTextView.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(autoCompleteTextView, InputMethodManager.SHOW_IMPLICIT);

                        requestAgencyCompany();
                        companyAdapter = new AgencyCompanyAdapter(RegisterPropertyAgencyActivity.this, listAgentcyCompany);
                        autoCompleteTextView.setAdapter(companyAdapter);
                        autoCompleteTextView.setThreshold(1);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterPropertyAgencyActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterPropertyAgencyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("companyCode", txtCompanyCode.getText().toString().trim());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "registerAgency");
    }

    private void requestAgencyCompany() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.GET,
                WebService.getAgencyCompany(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d(TAG, "response login " + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result agency Company", response);
                    generateListAgencyCompany(jsonArray);

                    autoCompleteTextView.setVisibility(View.VISIBLE);
                    txtPrincipleName.setVisibility(View.VISIBLE);
                    txtCompanyPhone.setVisibility(View.VISIBLE);
                    txtCompanyMobile.setVisibility(View.VISIBLE);
                    txtCompanyAddress.setVisibility(View.VISIBLE);
                    btnFinish.setVisibility(View.VISIBLE);

                    autoCompleteTextView.setFocusableInTouchMode(true);
                    autoCompleteTextView.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(autoCompleteTextView, InputMethodManager.SHOW_IMPLICIT);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterPropertyAgencyActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterPropertyAgencyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                });

        BaseApplication.getInstance().addToRequestQueue(request, "registerAgency");
    }

    private void generateListAgencyCompany(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CompanyModel agencyCompany = new CompanyModel();
                agencyCompany.setAgencyCompanyRef(jo.getString("agencyCompanyRef"));
                agencyCompany.setCompanyName(jo.getString("companyName"));
                agencyCompany.setPrincipleName(jo.getString("principleName"));
                agencyCompany.setCompanyHP(jo.getString("companyHP"));
                agencyCompany.setCompanyPhone(jo.getString("companyPhone"));
                agencyCompany.setAddress(jo.getString("address"));

                Log.d("agencyCompanyRef",""+jo.getString("agencyCompanyRef"));

                listAgentcyCompany.add(agencyCompany);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        companyAdapter.notifyDataSetChanged();
    }

    /**
     * finish register
     */
    private void registerFinish() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getRegisterAgency(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d(TAG, "response login " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result ", response);
                    int status = jo.getInt("status");

                    if (status == 200) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        SharedPreferences sharedPreferences = RegisterPropertyAgencyActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isfullname", "");
                        editor.putString("isbirthDate", "");
                        editor.putString("isphone", "");
                        editor.putString("isemail", "");
                        editor.putString("ispassword", "");
                        editor.putString("isagentType", "");
                        editor.putString("isagentTypeName", "");
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();

                        // Launch finish activity
                        Intent intent = new Intent(RegisterPropertyAgencyActivity.this, RegisterFinishActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else if (status == 201) {
                        message = jo.getString("message");
                        Intent intent = new Intent(RegisterPropertyAgencyActivity.this, RegisterStepOneActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.putExtra(MOBILE, phone);
                        intent.putExtra(MESSAGE, message);
                        startActivity(intent);
                        finish();

                    } else if (status == 500) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        SharedPreferences sharedPreferences = RegisterPropertyAgencyActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();

                        Intent intent = new Intent(RegisterPropertyAgencyActivity.this, LaunchActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterPropertyAgencyActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterPropertyAgencyActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fullname", fullname);
                params.put("username", email);
                params.put("mobile", phone);
                params.put("agentType", agentType);
                params.put("password", password);
                params.put("birthDate", birthDate);
                params.put("companyName", autoCompleteTextView.getText().toString().trim());
                params.put("principleName", txtPrincipleName.getText().toString());
                params.put("companyPhone",  txtCompanyPhone.getText().toString());
                params.put("companyMobile", txtCompanyMobile.getText().toString());
                params.put("companyAddress", txtCompanyAddress.getText().toString());
                params.put("agencyCompanyRef", agencyCompanyRef.toString());
                params.put("status", statusGoogleSignIn);
                params.put("projectCode", General.projectCode + "#" + General.projectCodeSingleProject);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "registerAgency");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
