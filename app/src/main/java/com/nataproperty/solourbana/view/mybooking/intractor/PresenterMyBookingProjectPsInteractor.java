package com.nataproperty.solourbana.view.mybooking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyBookingProjectPsInteractor {
    void getListBookingProjectPs(String dbMasterRef,String projectRef,String projectPsRef);
}
