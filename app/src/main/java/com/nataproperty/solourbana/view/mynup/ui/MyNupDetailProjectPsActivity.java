package com.nataproperty.solourbana.view.mynup.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mynup.model.MyNupDetailProjectPsModel;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.mynup.presenter.MyNupDetailProjectPsPresenter;

import retrofit2.Response;

public class MyNupDetailProjectPsActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    private static final String TAG = "MyNupProjectPs";

    Toolbar toolbar;
    TextView title;
    ListView listView;
    SharedPreferences sharedPreferences;
    String memberRef, projectRef, dbMasterRef, projectPsRef, nupRef, ppNup, ppName;
    String nupOrderCode, qty, ppAmt, totalAmt, nupOrderDetailRef, nupOrderStatus,
            customerName, projectName, status, nupOrderRef, nupPsCode, nupNo, nupName, amount,
            nupStatusName, paymentTypeName, bankName, nupidCountryName, nupidProvinceName, nupidCityName,
            nupidAddr, projectSchemeRef, buyDate, clusterName, productName, detailName, blockName, unitName, nuphP1,
            nupphone1, nupemail, nupktpid, custBankAccName, custBankAccNo, custBankBranch;
    ProgressDialog progressDialog;

    MyNupDetailProjectPsPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    TextView txtNupCode, txtByDate, txtPaymentType, txtPpQty, txtPpAmt, txtTotalAmt, txtAddress,
            txtNupStatus, txtBankName, txtProjectName, txtPpNup, txtPpName, txtTotal, txtBuyDate,
            txtPaymentTypeName, txtCustBankAccName, txtCustBankAccNo, txtCustBankBranch, txtPropertyName,
            txtCtegoryType, txtProduct, txtUnitNo, txtFullname, txtMobile, txtPhone, txtEmail, txtKtpId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_nup_detail_project_ps);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyNupDetailProjectPsPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        projectPsRef = intent.getStringExtra("projectPsRef");
        nupRef = intent.getStringExtra("nupRef");
        projectName = intent.getStringExtra("projectName");
        ppNup = intent.getStringExtra("ppNup");
        ppName = intent.getStringExtra("ppName");
        ppNup = intent.getStringExtra("ppNup");
        totalAmt = intent.getStringExtra("totalAmt");
        projectSchemeRef = intent.getStringExtra("projectSchemeRef");

        requestDetailProjectPs();
    }

    private void requestDetailProjectPs() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListNupDetailProjectPs(dbMasterRef, projectRef, projectPsRef, nupRef, memberRef, projectSchemeRef);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.txt_information_nup));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtNupCode = (TextView) findViewById(R.id.txt_nup_no);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtBuyDate = (TextView) findViewById(R.id.txt_buy_date);
        txtPpName = (TextView) findViewById(R.id.txt_pp_name);
        txtNupStatus = (TextView) findViewById(R.id.txt_nup_status_name);
        txtTotal = (TextView) findViewById(R.id.txt_total_amt);

        txtPaymentTypeName = (TextView) findViewById(R.id.txt_paymentTypeName);
        txtBankName = (TextView) findViewById(R.id.txt_bankName);
        txtCustBankAccName = (TextView) findViewById(R.id.txt_custBankAccName);
        txtCustBankAccNo = (TextView) findViewById(R.id.txt_custBankAccNo);
        txtCustBankBranch = (TextView) findViewById(R.id.txt_custBankBranch);

        txtCtegoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);

        txtFullname = (TextView) findViewById(R.id.txt_fullname);
        txtMobile = (TextView) findViewById(R.id.txt_mobile);
        txtPhone = (TextView) findViewById(R.id.txt_phone);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtKtpId = (TextView) findViewById(R.id.txt_ktp_id);

    }

    public void showListNupProjectPsResults(Response<MyNupDetailProjectPsModel> response) {
        progressDialog.dismiss();
        nupPsCode = response.body().getNupPsCode();
        qty = response.body().getQty();
        nupNo = response.body().getNupNo();
        nupName = response.body().getNupName();
        amount = response.body().getAmount();
        nupStatusName = response.body().getNupStatusName();
        paymentTypeName = response.body().getPaymentTypeName();
        bankName = response.body().getBankName();
        nupidCountryName = response.body().getNupidCountryName();
        nupidProvinceName = response.body().getNupidProvinceName();
        nupidCityName = response.body().getNupidCityName();
        nupidAddr = response.body().getNupidAddr();
        buyDate = response.body().getBuyDate();
        clusterName = response.body().getClusterName();
        productName = response.body().getProductName();
        detailName = response.body().getDetailName();
        blockName = response.body().getBlockName();
        unitName = response.body().getUnitName();
        nuphP1 = response.body().getNuphP1();
        nupphone1 = response.body().getNupphone1();
        nupemail = response.body().getNupemail();
        nupktpid = response.body().getNupktpid();
        custBankAccName = response.body().getCustBankAccName();
        custBankAccNo = response.body().getCustBankAccNo();
        custBankBranch = response.body().getCustBankBranch();

        txtNupCode.setText(nupNo);
        txtProjectName.setText(projectName);
        txtBuyDate.setText(buyDate);
        txtPpName.setText(ppName);
        txtNupStatus.setText(nupStatusName);
        txtTotal.setText(totalAmt);

        if (paymentTypeName.equals("")) {
            txtPaymentTypeName.setText("-");
        } else {
            txtPaymentTypeName.setText(paymentTypeName);
        }

        if (bankName.equals("")) {
            txtBankName.setText("-");
        } else {
            txtBankName.setText(bankName);
        }

        if (custBankAccName.equals("")) {
            txtCustBankAccName.setText("-");
        } else {
            txtCustBankAccName.setText(custBankAccName);
        }

        if (custBankAccNo.equals("")) {
            txtCustBankAccNo.setText("-");
        } else {
            txtCustBankAccNo.setText(custBankAccNo);
        }

        if (custBankBranch.equals("")) {
            txtCustBankBranch.setText("-");
        } else {
            txtCustBankBranch.setText(custBankBranch);
        }

        if (clusterName.equals("")) {
            txtCtegoryType.setText("-");
        } else {
            txtCtegoryType.setText(clusterName);
        }

        if (productName.equals("")) {
            txtProduct.setText("-");
        } else {
            txtProduct.setText(productName);
        }

        if (unitName.equals("")) {
            txtUnitNo.setText("-");
        } else {
            txtUnitNo.setText(unitName);
        }

        txtFullname.setText(ppName);
        if (nuphP1.equals("")) {
            txtMobile.setText("-");
        } else {
            txtMobile.setText(nuphP1);
        }
        if (nupphone1.equals("")) {
            txtPhone.setText("-");
        } else {
            txtPhone.setText(nupphone1);
        }
        if (nupemail.equals("")) {
            txtEmail.setText("-");
        } else {
            txtEmail.setText(nupemail);
        }
        if (nupidAddr.equals("")) {
            txtAddress.setText("-");
        } else {
            txtAddress.setText(nupidAddr);
        }
        if (nupktpid.equals("")) {
            txtKtpId.setText("-");
        } else {
            txtKtpId.setText(nupktpid);
        }

    }

    public void showListNupProjectPsFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(MyNupDetailProjectPsActivity.this, getString(R.string.error_connection), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
