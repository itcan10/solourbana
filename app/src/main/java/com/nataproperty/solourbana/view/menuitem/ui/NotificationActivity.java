package com.nataproperty.solourbana.view.menuitem.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.menuitem.adapter.NotificationAdapter;
import com.nataproperty.solourbana.view.menuitem.model.NotificationModel;
import com.nataproperty.solourbana.view.menuitem.presenter.NotificationPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;

import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {
    public static final String TAG = "NotificationActivity";

    public static final String PREF_NAME = "pref";

    public static final String TITLE = "title";
    public static final String DATE = "date";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String GCM_MESSAGE_REF = "gcmMessageRef";
    public static final String GCM_MESSAGE_TYPE_REF = "gcmMessageTypeRef";
    private static final String EXTRA_RX = "EXTRA_RX";

    SharedPreferences sharedPreferences;

    ListView listView;

    private ArrayList<NotificationModel> listNotification = new ArrayList<NotificationModel>();
    private NotificationAdapter adapter;

    String memberRef, linkDetail, titleGcm, date, gcmMessageRef, gcmMessageTypeRef;

    LinearLayout linearNoData, linearLayout;
    Toolbar toolbar;
    TextView title;
    Typeface font;

    NotificationPresenter presenter;
    ServiceRetrofitProject service;
    boolean rxCallInWorks = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new NotificationPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        requestListNotification();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                linkDetail = listNotification.get(position).getLinkDetail();
                titleGcm = listNotification.get(position).getGcmTitle();
                date = listNotification.get(position).getInputTime();
                gcmMessageRef = listNotification.get(position).getGcmMessageRef();
                gcmMessageTypeRef = listNotification.get(position).getGcmMessageTypeRef();

                Intent intent = new Intent(NotificationActivity.this, NotificationDetailActivity.class);
                intent.putExtra(TITLE, titleGcm);
                intent.putExtra(DATE, date);
                intent.putExtra(LINK_DETAIL, linkDetail);
                intent.putExtra(GCM_MESSAGE_REF, gcmMessageRef);
                intent.putExtra(GCM_MESSAGE_TYPE_REF, gcmMessageTypeRef);
                startActivity(intent);
            }
        });

    }

    public void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_notification));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        listView = (ListView) findViewById(R.id.list_notification);
    }

    private void requestListNotification() {
        presenter.getListNotificationSSTSvc(memberRef, General.projectCode);
    }

    public void showListNotificationResults(Response<ArrayList<NotificationModel>> response) {
        listNotification = response.body();

        if (listNotification.size() != 0) {
            initAdapter();
            linearNoData.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            linearNoData.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
    }

    public void showListNotificationFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapter() {
        adapter = new NotificationAdapter(this, listNotification);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NotificationActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
