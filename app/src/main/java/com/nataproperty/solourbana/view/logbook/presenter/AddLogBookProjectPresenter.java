package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.CountryModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;
import com.nataproperty.solourbana.view.logbook.intractor.AddLogBookProjectInteractor;
import com.nataproperty.solourbana.view.logbook.model.ContactProjectModel;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.logbook.ui.AddLogBookProjectActivity;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class AddLogBookProjectPresenter implements AddLogBookProjectInteractor {
    private AddLogBookProjectActivity view;
    private ServiceRetrofitProject service;

    public AddLogBookProjectPresenter(AddLogBookProjectActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetLookupContactProjectSvc(String dbMasterRef, String projectRef, String memberRef) {
        Call<ContactProjectModel> call = service.getAPI().GetLookupContactProjectSvc(dbMasterRef,projectRef,memberRef);
        call.enqueue(new Callback<ContactProjectModel>() {
            @Override
            public void onResponse(Call<ContactProjectModel> call, Response<ContactProjectModel> response) {
                view.showListContactProjectResults(response);
            }

            @Override
            public void onFailure(Call<ContactProjectModel> call, Throwable t) {
                view.showListContactProjectFailure(t);

            }


        });
    }

    @Override
    public void getCountryListSvc() {
        Call<ArrayList<CountryModel>> call = service.getAPI().getCountryListSvc();
        call.enqueue(new Callback<ArrayList<CountryModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CountryModel>> call, Response<ArrayList<CountryModel>> response) {
                view.showListCountryResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<CountryModel>> call, Throwable t) {
                view.showListCountryFailure(t);

            }


        });
    }

    @Override
    public void getProvinceListSvc(String countryCode) {
        Call<ArrayList<ProvinceModel>> call = service.getAPI().getProvinceListSvc(countryCode);
        call.enqueue(new Callback<ArrayList<ProvinceModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ProvinceModel>> call, Response<ArrayList<ProvinceModel>> response) {
                view.showListProvinceResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<ProvinceModel>> call, Throwable t) {
                view.showListProvinceFailure(t);

            }


        });
    }

    @Override
    public void getCityListSvc(String countryCode, String provinceCode) {
        Call<ArrayList<CityModel>> call = service.getAPI().getCityListSvc(countryCode,provinceCode);
        call.enqueue(new Callback<ArrayList<CityModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CityModel>> call, Response<ArrayList<CityModel>> response) {
                view.showListCityResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<CityModel>> call, Throwable t) {
                view.showListCityFailure(t);

            }


        });
    }

    @Override
    public void insertContactBankProjectSvc(Map<String, String> fields) {
        Call<ResponeModel> call = service.getAPI().insertContactBankProjectSvc(fields);
        call.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, Response<ResponeModel> response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.showResponeFailure(t);
            }
        });
    }
}
