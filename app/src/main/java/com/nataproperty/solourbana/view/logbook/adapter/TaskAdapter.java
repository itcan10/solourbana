package com.nataproperty.solourbana.view.logbook.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.view.logbook.model.TaskModel;
import com.nataproperty.solourbana.view.logbook.model.TaskSubModel;
import com.nataproperty.solourbana.view.logbook.ui.AddTaskActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nata on 2/17/2017.
 */

public class TaskAdapter extends BaseExpandableListAdapter {
    private Context context;
    private ArrayList<TaskModel> deptList;
    private TaskAdapterHolder holder;
    private TaskAdapterHolder holderSub;
    private String dbMasterRef, projectRef, customerRef, projectCustomerStatus = "", name;

    public TaskAdapter(Context context, ArrayList<TaskModel> deptList, String dbMasterRef, String projectRef, String customerRef,
                       String projectCustomerStatus, String name) {
        this.context = context;
        this.deptList = deptList;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        this.customerRef = customerRef;
        this.projectCustomerStatus = projectCustomerStatus;
        this.name = name;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<TaskSubModel> productList = deptList.get(groupPosition).getChildTask();
        return productList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        final TaskSubModel taskSubModel = (TaskSubModel) getChild(groupPosition, childPosition);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.item_list_task_sub, null);
            holderSub = new TaskAdapterHolder();
            holderSub.customerProjectActionName = (TextView) view.findViewById(R.id.customerProjectActionName);
            holderSub.projectCustomerStatusName = (TextView) view.findViewById(R.id.projectCustomerStatusName);
            holderSub.scheduleDate = (TextView) view.findViewById(R.id.scheduleDate);
            holderSub.finishDate = (TextView) view.findViewById(R.id.finishDate);
            holderSub.descriptionTask = (TextView) view.findViewById(R.id.descriptionTask);
            holderSub.descriptionResult = (TextView) view.findViewById(R.id.descriptionResult);
            holderSub.addTask = (TextView) view.findViewById(R.id.add_task);
            holderSub.editTask = (TextView) view.findViewById(R.id.edit_task);
            holderSub.number = (TextView) view.findViewById(R.id.number);
            holderSub.result = (TextView) view.findViewById(R.id.result);
            holderSub.iconResponse = (ImageView) view.findViewById(R.id.icon_response);
            view.setTag(holderSub);
        } else {
            holderSub = (TaskAdapterHolder) view.getTag();
        }

        holderSub.customerProjectActionName.setText(taskSubModel.getCustomerProjectActionName());
        holderSub.projectCustomerStatusName.setText(taskSubModel.getProjectCustomerStatusName());
        holderSub.scheduleDate.setText(taskSubModel.getScheduleDate());
        holderSub.finishDate.setText(taskSubModel.getFinishDate());
        holderSub.descriptionTask.setText(taskSubModel.getDescriptionTask());
        holderSub.descriptionResult.setText(taskSubModel.getDescriptionResult());
        holderSub.number.setText(taskSubModel.getNumber());

        if (projectCustomerStatus.equals(General.projectCustomerStatusFinish)) {
            holderSub.addTask.setVisibility(View.GONE);
            holderSub.result.setVisibility(View.GONE);
            holderSub.editTask.setVisibility(View.GONE);
        } else {
            if (taskSubModel.getFinishDate().equals("-")) {
                holderSub.addTask.setVisibility(View.GONE);
                holderSub.result.setVisibility(View.VISIBLE);
                holderSub.editTask.setVisibility(View.VISIBLE);
            } else {
                holderSub.addTask.setVisibility(View.VISIBLE);
                holderSub.result.setVisibility(View.GONE);
                holderSub.editTask.setVisibility(View.GONE);
            }
        }

        holderSub.addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddTaskActivity.class);
                i.putExtra("dbMasterRef", dbMasterRef);
                i.putExtra("projectRef", projectRef);
                i.putExtra("customerRef", customerRef);
                i.putExtra("projectCustomerStatus", projectCustomerStatus);
                i.putExtra("name", name);
                if (taskSubModel.getLogProjectCustomerStatusTaskRef() != null) {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", taskSubModel.getLogProjectCustomerStatusTaskRef());
                } else {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", "0");
                }
                i.putExtra("logProjectCustomerStatusTaskRef", "");
                context.startActivity(i);
            }
        });

        holderSub.editTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddTaskActivity.class);
                i.putExtra("dbMasterRef", dbMasterRef);
                i.putExtra("projectRef", projectRef);
                i.putExtra("customerRef", customerRef);
                i.putExtra("projectCustomerStatus", projectCustomerStatus);
                i.putExtra("name", name);
                if (taskSubModel.getLogProjectCustomerStatusTaskRef() != null) {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", taskSubModel.getLogProjectCustomerStatusTaskRef());
                } else {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", "0");
                }
                i.putExtra("logProjectCustomerStatusTaskRef", taskSubModel.getLogProjectCustomerStatusTaskRef());
                i.putExtra("customerProjectAction", taskSubModel.getCustomerProjectAction());
                i.putExtra("scheduleDate", taskSubModel.getScheduleDateValue());
                i.putExtra("descriptionTask", taskSubModel.getDescriptionTask());
                context.startActivity(i);
            }
        });
        holderSub.result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddTaskActivity.class);
                i.putExtra("isResult", true);
                i.putExtra("dbMasterRef", dbMasterRef);
                i.putExtra("projectRef", projectRef);
                i.putExtra("customerRef", customerRef);
                i.putExtra("projectCustomerStatus", projectCustomerStatus);
                i.putExtra("name", name);
                if (taskSubModel.getLogProjectCustomerStatusTaskRef() != null) {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", taskSubModel.getLogProjectCustomerStatusTaskRef());
                } else {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", "0");
                }
                i.putExtra("logProjectCustomerStatusTaskRef", taskSubModel.getLogProjectCustomerStatusTaskRef());
                i.putExtra("customerProjectAction", taskSubModel.getCustomerProjectAction());
                i.putExtra("scheduleDate", taskSubModel.getScheduleDateValue());
                i.putExtra("descriptionTask", taskSubModel.getDescriptionTask());
                i.putExtra("newToProgress", taskSubModel.getNewToProgress());
                i.putExtra("progressToInterest", taskSubModel.getProgressToInterest());
                i.putExtra("progressToNotInterest", taskSubModel.getProgressToNotInterest());
                i.putExtra("interestToFinish", taskSubModel.getInterestToFinish());
                i.putExtra("notInterestToFinish", taskSubModel.getNotInterestToFinish());
                context.startActivity(i);
            }
        });
        Glide.with(context).load(taskSubModel.getIconResponse()).into(holderSub.iconResponse);
        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<TaskSubModel> productList = deptList.get(groupPosition).getChildTask();
        return productList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return deptList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return deptList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        final TaskModel taskModel = (TaskModel) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.item_list_task, null);
            holder = new TaskAdapterHolder();
            holder.customerProjectActionName = (TextView) view.findViewById(R.id.customerProjectActionName);
            holder.projectCustomerStatusName = (TextView) view.findViewById(R.id.projectCustomerStatusName);
            holder.scheduleDate = (TextView) view.findViewById(R.id.scheduleDate);
            holder.finishDate = (TextView) view.findViewById(R.id.finishDate);
            holder.descriptionTask = (TextView) view.findViewById(R.id.descriptionTask);
            holder.descriptionResult = (TextView) view.findViewById(R.id.descriptionResult);
            holder.addTask = (TextView) view.findViewById(R.id.add_task);
            holder.editTask = (TextView) view.findViewById(R.id.edit_task);
            holder.number = (TextView) view.findViewById(R.id.number);
            holder.result = (TextView) view.findViewById(R.id.result);
            holder.iconResponse = (ImageView) view.findViewById(R.id.icon_response);
            view.setTag(holder);
        } else {
            holder = (TaskAdapterHolder) view.getTag();
        }

        holder.customerProjectActionName.setText(taskModel.getCustomerProjectActionName());
        holder.projectCustomerStatusName.setText(taskModel.getProjectCustomerStatusName());
        holder.scheduleDate.setText(taskModel.getScheduleDate());
        holder.finishDate.setText(taskModel.getFinishDate());
        holder.descriptionTask.setText(taskModel.getDescriptionTask());
        holder.descriptionResult.setText(taskModel.getDescriptionResult());
        holder.number.setText(taskModel.getNumber());

        if (projectCustomerStatus.equals(General.projectCustomerStatusFinish)) {
            holder.addTask.setVisibility(View.GONE);
            holder.result.setVisibility(View.GONE);
            holder.editTask.setVisibility(View.GONE);
        } else {
            if (taskModel.getFinishDate().equals("-")) {
                holder.addTask.setVisibility(View.GONE);
                holder.result.setVisibility(View.VISIBLE);
                holder.editTask.setVisibility(View.VISIBLE);
            } else {
                holder.addTask.setVisibility(View.VISIBLE);
                holder.result.setVisibility(View.GONE);
                holder.editTask.setVisibility(View.GONE);
            }
        }


        holder.addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddTaskActivity.class);
                i.putExtra("dbMasterRef", dbMasterRef);
                i.putExtra("projectRef", projectRef);
                i.putExtra("customerRef", customerRef);
                i.putExtra("projectCustomerStatus", projectCustomerStatus);
                i.putExtra("name", name);
                if (taskModel.getLogProjectCustomerStatusTaskRef() != null) {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", taskModel.getLogProjectCustomerStatusTaskRef());
                } else {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", "0");
                }
                i.putExtra("logProjectCustomerStatusTaskRef", "");
                context.startActivity(i);
            }
        });
        holder.editTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddTaskActivity.class);
                i.putExtra("dbMasterRef", dbMasterRef);
                i.putExtra("projectRef", projectRef);
                i.putExtra("customerRef", customerRef);
                i.putExtra("projectCustomerStatus", projectCustomerStatus);
                i.putExtra("name", name);
                if (taskModel.getLogProjectCustomerStatusTaskRef() != null) {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", taskModel.getLogProjectCustomerStatusTaskRef());
                } else {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", "0");
                }
                i.putExtra("logProjectCustomerStatusTaskRef", taskModel.getLogProjectCustomerStatusTaskRef());
                i.putExtra("customerProjectAction", taskModel.getCustomerProjectAction());
                i.putExtra("scheduleDate", taskModel.getScheduleDateValue());
                i.putExtra("descriptionTask", taskModel.getDescriptionTask());
                context.startActivity(i);
            }
        });
        holder.result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddTaskActivity.class);
                i.putExtra("isResult", true);
                i.putExtra("dbMasterRef", dbMasterRef);
                i.putExtra("projectRef", projectRef);
                i.putExtra("customerRef", customerRef);
                i.putExtra("projectCustomerStatus", projectCustomerStatus);
                i.putExtra("name", name);
                if (taskModel.getLogProjectCustomerStatusTaskRef() != null) {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", taskModel.getLogProjectCustomerStatusTaskRef());
                } else {
                    i.putExtra("parentLogProjectCustomerStatusTaskRef", "0");
                }
                i.putExtra("logProjectCustomerStatusTaskRef", taskModel.getLogProjectCustomerStatusTaskRef());
                i.putExtra("customerProjectAction", taskModel.getCustomerProjectAction());
                i.putExtra("scheduleDate", taskModel.getScheduleDateValue());
                i.putExtra("descriptionTask", taskModel.getDescriptionTask());
                i.putExtra("newToProgress", taskModel.getNewToProgress());
                i.putExtra("progressToInterest", taskModel.getProgressToInterest());
                i.putExtra("progressToNotInterest", taskModel.getProgressToNotInterest());
                i.putExtra("interestToFinish", taskModel.getInterestToFinish());
                i.putExtra("notInterestToFinish", taskModel.getNotInterestToFinish());
                context.startActivity(i);
            }
        });
        Glide.with(context).load(taskModel.getIconResponse()).into(holder.iconResponse);
        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private class TaskAdapterHolder {
        TextView customerProjectActionName, projectCustomerStatusName, scheduleDate, descriptionTask, finishDate, descriptionResult, number;
        TextView addTask, editTask, result;
        ImageView iconResponse;
    }

}
