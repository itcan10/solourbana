package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class DataSchedule {
    String schedule,dueDate,amount;

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
