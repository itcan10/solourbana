package com.nataproperty.solourbana.view.logbook.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.logbook.model.LogBookSectionModel;

import java.util.List;

/**
 * Created by UserModel on 4/21/2016.
 */
public class LogBookSectionAdapter extends BaseAdapter {
    public static final String TAG = "MyNupAdapter";

    private Context context;
    private List<LogBookSectionModel> list;
    private ListMyNupHolder holder;

    public LogBookSectionAdapter(Context context, List<LogBookSectionModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_section_log_book, null);
            holder = new ListMyNupHolder();
            holder.sectionName = (TextView) convertView.findViewById(R.id.txt_nup_type);
            holder.bookingCount = (TextView) convertView.findViewById(R.id.txt_nup_count);
            //holder.itemSection = (LinearLayout) convertView.findViewById(R.id.linear_count) ;

            convertView.setTag(holder);
        } else {
            holder = (ListMyNupHolder) convertView.getTag();
        }

        LogBookSectionModel logBookSectionModel = list.get(position);
        holder.sectionName.setText(logBookSectionModel.getProjectCustomerStatusName());
        holder.bookingCount.setText(logBookSectionModel.getCountStatus());
        if (!logBookSectionModel.getColor().trim().equals("")){
            holder.bookingCount.setBackgroundColor(Color.parseColor(logBookSectionModel.getColor()));
        }

        return convertView;
    }

    public class ListMyNupHolder {
        TextView sectionName, bookingCount;
        LinearLayout itemSection;
    }
}