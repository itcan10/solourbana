package com.nataproperty.solourbana.view.loginregister.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/14/2016.
 */
public class RegisterInhouseActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String AGENTY_TYPE = "agentType";
    public static final String AGENTY_TYPE_NAME = "agentTypeName";
    public static final String EMAIL = "email";

    EditText edtInhouseCode, edtMemberCode;

    Button btnValidate;

    String fullname, birthDate, phone, email, password, inhouse, memberCode = "";
    String agentType, agentTypeName, statusGoogleSignIn;

    private SharedPreferences sharedPreferences;

    ProgressDialog progressDialog;
    String message;
    int status;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_inhouse);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        fullname = sharedPreferences.getString("isfullname", null);
        birthDate = sharedPreferences.getString("isbirthDate", null);
        phone = sharedPreferences.getString("isphone", null);
        email = sharedPreferences.getString("isemail", null);
        password = sharedPreferences.getString("ispassword", null);
        agentType = sharedPreferences.getString("isagentType", null);
        agentTypeName = sharedPreferences.getString("isagentTypeName", null);
        statusGoogleSignIn = sharedPreferences.getString("statusGoogleSignIn", null);

        Log.d("cekProperty", " " + fullname + " " + birthDate + " " + phone + " " +
                email + " " + password + " " + agentType + " " + agentTypeName);

        title.setText(String.valueOf(agentTypeName));

        edtInhouseCode = (EditText) findViewById(R.id.txtInhouseCode);
        edtMemberCode = (EditText) findViewById(R.id.txtMemberCode);

        btnValidate = (Button) findViewById(R.id.btn_validate);
        btnValidate.setTypeface(font);

        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inhouse = edtInhouseCode.getText().toString().trim();
                memberCode = edtMemberCode.getText().toString().trim();

                if (!inhouse.isEmpty()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterInhouseActivity.this);
                    alertDialogBuilder.setMessage("Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
                    alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            registerFinish();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialogBuilder.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(RegisterInhouseActivity.this,RegisterTermActivity.class));
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    if (inhouse.isEmpty()) {
                        edtInhouseCode.setError("Inhouse code kosong");
                    } else {
                        edtInhouseCode.setError(null);
                    }
                }

            }
        });

    }

    private void registerFinish() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.registerInhouseForProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();

                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result register", response);
                    status = jo.getInt("status");

                    if (status == 200) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        SharedPreferences sharedPreferences = RegisterInhouseActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();
                        Intent intent = new Intent(RegisterInhouseActivity.this, RegisterFinishActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else if (status == 201) {
                        message = jo.getString("message");
                        Intent intent = new Intent(RegisterInhouseActivity.this, RegisterStepTwoActivity.class);
                        startActivity(intent);
                        finish();

                    } else if (status == 202) {
                        message = jo.getString("message");
                        edtInhouseCode.setError(message);

                    } else if (status == 203) {
                        message = jo.getString("message");

                    } else if (status == 500) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        Log.d("cek", memberType);
                        SharedPreferences sharedPreferences = RegisterInhouseActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();

                        Intent intent = new Intent(RegisterInhouseActivity.this, LaunchActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent);
                    } else {

                    }

                    if (status == 202) {
                        edtInhouseCode.setError(message);
                    } else {
                        edtInhouseCode.setError(null);
                        if (status == 203) {
                            edtMemberCode.setError(message);
                        } else {
                            edtMemberCode.setError(null);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterInhouseActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterInhouseActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fullname", fullname);
                params.put("username", email);
                params.put("mobile", phone);
                params.put("agentType", agentType);
                params.put("password", password);
                params.put("birthDate", birthDate);
                params.put("inhouseCode", edtInhouseCode.getText().toString().trim());
                params.put("status", statusGoogleSignIn);
                params.put("memberCode", memberCode);
                params.put("projectCode", General.projectCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "registerSales");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
