package com.nataproperty.solourbana.view.logbook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.logbook.model.ListEvent;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class ListEventAdapter extends BaseAdapter {
    private Context context;
    private List<ListEvent> list;
    private ListCountryHolder holder;

    public ListEventAdapter(Context context, List<ListEvent> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_spinner,null);
            holder = new ListCountryHolder();
            holder.countryName = (TextView) convertView.findViewById(R.id.name);

            convertView.setTag(holder);
        }else{
            holder = (ListCountryHolder) convertView.getTag();
        }
        ListEvent event = list.get(position);
        holder.countryName.setText(event.getEventName());

        return convertView;
    }

    private class ListCountryHolder {
        TextView countryName;
    }
}
