package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by UserModel on 7/14/2016.
 */
public class KprTableModel {
    String ref,insPeriode,instMonth,total,pct;

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getInsPeriode() {
        return insPeriode;
    }

    public void setInsPeriode(String insPeriode) {
        this.insPeriode = insPeriode;
    }

    public String getInstMonth() {
        return instMonth;
    }

    public void setInstMonth(String instMonth) {
        this.instMonth = instMonth;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPct() {
        return pct;
    }

    public void setPct(String pct) {
        this.pct = pct;
    }
}
