package com.nataproperty.solourbana.view.commision.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCommisionDetailInteractor {
    void GetListCommissionDetailProjectSvc(String dbMasterRef, String projectRef, String bookingRef, String projectPsRef);

}
