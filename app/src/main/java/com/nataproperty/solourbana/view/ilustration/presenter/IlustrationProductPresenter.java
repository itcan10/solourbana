package com.nataproperty.solourbana.view.ilustration.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.ilustration.intractor.IlustrationProductInteractor;
import com.nataproperty.solourbana.view.ilustration.model.GenerateClusterInfoParam;
import com.nataproperty.solourbana.view.ilustration.model.IlustrationModel;
import com.nataproperty.solourbana.view.ilustration.model.IlustrationStatusModel;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationProductActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationProductPresenter implements IlustrationProductInteractor {
    private IlustrationProductActivity view;
    private ServiceRetrofit service;

    public IlustrationProductPresenter(IlustrationProductActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }



    @Override
    public void getIlustration(String dbMasterRef, String projectRef, String clusterRef, String edtSearch,String productRef) {
        Call<List<IlustrationModel>> call = service.getAPI().getIlustrationPayment2(dbMasterRef,projectRef,clusterRef,edtSearch,productRef);
        call.enqueue(new Callback<List<IlustrationModel>>() {
            @Override
            public void onResponse(Call<List<IlustrationModel>> call, Response<List<IlustrationModel>> response) {
                view.showIlustrationResults(response);
            }

            @Override
            public void onFailure(Call<List<IlustrationModel>> call, Throwable t) {
                view.showIlustrationFailure(t);

            }


        });

    }

    @Override
    public void getClusterInfo(GenerateClusterInfoParam generateClusterInfoParam) {
        Call<IlustrationStatusModel> call = service.getAPI().getClusterInfo_V2(Utils.modelToJson(generateClusterInfoParam));
        call.enqueue(new Callback<IlustrationStatusModel>() {
            @Override
            public void onResponse(Call<IlustrationStatusModel> call, Response<IlustrationStatusModel> response) {
                view.showClusterInfoResults(response);
            }

            @Override
            public void onFailure(Call<IlustrationStatusModel> call, Throwable t) {
                view.showClusterInfoFailure(t);

            }


        });

    }

}
