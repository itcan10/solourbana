package com.nataproperty.solourbana.view.loginregister.presenter;


import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.loginregister.intractor.RegisterQRInteractor;
import com.nataproperty.solourbana.view.loginregister.ui.RegisterCustomerQRActivity;
import com.nataproperty.solourbana.view.scanQRCode.model.ScanURLModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class RegisterQRPresenter implements RegisterQRInteractor {
    private RegisterCustomerQRActivity view;
    private ServiceRetrofitProject service;

    public RegisterQRPresenter(RegisterCustomerQRActivity view, ServiceRetrofitProject service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void CheckingDocumentProject(String projectCode, String bookingCode) {
        Call<ScanURLModel> call = service.getAPI().GenerateDocumentProjectQRCode(projectCode, bookingCode);
        call.enqueue(new Callback<ScanURLModel>() {
            @Override
            public void onResponse(Call<ScanURLModel> call, Response<ScanURLModel> response) {
                view.showScanResults(response);
            }

            @Override
            public void onFailure(Call<ScanURLModel> call, Throwable t) {
                view.showScanFailure(t);

            }


        });
    }

}
