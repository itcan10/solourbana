package com.nataproperty.solourbana.view.before_login.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterLaunchInteractor {
    void getVersionApp(String version);
}
