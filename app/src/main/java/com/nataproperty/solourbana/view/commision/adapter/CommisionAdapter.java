package com.nataproperty.solourbana.view.commision.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.commision.model.CommisionModel;
import com.nataproperty.solourbana.view.commision.ui.CommisionDetailActivity;
import com.nataproperty.solourbana.view.commision.ui.CommisionScheduleActivity;

import java.util.List;

/**
 * Created by UserModel on 4/21/2016.
 */
public class CommisionAdapter extends BaseAdapter {
    public static final String TAG = "MyNupAdapter";

    private Context context;
    private List<CommisionModel> list;
    private ListMyNupHolder holder;
    private Typeface font;

    public CommisionAdapter(Context context, List<CommisionModel> list, Typeface font) {
        this.context = context;
        this.list = list;
        this.font = font;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list_commision, null);
            holder = new ListMyNupHolder();
            holder.bookingCode = (TextView) convertView.findViewById(R.id.txt_booking_code);
            holder.projectName = (TextView) convertView.findViewById(R.id.txt_property);
            holder.clusterName = (TextView) convertView.findViewById(R.id.txt_cluster_name);
            holder.blockName = (TextView) convertView.findViewById(R.id.txt_block_name);
            holder.unitName = (TextView) convertView.findViewById(R.id.txt_unit_no);
            holder.comPriceAmt = (TextView) convertView.findViewById(R.id.txt_comPriceAmt);
            holder.commisionStatusName = (TextView) convertView.findViewById(R.id.txt_commisionStatusName);
            holder.totalAmtCommision = (TextView) convertView.findViewById(R.id.txt_totalAmtCommision);
            holder.custPaid = (TextView) convertView.findViewById(R.id.txt_custPaid);
            holder.btnPaymentDetail = (Button) convertView.findViewById(R.id.btn_payment_detail);
            holder.customerName = (TextView) convertView.findViewById(R.id.txt_customer_name);
            holder.btnPaymentSchedule = (Button) convertView.findViewById(R.id.btn_payment_schedule);

            convertView.setTag(holder);
        } else {
            holder = (ListMyNupHolder) convertView.getTag();
        }

        final CommisionModel commisionModel = list.get(position);
        holder.bookingCode.setText(commisionModel.getBookingCode());
        holder.projectName.setText(commisionModel.getProjectName());
        holder.clusterName.setText(commisionModel.getClusterName());
        holder.blockName.setText(commisionModel.getBlockName());
        holder.unitName.setText(commisionModel.getUnitName());
        holder.comPriceAmt.setText(commisionModel.getComPriceAmt());
        if (commisionModel.getCountCommision().equals("0")) {
            holder.commisionStatusName.setText(commisionModel.getCommisionStatusName());
        } else {
            holder.commisionStatusName.setText(commisionModel.getCommisionStatusName() + " (" + commisionModel.getCountCommision() + ")");
        }
        holder.totalAmtCommision.setText(commisionModel.getTotalAmtCommision());
        holder.custPaid.setText(commisionModel.getCustPaid()+" %");
        holder.customerName.setText(commisionModel.getCustomerName());

        holder.btnPaymentDetail.setTypeface(font);
        holder.btnPaymentDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!commisionModel.getCountCommision().trim().equals("0")) {
                    Intent intent = new Intent(context, CommisionDetailActivity.class);
                    intent.putExtra("dbMasterRef", commisionModel.getDbMasterRef());
                    intent.putExtra("projectRef", commisionModel.getProjectRef());
                    intent.putExtra("bookingRef", commisionModel.getBookingRef());
                    intent.putExtra("projectPsRef", commisionModel.getProjectPsRef());
                    intent.putExtra("bookingCode", commisionModel.getBookingCode());
                    intent.putExtra("projectName", commisionModel.getProjectName());
                    intent.putExtra("clusterName", commisionModel.getClusterName());
                    intent.putExtra("blockName", commisionModel.getBlockName());
                    intent.putExtra("unitName", commisionModel.getUnitName());
                    intent.putExtra("comPriceAmt", commisionModel.getComPriceAmt());
                    intent.putExtra("commisionStatusName", commisionModel.getCommisionStatusName());
                    intent.putExtra("countCommision", commisionModel.getCountCommision());
                    intent.putExtra("totalAmtCommision", commisionModel.getTotalAmtCommision());
                    intent.putExtra("custPaid", commisionModel.getCustPaid());
                    intent.putExtra("customerName", commisionModel.getCustomerName());
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "New Komisi", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (!commisionModel.getIsShowCommissionCust().trim().equals("0")){
            holder.btnPaymentSchedule.setVisibility(View.VISIBLE);
        } else {
            holder.btnPaymentSchedule.setVisibility(View.GONE);
        }

        Log.d(TAG,"IsShowCommissionCust "+commisionModel.getIsShowCommissionCust());

        holder.btnPaymentSchedule.setTypeface(font);
        holder.btnPaymentSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommisionScheduleActivity.class);
                intent.putExtra("dbMasterRef", commisionModel.getDbMasterRef());
                intent.putExtra("projectRef", commisionModel.getProjectRef());
                intent.putExtra("bookingRef", commisionModel.getBookingRef());
                intent.putExtra("projectPsRef", commisionModel.getProjectPsRef());
                intent.putExtra("bookingCode", commisionModel.getBookingCode());
                intent.putExtra("projectName", commisionModel.getProjectName());
                intent.putExtra("clusterName", commisionModel.getClusterName());
                intent.putExtra("blockName", commisionModel.getBlockName());
                intent.putExtra("unitName", commisionModel.getUnitName());
                intent.putExtra("comPriceAmt", commisionModel.getComPriceAmt());
                intent.putExtra("commisionStatusName", commisionModel.getCommisionStatusName());
                intent.putExtra("countCommision", commisionModel.getCountCommision());
                intent.putExtra("totalAmtCommision", commisionModel.getTotalAmtCommision());
                intent.putExtra("custPaid", commisionModel.getCustPaid());
                intent.putExtra("customerName", commisionModel.getCustomerName());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    public class ListMyNupHolder {
        TextView bookingCode,customerName, projectName, clusterName, blockName, unitName, commisionStatusName, comPriceAmt, custPaid, totalAmtCommision;
        Button btnPaymentDetail,btnPaymentSchedule;
    }
}