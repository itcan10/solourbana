package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by UserModel on 11/3/2016.
 */
public class UnitFloorModel {
    String surveyUnitFloorRef,surveyUnitFloorName;

    public String getSurveyUnitFloorRef() {
        return surveyUnitFloorRef;
    }

    public void setSurveyUnitFloorRef(String surveyUnitFloorRef) {
        this.surveyUnitFloorRef = surveyUnitFloorRef;
    }

    public String getSurveyUnitFloorName() {
        return surveyUnitFloorName;
    }

    public void setSurveyUnitFloorName(String surveyUnitFloorName) {
        this.surveyUnitFloorName = surveyUnitFloorName;
    }
}
