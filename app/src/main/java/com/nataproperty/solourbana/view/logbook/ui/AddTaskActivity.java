package com.nataproperty.solourbana.view.logbook.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.FixedHoloDatePickerDialog;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerCustomerResponeAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerProjectActionAdapter;
import com.nataproperty.solourbana.view.logbook.model.ContactActionModel;
import com.nataproperty.solourbana.view.logbook.model.CustomerResponse;
import com.nataproperty.solourbana.view.logbook.model.ProjectAction;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.logbook.presenter.AddTaskPresenter;
import com.nataproperty.solourbana.view.nup.ui.NupCreateCustomerActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Response;

public class AddTaskActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private AddTaskPresenter presenter;
    Spinner spnContactAction;
    MyListView listViewResponeCustomer;
    EditText edtDescTask, edtDescResult, edtSchaduleDate, edtFinishDate;
    String dbMasterRef, projectRef, logProjectCustomerStatusTaskRef = "", customerRef = "", projectCustomerStatus = "",
            memberRef, customerProjectAction = "", customerResponseStatus = "0", name = "", parentLogProjectCustomerStatusTaskRef = "0",
            scheduleDate, finishDate, descriptionTask, descriptionResult, newToProgress, progressToInterest, progressToNotInterest, interestToFinish, notInterestToFinish;
    SpinnerProjectActionAdapter adapter;
    SpinnerCustomerResponeAdapter adapterCustomerResponeAdapter;
    ArrayList<ProjectAction> listProjectAction = new ArrayList<>();
    ArrayList<CustomerResponse> listCustomerResponse = new ArrayList<>();

    Toolbar toolbar;
    Typeface font;
    TextView title, txtResult;
    View lineView;
    LinearLayout linearLayoutResult, activityAddTask;

    Button btnSave, btnNotInterest, btnInterest, btnSaveFinish;

    SharedPreferences sharedPreferences;
    Calendar myCalendar = Calendar.getInstance();
    ProgressDialog progressDialog;
    Map<String, String> fieldss = new HashMap<>();

    boolean isResult;

    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    String formattedDate = df.format(c.getTime());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new AddTaskPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent i = getIntent();
        dbMasterRef = i.getStringExtra("dbMasterRef");
        projectRef = i.getStringExtra("projectRef");
        customerRef = i.getStringExtra("customerRef");
        projectCustomerStatus = i.getStringExtra("projectCustomerStatus");
        logProjectCustomerStatusTaskRef = i.getStringExtra("logProjectCustomerStatusTaskRef");
        parentLogProjectCustomerStatusTaskRef = i.getStringExtra("parentLogProjectCustomerStatusTaskRef");
        name = i.getStringExtra("name");
        customerProjectAction = i.getStringExtra("customerProjectAction");
        scheduleDate = i.getStringExtra("scheduleDate");
        descriptionTask = i.getStringExtra("descriptionTask");
        isResult = i.getBooleanExtra("isResult", false);

        requestActionContact();

        if (logProjectCustomerStatusTaskRef.equals("")) {
            //add task
            linearLayoutResult.setVisibility(View.GONE);
            edtSchaduleDate.setText(formattedDate);
        } else {
            //edit / result
            //linearLayoutResult.setVisibility(View.VISIBLE);
            if (isResult) {
                linearLayoutResult.setVisibility(View.VISIBLE);
                edtSchaduleDate.setEnabled(false);
                spnContactAction.setEnabled(false);
                edtDescTask.setEnabled(false);

                newToProgress = i.getStringExtra("newToProgress");
                progressToInterest = i.getStringExtra("progressToInterest");
                progressToNotInterest = i.getStringExtra("progressToNotInterest");
                interestToFinish = i.getStringExtra("interestToFinish");
                notInterestToFinish = i.getStringExtra("notInterestToFinish");

                if (projectCustomerStatus.equals(General.projectCustomerStatusProgress)) {
                    if (!progressToInterest.equals("0")) {
                        btnInterest.setVisibility(View.VISIBLE);
                    }
                    if (!progressToNotInterest.equals("0")) {
                        btnNotInterest.setVisibility(View.VISIBLE);
                    }
                }

                if (projectCustomerStatus.equals(General.projectCustomerStatusInterest)) {
                    if (!interestToFinish.equals("0")) {
                        btnSaveFinish.setVisibility(View.VISIBLE);
                    } else {
                        btnSaveFinish.setVisibility(View.GONE);
                    }

                }

                if (projectCustomerStatus.equals(General.projectCustomerStatusNotInterest)) {
                    if (!notInterestToFinish.equals("0")) {
                        btnSaveFinish.setVisibility(View.VISIBLE);
                    } else {
                        btnSaveFinish.setVisibility(View.GONE);
                    }

                }

            }

            edtSchaduleDate.setText(scheduleDate);
            spnContactAction.setSelection(Integer.parseInt(customerProjectAction));
            edtDescTask.setText(descriptionTask);
            edtFinishDate.setText(formattedDate);
        }

        setupSpinner();
    }

    private void requestActionContact() {
        presenter.GetLookupProjectAction(dbMasterRef, projectRef);
    }

    public void showListAddTaskFailure(Throwable t) {

    }

    public void showListAddTaskResults(Response<ContactActionModel> response) {
        if (response.isSuccessful()) {
            listProjectAction = response.body().getProjectAction();
            listCustomerResponse = response.body().getCustomerResponse();
            initAdapter();
        }

    }

    private void initAdapter() {
        adapter = new SpinnerProjectActionAdapter(this, listProjectAction);
        spnContactAction.setAdapter(adapter);
        adapterCustomerResponeAdapter = new SpinnerCustomerResponeAdapter(this, listCustomerResponse);
        listViewResponeCustomer.setAdapter(adapterCustomerResponeAdapter);
        listViewResponeCustomer.setExpanded(true);
        listViewResponeCustomer.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Add Contact Task");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        spnContactAction = (Spinner) findViewById(R.id.spn_contact_action);
        listViewResponeCustomer = (MyListView) findViewById(R.id.spn_respone_customer);
        edtDescTask = (EditText) findViewById(R.id.edt_desc);
        edtDescResult = (EditText) findViewById(R.id.edt_desc_result);
        edtSchaduleDate = (EditText) findViewById(R.id.edt_schadule_date);
        edtFinishDate = (EditText) findViewById(R.id.edt_finish_date);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        btnNotInterest = (Button) findViewById(R.id.btn_save_not_interes);
        btnNotInterest.setOnClickListener(this);
        btnInterest = (Button) findViewById(R.id.btn_save_interes);
        btnInterest.setOnClickListener(this);
        btnSaveFinish = (Button) findViewById(R.id.btn_save_finish);
        btnSaveFinish.setOnClickListener(this);
        linearLayoutResult = (LinearLayout) findViewById(R.id.linearLayoutResult);

        edtSchaduleDate.setInputType(InputType.TYPE_NULL);
        edtSchaduleDate.setTextIsSelectable(true);
        edtSchaduleDate.setFocusable(false);
        edtSchaduleDate.setOnClickListener(this);

        edtFinishDate.setInputType(InputType.TYPE_NULL);
        edtFinishDate.setTextIsSelectable(true);
        edtFinishDate.setFocusable(false);
        edtFinishDate.setOnClickListener(this);
        activityAddTask = (LinearLayout) findViewById(R.id.activity_add_task);
    }

    private void setupSpinner() {
        spnContactAction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customerProjectAction = listProjectAction.get(position).getCustomerProjectAction();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listViewResponeCustomer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                customerResponseStatus = listCustomerResponse.get(position).getCustomerResponseStatus();
            }
        });

    }

    @Override
    public void onClick(View v) {
        scheduleDate = edtSchaduleDate.getText().toString();
        descriptionTask = edtDescTask.getText().toString();
        finishDate = edtFinishDate.getText().toString();
        descriptionResult = edtDescResult.getText().toString();
        switch (v.getId()) {
            case R.id.edt_schadule_date:
                dialogPicker(edtSchaduleDate);
                break;

            case R.id.edt_finish_date:
                dialogPicker(edtFinishDate);
                break;

            case R.id.btn_save:
                if (validate()) {
                    if (isResult) {
                        if (validateResult()) {
                            if (validateFinishDate()) {
                                insertTask(projectCustomerStatus, "1", projectCustomerStatus);
                            } else {
                                showNotSave();
                            }
                        }
                    } else {
                        finishDate = "";
                        insertTask(projectCustomerStatus, "1", projectCustomerStatus);
                    }
                }
                break;

            case R.id.btn_save_not_interes:
                if (validate()) {
                    if (isResult) {
                        if (validateResult()) {
                            if (validateFinishDate()) {
                                insertTask(General.projectCustomerStatusNotInterest, "2", projectCustomerStatus);
                            } else {
                                showNotSave();
                            }
                        }
                    } else {
                        finishDate = "";
                        insertTask(General.projectCustomerStatusNotInterest, "2", projectCustomerStatus);
                    }

                }
                break;

            case R.id.btn_save_interes:
                if (validate()) {
                    if (isResult) {
                        if (validateResult()) {
                            if (validateFinishDate()) {
                                insertTask(General.projectCustomerStatusInterest, "3", projectCustomerStatus); //3 interest
                            } else {
                                showNotSave();
                            }
                        }
                    } else {
                        finishDate = "";
                        insertTask(General.projectCustomerStatusInterest, "3", projectCustomerStatus);
                    }
                }
                break;

            case R.id.btn_save_finish:
                if (isResult) {
                    if (validateResult()) {
                        if (validateFinishDate()) {
                            insertTask(General.projectCustomerStatusFinish, "4", projectCustomerStatus);
                        } else {
                            showNotSave();
                        }
                    }
                } else {
                    finishDate = "";
                    insertTask(General.projectCustomerStatusFinish, "4", projectCustomerStatus);
                }

                break;
        }
    }

    private boolean validateFinishDate() {
        boolean valid = true;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date scDate = sdf.parse(scheduleDate);
            Date fnDate = sdf.parse(finishDate);
            Date currenDate = sdf.parse(formattedDate);
            if (fnDate.getTime() < scDate.getTime()) {
                return false;
            }
//            else if (fnDate.getTime() > currenDate.getTime()) {
//                return false;
//            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return valid;
    }

    private boolean validate() {
        boolean valid = true;
        if (scheduleDate.equals("") || descriptionTask.equals("")) {
            if (scheduleDate.equals("")) {
                edtSchaduleDate.setError(getString(R.string.isEmpty));
                valid = false;
            } else {
                edtSchaduleDate.setError(null);
            }

            if (descriptionTask.equals("")) {
                edtDescTask.setError(getString(R.string.isEmpty));
                valid = false;
            } else {
                edtDescTask.setError(null);
            }
        }
        return valid;
    }

    private boolean validateResult() {
        boolean valid = true;
        if (finishDate.equals("") || descriptionResult.equals("") || customerResponseStatus.equals("0")) {
            if (finishDate.equals("")) {
                edtFinishDate.setError(getString(R.string.isEmpty));
                valid = false;
            } else {
                edtFinishDate.setError(null);
            }

            if (descriptionResult.equals("")) {
                edtDescResult.setError(getString(R.string.isEmpty));
                valid = false;
            } else {
                edtDescResult.setError(null);
            }

            if (customerResponseStatus.equals("0")) {
                Snackbar snackbar = Snackbar.make(activityAddTask, "Silakan isi customer response.", Snackbar.LENGTH_SHORT);
                snackbar.show();
                valid = false;
            }
        }
        return valid;
    }

    private void showNotSave(){
        Snackbar snackbar = Snackbar.make(activityAddTask, "Finish date harus lebih besar sama dengan scheduledate", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void insertTask(String projectCustomer, String btnStatus, String projectCustomerStatus) {
       /* Log.d("insert", " " + memberRef + "," + dbMasterRef + "," + projectRef + "," + customerRef + "," + projectCustomerStatus + "," +
                customerProjectAction + "," + parentLogProjectCustomerStatusTaskRef + "," + edtDescTask.getText().toString() + "," +
                edtSchaduleDate.getText().toString() + "," + edtFinishDate.getText().toString() + "," + name + "," + customerResponseStatus + "," +
                logProjectCustomerStatusTaskRef);*/
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        fieldss.put("memberRef", memberRef);
        fieldss.put("dbMasterRef", dbMasterRef);
        fieldss.put("projectRef", projectRef);
        fieldss.put("customerRef", customerRef);
        fieldss.put("customerProjectStatusRef", projectCustomer);
        fieldss.put("customerProjectAction", customerProjectAction);
        fieldss.put("parentLogProjectCustomerStatusTaskRef", parentLogProjectCustomerStatusTaskRef);
        fieldss.put("descriptionTask", edtDescTask.getText().toString());
        fieldss.put("scheduleDate", edtSchaduleDate.getText().toString());
        fieldss.put("finishDate", finishDate);
        fieldss.put("descriptionResult", descriptionResult);
        fieldss.put("customerName", name);
        fieldss.put("customerResponseStatus", customerResponseStatus);
        fieldss.put("logProjectCustomerStatusTaskRef", logProjectCustomerStatusTaskRef);
        presenter.insertTaskContactBankSvc(fieldss, btnStatus, projectCustomerStatus);
    }

    public void showResponeResults(Response<ResponeModel> response, String statusButton, String projectCustomerStatus) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            Log.d("Respone", " " + status);
            if (status == 200) {
                if (statusButton.equals("1")) {//jika status buttonya 1 cuma finish tapi jika yang lain ke section logbook
                    if (projectCustomerStatus.equals(General.projectCustomerStatusNew)) { //jika new dah baru bikin tast pertama kali diintent ke section pindah ke status progress
                        Intent i = new Intent(AddTaskActivity.this, LogBookSectionActivity.class);
                        i.putExtra("dbMasterRef", dbMasterRef);
                        i.putExtra("projectRef", projectRef);
                        i.putExtra("message","Status berubah menjadi Progress");
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    } else {
                        finish();
                    }
                } else {
                    Intent i = new Intent(AddTaskActivity.this, LogBookSectionActivity.class);
                    i.putExtra("dbMasterRef", dbMasterRef);
                    i.putExtra("projectRef", projectRef);
                    if (statusButton.equals("2")){
                        i.putExtra("message","Status berubah menjadi Not Interest");
                    } else if (statusButton.equals("3")){
                        i.putExtra("message","Status berubah menjadi Interest");
                    } else {
                        i.putExtra("message","Status berubah menjadi Finish");
                    }
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }

            } else {
                Snackbar snackbar = Snackbar.make(activityAddTask, message, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    public void showResponeFailure(Throwable t) {
        progressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(activityAddTask, "Terjadi kesalahan.", Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    final long today = System.currentTimeMillis() - 1000;



//    private void dialogPicker(final EditText edt) {
//        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                updateLabel(edt);
//            }
//
//        };
//
//        new DatePickerDialog(AddTaskActivity.this, R.style.DatePickerDialogTheme, date,
//                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
//    }

    private void dialogPicker(final EditText edt) {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(edt);
            }

        };

        final Context themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog);
        final DatePickerDialog dialog = new FixedHoloDatePickerDialog(
                themedContext,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dialog.show();
    }

    private void updateLabel(EditText edtText) {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edtText.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

}
