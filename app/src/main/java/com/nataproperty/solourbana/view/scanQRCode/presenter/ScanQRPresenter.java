package com.nataproperty.solourbana.view.scanQRCode.presenter;

import com.google.android.gms.fitness.data.Subscription;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.scanQRCode.intractor.ScanQRInteractor;
import com.nataproperty.solourbana.view.scanQRCode.model.ScanURLModel;
import com.nataproperty.solourbana.view.scanQRCode.ui.ScanQRActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("unchecked")
public class ScanQRPresenter implements ScanQRInteractor {
    private ScanQRActivity view;
    private ServiceRetrofitProject service;
    private Subscription subscription;

    public ScanQRPresenter(ScanQRActivity view, ServiceRetrofitProject service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void CheckingDocumentProject(String projectCode,final String bookingCode) {
        Call<ScanURLModel> call = service.getAPI().GenerateDocumentProjectQRCode(projectCode, bookingCode);
        call.enqueue(new Callback<ScanURLModel>() {
            @Override
            public void onResponse(Call<ScanURLModel> call, Response<ScanURLModel> response) {
                view.showScanResults(response,bookingCode);
            }

            @Override
            public void onFailure(Call<ScanURLModel>  call, Throwable t) {
                view.showScanFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {

    }


}
