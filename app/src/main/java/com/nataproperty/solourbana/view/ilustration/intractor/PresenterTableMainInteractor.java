package com.nataproperty.solourbana.view.ilustration.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterTableMainInteractor {
    void getBlockMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef, String productRefParam, String memberRef);

    void getUnitMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef);

}
