package com.nataproperty.solourbana.view.gallery.model;

/**
 * Created by UserModel on 9/6/2016.
 */
public class CategoryGalleryModel {
    String groupGalleryRef,dbMasterRef,projectRef,groupGalleryName,groupGalleryDescription;

    public String getGroupGalleryRef() {
        return groupGalleryRef;
    }

    public void setGroupGalleryRef(String groupGalleryRef) {
        this.groupGalleryRef = groupGalleryRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getGroupGalleryName() {
        return groupGalleryName;
    }

    public void setGroupGalleryName(String groupGalleryName) {
        this.groupGalleryName = groupGalleryName;
    }

    public String getGroupGalleryDescription() {
        return groupGalleryDescription;
    }

    public void setGroupGalleryDescription(String groupGalleryDescription) {
        this.groupGalleryDescription = groupGalleryDescription;
    }
}
