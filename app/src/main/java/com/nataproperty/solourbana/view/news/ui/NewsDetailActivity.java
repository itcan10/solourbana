package com.nataproperty.solourbana.view.news.ui;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mainmenu.ui.MainMenuActivity;
import com.nataproperty.solourbana.view.news.adapter.NewsDetailAdapter;
import com.nataproperty.solourbana.view.news.model.NewsDetailModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

public class NewsDetailActivity extends AppCompatActivity {
    public static final String TAG = "NewsDetailActivity" ;

    public static final String DBMASTER_REF = "dbMasterRef" ;
    public static final String PROJECT_REF = "projectRef" ;
    public static final String TITLE = "title" ;
    public static final String PUBLISH_DATE = "publishDate" ;
    public static final String DEVELOPER_NAME = "developerName" ;
    public static final String CONTENT = "content" ;
    public static final String CONTENT_REF = "contentRef" ;
    public static final String LINK_DETAIL = "linkDetail" ;
    public static final String LINK_SHARE = "linkShare" ;

    String dbMasterRef,projectRef,txtTitle,publishDate,developerName,content,contentRef,linkDetail
            , linkShare;

    private List<NewsDetailModel> listImage = new ArrayList<NewsDetailModel>();
    private NewsDetailAdapter adapter;
    ViewPager viewPager;

    TextView txttitle,txtPublishDate,txtSynopsis,txtDeveloperName;

    WebView webView;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_news));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);

        txtTitle = intent.getStringExtra(TITLE);
        publishDate = intent.getStringExtra(PUBLISH_DATE);
        developerName = intent.getStringExtra(DEVELOPER_NAME);

        content = intent.getStringExtra(CONTENT);
        contentRef = intent.getStringExtra(CONTENT_REF);

        linkDetail= intent.getStringExtra(LINK_DETAIL);
        linkShare= intent.getStringExtra(LINK_SHARE);

        Log.d(TAG,txtTitle+" "+publishDate+" "+developerName+" "+contentRef+" "+linkDetail);

        requestImage();

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.viewpager_news_detail);
        adapter = new NewsDetailAdapter(this, listImage);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());

        txttitle = (TextView) findViewById(R.id.txt_title);
        txtPublishDate = (TextView) findViewById(R.id.txt_publish_date);
        txtDeveloperName = (TextView) findViewById(R.id.txt_developer_name);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, txtTitle);
                i.putExtra(Intent.EXTRA_TEXT, linkShare);
                startActivity(Intent.createChooser(i, "Share via"));
            }
        });
        txttitle.setText(txtTitle);
        txtPublishDate.setText(publishDate);
        txtDeveloperName.setText(developerName);

        webView = (WebView) findViewById(R.id.web_content);
        webView.loadUrl(linkDetail);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.78125;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
    }

    public void requestImage() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getImageSlider(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result projectttt",response);
                    generateListImage(jsonArray);

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NewsDetailActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NewsDetailActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("contentRef", contentRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "newsImage");

    }

    private void generateListImage(JSONArray response){
        for (int i = 0; i < response.length();i++){
            try {
                JSONObject jo = response.getJSONObject(i);
                NewsDetailModel newsDetailModel = new NewsDetailModel();
                newsDetailModel.setImageSlider(jo.getString("imageSlider"));

                listImage.add(newsDetailModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NewsDetailActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
