package com.nataproperty.solourbana.view.booking.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.booking.model.SaveUnitBookingModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/18/2016.
 */
public class BookingReviewOrderActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    ProgressDialog progressDialog;

    SharedPreferences sharedPreferences;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    TextView txtPriortyPrice, txtTotal;
    TextView txtFullname, txtEmail, txtMobile, txtPhone, txtAddress, txtKtpId, txtQty;
    EditText editFullname, editEmail, editMobile, editPhone, editAddress, editKtpId, editQty,editNote;
    ImageView imgKtp, imgNpwp;
    Button btnUploadKtp, btnUploadNpwp, btnNext;

    private String propertys, category, product, unit, area, priceInc;
    private String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address, qty, total, npwpid;
    private String dbMasterRef, categoryRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo, projectName;
    private String memberRef, memberCustomerRef, nupAmt, projectDescription, status;
    private String nupOrderRef, message, projectBookingRef;
    private String va1, va2, salesEvent, salesLocation, salesReferral, salesPurpose, moneySource,salesEventName,salesLocationName
            ,salesReferralName,salesPurposeName,moneySourceName;
    LinearLayout viewInfoBooking,linearSalesEvent,linearSalesLocation,linearSalesReferal,linearSalesPurpose,linearMoneySource;
    TextView txtSalesEvent,txtSalesLocation,txtSalesReferal,txtSalesPurpose,txtMoneySource;
    private String note;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_review_order);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_booking_review));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberCustomerRef = sharedPreferences.getString("isMemberCostumerRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");

        fullname = intent.getStringExtra(FULLNAME);
        mobile1 = intent.getStringExtra(MOBILE);
        phone1 = intent.getStringExtra(PHONE);
        email1 = intent.getStringExtra(EMAIL);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);
        ktpRef = intent.getStringExtra(KTP_REF);
        npwpid = intent.getStringExtra("NPWP");
        npwpRef = intent.getStringExtra(NPWP_REF);

        //booking information
        salesEvent = intent.getStringExtra("salesEvent");
        salesEventName = intent.getStringExtra("salesEventName");
        salesLocation = intent.getStringExtra("salesLocation");
        salesLocationName = intent.getStringExtra("salesLocationName");
        salesReferral = intent.getStringExtra("salesReferral");
        salesReferralName = intent.getStringExtra("salesReferralName");
        salesPurpose = intent.getStringExtra("salesPurpose");
        salesPurposeName = intent.getStringExtra("salesPurposeName");
        moneySource = intent.getStringExtra("moneySource");
        moneySourceName = intent.getStringExtra("moneySourceName");
        note = intent.getStringExtra("note");

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * Booking info
         */
        viewInfoBooking = (LinearLayout) findViewById(R.id.linear_booking_info);
        linearSalesEvent = (LinearLayout) findViewById(R.id.linear_sales_event);
        linearSalesLocation = (LinearLayout) findViewById(R.id.linear_sales_location);
        linearSalesReferal = (LinearLayout) findViewById(R.id.linear_sales_referal);
        linearSalesPurpose = (LinearLayout) findViewById(R.id.linear_sales_purpose);
        linearMoneySource = (LinearLayout) findViewById(R.id.linear_money_source);
        txtSalesEvent = (TextView) findViewById(R.id.txt_sales_event) ;
        txtSalesLocation = (TextView) findViewById(R.id.txt_sales_location) ;
        txtSalesReferal = (TextView) findViewById(R.id.txt_sales_referal) ;
        txtSalesPurpose = (TextView) findViewById(R.id.txt_sales_purpose) ;
        txtMoneySource = (TextView) findViewById(R.id.txt_money_source) ;

        /**
         * informasion customer
         */
        txtFullname = (TextView) findViewById(R.id.txt_fullname);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtMobile = (TextView) findViewById(R.id.txt_mobile);
        txtPhone = (TextView) findViewById(R.id.txt_phone);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtKtpId = (TextView) findViewById(R.id.txt_ktp_id);

        editNote = (EditText) findViewById(R.id.edit_note);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setTypeface(font);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        /**
         * seter
         */
        txtFullname.setText(fullname);
        txtEmail.setText(email1);
        txtPhone.setText(phone1);
        txtMobile.setText(mobile1);
        txtAddress.setText(address);
        txtKtpId.setText(ktpid);
        txtSalesEvent.setText(salesEventName);
        txtSalesLocation.setText(salesLocationName);
        txtSalesReferal.setText(salesReferralName);
        txtSalesPurpose.setText(salesPurposeName);
        txtMoneySource.setText(moneySourceName);

        Glide.with(BookingReviewOrderActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(imgKtp);

        Glide.with(BookingReviewOrderActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(imgNpwp);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (editNote.getText().toString().isEmpty()){
//                    note = "";
//
//                } else   {
//                    note = editNote.getText().toString();
//                }

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BookingReviewOrderActivity.this);
                alertDialogBuilder.setTitle("Are you sure ?");
                alertDialogBuilder.setMessage("Dengan klik “Ya” maka status unit akan ter-booking dan Anda harus melunasi pembayaran Booking Fee dalam jangka waktu yang ditentukan.");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveOrderBooking();

                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPayment();

    }

    public void requestPayment() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getPayment(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    Log.d("Cek payment info", response);
                    if (status == 200) {
                        paymentTerm = jo.getJSONObject("dataPrice").getString("paymentTerm");
                        priceIncVat = jo.getJSONObject("dataPrice").getString("priceInc");
                        discPercent = jo.getJSONObject("dataPrice").getString("discPercent");
                        discAmt = jo.getJSONObject("dataPrice").getString("discAmt");
                        netPrice = jo.getJSONObject("dataPrice").getString("netPrice");
                        termCondition = jo.getJSONObject("dataPrice").getString("termCondition");

                        propertys = jo.getJSONObject("propertyInfo").getString("propertys");
                        category = jo.getJSONObject("propertyInfo").getString("category");
                        product = jo.getJSONObject("propertyInfo").getString("product");
                        unit = jo.getJSONObject("propertyInfo").getString("unit");
                        area = jo.getJSONObject("propertyInfo").getString("landBuild");
                        priceInc = jo.getJSONObject("propertyInfo").getString("priceInc");

                        txtPropertyName.setText(propertys);
                        txtCategoryType.setText(category);
                        txtProduct.setText(product);
                        txtUnitNo.setText(unit);
                        txtArea.setText(Html.fromHtml(area));
                        txtEstimate.setText(priceInc);

                        txtPaymentTerms.setText(paymentTerm);
                        txtPriceInc.setText(priceIncVat);
                        txtDisconutPersen.setText(discPercent);
                        txtdiscount.setText(discAmt);
                        txtNetPrice.setText(netPrice);

                    } else {

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingReviewOrderActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingReviewOrderActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);
                params.put("unitRef", unitRef);
                params.put("termRef", termRef);
                params.put("termNo", termNo);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "paymentInfo");

    }

    public void saveOrderBooking() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.saveOrderBooking(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result booking review", response);
                    int status = jo.getInt("status");
                    message = jo.getString("message");
                    String bookingRef = jo.getString("bookingRef");
                    projectBookingRef = jo.getString("projectBookingRef");

                    if (status == 200) {
                        Intent intent = new Intent(BookingReviewOrderActivity.this, BookingBankTransferActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(CATEGORY_REF, categoryRef);
                        intent.putExtra(CLUSTER_REF, clusterRef);
                        intent.putExtra(PRODUCT_REF, productRef);
                        intent.putExtra(UNIT_REF, unitRef);
                        intent.putExtra(TERM_REF, termRef);
                        intent.putExtra(TERM_NO, termNo);
                        intent.putExtra(BOOKING_REF, bookingRef);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                        intent.putExtra("va1", va1);
                        intent.putExtra("va2", va2);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    } else {
                        Log.d("error", " " + message);

                    }

                    Toast.makeText(BookingReviewOrderActivity.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(BookingReviewOrderActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String dpMasterRefProjectCode = dbMasterRef + "#" + General.projectCode;
                params.put("dbMasterRef", dpMasterRefProjectCode);
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);
                params.put("agentMemberRef", memberRef);
                params.put("customerMemberRef", memberCustomerRef);
                params.put("unitRef", unitRef);
                params.put("termRef", termRef);
                params.put("termNo", termNo);
                params.put("customerName", fullname);
                params.put("customerNIK", ktpid);
                params.put("customerHP", mobile1);
                params.put("customerPHONE", phone1);
                params.put("customerEMAIL", email1);
                params.put("customerADDRESS", address);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "savebooking");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
