package com.nataproperty.solourbana.view.event.model;

/**
 * Created by UserModel on 7/28/2016.
 */
public class MyTicketModel {
    String eventScheduleRef,title,eventScheduleDate,countRsvp;

    public String getEventScheduleRef() {
        return eventScheduleRef;
    }

    public void setEventScheduleRef(String eventScheduleRef) {
        this.eventScheduleRef = eventScheduleRef;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEventScheduleDate() {
        return eventScheduleDate;
    }

    public void setEventScheduleDate(String eventScheduleDate) {
        this.eventScheduleDate = eventScheduleDate;
    }

    public String getCountRsvp() {
        return countRsvp;
    }

    public void setCountRsvp(String countRsvp) {
        this.countRsvp = countRsvp;
    }
}
