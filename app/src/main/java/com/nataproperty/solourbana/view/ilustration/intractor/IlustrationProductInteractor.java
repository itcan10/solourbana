package com.nataproperty.solourbana.view.ilustration.intractor;

import com.nataproperty.solourbana.view.ilustration.model.GenerateClusterInfoParam;

/**
 * Created by nata on 11/23/2016.
 */

public interface IlustrationProductInteractor {
    void getIlustration(String dbMasterRef,String projectRef,String clusterRef,String edtSearch,String productRef);
//    void getClusterInfo(String dbMasterRef,String projectRef,String clusterRef);
    void getClusterInfo(GenerateClusterInfoParam generateClusterInfoParam);

}
