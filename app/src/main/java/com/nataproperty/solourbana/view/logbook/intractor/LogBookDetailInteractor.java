package com.nataproperty.solourbana.view.logbook.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface LogBookDetailInteractor {
    void GetContactProjectTaskListSvc(String dbMasterRef, String projectRef, String customerRef);

}
