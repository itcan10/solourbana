package com.nataproperty.solourbana.view.mynup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mynup.model.PaidModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class PaidDetailAdapter extends BaseAdapter {
    private Context context;
    private List<PaidModel> list;
    private ListUnPaidHolder holder;

    public PaidDetailAdapter(Context context, List<PaidModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_paid_detail,null);
            holder = new ListUnPaidHolder();
            holder.nupNo = (TextView) convertView.findViewById(R.id.txt_nup_no);
            holder.orderStatus = (TextView) convertView.findViewById(R.id.txt_order_status);
            holder.costumerName = (TextView) convertView.findViewById(R.id.txt_costumer_name);

            convertView.setTag(holder);
        }else{
            holder = (ListUnPaidHolder) convertView.getTag();
        }

        PaidModel paidDetail = list.get(position);

        holder.nupNo.setText(paidDetail.getNupNo());
        holder.orderStatus.setText(paidDetail.getNupOrderStatus());
        holder.costumerName.setText(paidDetail.getCustomerName());

        Log.d("cek delail nup adapter",paidDetail.getNupOrderStatus());

        return convertView;
    }

    private class ListUnPaidHolder {
        TextView nupNo,orderStatus,costumerName;
    }
}
