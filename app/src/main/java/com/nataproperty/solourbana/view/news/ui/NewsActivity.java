package com.nataproperty.solourbana.view.news.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.EndlessScrollListener;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.news.adapter.NewsAdapter;
import com.nataproperty.solourbana.view.news.model.NewsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewsActivity extends AppCompatActivity {
    public static final String TAG = "NewsActivity";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String TITLE = "title";
    public static final String PUBLISH_DATE = "publishDate";
    public static final String DEVELOPER_NAME = "developerName";
    public static final String CONTENT = "content";
    public static final String CONTENT_REF = "contentRef";
    public static final String LINK_DETAIL = "linkDetail";

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<NewsModel> listNews = new ArrayList<NewsModel>();
    private NewsAdapter adapter;

    String dbMasterRef, projectRef, txtTitle, publishDate, developerName, content, contentRef, linkDetail, imgSetting;

    private final int AUTOLOAD_THRESHOLD = 4;
    private View mFooterView;
    private boolean mIsLoading = false;
    private boolean mMoreDataAvailable = true;
    private boolean mWasLoading = false;
    private Handler mHandler;
    private int max_item;
    private int addMoreItems = 4;

    Display display;
    private Runnable mAddItemsRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d("addMoreItems", "" + addMoreItems);
            adapter.addMoreItems(addMoreItems);
            mIsLoading = false;
        }
    };

    String keyword = "";
    private int page = 1;
    private int countTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_news));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Display display = getWindowManager().getDefaultDisplay();
        mHandler = new Handler();
        listView = (ListView) findViewById(R.id.list_news);

        adapter = new NewsAdapter(this, listNews, display);
        listView.setAdapter(adapter);
       /* mFooterView = LayoutInflater.from(NewsActivity.this).inflate(R.layout.loading, null);
        adapter.addMoreItems(4);
        mIsLoading = false;
        listView.addFooterView(mFooterView, null, false);*/

        requestList(page);

        /*listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!mIsLoading && mMoreDataAvailable) {
                    if (totalItemCount >= max_item) {
                        mMoreDataAvailable = false;
                        listView.removeFooterView(mFooterView);
                    } else if (totalItemCount - AUTOLOAD_THRESHOLD <= firstVisibleItem + visibleItemCount) {
                        mIsLoading = true;
                        mHandler.postDelayed(mAddItemsRunnable, 3000);

                    }
                }
            }
        });*/

        listView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                requestList(page);

            }
        });

    }

    public void requestList(final int page) {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListNewsPagingProjectSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    countTotal = jsonObject.getInt("totalPage");

                    if (page <= countTotal) {
                        JSONArray jsonArray = new JSONArray(jsonObject.getJSONArray("data").toString());
                        generateListNews(jsonArray);
                    }

                    Log.d(TAG, "count=" + page + " " + countTotal);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NewsActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NewsActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("keyword", keyword);
                params.put("pageNo", String.valueOf(page));
                params.put("projectCode", General.projectCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "news");

    }

    private void generateListNews(JSONArray response) {
        // max_item = response.length();
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                NewsModel News = new NewsModel();
                News.setDbMasterRef(jo.getString("dbMasterRef"));
                News.setProjectRef(jo.getString("projectRef"));
                News.setTitle(jo.getString("title"));
                News.setContentRef(jo.getString("contentRef"));
                News.setPublishDate(jo.getString("publishDate"));
                News.setDeveloperName(jo.getString("developerName"));
                News.setImageHeader(jo.getString("imageHeader"));
                News.setSynopsis(jo.getString("synopsis"));
                News.setContent(jo.getString("content"));
                News.setLinkDetail(jo.getString("linkDetail"));
                News.setImgSetting(jo.getString("imgSetting"));
                News.setLinkShare(jo.getString("linkShare"));
                listNews.add(News);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_search:
                Intent intent = new Intent(NewsActivity.this, NewsSearchActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
