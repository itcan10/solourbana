package com.nataproperty.solourbana.view.logbook.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface SearchLogBookInteractor {
    void GetContactBankSvc(String memberRef, String customerStatusRef, String keyword, String projectCode);

}
