package com.nataproperty.solourbana.view.ilustration.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyGridView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.project.adapter.ClusterAdapter;
import com.nataproperty.solourbana.view.project.model.ClusterModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nataproperty.solourbana.R.string.cluster;

/**
 * Created by User on 5/4/2016.
 */
public class    IlustrationClusterActivity extends AppCompatActivity {
    private List<ClusterModel> listCluster = new ArrayList<ClusterModel>();
    private ClusterAdapter adapter;
    ImageView imgLogo;
    Button btnNUP;
    TextView txtProjectName;
    long dbMasterRef;
    String projectRef, categoryRef, clusterRef, clusterDescription, projectName, isBooking,
            isShowAvailableUnit, imageLogo, isSales,imgCluster;
    MyGridView listView;
    Display display;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cluster);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getString(cluster));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        dbMasterRef = intent.getLongExtra(General.DBMASTER_REF, 0);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        categoryRef = String.valueOf(intent.getLongExtra(General.CATEGORY_REF,0));
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        isBooking = intent.getStringExtra(General.IS_BOOKING);
        imageLogo = intent.getStringExtra(General.IMAGE_LOGO);

        Log.d("Cek Cluster", dbMasterRef + " " + projectRef + " " + categoryRef);

        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);
        btnNUP.setVisibility(View.GONE);
        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this).load(imageLogo).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        listView = (MyGridView) findViewById(R.id.list_cluster);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clusterRef = String.valueOf(listCluster.get(position).getClusterRef());
                clusterDescription = listCluster.get(position).getClusterDescription();
                isShowAvailableUnit = listCluster.get(position).getIsShowAvailableUnit();
                isSales = listCluster.get(position).getIsSales();
                imgCluster =  listCluster.get(position).getImage();

                if (isSales.equals("1")){
                    Intent intentProduct = new Intent(IlustrationClusterActivity.this, IlustrationProductActivity.class);
                    intentProduct.putExtra(General.PROJECT_REF, projectRef);
                    intentProduct.putExtra(General.DBMASTER_REF, dbMasterRef);
                    intentProduct.putExtra(General.CLUSTER_REF, clusterRef);
                    intentProduct.putExtra(General.CATEGORY_REF, categoryRef);
                    intentProduct.putExtra(General.PROJECT_NAME, projectName);
                    intentProduct.putExtra(General.PRODUCT_REF, "");
                    intentProduct.putExtra(General.IS_BOOKING, isBooking);
                    intentProduct.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentProduct.putExtra(General.IMAGE_LOGO, imageLogo);
                    intentProduct.putExtra(General.CLUSTER_NAME, clusterDescription);
                    intentProduct.putExtra("imageCluster", imgCluster);

                    startActivity(intentProduct);
                } else {
                    Snackbar snackbar = Snackbar
                            .make(linearLayout, getString(R.string.comingsoon), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }

            }
        });

        requestCluster();
    }

    public void requestCluster() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCluster(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCluster(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        LoadingBar.stopLoader();
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, getString(R.string.offline), Snackbar.LENGTH_INDEFINITE)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivity(new Intent(IlustrationClusterActivity.this, LaunchActivity.class));
                                        finish();
                                    }
                                });

                        snackbar.setActionTextColor(Color.RED);
                        snackbar.show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("categoryRef", categoryRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "cluster");

    }

    private void generateListCluster(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ClusterModel cluster = new ClusterModel();
                cluster.setDbMasterRef(jo.getString("dbMasterRef"));
                cluster.setProjectRef(jo.getString("projectRef"));
                cluster.setCategoryRef(jo.getString("categoryRef"));
                cluster.setClusterRef(jo.getLong("clusterRef"));
                cluster.setClusterDescription(jo.getString("clusterDescription"));
                cluster.setIsShowAvailableUnit(jo.getString("isShowAvailableUnit"));
                cluster.setImage(jo.getString("image"));
                cluster.setIsSales(jo.getString("isSales"));
                listCluster.add(cluster);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        initListView();
        adapter.notifyDataSetChanged();
    }

    private void initListView() {
        adapter = new ClusterAdapter(this, listCluster, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationClusterActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
