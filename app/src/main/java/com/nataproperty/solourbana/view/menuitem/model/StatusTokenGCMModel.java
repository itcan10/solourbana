package com.nataproperty.solourbana.view.menuitem.model;

/**
 * Created by nata on 11/23/2016.
 */
public class StatusTokenGCMModel {
    int status;
    String message;
    String versionApp;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }
}
