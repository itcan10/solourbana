package com.nataproperty.solourbana.view.booking.presenter;

import com.google.android.gms.fitness.data.Subscription;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.booking.intractor.PresenterBookingBankTransferInteractor;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.booking.model.PaymentModel;
import com.nataproperty.solourbana.view.booking.model.SaveBookingModel;
import com.nataproperty.solourbana.view.booking.ui.BookingBankTransferActivity;
import com.nataproperty.solourbana.view.nup.model.AccountBankModel;
import com.nataproperty.solourbana.view.nup.model.BankModel;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class BookingBookingBankTransferPresenter implements PresenterBookingBankTransferInteractor {
    private BookingBankTransferActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public BookingBookingBankTransferPresenter(BookingBankTransferActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getIlustrationPaymentSvc(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<PaymentModel> call = service.getAPI().getIlustrationPaymentSvc(dbMasterRef,projectRef, clusterRef,  productRef,  unitRef,  termRef,  termNo);
        call.enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
                view.showPaymentModelResults(response);
            }

            @Override
            public void onFailure(Call<PaymentModel> call, Throwable t) {
                view.showPaymentModelFailure(t);

            }


        });
    }

    @Override
    public void getProjectAccountBank(String dbMasterRef, String projectRef) {
        Call<ArrayList<AccountBankModel>> call = service.getAPI().getProjectAccountBank(dbMasterRef,projectRef);
        call.enqueue(new Callback<ArrayList<AccountBankModel>>() {
            @Override
            public void onResponse(Call<ArrayList<AccountBankModel>> call, Response<ArrayList<AccountBankModel>> response) {
                view.showListProjectAccountBankResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<AccountBankModel>> call, Throwable t) {
                view.showListProjectAccountBankFailure(t);

            }


        });
    }

    @Override
    public void getListBankSvc() {
        Call<ArrayList<BankModel>> call = service.getAPI().getListBankSvc();
        call.enqueue(new Callback<ArrayList<BankModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BankModel>> call, Response<ArrayList<BankModel>> response) {
                view.showListBankResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<BankModel>> call, Throwable t) {
                view.showListBankFailure(t);

            }


        });
    }

    @Override
    public void getBookingDetailSvc(String dbMasterRef, String projectRef, String bookingRef) {
        Call<DetailBooking> call = service.getAPI().getBookingDetailSvc(dbMasterRef,projectRef, bookingRef);
        call.enqueue(new Callback<DetailBooking>() {
            @Override
            public void onResponse(Call<DetailBooking> call, Response<DetailBooking> response) {
                view.showBookingInfoResults(response);
            }

            @Override
            public void onFailure(Call<DetailBooking> call, Throwable t) {
                view.showBookingInfoFailure(t);

            }


        });
    }

    @Override
    public void savePaymentBookingBankTransferSvc(String jsonIn) {
        Call<SaveBookingModel> call = service.getAPI().savePaymentBookingVASvc(jsonIn);
//        Call<SaveBookingModel> call = service.getAPI().savePaymentBookingBankTransferSvc(fields);
        call.enqueue(new Callback<SaveBookingModel>() {
            @Override
            public void onResponse(Call<SaveBookingModel> call, Response<SaveBookingModel> response) {
                view.showSaveBookingResults(response);
            }

            @Override
            public void onFailure(Call<SaveBookingModel> call, Throwable t) {
                view.showSaveBookingFailure(t);

            }


        });
    }

}
