package com.nataproperty.solourbana.view.project.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.view.project.model.ProductDetailModel;
import com.nataproperty.solourbana.view.project.ui.ProductDetailImageActivity;

import java.util.List;


/**
 * Created by User on 5/11/2016.
 */
public class ProductDetailAdapter extends PagerAdapter {
    Context context;
    private List<ProductDetailModel> list;

    public ProductDetailAdapter(Context context, List<ProductDetailModel> list) {
        this.context = context;
        this.list = list;
    }

    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_product_image, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.imageView);
        TextView title = (TextView) viewItem.findViewById(R.id.title);

        ProductDetailModel productDetailModel = list.get(position);
        Glide.with(context)
                    .load(WebService.getProductImageDetail() + productDetailModel.getQuerystring()).into(imageView);

        if (!productDetailModel.getTitle().trim().equals("")) {
            title.setVisibility(View.VISIBLE);
            title.setText(productDetailModel.getTitle());
        } else {
            title.setVisibility(View.GONE);
        }

        imageView.setOnClickListener(new OnImageClickListener(position));

        ((ViewPager) container).addView(viewItem);

        return viewItem;
    }

    class OnImageClickListener implements View.OnClickListener {

        int position;

        // constructor
        public OnImageClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
            Intent i = new Intent(context, ProductDetailImageActivity.class);
            i.putExtra(General.PROJECT_REF, list.get(position).getProjectRef());
            i.putExtra(General.DBMASTER_REF, list.get(position).getDbMasterRef());
            i.putExtra(General.PRODUCT_REF, list.get(position).getProductRef());
            i.putExtra(General.POSISION, position);
            context.startActivity(i);
        }

    }

    public int getCount() {
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {

        return view == ((View) object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
