package com.nataproperty.solourbana.view.menuitem.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.menuitem.intractor.PresenterNotificationInteractor;
import com.nataproperty.solourbana.view.menuitem.model.NotificationModel;
import com.nataproperty.solourbana.view.menuitem.ui.NotificationActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NotificationPresenter implements PresenterNotificationInteractor {
    private NotificationActivity view;
    private ServiceRetrofitProject service;

    public NotificationPresenter(NotificationActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListNotificationSSTSvc(String memberRef, String projectCode) {
        Call<ArrayList<NotificationModel>> call = service.getAPI().getListNotificationSSTSvc(memberRef,projectCode);
        call.enqueue(new Callback<ArrayList<NotificationModel>>() {
            @Override
            public void onResponse(Call<ArrayList<NotificationModel>> call, Response<ArrayList<NotificationModel>> response) {
                view.showListNotificationResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<NotificationModel>> call, Throwable t) {
                view.showListNotificationFailure(t);

            }


        });
    }
}
