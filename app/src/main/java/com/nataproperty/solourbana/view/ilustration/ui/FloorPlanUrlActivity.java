package com.nataproperty.solourbana.view.ilustration.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.SessionManager;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.ilustration.model.FloorPlanParam;
import com.nataproperty.solourbana.view.ilustration.model.FloorPlanRespone;
import com.nataproperty.solourbana.view.ilustration.presenter.FloorPlanPresenter;

import retrofit2.Response;

import static com.nataproperty.solourbana.config.General.IS_MEMBER_REF;
import static com.nataproperty.solourbana.config.General.VIEW_FLOOR_PLAN_URL;


/**
 * Created by User on 4/21/2016.
 */
public class FloorPlanUrlActivity extends AppCompatActivity {
    public static final String TAG = "FloorPlanUrlActivity";
    private WebView webView;
    private String viewFloorUrl;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    private ProgressBar progressBar;
    private ServiceRetrofit service;
    FloorPlanPresenter floorPlanPresenter;
    SessionManager mSessionManager;
    String dbMasterRef, projectRef, categoryRef, clusterRef, projectName, isBooking, productRef, unitRef, productRefIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan_url);
        initWidget();

        service = ((BaseApplication) getApplication()).getNetworkService();
        floorPlanPresenter = new FloorPlanPresenter(this, service);

        mSessionManager = new SessionManager(this);

        Intent intent = getIntent();
        viewFloorUrl = intent.getStringExtra(VIEW_FLOOR_PLAN_URL);
        dbMasterRef = intent.getStringExtra(General.DBMASTER_REF);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        categoryRef = intent.getStringExtra(General.CATEGORY_REF);
        clusterRef = intent.getStringExtra(General.CLUSTER_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        isBooking = intent.getStringExtra(General.IS_BOOKING);
        productRefIntent = intent.getStringExtra(General.PRODUCT_REF);

        Log.d(TAG, viewFloorUrl + " ");

        title.setText(R.string.floor_plan);

        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        webView.loadUrl(viewFloorUrl);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progress1);
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void tagUnitClicked(String clusterRef, String blockName, String unitRef, String unitName, String productRef) {
            if (productRefIntent.equals("")) {
                FloorPlanParam param = new FloorPlanParam();
                param.setMemberRef(mSessionManager.getStringFromSP(IS_MEMBER_REF));
                param.setDbMasterRef(dbMasterRef);
                param.setProjectRef(projectRef);
                param.setClusterRef(clusterRef);
                param.setBlockName(blockName);
                param.setUnitRef(unitRef);
                param.setUnitName(unitName);

                floorPlanPresenter.getUnitStatusSVC(Utils.modelToJson(param), unitRef, productRef);
            } else {
                if (productRefIntent.equals(productRef)){
                    FloorPlanParam param = new FloorPlanParam();
                    param.setMemberRef(mSessionManager.getStringFromSP(IS_MEMBER_REF));
                    param.setDbMasterRef(dbMasterRef);
                    param.setProjectRef(projectRef);
                    param.setClusterRef(clusterRef);
                    param.setBlockName(blockName);
                    param.setUnitRef(unitRef);
                    param.setUnitName(unitName);

                    floorPlanPresenter.getUnitStatusSVC(Utils.modelToJson(param), unitRef, productRef);
                }
            }
        }

    }

    public void showResults(Response<FloorPlanRespone> response, String unitRef, String productRef) {
        Log.d(TAG, "getUnitStatus " + response.body().getData().getUnitStatus());

        if (response.body().getData().getUnitStatus().equals("A")) {
            Intent intent = new Intent(this, IlustrationPaymentTermActivity.class);
            intent.putExtra(General.DBMASTER_REF, dbMasterRef);
            intent.putExtra(General.PROJECT_REF, projectRef);
            intent.putExtra(General.CLUSTER_REF, clusterRef);
            intent.putExtra(General.CATEGORY_REF, categoryRef);
            intent.putExtra(General.PROJECT_NAME, projectName);
            intent.putExtra(General.PRODUCT_REF, productRef);
            intent.putExtra(General.UNIT_REF, unitRef);
            intent.putExtra(General.IS_BOOKING, isBooking);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.unit_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    public void showFailure(Throwable t) {

    }


    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_close, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
