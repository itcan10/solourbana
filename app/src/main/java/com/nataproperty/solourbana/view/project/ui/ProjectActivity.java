package com.nataproperty.solourbana.view.project.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.project.adapter.LocationAdapter;
import com.nataproperty.solourbana.view.project.adapter.RVProjectAdapter;
import com.nataproperty.solourbana.view.project.model.LocationModel;
import com.nataproperty.solourbana.view.project.model.ProjectModelNew;
import com.nataproperty.solourbana.view.project.presenter.ProjectPresenter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by UserModel on 4/21/2016.
 */
public class ProjectActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    private static final String TAG = ProjectActivity.class.getSimpleName();
    private List<ProjectModelNew> listProject = new ArrayList<>();
    private RVProjectAdapter adapter;
    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private ProjectPresenter presenter;
    ProgressDialog progressDialog;
    Display display;
    RecyclerView rv_myproject;
    SharedPreferences sharedPreferences;
    private boolean state;
    String memberRef;
    private List<LocationModel> listLocation = new ArrayList<>();
    private LocationAdapter locationAdapter;
    private String locationRef;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    private CardView cardViewSpinner;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Spinner spinner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new ProjectPresenter(this, service);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        String email = sharedPreferences.getString("isEmail", null);
        String name = sharedPreferences.getString("isName", null);
        requestLocation();
        retry.setOnClickListener(this);
        display = getWindowManager().getDefaultDisplay();
    }

    private void initWidget() {
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        cardViewSpinner = (CardView) findViewById(R.id.cv_spiner);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("All Project");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv_myproject = (RecyclerView) findViewById(R.id.rv_myproject);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_myproject.setLayoutManager(llm);
    }

    public void requestLocation() {
        presenter.getLocation(memberRef, General.projectCode);
    }

    public void showLocationResults(retrofit2.Response<List<LocationModel>> response) {
        snackBarBuatan.setVisibility(View.GONE);
        cardViewSpinner.setVisibility(View.VISIBLE);
        listLocation = response.body();
        spinnerLoc();
    }

    public void showLocationFailure(Throwable t) {
        snackBarBuatan.setVisibility(View.VISIBLE);
        cardViewSpinner.setVisibility(View.VISIBLE);

    }

    private void spinnerLoc() {
        spinner = (Spinner) findViewById(R.id.spinner_nav);
        locationAdapter = new LocationAdapter(this, listLocation);
        spinner.setAdapter(locationAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                locationRef = String.valueOf(listLocation.get(position).getLocationRef());
                listProject.clear();
                requestProject();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void requestProject() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListProject(memberRef, locationRef, General.projectCode);
    }

    public void showListProjectResults(retrofit2.Response<List<ProjectModelNew>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful())
            listProject = response.body();
        initializeAdapter();
    }

    public void showListProjectFailure(Throwable t) {
        progressDialog.dismiss();

    }

    private void initializeAdapter() {
        adapter = new RVProjectAdapter(this, listProject, display);
        rv_myproject.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(ProjectActivity.this, ProjectSearchActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_retry:
                startActivity(new Intent(this, LaunchActivity.class));
                break;


        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                ProjectActivity.this.recreate();
            }
        }
    }

}
