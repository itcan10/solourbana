package com.nataproperty.solourbana.view.profile.model;

/**
 * Created by UserModel on 4/25/2016.
 */

public class SubLocationModel {
    long id;
    String subLocationRef;
    String subLocationName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSubLocationRef() {
        return subLocationRef;
    }

    public void setSubLocationRef(String subLocationRef) {
        this.subLocationRef = subLocationRef;
    }

    public String getSubLocationName() {
        return subLocationName;
    }

    public void setSubLocationName(String subLocationName) {
        this.subLocationName = subLocationName;
    }
}
