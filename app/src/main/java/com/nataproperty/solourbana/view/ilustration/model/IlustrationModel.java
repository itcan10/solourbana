package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by UserModel on 5/13/2016.
 */

public class IlustrationModel {
    long unitRef;
    String unitNo;
    String area;
    String priceInc;
    String salesDisc;
    String netPrice;
    String dbMasterRef;
    String projectRef;
    String categoryRef;
    String clusterRef;
    String productRef;
    String productName;
    String landBuild;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getUnitRef() {
        return unitRef;
    }

    public void setUnitRef(long unitRef) {
        this.unitRef = unitRef;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPriceInc() {
        return priceInc;
    }

    public void setPriceInc(String priceInc) {
        this.priceInc = priceInc;
    }

    public String getSalesDisc() {
        return salesDisc;
    }

    public void setSalesDisc(String salesDisc) {
        this.salesDisc = salesDisc;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public String getLandBuild() {
        return landBuild;
    }

    public void setLandBuild(String landBuild) {
        this.landBuild = landBuild;
    }
}
