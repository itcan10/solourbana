package com.nataproperty.solourbana.view.event.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.event.model.EventDetailModel;

import java.util.List;

/**
 * Created by UserModel on 5/11/2016.
 */
public class EventDetailAdapter extends PagerAdapter {
    Context context;
    private List<EventDetailModel> list;

    public EventDetailAdapter(Context context, List<EventDetailModel> list){
        this.context = context;
        this.list = list;
    }

    public Object instantiateItem(ViewGroup container, final int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_event_detail, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.img_event_detail);
        //imageView.setImageResource(imageId[position]);
        EventDetailModel image = list.get(position);

        Glide.with(context)
                .load(image.getImageSlider()).into(imageView);


        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }


    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}
