package com.nataproperty.solourbana.view.logbook.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface AddLogBookProjectInteractor {
    void GetLookupContactProjectSvc(String dbMasterRef, String projectRef, String memberRef);
    void getCountryListSvc();
    void getProvinceListSvc(String countryCode);
    void getCityListSvc(String countryCode, String provinceCode);
    void insertContactBankProjectSvc(Map<String, String> fields) ;
}
