package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.ui.SectionProfileActivity;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/21/2016.
 */
public class SelectorAdapter extends BaseAdapter {
    //String[] nameList;
    Context context;
    private ArrayList<String> list;
    private static LayoutInflater inflater = null;

    public SelectorAdapter(SectionProfileActivity selectionActivity, ArrayList<String> list) {
        context = selectionActivity;
        //nameList = nameListMenu;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        //return nameList.length;
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.item_list_selection, null);
        holder.nameListMenu=(TextView) rowView.findViewById(R.id.list_selection);

        //holder.nameListMenu.setText(nameList[position]);
        holder.nameListMenu.setText(list.get(position));

        return rowView;
    }

    public class Holder
    {
        TextView nameListMenu;

    }
}