package com.nataproperty.solourbana.view.booking.ui;

import android.support.v7.app.AppCompatActivity;

//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.util.EntityUtils;

/**
 * Created by UserModel on 5/21/2016.
 */
public class BookingCreditCardActivity extends AppCompatActivity {
    /*public static final String TAG = "BookingCreditCard" ;

    public static final String PREF_NAME = "pref" ;

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String STATUS_PAYMENT = "statusPayment";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    private List<MouthModel> listMounth = new ArrayList<MouthModel>();
    private MounthAdapter adapterMounth;
    private List<YearModel> listYear = new ArrayList<YearModel>();
    private YearAdapter adapterYear;

    SharedPreferences sharedPreferences;

    //logo
    ImageView imgLogo;

    Spinner spnMounth,spnYear;

    ImageView imgProject;
    TextView txtProjectName,txtCostumerName,txtOrderCode,txtPriortyPass,txtAmount,txtEmail,txtAmountTotal;
    EditText editCardNumber,editSecurityCard;
    TextView txtPropertyName,txtCategoryType,txtProduct,txtUnitNo,txtArea,txtEstimate;
    TextView txtPaymentTerms,txtPriceInc,txtDisconutPersen,txtNetPrice,txtdiscount;
    private TextView txtBookingFee,txtBookingInfo;

    Button btnPayment;

    String costumerName,orderCode,priorityPass,amount,totalAmount;
    String bookingFee;
    String memberRef,dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo,bookingRef,projectName;
    String monthRef,year;
    String propertys,category,product,unit,area,priceInc;
    String paymentTerm,priceIncVat,discPercent,discAmt,netPrice,termCondition;
    String projectBookingRef;

    //veritrans
    AlertDialog dialog3ds;
    ProgressDialog sendServerProgress;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font,fontLight;
    RelativeLayout rPage;
    Display display;
    Integer width;
    Double result;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private BookingCCPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_credit_card);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new BookingCCPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        Log.d("cek booking credit",bookingRef+"-"+dbMasterRef+"-"+projectRef+"-"+clusterRef+"-"+productRef+"-"+
                unitRef+"-"+termRef+"-"+termNo);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage()+dbMasterRef+
                        "&pr="+projectRef).into(imgLogo);
        btnPayment.setOnClickListener(this);
        requestMounth();
        requestYear();
        adapterMounth = new MounthAdapter(this,listMounth);
        spnMounth.setAdapter(adapterMounth);
        spnMounth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthRef = listMounth.get(position).getMounthRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        adapterYear = new YearAdapter(this,listYear);
        spnYear.setAdapter(adapterYear);
        spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                year = listYear.get(position).getYear();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Credit Card");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        rPage = (RelativeLayout)findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        result = width/1.233333333333333;
        Log.d("screen width", result.toString()+"--"+Math.round(result));
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount =(TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);
        txtBookingFee = (TextView) findViewById(R.id.txt_booking_fee);
        txtBookingInfo = (TextView) findViewById(R.id.txt_booking_info);
        editCardNumber = (EditText) findViewById(R.id.edit_card_number);
        editSecurityCard = (EditText) findViewById(R.id.edit_security_card);
        spnMounth = (Spinner) findViewById(R.id.list_mounth);
        spnYear =  (Spinner) findViewById(R.id.list_year);
        btnPayment = (Button) findViewById(R.id.btn_payment);
        btnPayment.setTypeface(font);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPayment();
        requestMyBookingDetail();

        txtPropertyName.setText(propertys);
        txtCategoryType.setText(category);
        txtProduct.setText(product);
        txtUnitNo.setText(unit);
        txtArea.setText(Html.fromHtml(area));
        txtEstimate.setText(priceInc);

        txtPaymentTerms.setText(paymentTerm);
        txtPriceInc.setText(priceIncVat);
        txtDisconutPersen.setText(discPercent);
        txtdiscount.setText(discAmt);
        txtNetPrice.setText(netPrice);
    }

    private void requestMyBookingDetail() {
        sendServerProgress = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getBookingDetail(dbMasterRef.toString(),projectRef.toString(),projectBookingRef.toString());
    }

    private void requestPayment(){
        sendServerProgress = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getPayment(dbMasterRef.toString(),projectRef.toString(),clusterRef.toString(),productRef.toString(),unitRef.toString(),termRef.toString(),termNo.toString());
    }

    private void requestOrderInfo (){
        String url = WebService.getOrderInfo();
        String urlPostParameter ="&dbMasterRef="+dbMasterRef.toString()+
                "&projectRef="+projectRef.toString()+
                "&bookingRef="+bookingRef.toString();                ;
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d("result nup review",result);
            int status = jo.getInt("status");
            String message = jo.getString("message");

            if (status==200){
                projectName = jo.getJSONObject("data").getString("projectName");
                orderCode = jo.getJSONObject("data").getString("nupOrderCode");
                costumerName = jo.getJSONObject("data").getString("customerName");
                priorityPass = jo.getJSONObject("data").getString("quantity");
                amount = jo.getJSONObject("data").getString("amount");
                totalAmount = jo.getJSONObject("data").getString("totalAmount");

                txtProjectName.setText(projectName);
                txtCostumerName.setText(costumerName);
                txtOrderCode.setText(orderCode);
                txtPriortyPass.setText(priorityPass);
                DecimalFormat decimalFormat= new DecimalFormat("###,##0");
                txtAmount.setText("IDR "+ String.valueOf(decimalFormat.format(Double.parseDouble(amount))));
                txtAmountTotal.setText("IDR "+ String.valueOf(decimalFormat.format(Double.parseDouble(totalAmount))));

                Glide.with(this)
                        .load(WebService.getProjectImage()+dbMasterRef+
                                "&pr="+projectRef).into(imgProject);

            }else {
                Log.d("error"," "+message);

            }

        }catch (JSONException e){

        }

    }

    private void requestMounth (){
        sendServerProgress = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getMontList("");
    }


    private void requestYear (){
        sendServerProgress = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getYearsList("");
       }


    private void paymentCreditcard(){
        //set environment
//        VTConfig.VT_IsProduction = WebService.VT_IsProduction;
//        //set client key
//        VTConfig.CLIENT_KEY = WebService.CLIENT_KEY;
//
//        VTDirect vtDirect = new VTDirect();
//        VTCardDetails cardDetails = null;
//        cardDetails = CardFactory(true);
//        vtDirect.setCard_details(cardDetails);
//
//        final ProgressDialog loadingDialog = ProgressDialog.show(BookingCreditCardActivity.this,"","Loading, Please Wait...",true);
//        vtDirect.getToken(new ITokenCallback() {
//            @Override
//            public void onSuccess(VTToken vtToken) {
//                loadingDialog.cancel();
//                if(vtToken.getRedirect_url() != null){
//                    //show it to user using webview
//                    Log.d("VtLog",vtToken.getToken_id());
//
//                    CustomWebView webView = new CustomWebView(BookingCreditCardActivity.this);
//                    webView.getSettings().setJavaScriptEnabled(true);
//                    webView.setOnTouchListener(new View.OnTouchListener() {
//                        @Override
//                        public boolean onTouch(View v, MotionEvent event) {
//                            switch (event.getAction()) {
//                                case MotionEvent.ACTION_DOWN:
//                                case MotionEvent.ACTION_UP:
//                                    if (!v.hasFocus()) {
//                                        v.requestFocus();
//                                    }
//                                    break;
//                            }
//                            return false;
//                        }
//                    });
//                    webView.setWebChromeClient(new WebChromeClient());
//                    webView.setWebViewClient(new VtWebViewClient(vtToken.getToken_id(),dbMasterRef,projectRef,memberRef,
//                            bookingRef, termRef));
//
//                    Log.d("cek param booking"," "+bookingFee);
//                    webView.loadUrl(vtToken.getRedirect_url());
//
//                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(BookingCreditCardActivity.this);
//                    dialog3ds = alertBuilder.create();
//
//                    dialog3ds.setTitle("3D Secure Veritrans");
//                    dialog3ds.setView(webView);
//                    webView.requestFocus(View.FOCUS_DOWN);
//                    alertBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.dismiss();
//                        }
//                    });
//
//                    dialog3ds.show();
//
//                }
//            }
//
//            @Override
//            public void onError(Exception e) {
//                loadingDialog.cancel();
//                Toast.makeText(BookingCreditCardActivity.this,e.getMessage(),Toast.LENGTH_SHORT);
//            }
//        });

    }

//    private VTCardDetails CardFactory(boolean secure){
//        VTCardDetails cardDetails = new VTCardDetails();
//        cardDetails.setCard_number(editCardNumber.getText().toString());
//        cardDetails.setCard_cvv(editSecurityCard.getText().toString());
//        cardDetails.setCard_exp_month(Integer.parseInt(monthRef));
//        cardDetails.setCard_exp_year(Integer.parseInt(year));
//        cardDetails.setSecure(secure);
//        cardDetails.setGross_amount(Integer.parseInt(bookingFee)+"");
//        return cardDetails;
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_payment:
                String cekCardNumber = editCardNumber.getText().toString().toString();
                String cekSecurityCard = editSecurityCard.getText().toString().toString();
                String cekMount = monthRef;
                String cekYear = year;

                if (!cekCardNumber.isEmpty() && editCardNumber.length()==16 && !cekSecurityCard.isEmpty() && !cekMount.equals("0") &&
                        !cekYear.equals("Not Defined")){

                    paymentCreditcard();

                }else {
                    if (editCardNumber.getText().toString().toString().equals("")){
                        editCardNumber.setError("Credit card is empty");
                    }else if(editCardNumber.length()!=16){
                        editCardNumber.setError("Credit card not valid");
                    }else {
                        editCardNumber.setError(null);
                    }

                    if (cekSecurityCard.isEmpty()){
                        editSecurityCard.setError("Security card is empty");
                    }else {
                        editSecurityCard.setError(null);
                    }

                    if (cekMount.equals("0")){
                        Toast.makeText(BookingCreditCardActivity.this,"Select month expired date",Toast.LENGTH_SHORT).show();
                    }else if (cekYear.equals("Not Defined")){
                        Toast.makeText(BookingCreditCardActivity.this,"Select year expired date",Toast.LENGTH_SHORT).show();
                    }
                }
                break;
//            case R.id.image_all_project:
//                startActivity(new Intent(MainMenuActivity.this, ProjectActivity.class));
//                break;
        }
    }

    public void showMonthResults(Response<List<MouthModel>> response) {
        listMounth = response.body();
        sendServerProgress.dismiss();

    }

    public void showMonthFailure(Throwable t) {
        sendServerProgress.dismiss();

    }

    public void showYearsResults(Response<List<YearModel>> response) {
        sendServerProgress.dismiss();
        listYear = response.body();

    }

    public void showYearsFailure(Throwable t) {
        sendServerProgress.dismiss();

    }

    public void showPaymentResults(Response<PaymentCCStatusModel> response) {
        sendServerProgress.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        if(status == 200){
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();
            propertys = response.body().getPropertyInfo().getPropertys();
            category = response.body().getPropertyInfo().getCategory();
            product = response.body().getPropertyInfo().getProduct();
            unit = response.body().getPropertyInfo().getUnit();
            area = response.body().getPropertyInfo().getArea();
            priceInc = response.body().getPropertyInfo().getPriceInc();
            bookingFee = response.body().getPropertyInfo().getBookingFee();
            DecimalFormat decimalFormat= new DecimalFormat("###,##0");
            txtBookingFee.setText("IDR "+ String.valueOf(decimalFormat.format(Double.parseDouble(bookingFee))));

        }else {

            Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();
        }

    }

    public void showPaymentFailure(Throwable t) {
        sendServerProgress.dismiss();

    }

    public void showBookingDtlResults(Response<BookingDetailStatusModel> response) {
        sendServerProgress.dismiss();
        int status = response.body().getStatus();
        if(status == 200){
            String bookDate = response.body().getBookingInfo().getBookDate();
            String bookHour = response.body().getBookingInfo().getBookHour();
            txtBookingInfo.setText("Lakukan pembayaran booking fee dalam waktu 3 jam atau sebelum pukul " + bookHour + ". " +
                    "jika tidak ada konfirmasi pembayaran maka unit booking akan kembali available");
        } else {
            Log.d(TAG, "get error");
        }


    }

    public void showBookingDtlFailure(Throwable t) {
        sendServerProgress.dismiss();
    }

    private class VtWebViewClient extends WebViewClient {
        String token;
        String dbMasterRef;
        String projectRef;
        String memberRef;
        String bookingRef;
        String termRef;

        public VtWebViewClient(String token, String dbMasterRef,String projectRef,String memberRef,
                               String bookingRef,String termRef){
            this.token = token;
            this.dbMasterRef = dbMasterRef;
            this.projectRef = projectRef;
            this.memberRef = memberRef;
            this.bookingRef = bookingRef;
            this.termRef = termRef;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d("VtUrl",url);
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            Log.d("VtLog", url);

            if (url.startsWith(Constants.getPaymentApiUrl() + "/callback/")) {
                //send token to server
//                SendTokenAsync sendTokenAsync = new SendTokenAsync();
//                sendTokenAsync.execute(token,dbMasterRef,projectRef,memberRef,bookingRef,termRef);
//                //close web dialog
                dialog3ds.dismiss();
                //show loading dialog
                sendServerProgress = ProgressDialog.show(BookingCreditCardActivity.this,"","Sending Data to Server. Please Wait...",true);

            } else {
                if (url.startsWith(Constants.getPaymentApiUrl() + "/redirect/") || url.contains("3dsecure")) {
                *//* Do nothing *//*
                } else {
                    if (dialog3ds != null) {
                        dialog3ds.dismiss();
                    }
                }
            }
        }
    }
//
//
//    private class SendTokenAsync extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... params) {
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpPost httpPost = new HttpPost(WebService.getVeritransBooking());
//            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
//            Log.d("cek param",params[0]+"-"+params[1]+"-"+params[2]+"-"+params[3]+"-"+params[4]+"-"+params[5]);
//            try {
//                nameValuePairs.add(new BasicNameValuePair("tokenid", URLEncoder.encode(params[0],"UTF-8")));
//                nameValuePairs.add(new BasicNameValuePair("dbMasterRef",params[1]));
//                nameValuePairs.add(new BasicNameValuePair("projectRef",params[2]));
//                nameValuePairs.add(new BasicNameValuePair("memberRef",params[3]));
//                nameValuePairs.add(new BasicNameValuePair("bookingRef",params[4]));
//                nameValuePairs.add(new BasicNameValuePair("termRef",params[5]));
//                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//                HttpResponse response = httpClient.execute(httpPost);
//                String jsonResponse = EntityUtils.toString(response.getEntity()).toString();
//                Log.d("VtLog Json", jsonResponse);
//                return jsonResponse;
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            return "";
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            //dismiss dialog
//            if(dialog3ds != null){
//                dialog3ds.dismiss();
//            }
//            if(sendServerProgress.isShowing()){
//                sendServerProgress.dismiss();
//            }
//
//            try{
//                VTRestResponse response = new Gson().fromJson(s,VTRestResponse.class);
//
//                if(response.status.equalsIgnoreCase("success")){
//                    Toast.makeText(BookingCreditCardActivity.this,"Success To Add payment: "+response.data.order_id,Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(BookingCreditCardActivity.this,BookingFinishActivity.class);
//                    intent.putExtra(DBMASTER_REF, dbMasterRef);
//                    intent.putExtra(PROJECT_REF, projectRef);
//                    intent.putExtra(CLUSTER_REF,clusterRef);
//                    intent.putExtra(PRODUCT_REF,productRef);
//                    intent.putExtra(UNIT_REF,unitRef);
//                    intent.putExtra(TERM_REF,termRef);
//                    intent.putExtra(TERM_NO,termNo);
//
//                    intent.putExtra(BOOKING_REF, bookingRef);
//                    intent.putExtra(PROJECT_NAME,projectName);
//                    intent.putExtra(STATUS_PAYMENT,"1");
//
//                    startActivity(intent);
//                }else{
//                    Toast.makeText(BookingCreditCardActivity.this,"Failed to Pay",Toast.LENGTH_SHORT).show();
//                }
//
//            }catch (Exception ex){
//                Toast.makeText(BookingCreditCardActivity.this,"Failed to Pay ex",Toast.LENGTH_LONG).show();
//                Log.d("respon",ex.getMessage());
//            }
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }*/

}
