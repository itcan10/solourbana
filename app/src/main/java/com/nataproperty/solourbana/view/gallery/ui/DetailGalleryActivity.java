package com.nataproperty.solourbana.view.gallery.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.HackyViewPager;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.view.gallery.adapter.ProjectDetailImageAdapter;
import com.nataproperty.solourbana.view.gallery.model.ProjectGalleryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

public class DetailGalleryActivity extends AppCompatActivity {
    public static final String IMAGE_GALLERY_REF = "imageGalleryRef";
    public static final String POSISION = "position";
    public static final String GROUP_GALLERY_REF = "groupGalleryRef";
    public static final String PROJECT_NAME = "projectName";
    HackyViewPager viewPager;
    private List<ProjectGalleryModel> listProjectGallery = new ArrayList<ProjectGalleryModel>();
    private ProjectDetailImageAdapter adapter;
    String imageGalleryRef, groupGalleryRef, projectName;
    int position;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gallery);

        Intent intent = getIntent();
        imageGalleryRef = intent.getStringExtra(IMAGE_GALLERY_REF);
        position = intent.getIntExtra(POSISION, 0);
        groupGalleryRef = intent.getStringExtra(GROUP_GALLERY_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);

        listProjectGallery.clear();
        requestProjectGallery();

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (HackyViewPager) findViewById(R.id.pager);

        adapter = new ProjectDetailImageAdapter(this, listProjectGallery);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());

        Log.d("position", "" + String.valueOf(position));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 0.8;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        viewPager.setLayoutParams(params);
        viewPager.requestLayout();
    }

    public void requestProjectGallery() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListProjectGallery(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateList(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(DetailGalleryActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(DetailGalleryActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("groupGalleryRef", groupGalleryRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "projectGallery");

    }

    private void generateList(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProjectGalleryModel projectGalleryModel = new ProjectGalleryModel();
                projectGalleryModel.setImgGalleryRef(jo.getString("imgGalleryRef"));
                projectGalleryModel.setGroupGalleryRef(jo.getString("groupGalleryRef"));
                projectGalleryModel.setTitle(jo.getString("title"));
                projectGalleryModel.setDescription(jo.getString("description"));
                projectGalleryModel.setFileName(jo.getString("fileName"));
                projectGalleryModel.setImgFile(jo.getString("imgFile"));
                projectGalleryModel.setProjectName(projectName);

                listProjectGallery.add(projectGalleryModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(position);
    }

    public void showDialogCekPermission (){
        if (ContextCompat.checkSelfPermission(DetailGalleryActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DetailGalleryActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
        } else {
            proceedAfterPermissionAllow();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermissionAllow();
            } else {
                proceedAfterPermissionDeny();
            }
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

}
