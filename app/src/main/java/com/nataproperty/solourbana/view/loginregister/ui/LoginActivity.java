package com.nataproperty.solourbana.view.loginregister.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.PrefUtils;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyEditTextLatoReguler;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.loginregister.model.UserInfoFacebookModel;
import com.nataproperty.solourbana.view.menuitem.ui.ForgotPasswordActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by User on 4/13/2016.
 */
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";
    public static final String NAMA = "name";
    public static final String ISACTIVE = "isActive";
    public static final String PASSWORD = "password";
    public static final String MOBILE = "mobile";
    public static final String MEMBER_REF = "memberRef";
    public static final String BIRTHDATE = "birthDate";
    public static final String STATUS = "status";
    public static final String STATUS_GOOGLE_SIGN_IN = "statusGoogleSignIn";
    private MyEditTextLatoReguler txtEmail, txtPassword;
    private Button btnLogin;
    private Button btnGoogle, btnRegisterCustomer, btnRegisterCustomerQR, btnRegisterGuest;
    private CheckBox cbShowPassword;
    private MyTextViewLatoReguler btnLinkToForgotPassword, btnLinkToRegister;
    private String email;
    private String password, urlLogin, projectCode;
    private SharedPreferences sharedPreferences;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 0;
    private ProgressDialog progress_dialog;
    private GoogleSignInAccount acct;
    private String emailGet, nameGet, acctName, acctEmail;
    private int loginType; // login tipe customer atau yang lainnya
    private Toolbar toolbar;
    private TextView title, txtOr;
    private Typeface font;
    UserInfoFacebookModel user;
    private LoginButton loginButtonFB;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initWidget();
        // cek login
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        boolean state = sharedPreferences.getBoolean("isLogin", false);

        if (state) {
            // open main activity
            Intent intent = new Intent(LoginActivity.this, LaunchActivity.class);
            startActivity(intent);
        }

        setVisibleWidget();
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    txtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    txtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        //Google+
        buidNewGoogleApiClient();
        progress_dialog = new ProgressDialog(this);
        progress_dialog.setMessage("Signing in....");

        loginButtonFB.setReadPermissions("public_profile", "email", "user_friends");
        loginButtonFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButtonFB.registerCallback(callbackManager, mCallBack);
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtEmail = (MyEditTextLatoReguler) findViewById(R.id.txtEmail);
        txtPassword = (MyEditTextLatoReguler) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        cbShowPassword = (CheckBox) findViewById(R.id.cbShowPassword);
        btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnRegisterCustomer = (Button) findViewById(R.id.btnRegisterCustomer);
        btnRegisterCustomerQR = (Button) findViewById(R.id.btnRegisterCustomerQR);
        btnLinkToForgotPassword = (MyTextViewLatoReguler) findViewById(R.id.btnLinkToForgotPassword);
        btnLinkToRegister = (MyTextViewLatoReguler) findViewById(R.id.btnLinkToRegister);
        btnRegisterGuest = (Button) findViewById(R.id.btnRegisterGuest);
        txtOr = (TextView) findViewById(R.id.txt_or);
        loginButtonFB = (LoginButton) findViewById(R.id.login_btn_fb);
        callbackManager = CallbackManager.Factory.create();

        btnRegisterCustomerQR.setTypeface(font);
        btnRegisterCustomer.setTypeface(font);
        btnLogin.setTypeface(font);
        btnRegisterGuest.setTypeface(font);
        cbShowPassword.setTypeface(font);

        btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnGoogle.setTypeface(font);

        btnRegisterCustomerQR.setOnClickListener(this);
        btnRegisterCustomer.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnRegisterGuest.setOnClickListener(this);
        btnLinkToForgotPassword.setOnClickListener(this);
        btnLinkToRegister.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                userLogin();
                break;
            case R.id.btnRegisterCustomer:
                startActivity(new Intent(LoginActivity.this, RegisterCustomerActivity.class));
                break;
            case R.id.btnRegisterCustomerQR:
                if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.CAMERA}, 0);
                } else {
                    intentToRegisterCustomerQRActivity();
                }
                break;
            case R.id.btnLinkToRegister:
                startActivity(new Intent(LoginActivity.this, RegisterStepOneActivity.class));
                break;
            case R.id.btnLinkToForgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                break;
            case R.id.btnGoogle:
                gSignIn();
                break;
            case R.id.btnRegisterGuest:
                Intent i = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                i.putExtra(General.GUEST, true);
                startActivity(i);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                intentToRegisterCustomerQRActivity();
            } else {
                Toast.makeText(getBaseContext(), "We don't have the Camera Permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void intentToRegisterCustomerQRActivity() {
        startActivity(new Intent(LoginActivity.this, RegisterCustomerQRActivity.class));
    }

    /**
     * lakukan pengecekan, jika login typenya adalah customer
     * jika customer button register customer akan dimunculkan.
     */
    private void setVisibleWidget() {
        // get intent extra login type
        loginType = getIntent().getIntExtra(General.INTENT_CAT_LOGIN, 1);
        Log.d("loginType", "loginType " + loginType);
        if (loginType == General.LOGIN_CUSTOMER) {
            title.setText(getResources().getString(R.string.title_login_customer));
            btnGoogle.setVisibility(View.GONE);
            btnRegisterCustomer.setVisibility(View.VISIBLE);
            btnRegisterCustomerQR.setVisibility(View.VISIBLE);
            btnLinkToRegister.setVisibility(View.GONE);
            btnLinkToForgotPassword.setVisibility(View.GONE);
            btnRegisterGuest.setVisibility(View.VISIBLE);
            txtOr.setVisibility(View.VISIBLE);
            loginButtonFB.setVisibility(View.GONE);
        } else {
            title.setText(getResources().getString(R.string.title_login_agent));
            btnGoogle.setVisibility(View.VISIBLE);
            btnRegisterCustomer.setVisibility(View.GONE);
            btnRegisterCustomerQR.setVisibility(View.GONE);
            btnLinkToRegister.setVisibility(View.VISIBLE);
            btnLinkToForgotPassword.setVisibility(View.VISIBLE);
            btnRegisterGuest.setVisibility(View.GONE);
            txtOr.setVisibility(View.GONE);
            loginButtonFB.setVisibility(View.VISIBLE);
        }
    }

    private void gSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
        progress_dialog.show();
    }

    /*Google+*/
    private void buidNewGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail() //Remove these below according to your needs
                .requestProfile()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                progress_dialog.dismiss();
            }
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            getSignInResult(result);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void getSignInResult(GoogleSignInResult result) {
        if (result == null) {
            Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_LONG).show();
        } else {
            if (result.isSuccess() && result != null) {
                acct = result.getSignInAccount();
                acctName = acct.getDisplayName();
                acctEmail = acct.getEmail();

                if (acctName == null) {
                    acctName = "Mr/Mrs VIP";
                }

                registerGoogleSignIn(acctEmail);
                progress_dialog.dismiss();
            } else {
                Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void userLogin() {
        if (!validate()) {
            onLoginFailed();
            return;
        }
        email = txtEmail.getText().toString().trim();
        password = txtPassword.getText().toString().trim();
        Log.d(KEY_EMAIL, "email " + email);
        Log.d(KEY_PASSWORD, "password " + password);

        // url login  dan project code
        urlLogin = WebService.getLogin();
        if (loginType == General.LOGIN_CUSTOMER) {
            urlLogin = WebService.getLoginCustomer();
            projectCode = General.projectCode;
        }
        Log.d("urlLogin", "urlLogin " + urlLogin);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                urlLogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                Log.d("cek", "response login " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    Log.d("status login", "" + String.valueOf(status));
                    if (status == 200) {
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");

                        SharedPreferences sharedPreferences = LoginActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this, LaunchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } else if (status == 201) {
                        txtEmail.setError(message);

                    } else if (status == 202) {
                        txtPassword.setError(message);

                    } else if (status == 203) {
                        txtEmail.setError(message);

                    } else if (status == 205) {
                        cekEmailPopup();

                    } else if (status == 206) {
                        cekEmailCostumerPopup();

                        //Login dari KALK Biasa
                    } else if (status == 400) {
                        String name = jo.getJSONObject("data").getString("fullName");
                        String email = jo.getJSONObject("data").getString("username");
                        String mobile = jo.getJSONObject("data").getString("hp1");
                        String birthDate = jo.getJSONObject("data").getString("birthDate");
                        Intent intent = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                        intent.putExtra(KEY_EMAIL, email);
                        intent.putExtra(NAMA, name);
                        intent.putExtra(PASSWORD, txtPassword.getText().toString());
                        intent.putExtra(MOBILE, mobile);
                        intent.putExtra(BIRTHDATE, birthDate);
                        intent.putExtra(ISACTIVE, 1);
                        startActivity(intent);

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(LoginActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_EMAIL, email);
                params.put(KEY_PASSWORD, password);
                params.put("projectCode", General.projectCode + "#" + General.projectCodeSingleProject);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "login");

    }

    public boolean validate() {
        boolean valid = true;
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txtEmail.setError("enter a valid email address");
            txtEmail.requestFocus();
            valid = false;
        } else {
            txtEmail.setError(null);
        }

        if (password.isEmpty()) {
            txtPassword.setError("enter a password");
            txtPassword.requestFocus();
            valid = false;
        } else {
            txtPassword.setError(null);
        }

        return valid;
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void registerGoogleSignIn(final String acctEmail) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.signInGoogle(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Login", "response " + response);
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    final String message = jo.getString("message");
                    final String memberRef = jo.getString("memberRef");
                    final String memberType = jo.getString("memberType");
                    final String memberTypeCode = jo.getString("memberTypeCode");
                    /*200 = sukses*/
                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        emailGet = jo.getJSONObject("data").getString("email1");
                        nameGet = jo.getJSONObject("data").getString("name");
                        SharedPreferences sharedPreferences = LoginActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", emailGet);
                        editor.putString("isName", nameGet);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this, LaunchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                        /*201 = new*/
                    } else if (status == 201) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                                        intent.putExtra(STATUS_GOOGLE_SIGN_IN, "201");
                                        intent.putExtra(KEY_EMAIL, acctEmail);
                                        intent.putExtra(NAMA, acctName);
                                        intent.putExtra(MEMBER_REF, memberRef);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                    /*202 = Register dari calculator*/
                    } else if (status == 300) {
                        final String name = jo.getJSONObject("data").getString("name");
                        final String mobile = jo.getJSONObject("data").getString("hP1");
                        final String birthDate = jo.getJSONObject("data").getString("birthDate");
                        final String password = jo.getJSONObject("data").getString("key");
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                                        intent.putExtra(STATUS_GOOGLE_SIGN_IN, "202");
                                        intent.putExtra(KEY_EMAIL, acctEmail);
                                        intent.putExtra(PASSWORD, password);
                                        intent.putExtra(MEMBER_REF, memberRef);
                                        intent.putExtra(NAMA, name);
                                        intent.putExtra(MOBILE, mobile);
                                        intent.putExtra(BIRTHDATE, birthDate);
                                        intent.putExtra(ISACTIVE, 1);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                    /*locked*/
                    } else if (status == 400) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else if (status == 206) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        cekEmailCostumerPopup();
                                    }
                                });
                    } else {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {

                                    }
                                });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("email", "email " + acctEmail);
                params.put("email", acctEmail);
                params.put("projectCode", General.projectCode + "#" + General.projectCodeSingleProject);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productImage");

    }

    private void cekEmailPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_activate_email, null);
        dialogBuilder.setView(dialogView);
        final TextView txtActivate = (TextView) dialogView.findViewById(R.id.txt_activate);
        txtActivate.setText("please activate your account first, check your email to activate");
        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void cekEmailCostumerPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_activate_email, null);
        dialogBuilder.setView(dialogView);
        final TextView txtActivate = (TextView) dialogView.findViewById(R.id.txt_activate);
        if (loginType == General.LOGIN_GENERAL) {
            txtActivate.setText(getResources().getString(R.string.if_email_costumer));
        } else {
            txtActivate.setText(getResources().getString(R.string.if_email_agent));
        }
        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("callbackFB", "success");
//            progressDialog.dismiss();

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.e("response: ", response + "");

                            try {
                                user = new UserInfoFacebookModel();
                                user.facebookID = object.getString("id").toString();
                                user.email = object.getString("email").toString();
                                user.name = object.getString("name").toString();
                                user.gender = ""; // object.getString("gender").toString();
                                user.urlPhotoProfile = "http://graph.facebook.com/" + user.facebookID + "/picture?type=large";
                                PrefUtils.setCurrentUser(user, LoginActivity.this);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            loginByFacebook();

                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
//            progressDialog.dismiss();
            Log.d("callbackFB", "cancel");
        }

        @Override
        public void onError(FacebookException e) {
            Log.d("callbackFB", e.getMessage().toString());
//            progressDialog.dismiss();
        }
    };

    private void loginByFacebook() {
        progress_dialog.show();
        // logout fb
        LoginManager.getInstance().logOut();
        if (PrefUtils.getCurrentUser(LoginActivity.this) != null) {
            final StringRequest request = new StringRequest(Request.Method.POST,
                    WebService.getRegisterFacebook(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Login", "response " + response);
                    progress_dialog.dismiss();
                    try {
                        JSONObject jo = new JSONObject(response);
                        int status = jo.getInt("status");
                        final String message = jo.getString("message");
                        final String memberRef = jo.getString("memberRef");
                        final String memberType = jo.getString("memberType");
                        final String memberTypeCode = jo.getString("memberTypeCode");
                    /*200 = sukses*/
                        if (status == 200) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                            emailGet = jo.getJSONObject("data").getString("email1");
                            nameGet = jo.getJSONObject("data").getString("name");
                            SharedPreferences sharedPreferences = LoginActivity.this.
                                    getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("isLogin", true);
                            editor.putString("isMemberRef", memberRef);
                            editor.putString("isEmail", emailGet);
                            editor.putString("isName", nameGet);
                            editor.putString("isMemberType", memberType);
                            editor.putString("isMemberTypeCode", memberTypeCode);
                            editor.commit();

                            Intent intent = new Intent(LoginActivity.this, LaunchActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        /*201 = new*/
                        } else if (status == 201) {
                            // tampilkan ke halaman register
                            Toast.makeText(LoginActivity.this, "welcome " + user.email, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                            intent.putExtra("email", user.email);
                            intent.putExtra("name", user.name);
                            intent.putExtra("urlPhotoProfile", user.urlPhotoProfile);

                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse networkResponse = error.networkResponse;
                            progress_dialog.dismiss();
                            if (networkResponse != null) {
                                Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                            }
                            if (error instanceof TimeoutError) {
                                Toast.makeText(LoginActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                                Log.d("Volley", "TimeoutError");
                            } else if (error instanceof NoConnectionError) {
                                Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                                Log.d("Volley", "NoConnectionError");
                            } else if (error instanceof AuthFailureError) {
                                Log.d("Volley", "AuthFailureError");
                            } else if (error instanceof ServerError) {
                                Log.d("Volley", "ServerError");
                            } else if (error instanceof NetworkError) {
                                Log.d("Volley", "NetworkError");
                            } else if (error instanceof ParseError) {
                                Log.d("Volley", "ParseError");
                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", user.email);
                    params.put("projectCode", General.projectCode);
                    params.put("urlProfile", user.urlPhotoProfile);
                    params.put("sosmedID", user.facebookID);
                    params.put("fbName", user.name);
                    params.put("birthDate", "");
                    return params;
                }
            };

            BaseApplication.getInstance().addToRequestQueue(request, "productImage");
        }


    }


}
