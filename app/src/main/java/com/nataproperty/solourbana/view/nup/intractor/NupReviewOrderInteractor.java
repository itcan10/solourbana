package com.nataproperty.solourbana.view.nup.intractor;

import com.nataproperty.solourbana.view.nup.model.JsonInCustSaveOrderNupModel;
import com.nataproperty.solourbana.view.nup.model.JsonInSaveOrderNupModel;

public interface NupReviewOrderInteractor {
    void saveOrderNup_v2(JsonInSaveOrderNupModel jsonInSaveOrderNupModel, JsonInCustSaveOrderNupModel jsonInCustSaveOrderNupModel);
}
