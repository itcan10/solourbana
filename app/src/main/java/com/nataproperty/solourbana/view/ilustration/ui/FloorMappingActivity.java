package com.nataproperty.solourbana.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.SessionManager;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.ilustration.adapter.FloorMappingAdapter;
import com.nataproperty.solourbana.view.ilustration.model.ListBlockDiagramModel;
import com.nataproperty.solourbana.view.ilustration.presenter.FloorMappingPresenter;

import java.util.ArrayList;
import java.util.List;

public class FloorMappingActivity extends AppCompatActivity {
    private ServiceRetrofit service;
    private FloorMappingPresenter presenter;
    ProgressDialog progressDialog;
    private List<ListBlockDiagramModel> listFloorMapping = new ArrayList<>();
    private FloorMappingAdapter adapter;
    ImageView imgLogo;
    TextView txtProjectName, txtFloor;
    String projectRef, dbMasterRef, categoryRef, clusterRef, projectName, blockName,
            isBooking, titleProduct, productRef, isShowAvailableUnit, memberRef, unitMaping, unitRefList,
            color, statusColor, statusColorName, unitStatusList, productNameList, productRefList, viewFloorPlan,viewFloorPlanUrl;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Display display;
    Point size;
    Integer width;
    Double result;
    RelativeLayout rPage;
    MyListView listView;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_mapping);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new FloorMappingPresenter(this, service);
        sessionManager = new SessionManager(this);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(General.DBMASTER_REF);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        categoryRef = intent.getStringExtra(General.CATEGORY_REF);
        clusterRef = intent.getStringExtra(General.CLUSTER_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        isBooking = intent.getStringExtra(General.IS_BOOKING);
        productRef = intent.getStringExtra(General.PRODUCT_REF);
        isShowAvailableUnit = intent.getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        titleProduct = intent.getStringExtra(General.TITLE_PRODUCT);

        initWidget();

        Log.d("cek Block", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef + " "+productRef);

        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        memberRef = sessionManager.getStringFromSP(General.IS_MEMBER_REF);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                blockName = listFloorMapping.get(position).getBlockName();
                unitMaping = listFloorMapping.get(position).getUnitMaping();
                unitRefList = listFloorMapping.get(position).getUnitRefList();
                color = listFloorMapping.get(position).getColor();
                statusColor = listFloorMapping.get(position).getStatusColor();
                statusColorName = listFloorMapping.get(position).getStatusColorName();
                unitStatusList = listFloorMapping.get(position).getUnitStatusList();
                productNameList = listFloorMapping.get(position).getProductNameList();
                productRefList = listFloorMapping.get(position).getProductRefList();
                viewFloorPlan = listFloorMapping.get(position).getViewFloorPlan();
                viewFloorPlanUrl = listFloorMapping.get(position).getViewFloorPlanUrl();

                Intent intentUnit = new Intent(FloorMappingActivity.this, UnitMappingActivity.class);
                intentUnit.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentUnit.putExtra(General.PROJECT_REF, projectRef);
                intentUnit.putExtra(General.CATEGORY_REF, categoryRef);
                intentUnit.putExtra(General.CLUSTER_REF, clusterRef);
                intentUnit.putExtra(General.PROJECT_NAME, projectName);
                intentUnit.putExtra(General.BLOCK_NAME, blockName);
                intentUnit.putExtra(General.IS_BOOKING, isBooking);
                intentUnit.putExtra(General.PRODUCT_REF, productRef);
                intentUnit.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                intentUnit.putExtra(General.UNIT_MAPPING, unitMaping);
                intentUnit.putExtra(General.UNIT_REF_LIST, unitRefList);
                intentUnit.putExtra(General.COLOR, color);
                intentUnit.putExtra(General.STATUS_COLOR, statusColor);
                intentUnit.putExtra(General.STATUS_COLOR_NAME, statusColorName);
                intentUnit.putExtra(General.UNIT_STATUS_LIST, unitStatusList);
                intentUnit.putExtra(General.PRODUCT_NAME_LIST, productNameList);
                intentUnit.putExtra(General.PRODUCT_REF_LIST, productRefList);
                intentUnit.putExtra(General.TITLE_PRODUCT, titleProduct);
                intentUnit.putExtra(General.VIEW_FLOOR_PLAN, viewFloorPlan);
                intentUnit.putExtra(General.VIEW_FLOOR_PLAN_URL, viewFloorPlanUrl);
                startActivity(intentUnit);
            }
        });

        requestBlockMapping();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_illustrastion_unit_mapping));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtFloor = (TextView) findViewById(R.id.txt_floor);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyListView) findViewById(R.id.list_floor);

    }

    public void requestBlockMapping() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        // presenter.getListBlok(dbMasterRef, projectRef, categoryRef, clusterRef);
        presenter.getBlockMapping(dbMasterRef, projectRef, categoryRef, clusterRef, productRef, memberRef);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showBlockMappingResults(retrofit2.Response<List<ListBlockDiagramModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful())
            listFloorMapping = response.body();
        initAdapter();
    }

    private void initAdapter() {
        adapter = new FloorMappingAdapter(this, listFloorMapping);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showBlockMappingFailure(Throwable t) {
        progressDialog.dismiss();

    }
}
