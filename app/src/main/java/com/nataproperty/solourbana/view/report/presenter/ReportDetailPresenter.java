package com.nataproperty.solourbana.view.report.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.report.intractor.ReportDetailInteractor;
import com.nataproperty.solourbana.view.report.model.ResponGenerateReportModel;
import com.nataproperty.solourbana.view.report.ui.ReportDetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ReportDetailPresenter implements ReportDetailInteractor {
    private ReportDetailActivity view;
    private ServiceRetrofitProject service;

    public ReportDetailPresenter(ReportDetailActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GenerateDashboardReportSvc(String projectCode, String dbMasterRef, String projectRef, String memberRef, String userRef) {
        Call<ResponGenerateReportModel> call = service.getAPI().GenerateDashboardReportSvc(projectCode,dbMasterRef,projectRef,memberRef,userRef);
        call.enqueue(new Callback<ResponGenerateReportModel>() {
            @Override
            public void onResponse(Call<ResponGenerateReportModel> call, Response<ResponGenerateReportModel> response) {
                view.showGenerateResults(response);
            }

            @Override
            public void onFailure(Call<ResponGenerateReportModel> call, Throwable t) {
                view.showGenerateFailure(t);

            }


        });
    }

}
