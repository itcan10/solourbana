package com.nataproperty.solourbana.view.project.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by UserModel on 4/21/2016.
 */
public class ProjectDetailActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = "ProjectDetailActivity";
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

    DownloadFileAsync dfa = new DownloadFileAsync();

    private WebView webView;
    LinearLayout linearMaps;
    private GoogleMap googleMap;
    ImageView imgLogo;
    TextView txtProjectName;
    private long dbMasterRef;
    private String projectRef, projectName, locationName, locationRef, sublocationName, sublocationRef, linkDetail;
    private String projectDescription, urlVideo = "", imageLogo, downloadProjectInfo;
    private double latitude, longitude;
    private String projectWA, projectEmail, projectPhone;
    Button btnPropertyVideo;
    FloatingActionButton fab, fabwhatsapp, fabcall, fabemail;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Point size;
    Integer width;
    Double result;
    ProgressDialog mProgressDialog;
    AlertDialog alertDialog;
    private ProgressBar progressBar;
    String strToday;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
        initWidget();

        Intent intent = getIntent();
        projectRef = intent.getStringExtra("projectRef");
        dbMasterRef = intent.getLongExtra("dbMasterRef", 0);
        projectName = intent.getStringExtra("projectName");
        linkDetail = intent.getStringExtra(General.LINK_DETAIL);
        latitude = Double.parseDouble(intent.getStringExtra(General.LATITUDE));
        longitude = Double.parseDouble(intent.getStringExtra(General.LONGITUDE));
        urlVideo = intent.getStringExtra("urlVideo");
        //imageLogo = intent.getStringExtra("imageLogo");
        downloadProjectInfo = intent.getStringExtra("downloadProjectInfo");
        projectWA = intent.getStringExtra("projectWA");
        projectEmail = intent.getStringExtra("projectEmail");
        projectPhone = intent.getStringExtra("projectPhone");

        Log.d(TAG, downloadProjectInfo + " ");
        if (projectEmail.equals("")) {
            fabemail.setVisibility(View.GONE);
        }

        if (projectWA.equals("")) {
            fabwhatsapp.setVisibility(View.GONE);
        }

        if (projectPhone.equals("")) {
            fabcall.setVisibility(View.GONE);
        }

        title.setText(String.valueOf(projectName));
        txtProjectName.setText(projectName);
        Glide.with(this).load(WebService.getProjectImage() + String.valueOf(dbMasterRef) +
                "&pr=" + projectRef).into(imgLogo);

        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        webView.loadUrl(linkDetail);

        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");

        if (!urlVideo.equals("") && urlVideo != null) {
            btnPropertyVideo.setVisibility(View.VISIBLE);
        } else {
            btnPropertyVideo.setVisibility(View.GONE);
        }
        if (latitude == 0.0) {
            linearMaps.setVisibility(View.GONE);
        } else {
            linearMaps.setVisibility(View.VISIBLE);
        }
        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void showToast(String msg) {
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void showMaps (String latitude,String longitude,String label){

            String uri = String.format("geo:"+latitude+","+ longitude+"?q="+latitude+","+longitude+"("+label+")");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(uri));
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(mapIntent);
            }

        }

        @JavascriptInterface
        public void showWeb(String url){
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        webView = (WebView) findViewById(R.id.webView);
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        linearMaps = (LinearLayout) findViewById(R.id.linier_maps);
        btnPropertyVideo = (Button) findViewById(R.id.btn_property_video);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabwhatsapp = (FloatingActionButton) findViewById(R.id.fabwhatsapp);
        fabcall = (FloatingActionButton) findViewById(R.id.fabcall);
        fabemail = (FloatingActionButton) findViewById(R.id.fabemail);

        btnPropertyVideo.setTypeface(font);
        btnPropertyVideo.setOnClickListener(this);

        fab.setOnClickListener(this);
        fabwhatsapp.setOnClickListener(this);
        fabcall.setOnClickListener(this);
        fabemail.setOnClickListener(this);


        progressBar = (ProgressBar) findViewById(R.id.progress1);
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.maps)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("mailto:")) {
                //Handle mail Urls
                startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse(url)));
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProjectDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_property_video:
                Intent intent = new Intent(this, YoutubeActivity.class);
                intent.putExtra(General.URL_VIDEO, urlVideo);
                startActivity(intent);
                break;
            case R.id.fab:
                if (checkWriteExternalPermission()) {
                    showDialogDownload();
                } else {
                    showDialogCekPermission();
                    Toast.makeText(getBaseContext(), "We need the Storage Permission", Toast.LENGTH_LONG).show();
                }

//                String latitude1 ="-6.353387";
//                String longitude1="106.578507";
//                String label = "Millennium City";
//
//                String uri = String.format("geo:"+latitude1+","+ longitude1+"?q="+latitude1+","+longitude1+"("+label+")");
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(uri));
//                mapIntent.setPackage("com.google.android.apps.maps");
//                if (mapIntent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(mapIntent);
//                }


                break;
            case R.id.fabwhatsapp:
                try {
                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName(General.PACKAGE_WHATSHAPP, "com.whatsapp.Conversation"));
                    sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(projectWA) + "@s.whatsapp.net");
                    startActivity(sendIntent);
                } catch (Exception e) {
                    Toast.makeText(this, getString(R.string.whatshapp_not_instal), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.fabcall:
                if (isPermissionGranted()) {
                    call_action();
                }
                break;
            case R.id.fabemail:
                Intent intentemail = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", projectEmail, null));
                intentemail.putExtra(Intent.EXTRA_SUBJECT, "Inquiries");
                intentemail.putExtra(Intent.EXTRA_TEXT, "Please type your inquiry here");
                startActivity(intentemail.createChooser(intentemail, "Choose an Email client :"));
                break;

        }

    }

    public void call_action() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + projectPhone));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }


    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    private void showDialogDownload() {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHH");
        Date today = Calendar.getInstance().getTime();
        strToday = df.format(today);
        String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                + projectName.trim().toString() + "_" + strToday + ".pdf";
        File f = new File(path);

        if (f.exists()) {
            sharedDocument();
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProjectDetailActivity.this);
            alertDialogBuilder.setCancelable(true);
            alertDialogBuilder.setMessage(getString(R.string.sharedProject).replace("projectName", projectName + "."));
            alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startDownload(downloadProjectInfo);

                }
            });
            alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    public void showDialogCekPermission() {
//        try {
//            if (ContextCompat.checkSelfPermission(IlustrationPaymentActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    != PackageManager.PERMISSION_GRANTED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale(IlustrationPaymentActivity.this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    ActivityCompat.requestPermissions(IlustrationPaymentActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            EXTERNAL_STORAGE_PERMISSION_CONSTANT);
//                }
//            }
//        } catch (SecurityException e) {
//            Toast.makeText(getBaseContext(), "We need the Storage Permission, Enable Storage Permission", Toast.LENGTH_LONG).show();
//        }

        // Here, thisActivity is the current activity
        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(ProjectDetailActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProjectDetailActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(ProjectDetailActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
        }

    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermissionAllow();
            } else {
                proceedAfterPermissionDeny();
            }
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
        showDialogDownload();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void startDownload(String url) {
        Log.d("Cek", "" + checkWriteExternalPermission());
        if (!url.equals("")) {
//            new DownloadFileAsync().execute(url);
            dfa = new DownloadFileAsync();
            dfa.execute(url);   // Add
        } else {
            Toast.makeText(ProjectDetailActivity.this, "No input url", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this, R.style.AlertDialogTheme);
                mProgressDialog.setMessage("Downloading file..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);

                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dfa.cancel(true);   // Add
                        String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                                + projectName.trim().toString() + "_" + strToday + ".pdf";
                        File f = new File(path);

                        try{
                            f.delete();
                        }catch (Exception e) {

                        }
                        dialog.dismiss();
                    }
                });

                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }


    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            int count;

            try {
                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                File wallpaperDirectory = new File(android.os.Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(wallpaperDirectory + "/" + projectName.trim().toString() + "_" + strToday + ".pdf");

                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                    if (isCancelled()) break;// Add

                }

                if (isCancelled()) {
                    dfa.cancel(true);
                }

                output.flush();
                output.close();
                input.close();

                if (!isCancelled()) {
                    sharedDocument();
                }

            } catch (Exception e) {
            }
            return null;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d("ANDRO_ASYNC", values[0]);
            mProgressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

    }

    private void sharedDocument() {
        try {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("application/pdf");
            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                    "/nataproperty/" + projectName.trim().toString() + "_" + strToday + ".pdf";
            File imageFileToShare = new File(imagePath);
            Uri uri = Uri.fromFile(imageFileToShare);
            share.putExtra(Intent.EXTRA_STREAM, uri);
            // kalo mao nambah di subject dan body email disini
            share.putExtra(Intent.EXTRA_SUBJECT, "Informasi Project " + projectName);
            share.putExtra(Intent.EXTRA_TEXT, "Berikut adalah informasi project " + projectName);
            startActivity(Intent.createChooser(share, "Share Document!"));

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "File tidak bisa dibagikan", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }
}
