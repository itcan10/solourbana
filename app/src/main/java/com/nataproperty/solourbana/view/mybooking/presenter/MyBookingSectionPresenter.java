package com.nataproperty.solourbana.view.mybooking.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.mybooking.intractor.PresenterMyBookingSectionInteractor;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingSectionModel;
import com.nataproperty.solourbana.view.mybooking.ui.MyBookingSectionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookingSectionPresenter implements PresenterMyBookingSectionInteractor {
    private MyBookingSectionActivity view;
    private ServiceRetrofitProject service;

    public MyBookingSectionPresenter(MyBookingSectionActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListBookingSection(String memberRef, String projectCode) {
        Call<List<MyBookingSectionModel>> call = service.getAPI().GetSectionMyBookingProject(memberRef,projectCode);
        call.enqueue(new Callback<List<MyBookingSectionModel>>() {
            @Override
            public void onResponse(Call<List<MyBookingSectionModel>> call, Response<List<MyBookingSectionModel>> response) {
                view.showListBookingSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<MyBookingSectionModel>> call, Throwable t) {
                view.showListBookingSectionFailure(t);

            }


        });
    }
}
