package com.nataproperty.solourbana.view.ilustration.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.MyTextViewLatoLight;
import com.nataproperty.solourbana.view.kpr.model.DataKalkulator;

import java.util.List;

/**
 * Created by nata on 3/27/2018.
 */

public class IlustrationPaymentBankKPRAdapter extends BaseAdapter {

    public static final String TAG = "PaymentTermAdapter";

    private Context context;
    private List<DataKalkulator> list;
    private ListBankKPRHolder holder;
    private String tenor;

    public IlustrationPaymentBankKPRAdapter(Context context, List<DataKalkulator> list, String tenor) {
        this.context = context;
        this.list = list;
        this.tenor = tenor;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_bank_kpr,null);
            holder = new ListBankKPRHolder();
            holder.txtTenor = (MyTextViewLatoLight) convertView.findViewById(R.id.txt_tenor);
            holder.txtBank = (MyTextViewLatoLight) convertView.findViewById(R.id.txt_bank);
            holder.txtInstallment = (MyTextViewLatoLight) convertView.findViewById(R.id.txt_installment);
            holder.txtFloat = (MyTextViewLatoLight) convertView.findViewById(R.id.txt_float);
            holder.txtTerm = (MyTextViewLatoLight) convertView.findViewById(R.id.txt_term);
            holder.txtStatus = (MyTextViewLatoLight) convertView.findViewById(R.id.txt_status);

            convertView.setTag(holder);
        }else{
            holder = (ListBankKPRHolder) convertView.getTag();
        }

        DataKalkulator dataKalkulator = list.get(position);
        String installmentStr = dataKalkulator.getInstallment() + " ("+ dataKalkulator.getInterest() +" % - " + dataKalkulator.getFixYear() + " Tahun)";
        String floatStr = dataKalkulator.getFloatStr() + " ("+ dataKalkulator.getFloatInterest() +" % - " + String.valueOf(Integer.valueOf(tenor) - Integer.valueOf(dataKalkulator.getFixYear())) + " Tahun)";

        holder.txtTenor.setText(tenor + " Tahun");
        holder.txtBank.setText(dataKalkulator.getBankName());
        holder.txtInstallment.setText(installmentStr);
        holder.txtFloat.setText(floatStr);
        holder.txtTerm.setText(dataKalkulator.getTerm());
        holder.txtStatus.setText(dataKalkulator.getStatus());

        Log.d(TAG,"getFloatStr "+dataKalkulator.getFloatStr());

        return convertView;
    }

    private class ListBankKPRHolder {
        MyTextViewLatoLight txtBank, txtInstallment, txtFloat, txtTerm, txtStatus,txtTenor;
    }
}
