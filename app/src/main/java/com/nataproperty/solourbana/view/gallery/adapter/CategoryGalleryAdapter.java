package com.nataproperty.solourbana.view.gallery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.gallery.model.CategoryGalleryModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class CategoryGalleryAdapter extends BaseAdapter {
    private Context context;
    private List<CategoryGalleryModel> list;
    private ListCategoryGalleryHolder holder;

    public CategoryGalleryAdapter(Context context, List<CategoryGalleryModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_category_gallery,null);
            holder = new ListCategoryGalleryHolder();
            holder.fileName = (TextView) convertView.findViewById(R.id.txt_fileName);

            convertView.setTag(holder);
        }else{
            holder = (ListCategoryGalleryHolder) convertView.getTag();
        }
        CategoryGalleryModel download = list.get(position);
        holder.fileName.setText(download.getGroupGalleryName());
        //holder.extension.setText(download.getExtension());


        return convertView;
    }

    private class ListCategoryGalleryHolder {
        TextView fileName,extension;
    }
}
