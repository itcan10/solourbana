package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.nup.model.BankModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class BankAdapter extends BaseAdapter {
    private Context context;
    private List<BankModel> list;
    private ListBankHolder holder;

    public BankAdapter(Context context, List<BankModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_bank,null);
            holder = new ListBankHolder();
            holder.bankName = (TextView) convertView.findViewById(R.id.txt_bank);

            convertView.setTag(holder);
        }else{
            holder = (ListBankHolder) convertView.getTag();
        }

        BankModel feature = list.get(position);
        holder.bankName.setText(feature.getBankName());

       /* Picasso.with(context)
                .load("http://192.168.10.180/andro/wf/support/displayImage.aspx?is=cluster&dr=5&pr=1&cr=1&quot;).into(holder.clusterImg);*/

        return convertView;

    }

    private class ListBankHolder {
        TextView bankName;
    }
}
