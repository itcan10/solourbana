package com.nataproperty.solourbana.view.projectmenu.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterProjectMenuInteractor {
    //void getProjectDtl(String dbMasterRef,String projectRef);
    void getCommision(String dbMasterRef,String projectRef, String memberRef);
    void getProjectDetailSvc(String projectCode,String memberRef);
    void saveGCMTokenProjectSvc(String GCMToken, String memberRef, String projectCode);
    void getListNupSection(String memberRef,String projectCode);
    void getListBookingSection(String memberRef,String projectCode);
}
