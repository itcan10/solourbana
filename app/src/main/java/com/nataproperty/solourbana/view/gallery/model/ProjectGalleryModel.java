package com.nataproperty.solourbana.view.gallery.model;

/**
 * Created by UserModel on 9/6/2016.
 */
public class ProjectGalleryModel {
    String imgGalleryRef, groupGalleryRef, title, description, fileName, imgFile, projectName;

    public String getImgGalleryRef() {
        return imgGalleryRef;
    }

    public void setImgGalleryRef(String imgGalleryRef) {
        this.imgGalleryRef = imgGalleryRef;
    }

    public String getGroupGalleryRef() {
        return groupGalleryRef;
    }

    public void setGroupGalleryRef(String groupGalleryRef) {
        this.groupGalleryRef = groupGalleryRef;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getImgFile() {
        return imgFile;
    }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
