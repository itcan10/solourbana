package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by nata on 12/1/2016.
 */

public class StatusIlustrationAddKprModel {
    int status;
    String message;
    DataPriceIlusAddKpr dataPrice;
    String isKPAAdvance;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataPriceIlusAddKpr getDataPrice() {
        return dataPrice;
    }

    public void setDataPrice(DataPriceIlusAddKpr dataPrice) {
        this.dataPrice = dataPrice;
    }

    public String getIsKPAAdvance() {
        return isKPAAdvance;
    }

    public void setIsKPAAdvance(String isKPAAdvance) {
        this.isKPAAdvance = isKPAAdvance;
    }
}
