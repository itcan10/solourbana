package com.nataproperty.solourbana.view.loginregister.ui;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyEditTextLatoReguler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by arifcebe
 * on Mar 3/27/17 10:35.
 * Project : SerpongGardenApps
 */

public class RegisterCustomerActivity extends AppCompatActivity implements View.OnClickListener {

    static MyEditTextLatoReguler edSpu, edTglLahir;
    Button btnNext;
    Toolbar toolbar;
    Calendar myCalendar = Calendar.getInstance();
    Typeface font;
    String birthDate, bookingCode;
    boolean qrCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_customer);

        // instansiasi widget
        edSpu = (MyEditTextLatoReguler) findViewById(R.id.ed_spu);
        edTglLahir = (MyEditTextLatoReguler) findViewById(R.id.ed_tglLahir);
        edTglLahir.setInputType(InputType.TYPE_NULL);
        edTglLahir.setTextIsSelectable(true);
        edTglLahir.setFocusable(false);
        btnNext = (Button) findViewById(R.id.btnNext);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        // setting toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.register_customer));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");

        edSpu.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        btnNext.setOnClickListener(this);
        btnNext.setTypeface(font);
        edTglLahir.setOnClickListener(this);

        //set dari QRCODE
        Intent i = getIntent();
        bookingCode = i.getStringExtra("bookingCode");
        birthDate = i.getStringExtra("birthDate");
        qrCode = i.getBooleanExtra("qrCode", false);

        if (qrCode) {
            edSpu.setText(bookingCode);
            edTglLahir.setText(birthDate);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNext:
//                  lakukan pengecekan spu / sp . setelah itu akan langsung menuju halaman
//                  register activity {RegisterStepOneActivity}. di activity ini akan menampilkan
//                  usernam, password dan kebutuhan lain yang didapat dari backend
                if (edSpu.getText().toString().isEmpty() || edTglLahir.getText().toString().isEmpty()) {
                    if (edSpu.getText().toString().isEmpty()) {
                        edSpu.setError(getResources().getString(R.string.harus_diisi));
                    } else {
                        edSpu.setError(null);
                    }

                    if (edTglLahir.getText().toString().isEmpty()) {
                        edTglLahir.setError(getResources().getString(R.string.harus_diisi));
                    } else {
                        edTglLahir.setError(null);
                    }

                } else {
                    fetchCekLoginCustomer();
                }

                break;
            case R.id.ed_tglLahir:
                dialogPicker();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * fungsi untuk cek login customer
     */
    public void fetchCekLoginCustomer() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getRegisterCustomerSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    BaseApplication.showLog("result register", response);
                    int statusCekLogin = jo.getInt("status");
                    String message = jo.getString("message");

                    if (statusCekLogin == 200) {
                        Intent intent = new Intent(RegisterCustomerActivity.this, RegisterStepTwoActivity.class);
                        intent.putExtra("phone", jo.getString("hp1"));
                        intent.putExtra("email", jo.getString("email1"));
                        intent.putExtra("memberTypeName", jo.getString("memberTypeName"));
                        intent.putExtra(RegisterStepTwoActivity.NAME, jo.getString("name"));
                        intent.putExtra("memberType", jo.getString("memberType"));
                        intent.putExtra(RegisterStepTwoActivity.BIRTHDATE, jo.getString("birthDate"));
                        intent.putExtra("projectPsRef", jo.getString("projectPsRef"));
                        intent.putExtra(General.INTENT_CAT_LOGIN, General.LOGIN_CUSTOMER);
                        startActivity(intent);
                    } else if (statusCekLogin == 201) { // failed
//                        txtEmail.setError(message);
                        btnNext.setEnabled(true);
                        Toast.makeText(RegisterCustomerActivity.this, message, Toast.LENGTH_LONG).show();
                    } else if (statusCekLogin == 202) { // user sudah terdaftar, tinggal login saja
                        notifLoginDialog(message, 202);
                        btnNext.setEnabled(true);
                    } else if (statusCekLogin == 203) { // user sudah terdaftar, tinggal login saja
                        notifLoginDialog(message, 203);
                        btnNext.setEnabled(true);
                    } else if (statusCekLogin == 204) {
                        Intent intent = new Intent(RegisterCustomerActivity.this, RegisterStepTwoActivity.class);
                        intent.putExtra("phone", jo.getString("hp1"));
                        intent.putExtra("email", jo.getString("email1"));
                        intent.putExtra("memberTypeName", jo.getString("memberTypeName"));
                        intent.putExtra(RegisterStepTwoActivity.NAME, jo.getString("name"));
                        intent.putExtra("memberType", jo.getString("memberType"));
                        intent.putExtra(RegisterStepTwoActivity.BIRTHDATE, jo.getString("birthDate"));
                        intent.putExtra("projectPsRef", jo.getString("projectPsRef"));
                        intent.putExtra(General.INTENT_CAT_LOGIN, General.LOGIN_CUSTOMER);
                        startActivity(intent);
                    } else {
                        Toast.makeText(RegisterCustomerActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterCustomerActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterCustomerActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("bookingCode", edSpu.getText().toString().trim());
                params.put("birthDate", edTglLahir.getText().toString().trim());
                params.put("projectCode", General.projectCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "registerCustomerSvc");

    }

    /**
     * notifikasi agar user login, karena user sudah terdaftar.
     */
    private void notifLoginDialog(String message, int buttonStatus) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        if (buttonStatus == 202) {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.btn_login), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
                }
            });
        } else {
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    /**
     * dialog picker untuk menampilkan pemilihan tanggal
     */
    private void dialogPicker() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        new DatePickerDialog(RegisterCustomerActivity.this, R.style.DatePickerDialogTheme, date,
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    /**
     * update label setelah memilih tanggals
     */
    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edTglLahir.setFocusable(false);
        edTglLahir.setInputType(InputType.TYPE_NULL);
        edTglLahir.setTextIsSelectable(true);
        edTglLahir.setText(sdf.format(myCalendar.getTime()));
    }
}
