package com.nataproperty.solourbana.view.mainmenu.model;

/**
 * Created by arifcebe
 * on Mar 3/16/17 15:03.
 * Project : wikaAppsAndroid
 */

public class MainMenuModel {

    private int menuIcon;
    private String menuTitle;

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }
}
