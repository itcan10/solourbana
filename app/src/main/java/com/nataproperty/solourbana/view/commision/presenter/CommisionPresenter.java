package com.nataproperty.solourbana.view.commision.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.commision.intractor.PresenterCommisionInteractor;
import com.nataproperty.solourbana.view.commision.model.CommisionModel;
import com.nataproperty.solourbana.view.commision.ui.CommisionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CommisionPresenter implements PresenterCommisionInteractor {
    private CommisionActivity view;
    private ServiceRetrofit service;

    public CommisionPresenter(CommisionActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetListCommissionProjectSvc(String dbMasterRef, String projectRef, String projectPsRef, String memberRef) {
        Call<List<CommisionModel>> call = service.getAPI().GetListCommissionProjectSvc(dbMasterRef,projectRef,projectPsRef,memberRef);
        call.enqueue(new Callback<List<CommisionModel>>() {
            @Override
            public void onResponse(Call<List<CommisionModel>> call, Response<List<CommisionModel>> response) {
                view.showListCommisionResults(response);
            }

            @Override
            public void onFailure(Call<List<CommisionModel>> call, Throwable t) {
                view.showListCommisionnFailure(t);

            }


        });
    }

}
