package com.nataproperty.solourbana.view.logbook.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface LogBookSectionInteractor {
    void GetSectionContactBankProjectSvc(String memberRef, String dbMasterRef, String projectRef);

}
