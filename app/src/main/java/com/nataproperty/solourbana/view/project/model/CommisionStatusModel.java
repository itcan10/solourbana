package com.nataproperty.solourbana.view.project.model;

import com.nataproperty.solourbana.view.report.model.CheckAvailableReportModel;

/**
 * Created by nata on 11/25/2016.
 */

public class CommisionStatusModel {
    int status;
    String message;
    CommisionDetailModel data;
    CheckAvailableReportModel dataReport;

    public CheckAvailableReportModel getDataReport() {
        return dataReport;
    }

    public void setDataReport(CheckAvailableReportModel dataReport) {
        this.dataReport = dataReport;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CommisionDetailModel getData() {
        return data;
    }

    public void setData(CommisionDetailModel data) {
        this.data = data;
    }
}
