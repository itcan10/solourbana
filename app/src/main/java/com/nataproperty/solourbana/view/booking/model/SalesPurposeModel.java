package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by User on 11/3/2016.
 */
public class SalesPurposeModel {
    String salesPurpose;
    String salesPurposeName;

    public String getSalesPurpose() {
        return salesPurpose;
    }

    public void setSalesPurpose(String salesPurpose) {
        this.salesPurpose = salesPurpose;
    }

    public String getSalesPurposeName() {
        return salesPurposeName;
    }

    public void setSalesPurposeName(String salesPurposeName) {
        this.salesPurposeName = salesPurposeName;
    }
}
