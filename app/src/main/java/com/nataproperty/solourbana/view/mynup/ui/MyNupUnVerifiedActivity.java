package com.nataproperty.solourbana.view.mynup.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mynup.adapter.UnVerifiedAdapter;
import com.nataproperty.solourbana.view.mynup.model.UnVerifiedModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyNupUnVerifiedActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String DB_MASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String NUP_ORDE_CODE = "nupOrderCode";
    public static final String BUY_DATE = "buyDate";
    public static final String PAYMENT_TYPE_NAME = "paymentTypeName";
    public static final String QTY = "qty";
    public static final String PP_AMT = "ppAmt";
    public static final String TOTAL_AMT = "totalAmt";
    public static final String NUP_ORDER_DETAIL_REF = "nupOrderDetailRef";
    public static final String NUP_ORDER_STATUS = "nupOrderStatus";
    public static final String COSTUMER_NAME = "customerName";
    public static final String PROJECT_NAME = "projectName";
    public static final String NUP_NO = "nupNo";

    public static final String STATUS = "status";

    String dbMasterRef, projectRef, nupOrderCode, buyDate, paymentTypeName, qty, ppAmt, totalAmt, nupOrderDetailRef, nupOrderStatus,
            customerName, projectName, nupNo, status;

    SharedPreferences sharedPreferences;

    private ArrayList<UnVerifiedModel> listUnVeriFied = new ArrayList<UnVerifiedModel>();
    private UnVerifiedAdapter adapter;

    String memberRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_nup_un_verified);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.tab_title_un_verified));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Log.d("cek", "" + memberRef);

        requestListUnVerified();

        final MyListView listView = (MyListView) findViewById(R.id.list_un_verified);
        adapter = new UnVerifiedAdapter(MyNupUnVerifiedActivity.this, listUnVeriFied);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dbMasterRef = listUnVeriFied.get(position).getDbMasterRef();
                projectRef = listUnVeriFied.get(position).getProjectRef();
                nupOrderCode = listUnVeriFied.get(position).getNupOrderCode();
                buyDate = listUnVeriFied.get(position).getBuyDate();
                paymentTypeName = listUnVeriFied.get(position).getPaymentTypeName();
                qty = listUnVeriFied.get(position).getQty();
                ppAmt = listUnVeriFied.get(position).getPpAmt();
                totalAmt = listUnVeriFied.get(position).getTotalAmt();
                nupOrderDetailRef = listUnVeriFied.get(position).getNupOrderDetailRef();
                nupOrderStatus = listUnVeriFied.get(position).getNupOrderStatus();
                customerName = listUnVeriFied.get(position).getCustomerName();
                projectName = listUnVeriFied.get(position).getProjectName();
                projectName = listUnVeriFied.get(position).getProjectName();
                nupNo = listUnVeriFied.get(position).getNupNo();

                Intent intent = new Intent(MyNupUnVerifiedActivity.this, MyNupDetailActivity.class);
                intent.putExtra(DB_MASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(NUP_ORDE_CODE, nupOrderCode);
                intent.putExtra(BUY_DATE, buyDate);
                intent.putExtra(PAYMENT_TYPE_NAME, paymentTypeName);
                intent.putExtra(QTY, qty);
                intent.putExtra(PP_AMT, ppAmt);
                intent.putExtra(TOTAL_AMT, totalAmt);
                intent.putExtra(NUP_ORDER_DETAIL_REF, nupOrderDetailRef);
                intent.putExtra(NUP_ORDER_STATUS, nupOrderStatus);
                intent.putExtra(COSTUMER_NAME, customerName);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(NUP_NO, nupNo);
                intent.putExtra(STATUS, "3");
                startActivity(intent);
            }
        });
    }

    public void requestListUnVerified() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getWaitingVerificationForProjectSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result unpaid", response);
                    generateListPaid(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(MyNupUnVerifiedActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(MyNupUnVerifiedActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("projectCode", General.projectCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestListUnVerified");

    }

    private void generateListPaid(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                UnVerifiedModel UnVerified = new UnVerifiedModel();
                UnVerified.setDbMasterRef(jo.getString("dbMasterRef"));
                UnVerified.setProjectRef(jo.getString("projectRef"));
                UnVerified.setNupOrderCode(jo.getString("nupOrderCode"));
                UnVerified.setBuyDate(jo.getString("buyDate"));
                UnVerified.setPaymentTypeName(jo.getString("paymentTypeName"));
                UnVerified.setQty(jo.getString("qty"));
                UnVerified.setPpAmt(jo.getString("ppAmt"));
                UnVerified.setTotalAmt(jo.getString("totalAmt"));
                UnVerified.setNupOrderDetailRef(jo.getString("nupOrderDetailRef"));
                UnVerified.setNupOrderStatus(jo.getString("nupOrderStatus"));
                UnVerified.setCustomerName(jo.getString("customerName"));
                UnVerified.setProjectName(jo.getString("projectName"));
                UnVerified.setNupNo(jo.getString("nupNo"));

                listUnVeriFied.add(UnVerified);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyNupUnVerifiedActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
