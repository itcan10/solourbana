package com.nataproperty.solourbana.view.project.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.project.adapter.RVMyListAdapter;
import com.nataproperty.solourbana.view.project.model.MyListingModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by UserModel on 5/2/2016.
 */
public class MyListingActivity extends AppCompatActivity {
    public static final String TAG = "MyListingActivity";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";

    public static final String MEMBER_REF = "memberRef";

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<MyListingModel> listProject = new ArrayList<>();

    SharedPreferences sharedPreferences;
    RecyclerView rv_myproject;
    private boolean state;
    private String memberRef;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    Display display;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylisting);
        initWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        String name = sharedPreferences.getString("isName", null);
        requestProject();
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyListingActivity.this, LaunchActivity.class));
                finish();
            }
        });
    }

    public void requestProject() {
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMyListingForProjectSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                snackBarBuatan.setVisibility(View.GONE);
                LoadingBar.stopLoader();
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()>0){
                        generateListProject(jsonArray);
//                        for ( int i = 0; i < jsonArray.length();i++) {
//                            JSONObject data = jsonArray.getJSONObject(i);
//                            Gson gson = new GsonBuilder().serializeNulls().create();
//                            MyListingModel projectModel = gson.fromJson("" + data, MyListingModel.class);
//                            projectModel.setDbMasterRef(data.optLong("dbMasterRef"));
//                            projectModel.setProjectRef(data.optString("projectRef"));
//                            projectModel.setLocationRef(data.optString("locationRef"));
//                            projectModel.setLocationName(data.optString("locationName"));
//                            projectModel.setSublocationRef(data.optString("sublocationRef"));
//                            projectModel.setSubLocationName(data.optString("subLocationName"));
//                            projectModel.setProjectName(data.optString("projectName"));
//                            projectModel.setIsJoin(data.optString("isJoin"));
//                            projectModel.setIsWaiting(data.optString("isWaiting"));
//                            MyLIstingModelDao projectModelDao = daoSession.getMyLIstingModelDao();
//                            projectModelDao.insertOrReplace(projectModel);
//                        }
                    } else {
                        AlertDialog.Builder alertDialogBuilder =
                                new AlertDialog.Builder(MyListingActivity.this);
                        alertDialogBuilder.setMessage("Anda belum join project, pilih project sekarang.");
                        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(MyListingActivity.this, ProjectActivity.class));
                                finish();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();

                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        snackBarBuatan.setVisibility(View.VISIBLE);
                        LoadingBar.stopLoader();
//                        MyLIstingModelDao projectModelDao = daoSession.getMyLIstingModelDao();
//                        List<MyListingModel> projectModels = projectModelDao.queryBuilder()
//                                .list();
//                        Log.d("getProjectLocalDB", "" + projectModels.toString());
//                        int numData = projectModels.size();
//                        if (numData > 0) {
//                            for (int i = 0; i<numData; i++) {
//                                MyListingModel projectModel = projectModels.get(i);
//                                listProject.add(projectModel);
//                                initializeAdapter();
//
//                            }
//                        }
//                        else {
//                            finish();
//                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(MEMBER_REF, memberRef);
                params.put("projectCode", General.projectCode);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "inpute");

    }

    private void generateListProject(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                MyListingModel project = new MyListingModel();
                project.setDbMasterRef(jo.getLong("dbMasterRef"));
                project.setProjectRef(jo.getString("projectRef"));
                project.setLocationRef(jo.getString("locationRef"));
                project.setLocationName(jo.getString("locationName"));
                project.setSublocationRef(jo.getString("sublocationRef"));
                project.setSubLocationName(jo.getString("subLocationName"));
                project.setProjectName(jo.getString("projectName"));
                project.setIsWaiting(jo.getString("isWaiting"));
                project.setIsJoin(jo.getString("isJoin"));

                listProject.add(project);
                initializeAdapter();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("My Project");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        rv_myproject = (RecyclerView) findViewById(R.id.rv_myproject);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_myproject.setLayoutManager(llm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyListingActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void initializeAdapter() {
        RVMyListAdapter adapter = new RVMyListAdapter(this, listProject, display);
        rv_myproject.setAdapter(adapter);
    }

}
