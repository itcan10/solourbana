package com.nataproperty.solourbana.view.report.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.report.model.ResponGenerateReportModel;
import com.nataproperty.solourbana.view.report.presenter.ReportDetailPresenter;

import retrofit2.Response;

public class ReportDetailActivity extends AppCompatActivity {
    public static final String TAG = "ReportDetailActivity";
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private ReportDetailPresenter presenter;

    Toolbar toolbar;
    TextView title;
    Typeface font;
    WebView webContent;
    String link, dbMasterRef = "", projectRef = "", memberRef, userRef;
    ProgressBar progressBar;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_detail);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new ReportDetailPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        userRef = intent.getStringExtra("userRef");
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");

        initWidget();

        Log.d(TAG, dbMasterRef + " " + projectRef);

        if (dbMasterRef != null && projectRef != null) {
            requestLinkProject();
        } else {
            requestLink();
        }

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.menu_report));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        webContent = (WebView) findViewById(R.id.web_content);
        progressBar = (ProgressBar) findViewById(R.id.progress1);
    }

    private void requestLink() {
        presenter.GenerateDashboardReportSvc(General.projectCode, "", "", memberRef, userRef);
    }

    private void requestLinkProject() {
        presenter.GenerateDashboardReportSvc("", dbMasterRef, projectRef, memberRef, userRef);
    }

    public void showGenerateResults(Response<ResponGenerateReportModel> response) {
        if (response.isSuccessful())
            link = response.body().getUrlReport();
        initWebView();

    }

    public void showGenerateFailure(Throwable t) {

    }

    private void initWebView() {
        webContent.setWebViewClient(new myWebClient());
        webContent.getSettings().setJavaScriptEnabled(true);
        webContent.getSettings().setBuiltInZoomControls(true);
        webContent.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.GONE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        webContent.loadUrl(link);
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webContent.canGoBack()) {
            webContent.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
