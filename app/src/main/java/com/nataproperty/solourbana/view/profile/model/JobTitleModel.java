package com.nataproperty.solourbana.view.profile.model;

/**
 * Created by UserModel on 5/15/2016.
 */
public class JobTitleModel {
    String jobTitleRef,jobTitleName,jobTitleSortNo;

    public String getJobTitleRef() {
        return jobTitleRef;
    }

    public void setJobTitleRef(String jobTitleRef) {
        this.jobTitleRef = jobTitleRef;
    }

    public String getJobTitleName() {
        return jobTitleName;
    }

    public void setJobTitleName(String jobTitleName) {
        this.jobTitleName = jobTitleName;
    }

    public String getJobTitleSortNo() {
        return jobTitleSortNo;
    }

    public void setJobTitleSortNo(String jobTitleSortNo) {
        this.jobTitleSortNo = jobTitleSortNo;
    }
}
