package com.nataproperty.solourbana.view.menuitem.presenter;

import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.menuitem.intractor.ContactUsInteractor;
import com.nataproperty.solourbana.view.menuitem.model.ContactUsModel;
import com.nataproperty.solourbana.view.menuitem.ui.ContactUsActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by arifcebe
 * on Mar 3/9/17 15:42.
 * Project : adhipersadaproperti
 */

public class ContactUsPresenter implements ContactUsInteractor {

    private static final String TAG = ContactUsPresenter.class.getSimpleName();
    private ContactUsActivity view;
    private ServiceRetrofitProject retrofitProject;

    public ContactUsPresenter(ContactUsActivity view, ServiceRetrofitProject retrofitProject) {
        this.view = view;
        this.retrofitProject = retrofitProject;
    }

    @Override
    public void sendFeedback(String contactName, String contactEmail, String contactSubject, String contactMessage, String projectCode) {
        Call<ContactUsModel> callContact = retrofitProject.getAPI()
                .saveFeedbackProjectSvc(contactName,contactEmail,contactSubject,contactMessage,projectCode);
        view.onLoading(true);
        callContact.enqueue(new Callback<ContactUsModel>() {
            @Override
            public void onResponse(Call<ContactUsModel> call, Response<ContactUsModel> response) {
                view.onLoading(false);
                if (response.body().getStatus().equals("200")) {
                    view.successResponseFeedback();
                } else{
                    view.failedResponseFeedback(General.NETWORK_ERROR_PERMISSION_ACCESS);
                }
            }

            @Override
            public void onFailure(Call<ContactUsModel> call, Throwable t) {
                view.onLoading(false);
                view.failedResponseFeedback(General.NETWORK_ERROR_CONNCECTION);
            }
        });
    }
}
