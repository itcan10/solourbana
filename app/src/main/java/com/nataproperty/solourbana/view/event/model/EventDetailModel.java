package com.nataproperty.solourbana.view.event.model;

/**
 * Created by UserModel on 6/11/2016.
 */
public class EventDetailModel {
    String imageSlider;

    public String getImageSlider() {
        return imageSlider;
    }

    public void setImageSlider(String imageSlider) {
        this.imageSlider = imageSlider;
    }
}
