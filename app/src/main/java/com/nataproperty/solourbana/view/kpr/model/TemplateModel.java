package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by UserModel on 7/13/2016.
 */
public class TemplateModel {
    String calcPayTermRef,calcUserProjectRef,payTermName,installmentType,dpType,bookingFeeDate,bookingFee,numOfInst,
            daysOfInst,cluster,product,block,unit;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCalcPayTermRef() {
        return calcPayTermRef;
    }

    public void setCalcPayTermRef(String calcPayTermRef) {
        this.calcPayTermRef = calcPayTermRef;
    }

    public String getCalcUserProjectRef() {
        return calcUserProjectRef;
    }

    public void setCalcUserProjectRef(String calcUserProjectRef) {
        this.calcUserProjectRef = calcUserProjectRef;
    }

    public String getPayTermName() {
        return payTermName;
    }

    public void setPayTermName(String payTermName) {
        this.payTermName = payTermName;
    }

    public String getInstallmentType() {
        return installmentType;
    }

    public void setInstallmentType(String installmentType) {
        this.installmentType = installmentType;
    }

    public String getDpType() {
        return dpType;
    }

    public void setDpType(String dpType) {
        this.dpType = dpType;
    }

    public String getBookingFeeDate() {
        return bookingFeeDate;
    }

    public void setBookingFeeDate(String bookingFeeDate) {
        this.bookingFeeDate = bookingFeeDate;
    }

    public String getBookingFee() {
        return bookingFee;
    }

    public void setBookingFee(String bookingFee) {
        this.bookingFee = bookingFee;
    }

    public String getNumOfInst() {
        return numOfInst;
    }

    public void setNumOfInst(String numOfInst) {
        this.numOfInst = numOfInst;
    }

    public String getDaysOfInst() {
        return daysOfInst;
    }

    public void setDaysOfInst(String daysOfInst) {
        this.daysOfInst = daysOfInst;
    }
}
