package com.nataproperty.solourbana.view.mynup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mynup.model.MyNupProjectPsModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class MyNupProjectPsAdapter extends BaseAdapter {
    private Context context;
    private List<MyNupProjectPsModel> list;
    private ListUnPaidHolder holder;

    public MyNupProjectPsAdapter(Context context, List<MyNupProjectPsModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_my_nup_project, null);
            holder = new ListUnPaidHolder();
            holder.orderCode = (TextView) convertView.findViewById(R.id.txt_nup_no);
            holder.projectName = (TextView) convertView.findViewById(R.id.txt_project_name);
            holder.buyDate = (TextView) convertView.findViewById(R.id.txt_buy_date);
            holder.ppName = (TextView) convertView.findViewById(R.id.txt_pp_name);
            holder.nupStatus = (TextView) convertView.findViewById(R.id.txt_nup_status_name);
            holder.totalAmt = (TextView) convertView.findViewById(R.id.txt_total_amt);

            convertView.setTag(holder);
        } else {
            holder = (ListUnPaidHolder) convertView.getTag();
        }

        MyNupProjectPsModel myNupProjectPsModel = list.get(position);

        holder.orderCode.setText(myNupProjectPsModel.getNupNo());
        holder.projectName.setText(myNupProjectPsModel.getProjectName());
        holder.buyDate.setText(myNupProjectPsModel.getBuyDate());
        holder.ppName.setText(myNupProjectPsModel.getNupName());
        holder.nupStatus.setText(myNupProjectPsModel.getNupStatusName());
        holder.totalAmt.setText(myNupProjectPsModel.getAmount());

        return convertView;
    }

    private class ListUnPaidHolder {
        TextView orderCode, qty, totalAmt, projectName, ppNup, ppName, buyDate, nupStatus;
    }
}
