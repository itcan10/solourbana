package com.nataproperty.solourbana.view.profile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by UserModel on 5/15/2016.
 */
public class MaritalStatusModel {
    @SerializedName("maritalStatus")
    String maritalStatusRef;
    @SerializedName("maritalStatusName")
    String maritalStatusName;
    @SerializedName("maritalSortNo")
    String maritalStatusSorNo;

    public String getMaritalStatusRef() {
        return maritalStatusRef;
    }

    public void setMaritalStatusRef(String maritalStatusRef) {
        this.maritalStatusRef = maritalStatusRef;
    }

    public String getMaritalStatusName() {
        return maritalStatusName;
    }

    public void setMaritalStatusName(String maritalStatusName) {
        this.maritalStatusName = maritalStatusName;
    }

    public String getMaritalStatusSorNo() {
        return maritalStatusSorNo;
    }

    public void setMaritalStatusSorNo(String maritalStatusSorNo) {
        this.maritalStatusSorNo = maritalStatusSorNo;
    }
}
