package com.nataproperty.solourbana.view.scanQRCode.model;

/**
 * Created by arifcebe
 * on Mar 3/27/17 17:49.
 * Project : SerpongGardenApps
 */

public class ScanURLModel {
    private int status;
    private String message;
    private String urlPrint;
    private String bookingCode;
    private String birthDate;

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrlPrint() {
        return urlPrint;
    }

    public void setUrlPrint(String urlPrint) {
        this.urlPrint = urlPrint;
    }
}
