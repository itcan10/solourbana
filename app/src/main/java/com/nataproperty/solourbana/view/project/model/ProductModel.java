package com.nataproperty.solourbana.view.project.model;

/**
 * Created by UserModel on 5/3/2016.
 */
public class ProductModel {
    long productRef;
    String dbMasterRef,projectRef, clusterRef, numOfBedrooms, numOfBathrooms, titleProduct, productDescription, numOfAdditionalrooms, numOfAdditionalBathrooms,
            image , isSales, linkThreesixty, linkVR;

    public String getLinkThreesixty() {
        return linkThreesixty;
    }

    public void setLinkThreesixty(String linkThreesixty) {
        this.linkThreesixty = linkThreesixty;
    }

    public String getLinkVR() {
        return linkVR;
    }

    public void setLinkVR(String linkVR) {
        this.linkVR = linkVR;
    }

    public String getIsSales() {
        return isSales;
    }

    public void setIsSales(String isSales) {
        this.isSales = isSales;
    }

    public long getProductRef() {
        return productRef;
    }

    public void setProductRef(long productRef) {
        this.productRef = productRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }


    public String getNumOfBedrooms() {
        return numOfBedrooms;
    }

    public void setNumOfBedrooms(String numOfBedrooms) {
        this.numOfBedrooms = numOfBedrooms;
    }

    public String getNumOfBathrooms() {
        return numOfBathrooms;
    }

    public void setNumOfBathrooms(String numOfBathrooms) {
        this.numOfBathrooms = numOfBathrooms;
    }

    public String getTitleProduct() {
        return titleProduct;
    }

    public void setTitleProduct(String titleProduct) {
        this.titleProduct = titleProduct;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }


    public String getNumOfAdditionalrooms() {
        return numOfAdditionalrooms;
    }

    public void setNumOfAdditionalrooms(String numOfAdditionalrooms) {
        this.numOfAdditionalrooms = numOfAdditionalrooms;
    }


    public String getNumOfAdditionalBathrooms() {
        return numOfAdditionalBathrooms;
    }

    public void setNumOfAdditionalBathrooms(String numOfAdditionalBathrooms) {
        this.numOfAdditionalBathrooms = numOfAdditionalBathrooms;
    }
}
