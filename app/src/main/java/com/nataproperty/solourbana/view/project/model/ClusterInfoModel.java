package com.nataproperty.solourbana.view.project.model;

/**
 * Created by UserModel on 5/13/2016.
 */
public class ClusterInfoModel {
    String dbMasterRef,projectRef,categoryRef,clusterRef,clusterDescription,priceRangeStart,
            priceRangeEnd,isAllowCalculatePrice,isNUP,isBooking;

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getClusterDescription() {
        return clusterDescription;
    }

    public void setClusterDescription(String clusterDescription) {
        this.clusterDescription = clusterDescription;
    }

    public String getPriceRangeStart() {
        return priceRangeStart;
    }

    public void setPriceRangeStart(String priceRangeStart) {
        this.priceRangeStart = priceRangeStart;
    }

    public String getPriceRangeEnd() {
        return priceRangeEnd;
    }

    public void setPriceRangeEnd(String priceRangeEnd) {
        this.priceRangeEnd = priceRangeEnd;
    }

    public String getIsAllowCalculatePrice() {
        return isAllowCalculatePrice;
    }

    public void setIsAllowCalculatePrice(String isAllowCalculatePrice) {
        this.isAllowCalculatePrice = isAllowCalculatePrice;
    }

    public String getIsNUP() {
        return isNUP;
    }

    public void setIsNUP(String isNUP) {
        this.isNUP = isNUP;
    }

    public String getIsBooking() {
        return isBooking;
    }

    public void setIsBooking(String isBooking) {
        this.isBooking = isBooking;
    }
}
