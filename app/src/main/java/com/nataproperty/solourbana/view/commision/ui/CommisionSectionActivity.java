package com.nataproperty.solourbana.view.commision.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.commision.adapter.CommisionSectionAdapter;
import com.nataproperty.solourbana.view.commision.model.CommisionSectionModel;
import com.nataproperty.solourbana.view.commision.presenter.CommisionSectionPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class CommisionSectionActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    CommisionSectionPresenter presenter;
    ServiceRetrofitProject service;
    boolean rxCallInWorks = false;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    SharedPreferences sharedPreferences;
    String memberRef, dbMasterRef, projectRef, projectPsRef;
    ProgressDialog progressDialog;

    ListView listView;
    CommisionSectionAdapter adapter;
    List<CommisionSectionModel> listCommisionSection = new ArrayList<>();

    LinearLayout linearNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commision_section);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new CommisionSectionPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        requestSectionCommision();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dbMasterRef = listCommisionSection.get(position).getDbMasterRef();
                projectRef = listCommisionSection.get(position).getProjectRef();
                projectPsRef = listCommisionSection.get(position).getProjectPsRef();

                Intent intent = new Intent(CommisionSectionActivity.this, CommisionActivity.class);
                intent.putExtra("dbMasterRef", dbMasterRef);
                intent.putExtra("projectRef", projectRef);
                intent.putExtra("projectPsRef", projectPsRef);
                startActivity(intent);

            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_commision));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.list_commision_section);
        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);

    }

    private void requestSectionCommision() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListCommisionSection(memberRef.toString(), General.projectCode);
    }

    public void showListCommisionSectionResults(Response<List<CommisionSectionModel>> response) {
        progressDialog.dismiss();
        listCommisionSection = response.body();

        if (listCommisionSection.size() > 0) {
            initAdapter();
            linearNoData.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            linearNoData.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

    }

    public void showListCommisionSectionFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new CommisionSectionAdapter(CommisionSectionActivity.this, listCommisionSection);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
