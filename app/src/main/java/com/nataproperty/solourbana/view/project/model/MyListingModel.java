package com.nataproperty.solourbana.view.project.model;



/**
 * Created by herlambang-nata on 11/3/2016.
 */
public class MyListingModel {
    long dbMasterRef;
    String projectRef;
    String locationRef;
    String locationName;
    String sublocationRef;
    String subLocationName;
    String projectName;
    String isJoin;
    String image;
    String isWaiting;

    public String getIsWaiting() {
        return isWaiting;
    }

    public void setIsWaiting(String isWaiting) {
        this.isWaiting = isWaiting;
    }

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getLocationRef() {
        return locationRef;
    }

    public void setLocationRef(String locationRef) {
        this.locationRef = locationRef;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSublocationRef() {
        return sublocationRef;
    }

    public void setSublocationRef(String sublocationRef) {
        this.sublocationRef = sublocationRef;
    }

    public String getSubLocationName() {
        return subLocationName;
    }

    public void setSubLocationName(String subLocationName) {
        this.subLocationName = subLocationName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getIsJoin() {
        return isJoin;
    }

    public void setIsJoin(String isJoin) {
        this.isJoin = isJoin;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
