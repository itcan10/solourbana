package com.nataproperty.solourbana.view.mybooking.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.mybooking.intractor.PresenterMyBookingDetailInteractor;
import com.nataproperty.solourbana.view.mybooking.model.PaymentSchaduleModel;
import com.nataproperty.solourbana.view.mybooking.ui.MyBookingDetailActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookingDetailPresenter implements PresenterMyBookingDetailInteractor {
    private MyBookingDetailActivity view;
    private ServiceRetrofit service;

    public MyBookingDetailPresenter(MyBookingDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListPaymentScheduleSvc(String dbMasterRef, String projectRef, String bookingRef) {
        Call<ArrayList<PaymentSchaduleModel>> call = service.getAPI().getListPaymentScheduleSvc(dbMasterRef,projectRef,bookingRef);
        call.enqueue(new Callback<ArrayList<PaymentSchaduleModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PaymentSchaduleModel>> call, Response<ArrayList<PaymentSchaduleModel>> response) {
                view.showListPaymentScheduleResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<PaymentSchaduleModel>> call, Throwable t) {
                view.showListPaymentScheduleFailure(t);

            }


        });
    }

    @Override
    public void getBookingDetailSvc(String dbMasterRef, String projectRef, String bookingRef) {
        Call<DetailBooking> call = service.getAPI().getBookingDetailSvc(dbMasterRef,projectRef, bookingRef);
        call.enqueue(new Callback<DetailBooking>() {
            @Override
            public void onResponse(Call<DetailBooking> call, Response<DetailBooking> response) {
                view.showBookingInfoResults(response);
            }

            @Override
            public void onFailure(Call<DetailBooking> call, Throwable t) {
                view.showBookingInfoFailure(t);

            }


        });
    }
}
