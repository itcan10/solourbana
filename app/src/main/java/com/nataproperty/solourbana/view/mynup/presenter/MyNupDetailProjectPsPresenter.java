package com.nataproperty.solourbana.view.mynup.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.mynup.intractor.PresenterMyNupDetailProjectPsInteractor;
import com.nataproperty.solourbana.view.mynup.model.MyNupDetailProjectPsModel;
import com.nataproperty.solourbana.view.mynup.ui.MyNupDetailProjectPsActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyNupDetailProjectPsPresenter implements PresenterMyNupDetailProjectPsInteractor {
    private MyNupDetailProjectPsActivity view;
    private ServiceRetrofit service;

    public MyNupDetailProjectPsPresenter(MyNupDetailProjectPsActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListNupDetailProjectPs(String dbMasterRef, String projectRef, String projectPsRef, String NUPRef, String memberRef, String projectSchemeRef) {
        Call<MyNupDetailProjectPsModel> call = service.getAPI().GetNUPInfoProject(dbMasterRef, projectRef, projectPsRef, NUPRef, memberRef, projectSchemeRef);
        call.enqueue(new Callback<MyNupDetailProjectPsModel>() {
            @Override
            public void onResponse(Call<MyNupDetailProjectPsModel> call, Response<MyNupDetailProjectPsModel> response) {
                view.showListNupProjectPsResults(response);
            }

            @Override
            public void onFailure(Call<MyNupDetailProjectPsModel> call, Throwable t) {
                view.showListNupProjectPsFailure(t);

            }


        });
    }

}
