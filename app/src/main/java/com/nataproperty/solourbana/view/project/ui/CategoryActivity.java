package com.nataproperty.solourbana.view.project.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyGridView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.nup.ui.NupTermActivity;
import com.nataproperty.solourbana.view.project.adapter.CategoryAdapter;
import com.nataproperty.solourbana.view.project.model.CategoryModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by User on 5/3/2016.
 */
public class CategoryActivity extends AppCompatActivity {
    public static final String TAG = "CategoryActivity";
    private List<CategoryModel> listCategory = new ArrayList<>();
    private CategoryAdapter adapter;
    MyGridView listView;
    Display display;
    Button btnNUP;
    ImageView imgLogo;
    TextView txtProjectName;
    long categoryRef, dbMasterRef;
    String projectRef, clusterRef, projectName, isJoin, isWaiting, countCluster, isNUP, nupAmt, isBooking,
            isShowAvailableUnit, imageLogo, isSales, clusterName;
    private double latitude, longitude;
    SharedPreferences sharedPreferences;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.categorty));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        dbMasterRef = getIntent().getLongExtra(General.DBMASTER_REF, 0);
        projectRef = getIntent().getStringExtra(General.PROJECT_REF);
        projectName = getIntent().getStringExtra(General.PROJECT_NAME);
        latitude = getIntent().getDoubleExtra(General.LATITUDE, 0);
        longitude = getIntent().getDoubleExtra(General.LONGITUDE, 0);
        isNUP = getIntent().getStringExtra(General.IS_NUP);
        isBooking = getIntent().getStringExtra(General.IS_BOOKING);
        nupAmt = getIntent().getStringExtra(General.NUP_AMT);
        isShowAvailableUnit = getIntent().getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        imageLogo = getIntent().getStringExtra(General.IMAGE_LOGO);
        clusterName = getIntent().getStringExtra(General.CLUSTER_NAME);

        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        isWaiting = sharedPreferences.getString(General.IS_WAITING, null);
        isJoin = sharedPreferences.getString(General.IS_JOIN, null);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);
        if (!isNUP.equals("0")) {
            btnNUP.setVisibility(View.VISIBLE);
            if (isJoin.equals("0")) {
                if (isWaiting.equals("1")) {
                    //sudah join tetapi belum di approv
                    btnNUP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogNotJoin(getString(R.string.waitingJoinNup).replace("@projectName", projectName));
                        }
                    });

                } else {
                    //belum join
                    btnNUP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogNotJoin(getString(R.string.notJoinNup).replace("@projectName", projectName));
                        }
                    });

                }

            } else {
                //jika dia sudah join
                btnNUP.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intentNup = new Intent(CategoryActivity.this, NupTermActivity.class);
                        intentNup.putExtra(General.PROJECT_REF, projectRef);
                        intentNup.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                        intentNup.putExtra(General.NUP_AMT, nupAmt);
                        intentNup.putExtra("imageLogo", imageLogo);
                        startActivity(intentNup);
                    }
                });

            }
        } else {
            btnNUP.setVisibility(View.GONE);
        }

        Log.d("Cek Category", dbMasterRef + " " + projectRef + " " + categoryRef);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this).load(imageLogo).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyGridView) findViewById(R.id.list_category);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                countCluster = listCategory.get(position).getCountCluster();
                categoryRef = listCategory.get(position).getCategoryRef();
                clusterRef = listCategory.get(position).getClusterRef();
                isShowAvailableUnit = listCategory.get(position).getIsShowAvailableUnit();
                isSales = listCategory.get(position).getIsSales();

                if (isSales.equals("1")) {
                    if (countCluster.equals("1")) {
                        Intent intentProduct = new Intent(CategoryActivity.this, ProductActivity.class);
                        intentProduct.putExtra(General.PROJECT_REF, projectRef);
                        intentProduct.putExtra(General.DBMASTER_REF, dbMasterRef);
                        intentProduct.putExtra(General.CLUSTER_REF, clusterRef);
                        intentProduct.putExtra(General.PROJECT_NAME, projectName);
                        intentProduct.putExtra(General.CATEGORY_REF, String.valueOf(categoryRef));
                        intentProduct.putExtra(General.LATITUDE, latitude);
                        intentProduct.putExtra(General.LONGITUDE, longitude);
                        intentProduct.putExtra(General.IS_NUP, isNUP);
                        intentProduct.putExtra(General.IS_BOOKING, isBooking);
                        intentProduct.putExtra(General.NUP_AMT, nupAmt);
                        intentProduct.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                        intentProduct.putExtra(General.IMAGE_LOGO, imageLogo);
                        intentProduct.putExtra(General.CLUSTER_NAME, clusterName);
                        startActivity(intentProduct);
                    } else {
                        Intent intentCluster = new Intent(CategoryActivity.this, ClusterActivity.class);
                        intentCluster.putExtra(General.PROJECT_REF, projectRef);
                        intentCluster.putExtra(General.DBMASTER_REF, dbMasterRef);
                        intentCluster.putExtra(General.CATEGORY_REF, categoryRef);
                        intentCluster.putExtra(General.PROJECT_NAME, projectName);
                        intentCluster.putExtra(General.LATITUDE, latitude);
                        intentCluster.putExtra(General.LONGITUDE, longitude);
                        intentCluster.putExtra(General.IS_NUP, isNUP);
                        intentCluster.putExtra(General.IS_BOOKING, isBooking);
                        intentCluster.putExtra(General.NUP_AMT, nupAmt);
                        intentCluster.putExtra(General.IMAGE_LOGO, imageLogo);
                        intentCluster.putExtra(General.CLUSTER_NAME, clusterName);
                        startActivity(intentCluster);
                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(linearLayout, getString(R.string.comingsoon), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }

            }
        });

        requestCategory();
        initListView();
    }

    public void requestCategory() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCategory(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCategory(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, getString(R.string.offline), Snackbar.LENGTH_INDEFINITE)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivity(new Intent(CategoryActivity.this, LaunchActivity.class));
                                        finish();
                                    }
                                });

                        snackbar.setActionTextColor(Color.RED);
                        snackbar.show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "category");

    }

    private void generateListCategory(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CategoryModel category = new CategoryModel();
                category.setDbMasterRef(jo.getString("dbMasterRef"));
                category.setProjectRef(jo.getString("projectRef"));
                category.setCategoryRef(jo.getLong("categoryRef"));
                category.setProjectCategoryType(jo.getString("projectCategoryType"));
                category.setCategoryDescription(jo.getString("categoryDescription"));
                category.setCountCluster(jo.getString("countCluster"));
                category.setClusterRef(jo.getString("clusterRef"));
                category.setIsShowAvailableUnit(jo.getString("isShowAvailableUnit"));
                category.setImage(jo.getString("image"));
                category.setIsSales(jo.getString("isSales"));
                listCategory.add(category);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void initListView() {
        adapter = new CategoryAdapter(this, listCategory, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    private void dialogNotJoin(String statusJoin) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CategoryActivity.this);
        LayoutInflater inflater = CategoryActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView textView = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Notifikasi");
        textView.setText(statusJoin);
        dialogBuilder.setPositiveButton("GO TO PROJECT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intentProjectMenu = new Intent(CategoryActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                finish();
            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CategoryActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
