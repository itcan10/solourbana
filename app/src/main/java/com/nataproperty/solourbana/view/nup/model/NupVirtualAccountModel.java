package com.nataproperty.solourbana.view.nup.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nata on 7/16/2018.
 */

public class NupVirtualAccountModel implements Parcelable {
    String noVA;

    public NupVirtualAccountModel(String noVA) {
        this.noVA = noVA;
    }

    protected NupVirtualAccountModel(Parcel in) {
        noVA = in.readString();
    }

    public NupVirtualAccountModel() {
    }

    public String getNoVA() {
        return noVA;
    }

    public void setNoVA(String noVA) {
        this.noVA = noVA;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(noVA);
    }

    public static final Creator<NupVirtualAccountModel> CREATOR = new Creator<NupVirtualAccountModel>() {
        @Override
        public NupVirtualAccountModel createFromParcel(Parcel in) {
            return new NupVirtualAccountModel(in);
        }

        @Override
        public NupVirtualAccountModel[] newArray(int size) {
            return new NupVirtualAccountModel[size];
        }
    };
}
