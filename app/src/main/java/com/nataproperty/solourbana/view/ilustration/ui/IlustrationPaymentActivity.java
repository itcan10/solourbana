package com.nataproperty.solourbana.view.ilustration.ui;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.booking.ui.BookingTermActivity;
import com.nataproperty.solourbana.view.ilustration.adapter.IlustrationPaymentAdapter;
import com.nataproperty.solourbana.view.ilustration.adapter.IlustrationPaymentAdapter2;
import com.nataproperty.solourbana.view.ilustration.adapter.IlustrationPaymentBankKPRAdapter;
import com.nataproperty.solourbana.view.ilustration.model.GenerateKalkulatorKPRParam;
import com.nataproperty.solourbana.view.ilustration.presenter.IlustrationPaymentPresenter;
import com.nataproperty.solourbana.view.kpr.adapter.KprTableAdapter;
import com.nataproperty.solourbana.view.kpr.model.DataKPR;
import com.nataproperty.solourbana.view.kpr.model.DataSchedule;
import com.nataproperty.solourbana.view.kpr.model.DataSchedule2;
import com.nataproperty.solourbana.view.kpr.model.PaymentCCStatusModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by UserModel on 5/13/2016.
 */
public class IlustrationPaymentActivity extends AppCompatActivity {

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    DownloadFileAsync dfa = new DownloadFileAsync();

    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PROJECT_NAME = "projectName";
    public static final String IS_BOOKING = "isBooking";

    private List<DataSchedule> listPayment = new ArrayList<>();
    private List<DataSchedule2> listPayment2 = new ArrayList<>();
    private IlustrationPaymentAdapter adapter;
    private IlustrationPaymentAdapter2 adapter2;

    ImageView imgLogo;
    TextView txtProjectName;

    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtView, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    private TextView txtTerm;
    private TextView txtTotal;
    private MyListView listView, listViewKprTable;
    private Button btnBooking, btnSendtoFriend;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private IlustrationPaymentPresenter presenter;
    ProgressDialog progressDialog;
    ProgressDialog mProgressDialog;

    MyListView listviewBankKPR;
    IlustrationPaymentBankKPRAdapter adapterBankKPR;
    ImageView ivDividerBankKPR;
    TextView txtTitleBankKPR;

    String dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo, projectName,memberRef;
    String propertys, category, product, unit, area, priceInc, bookingContact = "",viewName;
    String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, netPrice2, termCondition, view,va1, va2;
    String total;
    String isBooking = "", stringListKpr;
    String incomeAmt, tenor;

    private int finType;
    private List<DataKPR> listKprTable = new ArrayList<>();
    private KprTableAdapter adapterKprTable;
    private LinearLayout linearLayout, linearLayoutByKPR;

    private TextView txtNilaiKpr;
    private String nilaiKpr;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Point size;
    Integer width;
    Double result;
    FloatingActionButton fab;
    String strToday,urlDown,fileName;

    SharedPreferences sharedPreferences;

//    static IlustrationPaymentActivity ilustrationPaymentActivity;

    AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_payment);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new IlustrationPaymentPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        isBooking = intent.getStringExtra(IS_BOOKING);
        finType = intent.getIntExtra("finType", 0);
        stringListKpr = intent.getStringExtra("stringListKpr");
        propertys = intent.getStringExtra("propertys");
        category = intent.getStringExtra("category");
        product = intent.getStringExtra("product");
        unit = intent.getStringExtra("unit");
        area = intent.getStringExtra("area");
        priceInc = intent.getStringExtra("priceInc");
        netPrice2 = intent.getStringExtra("netPrice");
        bookingContact = intent.getStringExtra("bookingContact");
        viewName = intent.getStringExtra("viewName");
        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");
        incomeAmt = intent.getStringExtra("incomeAmt");
        tenor = intent.getStringExtra("tenor");

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

//        if (finType == 2) {
//            if (incomeAmt.equals("")) {
//                incomeAmt = "0";
//            }
//        }
//
//        if (finType == 2) {
//            if (tenor.equals("")) {
//                tenor = "0";
//            }
//        }


        initWidget();

//        ilustrationPaymentActivity = this;

        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        Log.d("get intent", "finType" + finType + "-" + dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" + unit + "-" +
                termRef + "-" + termNo);
        Log.d("isBooking ", "" + isBooking);

        if (finType == 2) {
            if (!"".equals(stringListKpr) && !incomeAmt.equals("") && !tenor.equals("")) {
                linearLayout.setVisibility(View.VISIBLE);
                txtTitleBankKPR.setVisibility(View.VISIBLE);
                linearLayoutByKPR.setVisibility(View.VISIBLE);
            } else {
                if (!"".equals(stringListKpr)) {  // kpr - ilangin kalkulator advance
                    linearLayout.setVisibility(View.VISIBLE);
                    txtTitleBankKPR.setVisibility(View.GONE);
                    listviewBankKPR.setVisibility(View.GONE);
                    linearLayoutByKPR.setVisibility(View.VISIBLE);
                } else if (!incomeAmt.equals("") && !tenor.equals("")) { // kpa
                    linearLayoutByKPR.setVisibility(View.VISIBLE);
                    linearLayout.setVisibility(View.GONE);
                    txtTitleBankKPR.setVisibility(View.VISIBLE);
                } else {  // bukan kpr atau kpa, atau kpa tapi tidak diinput
                    linearLayout.setVisibility(View.GONE);
                    linearLayoutByKPR.setVisibility(View.GONE);
                    listviewBankKPR.setVisibility(View.GONE);
                    txtTitleBankKPR.setVisibility(View.GONE);
                }
            }

            requestPaymentKpr();
        } else {
            requestPayment();
            linearLayout.setVisibility(View.GONE);
            listviewBankKPR.setVisibility(View.GONE);
            linearLayoutByKPR.setVisibility(View.GONE);
            txtTitleBankKPR.setVisibility(View.GONE);
            ivDividerBankKPR.setVisibility(View.GONE);
        }


        txtPropertyName.setText(propertys);
        txtCategoryType.setText(category);
        txtProduct.setText(product);
        txtUnitNo.setText(unit);
        txtArea.setText(Html.fromHtml(area));
        txtEstimate.setText(priceInc);
        txtView.setText(viewName);

        if (!isBooking.equals("0")) {
            setupBtnBooking();
        } else {
            if (bookingContact.equals("")) {
                btnBooking.setText(getString(R.string.btn_booking_disable));
                btnBooking.setEnabled(false);
                btnBooking.setAlpha(.5f);
                btnBooking.setClickable(false);
            } else {
                showBookingContact();
            }

        }

        /**
         * send to friend
         */
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkWriteExternalPermission()) {
                    showDialogDownload();
                } else {
                    showDialogCekPermission();
                    Toast.makeText(getBaseContext(), "We need the Storage Permission", Toast.LENGTH_LONG).show();
                }
            }

        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_illustrastion_payment_schedule));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        txtNilaiKpr = (TextView) findViewById(R.id.txt_nilai_kpr);
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtView = (TextView) findViewById(R.id.txt_view);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);
        listView = (MyListView) findViewById(R.id.list_payment_schedule);
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);
        txtTerm = (TextView) findViewById(R.id.txt_term);
        txtTotal = (TextView) findViewById(R.id.txt_total);


        //pengajuan kpr
        linearLayout = (LinearLayout) findViewById(R.id.linear_header_kpr);
        listViewKprTable = (MyListView) findViewById(R.id.list_kpr_table);

        //kpr advance
        ivDividerBankKPR = (ImageView) findViewById(R.id.iv_divider_bank_kpr);
        txtTitleBankKPR = (TextView) findViewById(R.id.txt_title_bank_kpr);
        listviewBankKPR = (MyListView) findViewById(R.id.list_bank_kpr);

        //txt keterangan paling bawah
        linearLayoutByKPR = (LinearLayout) findViewById(R.id.linear_by_kpr);

        btnBooking = (Button) findViewById(R.id.btn_proses_booking);
        btnSendtoFriend = (Button) findViewById(R.id.btn_send_to_friend);
        btnSendtoFriend.setTypeface(font);
        btnBooking.setTypeface(font);

    }

    public void requestPayment() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getPayment(dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo,"0",memberRef);

    }

    public void requestPaymentKpr() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        GenerateKalkulatorKPRParam generateKalkulatorKPRParam = new GenerateKalkulatorKPRParam();
        generateKalkulatorKPRParam.setDbMasterRef(dbMasterRef);
        generateKalkulatorKPRParam.setProjectRef(projectRef);
        generateKalkulatorKPRParam.setProjectCode(General.projectCode);
        generateKalkulatorKPRParam.setClusterRef(clusterRef);
        generateKalkulatorKPRParam.setProductRef(productRef);
        generateKalkulatorKPRParam.setUnitRef(unitRef);
        generateKalkulatorKPRParam.setTermRef(termRef);
        generateKalkulatorKPRParam.setTermNo(termNo);
        generateKalkulatorKPRParam.setKPRYear(stringListKpr.toString());
        generateKalkulatorKPRParam.setDiscRoomPrice("0");
        generateKalkulatorKPRParam.setIncome(incomeAmt);
        generateKalkulatorKPRParam.setTenor(tenor);
        generateKalkulatorKPRParam.setMemberRef(memberRef);
        presenter.getPaymentKpr(generateKalkulatorKPRParam);
        String js = (Utils.modelToJson(generateKalkulatorKPRParam));
        System.out.print("test"+js);
    }

    public void showPaymentResults(retrofit2.Response<PaymentCCStatusModel> response) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        total = response.body().getTotalPayment();
        if (status == 200) {
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();
            urlDown = response.body().getUrlDownload();
            fileName = response.body().getFileName();

            txtNilaiKpr.setText(nilaiKpr);
            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice2);
            txtTerm.setText(Html.fromHtml(termCondition));


            txtTotal.setText(total);
            listPayment = response.body().getDataSchedule();
            for (int i = 0; i < listPayment.size(); i++) {
                nilaiKpr = listPayment.get(i).getAmount();
            }
            initAdapterPayment();

        } else {

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    private void initAdapterPayment() {
        adapter = new IlustrationPaymentAdapter(this, listPayment);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    private void initAdapterPaymentKpr() {
        adapter2 = new IlustrationPaymentAdapter2(this, listPayment2);
        listView.setAdapter(adapter2);
        listView.setExpanded(true);

        adapterKprTable = new KprTableAdapter(this, listKprTable);
        listViewKprTable.setAdapter(adapterKprTable);
        listViewKprTable.setExpanded(true);
    }

    public void showPaymentFailure(Throwable t) {

    }

    public void showPaymentKprResults(retrofit2.Response<PaymentCCStatusModel> response) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        total = response.body().getTotalPayment();
        if (status == 200) {
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();
            urlDown = response.body().getUrlDownload();
            fileName = response.body().getFileName();

            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice);

            txtTerm.setText(Html.fromHtml(termCondition));

            txtTotal.setText(total);
            listPayment2 = response.body().getDataSchedule2();
            for (int i = 0; i < listPayment2.size(); i++) {
                nilaiKpr = listPayment2.get(i).getAmount();
            }
            txtNilaiKpr.setText(nilaiKpr);
            listKprTable = response.body().getDataKPR();
            initAdapterPaymentKpr();

            if (!incomeAmt.equals("") && !tenor.equals("")) {
                adapterBankKPR = new IlustrationPaymentBankKPRAdapter(this, response.body().getDataKalkulator(), tenor);
                listviewBankKPR.setAdapter(adapterBankKPR);
                listviewBankKPR.setExpanded(true);
            } else {
                ivDividerBankKPR.setVisibility(View.GONE);
                txtTitleBankKPR.setVisibility(View.GONE);
            }

        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showPaymentKprFailure(Throwable t) {

    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void showDialogDownload() {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHH");
        Date today = Calendar.getInstance().getTime();
        strToday = df.format(today);
        String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                +fileName + "_" + strToday + ".pdf";
        File f = new File(path);

        if (f.exists()) {
            sharedDocument();
        } else {
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(IlustrationPaymentActivity.this);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setMessage(getString(R.string.sharedIlustrasi).replace("projectName", projectName + "."));
            alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (urlDown != null){
                    startDownload(urlDown);
                    } else {
                        Toast.makeText(IlustrationPaymentActivity.this, "No input url", Toast.LENGTH_LONG).show();
                    }

                }
            });
            alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this,R.style.AlertDialogTheme);
                mProgressDialog.setMessage("Downloading file..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);

                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dfa.cancel(true);   // Add
                        String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                                +fileName + "_" + strToday  + ".pdf";
                        File f = new File(path);
                        try{
                            f.delete();
                        }catch (Exception e) {

                        }
                        dialog.dismiss();
                    }
                });

                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }

    private void sharedDocument() {
        try {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("application/pdf");
            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                    "/nataproperty/" + fileName + "_" + strToday + ".pdf";
            File imageFileToShare = new File(imagePath);
            Uri uri = Uri.fromFile(imageFileToShare);
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.putExtra(Intent.EXTRA_SUBJECT, "Ilustrasi " + paymentTerm + " " + projectName);
            share.putExtra(Intent.EXTRA_TEXT, "Berikut adalah ilustrasi cara bayar " + paymentTerm + " " + projectName);
            startActivity(Intent.createChooser(share, "Share Document!"));

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "File tidak bisa dibagikan", Toast.LENGTH_SHORT).show();
        }
    }

    private void startDownload(String url) {
        Log.d("Cek", "" + checkWriteExternalPermission());
        if (!url.equals("")) {
            dfa = new DownloadFileAsync();
            dfa.execute(url);   // Add
        } else {
            Toast.makeText(IlustrationPaymentActivity.this, "No input url", Toast.LENGTH_LONG).show();
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            int count;

            try {
                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                File wallpaperDirectory = new File(android.os.Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(wallpaperDirectory + "/" + fileName + "_" + strToday + ".pdf" );

                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                if (isCancelled()){
                    dfa.cancel(true);
                }

                output.flush();
                output.close();
                input.close();

                if (!isCancelled()) {
                    sharedDocument();
                }

            } catch (Exception e) {
                Log.d("Exception",e.getMessage());
            }
            return null;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d("ANDRO_ASYNC", values[0]);
            mProgressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

    }

    public void showDialogCekPermission() {
//        try {
//            if (ContextCompat.checkSelfPermission(IlustrationPaymentActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    != PackageManager.PERMISSION_GRANTED) {
//                if (ActivityCompat.shouldShowRequestPermissionRationale(IlustrationPaymentActivity.this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    ActivityCompat.requestPermissions(IlustrationPaymentActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            EXTERNAL_STORAGE_PERMISSION_CONSTANT);
//                }
//            }
//        } catch (SecurityException e) {
//            Toast.makeText(getBaseContext(), "We need the Storage Permission, Enable Storage Permission", Toast.LENGTH_LONG).show();
//        }

        // Here, thisActivity is the current activity
        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(IlustrationPaymentActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(IlustrationPaymentActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(IlustrationPaymentActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
        }

    }



    private void showBookingContact() {
        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IlustrationPaymentActivity.this);
                LayoutInflater inflater = IlustrationPaymentActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
                dialogBuilder.setView(dialogView);

                final TextView txtspecialEnquiries = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
                dialogBuilder.setMessage("Booking Contact");
                txtspecialEnquiries.setText(bookingContact);

                dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });

    }

    private void setupBtnBooking() {
        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBooking = new Intent(IlustrationPaymentActivity.this, BookingTermActivity.class);
                intentBooking.putExtra("dbMasterRef", dbMasterRef);
                intentBooking.putExtra("projectRef", projectRef);
                intentBooking.putExtra("categoryRef", categoryRef);
                intentBooking.putExtra("clusterRef", clusterRef);
                intentBooking.putExtra("productRef", productRef);
                intentBooking.putExtra("unitRef", unitRef);
                intentBooking.putExtra("termRef", termRef);
                intentBooking.putExtra("termNo", termNo);
                intentBooking.putExtra("va1", va1);
                intentBooking.putExtra("va2", va2);
                intentBooking.putExtra(PROJECT_NAME, projectName);
                Log.d("projectName", "" + projectName);
                startActivity(intentBooking);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationPaymentActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

    }
}
