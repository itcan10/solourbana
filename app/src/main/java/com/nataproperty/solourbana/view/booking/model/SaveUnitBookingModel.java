package com.nataproperty.solourbana.view.booking.model;

public class SaveUnitBookingModel {

    private String dbMasterRef;
    private String projectRef;
    private String clusterRef;
    private String productRef;
    private String agentMemberRef;
    private String customerMemberRef;
    private String unitRef;
    private String termNo;
    private String termRef;

    public String getTermRef() {
        return termRef;
    }

    public void setTermRef(String termRef) {
        this.termRef = termRef;
    }

    private String customerName;
    private String customerNIK;
    private String customerHP;
    private String customerPHONE;
    private String customerEMAIL;
    private String customerADDRESS;
    private String discRoomPrice;
    private String bookingSource;
    private String bookingNote;

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public String getAgentMemberRef() {
        return agentMemberRef;
    }

    public void setAgentMemberRef(String agentMemberRef) {
        this.agentMemberRef = agentMemberRef;
    }

    public String getCustomerMemberRef() {
        return customerMemberRef;
    }

    public void setCustomerMemberRef(String customerMemberRef) {
        this.customerMemberRef = customerMemberRef;
    }

    public String getUnitRef() {
        return unitRef;
    }

    public void setUnitRef(String unitRef) {
        this.unitRef = unitRef;
    }

    public String getTermNo() {
        return termNo;
    }

    public void setTermNo(String termNo) {
        this.termNo = termNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerNIK() {
        return customerNIK;
    }

    public void setCustomerNIK(String customerNIK) {
        this.customerNIK = customerNIK;
    }

    public String getCustomerHP() {
        return customerHP;
    }

    public void setCustomerHP(String customerHP) {
        this.customerHP = customerHP;
    }

    public String getCustomerPHONE() {
        return customerPHONE;
    }

    public void setCustomerPHONE(String customerPHONE) {
        this.customerPHONE = customerPHONE;
    }

    public String getCustomerEMAIL() {
        return customerEMAIL;
    }

    public void setCustomerEMAIL(String customerEMAIL) {
        this.customerEMAIL = customerEMAIL;
    }

    public String getCustomerADDRESS() {
        return customerADDRESS;
    }

    public void setCustomerADDRESS(String customerADDRESS) {
        this.customerADDRESS = customerADDRESS;
    }

    public String getDiscRoomPrice() {
        return discRoomPrice;
    }

    public void setDiscRoomPrice(String discRoomPrice) {
        this.discRoomPrice = discRoomPrice;
    }

    public String getBookingSource() {
        return bookingSource;
    }

    public void setBookingSource(String bookingSource) {
        this.bookingSource = bookingSource;
    }

    public String getBookingNote() {
        return bookingNote;
    }

    public void setBookingNote(String bookingNote) {
        this.bookingNote = bookingNote;
    }

}

