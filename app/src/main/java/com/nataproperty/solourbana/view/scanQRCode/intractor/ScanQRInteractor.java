package com.nataproperty.solourbana.view.scanQRCode.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface ScanQRInteractor {
    void CheckingDocumentProject(String projectCode, String bookingCode);

    void rxUnSubscribe();

}
