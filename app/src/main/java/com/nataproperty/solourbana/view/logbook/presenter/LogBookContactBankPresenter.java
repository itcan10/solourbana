package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.LogBookContactBankInteractor;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.ui.LogBookContactBankActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LogBookContactBankPresenter implements LogBookContactBankInteractor {
    private LogBookContactBankActivity view;
    private ServiceRetrofitProject service;

    public LogBookContactBankPresenter(LogBookContactBankActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetContactBankTaskSvc(String memberRef,String dbMasterRef,String projectRef, String customerStatusRef, String keyword,
                                  String countryCode, String provinceCode, String cityCode) {
        Call<ArrayList<LogBookModel>> call = service.getAPI().GetContactBankTaskSvc(memberRef,dbMasterRef,projectRef,customerStatusRef,keyword,countryCode,
                provinceCode,cityCode);
        call.enqueue(new Callback<ArrayList<LogBookModel>>() {
            @Override
            public void onResponse(Call<ArrayList<LogBookModel>> call, Response<ArrayList<LogBookModel>> response) {
                view.showListLogBookResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<LogBookModel>> call, Throwable t) {
                view.showListLogBookFailure(t);

            }


        });
    }

}
