package com.nataproperty.solourbana.view.logbook.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface AddLogBookInteractor {
    void getCountryListSvc();
    void getProvinceListSvc(String countryCode);
    void getCityListSvc(String countryCode, String provinceCode);
    void insertContactBankSvc (Map<String, String> fields) ;
    void GetInfoContactBankSvc(String memberRef, String customerRef, String projectCode);
    void updateContactBankSvc (Map<String, String> fields) ;

}
