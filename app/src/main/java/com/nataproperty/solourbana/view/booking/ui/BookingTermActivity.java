
package com.nataproperty.solourbana.view.booking.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/24/2016.
 */
public class BookingTermActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PROJECT_NAME = "projectName";

    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtTerm, txtBookingOnline;
    Button btnNext;
    CheckBox cbTerm;

    HtmlTextView text;

    String dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo,va1, va2;
    private String projectName, termAndCondition;

    Typeface font, fontLight;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_trem);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        projectRef = getIntent().getStringExtra(PROJECT_REF);
        dbMasterRef = getIntent().getStringExtra(DBMASTER_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");

        Log.d("projectName",""+projectName);

        /*Logo*/
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);

        cbTerm = (CheckBox) findViewById(R.id.cb_term);
        cbTerm.setTypeface(font);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setTypeface(font);
        txtBookingOnline = (TextView) findViewById(R.id.txt_booking_online);
        text = (HtmlTextView) findViewById(R.id.html_text);
        text.setTypeface(fontLight);
        text.setTextColor(Color.BLACK);

        btnNext.setEnabled(false);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingTermActivity.this, BookingSearchCostumerActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CATEGORY_REF, categoryRef);
                intent.putExtra(CLUSTER_REF, clusterRef);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(UNIT_REF, unitRef);
                intent.putExtra(TERM_REF, termRef);
                intent.putExtra(TERM_NO, termNo);
                intent.putExtra("va1", va1);
                intent.putExtra("va2", va2);
                startActivity(intent);
            }
        });

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        //requestPaymentInfo();
        requestTermAndConditionBooking();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void requestTermAndConditionBooking() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getTermAndConditionBooking(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("resultBooking", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    termAndCondition = jo.getString("termAndCondition");

                    text.setHtmlFromString(termAndCondition, new HtmlTextView.RemoteImageGetter());

                    if (status == 200) {
                        txtBookingOnline.setVisibility(View.VISIBLE);
                        text.setVisibility(View.VISIBLE);
                        cbTerm.setVisibility(View.VISIBLE);
                        cbTerm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (!isChecked) {
                                    btnNext.setEnabled(false);
                                } else {
                                    btnNext.setEnabled(true);
                                }
                            }
                        });
                    } else if (status == 201) {
                        txtBookingOnline.setVisibility(View.GONE);
                        text.setVisibility(View.GONE);
                        cbTerm.setVisibility(View.GONE);
                        btnNext.setEnabled(true);
                    } else {
                        Log.d("error", " " + message);
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingTermActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingTermActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "bookingTerm");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
