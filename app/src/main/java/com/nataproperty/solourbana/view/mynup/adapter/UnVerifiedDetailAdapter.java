package com.nataproperty.solourbana.view.mynup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mynup.model.UnVerifiedModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class UnVerifiedDetailAdapter extends BaseAdapter {
    private Context context;
    private List<UnVerifiedModel> list;
    private ListUnPaidHolder holder;

    public UnVerifiedDetailAdapter(Context context, List<UnVerifiedModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_paid_detail,null);
            holder = new ListUnPaidHolder();
            holder.nupNo = (TextView) convertView.findViewById(R.id.txt_nup_no);
            holder.orderStatus = (TextView) convertView.findViewById(R.id.txt_order_status);
            holder.costumerName = (TextView) convertView.findViewById(R.id.txt_costumer_name);

            convertView.setTag(holder);
        }else{
            holder = (ListUnPaidHolder) convertView.getTag();
        }

        UnVerifiedModel unVerifiedModel = list.get(position);

        holder.nupNo.setText(unVerifiedModel.getNupNo());
        holder.orderStatus.setText(unVerifiedModel.getNupOrderStatus());
        holder.costumerName.setText(unVerifiedModel.getCustomerName());

        Log.d("cek delail nup adapter",unVerifiedModel.getNupOrderStatus());

        return convertView;
    }

    private class ListUnPaidHolder {
        TextView nupNo,orderStatus,costumerName;
    }
}
