package com.nataproperty.solourbana.view.ilustration.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.ilustration.adapter.ColorListAdapter;
import com.nataproperty.solourbana.view.ilustration.model.ColorModel;
import com.nataproperty.solourbana.view.ilustration.model.ListBlockDiagramModel;
import com.nataproperty.solourbana.view.ilustration.model.ListUnitMappingModel;
import com.nataproperty.solourbana.view.ilustration.presenter.DiagramMaticInflatePresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Response;

public class DiagramMaticInflateActivity extends AppCompatActivity {
    private ServiceRetrofit service;
    private DiagramMaticInflatePresenter presenter;
    private List<ListBlockDiagramModel> listBlockDiagramModels = new ArrayList<>();
    TextView txtProjectName, txtNoIsShow, txtIsShow, txtFromProduct, txtCluster;
    RelativeLayout contentDiagram, contentDiagramNoDate;
    String clusterName, dbMasterRef, projectRef, projectName, isShowAvailableUnit, productRefIntent,
            titleProduct, categoryRef, clusterRef;
    int lenght;
    SharedPreferences sharedPreferences;
    String memberRef, color1, color2;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RecyclerView listColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagram_matic_inflate);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new DiagramMaticInflatePresenter(this, service);
        initWidget();

        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        contentDiagram.addView(new TableMainLayout(this));

        final Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(General.DBMASTER_REF);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        categoryRef = intent.getStringExtra(General.CATEGORY_REF);
        clusterRef = intent.getStringExtra(General.CLUSTER_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        productRefIntent = intent.getStringExtra(General.PRODUCT_REF);
        titleProduct = intent.getStringExtra(General.TITLE_PRODUCT);
        isShowAvailableUnit = intent.getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        clusterName = intent.getStringExtra(General.CLUSTER_NAME);

        title.setText(getResources().getString(R.string.title_diagarammatic) + " " + projectName);
        txtCluster.setText(clusterName);
        request();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtCluster = (TextView) findViewById(R.id.txt_cluster);
        txtIsShow = (TextView) findViewById(R.id.txt_is_show);
        txtNoIsShow = (TextView) findViewById(R.id.txt_no_is_show);
        txtFromProduct = (TextView) findViewById(R.id.txt_from_product);
        contentDiagram = (RelativeLayout) findViewById(R.id.contentRel);
        contentDiagramNoDate = (RelativeLayout) findViewById(R.id.contentNoDate);
        listColor = (RecyclerView) findViewById(R.id.list_color);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        listColor.setLayoutManager(layoutManager);

    }

    public void request() {
        presenter.getBlockMapping(dbMasterRef, projectRef, categoryRef, clusterRef, "", memberRef);
    }


    public void showUnitMappingResults(Response<List<ListUnitMappingModel>> response) {
        //listUnitMappingModels = response.body();
        //lenght = listUnitMappingModels.size();
        if (lenght != 0) {
            if (!color1.equals("") && !color2.equals("")) {
                txtIsShow.setVisibility(View.VISIBLE);
                txtNoIsShow.setVisibility(View.GONE);
                txtFromProduct.setVisibility(View.GONE);
            } else {
                if (!productRefIntent.equals("")) {
                    if (isShowAvailableUnit.equals("1")) {
                        txtIsShow.setVisibility(View.VISIBLE);
                        txtNoIsShow.setVisibility(View.GONE);
                        txtFromProduct.setVisibility(View.GONE);

                    } else {
                        txtIsShow.setVisibility(View.GONE);
                        txtNoIsShow.setVisibility(View.VISIBLE);
                        txtFromProduct.setVisibility(View.VISIBLE);
                    }

                } else {
                    if (isShowAvailableUnit.equals("1")) {
                        txtIsShow.setVisibility(View.VISIBLE);
                        txtNoIsShow.setVisibility(View.GONE);

                    } else {
                        txtIsShow.setVisibility(View.GONE);
                        txtNoIsShow.setVisibility(View.VISIBLE);
                    }
                }
            }
        } else {
            contentDiagramNoDate.setVisibility(View.VISIBLE);
            txtProjectName.setVisibility(View.GONE);
        }
    }


    public void showBlockMappingResults(Response<List<ListBlockDiagramModel>> response) {
        listBlockDiagramModels = response.body();

        String color = listBlockDiagramModels.get(0).getStatusColor();
        String colorName = listBlockDiagramModels.get(0).getStatusColorName();
        List<String> colors = Arrays.asList(color.split(","));
        List<String> colorNames = Arrays.asList(colorName.split(","));
        List<ColorModel> list = new ArrayList<>();

        for (int x = 0; x < colors.size(); x++) {
            if (!colors.get(x).toString().trim().toLowerCase().equals("#ffffff") &&
                    !colors.get(x).toString().trim().toLowerCase().equals("")) {
                ColorModel c = new ColorModel();
                c.setColor(colors.get(x));
                c.setColorName(colorNames.get(x));
                list.add(c);
            }
        }

//        if (!productRefIntent.equals("")){
//            ColorModel c = new ColorModel();
//            c.setColor("#c7c7c7");
//            c.setColorName(titleProduct);
//            list.add(c);
//        }

        ColorListAdapter adapter = new ColorListAdapter(getApplicationContext(), list);
        listColor.setAdapter(adapter);
    }

    public void showBlockMappingFailure(Throwable t) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(DiagramMaticInflateActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
