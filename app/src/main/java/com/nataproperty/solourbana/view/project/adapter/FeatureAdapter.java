package com.nataproperty.solourbana.view.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.project.model.FeatureModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class FeatureAdapter extends BaseAdapter {
    private Context context;
    private List<FeatureModel> list;
    private ListFeatureHolder holder;

    public FeatureAdapter(Context context, List<FeatureModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_feature,null);
            holder = new ListFeatureHolder();
            holder.projectFeatureName = (TextView) convertView.findViewById(R.id.txt_feature);

            convertView.setTag(holder);
        }else{
            holder = (ListFeatureHolder) convertView.getTag();
        }

        FeatureModel feature = list.get(position);
        holder.projectFeatureName.setText(feature.getProjectFeatureName());

        return convertView;

    }

    private class ListFeatureHolder {
        TextView projectFeatureName;
    }
}
