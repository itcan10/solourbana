package com.nataproperty.solourbana.view.profile.model;

/**
 * Created by UserModel on 5/15/2016.
 */
public class OccupationModel {
    String occupationRef,occupationName,occupationSortNo;

    public String getOccupationRef() {
        return occupationRef;
    }

    public void setOccupationRef(String occupationRef) {
        this.occupationRef = occupationRef;
    }

    public String getOccupationName() {
        return occupationName;
    }

    public void setOccupationName(String occupationName) {
        this.occupationName = occupationName;
    }

    public String getOccupationSortNo() {
        return occupationSortNo;
    }

    public void setOccupationSortNo(String occupationSortNo) {
        this.occupationSortNo = occupationSortNo;
    }
}
