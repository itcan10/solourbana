package com.nataproperty.solourbana.view.commision.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.commision.adapter.CommisionAdapter;
import com.nataproperty.solourbana.view.commision.model.CommisionModel;
import com.nataproperty.solourbana.view.commision.presenter.CommisionPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class CommisionActivity extends AppCompatActivity {
    public static final String TAG = "CommisionActivity";
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    CommisionPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    ListView listView;
    ProgressDialog progressDialog;
    String memberRef, dbMasterRef, projectRef, projectPsRef, bookingRef, countCommision;
    CommisionAdapter adapter;
    List<CommisionModel> listCommision = new ArrayList<>();

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commision);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new CommisionPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        projectPsRef = intent.getStringExtra("projectPsRef");

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        initWidget();

        requestCommision();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dbMasterRef = listCommision.get(position).getDbMasterRef();
                projectRef = listCommision.get(position).getDbMasterRef();
                bookingRef = listCommision.get(position).getBookingRef();
                projectPsRef = listCommision.get(position).getProjectPsRef();
                countCommision = listCommision.get(position).getCountCommision();

                if (!countCommision.trim().equals("0")) {
                    Intent intent = new Intent(CommisionActivity.this, CommisionDetailActivity.class);
                    intent.putExtra("dbMasterRef", dbMasterRef);
                    intent.putExtra("projectRef", projectRef);
                    intent.putExtra("bookingRef", bookingRef);
                    intent.putExtra("projectPsRef", projectPsRef);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "New Komisi", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void requestCommision() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetListCommissionProjectSvc(dbMasterRef, projectRef, projectPsRef,memberRef);

        Log.d(TAG, "param " + dbMasterRef + " " + projectRef + " " + projectPsRef);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_commision));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.list_commision);
    }


    public void showListCommisionResults(Response<List<CommisionModel>> response) {
        progressDialog.dismiss();
        listCommision = response.body();
        initAdapter();
    }

    public void showListCommisionnFailure(Throwable t) {
        progressDialog.dismiss();

    }

    private void initAdapter() {
        adapter = new CommisionAdapter(CommisionActivity.this, listCommision, font);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
