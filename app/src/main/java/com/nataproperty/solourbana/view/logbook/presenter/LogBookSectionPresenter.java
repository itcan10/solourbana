package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.LogBookSectionInteractor;
import com.nataproperty.solourbana.view.logbook.model.LogBookSectionModel;
import com.nataproperty.solourbana.view.logbook.ui.LogBookSectionActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LogBookSectionPresenter implements LogBookSectionInteractor {
    private LogBookSectionActivity view;
    private ServiceRetrofitProject service;

    public LogBookSectionPresenter(LogBookSectionActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void GetSectionContactBankProjectSvc(String memberRef, String dbMasterRef, String projectRef) {
        Call<ArrayList<LogBookSectionModel>> call = service.getAPI().GetSectionContactBankProjectSvc(memberRef,dbMasterRef,projectRef);
        call.enqueue(new Callback<ArrayList<LogBookSectionModel>>() {
            @Override
            public void onResponse(Call<ArrayList<LogBookSectionModel>> call, Response<ArrayList<LogBookSectionModel>> response) {
                view.showListLogBookSectionResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<LogBookSectionModel>> call, Throwable t) {
                view.showListLogBookSectionFailure(t);

            }
        });

    }
}
