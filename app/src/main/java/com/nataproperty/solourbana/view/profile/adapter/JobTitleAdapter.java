package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.JobTitleModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class JobTitleAdapter extends BaseAdapter {
    private Context context;
    private List<JobTitleModel> list;
    private ListJobTitlelHolder holder;

    public JobTitleAdapter(Context context, List<JobTitleModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_job_title,null);
            holder = new ListJobTitlelHolder();
            holder.jobTitleName = (TextView) convertView.findViewById(R.id.txt_job_title);

            convertView.setTag(holder);
        }else{
            holder = (ListJobTitlelHolder) convertView.getTag();
        }
        JobTitleModel nation = list.get(position);
        holder.jobTitleName.setText(nation.getJobTitleName());

        return convertView;
    }

    private class ListJobTitlelHolder {
        TextView jobTitleName;
    }
}
