package com.nataproperty.solourbana.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyGridView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.ilustration.presenter.IlustrationCategoryPresenter;
import com.nataproperty.solourbana.view.mainmenu.ui.MainMenuActivity;
import com.nataproperty.solourbana.view.project.adapter.CategoryAdapter;
import com.nataproperty.solourbana.view.project.model.CategoryModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

//import com.nataproperty.wika.model.project.DaoSession;

/**
 * Created by UserModel on 5/3/2016.
 */
public class IlustrationCategoryActivity extends AppCompatActivity {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String IS_NUP = "isNUP";
    public static final String NUP_AMT = "nupAmt";

    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String IS_BOOKING = "isBooking";
    public static final String PRODUCT_REF = "productRef";

    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    public static final String IMAGE_LOGO = "imageLogo";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private IlustrationCategoryPresenter presenter;
    ProgressDialog progressDialog;

    private List<CategoryModel> listCategory = new ArrayList<CategoryModel>();
    private CategoryAdapter adapter;

    ImageView imgLogo;
    Button btnNUP;
    TextView txtProjectName;

    long dbMasterRef, categoryRef;
    String projectRef, projectDescription, projectName;
    String isNUP, nupAmt, isBooking;
    String countCluster, clusterRef;
    String isShowAvailableUnit, imageLogo;

    private RelativeLayout snackBarBuatan;
    private TextView retry;
    Display display;
    MyGridView listView;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Point size;
    Integer width;
    Double result;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new IlustrationCategoryPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        dbMasterRef = getIntent().getLongExtra(General.DBMASTER_REF, 0);
        projectRef = getIntent().getStringExtra(General.PROJECT_REF);
        projectRef = getIntent().getStringExtra(General.PROJECT_REF);
        projectName = getIntent().getStringExtra(General.PROJECT_NAME);
        projectDescription = getIntent().getStringExtra(General.PROJECT_DESCRIPTION);
        isShowAvailableUnit = getIntent().getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        isNUP = getIntent().getStringExtra(General.IS_NUP);
        isBooking = getIntent().getStringExtra(General.IS_BOOKING);
        nupAmt = getIntent().getStringExtra(NUP_AMT);
        imageLogo = getIntent().getStringExtra(IMAGE_LOGO);
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        btnNUP.setVisibility(View.GONE);
        Glide.with(this).load(WebService.getProjectImage() + dbMasterRef +
                "&pr=" + projectRef).into(imgLogo);


        Log.d("screen width", result.toString() + "--" + Math.round(result));


        adapter = new CategoryAdapter(this, listCategory, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                countCluster = listCategory.get(position).getCountCluster();
                categoryRef = listCategory.get(position).getCategoryRef();
                clusterRef = listCategory.get(position).getClusterRef();
                isShowAvailableUnit = listCategory.get(position).getIsShowAvailableUnit();
                Log.d("cek CategoryRef", countCluster + " " + categoryRef + " " + clusterRef);

                if (countCluster.equals("1")) {
                    Intent intentProduct = new Intent(IlustrationCategoryActivity.this, IlustrationProductActivity.class);
                    intentProduct.putExtra(PROJECT_REF, projectRef);
                    intentProduct.putExtra(DBMASTER_REF, dbMasterRef);
                    intentProduct.putExtra(CLUSTER_REF, clusterRef);
                    intentProduct.putExtra(CATEGORY_REF, categoryRef);
                    intentProduct.putExtra(PROJECT_NAME, projectName);
                    intentProduct.putExtra(IS_BOOKING, isBooking);
                    intentProduct.putExtra(PRODUCT_REF, "");
                    intentProduct.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentProduct.putExtra(IMAGE_LOGO, imageLogo);
                    startActivity(intentProduct);
                } else {
                    Intent intentCluster = new Intent(IlustrationCategoryActivity.this, IlustrationClusterActivity.class);
                    intentCluster.putExtra(PROJECT_REF, projectRef);
                    intentCluster.putExtra(DBMASTER_REF, dbMasterRef);
                    intentCluster.putExtra(CATEGORY_REF, categoryRef);
                    intentCluster.putExtra(PROJECT_NAME, projectName);
                    intentCluster.putExtra(IS_BOOKING, isBooking);
                    intentCluster.putExtra(IMAGE_LOGO, imageLogo);
                    startActivity(intentCluster);
                }
            }
        });

        requestCategory();
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(IlustrationCategoryActivity.this, LaunchActivity.class));
                finish();
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Category");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyGridView) findViewById(R.id.list_category);
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);

    }

    public void requestCategory() {
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListCategory(String.valueOf(dbMasterRef), projectRef);

    }



    private void initListView() {
        adapter = new CategoryAdapter(this, listCategory, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationCategoryActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showListCategoryResults(retrofit2.Response<List<CategoryModel>> response) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = false;
        snackBarBuatan.setVisibility(View.GONE);
        //final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        listCategory=response.body();
//        for (int i = 0; i < listCategory.size(); i++) {
//
//            CategoryModel categoryModel = new CategoryModel();
//            categoryModel.setDbMasterRef(listCategory.get(i).getDbMasterRef());
//            categoryModel.setProjectRef(listCategory.get(i).getProjectRef());
//            categoryModel.setCategoryRef(listCategory.get(i).getCategoryRef());
//            categoryModel.setProjectCategoryType(listCategory.get(i).getProjectCategoryType());
//            categoryModel.setCategoryDescription(listCategory.get(i).getCategoryDescription());
//            categoryModel.setCountCluster(listCategory.get(i).getCountCluster());
//            categoryModel.setClusterRef(listCategory.get(i).getClusterRef());
//            categoryModel.setIsShowAvailableUnit(listCategory.get(i).getIsShowAvailableUnit());
//            categoryModel.setImage(listCategory.get(i).getImage());
//            CategoryModelDao categoryModelDao = daoSession.getCategoryModelDao();
//            categoryModelDao.insertOrReplace(categoryModel);
//        }
        initListView();

    }

    public void showListCategoryFailure(Throwable t) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = true;
        snackBarBuatan.setVisibility(View.VISIBLE);
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
//        CategoryModelDao categoryModelDao = daoSession.getCategoryModelDao();
//        List<CategoryModel> categoryModels = categoryModelDao.queryBuilder()
//                .where(CategoryModelDao.Properties.DbMasterRef.eq(dbMasterRef)).list();
//        int numData = categoryModels.size();
//        if (numData > 0) {
//            for (int i = 0; i < numData; i++) {
//                CategoryModel categoryModel = categoryModels.get(i);
//                listCategory.add(categoryModel);
//            }
//            initListView();
//        } else {
//            finish();
//        }


    }
}
