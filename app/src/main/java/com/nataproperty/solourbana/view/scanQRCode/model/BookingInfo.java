package com.nataproperty.solourbana.view.scanQRCode.model;

/**
 * Created by Nata on 3/14/2017.
 */

public class BookingInfo {
    String bookDate, salesReferral, salesEvent, purpose, salesLocation, bookHour, urlSchedule, finType;

    public String getBookDate() {
        return bookDate;
    }

    public void setBookDate(String bookDate) {
        this.bookDate = bookDate;
    }

    public String getSalesReferral() {
        return salesReferral;
    }

    public void setSalesReferral(String salesReferral) {
        this.salesReferral = salesReferral;
    }

    public String getSalesEvent() {
        return salesEvent;
    }

    public void setSalesEvent(String salesEvent) {
        this.salesEvent = salesEvent;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getSalesLocation() {
        return salesLocation;
    }

    public void setSalesLocation(String salesLocation) {
        this.salesLocation = salesLocation;
    }

    public String getBookHour() {
        return bookHour;
    }

    public void setBookHour(String bookHour) {
        this.bookHour = bookHour;
    }

    public String getUrlSchedule() {
        return urlSchedule;
    }

    public void setUrlSchedule(String urlSchedule) {
        this.urlSchedule = urlSchedule;
    }

    public String getFinType() {
        return finType;
    }

    public void setFinType(String finType) {
        this.finType = finType;
    }
}
