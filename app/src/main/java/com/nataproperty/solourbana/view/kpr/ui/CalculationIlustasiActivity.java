package com.nataproperty.solourbana.view.kpr.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.ilustration.adapter.IlustrationPaymentCalculatorAdapter;
import com.nataproperty.solourbana.view.ilustration.model.PaymentModel;
import com.nataproperty.solourbana.view.kpr.adapter.KprTableAdapter2;
import com.nataproperty.solourbana.view.kpr.model.KprTableModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculationIlustasiActivity extends AppCompatActivity {
    public static final String TAG = "CalculationIlustasi";

    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;
    String memberRef;

    private List<PaymentModel> listPayment = new ArrayList<>();
    private IlustrationPaymentCalculatorAdapter adapter;

    private ArrayList<KprTableModel> listKprTable = new ArrayList<KprTableModel>();
    private KprTableAdapter2 adapterKprTable;

    TextView txtProject, txtCluster, txtProduct, txtBlock, txtUnit, txtPrice, txtTotal, txtInstallmentType, txtNilaiKpr;

    String calcPayTermRef, calcUserProjectRef, payTermName, installmentType, dpType, bookingFeeDate,
            bookingFee, numOfInst, daysOfInst, listDP, KPRYear;

    String project, cluster, block, product, unit, price, type, nilaiKpr;

    Button btnSendToFriend;

    static CalculationIlustasiActivity calculationIlustasiActivity;

    LinearLayout linearLayoutByKPR,linearLayoutNoteSave;

    int noteSave;


    String stringListDp;
    String stringListKpr;
    public ArrayList<String> dpList = new ArrayList<String>();
    public ArrayList<String> cicilList = new ArrayList<String>();
    public ArrayList<String> hargaList = new ArrayList<String>();

    public ArrayList<String> tahunKprList = new ArrayList<String>();
    public ArrayList<String> bungaList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_ilustasi);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_calculation));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        calculationIlustasiActivity = this;

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        txtProject = (TextView) findViewById(R.id.txt_project);
        txtCluster = (TextView) findViewById(R.id.txt_cluster);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtBlock = (TextView) findViewById(R.id.txt_block);
        txtUnit = (TextView) findViewById(R.id.txt_unit);
        txtPrice = (TextView) findViewById(R.id.txt_price);
        txtTotal = (TextView) findViewById(R.id.txt_total);
        txtInstallmentType = (TextView) findViewById(R.id.txt_installment_type);
        txtNilaiKpr = (TextView) findViewById(R.id.txt_nilai_kpr);

        btnSendToFriend = (Button) findViewById(R.id.btn_send_to_friend);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear_header_kpr);
        linearLayoutByKPR = (LinearLayout) findViewById(R.id.linear_by_kpr);
        linearLayoutNoteSave = (LinearLayout) findViewById(R.id.linear_note_save);

        Intent intent = getIntent();
        calcPayTermRef = intent.getStringExtra("calcPayTermRef");
        calcUserProjectRef = intent.getStringExtra("calcUserProjectRef");
        payTermName = intent.getStringExtra("payTermName");
        installmentType = intent.getStringExtra("installmentType");
        dpType = intent.getStringExtra("DPType");
        bookingFeeDate = intent.getStringExtra("bookingFeeDate");
        bookingFee = intent.getStringExtra("bookingFee");
        numOfInst = intent.getStringExtra("numOfInst");
        daysOfInst = intent.getStringExtra("daysOfInst");
        listDP = intent.getStringExtra("listDP");
        KPRYear = intent.getStringExtra("KPRYear");
        noteSave = intent.getIntExtra("noteSave", 0);

        Log.d(TAG, "getIntent=" + calcPayTermRef + " " + calcUserProjectRef + " " + installmentType +
                " " + dpType + " " + bookingFeeDate
                + " " + bookingFee + " " + numOfInst + " " + listDP + " " + KPRYear);

        project = intent.getStringExtra("project");
        cluster = intent.getStringExtra("cluster");
        product = intent.getStringExtra("product");
        block = intent.getStringExtra("block");
        unit = intent.getStringExtra("unit");
        price = intent.getStringExtra("price");

        if (cluster.equals("")) {
            cluster = "-";
        }
        if (product.equals("")) {
            product = "-";
        }
        if (block.equals("")) {
            block = "-";
        }
        if (unit.equals("")) {
            unit = "-";
        }
        if (installmentType.equals("1")) {
            type = "Cicilan";
        } else {
            type = "KPR";
        }
        if (noteSave == 1) {
            linearLayoutNoteSave.setVisibility(View.VISIBLE);
        } else {
            linearLayoutNoteSave.setVisibility(View.GONE);
        }

        txtProject.setText(project);
        txtCluster.setText(cluster);
        txtProduct.setText(product);
        txtBlock.setText(block);
        txtUnit.setText(unit);
        txtPrice.setText(price);
        txtInstallmentType.setText(type);
        txtTotal.setText(price);

        MyListView listView = (MyListView) findViewById(R.id.list_payment_schedule);
        MyListView listViewKprTable = (MyListView) findViewById(R.id.list_kpr_table);

        requestPayment();

        adapter = new IlustrationPaymentCalculatorAdapter(this, listPayment);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        adapterKprTable = new KprTableAdapter2(this, listKprTable);
        listViewKprTable.setAdapter(adapterKprTable);
        listViewKprTable.setExpanded(true);

        if (installmentType.equals("1")) {
            linearLayout.setVisibility(View.GONE);
            linearLayoutByKPR.setVisibility(View.GONE);
        } else {
            linearLayout.setVisibility(View.VISIBLE);
            linearLayoutByKPR.setVisibility(View.VISIBLE);
        }

        btnSendToFriend.setTypeface(font);
        btnSendToFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CalculationIlustasiActivity.this, CalculationSendToFriendActivity.class);
                intent.putExtra("calcUserProjectRef", calcUserProjectRef);
                intent.putExtra("payTermName", payTermName);
                intent.putExtra("installmentType", installmentType);
                intent.putExtra("DPType", dpType);
                intent.putExtra("bookingFeeDate", bookingFeeDate);
                intent.putExtra("bookingFee", bookingFee);
                intent.putExtra("numOfInst", numOfInst);
                intent.putExtra("daysOfInst", daysOfInst);
                intent.putExtra("listDP", listDP);
                intent.putExtra("KPRYear", KPRYear);
                intent.putExtra("project", project);
                intent.putExtra("cluster", cluster);
                intent.putExtra("product", product);
                intent.putExtra("block", block);
                intent.putExtra("unit", unit);
                intent.putExtra("price", price);
                startActivity(intent);
            }
        });

    }

    public void requestPayment() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListCalcUserProjectIlustration(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonArrayTable1 = new JSONArray(jsonObject.getJSONArray("table1").toString());
                    generatePaymentTable1(jsonArrayTable1);

                    JSONArray jsonArrayTable2 = new JSONArray(jsonObject.getJSONArray("table2").toString());
                    generatePaymentTable2(jsonArrayTable2);

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(CalculationIlustasiActivity.this, "error connection", Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef.toString());
                params.put("calcUserProjectRef", calcUserProjectRef.toString());
                params.put("payTermName", payTermName.toString());
                params.put("installmentType", installmentType.toString());
                params.put("DPType", dpType.toString());
                params.put("bookingFeeDate", bookingFeeDate.toString());
                params.put("bookingFee", bookingFee.toString().replace(".","").replace(",",""));
                params.put("numOfInst", numOfInst.toString());
                params.put("daysOfInst", daysOfInst.toString());
                params.put("listDP", listDP.toString());
                params.put("KPRYear", KPRYear.toString());
                params.put("price", price.toString().replace(".","").replace(",",""));
                Log.d("param ",params.toString());
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "ilustrasi");

    }

    private void generatePaymentTable1(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                PaymentModel payment = new PaymentModel();
                payment.setSchedule(joArray.getString("schedule"));
                payment.setDueDate(joArray.getString("duedate"));
                payment.setAmount(joArray.getString("amount"));

                nilaiKpr = joArray.getString("amount");
                txtNilaiKpr.setText(nilaiKpr);

                listPayment.add(payment);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void generatePaymentTable2(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                KprTableModel kprTableModel = new KprTableModel();
                kprTableModel.setRef(joArray.getString("ref"));
                kprTableModel.setInsPeriode(joArray.getString("insPeriode"));
                kprTableModel.setPct(joArray.getString("pct"));
                kprTableModel.setInstMonth(joArray.getString("instMonth"));
                kprTableModel.setTotal(joArray.getString("total"));

                listKprTable.add(kprTableModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    public static CalculationIlustasiActivity getInstance() {
        return calculationIlustasiActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CalculationIlustasiActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
