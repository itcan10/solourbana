package com.nataproperty.solourbana.view.booking.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

public class BookingCreditCardWebviewActivity extends AppCompatActivity {
    public static final String TAG = "BookingCreditCard";
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String STATUS_PAYMENT = "statusPayment";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";
    private SharedPreferences sharedPreferences;
    private String memberRef, dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo, bookingRef, projectName,
            projectBookingRef, link, titlePayment;
    private WebView webView;
    private Toolbar toolbar;
    private TextView title;
    private Typeface font, fontLight;
    private ProgressBar progressBar;
    private int typePayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_booking_credit_card_webview);
        initWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        link = intent.getStringExtra("link");
        titlePayment = intent.getStringExtra("titlePayment");
        typePayment = intent.getIntExtra("typePayment",0);

        title.setText(titlePayment);

        initWebView();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Credit Card");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progress1);
    }

    private void initWebView() {
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        webView.loadUrl(link);
        Log.d("lingWebView", "" + link);
        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void intentFinish() {
            Toast.makeText(mContext, "Berhasil", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(BookingCreditCardWebviewActivity.this, BookingFinishActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(CLUSTER_REF, clusterRef);
            intent.putExtra(PRODUCT_REF, productRef);
            intent.putExtra(UNIT_REF, unitRef);
            intent.putExtra(TERM_REF, termRef);
            intent.putExtra(TERM_NO, termNo);
            intent.putExtra(BOOKING_REF, bookingRef);
            intent.putExtra(PROJECT_NAME, projectName);
            intent.putExtra(STATUS_PAYMENT, WebService.updatePaymentTypeBankTransfer);
            intent.putExtra("typePayment", typePayment);
            startActivity(intent);
            finish();
        }

        @JavascriptInterface
        public void intentPaymentMethod() {
            Toast.makeText(mContext, "Failed to Pay", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
