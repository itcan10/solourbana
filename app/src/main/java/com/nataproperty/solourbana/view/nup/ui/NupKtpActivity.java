package com.nataproperty.solourbana.view.nup.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by UserModel on 5/6/2016.
 */
public class NupKtpActivity extends AppCompatActivity {
    public static final String TAG = "NupKtpActivity";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String NUP_AMT = "nupAmt";
    public static final String PROJECT_NAME = "projectName";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String NPWP = "npwp";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";

    public static final String FILE_CODE_KTP = "fileCodeKtp";
    public static final String FILE_CODE_NPWP = "fileCodeNpwp";
    public static final String URL_DOC_KTP = "urlDocKtp";
    public static final String URL_DOC_NPWP = "urlDocNpwp";

    private String KEY_MEMBERREF = "memberRef";
    private String KEY_IMAGE = "Image";

    String memberCostumerRef;
    private String dbMasterRef, projectRef, projectName, nupAmt, phone, address, ktpId,imagePath;
    private String fullname, birthdate, mobile1, email1,npwp, ktpRef, npwpRef;
    private String fileCodeKtp, fileCodeNpwp,urlDocKtp, urlDocNpwp,flaq;

    SharedPreferences sharedPreferences;
    private Bitmap bitmap;
    CropImageView cropImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ktp_image);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Edit Ktp");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        final Button upload = (Button) findViewById(R.id.btn_upload);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCostumerRef = sharedPreferences.getString("isMemberCostumerRef", null);

        Intent intent = getIntent();
        // bitmap = (Bitmap) intent.getParcelableExtra("Image");
        //byte[] byteArray = getIntent().getByteArrayExtra("Image");
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupAmt = intent.getStringExtra(NUP_AMT);
        projectName = intent.getStringExtra(PROJECT_NAME);
        imagePath = intent.getStringExtra("pathImage");

        fullname = intent.getStringExtra(FULLNAME);
        birthdate = intent.getStringExtra("birthdate");
        mobile1 = intent.getStringExtra(MOBILE);
        email1 = intent.getStringExtra(EMAIL);
        phone = intent.getStringExtra(PHONE);
        address = intent.getStringExtra(ADDRESS);
        ktpId = intent.getStringExtra(KTP_ID);
        npwp = intent.getStringExtra(NPWP);

        fileCodeKtp = intent.getStringExtra(FILE_CODE_KTP);
        fileCodeNpwp = intent.getStringExtra(FILE_CODE_NPWP);
        urlDocKtp = intent.getStringExtra(URL_DOC_KTP);
        urlDocNpwp = intent.getStringExtra(URL_DOC_NPWP);

        Log.d(TAG, "" + phone + " " + address + " " + ktpId);

        //bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        cropImageView.setFixedAspectRatio(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                cropImageView.setAspectRatio(3, 2);
            }
        }, 500);
        //cropImageView.setImageBitmap(bitmap);
        cropImageView.setImageBitmap(decodeSampledBitmapFromResource(imagePath, 500, 500));

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = cropImageView.getCroppedImage(500, 500);
                if (bitmap != null)
                    cropImageView.setImageBitmap(bitmap);
                cropImageView.setVisibility(View.GONE);
                upload.setVisibility(View.GONE);

                uploadImage();
            }
        });

    }

    public static Bitmap decodeSampledBitmapFromResource(String resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage() {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebService.getUpdateImageOrderSvc(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TAG", "Upload image " + response.toString());
                        loading.dismiss();

                        try {
                            JSONObject jo = new JSONObject(response);
                            int status = jo.getInt("status");

                            fileCodeKtp = jo.getString("fileCode");
                            urlDocKtp = jo.getString("urlDoc");

                            if (status == 200) {
                                Intent intent = new Intent(NupKtpActivity.this, NupInformasiCustomerActivity.class);
                                intent.putExtra(DBMASTER_REF, dbMasterRef);
                                intent.putExtra(PROJECT_REF, projectRef);
                                intent.putExtra(NUP_AMT, nupAmt);
                                intent.putExtra(PROJECT_NAME, projectName);

                                intent.putExtra(FULLNAME, fullname);
                                intent.putExtra("birthdate", birthdate);
                                intent.putExtra(MOBILE, mobile1);
                                intent.putExtra(PHONE, phone);
                                intent.putExtra(EMAIL, email1);
                                intent.putExtra(ADDRESS, address);
                                intent.putExtra(KTP_ID, ktpId);
                                intent.putExtra(NPWP, npwp);

                                intent.putExtra(FILE_CODE_KTP, fileCodeKtp);
                                intent.putExtra(FILE_CODE_NPWP, fileCodeNpwp);
                                intent.putExtra(URL_DOC_KTP, urlDocKtp);
                                intent.putExtra(URL_DOC_NPWP, urlDocNpwp);
                                startActivity(intent);
                                finish();
                                Toast.makeText(NupKtpActivity.this, "Upload KTP Succes", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(NupKtpActivity.this, "Upload Failed", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupKtpActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupKtpActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);

                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put(KEY_IMAGE, image);
//                params.put(KEY_MEMBERREF, memberCostumerRef);
                params.put("docType", General.docTypeKtp);

                Log.d("Param", memberCostumerRef);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rotate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_rotate:
                cropImageView.rotateImage(90);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
