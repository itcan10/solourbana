package com.nataproperty.solourbana.view.ilustration.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterIlustrationCategoryInteractor {
    void getListCategory(String dbMasterRef, String projectRef);

}
