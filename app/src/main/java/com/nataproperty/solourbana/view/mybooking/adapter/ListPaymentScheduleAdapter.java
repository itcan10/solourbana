package com.nataproperty.solourbana.view.mybooking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mybooking.model.PaymentSchaduleModel;

import java.util.List;

/**
 * Created by User on 5/14/2016.
 */
public class ListPaymentScheduleAdapter extends BaseAdapter {
    private Context context;
    private List<PaymentSchaduleModel> list;
    private ListPaymentHolder holder;

    public ListPaymentScheduleAdapter(Context context, List<PaymentSchaduleModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_payment_schedule_outstanding,null);
            holder = new ListPaymentHolder();
            holder.schedule = (TextView) convertView.findViewById(R.id.txt_schedule);
            holder.dueDate = (TextView) convertView.findViewById(R.id.txt_duedate);
            holder.amount = (TextView) convertView.findViewById(R.id.txt_amount);
            holder.outstanding = (TextView) convertView.findViewById(R.id.txt_outstanding);

            convertView.setTag(holder);
        }else{
            holder = (ListPaymentHolder) convertView.getTag();
        }

        PaymentSchaduleModel paymentSchaduleModel = list.get(position);
        holder.schedule.setText(paymentSchaduleModel.getType());
        holder.dueDate.setText(paymentSchaduleModel.getDueDate());
        holder.amount.setText(paymentSchaduleModel.getAmount());
        holder.outstanding.setText(paymentSchaduleModel.getOutstanding());

        return convertView;
    }

    private class ListPaymentHolder {
        TextView schedule,dueDate,amount,outstanding;
    }
}
