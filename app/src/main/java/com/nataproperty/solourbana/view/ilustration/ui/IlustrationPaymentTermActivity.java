package com.nataproperty.solourbana.view.ilustration.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.view.ilustration.adapter.IlustrationPaymentImageAdapter;
import com.nataproperty.solourbana.view.ilustration.adapter.IlustrationPaymentTermAdapter;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.ilustration.model.PaymentTermModel;
import com.nataproperty.solourbana.view.project.model.ProductDetailModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by UserModel on 5/13/2016.
 */
public class IlustrationPaymentTermActivity extends AppCompatActivity {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String IS_BOOKING = "isBooking";

    private ArrayList<PaymentTermModel> listPayment = new ArrayList<PaymentTermModel>();
    private IlustrationPaymentTermAdapter adapter;

    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtPropertyName, txtCategoryType, txtCluster, txtProduct, txtUnitNo, txtArea, txtEstimate, txtView;
    //    ListView listView;
    String dbMasterRef, projectRef, categoryRef, clusterRef, projectName, productRef, unitRef, isBooking;

    String propertys, category, product, unit, area, priceInc, netPrice, specialEnquiries, bookingContact = "", viewName, va1, va2;

    Button btnSpecialEnquiries;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    Point size;
    Integer width;
    Double result;
    MyListView listView;

    SharedPreferences sharedPreferences;
    String memberRef = "", employeeId;

    private IlustrationPaymentImageAdapter adapterImage;
    private List<ProductDetailModel> listProductDetail = new ArrayList<ProductDetailModel>();
    ViewPager viewPager;
    int pageView;
    Timer timer;
    int page = 0;
    CircleIndicator indicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_payment_term);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        isBooking = intent.getStringExtra(IS_BOOKING);

        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        memberRef = sharedPreferences.getString(General.IS_MEMBER_REF, null);


        if (intent.getStringExtra("employeeId") == null) {
            employeeId = "";
        } else {
            employeeId = intent.getStringExtra("employeeId");
        }

        initWidget();

        adapterImage = new IlustrationPaymentImageAdapter(this, listProductDetail);
        viewPager.setAdapter(adapterImage);
        pageSwitcher(4);
        indicator.setViewPager(viewPager);
        adapterImage.registerDataSetObserver(indicator.getDataSetObserver());
//        Glide.with(this)
//                .load(WebService.getProjectImage() + dbMasterRef +
//                        "&pr=" + projectRef).into(imgLogo);
//        Log.d("Cek payment", dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" + unitRef);
        requestPaymentTerm();

        Log.d("isBooking", " " + isBooking);

        adapter = new IlustrationPaymentTermAdapter(this, listPayment);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String termRef = listPayment.get(position).getTermRef();
                String termNo = listPayment.get(position).getTermNo();
                String finType = listPayment.get(position).getFinType();
                String dpNo = listPayment.get(position).getDpNo();
                String netPrice = listPayment.get(position).getNetPrice();

                if (Integer.parseInt(finType) == 2) {
                    Intent intentKpr = new Intent(IlustrationPaymentTermActivity.this, IlustrationAddKPRActivity.class);
                    intentKpr.putExtra("dbMasterRef", dbMasterRef);
                    intentKpr.putExtra("projectRef", projectRef);
                    intentKpr.putExtra("categoryRef", categoryRef);
                    intentKpr.putExtra("clusterRef", clusterRef);
                    intentKpr.putExtra("productRef", productRef);
                    intentKpr.putExtra("unitRef", unitRef);
                    intentKpr.putExtra("termRef", termRef);
                    intentKpr.putExtra("termNo", termNo);
                    intentKpr.putExtra("projectName", projectName);

                    intentKpr.putExtra("propertys", propertys);
                    intentKpr.putExtra("category", category);
                    intentKpr.putExtra("product", product);
                    intentKpr.putExtra("unit", unit);
                    intentKpr.putExtra("area", area);
                    intentKpr.putExtra("priceInc", priceInc);
                    intentKpr.putExtra("netPrice", netPrice);
                    intentKpr.putExtra("viewName", viewName);
                    intentKpr.putExtra("va1", va1);
                    intentKpr.putExtra("va2", va2);

                    intentKpr.putExtra(IS_BOOKING, isBooking);
                    intentKpr.putExtra("finType", Integer.parseInt(finType));
                    intentKpr.putExtra("bookingContact", bookingContact);
                    startActivity(intentKpr);
                } else {
                    Intent intentSchadule = new Intent(IlustrationPaymentTermActivity.this, IlustrationPaymentActivity.class);
                    intentSchadule.putExtra("dbMasterRef", dbMasterRef);
                    intentSchadule.putExtra("projectRef", projectRef);
                    intentSchadule.putExtra("categoryRef", categoryRef);
                    intentSchadule.putExtra("clusterRef", clusterRef);
                    intentSchadule.putExtra("productRef", productRef);
                    intentSchadule.putExtra("unitRef", unitRef);
                    intentSchadule.putExtra("termRef", termRef);
                    intentSchadule.putExtra("termNo", termNo);
                    intentSchadule.putExtra("projectName", projectName);

                    intentSchadule.putExtra("propertys", propertys);
                    intentSchadule.putExtra("category", category);
                    intentSchadule.putExtra("product", product);
                    intentSchadule.putExtra("unit", unit);
                    intentSchadule.putExtra("area", area);
                    intentSchadule.putExtra("priceInc", priceInc);
                    intentSchadule.putExtra("netPrice", netPrice);
                    intentSchadule.putExtra("viewName", viewName);
                    intentSchadule.putExtra("va1", va1);
                    intentSchadule.putExtra("va2", va2);

                    intentSchadule.putExtra(IS_BOOKING, isBooking);
                    intentSchadule.putExtra("finType", Integer.parseInt(finType));
                    intentSchadule.putExtra("bookingContact", bookingContact);
                    startActivity(intentSchadule);
                }

            }
        });

        btnSpecialEnquiries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IlustrationPaymentTermActivity.this);
                LayoutInflater inflater = IlustrationPaymentTermActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
                dialogBuilder.setView(dialogView);

                final TextView txtspecialEnquiries = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
                dialogBuilder.setMessage("Special Enquiries");
                txtspecialEnquiries.setText(specialEnquiries);

                dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_illustrastion_payment_term));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.777777777777778;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtView = (TextView) findViewById(R.id.txt_view);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);
        btnSpecialEnquiries = (Button) findViewById(R.id.btn_specialEnquiries);
        listView = (MyListView) findViewById(R.id.list_payment_term);
        btnSpecialEnquiries.setTypeface(font);

        viewPager = (ViewPager) findViewById(R.id.pager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);


    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
    }

    class RemindTask extends TimerTask {
        @Override
        public void run() {
            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {
                    //viewPager.setCurrentItem(page++);
                    if (page == pageView) { // In my case the number of pages are 5
                        //timer.cancel();
                        page = 0;
                    } else {
                        viewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    public void requestPaymentTerm() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getPaymentTerm(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    Log.d("payment", response);

                    if (status == 200) {
                        propertys = jo.getJSONObject("data").getString("propertys");
                        category = jo.getJSONObject("data").getString("category");
                        product = jo.getJSONObject("data").getString("product");
                        unit = jo.getJSONObject("data").getString("unit");
                        area = jo.getJSONObject("data").getString("landBuild");
                        priceInc = jo.getJSONObject("data").getString("priceInc");
                        specialEnquiries = jo.getJSONObject("data").getString("specialEnquiries");
                        bookingContact = jo.getJSONObject("data").getString("bookingContact");
                        viewName = jo.getJSONObject("data").getString("viewName");
                        va1 = jo.getJSONObject("data").getString("va1");
                        if (va1.equals("null") || va1.equals("0") || va1.equals("")) {
                            va1 = "";
                        }
                        va2 = jo.getJSONObject("data").getString("va2");
                        if (va2.equals("null") || va2.equals("0") || va2.equals("")) {
                            va2 = "";
                        }

                        try {
                            JSONArray jsonArrayImage = new JSONArray(jo.getJSONArray("productImage").toString());
                            generatePaymentIlustrationImage(jsonArrayImage);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("cek result", propertys + " " + category + " " + product + " " + unit + " " + area + " " + priceInc
                                + " " + bookingContact);
                        Log.d("va", va1 + va2);
                        txtPropertyName.setText(propertys);
                        txtCategoryType.setText(category);
                        txtProduct.setText(product);
                        txtUnitNo.setText(unit);
                        txtArea.setText(Html.fromHtml(area));
                        txtEstimate.setText(priceInc);
                        txtView.setText(viewName);

                        JSONArray jsonArray = new JSONArray(jo.getJSONArray("dataList").toString());
                        generatePaymentIlustration(jsonArray);

                        if (!specialEnquiries.equals("")) {
                            btnSpecialEnquiries.setVisibility(View.VISIBLE);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(IlustrationPaymentTermActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(IlustrationPaymentTermActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);
                params.put("unitRef", unitRef);
                params.put("memberRef", memberRef);
                params.put("employeeId", employeeId);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "Term");

    }

    private void generatePaymentIlustration(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                PaymentTermModel payment = new PaymentTermModel();
                payment.setTermRef(joArray.getString("termRef"));
                payment.setTermNo(joArray.getString("termNo"));
                payment.setTermName(joArray.getString("termName"));
                payment.setDiscount(joArray.getString("discount"));
                payment.setDownPayment(joArray.getString("downPayment"));
                payment.setPriceIncTerm(joArray.getString("priceIncTerm"));
                payment.setNetPrice(joArray.getString("netPrice"));
                payment.setFinType(joArray.getString("finType"));
                payment.setDpNo(joArray.getString("dpNo"));
                listPayment.add(payment);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapter.notifyDataSetChanged();
    }

    private void generatePaymentIlustrationImage(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jo = jsonArray.getJSONObject(i);
                ProductDetailModel imageProduct = new ProductDetailModel();
                imageProduct.setDbMasterProjectProductFileRef(jo.getString("dbMasterProjectProductFileRef"));
                imageProduct.setDbMasterRef(jo.getString("dbMasterRef"));
                imageProduct.setProjectRef(jo.getString("projectRef"));
                imageProduct.setProductRef(jo.getString("productRef"));
                imageProduct.setTitle(jo.getString("title"));
                imageProduct.setQuerystring("?is=productdetail" +
                        "&dr=" + imageProduct.getDbMasterRef() + "" +
                        "&pr=" + imageProduct.getProjectRef() + "" +
                        "&pd=" + imageProduct.getProductRef() + "" +
                        "&fr=" + imageProduct.getDbMasterProjectProductFileRef() + "");
                listProductDetail.add(imageProduct);
                pageView = jsonArray.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        adapterImage.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationPaymentTermActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
