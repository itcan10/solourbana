package com.nataproperty.solourbana.view.mainmenu.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.projectmenu.intractor.PresenterMainInteractor;
import com.nataproperty.solourbana.view.menuitem.model.StatusTokenGCMModel;
import com.nataproperty.solourbana.view.mainmenu.ui.MainMenuActivity;
import com.nataproperty.solourbana.view.report.model.CheckAvailableReportModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MainMenuPresenter implements PresenterMainInteractor {
    private MainMenuActivity view;
    private ServiceRetrofitProject service;

    public MainMenuPresenter(MainMenuActivity view, ServiceRetrofitProject service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void sendGCMToken(String GCMToken, String memberRef) {
        Call<StatusTokenGCMModel> call = service.getAPI().postGCMToken(GCMToken, memberRef);
        call.enqueue(new Callback<StatusTokenGCMModel>() {
            @Override
            public void onResponse(Call<StatusTokenGCMModel> call, Response<StatusTokenGCMModel> responseToken) {
                view.showSendGCMTokenResults(responseToken);
            }

            @Override
            public void onFailure(Call<StatusTokenGCMModel> call, Throwable t) {
                view.showSendGCMTokenFailure(t);

            }

        });

    }

    @Override
    public void CheckAvailableReportSvc(String projectCode, String dbMasterRef, String projectRef, String memberRef) {
        Call<CheckAvailableReportModel> call = service.getAPI().CheckAvailableReportSvc(projectCode, dbMasterRef, projectRef, memberRef);
        call.enqueue(new Callback<CheckAvailableReportModel>() {
            @Override
            public void onResponse(Call<CheckAvailableReportModel> call, Response<CheckAvailableReportModel> response) {
                view.showCheckAvailableReportResults(response);
            }

            @Override
            public void onFailure(Call<CheckAvailableReportModel> call, Throwable t) {
                view.showCheckAvailableReportFailure(t);

            }


        });
    }

}
