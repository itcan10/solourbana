package com.nataproperty.solourbana.view.mybooking.model;

/**
 * Created by nata on 11/24/2016.
 */
public class BookingDetailModel {
    String bookDate;
    String bookHour;

    public String getBookDate() {
        return bookDate;
    }

    public void setBookDate(String bookDate) {
        this.bookDate = bookDate;
    }

    public String getBookHour() {
        return bookHour;
    }

    public void setBookHour(String bookHour) {
        this.bookHour = bookHour;
    }

    //    String
}
