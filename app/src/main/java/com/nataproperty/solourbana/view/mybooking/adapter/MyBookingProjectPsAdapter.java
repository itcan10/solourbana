package com.nataproperty.solourbana.view.mybooking.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingModel;

import java.util.List;

/**
 * Created by UserModel on 5/14/2016.
 */
public class MyBookingProjectPsAdapter extends BaseAdapter {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";
    public static final String TYPE_PAYMENT = "typePayment";
    public static final String MEMBER_CUSTOMER_REF = "memberCustomerRef";

    private Context context;
    private List<MyBookingModel> list;
    private ListMyBookingHolder holder;

    Typeface font;

    public MyBookingProjectPsAdapter(Context context, List<MyBookingModel> list) {
        this.context = context;
        this.list = list;
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
    }

    String dbMasterRef, projectRef, categoryRef, productRef, clusterRef, unitRef, termNo, termRef, bookingRef, projectName, projectBookingRef,
            linkDownload, memberCustomerRef;
    int isPayment;
    int clearDate;

    SharedPreferences sharedPreferences;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_my_booking_project_ps, null);
            holder = new ListMyBookingHolder();
            holder.bookingCode = (TextView) convertView.findViewById(R.id.txt_booking_code);
            holder.projectName = (TextView) convertView.findViewById(R.id.txt_property);
            holder.clusterName = (TextView) convertView.findViewById(R.id.txt_cluster_name);
            holder.blockName = (TextView) convertView.findViewById(R.id.txt_block_name);
            holder.unitName = (TextView) convertView.findViewById(R.id.txt_unit_no);
            holder.btnBookingFee = (Button) convertView.findViewById(R.id.btn_pending_booking);
            holder.btnBookingInfo = (Button) convertView.findViewById(R.id.btn_booking_info);
            holder.btnWaitingVarification = (Button) convertView.findViewById(R.id.btn_waiting_verification);
            holder.linearItem = (LinearLayout) convertView.findViewById(R.id.linear_item_list_my_booking);
            holder.costumerName = (TextView) convertView.findViewById(R.id.txt_costumer_name);

            convertView.setTag(holder);
        } else {
            holder = (ListMyBookingHolder) convertView.getTag();
        }

        MyBookingModel myBookingModel = list.get(position);
        holder.bookingCode.setText(myBookingModel.getBookingCode());
        holder.projectName.setText(myBookingModel.getProjectName());
        holder.clusterName.setText(myBookingModel.getClusterName());
        holder.blockName.setText(myBookingModel.getBlockName());
        holder.unitName.setText(myBookingModel.getUnitName());
        holder.costumerName.setText(myBookingModel.getCustomerName());

        isPayment = Integer.valueOf(list.get(position).getIsPayment());
        clearDate = Integer.valueOf(list.get(position).getClearDate());
/*        if (isPayment==0){
            holder.btnBookingFee.setVisibility(View.VISIBLE);
            holder.btnBookingInfo.setVisibility(View.GONE);
            holder.btnWaitingVarification.setVisibility(View.GONE);
        } else if (clearDate != 0){
            holder.btnBookingFee.setVisibility(View.GONE);
            holder.btnBookingInfo.setVisibility(View.VISIBLE);
            holder.btnWaitingVarification.setVisibility(View.GONE);
        } else {
            holder.btnBookingFee.setVisibility(View.GONE);
            holder.btnBookingInfo.setVisibility(View.GONE);
            holder.btnWaitingVarification.setVisibility(View.VISIBLE);
        }*/
        holder.btnBookingFee.setVisibility(View.GONE);
        holder.btnBookingInfo.setVisibility(View.GONE);
        holder.btnWaitingVarification.setVisibility(View.GONE);

        /*holder.btnBookingFee.setTypeface(font);
        holder.btnBookingFee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbMasterRef = list.get(position).getDbMasterRef();
                projectRef = list.get(position).getProjectRef();
                categoryRef = list.get(position).getCategoryRef();
                clusterRef = list.get(position).getClusterRef();
                productRef = list.get(position).getProductRef();
                unitRef = list.get(position).getUnitRef();
                termRef = list.get(position).getTermRef();
                termNo = list.get(position).getTermNo();
                bookingRef = list.get(position).getBookingRef();
                projectName = list.get(position).getProjectName();
                projectBookingRef = list.get(position).getProjectBookingRef();
                memberCustomerRef = list.get(position).getMemberCostumerRef();

                Intent intent = new Intent(context, BookingPaymentMethodActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CATEGORY_REF, categoryRef);
                intent.putExtra(CLUSTER_REF, clusterRef);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(UNIT_REF, unitRef);
                intent.putExtra(TERM_REF, termRef);
                intent.putExtra(TERM_NO, termNo);
                intent.putExtra(BOOKING_REF, bookingRef);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(TYPE_PAYMENT, 1);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                SharedPreferences sharedPreferences = context.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", memberCustomerRef);
                editor.commit();
                context.startActivity(intent);
            }
        });

        holder.btnWaitingVarification.setTypeface(font);
        holder.btnWaitingVarification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbMasterRef = list.get(position).getDbMasterRef();
                projectRef = list.get(position).getProjectRef();
                bookingRef = list.get(position).getBookingRef();
                memberCustomerRef = list.get(position).getMemberCostumerRef();
                projectBookingRef = list.get(position).getProjectBookingRef();

                Intent intent = new Intent(context, MyBookingInfoProjectPsActivity.class);
                intent.putExtra(BOOKING_REF, bookingRef);
                intent.putExtra("waitingVerification", 1);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                SharedPreferences sharedPreferences = context.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", memberCustomerRef);
                editor.commit();
                context.startActivity(intent);
            }
        });

        holder.btnBookingInfo.setTypeface(font);
        holder.btnBookingInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbMasterRef = list.get(position).getDbMasterRef();
                projectRef = list.get(position).getProjectRef();
                bookingRef = list.get(position).getBookingRef();
                memberCustomerRef = list.get(position).getMemberCostumerRef();
                projectBookingRef = list.get(position).getProjectBookingRef();
                linkDownload = list.get(position).getLinkDownload();

                Intent intent = new Intent(context, MyBookingInfoActivity.class);
                intent.putExtra(BOOKING_REF, bookingRef);
                intent.putExtra("waitingVerification", 0);
                intent.putExtra("linkDownload", linkDownload);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                SharedPreferences sharedPreferences = context.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", memberCustomerRef);
                editor.commit();
                context.startActivity(intent);
            }
        });*/

        return convertView;
    }

    private class ListMyBookingHolder {
        TextView bookingCode, projectName, clusterName, blockName, unitName,costumerName;
        Button btnBookingFee, btnBookingInfo, btnWaitingVarification;
        LinearLayout linearItem;
    }

}
