package com.nataproperty.solourbana.view.logbook.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.ui.AddLogBookProjectActivity;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class LogBookContactBankAdapter extends BaseAdapter {
    public static final String TAG = "NotificationAdapter";
    public static final String PREF_NAME = "pref";

    private Context context;
    private ArrayList<LogBookModel> list;
    private ListNotificationHolder holder;
    private String dbMasterRef, projectRef;

    public LogBookContactBankAdapter(Context context, ArrayList<LogBookModel> list, String dbMasterRef, String projectRef) {
        this.context = context;
        this.list = list;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
    }

    SharedPreferences sharedPreferences;
    private boolean state;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_log_book_contact_bank, null);
            holder = new ListNotificationHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.hp = (TextView) convertView.findViewById(R.id.txt_hp);
            holder.email = (TextView) convertView.findViewById(R.id.txt_email);
            holder.address = (TextView) convertView.findViewById(R.id.txt_address);
            holder.btnAddToProject = (TextView) convertView.findViewById(R.id.btn_add_to_project);

            convertView.setTag(holder);
        } else {
            holder = (ListNotificationHolder) convertView.getTag();
        }

        final LogBookModel logBookModel = list.get(position);
        holder.name.setText(logBookModel.getName());
        holder.hp.setText(logBookModel.getHp1());
        if (logBookModel.getEmail1().equals("")) {
            holder.email.setText("-");
        } else {
            holder.email.setText(logBookModel.getEmail1());
        }
        if (logBookModel.getAddress().trim().equals("")) {
            holder.address.setText(logBookModel.getCityName() + ", " + logBookModel.getProvinceName());
        } else {
            holder.address.setText(logBookModel.getAddress() + ", " + logBookModel.getCityName() + ", "
                    + logBookModel.getProvinceName());
        }

        holder.btnAddToProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddLogBookProjectActivity.class);
                intent.putExtra("dbMasterRef", dbMasterRef);
                intent.putExtra("projectRef", projectRef);
                intent.putExtra("customerStatus", General.customerStatusVerifyRef);
                intent.putExtra("customerName", logBookModel.getName());
                intent.putExtra("countryCode", logBookModel.getCountryCode());
                intent.putExtra("provinceCode", logBookModel.getProvinceCode());
                intent.putExtra("cityCode", logBookModel.getCityCode());
                intent.putExtra("postCode", logBookModel.getPostCode());
                intent.putExtra("hp1", logBookModel.getHp1());
                intent.putExtra("hp2", logBookModel.getHp2());
                intent.putExtra("phone1", logBookModel.getPhone1());
                intent.putExtra("phone2", logBookModel.getPhone2());
                intent.putExtra("email1", logBookModel.getEmail1());
                intent.putExtra("email2", logBookModel.getEmail2());
                intent.putExtra("customerRef", logBookModel.getCustomerRef());

                context.startActivity(intent);
            }
        });


        return convertView;
    }


    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListNotificationHolder {
        TextView name, hp, email, address;
        TextView btnAddToProject;
    }

}
