package com.nataproperty.solourbana.view.booking.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jaredrummler.android.device.DeviceName;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoLight;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.booking.model.ParamConfirmPaymentModel;
import com.nataproperty.solourbana.view.booking.model.PaymentModel;
import com.nataproperty.solourbana.view.booking.model.SaveBookingModel;
import com.nataproperty.solourbana.view.booking.presenter.BookingBookingBankTransferPresenter;
import com.nataproperty.solourbana.view.nup.adapter.AccountBankAdapter;
import com.nataproperty.solourbana.view.nup.adapter.AccountVirtualAdapter;
import com.nataproperty.solourbana.view.nup.adapter.BankAdapter;
import com.nataproperty.solourbana.view.nup.model.AccountBankModel;
import com.nataproperty.solourbana.view.nup.model.BankModel;
import com.nataproperty.solourbana.view.nup.model.NupVirtualAccountModel;
import com.nataproperty.solourbana.view.nup.ui.NupBankTransferActivity;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.nataproperty.solourbana.view.profile.ui.EditProfileActivity.getImageContentUri;

/**
 * Created by UserModel on 5/21/2016.
 */
public class BookingBankTransferActivity extends AppCompatActivity {
    public static final String TAG = "BookingBankTransfer";
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String TYPE_PAYMENT = "typePayment";
    public static final String STATUS_PAYMENT = "statusPayment";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";
    private static final String EXTRA_RX = "EXTRA_RX";
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    BookingBookingBankTransferPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private List<AccountBankModel> listAccountBank = new ArrayList<AccountBankModel>();
    private ArrayList<NupVirtualAccountModel> listVA = new ArrayList<NupVirtualAccountModel>();
    private AccountBankAdapter adapterAccountbank;
    private AccountVirtualAdapter adapterAccountVirtual;
    MyListView listView;
    private List<BankModel> listBank = new ArrayList<BankModel>();
    private BankAdapter adapterBank;
    //logo
    ImageView imgLogo;
    TextView txtProjectName;
    MyTextViewLatoReguler txtConfirmPayment, txtUploadReceipt;
    MyTextViewLatoLight txtReceipt;

    SharedPreferences sharedPreferences;
    ImageView imgPayment;
    Spinner bankList;
    EditText accountName, accountNumber;
    Button btnConfirmPayment;
    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    private TextView txtBookingFee, txtBookingInfo;
    private String propertys, category, product, unit, area, priceInc;
    private String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    String dbMasterRef, projectRef, memberRef, projectName, bookingRef, clusterRef, productRef
            , unitRef, termRef, termNo,va1, va2;
    String bankRef, fileName = "", bankRefSave, extension, manufacturer, mTmpGalleryPicturePath;
    String cekImg = "";
    String projectBookingRef;
    int typePayment;
    private String bookingFee;
    ProgressDialog progressDialog;
    private Bitmap bitmap;
    Toolbar toolbar;
    TextView title;
    Typeface font;
    LinearLayout linearLayout;
    LinearLayout linlayBank, linlayVA, linlayListBank, linlayAccountName, linlayAccountNumber;
    MyListView listViewVA;
    Uri file, uri, selectedImage;
    String pathPayment = "";

    TextView txtNamaBankBaru, txtAccountBankBaru;
    String txtNamaBank_Baru, txtAccountName_Baru;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_bank_transfer);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new BookingBookingBankTransferPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        bankRef = sharedPreferences.getString("isBankRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        typePayment = intent.getIntExtra(TYPE_PAYMENT, 0);
        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");

        Log.d("va",va1+va2);
        initWidget();

        imgPayment = (ImageView) findViewById(R.id.img_receipt);
        imgPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = BookingBankTransferActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isBankRef", bankRef);
                editor.commit();
                Log.d("isBankRef", "" + bankRef);
                selectImage();
            }
        });

        requestPayment();
        requestMyBookingDetail();
        requestAcoountBank();
        requestVirtualAccount();
        requestBank();

        if (bankRef != null) {
            bankList.setSelection(Integer.parseInt(bankRef));
        } else {

        }

        bankList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankRef = listBank.get(position).getBankRef();
                Log.d("bank ref", " " + bankRef);
                bankList.setSelection(Integer.parseInt(bankRef));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnConfirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cekImg.equals("") && !pathPayment.equals("")) {
                    confirmPayment();
                } else {
                    if (cekImg.equals("")) {
                        Snackbar snackbar = Snackbar.make(linearLayout, "Upload bukti transfer", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    if (pathPayment.equals("")) {
                        Snackbar snackbar = Snackbar.make(linearLayout, "Upload bukti transfer", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }


//                String cekAccName = accountName.getText().toString();
//                String cekAccNum = accountNumber.getText().toString();
//                String cekBank = bankRef;
//
//                if (!cekAccName.isEmpty() && !cekAccNum.isEmpty() && !cekBank.equals("0") && !cekImg.equals("") && !pathPayment.equals("")) {
//                    confirmPayment();
//                } else {
//                    if (cekAccName.isEmpty()) {
//                        accountName.setError("Harus diisi.");
//                    } else {
//                        accountName.setError(null);
//                    }
//                    if (cekAccNum.isEmpty()) {
//                        accountNumber.setError("Harus diisi.");
//                    } else {
//                        accountName.setError(null);
//                    }
//                    if (bankRef.equals("0")) {
//                        Snackbar snackbar = Snackbar.make(linearLayout, "Bank harus dipilih", Snackbar.LENGTH_LONG);
//                        snackbar.show();
//                    } else if (cekImg.equals("")) {
//                        Snackbar snackbar = Snackbar.make(linearLayout, "Upload bukti tranfer", Snackbar.LENGTH_LONG);
//                        snackbar.show();
//                    }
//                    if (pathPayment.equals("")) {
//                        Snackbar snackbar = Snackbar.make(linearLayout, "Upload bukti tranfer", Snackbar.LENGTH_LONG);
//                        snackbar.show();
//                    }
//                }

            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Bank Transfer");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * booking fee
         */
        txtBookingFee = (TextView) findViewById(R.id.txt_booking_fee);
        txtBookingInfo = (TextView) findViewById(R.id.txt_booking_info);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtNamaBankBaru = (TextView) findViewById(R.id.txt_nama_bank_baru);
        txtAccountBankBaru = (TextView) findViewById(R.id.txt_account_name_baru);

        linlayBank = (LinearLayout) findViewById(R.id.linlay_account_bank);
        listView = (MyListView) findViewById(R.id.list_account_bank);
        linlayVA = (LinearLayout) findViewById(R.id.linlay_virtual_account);
        listViewVA = (MyListView) findViewById(R.id.list_virtual_account);

        txtConfirmPayment = (MyTextViewLatoReguler) findViewById(R.id.txt_confirm_payment);
        txtUploadReceipt = (MyTextViewLatoReguler) findViewById(R.id.txt_upload_receipt);
        linlayListBank = (LinearLayout) findViewById(R.id.linlay_list_bank);
        linlayAccountName = (LinearLayout) findViewById(R.id.linlay_account_name);
        linlayAccountNumber = (LinearLayout) findViewById(R.id.linlay_account_number);
        txtReceipt = (MyTextViewLatoLight) findViewById(R.id.txt_receipt);

        bankList = (Spinner) findViewById(R.id.list_bank_tranfer);
        accountName = (EditText) findViewById(R.id.edit_account_name);
        accountNumber = (EditText) findViewById(R.id.edit_account_number);

        btnConfirmPayment = (Button) findViewById(R.id.btn_confirm);
        btnConfirmPayment.setTypeface(font);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};
        AlertDialog.Builder builder = new AlertDialog.Builder(BookingBankTransferActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkCameraPermission()) {
                        ActivityCompat.requestPermissions(BookingBankTransferActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                    } else {
                        openIntentCamera();
                    }
                } else if (items[item].equals("Use Existing Foto")) {
                    if (checkWriteExternalPermission()) {
                        openIntentFile();
                    } else {
                        ActivityCompat.requestPermissions(BookingBankTransferActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);
                    }

                }
            }
        });
        builder.show();
    }

    public boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(BookingBankTransferActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }


    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = BookingBankTransferActivity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

//    public void showDialogCekPermission() {
//        if (ActivityCompat.checkSelfPermission(BookingBankTransferActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(BookingBankTransferActivity.this,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                ActivityCompat.requestPermissions(BookingBankTransferActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                        EXTERNAL_STORAGE_PERMISSION_CONSTANT);
//            }
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == SELECT_FILE) {
                openIntentFile();
            } else if (requestCode == REQUEST_CAMERA) {
                openIntentCamera();
            }
        } else {
            proceedAfterPermissionDeny();
        }
    }

//    private void proceedAfterPermissionAllow() {
//        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
//    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void openIntentFile() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void openIntentCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("picUri", file);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        file = savedInstanceState.getParcelable("picUri");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                } else {
                    handleGalleryResult19(data);
                }
            else if (requestCode == REQUEST_CAMERA) {
                imgPayment.setImageBitmap(BitmapFactory.decodeFile(file.getEncodedPath()));
                cekImg = "1";
                pathPayment = file.getEncodedPath();
                Log.d(TAG, "camera " + pathPayment);
            }
            //onCaptureImageResult(data);

        }
    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                cekImg = "1";
                pathPayment = mTmpGalleryPicturePath;
                Log.d(TAG, "media " + pathPayment);
            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                    cekImg = "1";
                    pathPayment = mTmpGalleryPicturePath;
                    Log.d(TAG, "media " + pathPayment);
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "Upload failed", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    private void handleGalleryResult19(Intent data) {
        uri = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                manufacturer = info.manufacturer;

                mTmpGalleryPicturePath = getRealPathFromURI(BookingBankTransferActivity.this, uri);
                if (mTmpGalleryPicturePath == null) {
                    mTmpGalleryPicturePath = getPath(selectedImage);
                    pathPayment = mTmpGalleryPicturePath;
                    Log.d(TAG, "media " + pathPayment);
                }

                Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                if (!mTmpGalleryPicturePath.equals("")) {
                    if (mTmpGalleryPicturePath != null) {
                        imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                        cekImg = "1";
                        pathPayment = mTmpGalleryPicturePath;
                        Log.d(TAG, "media " + pathPayment);
                        String filename = mTmpGalleryPicturePath.substring(mTmpGalleryPicturePath.lastIndexOf("/") + 1);
                    } else {
                        try {
                            InputStream is = getContentResolver().openInputStream(selectedImage);
                            //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                            mTmpGalleryPicturePath = selectedImage.getPath();
                            Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                            imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                            cekImg = "1";
                            pathPayment = mTmpGalleryPicturePath;
                            Log.d(TAG, "media " + pathPayment);
                            String filename = mTmpGalleryPicturePath.substring(mTmpGalleryPicturePath.lastIndexOf("/") + 1);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Snackbar snackbar = Snackbar.make(linearLayout, "Upload failed", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        });

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    private void requestPayment() {
        presenter.getIlustrationPaymentSvc(dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo);
    }

    public void showPaymentModelResults(Response<PaymentModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        if (status == 200) {
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();

            propertys = response.body().getPropertyInfo().getPropertys();
            category = response.body().getPropertyInfo().getCategory();
            product = response.body().getPropertyInfo().getProduct();
            unit = response.body().getPropertyInfo().getUnit();
            area = response.body().getPropertyInfo().getLandBuild();
            priceInc = response.body().getPropertyInfo().getPriceInc();
            bookingFee = response.body().getPropertyInfo().getBookingFee();

            txtPropertyName.setText(propertys);
            txtCategoryType.setText(category);
            txtProduct.setText(product);
            txtUnitNo.setText(unit);
            txtArea.setText(Html.fromHtml(area));
            txtEstimate.setText(priceInc);

            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice);

            DecimalFormat decimalFormat = new DecimalFormat("###,##0");
            txtBookingFee.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(bookingFee))));

        } else {

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showPaymentModelFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void requestMyBookingDetail() {
        presenter.getBookingDetailSvc(dbMasterRef, projectRef, projectBookingRef);
    }

    public void showBookingInfoResults(Response<DetailBooking> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            String bookDate = response.body().getBookingInfo().getBookDate();
            String bookHour = response.body().getBookingInfo().getBookHour();

            txtBookingInfo.setText("Lakukan pembayaran booking fee dalam waktu 3 jam atau sebelum pukul " + bookHour + ". " +
                    "jika tidak ada konfirmasi pembayaran maka unit booking akan kembali available");
        } else {
            Log.d(TAG, "get error");
        }
    }

    public void showBookingInfoFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void requestAcoountBank() {
        presenter.getProjectAccountBank(dbMasterRef, projectRef);
    }

    public void showListProjectAccountBankResults(Response<ArrayList<AccountBankModel>> response) {
        listAccountBank = response.body();

        txtNamaBank_Baru = listAccountBank.get(0).getBankName();
        txtNamaBankBaru.setText(txtNamaBank_Baru);

        txtAccountName_Baru = listAccountBank.get(0).getAccName();
        txtAccountBankBaru.setText(txtAccountName_Baru);

        initAccoutBank();
    }

    public void showListProjectAccountBankFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAccoutBank() {
        adapterAccountbank = new AccountBankAdapter(this, listAccountBank);
        listView.setAdapter(adapterAccountbank);
        listView.setExpanded(true);
    }

    private void requestVirtualAccount() {
        if (!va1.equals("") && !va1.equals("0")) {
            listVA.add(new NupVirtualAccountModel(va1));
//            NupVirtualAccountModel nupVAmodel = new NupVirtualAccountModel();
//            nupVAmodel.setNoVA(va1);
//            listVA.add(nupVAmodel);
        }
        if (!va2.equals("") && !va2.equals("0")) {
            listVA.add(new NupVirtualAccountModel(va2));
        }
        if (listVA.size() > 0) {
            linlayVA.setVisibility(View.VISIBLE);
            adapterAccountVirtual = new AccountVirtualAdapter(this, listVA);
            listViewVA.setAdapter(adapterAccountVirtual);
            listViewVA.setExpanded(true);
        } else {
            linlayVA.setVisibility(View.GONE);
        }
        linlayBank.setVisibility(View.GONE);
        txtConfirmPayment.setVisibility(View.GONE);
        linlayListBank.setVisibility(View.GONE);
        linlayAccountName.setVisibility(View.GONE);
        linlayAccountNumber.setVisibility(View.GONE);
        txtReceipt.setVisibility(View.GONE);
        txtUploadReceipt.setVisibility(View.VISIBLE);
    }

    private void requestBank() {
        presenter.getListBankSvc();
    }

    public void showListBankResults(Response<ArrayList<BankModel>> response) {
        listBank = response.body();
        initAdapterBank();
    }

    public void showListBankFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapterBank() {
        adapterBank = new BankAdapter(this, listBank);
        bankList.setAdapter(adapterBank);
    }

    public String getStringImage(String path) {
        Bitmap bmp = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void confirmPayment() {
        Log.d(TAG, "pathPayment " + pathPayment + "");
        //Map<String, String> fields = new HashMap<>();

        ParamConfirmPaymentModel param = new ParamConfirmPaymentModel();
        param.setDbMasterRef(dbMasterRef);
        param.setProjectRef(projectRef);
        param.setBookingRef(bookingRef);
        param.setTermRef(termRef);
        param.setMemberRef(memberRef);
        param.setImage(getStringImage(pathPayment));
        param.setFileName(fileName);
        param.setVa1(va1);
        param.setVa2(va2);
        String jsonIn = Utils.modelToJson(param);

//        fields.put("dbMasterRef", dbMasterRef);
//        fields.put("projectRef", projectRef);
//        fields.put("bookingRef", bookingRef);
//        fields.put("termRef", termRef);
//        fields.put("memberRef", memberRef);
//        //imgPayment.buildDrawingCache();
//        //Bitmap bmap = imgPayment.getDrawingCache();
//        fields.put("image", getStringImage(pathPayment));
//        Log.d(TAG, "image " +getStringImage(pathPayment) + "");
//        fields.put("bankRef", bankRef);
//        fields.put("fileName", fileName);
//        fields.put("accName", accountName.getText().toString());
//        fields.put("accNumber", accountNumber.getText().toString());
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
//        presenter.savePaymentBookingBankTransferSvc(fields);
        presenter.savePaymentBookingBankTransferSvc(jsonIn);
    }

    public void showSaveBookingResults(Response<SaveBookingModel> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()){
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            String bookingNotification = response.body().getBookingNotification();
            if (status == 200) {
                Intent intent = new Intent(BookingBankTransferActivity.this, BookingFinishActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CLUSTER_REF, clusterRef);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(UNIT_REF, unitRef);
                intent.putExtra(TERM_REF, termRef);
                intent.putExtra(TERM_NO, termNo);

                intent.putExtra(BOOKING_REF, bookingRef);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(STATUS_PAYMENT, "1");
                intent.putExtra("typePayment", typePayment);
                intent.putExtra("bookingNotification", bookingNotification);


                SharedPreferences sharedPreferences = BookingBankTransferActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isBankRef", "0");
                editor.commit();
                startActivity(intent);
            } else {
                Snackbar snackbar = Snackbar.make(linearLayout, "Pembayaran gagal", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        } else {

        }

    }

    public void showSaveBookingFailure(Throwable t) {
        progressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(linearLayout, "Booking Failed", Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        SharedPreferences sharedPreferences = BookingBankTransferActivity.this.
                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("isBankRef", "0");
        editor.commit();
        Intent intentProjectMenu = new Intent(BookingBankTransferActivity.this, ProjectMenuActivity.class);
        intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentProjectMenu);
    }
}
