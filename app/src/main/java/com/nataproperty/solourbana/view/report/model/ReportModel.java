package com.nataproperty.solourbana.view.report.model;

/**
 * Created by Nata on 2/27/2017.
 */

public class ReportModel {
    String no, name,aspx;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAspx() {
        return aspx;
    }

    public void setAspx(String aspx) {
        this.aspx = aspx;
    }
}
