package com.nataproperty.solourbana.view.mynup.model;

/**
 * Created by Nata on 1/9/2017.
 */

public class MyNupDetailProjectPsModel {
    String status, message, nupPsCode, qty, nupNo, nupName, amount,nupStatusName,paymentTypeName,bankName,nupidCountryName,
            nupidProvinceName,nupidCityName,nupidAddr,buyDate,clusterName,productName,detailName,blockName,unitName,nuphP1,
            nupphone1,nupemail,nupktpid,custBankAccName,custBankAccNo,custBankBranch;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNupPsCode() {
        return nupPsCode;
    }

    public void setNupPsCode(String nupPsCode) {
        this.nupPsCode = nupPsCode;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getNupNo() {
        return nupNo;
    }

    public void setNupNo(String nupNo) {
        this.nupNo = nupNo;
    }

    public String getNupName() {
        return nupName;
    }

    public void setNupName(String nupName) {
        this.nupName = nupName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getNupStatusName() {
        return nupStatusName;
    }

    public void setNupStatusName(String nupStatusName) {
        this.nupStatusName = nupStatusName;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getNupidCountryName() {
        return nupidCountryName;
    }

    public void setNupidCountryName(String nupidCountryName) {
        this.nupidCountryName = nupidCountryName;
    }

    public String getNupidProvinceName() {
        return nupidProvinceName;
    }

    public void setNupidProvinceName(String nupidProvinceName) {
        this.nupidProvinceName = nupidProvinceName;
    }

    public String getNupidCityName() {
        return nupidCityName;
    }

    public void setNupidCityName(String nupidCityName) {
        this.nupidCityName = nupidCityName;
    }

    public String getNupidAddr() {
        return nupidAddr;
    }

    public void setNupidAddr(String nupidAddr) {
        this.nupidAddr = nupidAddr;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(String buyDate) {
        this.buyDate = buyDate;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getNuphP1() {
        return nuphP1;
    }

    public void setNuphP1(String nuphP1) {
        this.nuphP1 = nuphP1;
    }

    public String getNupphone1() {
        return nupphone1;
    }

    public void setNupphone1(String nupphone1) {
        this.nupphone1 = nupphone1;
    }

    public String getNupemail() {
        return nupemail;
    }

    public void setNupemail(String nupemail) {
        this.nupemail = nupemail;
    }

    public String getNupktpid() {
        return nupktpid;
    }

    public void setNupktpid(String nupktpid) {
        this.nupktpid = nupktpid;
    }

    public String getCustBankAccName() {
        return custBankAccName;
    }

    public void setCustBankAccName(String custBankAccName) {
        this.custBankAccName = custBankAccName;
    }

    public String getCustBankAccNo() {
        return custBankAccNo;
    }

    public void setCustBankAccNo(String custBankAccNo) {
        this.custBankAccNo = custBankAccNo;
    }

    public String getCustBankBranch() {
        return custBankBranch;
    }

    public void setCustBankBranch(String custBankBranch) {
        this.custBankBranch = custBankBranch;
    }
}