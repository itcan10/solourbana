package com.nataproperty.solourbana.view.kpr.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface CalculationInputProjectInteractor {

    void saveNewCalcUserProject(Map<String, String> fields);

}
