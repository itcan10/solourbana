package com.nataproperty.solourbana.view.logbook.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface AddTaskInteractor {
    void GetLookupProjectAction(String dbMasterRef, String projectRef);

    void insertTaskContactBankSvc(Map<String, String> fields, String statusButton, String projectCustomerStatus);

}
