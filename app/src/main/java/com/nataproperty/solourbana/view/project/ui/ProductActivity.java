package com.nataproperty.solourbana.view.project.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyGridView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.nup.ui.NupTermActivity;
import com.nataproperty.solourbana.view.project.adapter.ProductAdapter;
import com.nataproperty.solourbana.view.project.model.ProductModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/4/2016.
 */
public class ProductActivity extends AppCompatActivity {
    private List<ProductModel> listProduct = new ArrayList<ProductModel>();
    private ProductAdapter adapter;
    private MyGridView listView;
    ImageView imgLogo;
    TextView txtProjectName, txtCluster;
    Button btnNUP;
    long dbMasterRef;
    String projectRef, clusterRef, productRef, projectName, bathroom, bedroom, categoryRef, titleProduct,
            newClusterRef, isNUP, nupAmt, isBooking, imageLogo, isShowAvailableUnit, isWaiting, isJoin, isSales,
            clusterName,clusterImg;
    private double latitude, longitude;
    Display display;
    SharedPreferences sharedPreferences;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.product));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();
        dbMasterRef = intent.getLongExtra(General.DBMASTER_REF, 0);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        clusterRef = intent.getStringExtra(General.CLUSTER_REF);
        categoryRef = intent.getStringExtra(General.CATEGORY_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        clusterName = intent.getStringExtra(General.CLUSTER_NAME);
        clusterImg = intent.getStringExtra("imageCluster");

        Log.d("cek productActivity", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef);
        Log.d("TAG", "clusterName " + " " + clusterName);

        latitude = getIntent().getDoubleExtra(General.LATITUDE, 0);
        longitude = getIntent().getDoubleExtra(General.LONGITUDE, 0);

        Log.d("cek productActivity", latitude + " " + longitude);

        isNUP = intent.getStringExtra(General.IS_NUP);
        isBooking = intent.getStringExtra(General.IS_BOOKING);
        nupAmt = getIntent().getStringExtra(General.NUP_AMT);
        isShowAvailableUnit = intent.getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        imageLogo = intent.getStringExtra(General.IMAGE_LOGO);
        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        isWaiting = sharedPreferences.getString("isWaiting", null);
        isJoin = sharedPreferences.getString("isJoin", null);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);

        if (!isNUP.equals("0")) {
            btnNUP.setVisibility(View.VISIBLE);
            if (isJoin.equals("0")) {
                if (isWaiting.equals("1")) {
                    //sudah join tetapi belum di approv
                    btnNUP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogNotJoin(getString(R.string.waitingJoinNup).replace("@projectName", projectName));
                        }
                    });

                } else {
                    //belum join
                    btnNUP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogNotJoin(getString(R.string.notJoinNup).replace("@projectName", projectName));
                        }
                    });

                }

            } else {
                //jika dia sudah join
                btnNUP.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intentNup = new Intent(ProductActivity.this, NupTermActivity.class);
                        intentNup.putExtra(General.PROJECT_REF, projectRef);
                        intentNup.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                        intentNup.putExtra(General.NUP_AMT, nupAmt);
                        intentNup.putExtra("imageLogo", imageLogo);
                        startActivity(intentNup);
                    }
                });

            }
        } else {
            btnNUP.setVisibility(View.GONE);
        }

        //logo , project name dan cluster name
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtCluster = (TextView) findViewById(R.id.txt_cluster);
        txtProjectName.setText(projectName);
        txtCluster.setText(clusterName);

        if (clusterImg==null){
            clusterImg="";
        }
        if (!clusterImg.equals("")) {
            Glide.with(this).load(clusterImg).into(imgLogo);
        }else{
            Glide.with(this).load(imageLogo).into(imgLogo);
        }
        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyGridView) findViewById(R.id.list_product);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productRef = String.valueOf(listProduct.get(position).getProductRef());
                newClusterRef = listProduct.get(position).getClusterRef();
                bathroom = listProduct.get(position).getNumOfBathrooms();
                bedroom = listProduct.get(position).getNumOfBedrooms();
                titleProduct = listProduct.get(position).getTitleProduct();
                isSales = listProduct.get(position).getIsSales();

                if (isSales.equals("1")){
                    Intent intent = new Intent(ProductActivity.this, ProductDetailActivity.class);
                    intent.putExtra(General.DBMASTER_REF, dbMasterRef);
                    intent.putExtra(General.PROJECT_REF, projectRef);
                    intent.putExtra(General.CLUSTER_REF, newClusterRef);
                    intent.putExtra(General.CLUSTER_NAME, clusterName);
                    intent.putExtra(General.PRODUCT_REF, productRef);
                    intent.putExtra(General.CATEGORY_REF, categoryRef);
                    intent.putExtra(General.PROJECT_NAME, projectName);
                    intent.putExtra(General.TITLE_PRODUCT, titleProduct);
                    intent.putExtra(General.BATHROOM, bathroom);
                    intent.putExtra(General.BEDROOM, bedroom);
                    intent.putExtra(General.LATITUDE, latitude);
                    intent.putExtra(General.LONGITUDE, longitude);
                    intent.putExtra(General.IS_NUP, isNUP);
                    intent.putExtra(General.IS_BOOKING, isBooking);
                    intent.putExtra(General.NUP_AMT, nupAmt);
                    intent.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intent.putExtra(General.IMAGE_LOGO, imageLogo);

                    startActivity(intent);
                } else {
                    Snackbar snackbar = Snackbar
                            .make(linearLayout, getString(R.string.comingsoon), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }

            }
        });

        requestProduct();
    }

    public void requestProduct() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getProduct(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListProduct(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        Snackbar snackbar = Snackbar
                                .make(linearLayout, getString(R.string.offline), Snackbar.LENGTH_INDEFINITE)
                                .setAction("RETRY", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivity(new Intent(ProductActivity.this, LaunchActivity.class));
                                        finish();
                                    }
                                });

                        snackbar.setActionTextColor(Color.RED);
                        snackbar.show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("categoryRef", categoryRef);
                params.put("clusterRef", clusterRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "product");

    }

    private void generateListProduct(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProductModel product = new ProductModel();
                product.setDbMasterRef(jo.getString("dbMasterRef"));
                product.setProjectRef(jo.getString("projectRef"));
                product.setClusterRef(jo.getString("clusterRef"));
                product.setProductRef(jo.getLong("productRef"));
                product.setNumOfBathrooms(jo.getString("numOfBathrooms"));
                product.setNumOfBedrooms(jo.getString("numOfBedrooms"));
                product.setTitleProduct(jo.getString("titleProduct"));
                product.setProductDescription(jo.getString("productDescription"));
                product.setNumOfAdditionalrooms(jo.getString("numOfAdditionalrooms"));
                product.setNumOfAdditionalBathrooms(jo.getString("numOfAdditionalBathrooms"));
                product.setImage(jo.getString("image"));
                product.setIsSales(jo.getString("isSales"));
                product.setLinkVR(jo.getString("linkVR"));
                product.setLinkThreesixty(jo.getString("linkThreesixty"));
                listProduct.add(product);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        initListView();
        adapter.notifyDataSetChanged();
    }

    private void initListView() {
        adapter = new ProductAdapter(this, listProduct, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    private void dialogNotJoin(String notJoint) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductActivity.this);
        LayoutInflater inflater = ProductActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView textView = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Notifikasi");
        textView.setText(notJoint);
        dialogBuilder.setPositiveButton("GO TO PROJECT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intentProjectMenu = new Intent(ProductActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                finish();
            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProductActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }
        return super.onOptionsItemSelected(item);

    }
}
