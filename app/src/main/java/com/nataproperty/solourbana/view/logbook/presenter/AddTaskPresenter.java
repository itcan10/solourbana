package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.AddTaskInteractor;
import com.nataproperty.solourbana.view.logbook.model.ContactActionModel;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.logbook.ui.AddTaskActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class AddTaskPresenter implements AddTaskInteractor {
    private AddTaskActivity view;
    private ServiceRetrofitProject service;

    public AddTaskPresenter(AddTaskActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetLookupProjectAction(String dbMasterRef, String projectRef) {
        Call<ContactActionModel> call = service.getAPI().GetLookupProjectAction(dbMasterRef,projectRef);
        call.enqueue(new Callback<ContactActionModel>() {
            @Override
            public void onResponse(Call<ContactActionModel> call, Response<ContactActionModel> response) {
                view.showListAddTaskResults(response);
            }

            @Override
            public void onFailure(Call<ContactActionModel> call, Throwable t) {
                view.showListAddTaskFailure(t);

            }


        });
    }

    @Override
    public void insertTaskContactBankSvc(Map<String, String> fields,final String statusButton,final String projectCustomerStatus) {
        Call<ResponeModel> call = service.getAPI().insertTaskContactBankSvc(fields);
        call.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, Response<ResponeModel> response) {
                view.showResponeResults(response,statusButton,projectCustomerStatus);
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.showResponeFailure(t);
            }
        });
    }

}
