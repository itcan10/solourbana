package com.nataproperty.solourbana.view.booking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterBookingPaymentMethodInteractor {
    void GetPaymentMethodSvc(String dbMasterRef, String projectRef);

    void veritransBookingVTSvc(String dbMasterRef, String projectRef, String memberRef, String bookingRef,
                               String termRef, String paymentType, String paymentMethod);

    void getIlustrationPaymentSvc(String dbMasterRef, String projectRef, String clusterRef, String productRef,
                                  String unitRef, String termRef, String termNo);

    void getBookingDetailSvc (String dbMasterRef,String projectRef, String bookingRef);

}
