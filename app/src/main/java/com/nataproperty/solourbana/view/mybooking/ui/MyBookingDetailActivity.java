package com.nataproperty.solourbana.view.mybooking.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.mybooking.adapter.ListPaymentScheduleAdapter;
import com.nataproperty.solourbana.view.mybooking.model.PaymentSchaduleModel;
import com.nataproperty.solourbana.view.mybooking.presenter.MyBookingDetailPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;

import retrofit2.Response;

public class MyBookingDetailActivity extends AppCompatActivity {
    public static final String TAG = "MyBookingDetailActivity";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";
    private static final String EXTRA_RX = "EXTRA_RX";

    MyBookingDetailPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    private ArrayList<PaymentSchaduleModel> listPayment = new ArrayList<PaymentSchaduleModel>();
    private ListPaymentScheduleAdapter adapter;

    String dbMasterRef, projectRef, bookingRef, total, projectBookingRef, payTerm;

    TextView txtBookDate, txtSalesReferral, txtSalesEvent, txtPurpose, txtSalesLocation,
            txtPaymentTerms, txtNetPrice, txtCluster, txtBlock, txtProduct, txtUnitName;

    TextView txtTotal;
    MyListView listView;

    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_detail);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyBookingDetailPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        payTerm = intent.getStringExtra("payTerm");
        total = intent.getStringExtra("total");

        txtPaymentTerms.setText(payTerm);
        txtNetPrice.setText(total);

        Log.d(TAG, dbMasterRef + " " + projectRef + " " + projectBookingRef);
        requestMyBookingDetail();
        requestPayment();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtBookDate = (TextView) findViewById(R.id.txt_booking_date);
        txtSalesReferral = (TextView) findViewById(R.id.txt_sales_referral);
        txtSalesEvent = (TextView) findViewById(R.id.txt_sales_event);
        txtPurpose = (TextView) findViewById(R.id.txt_purpose);
        txtSalesLocation = (TextView) findViewById(R.id.txt_sales_location);

        listView = (MyListView) findViewById(R.id.list_payment_schedule);
        txtTotal = (TextView) findViewById(R.id.txt_total);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);
    }

    private void requestMyBookingDetail() {
        presenter.getBookingDetailSvc(dbMasterRef, projectRef, projectBookingRef);
    }

    public void showBookingInfoResults(Response<DetailBooking> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            String bookDate = response.body().getBookingInfo().getBookDate();
            String salesReferral = response.body().getBookingInfo().getSalesReferral();
            String salesEvent = response.body().getBookingInfo().getSalesEvent();
            String purpose = response.body().getBookingInfo().getPurpose();
            String salesLocation = response.body().getBookingInfo().getSalesLocation();
            String total = response.body().getUnitInfo().getTotal();

            txtBookDate.setText(bookDate);
            txtSalesReferral.setText(salesReferral);
            txtSalesEvent.setText(salesEvent);
            txtPurpose.setText(purpose);
            txtSalesLocation.setText(salesLocation);
            txtTotal.setText(total);

        } else {
            Log.d(TAG, "get error");
        }
    }

    public void showBookingInfoFailure(Throwable t) {

    }

    private void requestPayment() {
        presenter.getListPaymentScheduleSvc(dbMasterRef, projectRef, projectBookingRef);
    }

    public void showListPaymentScheduleResults(Response<ArrayList<PaymentSchaduleModel>> response) {
        listPayment = response.body();
        initAdapter();
    }

    public void showListPaymentScheduleFailure(Throwable t) {

    }

    private void initAdapter() {
        adapter = new ListPaymentScheduleAdapter(this, listPayment);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
