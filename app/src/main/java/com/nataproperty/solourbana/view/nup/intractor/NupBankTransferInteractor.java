package com.nataproperty.solourbana.view.nup.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface NupBankTransferInteractor {

    void getProjectAccountBank(String dbMasterRef, String projectRef);

    void getListBankSvc();

    void savePaymentBankTransferSvc(Map<String, String> fields);

}
