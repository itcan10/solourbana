package com.nataproperty.solourbana.view.menuitem.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyEditTextLatoReguler;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.loginregister.ui.LoginActivity;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 4/20/2016.
 */
public class ChangePasswordActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String KEY_MEMBERREF = "memberRef";
    public static final String KEY_OLDPASSWORD = "oldPassword";
    public static final String KEY_NEWPASSWORD = "newPassword";

    private String oldPassword, newPassword;
    private String confirmPassword;

    MyEditTextLatoReguler txtOldPassword, txtNewPassword, txtConfirmPassword;
    Button btnChangePassword;

    SharedPreferences sharedPreferences;
    String memberRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_change_password));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtOldPassword = (MyEditTextLatoReguler) findViewById(R.id.txtOldPassword);
        txtNewPassword = (MyEditTextLatoReguler) findViewById(R.id.txtNewPassword);
        txtConfirmPassword = (MyEditTextLatoReguler) findViewById(R.id.txtPasswordConfirm);

        btnChangePassword = (Button) findViewById(R.id.btnChangePassword);
        btnChangePassword.setTypeface(font);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        boolean state = sharedPreferences.getBoolean("isLogin", false);
        String email = sharedPreferences.getString("isEmail", null);
        String name = sharedPreferences.getString("isName", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);

    }

    private void changePassword() {
        if (!validate()) {
            onLoginFailed();
            return;
        }

        btnChangePassword.setEnabled(false);

        oldPassword = txtOldPassword.getText().toString();
        newPassword = txtNewPassword.getText().toString();
        confirmPassword = txtConfirmPassword.getText().toString();

//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.changePassword(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("login", "response login " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                        btnChangePassword.setEnabled(true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_MEMBERREF, memberRef);
                params.put(KEY_OLDPASSWORD, oldPassword);
                params.put(KEY_NEWPASSWORD, newPassword);

                Log.d("cek", memberRef + " " + oldPassword + " " + newPassword);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "changePassword");
    }

    public boolean validate() {
        boolean valid = true;

        String passwordOld = txtOldPassword.getText().toString();
        String passwordNew = txtNewPassword.getText().toString();
        String passwordConfirm = txtConfirmPassword.getText().toString();

        if (passwordOld.isEmpty()) {
            txtOldPassword.setError("enter a password");
            valid = false;
        } else {
            txtOldPassword.setError(null);
        }

        if (passwordNew.isEmpty()) {
            txtNewPassword.setError("enter a password");
            valid = false;
        } else {
            txtNewPassword.setError(null);
        }

        if (passwordConfirm.isEmpty()) {
            txtConfirmPassword.setError("enter a password");
            valid = false;
        } else if (!passwordNew.equals(passwordConfirm)) {
            txtConfirmPassword.setError("password don't match");
            valid = false;
        } else {
            txtConfirmPassword.setError(null);
        }

        return valid;
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Change password failed", Toast.LENGTH_LONG).show();

        btnChangePassword.setEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ChangePasswordActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
