package com.nataproperty.solourbana.view.mynup.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyNupSectionInteractor {
    void getListNupSection(String memberRef,String projectCode);

}
