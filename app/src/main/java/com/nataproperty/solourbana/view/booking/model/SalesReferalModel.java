package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by User on 11/3/2016.
 */
public class SalesReferalModel {
    String salesReferral;
    String salesReferralName;


    public String getSalesReferral() {
        return salesReferral;
    }

    public void setSalesReferral(String salesReferral) {
        this.salesReferral = salesReferral;
    }

    public String getSalesReferralName() {
        return salesReferralName;
    }

    public void setSalesReferralName(String salesReferralName) {
        this.salesReferralName = salesReferralName;
    }
}
