package com.nataproperty.solourbana.view.project.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.CustomTabActivityHelper;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.WebviewFallback;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationProductActivity;
import com.nataproperty.solourbana.view.nup.ui.NupTermActivity;
import com.nataproperty.solourbana.view.project.adapter.FeatureAdapter;
import com.nataproperty.solourbana.view.project.adapter.ProductDetailAdapter;
import com.nataproperty.solourbana.view.project.model.FeatureModel;
import com.nataproperty.solourbana.view.project.model.ProductDetailModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by User on 5/10/2016.
 */
public class ProductDetailActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;
    private List<ProductDetailModel> listProductDetail = new ArrayList<ProductDetailModel>();
    private ProductDetailAdapter adapter;
    private List<FeatureModel> listFeature = new ArrayList<FeatureModel>();
    private FeatureAdapter adapterFeature;
    long dbMasterRef;
    String projectRef, clusterRef, productRef, projectName, categoryRef, titleProduct,
            isNUP, nupAmt, isBooking, imageLogo, isShowAvailableUnit, urlVideo, specialEnquiries, linkThreesixty,
            linkVR, productDescription, numOfBedrooms, numOfBathrooms, categoryName, isWaiting, isJoin,
            type, location, bedroom, bathroom, clusterName;
    TextView txtType, txtLocation, txtBedroom, txtBathroom, txtProductName, txtCluster, txtTitleProduct;
    private ImageView imgProductDetail, imgExspanImages;
    HtmlTextView textDescription;
    ViewPager viewPager;
    Timer timer;
    int page = 0;
    int pageView;
    RelativeLayout rPage;
    private Double latitude, longitude;
    private MyListView listView;
    LinearLayout linearMaps;
    private GoogleMap googleMap;
    Button btnNUP, btnCariUnit, btnPropertyVideo, btnSpecialEnquiries, btnThreesixty, btnVR;
    CircleIndicator indicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Product Detail");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imgProductDetail = (ImageView) findViewById(R.id.img_product_detail);
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        txtTitleProduct = (TextView) findViewById(R.id.txt_title_product);
        txtType = (TextView) findViewById(R.id.txt_type);
        txtLocation = (TextView) findViewById(R.id.txt_location);
        txtBedroom = (TextView) findViewById(R.id.txt_bedrooms);
        txtBathroom = (TextView) findViewById(R.id.txt_bathrooms);
        txtProductName = (TextView) findViewById(R.id.txt_project_name);
        txtCluster = (TextView) findViewById(R.id.txt_cluster);
        listView = (MyListView) findViewById(R.id.list_feature);
        textDescription = (HtmlTextView) findViewById(R.id.html_text_description);
        linearMaps = (LinearLayout) findViewById(R.id.linier_maps);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.pager);
        btnThreesixty = (Button) findViewById(R.id.btn_threesixty);
        btnVR = (Button) findViewById(R.id.btn_vr);
        btnPropertyVideo = (Button) findViewById(R.id.btn_property_video);
        btnSpecialEnquiries = (Button) findViewById(R.id.btn_specialEnquiries);
        btnCariUnit = (Button) findViewById(R.id.btn_cari_unit);
        btnNUP = (Button) findViewById(R.id.btn_NUP);

        final Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.777777777777778;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        viewPager.setLayoutParams(params);
        viewPager.requestLayout();

        Intent intent = getIntent();
        dbMasterRef = intent.getLongExtra(General.DBMASTER_REF, 0);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        clusterRef = intent.getStringExtra(General.CLUSTER_REF);
        categoryRef = intent.getStringExtra(General.CATEGORY_REF);
        clusterName = intent.getStringExtra(General.CLUSTER_NAME);
        productRef = intent.getStringExtra(General.PRODUCT_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        titleProduct = intent.getStringExtra(General.TITLE_PRODUCT);
        isShowAvailableUnit = intent.getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        latitude = getIntent().getDoubleExtra(General.LATITUDE, 0);
        longitude = getIntent().getDoubleExtra(General.LONGITUDE, 0);
        isNUP = intent.getStringExtra(General.IS_NUP);
        isBooking = intent.getStringExtra(General.IS_BOOKING);
        nupAmt = getIntent().getStringExtra(General.NUP_AMT);
        imageLogo = intent.getStringExtra(General.IMAGE_LOGO);
        Log.d("maps", " " + latitude + longitude);
        Log.d("cek productDetail", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef + " " + isShowAvailableUnit);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        isWaiting = sharedPreferences.getString("isWaiting", null);
        isJoin = sharedPreferences.getString("isJoin", null);

        if (latitude.equals(0.0)) {
            linearMaps.setVisibility(View.GONE);
        } else {
            linearMaps.setVisibility(View.VISIBLE);
        }

        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

        btnNUP.setTypeface(font);
        if (!isNUP.equals("0")) {
            btnNUP.setVisibility(View.VISIBLE);
            if (isJoin.equals("0")) {
                if (isWaiting.equals("1")) {
                    //sudah join tetapi belum di approv
                    btnNUP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogNotJoin(getString(R.string.waitingJoinNup).replace("@projectName", projectName));
                        }
                    });

                } else {
                    //belum join
                    btnNUP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogNotJoin(getString(R.string.notJoinNup).replace("@projectName", projectName));
                        }
                    });

                }

            } else {
                //jika dia sudah join
                btnNUP.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intentNup = new Intent(ProductDetailActivity.this, NupTermActivity.class);
                        intentNup.putExtra(General.PROJECT_REF, projectRef);
                        intentNup.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                        intentNup.putExtra(General.PROJECT_NAME, projectName);
                        intentNup.putExtra(General.NUP_AMT, nupAmt);
                        startActivity(intentNup);
                    }
                });

            }
        } else {
            btnNUP.setVisibility(View.GONE);
        }

        if (!isBooking.equals("0")) {
            btnCariUnit.setVisibility(View.VISIBLE);
        } else {
            btnCariUnit.setVisibility(View.GONE);
        }

        btnPropertyVideo.setTypeface(font);
        btnPropertyVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailActivity.this, YoutubeActivity.class);
                intent.putExtra(General.URL_VIDEO, urlVideo);
                startActivity(intent);
            }
        });

        //booking
        btnCariUnit.setTypeface(font);
        btnCariUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentProduct = new Intent(ProductDetailActivity.this, IlustrationProductActivity.class);
                intentProduct.putExtra(General.PROJECT_REF, projectRef);
                intentProduct.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProduct.putExtra(General.CLUSTER_REF, clusterRef);
                intentProduct.putExtra(General.CATEGORY_REF, categoryRef);
                intentProduct.putExtra(General.CLUSTER_NAME, clusterName);
                intentProduct.putExtra(General.PROJECT_NAME, projectName);
                intentProduct.putExtra(General.PRODUCT_REF, productRef);
                intentProduct.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                intentProduct.putExtra(General.TITLE_PRODUCT, titleProduct);
                startActivity(intentProduct);
            }
        });

        adapter = new ProductDetailAdapter(this, listProductDetail);
        viewPager.setAdapter(adapter);
        pageSwitcher(4);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());

        requestProductDetail();

        requestProductDetailImage();

        /**
         * feature
         */
        adapterFeature = new FeatureAdapter(this, listFeature);
        listView.setAdapter(adapterFeature);
        listView.setExpanded(true);

        btnSpecialEnquiries.setTypeface(font);
        btnSpecialEnquiries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductDetailActivity.this);
                LayoutInflater inflater = ProductDetailActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
                dialogBuilder.setView(dialogView);

                final TextView txtspecialEnquiries = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
                dialogBuilder.setMessage("Special Enquiries");
                txtspecialEnquiries.setText(specialEnquiries);

                dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });

        btnThreesixty.setTypeface(font);
        btnThreesixty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                        .setToolbarColor(getResources().getColor(R.color.colorPrimary))
                        .build();
                CustomTabActivityHelper.openCustomTab(
                        ProductDetailActivity.this, customTabsIntent, Uri.parse(linkThreesixty), new WebviewFallback());
            }
        });

        btnVR.setTypeface(font);
        btnVR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                        .setToolbarColor(getResources().getColor(R.color.colorPrimary))
                        .build();
                CustomTabActivityHelper.openCustomTab(
                        ProductDetailActivity.this, customTabsIntent, Uri.parse(linkVR), new WebviewFallback());
            }
        });
    }


    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay

    }

    class RemindTask extends TimerTask {
        @Override
        public void run() {
            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {
                    //viewPager.setCurrentItem(page++);
                    if (page == pageView) { // In my case the number of pages are 5
                        //timer.cancel();
                        page = 0;
                    } else {
                        viewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.maps)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void requestProductDetail() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getProductDetail(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result product detail", response);
                    int status = jo.getInt("status");
                    if (status == 200) {
                        productDescription = jo.getJSONObject("data").getString("productDescription");
                        urlVideo = jo.getJSONObject("data").getString("urlVideo");

                        numOfBedrooms = jo.getJSONObject("data").getString("numOfBedrooms");
                        numOfBathrooms = jo.getJSONObject("data").getString("numOfBathrooms");
                        location = jo.getJSONObject("data").getString("location");
                        categoryName = jo.getJSONObject("data").getString("categoryName");
                        specialEnquiries = jo.getJSONObject("data").getString("specialEnquiries");
                        linkThreesixty = jo.getJSONObject("data").getString("linkThreesixty");
                        linkVR = jo.getJSONObject("data").getString("linkVR");

                        if (!urlVideo.equals("")) {
                            btnPropertyVideo.setVisibility(View.VISIBLE);
                        } else {
                            btnPropertyVideo.setVisibility(View.GONE);
                        }

                        if (!specialEnquiries.equals("")) {
                            btnSpecialEnquiries.setVisibility(View.VISIBLE);
                        }

                        if (!linkThreesixty.equals("")) {
                            btnThreesixty.setVisibility(View.VISIBLE);
                        }

                        if (!linkVR.equals("")) {
                            btnVR.setVisibility(View.VISIBLE);
                        }

                        txtTitleProduct.setText(titleProduct);
                        txtBedroom.setText(numOfBedrooms);
                        txtBathroom.setText(numOfBathrooms);
                        txtLocation.setText(location);
                        txtType.setText(categoryName);
                        txtCluster.setText(clusterName);

                        textDescription.setHtmlFromString(productDescription, new HtmlTextView.RemoteImageGetter());
                        requestFeature();

                    } else {
                        String message = jo.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productDeatil");

    }

    public void requestProductDetailImage() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListProductImage(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result feature", " " + response);
                    generateListProductImage(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);
                params.put("productRef", productRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productImage");
    }

    private void generateListProductImage(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProductDetailModel imageProduct = new ProductDetailModel();
                imageProduct.setDbMasterRef(jo.getString("dbMasterRef"));
                imageProduct.setProjectRef(jo.getString("projectRef"));
                imageProduct.setProductRef(jo.getString("productRef"));
                imageProduct.setDbMasterProjectProductFileRef(jo.getString("dbMasterProjectProductFileRef"));
                imageProduct.setQuerystring(jo.getString("querystring"));
                imageProduct.setTitle(jo.getString("title"));

                listProductDetail.add(imageProduct);

                pageView = response.length();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    public void requestFeature() {
        listFeature.clear();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getFeatureList(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result feature", " " + response);
                    generateListFeature(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", String.valueOf(dbMasterRef));
                params.put("projectRef", projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productFeature");

    }

    private void generateListFeature(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                Gson gson = new GsonBuilder().serializeNulls().create();
                FeatureModel featureModel = gson.fromJson("" + jo, FeatureModel.class);
                featureModel.setProjectFeature(jo.getLong("projectFeature"));
                featureModel.setDbMasterRef(jo.getString("dbMasterRef"));
                featureModel.setProjectRef(jo.getString("projectRef"));
                featureModel.setProjectFeatureName(jo.getString("projectFeatureName"));
                listFeature.add(featureModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterFeature.notifyDataSetChanged();
    }

    private void dialogNotJoin(String notJoint) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductDetailActivity.this);
        LayoutInflater inflater = ProductDetailActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView textView = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Notifikasi");
        textView.setText(notJoint);
        dialogBuilder.setPositiveButton("GO TO PROJECT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intentProjectMenu = new Intent(ProductDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                finish();
            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProductDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
