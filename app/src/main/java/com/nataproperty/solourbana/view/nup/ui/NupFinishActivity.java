package com.nataproperty.solourbana.view.nup.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;
import com.nataproperty.solourbana.view.mynup.ui.MyNupActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/21/2016.
 */
public class NupFinishActivity extends AppCompatActivity {
    public static final String TAG = "NupFinishActivity";
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String PROJECT_NAME = "projectName";
    SharedPreferences sharedPreferences;
    ImageView imgProject;
    TextView txtProjectName, txtCostumerName, txtOrderCode, txtPriortyPass, txtAmount, txtEmail, txtAmountTotal;
    String costumerName, orderCode, priorityPass, amount, projectName, totalAmount;
    String email;
    String nupOrderRef, projectRef, dbMasterRef, paymentType,bookingNotification;
    Button btnMyNup;
    Toolbar toolbar;
    Typeface font, fontLight;
    TextView title;
    boolean mylistNup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_finish);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        email = sharedPreferences.getString("isEmail", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        paymentType = intent.getStringExtra("paymentType");
        mylistNup = intent.getBooleanExtra("mylistNup", false);
        bookingNotification = intent.getStringExtra("bookingNotification");
        initWidget();

        Log.d(TAG, "paymentType " + paymentType);

        btnMyNup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(NupFinishActivity.this, MyNupActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                finish();
            }
        });
        requestFinish();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_finish_nup));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imgProject = (ImageView) findViewById(R.id.image_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtCostumerName = (TextView) findViewById(R.id.txt_customer_name);
        txtOrderCode = (TextView) findViewById(R.id.txt_order_code);
        txtPriortyPass = (TextView) findViewById(R.id.txt_priorty_pass);
        txtAmount = (TextView) findViewById(R.id.txt_amount);
        txtAmountTotal = (TextView) findViewById(R.id.txt_amount_total);
        txtEmail = (TextView) findViewById(R.id.member_email);
        btnMyNup = (Button) findViewById(R.id.btn_my_nup);

        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgProject);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        btnMyNup.setTypeface(font);

//        bookingNotification="";

        if (bookingNotification.equals("")||bookingNotification==null) {
            txtEmail.setText(getResources().getString(R.string.booking_bank_tranfer));
        } else {
            txtEmail.setText(bookingNotification);
        }
    }

    private void requestFinish() {
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getOrderInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result nup review", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        projectName = jo.getJSONObject("data").getString("projectName");
                        orderCode = jo.getJSONObject("data").getString("nupOrderCode");
                        costumerName = jo.getJSONObject("data").getString("customerName");
                        priorityPass = jo.getJSONObject("data").getString("quantity");
                        amount = jo.getJSONObject("data").getString("amount");
                        totalAmount = jo.getJSONObject("data").getString("totalAmount");

                        txtProjectName.setText(projectName);
                        txtCostumerName.setText(costumerName);
                        txtOrderCode.setText(orderCode);
                        txtPriortyPass.setText(priorityPass);
                        DecimalFormat decimalFormat = new DecimalFormat("###,##0");
                        txtAmount.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(amount))));
                        txtAmountTotal.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(totalAmount))));

//                        if (paymentType.equals(WebService.updatePaymentTypeBankTransfer)) {
//                            String thankYou = txtEmail.getText().toString();
//                            txtEmail.setText(thankYou + " " + email);
//                        } else {
//                            txtEmail.setText("Terimakasih atas pembayaran anda.\n" +
//                                    "Cek Priority Pass (NUP) melalui menu My NUP atau email " + email);
//                        }

                    } else {
                        Log.d("error", " " + message);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupFinishActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupFinishActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("nupOrderRef", nupOrderRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestFinishNup");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_top_right:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mylistNup) {
            Intent i = new Intent(NupFinishActivity.this, MyNupActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } else {
            Intent intent = new Intent(NupFinishActivity.this, ProjectMenuActivity.class);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
            intent.putExtra(PROJECT_NAME, projectName);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

    }
}
