package com.nataproperty.solourbana.view.nup.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.nup.model.JsonInCustSaveOrderNupModel;
import com.nataproperty.solourbana.view.nup.model.JsonInSaveOrderNupModel;
import com.nataproperty.solourbana.view.nup.model.ResponeModelOrderNup;
import com.nataproperty.solourbana.view.nup.presenter.NupReviewOrderPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/18/2016.
 */
public class NupReviewOrderActivity extends AppCompatActivity {
    public static final String TAG = "NupReviewOrderActivity";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String NUP_AMT = "nupAmt";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String BIRTHDATE = "birthdate";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";
    public static final String QTY = "qty";
    public static final String TOTAL = "total";

    public static final String FILE_CODE_KTP = "fileCodeKtp";
    public static final String FILE_CODE_NPWP = "fileCodeNpwp";
    public static final String URL_DOC_KTP = "urlDocKtp";
    public static final String URL_DOC_NPWP = "urlDocNpwp";

    private ProgressDialog progressDialog;
    private String message;
    private NupReviewOrderPresenter presenter;
    private ServiceRetrofit service;

    private SharedPreferences sharedPreferences;

    //logo
    private ImageView imgLogo;
    private TextView txtProjectName;

    private TextView txtPriortyPrice, txtTotal;
    private TextView txtFullname, txtEmail, txtMobile, txtPhone, txtAddress, txtKtpId, txtQty, txtNpwp, txtBirthdate;
    private EditText editFullname, editEmail, editMobile, editPhone, editAddress, editKtpId, editQty;
    private ImageView imgKtp, imgNpwp;
    private Button btnUploadKtp, btnUploadNpwp, btnNext;

    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address, qty, total , npwp, birthdate;
    private String dbMasterRef, projectRef, projectName, memberRef, memberCustomerRef, nupAmt, projectDescription, status;
    private String nupOrderRef, surveyUnitTypeRef, surveyPaymentTermRef, surveyUnitViewRef, surveyUnitFloorRef;
    private String fileCodeKtp, fileCodeNpwp,urlDocKtp, urlDocNpwp,flaq;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_review_order);

        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new NupReviewOrderPresenter(this, service);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_nup_review));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
//        memberCustomerRef = sharedPreferences.getString("isMemberCostumerRef", null);

        Intent intent = getIntent();
        memberCustomerRef = intent.getStringExtra("memberCustomerRef");
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupAmt = intent.getStringExtra(NUP_AMT);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);
        projectName = intent.getStringExtra(PROJECT_NAME);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        fullname = intent.getStringExtra(FULLNAME);
        birthdate = intent.getStringExtra(BIRTHDATE);
        mobile1 = intent.getStringExtra(MOBILE);
        phone1 = intent.getStringExtra(PHONE);
        email1 = intent.getStringExtra(EMAIL);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);
        ktpRef = intent.getStringExtra(KTP_REF);
        npwpRef = intent.getStringExtra(NPWP_REF);
        qty = intent.getStringExtra(QTY);
        total = intent.getStringExtra(TOTAL);
        npwp = intent.getStringExtra("npwp");
        surveyUnitTypeRef = intent.getStringExtra("surveyUnitTypeRef");
        surveyPaymentTermRef = intent.getStringExtra("surveyPaymentTermRef");
        surveyUnitViewRef = intent.getStringExtra("surveyUnitViewRef");
        surveyUnitFloorRef = intent.getStringExtra("surveyUnitFloorRef");

        fileCodeKtp = intent.getStringExtra(FILE_CODE_KTP);
        fileCodeNpwp = intent.getStringExtra(FILE_CODE_NPWP);
        urlDocKtp = intent.getStringExtra(URL_DOC_KTP);
        urlDocNpwp = intent.getStringExtra(URL_DOC_NPWP);

        Log.d(TAG, "getIntent " + surveyUnitTypeRef + " " + surveyPaymentTermRef + " " + surveyUnitViewRef + " " + surveyUnitFloorRef);

        txtFullname = (TextView) findViewById(R.id.txt_fullname);
        txtBirthdate = (TextView) findViewById(R.id.txt_Birthdate);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtMobile = (TextView) findViewById(R.id.txt_mobile);
        txtPhone = (TextView) findViewById(R.id.txt_phone);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtKtpId = (TextView) findViewById(R.id.txt_ktp_id);
        txtQty = (TextView) findViewById(R.id.txt_qty);
        txtNpwp = (TextView) findViewById(R.id.txt_npwp);

        txtPriortyPrice = (TextView) findViewById(R.id.txt_priority_pass);
        txtTotal = (TextView) findViewById(R.id.txt_total);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setTypeface(font);

        /**
         * setterr
         */
        txtFullname.setText(fullname);
        txtBirthdate.setText(birthdate);
        txtEmail.setText(email1);
        txtPhone.setText(phone1);
        txtMobile.setText(mobile1);
        txtAddress.setText(address);
        txtKtpId.setText(ktpid);
        txtNpwp.setText(npwp);
        txtQty.setText(qty);

        txtTotal.setText(total);

        DecimalFormat decimalFormat = new DecimalFormat("###,##0");
        txtPriortyPrice.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(nupAmt))));

//        Glide.with(NupReviewOrderActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
//                .skipMemoryCache(true).into(imgKtp);
//
//        Glide.with(NupReviewOrderActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
//                .skipMemoryCache(true).into(imgNpwp);

        if(urlDocKtp!=null) {
            Glide.with(NupReviewOrderActivity.this).load(urlDocKtp).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgKtp);
        }
        if(urlDocKtp.equals("1")) {
            Glide.with(NupReviewOrderActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgKtp);
        }


        if(urlDocNpwp!=null) {
            Glide.with(NupReviewOrderActivity.this).load(urlDocNpwp).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgNpwp);
        }
        if (urlDocNpwp.equals("1")){
            Glide.with(NupReviewOrderActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgNpwp);
        }


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NupReviewOrderActivity.this);
                alertDialogBuilder.setMessage("Dengan klik “Ya” maka pembelian NUP akan diproses dan Anda harus melunasi pembayaran sesuai dengan jangka waktu yang ditentukan.");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        saveOrderNup();
                        saveOrderNup_v2();


                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });

        Log.d(TAG, total);

    }

    public void saveOrderNup() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.saveOrderNup(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result nup review", response);
                    int status = jo.getInt("status");
                    message = jo.getString("message");
                    String nupOrderRef = jo.getString("nupOrderRef");

                    if (status == 200) {
                        Intent intent = new Intent(NupReviewOrderActivity.this, NupPaymentMethodActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(TOTAL, total);
                        startActivity(intent);
                        finish();
                        Toast.makeText(NupReviewOrderActivity.this, message, Toast.LENGTH_SHORT).show();

                    } else {
                        Log.d("error", " " + message);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupReviewOrderActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupReviewOrderActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String dpMasterRefProjectCode = dbMasterRef + "#" + General.projectCode;
                params.put("dbMasterRef", dpMasterRefProjectCode);
                params.put("projectRef", projectRef);
                params.put("agentMemberRef", memberRef);
                params.put("customerMemberRef", memberCustomerRef);
                params.put("customerNIK", txtKtpId.getText().toString());
                params.put("customerHP", txtMobile.getText().toString());
                params.put("customerPHONE", txtPhone.getText().toString());
                params.put("customerEMAIL", txtEmail.getText().toString());
                params.put("customerADDRESS", txtAddress.getText().toString());
                params.put("NUPQty", txtQty.getText().toString());
                params.put("surveyUnitTypeRef", surveyUnitTypeRef);
                params.put("surveyPaymentTermRef", surveyPaymentTermRef);
                params.put("surveyUnitViewRef", surveyUnitViewRef);
                params.put("surveyUnitFloorRef", surveyUnitFloorRef);

                Log.d(TAG, "param "+dpMasterRefProjectCode+" " + surveyUnitTypeRef + " " + surveyPaymentTermRef + " " + surveyUnitViewRef + " " + surveyUnitFloorRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "saveOrderNup");

    }

    public void saveOrderNup_v2() {

        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);

        JsonInSaveOrderNupModel jsonInSaveOrderNupModel = new JsonInSaveOrderNupModel();
        jsonInSaveOrderNupModel.setProjectCode(General.projectCode);
        jsonInSaveOrderNupModel.setDbMasterRef(dbMasterRef);
        jsonInSaveOrderNupModel.setProjectRef(projectRef);
        jsonInSaveOrderNupModel.setAgentMemberRef(memberRef);
        jsonInSaveOrderNupModel.setNUPQty(qty);
        jsonInSaveOrderNupModel.setSurveyUnitTypeRef(surveyUnitTypeRef);
        jsonInSaveOrderNupModel.setSurveyPaymentTermRef(surveyPaymentTermRef);
        jsonInSaveOrderNupModel.setSurveyUnitViewRef(surveyUnitViewRef);
        jsonInSaveOrderNupModel.setSurveyUnitFloorRef(surveyUnitFloorRef);
//        jsonInSaveOrderNupModel.setUnitRefList(str);

        JsonInCustSaveOrderNupModel jsonInCustSaveOrderNupModel =  new JsonInCustSaveOrderNupModel();
        jsonInCustSaveOrderNupModel.setCustomerMemberRef(memberCustomerRef);
        jsonInCustSaveOrderNupModel.setCustomerName(fullname);
        jsonInCustSaveOrderNupModel.setCustomerBirthDate(birthdate);
        jsonInCustSaveOrderNupModel.setCustomerNIK(ktpid);
        jsonInCustSaveOrderNupModel.setCustomerHP(mobile1);
        jsonInCustSaveOrderNupModel.setCustomerPHONE(phone1);
        jsonInCustSaveOrderNupModel.setCustomerEMAIL(email1);
        jsonInCustSaveOrderNupModel.setCustomerADDRESS(address);
        jsonInCustSaveOrderNupModel.setCustomerNPWP(npwp);
        jsonInCustSaveOrderNupModel.setFileCodeKtp(fileCodeKtp);
        jsonInCustSaveOrderNupModel.setFileCodeNpwp(fileCodeNpwp);

        presenter.saveOrderNup_v2(jsonInSaveOrderNupModel, jsonInCustSaveOrderNupModel);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupReviewOrderActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showResponeResults(retrofit2.Response<ResponeModelOrderNup> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            String nupOrderRef = response.body().getNupOrderRef();
            if (status == 200) {
                Intent intent = new Intent(NupReviewOrderActivity.this, NupPaymentMethodActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("memberCustomerRef", memberCustomerRef);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                intent.putExtra(NUP_AMT, nupAmt);
                intent.putExtra(TOTAL, total);
                intent.putExtra(FULLNAME, fullname);
                intent.putExtra(BIRTHDATE, birthdate);
                intent.putExtra(EMAIL, email1);
                intent.putExtra(MOBILE, mobile1);
                intent.putExtra(PHONE,phone1);
                intent.putExtra(ADDRESS, address);
                intent.putExtra(KTP_ID, ktpid);
                intent.putExtra("npwp", npwp);
                intent.putExtra(KTP_REF, ktpRef);
                intent.putExtra(NPWP_REF, npwpRef);
                intent.putExtra(QTY,qty);
                intent.putExtra(TOTAL, total);

                intent.putExtra(FILE_CODE_NPWP,fileCodeNpwp);
                intent.putExtra(FILE_CODE_KTP, fileCodeKtp);
                intent.putExtra(URL_DOC_NPWP, urlDocNpwp);
                intent.putExtra(URL_DOC_KTP, urlDocKtp);
//                intent.putExtra("NUPPrice", NUPPrice);
                startActivity(intent);
                finish();
                Toast.makeText(NupReviewOrderActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void showResponeFailure(Throwable t) {
    }
}
