package com.nataproperty.solourbana.view.logbook.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface LogBookProjectInteractor {
    void GetContactBankProjectSvc(String dbMasterRef, String projectRef,String memberRef,
                                  String projectCustomerStatusRef, String eventRef, String referralRef, String keyword);

}
