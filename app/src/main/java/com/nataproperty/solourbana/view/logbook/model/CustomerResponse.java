package com.nataproperty.solourbana.view.logbook.model;

/**
 * Created by Nata on 2/17/2017.
 */

public class CustomerResponse {
    String customerResponseStatus, customerResponseStatusName, iconResponse;

    public String getIconResponse() {
        return iconResponse;
    }

    public void setIconResponse(String iconResponse) {
        this.iconResponse = iconResponse;
    }

    public String getCustomerResponseStatus() {
        return customerResponseStatus;
    }

    public void setCustomerResponseStatus(String customerResponseStatus) {
        this.customerResponseStatus = customerResponseStatus;
    }

    public String getCustomerResponseStatusName() {
        return customerResponseStatusName;
    }

    public void setCustomerResponseStatusName(String customerResponseStatusName) {
        this.customerResponseStatusName = customerResponseStatusName;
    }
}
