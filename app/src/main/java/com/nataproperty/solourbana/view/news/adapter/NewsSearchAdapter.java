package com.nataproperty.solourbana.view.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.news.model.NewsModel;
import com.nataproperty.solourbana.view.news.ui.NewsDetailActivity;
import com.nataproperty.solourbana.view.news.ui.NewsGalleryActivity;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class NewsSearchAdapter extends BaseAdapter implements Filterable {

    public static final String DBMASTER_REF = "dbMasterRef" ;
    public static final String PROJECT_REF = "projectRef" ;
    public static final String TITLE = "title" ;
    public static final String PUBLISH_DATE = "publishDate" ;
    public static final String DEVELOPER_NAME = "developerName" ;
    public static final String CONTENT = "content" ;
    public static final String CONTENT_REF = "contentRef" ;
    public static final String LINK_DETAIL = "linkDetail" ;

    public static final String PREF_NAME = "pref" ;

    private int mCount = 4;
    private Context context;
    private ArrayList<NewsModel> list;
    public ArrayList<NewsModel> orig;

    private Display display;
    private ListNewsHolder holder;

    String dbMasterRef,projectRef,txtTitle,publishDate,developerName,content,contentRef,linkDetail,imgSetting;

    public NewsSearchAdapter(Context context, ArrayList<NewsModel> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    SharedPreferences sharedPreferences;
    private boolean state;

   /* public void addMoreItems(int count) {
        mCount += count;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mCount;
    }*/

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state =  sharedPreferences.getBoolean("isLogin", false);

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_news,null);
            holder = new ListNewsHolder();
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.publishDate = (TextView) convertView.findViewById(R.id.txt_publish_date);
            holder.synopsis = (TextView) convertView.findViewById(R.id.txt_synopsis);
            holder.developerName = (TextView) convertView.findViewById(R.id.txt_developer_name);
            holder.imageHeader = (ImageView) convertView.findViewById(R.id.img_header);

            convertView.setTag(holder);
        }else{
            holder = (ListNewsHolder) convertView.getTag();
        }

        NewsModel news = list.get(position);
        holder.title.setText(news.getTitle());
        holder.publishDate.setText(news.getPublishDate());
        holder.synopsis.setText(news.getSynopsis());
        holder.developerName.setText(news.getDeveloperName());

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/1.78125;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = holder.imageHeader.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.imageHeader.setLayoutParams(params);
        holder.imageHeader.requestLayout();

        Glide.with(context)
                .load(news.getImageHeader()).into(holder.imageHeader);

        holder.imageHeader.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dbMasterRef = list.get(position).getDbMasterRef();
                projectRef = list.get(position).getProjectRef();
                txtTitle = list.get(position).getTitle();
                publishDate = list.get(position).getPublishDate();
                developerName = list.get(position).getDeveloperName();
                content = list.get(position).getContent();
                contentRef = list.get(position).getContentRef();
                linkDetail = list.get(position).getLinkDetail();
                imgSetting = list.get(position).getImgSetting();

                if (imgSetting.equals("4")){
                    Intent intent = new Intent(context,NewsGalleryActivity.class);
                    intent.putExtra(DBMASTER_REF,dbMasterRef);
                    intent.putExtra(PROJECT_REF,projectRef);
                    intent.putExtra(TITLE,txtTitle);
                    intent.putExtra(PUBLISH_DATE,publishDate);
                    intent.putExtra(DEVELOPER_NAME,developerName);
                    intent.putExtra(CONTENT,content);
                    intent.putExtra(CONTENT_REF,contentRef);
                    intent.putExtra(LINK_DETAIL,linkDetail);
                    context.startActivity(intent);
                }else {
                    Intent intent = new Intent(context,NewsDetailActivity.class);
                    intent.putExtra(DBMASTER_REF,dbMasterRef);
                    intent.putExtra(PROJECT_REF,projectRef);
                    intent.putExtra(TITLE,txtTitle);
                    intent.putExtra(PUBLISH_DATE,publishDate);
                    intent.putExtra(DEVELOPER_NAME,developerName);
                    intent.putExtra(CONTENT,content);
                    intent.putExtra(CONTENT_REF,contentRef);
                    intent.putExtra(LINK_DETAIL,linkDetail);
                    context.startActivity(intent);
                }
            }
        });


        return convertView;
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.d("constraint",constraint.toString());
                final FilterResults oReturn = new FilterResults();
                final ArrayList<NewsModel> results = new ArrayList<NewsModel>();
                if (orig == null)
                    orig = list;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final NewsModel newsModel : orig) {
                            if (newsModel.getTitle().toLowerCase().contains(constraint.toString()))
                                results.add(newsModel);
                        }
                    }
                    oReturn.values = results;
                    oReturn.count = results.size();

                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (ArrayList<NewsModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListNewsHolder {
        TextView title,publishDate,synopsis,developerName;
        ImageView imageHeader;
    }

}
