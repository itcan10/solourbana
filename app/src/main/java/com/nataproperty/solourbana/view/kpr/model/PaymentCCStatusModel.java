package com.nataproperty.solourbana.view.kpr.model;

import java.util.List;

/**
 * Created by nata on 11/23/2016.
 */

public class PaymentCCStatusModel {
    private int status;
    private String message;
    private DataPrice dataPrice;
    private String totalPayment;
    private List<DataSchedule> dataSchedule;
    private List<DataSchedule2> dataSchedule2;
    private List<DataKPR> dataKPR;
    private List<DataKalkulator> dataKalkulator;
    private PropertyInfo propertyInfo;
    private String urlDownload,fileName;

    public String getUrlDownload() {
        return urlDownload;
    }

    public void setUrlDownload(String urlDownload) {
        this.urlDownload = urlDownload;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataPrice getDataPrice() {
        return dataPrice;
    }

    public void setDataPrice(DataPrice dataPrice) {
        this.dataPrice = dataPrice;
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public List<DataSchedule> getDataSchedule() {
        return dataSchedule;
    }

    public void setDataSchedule(List<DataSchedule> dataSchedule) {
        this.dataSchedule = dataSchedule;
    }

    public List<DataSchedule2> getDataSchedule2() {
        return dataSchedule2;
    }

    public void setDataSchedule2(List<DataSchedule2> dataSchedule2) {
        this.dataSchedule2 = dataSchedule2;
    }

    public List<DataKPR> getDataKPR() {
        return dataKPR;
    }

    public void setDataKPR(List<DataKPR> dataKPR) {
        this.dataKPR = dataKPR;
    }

    public PropertyInfo getPropertyInfo() {
        return propertyInfo;
    }

    public void setPropertyInfo(PropertyInfo propertyInfo) {
        this.propertyInfo = propertyInfo;
    }

    public List<DataKalkulator> getDataKalkulator() {
        return dataKalkulator;
    }

    public void setDataKalkulator(List<DataKalkulator> dataKalkulator) {
        this.dataKalkulator = dataKalkulator;
    }
}
