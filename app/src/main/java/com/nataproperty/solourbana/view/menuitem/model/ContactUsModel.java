package com.nataproperty.solourbana.view.menuitem.model;

/**
 * Created by arifcebe
 * on Mar 3/9/17 15:45.
 * Project : adhipersadaproperti
 */

public class ContactUsModel {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
