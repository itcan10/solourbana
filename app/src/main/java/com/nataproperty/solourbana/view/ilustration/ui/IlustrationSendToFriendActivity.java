package com.nataproperty.solourbana.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 6/4/2016.
 */
public class IlustrationSendToFriendActivity extends AppCompatActivity {
    public static final String TAG = "IlustrationSendToFriend";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";

    public static final String PAYMENT_TERM = "paymentTerm";
    public static final String PROPERTYS = "propertys";

    public static final String PROJECT_NAME = "projectName";

    public static final String IS_BOOKING = "isBooking";

    private SharedPreferences sharedPreferences;

    ProgressDialog progressDialog;

    EditText edtEmail, edtMessage;
    Button btnSend;

    String memberRef, dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo, paymentTerm, projectName, isBooking;
    String propertys, category, product, unit, area, priceInc;
    String message, stringListKpr = "", bookingContact = "";

    int finType;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_send_to_friend);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_send_to_friend));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        projectRef = getIntent().getStringExtra(PROJECT_REF);
        dbMasterRef = getIntent().getStringExtra(DBMASTER_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        paymentTerm = intent.getStringExtra(PAYMENT_TERM);
        propertys = intent.getStringExtra(PROPERTYS);
        isBooking = intent.getStringExtra(IS_BOOKING);
        projectName = intent.getStringExtra(PROJECT_NAME);
        finType = intent.getIntExtra("finType", 0);
        stringListKpr = intent.getStringExtra("stringListKpr");
        bookingContact = intent.getStringExtra("bookingContact");

        Log.d("stringListKpr ", "" + stringListKpr);

        if (stringListKpr == null) {
            stringListKpr = "";
        }

        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtMessage = (EditText) findViewById(R.id.edt_message);

        edtEmail.setHint("email@email , email@email");
        edtMessage.setText("For info, berikut terlampir ilustrasi pembayaran " + paymentTerm + " " + propertys + "\n" +
                getResources().getString(R.string.hint_message_send_to_friend));

        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setTypeface(font);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekEmail = edtEmail.getText().toString();

                if (cekEmail.isEmpty()) {
                    if (cekEmail.isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(cekEmail).matches()) {
                        edtEmail.setError("enter a valid email address");
                    } else {
                        edtEmail.setError(null);
                    }

                } else {
                    sendToFriend();
                }
            }
        });

        requestPaymentTerm();

    }

/*    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }*/

    public void sendToFriend() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.sendToFriendForProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    message = jo.getString("message");
                    if (status == 200) {
                        finish();
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(IlustrationSendToFriendActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(IlustrationSendToFriendActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("dbMasterRef", dbMasterRef);
                params.put("categoryRef", categoryRef);
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);
                params.put("unitRef", unitRef);
                params.put("termRef", termRef);
                params.put("termNo", termNo);
                params.put("sendToFriendEmail", edtEmail.getText().toString());
                params.put("sendToFriendMessage", edtMessage.getText().toString());
                params.put("KPRYear", stringListKpr.toString());
                params.put("projectCode", General.projectCode);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "sendToFriend");

    }

    public void requestPaymentTerm() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getPaymentTerm(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    Log.d("payment", response);

                    if (status == 200) {
                        propertys = jo.getJSONObject("data").getString("propertys");
                        category = jo.getJSONObject("data").getString("category");
                        product = jo.getJSONObject("data").getString("product");
                        unit = jo.getJSONObject("data").getString("unit");
                        area = jo.getJSONObject("data").getString("area");
                        priceInc = jo.getJSONObject("data").getString("priceInc");
                        Log.d("cek result", propertys + " " + category + " " + product + " " + unit + " " + area + " " + priceInc);
//                        IlustrationPaymentActivity.getInstance().finish();

                    } else {

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(IlustrationSendToFriendActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(IlustrationSendToFriendActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);
                params.put("unitRef", unitRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "sendToFriend");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
