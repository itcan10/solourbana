package com.nataproperty.solourbana.view.menuitem.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.event.ui.EventDetailActivity;
import com.nataproperty.solourbana.view.menuitem.model.NotificationDetailModel;
import com.nataproperty.solourbana.view.menuitem.presenter.NotificationDetailPresenter;
import com.nataproperty.solourbana.view.news.ui.NewsDetailActivity;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import retrofit2.Response;

public class NotificationDetailActivity extends AppCompatActivity {
    public static final String TAG = "NotificationDetail";

    public static final String PREF_NAME = "pref";

    public static final String TITLE = "title";
    public static final String DATE = "date";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String GCM_MESSAGE_REF = "gcmMessageRef";
    public static final String GCM_MESSAGE_TYPE_REF = "gcmMessageTypeRef";

    public static final String CONTENT_REF = "contentRef";
    public static final String SCHADULE = "schedule";
    private static final String EXTRA_RX = "EXTRA_RX";

    SharedPreferences sharedPreferences;

    TextView txtTitle, txtDate;
    WebView webView;
    Button btnReadMore;
    String linkDetail, titleGcm, date, gcmMessageRef, gcmMessageTypeRef, memberRef, contentRef, schedule, titleEvent;

    LinearLayout linearLayout;
    Toolbar toolbar;
    TextView title;
    Typeface font;

    NotificationDetailPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new NotificationDetailPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        linkDetail = intent.getStringExtra(LINK_DETAIL);
        titleGcm = intent.getStringExtra(TITLE);
        date = intent.getStringExtra(DATE);
        gcmMessageRef = intent.getStringExtra(GCM_MESSAGE_REF);
        gcmMessageTypeRef = intent.getStringExtra(GCM_MESSAGE_TYPE_REF);

        Log.d(TAG, "" + gcmMessageTypeRef);

        txtTitle.setText(titleGcm);
        txtDate.setText(date);
        webView.loadUrl(linkDetail);

        if (gcmMessageTypeRef.equals(WebService.notificationEventDetail) || gcmMessageTypeRef.equals(WebService.notificationNews)) {
            btnReadMore.setVisibility(View.VISIBLE);
            getGCMMessage();
        }

        btnReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contentRef != null) {
                    if (gcmMessageTypeRef.equals(WebService.notificationEventDetail)) {
                        Intent intentEventDetail = new Intent(NotificationDetailActivity.this, EventDetailActivity.class);
                        intentEventDetail.putExtra(CONTENT_REF, contentRef);
                        intentEventDetail.putExtra(SCHADULE, schedule);
                        intentEventDetail.putExtra(TITLE, titleEvent);
                        intentEventDetail.putExtra(LINK_DETAIL, linkDetail);
                        startActivity(intentEventDetail);
                    } else if (gcmMessageTypeRef.equals(WebService.notificationNews)) {
                        Intent intentNewsDetail = new Intent(NotificationDetailActivity.this, NewsDetailActivity.class);
                        intentNewsDetail.putExtra(CONTENT_REF, contentRef);
                        intentNewsDetail.putExtra(TITLE, titleEvent);
                        intentNewsDetail.putExtra(LINK_DETAIL, linkDetail);
                        startActivity(intentNewsDetail);
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(linearLayout, "Data tidak ditemukan!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        });

    }

    public void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_notification));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtTitle = (TextView) findViewById(R.id.txt_title);
        txtDate = (TextView) findViewById(R.id.txt_date);
        webView = (WebView) findViewById(R.id.web_content);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        btnReadMore = (Button) findViewById(R.id.btn_read_more);
        btnReadMore.setTypeface(font);
    }

    private void getGCMMessage() {
        presenter.getGCMMessageSvc(gcmMessageRef, memberRef);
    }


    public void showListNotificationDetailResults(Response<NotificationDetailModel> response) {
        contentRef = response.body().getContentRef();
        schedule = response.body().getSchedule();
        titleEvent = response.body().getTitleEvent();
        linkDetail = response.body().getLinkDetail();
    }

    public void showListNotificationDetailFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NotificationDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
