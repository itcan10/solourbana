package com.nataproperty.solourbana.view.mybooking.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by UserModel on 6/14/2016.
 */
public class MyBookingModel {
    String dbMasterRef, projectRef, bookingRef, projectName, clusterName, blockName, unitName, bookingCode, bookingDate, priceIncVAT,
            customerName, isPayment, projectBookingRef, categoryRef, productRef, clusterRef, unitRef, termNo, termRef, clearDate,
            mobile, phone, email, alamat, ktpId, linkPdf, linkDownloadOR,va1, va2;

    public String getVa1() {
        return va1;
    }

    public void setVa1(String va1) {
        this.va1 = va1;
    }

    public String getVa2() {
        return va2;
    }

    public void setVa2(String va2) {
        this.va2 = va2;
    }

    @SerializedName("memberCustomerRef")
    String memberCostumerRef;
    @SerializedName("linkDownload")
    String linkDownload;
    @SerializedName("linkOR")
    String linkOR;


    public String getLinkOR() {
        return linkOR;
    }

    public void setLinkOR(String linkOR) {
        this.linkOR = linkOR;
    }

    public String getLinkDownloadOR() {
        return linkDownloadOR;
    }

    public void setLinkDownloadOR(String linkDownloadOR) {
        this.linkDownloadOR = linkDownloadOR;
    }

    public String getLinkPdf() {
        return linkPdf;
    }

    public void setLinkPdf(String linkPdf) {
        this.linkPdf = linkPdf;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getUnitRef() {
        return unitRef;
    }

    public void setUnitRef(String unitRef) {
        this.unitRef = unitRef;
    }

    public String getTermNo() {
        return termNo;
    }

    public void setTermNo(String termNo) {
        this.termNo = termNo;
    }

    public String getTermRef() {
        return termRef;
    }

    public void setTermRef(String termRef) {
        this.termRef = termRef;
    }

    public String getProjectBookingRef() {
        return projectBookingRef;
    }

    public void setProjectBookingRef(String projectBookingRef) {
        this.projectBookingRef = projectBookingRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getBookingRef() {
        return bookingRef;
    }

    public void setBookingRef(String bookingRef) {
        this.bookingRef = bookingRef;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getPriceIncVAT() {
        return priceIncVAT;
    }

    public void setPriceIncVAT(String priceIncVAT) {
        this.priceIncVAT = priceIncVAT;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getIsPayment() {
        return isPayment;
    }

    public void setIsPayment(String isPayment) {
        this.isPayment = isPayment;
    }

    public String getClearDate() {
        return clearDate;
    }

    public void setClearDate(String clearDate) {
        this.clearDate = clearDate;
    }


    public String getLinkDownload() {
        return linkDownload;
    }

    public void setLinkDownload(String linkDownload) {
        this.linkDownload = linkDownload;
    }

    public String getMemberCostumerRef() {
        return memberCostumerRef;
    }

    public void setMemberCostumerRef(String memberCostumerRef) {
        this.memberCostumerRef = memberCostumerRef;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getKtpId() {
        return ktpId;
    }

    public void setKtpId(String ktpId) {
        this.ktpId = ktpId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
