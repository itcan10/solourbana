package com.nataproperty.solourbana.view.booking.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.booking.adapter.BookingPaymentMethodAdapter;
import com.nataproperty.solourbana.view.booking.model.BookingPaymentMethodModel;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.booking.model.PaymentModel;
import com.nataproperty.solourbana.view.booking.model.VeritransBookingVTModel;
import com.nataproperty.solourbana.view.booking.presenter.BookingPaymentMethodPresenter;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationProductActivity;
import com.nataproperty.solourbana.view.mybooking.ui.MyBookingActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by UserModel on 5/21/2016.
 */
public class BookingPaymentMethodActivity extends AppCompatActivity {
    public static final String TAG = "BookingPaymentMethod";

    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String TYPE_PAYMENT = "typePayment";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";
    private static final String EXTRA_RX = "EXTRA_RX";

    BookingPaymentMethodPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    ImageView imgLogo;
    TextView txtProjectName;

    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    private TextView txtBookingFee, txtBookingInfo;

    private String propertys, category, product, unit, area, priceInc;
    private String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    private String dbMasterRef, categoryRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo, bookingRef, projectName;
    private String bookingFee, projectBookingRef, memberRef, isOnline, paymentType, paymentMethod;

    int typePayment;

    SharedPreferences sharedPreferences;
    Toolbar toolbar;
    TextView title;
    Typeface font;
    RelativeLayout rPage;
    Display display;

    MyListView listView;
    BookingPaymentMethodAdapter bookingPaymentMethodAdapter;
    List<BookingPaymentMethodModel> listBookingPaymentMethod = new ArrayList<>();
    ProgressDialog progressDialog;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_payment_method);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new BookingPaymentMethodPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        typePayment = intent.getIntExtra(TYPE_PAYMENT, 0);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        initWidget();

        Log.d(TAG, bookingRef + "-" + dbMasterRef + "-" + projectRef + "-" + clusterRef + "-" + productRef + "-" +
                unitRef + "-" + termRef + "-" + termNo + "-" + termNo);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isOnline = listBookingPaymentMethod.get(position).getIsOnline();
                paymentType = listBookingPaymentMethod.get(position).getPaymentType();
                paymentMethod = listBookingPaymentMethod.get(position).getPaymentMethod();

                Log.d(TAG, "isOnline " + isOnline + "");

                if (isOnline.equals("0")) {
                    Intent intent = new Intent(BookingPaymentMethodActivity.this, BookingBankTransferActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(CLUSTER_REF, clusterRef);
                    intent.putExtra(PRODUCT_REF, productRef);
                    intent.putExtra(UNIT_REF, unitRef);
                    intent.putExtra(TERM_REF, termRef);
                    intent.putExtra(TERM_NO, termNo);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(BOOKING_REF, bookingRef);
                    intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                    intent.putExtra("typePayment", typePayment);
                    startActivity(intent);
                } else {
                    paymentVeritrans(paymentType, paymentMethod);
                }
            }
        });

        requestPaymentMethod();
        requestPayment();
        requestMyBookingDetail();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_payment_method));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //logo
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * booking fee
         */
        txtBookingFee = (TextView) findViewById(R.id.txt_booking_fee);
        txtBookingInfo = (TextView) findViewById(R.id.txt_booking_info);

        listView = (MyListView) findViewById(R.id.list_booking_payment);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

    }

    private void requestPaymentMethod() {
        presenter.GetPaymentMethodSvc(dbMasterRef, projectRef);
    }

    public void showListBookingPaymentMethodResults(Response<List<BookingPaymentMethodModel>> response) {
        listBookingPaymentMethod = response.body();
        initBookingPaymentMethodAdapter();
    }

    public void showListBookingPaymentMethodFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initBookingPaymentMethodAdapter() {
        bookingPaymentMethodAdapter = new BookingPaymentMethodAdapter(this, listBookingPaymentMethod);
        listView.setAdapter(bookingPaymentMethodAdapter);
        listView.setExpanded(true);
    }

    private void requestPayment() {
        presenter.getIlustrationPaymentSvc(dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo);
    }

    public void showPaymentModelResults(Response<PaymentModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();

        if (status == 200) {
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();

            propertys = response.body().getPropertyInfo().getPropertys();
            category = response.body().getPropertyInfo().getCategory();
            product = response.body().getPropertyInfo().getProduct();
            unit = response.body().getPropertyInfo().getUnit();
            area = response.body().getPropertyInfo().getArea();
            priceInc = response.body().getPropertyInfo().getPriceInc();
            bookingFee = response.body().getPropertyInfo().getBookingFee();

            txtPropertyName.setText(propertys);
            txtCategoryType.setText(category);
            txtProduct.setText(product);
            txtUnitNo.setText(unit);
            txtArea.setText(Html.fromHtml(area));
            txtEstimate.setText(priceInc);

            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice);

            DecimalFormat decimalFormat = new DecimalFormat("###,##0");
            txtBookingFee.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(bookingFee))));

            imgLogo = (ImageView) findViewById(R.id.img_logo_project);
            txtProjectName = (TextView) findViewById(R.id.txt_project_name);
            txtProjectName.setText(propertys);
            Glide.with(this)
                    .load(WebService.getProjectImage() + dbMasterRef + "&pr=" + projectRef).into(imgLogo);

        } else {

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showPaymentModelFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void requestMyBookingDetail() {
        presenter.getBookingDetailSvc(dbMasterRef, projectRef, projectBookingRef);
    }

    public void showBookingInfoResults(Response<DetailBooking> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            String bookDate = response.body().getBookingInfo().getBookDate();
            String bookHour = response.body().getBookingInfo().getBookHour();

            txtBookingInfo.setText("Lakukan pembayaran booking fee dalam waktu 3 jam atau sebelum pukul " + bookHour + ". " +
                    "jika tidak ada konfirmasi pembayaran maka unit booking akan kembali available");
        } else {
            Log.d(TAG, "get error");
        }
    }

    public void showBookingInfoFailure(Throwable t) {

    }

    private void paymentVeritrans(String paymentType, String paymentMethod) {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        String dbMasterRefProjectCode = dbMasterRef + "#" + General.projectCode;
        presenter.veritransBookingVTSvc(dbMasterRefProjectCode, projectRef, memberRef, bookingRef, termRef, paymentType, paymentMethod);
    }

    public void showVeritransBookingVTResults(Response<VeritransBookingVTModel> response, String paymentType) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        String link = response.body().getRedirectUrl();

        Log.d(TAG, "showVeritransBookingVTResults " + status);

        if (status == 200) {
            Intent intent = new Intent(BookingPaymentMethodActivity.this, BookingCreditCardWebviewActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(CLUSTER_REF, clusterRef);
            intent.putExtra(PRODUCT_REF, productRef);
            intent.putExtra(UNIT_REF, unitRef);
            intent.putExtra(TERM_REF, termRef);
            intent.putExtra(TERM_NO, termNo);
            intent.putExtra(BOOKING_REF, bookingRef);
            intent.putExtra(PROJECT_NAME, projectName);
            intent.putExtra("link", link);
            intent.putExtra("titlePayment", paymentType);
            intent.putExtra("typePayment", typePayment);
            startActivity(intent);
        } else if (status == 406) {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showVeritransBookingVTFailure(Throwable t) {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (typePayment == 0) {
            Intent intent = new Intent(BookingPaymentMethodActivity.this, IlustrationProductActivity.class);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
            intent.putExtra(CATEGORY_REF, categoryRef);
            intent.putExtra(CLUSTER_REF, clusterRef);
            intent.putExtra(PROJECT_NAME, projectName);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(BookingPaymentMethodActivity.this, MyBookingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }

}
