package com.nataproperty.solourbana.view.mybooking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyBookingDetailInteractor {

    void getListPaymentScheduleSvc(String dbMasterRef, String projectRef, String bookingRef);

    void getBookingDetailSvc(String dbMasterRef, String projectRef, String bookingRef);

}
