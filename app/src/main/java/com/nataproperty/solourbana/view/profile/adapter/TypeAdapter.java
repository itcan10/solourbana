package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.TypeModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class TypeAdapter extends BaseAdapter {
    private Context context;
    private List<TypeModel> list;
    private ListTypeHolder holder;

    public TypeAdapter(Context context, List<TypeModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_type,null);
            holder = new ListTypeHolder();
            holder.typeName = (TextView) convertView.findViewById(R.id.txt_type);

            convertView.setTag(holder);
        }else{
            holder = (ListTypeHolder) convertView.getTag();
        }
        TypeModel nation = list.get(position);
        holder.typeName.setText(nation.getTypeName());

        return convertView;
    }

    private class ListTypeHolder {
        TextView typeName;
    }
}
