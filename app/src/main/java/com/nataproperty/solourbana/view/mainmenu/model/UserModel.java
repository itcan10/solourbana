package com.nataproperty.solourbana.view.mainmenu.model;

/**
 * Created by Nata
 * on Mar 3/20/2017 15:09.
 * Project:wika
 */

public class UserModel {
    private static UserModel ourInstance = new UserModel();
    private String userRef;

    public static UserModel getInstance() {
        return ourInstance;
    }

    private UserModel() {
    }

    public static UserModel getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(UserModel ourInstance) {
        UserModel.ourInstance = ourInstance;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }
}
