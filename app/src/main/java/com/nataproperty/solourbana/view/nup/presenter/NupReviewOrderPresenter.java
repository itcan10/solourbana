package com.nataproperty.solourbana.view.nup.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.nup.intractor.NupReviewOrderInteractor;
import com.nataproperty.solourbana.view.nup.model.JsonInCustSaveOrderNupModel;
import com.nataproperty.solourbana.view.nup.model.JsonInSaveOrderNupModel;
import com.nataproperty.solourbana.view.nup.model.ResponeModelOrderNup;
import com.nataproperty.solourbana.view.nup.ui.NupReviewOrderActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NupReviewOrderPresenter implements NupReviewOrderInteractor {

    private NupReviewOrderActivity view;
    private ServiceRetrofit service;

    public NupReviewOrderPresenter(NupReviewOrderActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }
    @Override
    public void saveOrderNup_v2(JsonInSaveOrderNupModel jsonInSaveOrderNupModel, JsonInCustSaveOrderNupModel jsonInCustSaveOrderNupModel) {
        Call<ResponeModelOrderNup> call = service.getAPI().saveOrderNup_v2(Utils.modelToJson(jsonInSaveOrderNupModel), Utils.modelToJson(jsonInCustSaveOrderNupModel));
        call.enqueue(new Callback<ResponeModelOrderNup>() {
            @Override
            public void onResponse(Call<ResponeModelOrderNup> call, Response<ResponeModelOrderNup> response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponeModelOrderNup> call, Throwable t) {
                view.showResponeFailure(t);
            }
        });
    }
}
