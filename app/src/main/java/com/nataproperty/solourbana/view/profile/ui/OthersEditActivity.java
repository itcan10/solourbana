package com.nataproperty.solourbana.view.profile.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.profile.adapter.JobTitleAdapter;
import com.nataproperty.solourbana.view.profile.adapter.MaritalStatusAdapter;
import com.nataproperty.solourbana.view.profile.adapter.NationAdapter;
import com.nataproperty.solourbana.view.profile.adapter.OccupationAdapter;
import com.nataproperty.solourbana.view.profile.adapter.RegionAdapter;
import com.nataproperty.solourbana.view.profile.adapter.SexAdapter;
import com.nataproperty.solourbana.view.profile.adapter.TypeAdapter;
import com.nataproperty.solourbana.view.profile.model.JobTitleModel;
import com.nataproperty.solourbana.view.profile.model.MaritalStatusModel;
import com.nataproperty.solourbana.view.profile.model.NationModel;
import com.nataproperty.solourbana.view.profile.model.OccupationModel;
import com.nataproperty.solourbana.view.profile.model.OthersEditModel;
import com.nataproperty.solourbana.view.profile.model.RegionModel;
import com.nataproperty.solourbana.view.profile.model.SexModel;
import com.nataproperty.solourbana.view.profile.model.TypeModel;
import com.nataproperty.solourbana.view.profile.presenter.OthersEditPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by UserModel on 5/15/2016.
 */
public class OthersEditActivity extends AppCompatActivity {
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private OthersEditPresenter presenter;
    private List<NationModel> listNation = new ArrayList<NationModel>();
    private List<TypeModel> listType = new ArrayList<TypeModel>();
    private List<RegionModel> listRegion = new ArrayList<RegionModel>();
    private List<SexModel> listSex = new ArrayList<SexModel>();
    private List<OccupationModel> listOccupation = new ArrayList<OccupationModel>();
    private List<JobTitleModel> listJobTitle = new ArrayList<JobTitleModel>();
    private List<MaritalStatusModel> listMaritalStatus = new ArrayList<MaritalStatusModel>();
    private NationAdapter adapterNation;
    private TypeAdapter adapterType;
    private RegionAdapter adapterRegion;
    private SexAdapter adapterSex;
    private OccupationAdapter adapterOccupation;
    private JobTitleAdapter adapterJobTitle;
    private MaritalStatusAdapter adapterMaritalStatus;
    private EditText editBloodType, editFax, editPassportId, editSimId;
    private Spinner spnNation, spnType, spnRegion, spnSex, spnOccupation, spnJobTitle, spnMaritalStatus;
    private Button btnSave;
    String bloodType, fax, passportID, simid;
    String nationSortNo, typeSortNo, regionSortNo, sexSortNo, occupationSortNo, jobTitleSortNo, maritalStatusSortNo;
    String memberRef;
    String nationRef, typeRef, religionRef, sexRef, occupationRef, jobTitleRef, maritalStatus;
    Toolbar toolbar;
    TextView title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_others);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new OthersEditPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        initToolbar();

        editBloodType = (EditText) findViewById(R.id.edit_bloodType);
        editFax = (EditText) findViewById(R.id.edit_fax);
        editPassportId = (EditText) findViewById(R.id.edit_passportId);
        editSimId = (EditText) findViewById(R.id.edit_simId);

        spnNation = (Spinner) findViewById(R.id.spn_nation);
        spnType = (Spinner) findViewById(R.id.spn_type);
        spnRegion = (Spinner) findViewById(R.id.spn_region);
        spnSex = (Spinner) findViewById(R.id.spn_sex);
        spnOccupation = (Spinner) findViewById(R.id.spn_occupation);
        spnJobTitle = (Spinner) findViewById(R.id.spn_jobTitle);
        spnMaritalStatus = (Spinner) findViewById(R.id.spn_maritalStatus);

        Intent intent = getIntent();
        memberRef = intent.getStringExtra("memberRef");
        bloodType = intent.getStringExtra("bloodType");
        fax = intent.getStringExtra("fax");
        passportID = intent.getStringExtra("passportID");
        simid = intent.getStringExtra("simid");

        nationSortNo = intent.getStringExtra("nationSortNo");
        typeSortNo = intent.getStringExtra("typeSortNo");
        regionSortNo = intent.getStringExtra("regionSortNo");
        sexSortNo = intent.getStringExtra("sexSortNo");
        occupationSortNo = intent.getStringExtra("occupationSortNo");
        jobTitleSortNo = intent.getStringExtra("jobTitleSortNo");
        maritalStatusSortNo = intent.getStringExtra("maritalStatusSortNo");

        requestOther();

        editBloodType.setText(bloodType);
        editFax.setText(fax);
        editPassportId.setText(passportID);
        editSimId.setText(simid);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setTypeface(font);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateOther();
            }
        });
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_other));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void requestOther() {
        presenter.getOtherProfileSvc();
    }

    public void showListResults(Response<OthersEditModel> response) {
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            if (status == 200) {
                listNation = response.body().getNation();
                listType = response.body().getType();
                listRegion = response.body().getReligion();
                listSex = response.body().getSex();
                listOccupation = response.body().getOccupation();
                listJobTitle = response.body().getJobTitle();
                listMaritalStatus = response.body().getMarital();
                initAdapter();
            }
        }
    }

    public void showListFailure(Throwable t) {

    }

    private void initAdapter() {
        adapterNation = new NationAdapter(this, listNation);
        spnNation.setAdapter(adapterNation);
        spnNation.setSelection(Integer.parseInt(nationSortNo) - 1);
        spnNation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nationRef = listNation.get(position).getNationRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterType = new TypeAdapter(this, listType);
        spnType.setAdapter(adapterType);
        spnType.setSelection(Integer.parseInt(typeSortNo) - 1);
        spnType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeRef = listType.get(position).getTypeRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterRegion = new RegionAdapter(this, listRegion);
        spnRegion.setAdapter(adapterRegion);
        spnRegion.setSelection(Integer.parseInt(regionSortNo) - 1);
        spnRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                religionRef = listRegion.get(position).getRegionRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterSex = new SexAdapter(this, listSex);
        spnSex.setAdapter(adapterSex);
        spnSex.setSelection(Integer.parseInt(sexSortNo) - 1);
        spnSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sexRef = listSex.get(position).getSexRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterOccupation = new OccupationAdapter(this, listOccupation);
        spnOccupation.setAdapter(adapterOccupation);
        spnOccupation.setSelection(Integer.parseInt(occupationSortNo) - 1);
        spnOccupation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                occupationRef = listOccupation.get(position).getOccupationRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Log.d("JobSortNo", " " + jobTitleSortNo);

        adapterJobTitle = new JobTitleAdapter(this, listJobTitle);
        spnJobTitle.setAdapter(adapterJobTitle);
        spnJobTitle.setSelection(Integer.parseInt(jobTitleSortNo) - 1);
        spnJobTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                jobTitleRef = listJobTitle.get(position).getJobTitleRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterMaritalStatus = new MaritalStatusAdapter(this, listMaritalStatus);
        spnMaritalStatus.setAdapter(adapterMaritalStatus);
        spnMaritalStatus.setSelection(Integer.parseInt(maritalStatusSortNo) - 1);
        spnMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maritalStatus = listMaritalStatus.get(position).getMaritalStatusRef();
                Log.d("Marital", " " + maritalStatus);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateOther() {
        String bloodType = editBloodType.getText().toString();
        String passportID = editPassportId.getText().toString();
        String SIMID = editSimId.getText().toString();
        String fax = editFax.getText().toString();

        Map<String, String> fields = new HashMap<>();
        fields.put("memberRef", memberRef);
        fields.put("bloodType", bloodType);
        fields.put("nationRef", nationRef);
        fields.put("fax", fax);
        fields.put("passportID", passportID);
        fields.put("typeRef", typeRef);
        fields.put("religionRef", religionRef);
        fields.put("sexRef", sexRef);
        fields.put("occupationRef", occupationRef);
        fields.put("jobTitleRef", jobTitleRef);
        fields.put("maritalStatus", maritalStatus);
        fields.put("SIMID", SIMID);
        presenter.updateOtherProfileSvc(fields);
    }

    public void showResponeResults(Response<ResponeModel> response) {
        if (response.isSuccessful()){
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            if (status == 200) {
                finish();
                Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showResponeFailure(Throwable t) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(OthersEditActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
