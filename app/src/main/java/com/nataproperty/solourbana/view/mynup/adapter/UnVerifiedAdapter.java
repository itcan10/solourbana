package com.nataproperty.solourbana.view.mynup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mynup.model.UnVerifiedModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class UnVerifiedAdapter extends BaseAdapter {
    private Context context;
    private List<UnVerifiedModel> list;
    private ListUnPaidHolder holder;

    public UnVerifiedAdapter(Context context, List<UnVerifiedModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_paid,null);
            holder = new ListUnPaidHolder();
            holder.orderCode = (TextView) convertView.findViewById(R.id.txt_nup_code);
            holder.qty = (TextView) convertView.findViewById(R.id.txt_qty);
            holder.totalAmt = (TextView) convertView.findViewById(R.id.txt_total_amt);
            holder.projectName = (TextView) convertView.findViewById(R.id.txt_project_name);
            holder.ppNup = (TextView) convertView.findViewById(R.id.txt_pp_nup);
            holder.ppName = (TextView) convertView.findViewById(R.id.txt_pp_name);

            convertView.setTag(holder);
        }else{
            holder = (ListUnPaidHolder) convertView.getTag();
        }

        UnVerifiedModel unVerifiedModel = list.get(position);

        holder.orderCode.setText(unVerifiedModel.getNupOrderCode());
        holder.qty.setText(unVerifiedModel.getQty());
        holder.totalAmt.setText(unVerifiedModel.getTotalAmt());
        holder.projectName.setText(unVerifiedModel.getProjectName());
        //holder.ppNup.setText(unVerifiedModel.getNupNo());
        holder.ppName.setText(unVerifiedModel.getCustomerName());

        if (unVerifiedModel.getNupNo()==null){
            holder.ppNup.setText("-");
        }else {
            holder.ppNup.setText(unVerifiedModel.getNupNo());
        }

        return convertView;
    }

    private class ListUnPaidHolder {
        TextView orderCode,qty,totalAmt,projectName,ppNup,ppName;
    }
}
