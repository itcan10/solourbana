package com.nataproperty.solourbana.view.mybooking.ui;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class MyBookingViewPDFActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = "MyBookingViewPDF";
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    FloatingActionButton fab;
    WebView webView;
    String link = "www.nataproperty.com";
    ProgressDialog mProgressDialog;
    String linkDownload, linkPdf, bookingCode, linkOR, linkDownloadOR;
    private ProgressBar progressBar;
    Toolbar toolbar;
    TextView title;
    Typeface font, fontLight;
    boolean downloadSP;
    File file, fileOpen;
    OutputStream output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_view_pdf);
        initWidget();
        Intent intent = getIntent();
        linkDownload = intent.getStringExtra("linkDownload");
        linkPdf = intent.getStringExtra("linkPdf");
        bookingCode = (intent.getStringExtra("bookingCode")).replace("/", "-");
        linkOR = intent.getStringExtra("linkOR");
        linkDownloadOR = intent.getStringExtra("linkDownloadOR");
        downloadSP = intent.getBooleanExtra("downloadSP", false);

        Log.d(TAG, "sp " + linkDownload + " " + linkPdf);
        Log.d(TAG, "or " + linkDownloadOR + " " + linkOR);

        if (downloadSP) {
            initWebview(linkDownload);
            title.setText("Surat Pesanan");
        } else {
            initWebview(linkDownloadOR);
            title.setText("Official Receipt");
        }

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        progressBar = (ProgressBar) findViewById(R.id.progress1);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        webView = (WebView) findViewById(R.id.web_content);
        fab.setOnClickListener(this);
    }

    private void initWebview(String linkContent) {
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                    fab.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.GONE);
                }
            }
        });
        webView.loadUrl(linkContent);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                if (checkWriteExternalPermission()) {
                    if (downloadSP) {
                        startDownload(linkPdf);
                    } else {
                        startDownload(linkOR);
                    }

                } else {
                    showDialogCekPermission();
                }
                break;
        }
    }


    public void showDialogCekPermission() {
        if (ContextCompat.checkSelfPermission(MyBookingViewPDFActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MyBookingViewPDFActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(MyBookingViewPDFActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
        }
    }

    private void startDownload(String url) {
        if (url != null) {
            new DownloadFileAsync().execute(url);
        } else {
            Toast.makeText(MyBookingViewPDFActivity.this, "File download tidak tersedia", Toast.LENGTH_LONG).show();
        }

    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermissionAllow();
            } else {
                proceedAfterPermissionDeny();
            }
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Downloading file..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            int count;

            try {

                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();

                if (downloadSP) {
                    file = new File(wallpaperDirectory, "SP-" + bookingCode + ".pdf");
                } else {
                    file = new File(wallpaperDirectory, "OR-" + bookingCode + ".pdf");
                }

                if (file.exists())
                    file.delete();

                InputStream input = new BufferedInputStream(url.openStream());
                if (downloadSP) {
                    output = new FileOutputStream(wallpaperDirectory + "/" + "SP-" + bookingCode + ".pdf");
                } else {
                    output = new FileOutputStream(wallpaperDirectory + "/" + "OR-" + bookingCode + ".pdf");
                }

                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
                notification();
            } catch (Exception e) {
            }
            return null;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.d("ANDRO_ASYNC", values[0]);
            mProgressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

    }

    public void notification() {
        try {
            if (downloadSP) {
                fileOpen = new File("/" + Environment.getExternalStorageDirectory().getPath()
                        + "/nataproperty/" + "SP-" + bookingCode + ".pdf");
            } else {
                fileOpen = new File("/" + Environment.getExternalStorageDirectory().getPath()
                        + "/nataproperty/" + "OR-" + bookingCode + ".pdf");
            }

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(fileOpen), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        } catch (Exception e) {
            Snackbar.make(webView, "Your don't have any PDF reader", Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
