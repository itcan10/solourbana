package com.nataproperty.solourbana.view.booking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterBookingInformasiCustomerInteractor {

    void getMemberInfoSvc(String memberRef);

}
