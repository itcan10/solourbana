package com.nataproperty.solourbana.view.ilustration.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.MyGridView;
import com.nataproperty.solourbana.view.ilustration.model.UnitMappingModel;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationPaymentTermActivity;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class UnitMappingAdapter extends BaseAdapter {
    private Context context;

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String BLOCK_NAME = "blockName";
    public static final String UNIT_REF = "unitRef";
    public static final String PRODUCT_REF = "productRef";

    public static final String IS_BOOKING = "isBooking";

    private List<UnitMappingModel> list;
    private ListUnitMappingHolder holder;

    ProgressDialog progressDialog;
    MyGridView listUnit;

    LinearLayout linear;

    public UnitMappingAdapter(Context context, List<UnitMappingModel> list, ProgressDialog progressDialog, MyGridView listUnit) {
        this.context = context;
        this.list = list;
        this.progressDialog = progressDialog;
        this.listUnit = listUnit;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_unit_mapping,null);
            holder = new ListUnitMappingHolder();
            holder.unitName = (TextView) convertView.findViewById(R.id.txt_unit_name);

            //my coding
            String unitStatus = list.get(position).getUnitStatus();
            String unitName = list.get(position).getUnitName();

            linear = (LinearLayout) convertView.findViewById(R.id.linear);
            linear.addView(componentText(position, unitStatus, unitName, list));

            /*holder.unitName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onToast(v, holder.unitName.getText().toString());
                }
            });*/

            convertView.setTag(holder);
        }else{
            holder = (ListUnitMappingHolder) convertView.getTag();
        }
        final UnitMappingModel unit = list.get(position);
        holder.unitName.setText(unit.getUnitName());

        /*listUnit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(context, list.get(position).getUnitName(), Toast.LENGTH_SHORT).show();
            }
        });*/

        Log.d("Cek GetView unit",""+unit.getUnitName());

        return convertView;
    }

    TextView componentText(int position, final String unitStatus,final String unitName, List<UnitMappingModel> list){

        final String productRef = list.get(position).getProductRef();
        final String unitRef = list.get(position).getUnitRef();
        final String dbMasterRef = list.get(position).getDbMasterRef();
        final String projectRef = list.get(position).getProjectRef();
        final String clusterRef = list.get(position).getClusterRef();
        final String categoryRef = list.get(position).getCategoryRef();
        final String isBooking = list.get(position).getIsBooking();
        final String projectName = list.get(position).getProjectName();

        TextView componentTextView = new TextView(this.context);
        componentTextView.setText(unitName);
        componentTextView.setId(position);

        componentTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unitStatus.equals("A")) {
                    Toast.makeText(context, unitName+"-"+unitStatus, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, IlustrationPaymentTermActivity.class);
                    intent.putExtra(DBMASTER_REF,dbMasterRef);
                    intent.putExtra(PROJECT_REF,projectRef);
                    intent.putExtra(CLUSTER_REF,clusterRef);
                    intent.putExtra(CATEGORY_REF,categoryRef);
                    intent.putExtra(PROJECT_NAME,projectName);
                    intent.putExtra(PRODUCT_REF,productRef);
                    intent.putExtra(UNIT_REF,unitRef);

                    intent.putExtra(IS_BOOKING,isBooking);
                    context.startActivity(intent);
                }else{
                    Toast.makeText(context, unitName+"-"+unitStatus, Toast.LENGTH_SHORT).show();
                }

            }
        });

        return componentTextView;
    }

    public void onToast(View v, String abc){
        Toast.makeText(context, ""+abc, Toast.LENGTH_SHORT).show();
    }

    private class ListUnitMappingHolder {
        TextView unitName;
    }
}
