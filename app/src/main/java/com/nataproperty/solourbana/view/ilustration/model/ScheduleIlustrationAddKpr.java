package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by nata on 12/1/2016.
 */
public class ScheduleIlustrationAddKpr {
    String schedule;
    String dueDate;
    String amount;

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
