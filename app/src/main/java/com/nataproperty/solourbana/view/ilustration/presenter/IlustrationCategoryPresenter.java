package com.nataproperty.solourbana.view.ilustration.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.ilustration.intractor.PresenterIlustrationCategoryInteractor;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationCategoryActivity;
import com.nataproperty.solourbana.view.project.model.CategoryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationCategoryPresenter implements PresenterIlustrationCategoryInteractor {
    private IlustrationCategoryActivity view;
    private ServiceRetrofit service;

    public IlustrationCategoryPresenter(IlustrationCategoryActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListCategory(String dbMasterRef, String projectRef) {
        Call<List<CategoryModel>> call = service.getAPI().getIlusCategory(dbMasterRef,projectRef);
        call.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
                view.showListCategoryResults(response);
            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable t) {
                view.showListCategoryFailure(t);

            }


        });

    }

}
