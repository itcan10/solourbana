package com.nataproperty.solourbana.view.menuitem.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.menuitem.model.NotificationModel;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class NotificationAdapter extends BaseAdapter  {
    public static final String TAG = "NotificationAdapter" ;

    public static final String PREF_NAME = "pref" ;

    private Context context;
    private ArrayList<NotificationModel> list;

    private ListNotificationHolder holder;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> list) {
        this.context = context;
        this.list = list;

    }

    SharedPreferences sharedPreferences;
    private boolean state;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state =  sharedPreferences.getBoolean("isLogin", false);

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_notification,null);
            holder = new ListNotificationHolder();
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.date = (TextView) convertView.findViewById(R.id.txt_date);

            convertView.setTag(holder);
        }else{
            holder = (ListNotificationHolder) convertView.getTag();
        }

        NotificationModel notificationModel = list.get(position);
        holder.title.setText(notificationModel.getGcmTitle());
        holder.date.setText(notificationModel.getInputTime());

        return convertView;
    }


    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListNotificationHolder {
        TextView title,date;
    }

}
