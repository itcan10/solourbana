package com.nataproperty.solourbana.view.ilustration.presenter;


import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.ilustration.intractor.PresenterFloorMappingInteractor;
import com.nataproperty.solourbana.view.ilustration.model.ListBlockDiagramModel;
import com.nataproperty.solourbana.view.ilustration.ui.FloorMappingActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class FloorMappingPresenter implements PresenterFloorMappingInteractor {
    private FloorMappingActivity view;
    private ServiceRetrofit service;

    public FloorMappingPresenter(FloorMappingActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getBlockMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef, String productRefParam, String memberRef) {
        Call<List<ListBlockDiagramModel>> call = service.getAPI().getListBlockProductColorSvc(dbMasterRef, projectRef, categoryRef, clusterRef, productRefParam, memberRef);
        call.enqueue(new Callback<List<ListBlockDiagramModel>>() {
            @Override
            public void onResponse(Call<List<ListBlockDiagramModel>> call, Response<List<ListBlockDiagramModel>> response) {
                view.showBlockMappingResults(response);
            }

            @Override
            public void onFailure(Call<List<ListBlockDiagramModel>> call, Throwable t) {
                view.showBlockMappingFailure(t);

            }


        });
    }
//    @Override
//    public void getListBlok(String dbMasterRef, String projectRef, String categoryRef, String clusterRef) {
//        Call<List<BlockMappingModel>> call = service.getAPI().getListBlok(dbMasterRef,projectRef,categoryRef,clusterRef);
//        call.enqueue(new Callback<List<BlockMappingModel>>() {
//            @Override
//            public void onResponse(Call<List<BlockMappingModel>> call, Response<List<BlockMappingModel>> response) {
//                view.showListBlokResults(response);
//            }
//
//            @Override
//            public void onFailure(Call<List<BlockMappingModel>> call, Throwable t) {
//                view.showListBlokFailure(t);
//
//            }
//
//
//        });
//
//    }

}
