package com.nataproperty.solourbana.view.booking.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jaredrummler.android.device.DeviceName;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.booking.model.MemberInfoModel;
import com.nataproperty.solourbana.view.booking.presenter.BookingInformasiCustomerPresenter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Response;

/**
 * Created by UserModel on 5/18/2016.
 */
public class BookingInformasiCustomerActivity extends AppCompatActivity {
    public static final String TAG = "BookingInformasi";
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PROJECT_NAME = "projectName";
    public static final String MEMBER_CUSTOMER_REF = "memberCustomerRef";
    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";
    private static final String EXTRA_RX = "EXTRA_RX";
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    BookingInformasiCustomerPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;
    SharedPreferences sharedPreferences;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtPriortyPrice, txtTotal;
    EditText editFullname, editEmail, editMobile, editPhone, editAddress, editKtpId, editQty, editNpwp;
    ImageView imgKtp, imgNpwp;

    Button btnUploadKtp, btnUploadNpwp, btnNext;

    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address;
    private String dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo, projectName;
    private String memberCustomerRef, projectDescription, status,va1, va2;
    private String type, manufacturer, mTmpGalleryPicturePath;
    private String ktpImg, npwpImg;
    String defaultQty = "1";
    double gTotal;
    private String cekKtp = "";
    private String cekNpwp = "";
    Bitmap bitmapKtp, bitmapNpwp;
    Toolbar toolbar;
    TextView title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    ProgressDialog progressDialog;
    LinearLayout linearLayout;
    Uri file, uri , selectedImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_informasi_customer);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new BookingInformasiCustomerPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCustomerRef = sharedPreferences.getString("isMemberCostumerRef", null);

        final Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");

        phone1 = intent.getStringExtra(PHONE);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);

        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);

        initWidget();

        Log.d("intent get", " " + dbMasterRef + "-" + projectRef + "-" + memberCustomerRef + "-" + projectDescription);

        if (phone1 != null && address != null && ktpid != null) {
            editPhone = (EditText) findViewById(R.id.edit_phone);
            editAddress = (EditText) findViewById(R.id.edit_address);
            editKtpId = (EditText) findViewById(R.id.edit_ktp_id);

            editPhone.setText(phone1);
            editAddress.setText(address);
            editKtpId.setText(ktpid);
        }

        imgKtp.setEnabled(false);
        imgNpwp.setEnabled(false);

        imgKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                selectImage();
            }
        });

        imgNpwp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "3";
                selectImage();
            }
        });

        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekFullname = editFullname.getText().toString();
                String cekPhone = editPhone.getText().toString();
                String cekMobile = editMobile.getText().toString();
                String cekEmail = editEmail.getText().toString();
                String cekAddress = editAddress.getText().toString();
                String cekKtpId = editKtpId.getText().toString();

                if (!cekFullname.isEmpty() && !cekPhone.isEmpty() && !cekMobile.isEmpty() && !cekEmail.isEmpty() && !cekAddress.isEmpty()
                        && !cekKtpId.isEmpty() && !ktpRef.equals("") && !npwpRef.equals("")) {
                    Intent intent = new Intent(BookingInformasiCustomerActivity.this, BookingInfoActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(CATEGORY_REF, categoryRef);
                    intent.putExtra(CLUSTER_REF, clusterRef);
                    intent.putExtra(PRODUCT_REF, productRef);
                    intent.putExtra(UNIT_REF, unitRef);
                    intent.putExtra(TERM_REF, termRef);
                    intent.putExtra(TERM_NO, termNo);
                    intent.putExtra(PROJECT_NAME, projectName);

                    intent.putExtra("va1", va1);
                    intent.putExtra("va2", va2);

                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    if (!editNpwp.getText().toString().equals("")){
                        intent.putExtra("NPWP", editNpwp.getText().toString());
                    } else {
                        intent.putExtra("NPWP", "");
                    }
                    intent.putExtra(KTP_REF, ktpRef);
                    intent.putExtra(NPWP_REF, npwpRef);
                    startActivity(intent);

                } else {
                    if (cekFullname.isEmpty()) {
                        editFullname.setError(getResources().getString(R.string.txt_no_fullname));
                    } else {
                        editFullname.setError(null);
                    }

                    if (cekPhone.isEmpty()) {
                        editPhone.setError(getResources().getString(R.string.txt_no_phone));
                    } else {
                        editPhone.setError(null);
                    }

                    if (cekMobile.isEmpty()) {
                        editMobile.setError(getResources().getString(R.string.txt_no_mobile));
                    } else {
                        editMobile.setError(null);
                    }

                    if (cekEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                        editEmail.setError(getResources().getString(R.string.txt_no_email));
                    } else {
                        editEmail.setError(null);
                    }

                    if (cekAddress.isEmpty()) {
                        editAddress.setError(getResources().getString(R.string.txt_no_address));
                    } else {
                        editAddress.setError(null);
                    }

                    if (cekKtpId.isEmpty()) {
                        editKtpId.setError(getResources().getString(R.string.txt_no_ktp_id));
                    } else {
                        editKtpId.setError(null);
                    }

                    if (ktpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_ktp)), Toast.LENGTH_LONG).show();
                    } else if (npwpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_npwp)), Toast.LENGTH_LONG).show();
                    }

                }

            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_bookng_informasi));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editMobile = (EditText) findViewById(R.id.edit_mobile);
        editPhone = (EditText) findViewById(R.id.edit_phone);
        editAddress = (EditText) findViewById(R.id.edit_address);
        editKtpId = (EditText) findViewById(R.id.edit_ktp_id);
        editNpwp = (EditText) findViewById(R.id.edit_npwp);

        txtPriortyPrice = (TextView) findViewById(R.id.txt_priority_pass);
        txtTotal = (TextView) findViewById(R.id.txt_total);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnUploadKtp = (Button) findViewById(R.id.btn_upload_ktp);
        btnUploadKtp.setTypeface(font);
        btnUploadNpwp = (Button) findViewById(R.id.btn_upload_npwp);
        btnUploadNpwp.setTypeface(font);

        btnNext = (Button) findViewById(R.id.btn_next);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPsInfo(memberCustomerRef);
    }

    private void requestPsInfo(String memberCustomerRef) {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getMemberInfoSvc(memberCustomerRef);
    }

    public void showMemberInfoResults(Response<MemberInfoModel> response) {
        progressDialog.dismiss();
        int statusGet = response.body().getStatus();
        String message = response.body().getMessage();
        if (statusGet == 200) {
            //reuired
            fullname = response.body().getData().getName();
            ktpid = response.body().getData().getKtpid();
            phone1 = response.body().getData().getPhone1();
            mobile1 = response.body().getData().gethP1();
            email1 = response.body().getData().getEmail1();
            address = response.body().getData().getIdAddr();
            ktpRef = response.body().getData().getKtpRef();
            npwpRef = response.body().getData().getNpwpRef();

            if (!fullname.isEmpty()) {
                editFullname.setText(fullname);
                editFullname.setEnabled(false);
            } else {
                editFullname.setEnabled(true);
            }

            if (!email1.isEmpty()) {
                editEmail.setText(email1);
                editEmail.setEnabled(false);
            } else {
                editEmail.setEnabled(true);
            }

            if (!ktpid.isEmpty()) {
                editKtpId.setText(ktpid);
                editKtpId.setEnabled(false);
            } else {
                editKtpId.setEnabled(true);
            }

            if (!mobile1.isEmpty()) {
                editMobile.setText(mobile1);
                editMobile.setEnabled(false);
            } else {
                editMobile.setEnabled(true);
            }

            if (!phone1.isEmpty()) {
                editPhone.setText(phone1);
                editPhone.setEnabled(false);
            } else {
                editPhone.setEnabled(true);
            }

            if (!address.isEmpty()) {
                editAddress.setText(address);
                editAddress.setEnabled(false);
            } else {
                editAddress.setEnabled(true);
            }

            if (ktpRef.equals("")) {
                imgKtp.setEnabled(true);
            } else {
                imgKtp.setEnabled(false);
            }

            if (npwpRef.equals("")) {
                imgNpwp.setEnabled(true);
            } else {
                imgNpwp.setEnabled(false);
            }

            Glide.with(BookingInformasiCustomerActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgKtp);

            Glide.with(BookingInformasiCustomerActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgNpwp);

            Log.d("KTP/NPWP", " " + WebService.getKtp() + ktpRef + " " + WebService.getNpwp() + npwpRef);
            Log.d("KTP/NPWP", "Ref " + ktpRef + " " + npwpRef);
        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showMemberInfoFailure(Throwable t) {
        progressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(BookingInformasiCustomerActivity.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkCameraPermission()) {
                        ActivityCompat.requestPermissions(BookingInformasiCustomerActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                    } else {
                        openIntentCamera();
                    }
                } else if (items[item].equals("Use Existing Foto")) {
                    if (checkWriteExternalPermission()) {
                        openIntentFile();
                    } else {
                        ActivityCompat.requestPermissions(BookingInformasiCustomerActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);
                    }

                }
            }
        });
        builder.show();
    }

    private void openIntentFile() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void openIntentCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(BookingInformasiCustomerActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = BookingInformasiCustomerActivity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void showDialogCekPermission() {
        if (ActivityCompat.checkSelfPermission(BookingInformasiCustomerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(BookingInformasiCustomerActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(BookingInformasiCustomerActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == SELECT_FILE) {
                openIntentFile();
            } else if (requestCode == REQUEST_CAMERA) {
                openIntentCamera();
            }
        } else {
            proceedAfterPermissionDeny();
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("picUri", file);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        file = savedInstanceState.getParcelable("picUri");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                //onSelectFromGalleryResult(data);
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                } else {
                    handleGalleryResult19(data);
                }
            else if (requestCode == REQUEST_CAMERA)
                //onCaptureImageResult(data);
                if (type != null && type.equals("2")) {
                    Intent i = new Intent(this, BookingKtpActivity.class);
                    i.putExtra(DBMASTER_REF, dbMasterRef);
                    i.putExtra(PROJECT_REF, projectRef);
                    i.putExtra(CATEGORY_REF, categoryRef);
                    i.putExtra(CLUSTER_REF, clusterRef);
                    i.putExtra(PRODUCT_REF, productRef);
                    i.putExtra(UNIT_REF, unitRef);
                    i.putExtra(TERM_REF, termRef);
                    i.putExtra(TERM_NO, termNo);
                    i.putExtra(PROJECT_NAME, projectName);
                    i.putExtra("va1", va1);
                    i.putExtra("va2", va2);
                    if (ktpRef.equals("")) {
                        i.putExtra(PHONE, editPhone.getText().toString());
                        i.putExtra(ADDRESS, editAddress.getText().toString());
                        i.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else if (type != null && type.equals("3")) {
                    Intent i = new Intent(this, BookingNpwpActivity.class);
                    i.putExtra(DBMASTER_REF, dbMasterRef);
                    i.putExtra(PROJECT_REF, projectRef);
                    i.putExtra(CATEGORY_REF, categoryRef);
                    i.putExtra(CLUSTER_REF, clusterRef);
                    i.putExtra(PRODUCT_REF, productRef);
                    i.putExtra(UNIT_REF, unitRef);
                    i.putExtra(TERM_REF, termRef);
                    i.putExtra(TERM_NO, termNo);
                    i.putExtra(PROJECT_NAME, projectName);
                    i.putExtra("va1", va1);
                    i.putExtra("va2", va2);
                    if (npwpRef.equals("")) {
                        i.putExtra(PHONE, editPhone.getText().toString());
                        i.putExtra(ADDRESS, editAddress.getText().toString());
                        i.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else {
                    Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("2")) {
                    Intent i = new Intent(this, BookingKtpActivity.class);
                    i.putExtra(DBMASTER_REF, dbMasterRef);
                    i.putExtra(PROJECT_REF, projectRef);
                    i.putExtra(CATEGORY_REF, categoryRef);
                    i.putExtra(CLUSTER_REF, clusterRef);
                    i.putExtra(PRODUCT_REF, productRef);
                    i.putExtra(UNIT_REF, unitRef);
                    i.putExtra(TERM_REF, termRef);
                    i.putExtra(TERM_NO, termNo);
                    i.putExtra(PROJECT_NAME, projectName);
                    i.putExtra("va1", va1);
                    i.putExtra("va2", va2);
                    if (ktpRef.equals("")) {
                        i.putExtra(PHONE, editPhone.getText().toString());
                        i.putExtra(ADDRESS, editAddress.getText().toString());
                        i.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else if (type != null && type.equals("3")) {
                    Intent i = new Intent(this, BookingNpwpActivity.class);
                    i.putExtra(DBMASTER_REF, dbMasterRef);
                    i.putExtra(PROJECT_REF, projectRef);
                    i.putExtra(CATEGORY_REF, categoryRef);
                    i.putExtra(CLUSTER_REF, clusterRef);
                    i.putExtra(PRODUCT_REF, productRef);
                    i.putExtra(UNIT_REF, unitRef);
                    i.putExtra(TERM_REF, termRef);
                    i.putExtra(TERM_NO, termNo);
                    i.putExtra(PROJECT_NAME, projectName);
                    i.putExtra("va1", va1);
                    i.putExtra("va2", va2);
                    if (npwpRef.equals("")) {
                        i.putExtra(PHONE, editPhone.getText().toString());
                        i.putExtra(ADDRESS, editAddress.getText().toString());
                        i.putExtra(KTP_ID, editKtpId.getText().toString());
                    }
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else {
                    Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("2")) {
                        Intent i = new Intent(this, BookingKtpActivity.class);
                        i.putExtra(DBMASTER_REF, dbMasterRef);
                        i.putExtra(PROJECT_REF, projectRef);
                        i.putExtra(CATEGORY_REF, categoryRef);
                        i.putExtra(CLUSTER_REF, clusterRef);
                        i.putExtra(PRODUCT_REF, productRef);
                        i.putExtra(UNIT_REF, unitRef);
                        i.putExtra(TERM_REF, termRef);
                        i.putExtra(TERM_NO, termNo);
                        i.putExtra(PROJECT_NAME, projectName);
                        i.putExtra("va1", va1);
                        i.putExtra("va2", va2);
                        if (ktpRef.equals("")) {
                            i.putExtra(PHONE, editPhone.getText().toString());
                            i.putExtra(ADDRESS, editAddress.getText().toString());
                            i.putExtra(KTP_ID, editKtpId.getText().toString());
                        }
                        i.putExtra("pathImage", file.getEncodedPath());
                        //i.putExtra("Image", byteArray);
                        startActivity(i);
                    } else if (type != null && type.equals("3")) {
                        Intent i = new Intent(this, BookingNpwpActivity.class);
                        i.putExtra(DBMASTER_REF, dbMasterRef);
                        i.putExtra(PROJECT_REF, projectRef);
                        i.putExtra(CATEGORY_REF, categoryRef);
                        i.putExtra(CLUSTER_REF, clusterRef);
                        i.putExtra(PRODUCT_REF, productRef);
                        i.putExtra(UNIT_REF, unitRef);
                        i.putExtra(TERM_REF, termRef);
                        i.putExtra(TERM_NO, termNo);
                        i.putExtra(PROJECT_NAME, projectName);
                        i.putExtra("va1", va1);
                        i.putExtra("va2", va2);
                        if (npwpRef.equals("")) {
                            i.putExtra(PHONE, editPhone.getText().toString());
                            i.putExtra(ADDRESS, editAddress.getText().toString());
                            i.putExtra(KTP_ID, editKtpId.getText().toString());
                        }
                        i.putExtra("pathImage", file.getEncodedPath());
                        //i.putExtra("Image", byteArray);
                        startActivity(i);
                    } else {
                        Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    private void handleGalleryResult19(Intent data) {
        uri = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                manufacturer = info.manufacturer;

                mTmpGalleryPicturePath = getRealPathFromURI(BookingInformasiCustomerActivity.this, uri);
                if (mTmpGalleryPicturePath == null) {
                    mTmpGalleryPicturePath = getPath(selectedImage);
                }

                Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                if (!mTmpGalleryPicturePath.equals("")) {
                    if (mTmpGalleryPicturePath != null) {
                        if (type != null && type.equals("2")) {
                            Intent i = new Intent(BookingInformasiCustomerActivity.this, BookingKtpActivity.class);
                            i.putExtra(DBMASTER_REF, dbMasterRef);
                            i.putExtra(PROJECT_REF, projectRef);
                            i.putExtra(CATEGORY_REF, categoryRef);
                            i.putExtra(CLUSTER_REF, clusterRef);
                            i.putExtra(PRODUCT_REF, productRef);
                            i.putExtra(UNIT_REF, unitRef);
                            i.putExtra(TERM_REF, termRef);
                            i.putExtra(TERM_NO, termNo);
                            i.putExtra(PROJECT_NAME, projectName);
                            i.putExtra("va1", va1);
                            i.putExtra("va2", va2);
                            if (ktpRef.equals("")) {
                                i.putExtra(PHONE, editPhone.getText().toString());
                                i.putExtra(ADDRESS, editAddress.getText().toString());
                                i.putExtra(KTP_ID, editKtpId.getText().toString());
                            }
                            i.putExtra("pathImage", mTmpGalleryPicturePath);
                            //i.putExtra("Image", byteArray);
                            startActivity(i);
                        } else if (type != null && type.equals("3")) {
                            Intent i = new Intent(BookingInformasiCustomerActivity.this, BookingNpwpActivity.class);
                            i.putExtra(DBMASTER_REF, dbMasterRef);
                            i.putExtra(PROJECT_REF, projectRef);
                            i.putExtra(CATEGORY_REF, categoryRef);
                            i.putExtra(CLUSTER_REF, clusterRef);
                            i.putExtra(PRODUCT_REF, productRef);
                            i.putExtra(UNIT_REF, unitRef);
                            i.putExtra(TERM_REF, termRef);
                            i.putExtra(TERM_NO, termNo);
                            i.putExtra(PROJECT_NAME, projectName);
                            i.putExtra("va1", va1);
                            i.putExtra("va2", va2);
                            if (npwpRef.equals("")) {
                                i.putExtra(PHONE, editPhone.getText().toString());
                                i.putExtra(ADDRESS, editAddress.getText().toString());
                                i.putExtra(KTP_ID, editKtpId.getText().toString());
                            }
                            i.putExtra("pathImage", mTmpGalleryPicturePath);
                            //i.putExtra("Image", byteArray);
                            startActivity(i);
                        } else {
                            Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        try {
                            InputStream is = getContentResolver().openInputStream(selectedImage);
                            //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                            mTmpGalleryPicturePath = selectedImage.getPath();
                            Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                            if (type != null && type.equals("2")) {
                                Intent i = new Intent(BookingInformasiCustomerActivity.this, BookingKtpActivity.class);
                                i.putExtra(DBMASTER_REF, dbMasterRef);
                                i.putExtra(PROJECT_REF, projectRef);
                                i.putExtra(CATEGORY_REF, categoryRef);
                                i.putExtra(CLUSTER_REF, clusterRef);
                                i.putExtra(PRODUCT_REF, productRef);
                                i.putExtra(UNIT_REF, unitRef);
                                i.putExtra(TERM_REF, termRef);
                                i.putExtra(TERM_NO, termNo);
                                i.putExtra(PROJECT_NAME, projectName);
                                i.putExtra("va1", va1);
                                i.putExtra("va2", va2);
                                if (ktpRef.equals("")) {
                                    i.putExtra(PHONE, editPhone.getText().toString());
                                    i.putExtra(ADDRESS, editAddress.getText().toString());
                                    i.putExtra(KTP_ID, editKtpId.getText().toString());
                                }
                                i.putExtra("pathImage", mTmpGalleryPicturePath);
                                //i.putExtra("Image", byteArray);
                                startActivity(i);
                            } else if (type != null && type.equals("3")) {
                                Intent i = new Intent(BookingInformasiCustomerActivity.this, BookingNpwpActivity.class);
                                i.putExtra(DBMASTER_REF, dbMasterRef);
                                i.putExtra(PROJECT_REF, projectRef);
                                i.putExtra(CATEGORY_REF, categoryRef);
                                i.putExtra(CLUSTER_REF, clusterRef);
                                i.putExtra(PRODUCT_REF, productRef);
                                i.putExtra(UNIT_REF, unitRef);
                                i.putExtra(TERM_REF, termRef);
                                i.putExtra(TERM_NO, termNo);
                                i.putExtra(PROJECT_NAME, projectName);
                                i.putExtra("va1", va1);
                                i.putExtra("va2", va2);
                                if (npwpRef.equals("")) {
                                    i.putExtra(PHONE, editPhone.getText().toString());
                                    i.putExtra(ADDRESS, editAddress.getText().toString());
                                    i.putExtra(KTP_ID, editKtpId.getText().toString());
                                }
                                i.putExtra("pathImage", mTmpGalleryPicturePath);
                                //i.putExtra("Image", byteArray);
                                startActivity(i);
                            } else {
                                Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (type != null && type.equals("2")) {
            Intent i = new Intent(this, BookingKtpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (ktpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else if (type != null && type.equals("3")) {
            Intent i = new Intent(this, BookingNpwpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (npwpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else {
            Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Log.d("Type Galery", " " + type);

        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        if (type != null && type.equals("2")) {
            Intent i = new Intent(this, BookingKtpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (ktpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else if (type != null && type.equals("3")) {
            Intent i = new Intent(this, BookingNpwpActivity.class);
            i.putExtra(DBMASTER_REF, dbMasterRef);
            i.putExtra(PROJECT_REF, projectRef);
            i.putExtra(CATEGORY_REF, categoryRef);
            i.putExtra(CLUSTER_REF, clusterRef);
            i.putExtra(PRODUCT_REF, productRef);
            i.putExtra(UNIT_REF, unitRef);
            i.putExtra(TERM_REF, termRef);
            i.putExtra(TERM_NO, termNo);
            i.putExtra(PROJECT_NAME, projectName);
            if (npwpRef.equals("")) {
                i.putExtra(PHONE, editPhone.getText().toString());
                i.putExtra(ADDRESS, editAddress.getText().toString());
                i.putExtra(KTP_ID, editKtpId.getText().toString());
            }
            i.putExtra("Image", byteArray);
            startActivity(i);
        } else {
            Toast.makeText(BookingInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
