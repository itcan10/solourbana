package com.nataproperty.solourbana.view.project.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.project.adapter.RVProjectAdapter;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.view.project.model.ProjectModelNew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectSearchActivity extends AppCompatActivity {

    public static final String PREF_NAME = "pref";

    private List<ProjectModelNew> listProject = new ArrayList<>();
    private Display display;
    private RecyclerView rv_myproject;
    private ProgressDialog progress;

    private LinearLayout linearLayoutNoData;
    private SharedPreferences sharedPreferences;

    private EditText edtSearch;
    private TextView txtClose;

    private String memberRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_search);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        display = getWindowManager().getDefaultDisplay();
        edtSearch = (EditText) findViewById(R.id.edt_search);
        txtClose = (TextView) findViewById(R.id.txt_close);
        linearLayoutNoData = (LinearLayout) findViewById(R.id.linear_no_data);

        initWidget();
        //requestProject();

        edtSearch.setHint("Search..");
        edtSearch.setHintTextColor(getResources().getColor(R.color.colorText));
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edtSearch.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Pencarian Tidak Boleh Kosong", Toast.LENGTH_LONG).show();
                    } else {
                        if (listProject.size()!=0){
                            listProject.clear();
                        }
                        requestProject();

                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                        return true;
                    }
                }
                return false;
            }
        });

        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void initializeAdapter() {
        RVProjectAdapter adapter = new RVProjectAdapter(this, listProject, display);
        rv_myproject.setAdapter(adapter);

    }

    private void initWidget() {
        rv_myproject = (RecyclerView) findViewById(R.id.rv_myproject);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_myproject.setLayoutManager(llm);
    }

    public void requestProject() {
        progress = ProgressDialog.show(this, "", "Please Wait...", true);
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListProjectKeywordForProjectSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                //swipeRefreshLayout.setRefreshing(false);
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListProject(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("keyword", edtSearch.getText().toString());
                params.put("projectCode", General.projectCode);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "getProjectKeyword");

    }

    private void generateListProject(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProjectModelNew project = new ProjectModelNew();
                project.setDbMasterRef(jo.getLong("dbMasterRef"));
                project.setProjectRef(jo.getString("projectRef"));
                project.setLocationRef(jo.getString("locationRef"));
                project.setLocationName(jo.getString("locationName"));
                project.setSublocationRef(jo.getString("sublocationRef"));
                project.setSubLocationName(jo.getString("subLocationName"));
                project.setProjectName(jo.getString("projectName"));
                project.setIsJoin(jo.getString("isJoin"));
                project.setImage(jo.getString("image"));
                listProject.add(project);
                initializeAdapter();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

//        adapter.notifyDataSetChanged();
    }
}
