package com.nataproperty.solourbana.view.booking.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.booking.intractor.PresenterBookingPaymentMethodInteractor;
import com.nataproperty.solourbana.view.booking.model.BookingPaymentMethodModel;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.booking.model.PaymentModel;
import com.nataproperty.solourbana.view.booking.model.VeritransBookingVTModel;
import com.nataproperty.solourbana.view.booking.ui.BookingPaymentMethodActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class BookingPaymentMethodPresenter implements PresenterBookingPaymentMethodInteractor {
    private BookingPaymentMethodActivity view;
    private ServiceRetrofit service;

    public BookingPaymentMethodPresenter(BookingPaymentMethodActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetPaymentMethodSvc(String dbMasterRef, String projectRef) {
        Call<List<BookingPaymentMethodModel>> call = service.getAPI().GetPaymentMethodSvc(dbMasterRef,projectRef);
        call.enqueue(new Callback<List<BookingPaymentMethodModel>>() {
            @Override
            public void onResponse(Call<List<BookingPaymentMethodModel>> call, Response<List<BookingPaymentMethodModel>> response) {
                view.showListBookingPaymentMethodResults(response);
            }

            @Override
            public void onFailure(Call<List<BookingPaymentMethodModel>> call, Throwable t) {
                view.showListBookingPaymentMethodFailure(t);

            }


        });
    }

    @Override
    public void veritransBookingVTSvc(String dbMasterRef, String projectRef, String memberRef,
                                      String bookingRef, String termRef,String paymentType,final String paymentMethod) {
        Call<VeritransBookingVTModel> call = service.getAPI().veritransBookingVTSvc(dbMasterRef,projectRef, memberRef,  bookingRef,  termRef, paymentType);
        call.enqueue(new Callback<VeritransBookingVTModel>() {
            @Override
            public void onResponse(Call<VeritransBookingVTModel> call, Response<VeritransBookingVTModel> response) {
                view.showVeritransBookingVTResults(response,paymentMethod);
            }

            @Override
            public void onFailure(Call<VeritransBookingVTModel> call, Throwable t) {
                view.showVeritransBookingVTFailure(t);

            }


        });
    }

    @Override
    public void getIlustrationPaymentSvc(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<PaymentModel> call = service.getAPI().getIlustrationPaymentSvc(dbMasterRef,projectRef, clusterRef,  productRef,  unitRef,  termRef,  termNo);
        call.enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
                view.showPaymentModelResults(response);
            }

            @Override
            public void onFailure(Call<PaymentModel> call, Throwable t) {
                view.showPaymentModelFailure(t);

            }


        });
    }

    @Override
    public void getBookingDetailSvc(String dbMasterRef, String projectRef, String bookingRef) {
        Call<DetailBooking> call = service.getAPI().getBookingDetailSvc(dbMasterRef,projectRef, bookingRef);
        call.enqueue(new Callback<DetailBooking>() {
            @Override
            public void onResponse(Call<DetailBooking> call, Response<DetailBooking> response) {
                view.showBookingInfoResults(response);
            }

            @Override
            public void onFailure(Call<DetailBooking> call, Throwable t) {
                view.showBookingInfoFailure(t);

            }


        });
    }

}
