package com.nataproperty.solourbana.view.mynup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mynup.model.NeedAttentionModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class NeedAttetionDetailAdapter extends BaseAdapter {
    private Context context;
    private List<NeedAttentionModel> list;
    private ListUnPaidHolder holder;

    public NeedAttetionDetailAdapter(Context context, List<NeedAttentionModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_un_paid_detail,null);
            holder = new ListUnPaidHolder();
            holder.orderStatus = (TextView) convertView.findViewById(R.id.txt_order_status);
            holder.costumerName = (TextView) convertView.findViewById(R.id.txt_costumer_name);

            convertView.setTag(holder);
        }else{
            holder = (ListUnPaidHolder) convertView.getTag();
        }

        NeedAttentionModel needAttentionModel = list.get(position);

        holder.orderStatus.setText(needAttentionModel.getNupOrderStatus());
        holder.costumerName.setText(needAttentionModel.getCustomerName());

        Log.d("cek nup adapter",needAttentionModel.getNupOrderStatus());

        return convertView;
    }

    private class ListUnPaidHolder {
        TextView orderStatus,costumerName;
    }
}
