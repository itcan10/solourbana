package com.nataproperty.solourbana.view.project.model;



/**
 * Created by herlambang-nata on 11/1/2016.
 */

public class ListUnitModel {
    long dbMasterRef;
    String unitName;

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
