package com.nataproperty.solourbana.view.before_login.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.nataproperty.solourbana.view.before_login.ui.ImageFiveFragment;
import com.nataproperty.solourbana.view.before_login.ui.ImageFourFragment;
import com.nataproperty.solourbana.view.before_login.ui.ImageOneFragment;
import com.nataproperty.solourbana.view.before_login.ui.ImageSevenFragment;
import com.nataproperty.solourbana.view.before_login.ui.ImageSixFragment;
import com.nataproperty.solourbana.view.before_login.ui.ImageThreeFragment;
import com.nataproperty.solourbana.view.before_login.ui.ImageTwoFragment;


public class ImageViewPagerAdapter extends FragmentPagerAdapter {
    private Context _context;
    public static int totalPage = 5;

    public ImageViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch (position) {
            case 0:
                f = new ImageOneFragment();
                break;
            case 1:
                f = new ImageTwoFragment();
                break;
            case 2:
                f = new ImageThreeFragment();
                break;
            case 3:
                f = new ImageFourFragment();
                break;
            case 4:
                f = new ImageFiveFragment();
                break;
//            case 5:
//                f = new ImageSixFragment();
//                break;
//            case 6:
//                f = new ImageSevenFragment();
//                break;
        }
        return f;
    }

    @Override
    public int getCount() {
        return totalPage;
    }

}

