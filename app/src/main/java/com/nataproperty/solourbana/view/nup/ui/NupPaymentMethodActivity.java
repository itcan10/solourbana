package com.nataproperty.solourbana.view.nup.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;
import com.nataproperty.solourbana.view.booking.model.BookingPaymentMethodModel;
import com.nataproperty.solourbana.view.mynup.ui.MyNupActivity;
import com.nataproperty.solourbana.view.nup.adapter.NupPaymentMethodAdapter;
import com.nataproperty.solourbana.view.nup.model.VeritransNupVTModel;
import com.nataproperty.solourbana.view.nup.presenter.NupPaymentMethodPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by UserModel on 5/21/2016.
 */
public class NupPaymentMethodActivity extends AppCompatActivity {
    public static final String TAG = "NupPaymentMethod";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String NUP_AMT = "nupAmt";
    public static final String TOTAL = "total";
    private static final String EXTRA_RX = "EXTRA_RX";

    NupPaymentMethodPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;
    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;
    //logo
    ImageView imgLogo;
    TextView txtTotal, txtProjectName;
    private String dbMasterRef, projectRef, projectName, nupAmt;
    private boolean mylistNup;
    private String nupOrderRef, total, memberRef, paymentType, isOnline, paymentMethod;

    Toolbar toolbar;
    TextView title;
    Typeface font, fontLight;
    MyListView listView;
    NupPaymentMethodAdapter nupPaymentMethodAdapter;
    List<BookingPaymentMethodModel> listNupPaymentMethod = new ArrayList<>();
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_payment_method);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new NupPaymentMethodPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        nupAmt = intent.getStringExtra(NUP_AMT);
        total = intent.getStringExtra(TOTAL);
        //fromMyListNup
        mylistNup = intent.getBooleanExtra("mylistNup", false);
        initWidget();

        Log.d(TAG, dbMasterRef + " " + projectRef + " " + nupOrderRef + " " + projectName + " " + total);
        Log.d(TAG, "mylistNup " + mylistNup);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isOnline = listNupPaymentMethod.get(position).getIsOnline();
                paymentType = listNupPaymentMethod.get(position).getPaymentType();
                paymentMethod = listNupPaymentMethod.get(position).getPaymentMethod();
                if (isOnline.equals("0")) {
                    paymentTypeBankTranfer();
                } else {
                    paymentVeritrans(paymentMethod);
                }
            }
        });

        requestPaymentMethod();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_nup_payment));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtTotal = (TextView) findViewById(R.id.txt_total);
        txtTotal.setText(total);

        listView = (MyListView) findViewById(R.id.list_nup_paymnet_method);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
    }


    private void requestPaymentMethod() {
        presenter.GetPaymentMethodSvc(dbMasterRef, projectRef);
    }

    public void showListNupPaymentMethodResults(Response<List<BookingPaymentMethodModel>> response) {
        listNupPaymentMethod = response.body();
        initNupPaymentMethodAdapter();
    }

    public void showListNupPaymentMethodFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initNupPaymentMethodAdapter() {
        nupPaymentMethodAdapter = new NupPaymentMethodAdapter(this, listNupPaymentMethod);
        listView.setAdapter(nupPaymentMethodAdapter);
        listView.setExpanded(true);
    }

    private void paymentTypeBankTranfer() {
        String nupOrderRefProjectCode = nupOrderRef + "#" + General.projectCode;
        presenter.updatePaymentTypeOrderSvc(nupOrderRefProjectCode, paymentType, memberRef);
    }

    public void showPaymentTypeResults(Response<VeritransNupVTModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        String link = response.body().getRedirectUrl();

        if (status == 200) {
            Intent intent = new Intent(NupPaymentMethodActivity.this, NupBankTransferActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(PROJECT_NAME, projectName);
            intent.putExtra(NUP_ORDER_REF, nupOrderRef);
            intent.putExtra(TOTAL, total);
            intent.putExtra("paymentType", paymentType);
            intent.putExtra("mylistNup", mylistNup);
            startActivity(intent);
        } else if (status == 300) {
            Intent intent = new Intent(NupPaymentMethodActivity.this, NupCreditCardWebviewActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(PROJECT_NAME, projectName);
            intent.putExtra(NUP_ORDER_REF, nupOrderRef);
            intent.putExtra(TOTAL, total);
            intent.putExtra("link", link);
            intent.putExtra("titlePayment", getString(R.string.txt_title_bank_transfer));
            intent.putExtra("paymentType", paymentType);
            intent.putExtra("mylistNup", mylistNup);
            startActivity(intent);
        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, message, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public void showPaymentTypeFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void paymentVeritrans(String paymentMethod) {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        String dpMasterRefProjectCode = dbMasterRef + "#" + General.projectCode;
        presenter.updatePaymentTypeOrderVTSvc(nupOrderRef, paymentType, dpMasterRefProjectCode, projectRef, memberRef, nupAmt, total, paymentMethod);
    }

    public void showPaymentTypeOrderVTResults(Response<VeritransNupVTModel> response, String paymentMethod) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        String link = response.body().getRedirectUrl();

        if (status == 200) {
            Intent intent = new Intent(NupPaymentMethodActivity.this, NupCreditCardWebviewActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(PROJECT_NAME, projectName);
            intent.putExtra(NUP_ORDER_REF, nupOrderRef);
            intent.putExtra(TOTAL, total);
            intent.putExtra("link", link);
            intent.putExtra("titlePayment", paymentMethod);
            intent.putExtra("paymentType", paymentType);
            intent.putExtra("mylistNup", mylistNup);
            startActivity(intent);
        } else if (status == 406) {
            Snackbar snackbar = Snackbar.make(linearLayout, message, Snackbar.LENGTH_LONG);
            snackbar.show();
        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, message, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public void showPaymentTypeOrderVTFailure(Throwable t) {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mylistNup) {
            Intent i = new Intent(NupPaymentMethodActivity.this, MyNupActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } else {
            Intent intent = new Intent(NupPaymentMethodActivity.this, ProjectMenuActivity.class);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
            intent.putExtra(PROJECT_NAME, projectName);
            //intent.putExtra("EXIT", mylistNup);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }


    }

}
