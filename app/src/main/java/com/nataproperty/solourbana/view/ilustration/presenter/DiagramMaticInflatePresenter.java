package com.nataproperty.solourbana.view.ilustration.presenter;


import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.ilustration.intractor.DiagramMaticInflateInteractor;
import com.nataproperty.solourbana.view.ilustration.model.ListBlockDiagramModel;
import com.nataproperty.solourbana.view.ilustration.ui.DiagramMaticInflateActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class DiagramMaticInflatePresenter implements DiagramMaticInflateInteractor {
    private DiagramMaticInflateActivity view;
    private ServiceRetrofit service;

    public DiagramMaticInflatePresenter(DiagramMaticInflateActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getBlockMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef, String productRefParam, String memberRef) {
        Call<List<ListBlockDiagramModel>> call = service.getAPI().getListBlockProductColorSvc(dbMasterRef, projectRef, categoryRef, clusterRef, productRefParam, memberRef);
        call.enqueue(new Callback<List<ListBlockDiagramModel>>() {
            @Override
            public void onResponse(Call<List<ListBlockDiagramModel>> call, Response<List<ListBlockDiagramModel>> response) {
                view.showBlockMappingResults(response);
            }

            @Override
            public void onFailure(Call<List<ListBlockDiagramModel>> call, Throwable t) {
                view.showBlockMappingFailure(t);

            }

        });

    }

}
