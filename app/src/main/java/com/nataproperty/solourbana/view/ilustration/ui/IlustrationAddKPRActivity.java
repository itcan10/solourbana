package com.nataproperty.solourbana.view.ilustration.ui;

import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.MoneyValueFilter;
import com.nataproperty.solourbana.helper.MyEditTextLatoReguler;
import com.nataproperty.solourbana.view.kpr.adapter.KprAdapter;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.ilustration.model.StatusIlustrationAddKprModel;
import com.nataproperty.solourbana.view.kpr.model.KprModel;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.ilustration.presenter.IlustrationAddKprPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;

import static com.nataproperty.solourbana.helper.MoneyValueFilter.getDecimalFormattedString;

/**
 * Created by UserModel on 8/29/2016.
 */
public class IlustrationAddKPRActivity extends AppCompatActivity {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";

    public static final String PROJECT_NAME = "projectName";

    public static final String IS_BOOKING = "isBooking";

    private String dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo, projectName;
    private String  propertys,category,product,unit,area,priceInc;
    private String paymentTerm,priceIncVat,discPercent,discAmt,netPrice, netPrice2,termCondition;

    private String isBooking,bookingContact, viewName;
    private String va1, va2;

    private ImageView imgLogo;
    private TextView txtProjectName;

    private AlertDialog alertDialogKPR;
    private Button btnAddTahunKpr, btnDeleteKPR;
    private KprModel kprModel;
    private ArrayList<KprModel> listKpr = new ArrayList<>();
    public ArrayList<String> tahunKprList = new ArrayList<String>();
    public ArrayList<String> bungaList = new ArrayList<String>();
    private String stringListKpr = "";
    private KprAdapter kprAdapter;
    private LinearLayout linearLayoutTahunKpr, linearNoDataKPR, layoutDisc, layoutNetVat,linearKPRAdvance;
    private MyListView listViewKPR;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private IlustrationAddKprPresenter presenter;
    ProgressDialog progressDialog;

    private Button btnNext;

    private TextView txtPropertyName,txtCategoryType,txtProduct,txtUnitNo,txtArea,txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount, txtView;
    private int finType;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    RelativeLayout rPage;
    Typeface font;
    Display display;
    Point size;
    Integer width;
    Double result;

    MyEditTextLatoReguler edtIncome;
    MyEditTextLatoReguler edtTenor;
    private String incomeAmt, tenor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_addkpr);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new IlustrationAddKprPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        isBooking = intent.getStringExtra(IS_BOOKING);
        propertys = intent.getStringExtra("propertys");
        category = intent.getStringExtra("category");
        product = intent.getStringExtra("product");
        unit = intent.getStringExtra("unit");
        area = intent.getStringExtra("area");
        priceInc = intent.getStringExtra("priceInc");
        netPrice2 = intent.getStringExtra("netPrice");
        bookingContact = intent.getStringExtra("bookingContact");
        viewName = intent.getStringExtra("viewName");
        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");

        finType = intent.getIntExtra("finType",0);
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);
        kprAdapter = new KprAdapter(this, listKpr);
        listViewKPR.setAdapter(kprAdapter);
        listViewKPR.setExpanded(true);


        btnAddTahunKpr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IlustrationAddKPRActivity.this,R.style.AlertDialogTheme);
                LayoutInflater inflater = IlustrationAddKPRActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_add_tahun_kpr, null);
                dialogBuilder.setView(dialogView);

                final EditText edtTahunKpr = (EditText) dialogView.findViewById(R.id.edt_tahun_kpr);
                final EditText edtBunga = (EditText) dialogView.findViewById(R.id.edt_bunga);

                dialogBuilder.setMessage(getString(R.string.txt_input_bunga));
                dialogBuilder.setPositiveButton(getResources().getString(R.string.btn_add), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                alertDialogKPR = dialogBuilder.create();
                alertDialogKPR.show();
                Button positiveButton = alertDialogKPR.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String tahunKpr = edtTahunKpr.getText().toString();
                        String bunga = edtBunga.getText().toString();

                        if (tahunKpr.isEmpty() || bunga.isEmpty() || bunga.equals(".")) {
                            if (tahunKpr.isEmpty()) {
                                edtTahunKpr.setError("masukkan tahun KPR");

                            } else {
                                edtTahunKpr.setError(null);
                            }
                            if (bunga.isEmpty()) {
                                edtBunga.setError("masukkan bunga");

                            } else if (bunga.equals(".")){
                                edtBunga.setError("bunga tidak sesuai");
                            } else {
                                edtBunga.setError(null);
                            }

                        } else {
                            int kprAkhir = Integer.parseInt(tahunKpr) * 12;

                            kprModel = new KprModel();
                            kprModel.setTahunKpr(tahunKpr);
                            kprModel.setBunga(bunga);
                            listKpr.add(kprModel);

                            tahunKprList.add(String.valueOf(kprAkhir));
                            bungaList.add(bunga);

                            stringListKpr = tahunKprList + " # " + bungaList;

                            IlustrationAddKPRActivity.this.kprAdapter.notifyDataSetChanged();

                            if (tahunKprList.size() == 0) {
                                linearNoDataKPR.setVisibility(View.VISIBLE);
                                listViewKPR.setVisibility(View.GONE);
                            } else {
                                linearNoDataKPR.setVisibility(View.GONE);
                                listViewKPR.setVisibility(View.VISIBLE);
                            }

                            alertDialogKPR.dismiss();
                        }

                    }
                });
            }
        });


        btnDeleteKPR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder =
                        new AlertDialog.Builder(IlustrationAddKPRActivity.this,R.style.AlertDialogTheme);
                alertDialogBuilder.setMessage("Apakah anda yakin menghapus semua KPR?");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listKpr.clear();
                        tahunKprList.clear();
                        bungaList.clear();

                        stringListKpr ="";
                        IlustrationAddKPRActivity.this.kprAdapter.notifyDataSetChanged();
                        linearNoDataKPR.setVisibility(View.VISIBLE);

                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intentSchadule = new Intent(IlustrationAddKPRActivity.this, IlustrationPaymentActivity.class);
//                intentSchadule.putExtra("dbMasterRef", dbMasterRef);
//                intentSchadule.putExtra("projectRef", projectRef);
//                intentSchadule.putExtra("categoryRef", categoryRef);
//                intentSchadule.putExtra("clusterRef", clusterRef);
//                intentSchadule.putExtra("productRef", productRef);
//                intentSchadule.putExtra("unitRef", unitRef);
//                intentSchadule.putExtra("termRef", termRef);
//                intentSchadule.putExtra("termNo", termNo);
//                intentSchadule.putExtra("projectName", projectName);
//
//                intentSchadule.putExtra("propertys", propertys);
//                intentSchadule.putExtra("category", category);
//                intentSchadule.putExtra("product", product);
//                intentSchadule.putExtra("unit", unit);
//                intentSchadule.putExtra("area", area);
//                intentSchadule.putExtra("priceInc", priceInc);
//                intentSchadule.putExtra("viewName", viewName);
//
//                intentSchadule.putExtra(IS_BOOKING, isBooking);
//
//                intentSchadule.putExtra("stringListKpr", stringListKpr);
//                intentSchadule.putExtra("finType", finType);
//                intentSchadule.putExtra("bookingContact", bookingContact);
//                startActivity(intentSchadule);

                boolean isValid = true;

                incomeAmt = edtIncome.getText().toString().replace(",","").trim();
                tenor = edtTenor.getText().toString().trim();

                if ((incomeAmt.equals("") && !tenor.equals("")) || (tenor.equals("") && !incomeAmt.equals(""))){
                    isValid = false;
                }

                if (isValid) {
                    Intent intentSchadule = new Intent(IlustrationAddKPRActivity.this, IlustrationPaymentActivity.class);
                    intentSchadule.putExtra("dbMasterRef", dbMasterRef);
                    intentSchadule.putExtra("projectRef", projectRef);
                    intentSchadule.putExtra("categoryRef", categoryRef);
                    intentSchadule.putExtra("clusterRef", clusterRef);
                    intentSchadule.putExtra("productRef", productRef);
                    intentSchadule.putExtra("unitRef", unitRef);
                    intentSchadule.putExtra("termRef", termRef);
                    intentSchadule.putExtra("termNo", termNo);
                    intentSchadule.putExtra("projectName", projectName);
                    intentSchadule.putExtra("propertys", propertys);
                    intentSchadule.putExtra("category", category);
                    intentSchadule.putExtra("product", product);
                    intentSchadule.putExtra("unit", unit);
                    intentSchadule.putExtra("area", area);
                    intentSchadule.putExtra("viewName", viewName);
                    intentSchadule.putExtra("priceInc", priceInc);
                    intentSchadule.putExtra("netPrice", netPrice2);
                    intentSchadule.putExtra(IS_BOOKING, isBooking);
                    intentSchadule.putExtra("stringListKpr", stringListKpr);
                    intentSchadule.putExtra("finType", finType);
                    intentSchadule.putExtra("bookingContact", bookingContact);
                    intentSchadule.putExtra("incomeAmt", incomeAmt);
                    intentSchadule.putExtra("tenor", tenor);
                    intentSchadule.putExtra("va1", va1);
                    intentSchadule.putExtra("va2", va2);

                    startActivity(intentSchadule);
                    edtTenor.setError(null);
                    edtIncome.setError(null);
                } else {
                    if (incomeAmt.equals("")) {
                        edtIncome.setError("Penghasilan harus diisi");
                    } else if (tenor.equals("")){
                        edtTenor.setError("Tenor harus diisi");
                    }
                }
            }
        });

        //requestpaymentinfo
        requestPayment();

        /**
         * Property info
         */


        txtPropertyName.setText(propertys);
        txtCategoryType.setText(category);
        txtProduct.setText(product);
        txtUnitNo.setText(unit);
        txtArea.setText(Html.fromHtml(area));
        txtEstimate.setText(priceInc);
        txtView.setText(viewName);

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_illustrastion_payment_schedule));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);

        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        linearLayoutTahunKpr = (LinearLayout) findViewById(R.id.linear_tahun_kpr);
        linearNoDataKPR = (LinearLayout) findViewById(R.id.linear_no_list_kpr);
        linearKPRAdvance = (LinearLayout) findViewById(R.id.linear_kprAdvance);
        btnAddTahunKpr = (Button) findViewById(R.id.btn_add_tahun_kpr);
        btnDeleteKPR = (Button) findViewById(R.id.btn_delete_kpr);
        listViewKPR = (MyListView) findViewById(R.id.list_tahun_kpr);

        btnNext = (Button) findViewById(R.id.btn_next);
        btnAddTahunKpr.setTypeface(font);
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        btnDeleteKPR.setTypeface(font);
        btnNext.setTypeface(font);
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);
        txtView = (TextView) findViewById(R.id.txt_view);


        edtIncome = (MyEditTextLatoReguler) findViewById(R.id.edt_penghasilan);
        edtTenor = (MyEditTextLatoReguler) findViewById(R.id.edt_tenor);

        incomeChange();

    }


    public void requestPayment() {
        presenter.getKprPayment(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo);

    }

    public void showKprPaymentResults(retrofit2.Response<StatusIlustrationAddKprModel> response) {

        int status = response.body().getStatus();
        String message = response.body().getMessage();
        try {
            String isKPAAdvance = response.body().getIsKPAAdvance();
            if (isKPAAdvance.equals("0")){
                linearKPRAdvance.setVisibility(View.GONE);
            }
        }catch (Exception e){

        }
        if (status==200){
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();

            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice2);
        }else {
            Toast.makeText(getApplicationContext(), message,Toast.LENGTH_LONG).show();
        }

    }

    public void showKprPaymentFailure(Throwable t) {

    }

    private void incomeChange(){
        edtIncome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int cursorPosition = edtIncome.getSelectionEnd();
                String originalStr = edtIncome.getText().toString();

                //cek untuk kasih separator koma untuk angka ribuan dan jutaan
                edtIncome.setFilters(new InputFilter[]{new MoneyValueFilter(Integer.parseInt("2"))});

                try {
                    edtIncome.removeTextChangedListener(this);
                    String value = edtIncome.getText().toString();

                    if (value != null && !value.equals("")) {
                        if (value.startsWith(".")) {
                            edtIncome.setText("0.");
                        }
//                        if (value.startsWith("0") && !value.startsWith("0.")) {
//                            edtIncome.setText("0");
//                        }
                        String str = edtIncome.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            edtIncome.setText(getDecimalFormattedString(str));

                        int diff = edtIncome.getText().toString().length() - originalStr.length();
                        edtIncome.setSelection(cursorPosition + diff);
                    }
                    edtIncome.addTextChangedListener(this);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    edtIncome.addTextChangedListener(this);
                }
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationAddKPRActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }


}
