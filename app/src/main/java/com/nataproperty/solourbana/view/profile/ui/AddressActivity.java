package com.nataproperty.solourbana.view.profile.ui;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.profile.adapter.CityAdapter;
import com.nataproperty.solourbana.view.profile.adapter.CountryAdapter;
import com.nataproperty.solourbana.view.profile.adapter.ProvinceAdapter;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.CountryModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AddressActivity extends AppCompatActivity {
    public static final String TAG = "AddressActivity";
    public static final String PREF_NAME = "pref";

    private List<CountryModel> listCounty = new ArrayList<CountryModel>();
    private CountryAdapter adapterCountry;
    private List<ProvinceModel> listProvince = new ArrayList<ProvinceModel>();
    private ProvinceAdapter adapterProvince;
    private List<CityModel> listCity = new ArrayList<CityModel>();
    private CityAdapter adapterCity;

    private List<CountryModel> listCorresCounty = new ArrayList<CountryModel>();
    private CountryAdapter adapterCorresCountry;
    private List<ProvinceModel> listCorresProvince = new ArrayList<ProvinceModel>();
    private ProvinceAdapter adapterCorresProvince;
    private List<CityModel> listCorresCity = new ArrayList<CityModel>();
    private CityAdapter adapterCorresCity;

    private String idCountryCode = "", idProvinceCode = "", idCityCode = "", idPostCode, idAddr;
    private String corresCountryCode = "", corresProvinceCode = "", corresCityCode = "", corresPostCode, corresAddr;
    private String memberRef;
    private SharedPreferences sharedPreferences;

    @Bind(R.id.spn_country)
    Spinner spnCountry;
    @Bind(R.id.spn_province)
    Spinner spnProvince;
    @Bind(R.id.spn_city)
    Spinner spnCity;
    @Bind(R.id.spn_corres_country)
    Spinner spnCorresCountry;
    @Bind(R.id.spn_corres_province)
    Spinner spnCorresProvince;
    @Bind(R.id.spn_corrres_city)
    Spinner spnCorresCity;

    @Bind(R.id.edt_id_address)
    EditText edtIdAddress;
    @Bind(R.id.edt_id_post_code)
    EditText edtIdPostCode;
    @Bind(R.id.edt_corres_address)
    EditText edtCorresAddress;
    @Bind(R.id.edt_corres_post_code)
    EditText edtCorresPostCode;

    @Bind(R.id.cb_corress_address)
    CheckBox checkBoxCorres;
    @Bind(R.id.rCorres)
    LinearLayout cardViewCorres;
    @Bind(R.id.btnSaveAddress)
    Button btnSaveAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_address));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        idCountryCode = intent.getStringExtra("idCountryCode");
        idProvinceCode = intent.getStringExtra("idProvinceCode");
        idCityCode = intent.getStringExtra("idCityCode");
        idPostCode = intent.getStringExtra("idPostCode");
        idAddr = intent.getStringExtra("idAddr");

        corresCountryCode = intent.getStringExtra("corresCountryCode");
        corresProvinceCode = intent.getStringExtra("corresProvinceCode");
        corresCityCode = intent.getStringExtra("corresCityCode");
        corresPostCode = intent.getStringExtra("corresPostCode");
        corresAddr = intent.getStringExtra("corresAddr");

        Log.d("AddressCorres",corresCountryCode+" "+corresProvinceCode+" "+corresCityCode+" "+corresPostCode+" "+corresAddr);

        /**
         * ID
         */
        adapterCountry = new CountryAdapter(this, listCounty);
        spnCountry.setAdapter(adapterCountry);
        spnCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idCountryCode = listCounty.get(position).getCountryCode();
                listProvince.clear();
                requestProvince(idCountryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterProvince = new ProvinceAdapter(this, listProvince);
        spnProvince.setAdapter(adapterProvince);
        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idProvinceCode = listProvince.get(position).getProvinceCode();
                listCity.clear();
                requestCity(idCountryCode, idProvinceCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterCity = new CityAdapter(this, listCity);
        spnCity.setAdapter(adapterCity);
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idCityCode = listCity.get(position).getCityCode();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtIdAddress.setText(idAddr);
        edtIdPostCode.setText(idPostCode);

        /**
         * Corres
         */
        adapterCorresCountry = new CountryAdapter(this, listCorresCounty);
        spnCorresCountry.setAdapter(adapterCorresCountry);
        spnCorresCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                corresCountryCode = listCorresCounty.get(position).getCountryCode();
                listCorresProvince.clear();
                requestCorresProvince(corresCountryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterCorresProvince = new ProvinceAdapter(this, listCorresProvince);
        spnCorresProvince.setAdapter(adapterCorresProvince);
        spnCorresProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                corresProvinceCode = listCorresProvince.get(position).getProvinceCode();
                listCorresCity.clear();
                requestCorresCity(corresCountryCode, corresProvinceCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterCorresCity = new CityAdapter(this, listCorresCity);
        spnCorresCity.setAdapter(adapterCorresCity);
        spnCorresCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                corresCityCode = listCorresCity.get(position).getCityCode();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtCorresAddress.setText(corresAddr);
        edtCorresPostCode.setText(corresPostCode);

        requestCountry();
        requestCorresCountry();

        if (idCountryCode.equals(corresCountryCode) && idProvinceCode.equals(corresProvinceCode) &&
                idCityCode.equals(corresCityCode) && idPostCode.equals(corresPostCode) &&
                idAddr.equals(corresAddr)) {
            checkBoxCorres.setChecked(true);
            cardViewCorres.setVisibility(View.GONE);

        } else {
            checkBoxCorres.setChecked(false);
            cardViewCorres.setVisibility(View.VISIBLE);
        }

        //chekbox
        checkBoxCorres.setTypeface(font);
        checkBoxCorres.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    cardViewCorres.setVisibility(View.VISIBLE);
                } else {
                    cardViewCorres.setVisibility(View.GONE);
                }
            }
        });

        btnSaveAddress.setTypeface(font);
        btnSaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateIdAddress();
            }
        });
    }

    public void requestCountry() {
        final StringRequest request = new StringRequest(Request.Method.GET,
                WebService.getCountry(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCountry(jsonArray);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                });

        BaseApplication.getInstance().addToRequestQueue(request, "requestCountry");

    }

    int setCountry = 0;

    private void generateListCountry(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CountryModel contry = new CountryModel();
                contry.setCountryCode(jo.getString("countryCode"));
                contry.setCountryName(jo.getString("countryName"));
                String countyCodeSet = jo.getString("countryCode");
                if (idCountryCode.equals(countyCodeSet)) {
                    setCountry = i;
                }
                listCounty.add(contry);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterCountry.notifyDataSetChanged();
    }

    public void requestProvince(final String countryCode) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getProvince(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListProvince(jsonArray);
                    Log.d("jsonArray", "" + jsonArray.length());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("countryCode", countryCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestProvince");

    }

    int setProvince = 0;

    private void generateListProvince(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProvinceModel province = new ProvinceModel();
                province.setProvinceCode(jo.getString("provinceCode"));
                province.setProvinceName(jo.getString("provinceName"));
                String provinceCodeSet = jo.getString("provinceCode");
                if (idProvinceCode.equals(provinceCodeSet)) {
                    setProvince = i;
                }
                listProvince.add(province);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spnProvince.setSelection(setProvince);
        adapterProvince.notifyDataSetChanged();
    }

    public void requestCity(final String countryCode, final String provinceCode) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCity(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCity(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("countryCode", countryCode);
                params.put("provinceCode", provinceCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestCity");

    }

    int setCity = 0;

    private void generateListCity(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CityModel city = new CityModel();
                city.setCityCode(jo.getString("cityCode"));
                city.setCityName(jo.getString("cityName"));
                String cityCodeSet = jo.getString("cityCode");
                if (idCityCode.equals(cityCodeSet)) {
                    setCity = i;
                }
                listCity.add(city);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spnCity.setSelection(setCity);
        adapterCity.notifyDataSetChanged();
    }

    /**
     * Corres
     */

    public void requestCorresCountry() {
        final StringRequest request = new StringRequest(Request.Method.GET,
                WebService.getCountry(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCorresCountry(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                });

        BaseApplication.getInstance().addToRequestQueue(request, "requestCountry");

    }

    int setCorresCountry = 0;

    private void generateListCorresCountry(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CountryModel contry = new CountryModel();
                contry.setCountryCode(jo.getString("countryCode"));
                contry.setCountryName(jo.getString("countryName"));
                String countyCodeSet = jo.getString("countryCode");
                if (corresCountryCode.equals(countyCodeSet)) {
                    setCorresCountry = i;
                }
                listCorresCounty.add(contry);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapterCorresCountry.notifyDataSetChanged();
    }

    public void requestCorresProvince(final String countryCode) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getProvince(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListCorresProvince(jsonArray);
                    Log.d("jsonArray", "" + jsonArray.length());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("countryCode", countryCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestProvince");

    }

    int setCorresProvince = 0;

    private void generateListCorresProvince(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ProvinceModel province = new ProvinceModel();
                province.setProvinceCode(jo.getString("provinceCode"));
                province.setProvinceName(jo.getString("provinceName"));
                String provinceCodeSet = jo.getString("provinceCode");
                if (corresProvinceCode.equals(provinceCodeSet)) {
                    setCorresProvince = i;
                }
                listCorresProvince.add(province);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spnCorresProvince.setSelection(setCorresProvince);
        adapterCorresProvince.notifyDataSetChanged();
    }

    public void requestCorresCity(final String countryCode, final String provinceCode) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCity(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    generateListCorresCity(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("countryCode", countryCode);
                params.put("provinceCode", provinceCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestCity");

    }

    int setCorresCity = 0;

    private void generateListCorresCity(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                CityModel city = new CityModel();
                city.setCityCode(jo.getString("cityCode"));
                city.setCityName(jo.getString("cityName"));
                String cityCodeSet = jo.getString("cityCode");
                if (corresCityCode.equals(cityCodeSet)) {
                    setCorresCity = i;
                }
                listCorresCity.add(city);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spnCorresCity.setSelection(setCorresCity);
        adapterCorresCity.notifyDataSetChanged();
    }

    public void updateIdAddress() {
        if (checkBoxCorres.isChecked()) {
            corresCountryCode = idCountryCode;
            corresProvinceCode = idProvinceCode;
            corresCityCode = idCityCode;
            edtCorresPostCode = edtIdPostCode;
            edtCorresAddress = edtIdAddress;

        }
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.idAddress(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);

                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        /*Intent intent = new Intent(AddressActivity.this, SectionProfileActivity.class);
                        SectionProfileActivity.getInstance().finish();
                        startActivity(intent);*/
                        finish();
                    } else if (status == 201) {
                        message = jo.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(AddressActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("countryCode", idCountryCode);
                params.put("provinceCode", idProvinceCode);
                params.put("cityCode", idCityCode);
                params.put("postCode", edtIdPostCode.getText().toString());
                params.put("address", edtIdAddress.getText().toString());
                params.put("corresCountryCode", corresCountryCode);
                params.put("corresProvinceCode", corresProvinceCode);
                params.put("corresCityCode", corresCityCode);
                params.put("corresPostCode", edtCorresPostCode.getText().toString());
                params.put("corresAddress", edtCorresAddress.getText().toString());

                Log.d(TAG,"param"+memberRef+" "+idCountryCode+" "+idProvinceCode+" "+idCityCode+" "+edtIdPostCode.getText().toString()+" "+edtIdAddress.getText().toString()+" | "+
                        corresCountryCode+" "+corresProvinceCode+" "+corresCityCode+" "+corresPostCode+" "+edtCorresPostCode.getText().toString()+" "+edtCorresAddress.getText().toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "updateIdAddress");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(AddressActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
