package com.nataproperty.solourbana.view.nup.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jaredrummler.android.device.DeviceName;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.booking.model.SaveBookingModel;
import com.nataproperty.solourbana.view.mynup.ui.MyNupActivity;
import com.nataproperty.solourbana.view.nup.adapter.AccountBankAdapter;
import com.nataproperty.solourbana.view.nup.adapter.BankAdapter;
import com.nataproperty.solourbana.view.nup.model.AccountBankModel;
import com.nataproperty.solourbana.view.nup.model.BankModel;
import com.nataproperty.solourbana.view.nup.presenter.NupBankTransferPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static com.nataproperty.solourbana.view.profile.ui.EditProfileActivity.getImageContentUri;

/**
 * Created by UserModel on 5/21/2016.
 */
public class NupBankTransferActivity extends AppCompatActivity {
    public static final String TAG = "NupBankTransferActivity";
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String TOTAL = "total";
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final String EXTRA_RX = "EXTRA_RX";
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    NupBankTransferPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;
    ImageView imgLogo;
    TextView txtProjectName;
    private List<AccountBankModel> listAccountBank = new ArrayList<AccountBankModel>();
    private AccountBankAdapter adapterAccountbank;
    MyListView listView;
    private List<BankModel> listBank = new ArrayList<BankModel>();
    private BankAdapter adapterBank;
    SharedPreferences sharedPreferences;
    ImageView imgPayment;
    Spinner bankList;
    EditText accountName, accountNumber;
    Button btnConfirmPayment;
    TextView txtTotal;
    String dbMasterRef, projectRef, projectName, memberRef, nupOrderRef, total, paymentType,
            bankRef, fileName = "", filePath, extension, manufacturer, mTmpGalleryPicturePath, cekImg = "";
    ProgressDialog progressDialog;
    private Bitmap bitmap;
    private boolean mylistNup;
    Toolbar toolbar;
    Typeface font, fontLight;
    TextView title;
    LinearLayout linearLayout;
    Uri file, uri, selectedImage;
    String pathPayment = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_bank_transfer);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new NupBankTransferPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        total = intent.getStringExtra(TOTAL);
        paymentType = intent.getStringExtra("paymentType");
        mylistNup = intent.getBooleanExtra("mylistNup", false);
        initWidget();

        Log.d(TAG, "paymentType " + paymentType);

        txtTotal.setText(total);
        imgPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = NupBankTransferActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isBankRef", bankRef);
                editor.commit();
                Log.d("isBankRef", "" + bankRef);
                selectImage();
            }
        });

        requestAcoountBank();

        btnConfirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekAccName = accountName.getText().toString();
                String cekAccNum = accountNumber.getText().toString();
                String cekBank = bankRef;

                if (!cekAccName.isEmpty() && !cekAccNum.isEmpty() && !cekBank.equals("0") && !cekImg.equals("")) {
                    confirmPayment();
                } else {

                    if (cekAccName.isEmpty()) {
                        accountName.setError("Account name must be filld");
                    } else {
                        accountName.setError(null);
                    }
                    if (cekAccNum.isEmpty()) {
                        accountNumber.setError("Account number must be filld");
                    } else {
                        accountName.setError(null);
                    }
                    if (bankRef.equals("0")) {
                        Toast.makeText(NupBankTransferActivity.this, "Bank must be filld", Toast.LENGTH_LONG)
                                .show();
                    } else if (cekImg.equals("")) {
                        Toast.makeText(NupBankTransferActivity.this, "You must upload receipt", Toast.LENGTH_LONG)
                                .show();
                    }
                }

            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Bank Transfer");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtTotal = (TextView) findViewById(R.id.txt_total);

        listView = (MyListView) findViewById(R.id.list_account_bank);

        bankList = (Spinner) findViewById(R.id.list_bank_tranfer);
        accountName = (EditText) findViewById(R.id.edit_account_name);
        accountNumber = (EditText) findViewById(R.id.edit_account_number);

        btnConfirmPayment = (Button) findViewById(R.id.btn_confirm);
        btnConfirmPayment.setTypeface(font);

        imgPayment = (ImageView) findViewById(R.id.img_receipt);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        bankRef = sharedPreferences.getString("isBankRef", null);
        if (bankRef != null) {
            bankList.setSelection(Integer.parseInt(bankRef));
        }
        requestBank();

        bankList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bankRef = listBank.get(position).getBankRef();
                //bankList.setSelection(Integer.parseInt(bankRef));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void requestBank() {
        presenter.getListBankSvc();
    }

    public void showListBankResults(Response<ArrayList<BankModel>> response) {
        listBank = response.body();
        adapterBank = new BankAdapter(this, listBank);
        bankList.setAdapter(adapterBank);
    }

    public void showListBankFailure(Throwable t) {

    }

    private void requestAcoountBank() {
        presenter.getProjectAccountBank(dbMasterRef, projectRef);
    }

    public void showListProjectAccountBankResults(Response<ArrayList<AccountBankModel>> response) {
        listAccountBank = response.body();
        adapterAccountbank = new AccountBankAdapter(this, listAccountBank);
        listView.setAdapter(adapterAccountbank);
        listView.setExpanded(true);
    }

    public void showListProjectAccountBankFailure(Throwable t) {

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(NupBankTransferActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkCameraPermission()) {
                        ActivityCompat.requestPermissions(NupBankTransferActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                    } else {
                        openIntentCamera();
                    }
                } else if (items[item].equals("Use Existing Foto")) {
                    if (checkWriteExternalPermission()) {
                        openIntentFile();
                    } else {
                        ActivityCompat.requestPermissions(NupBankTransferActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);
                    }
                }
            }
        });
        builder.show();
    }
    public boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(NupBankTransferActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = NupBankTransferActivity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void showDialogCekPermission() {
        if (ActivityCompat.checkSelfPermission(NupBankTransferActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(NupBankTransferActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(NupBankTransferActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == SELECT_FILE) {
                openIntentFile();
            } else if (requestCode == REQUEST_CAMERA) {
                openIntentCamera();
            }
        } else {
            proceedAfterPermissionDeny();
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void openIntentFile() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void openIntentCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("picUri", file);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        file = savedInstanceState.getParcelable("picUri");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                } else {
                    handleGalleryResult19(data);
                }
            }
            //onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA) {
                cekImg = "1";
                imgPayment.setImageBitmap(BitmapFactory.decodeFile(file.getEncodedPath()));
                pathPayment = file.getEncodedPath();
            }
            //onCaptureImageResult(data);

        }
    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                cekImg = "1";
                pathPayment = mTmpGalleryPicturePath;
            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                    cekImg = "1";
                    pathPayment = mTmpGalleryPicturePath;
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Snackbar snackbar = Snackbar.make(linearLayout, "Upload failed", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    private void handleGalleryResult19(Intent data) {
        uri = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                manufacturer = info.manufacturer;

                mTmpGalleryPicturePath = getRealPathFromURI(NupBankTransferActivity.this, uri);
                if (mTmpGalleryPicturePath == null) {
                    mTmpGalleryPicturePath = getPath(selectedImage);
                    pathPayment = mTmpGalleryPicturePath;
                }

                Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                if (!mTmpGalleryPicturePath.equals("")) {
                    if (mTmpGalleryPicturePath != null) {
                        imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                        cekImg = "1";
                        pathPayment = mTmpGalleryPicturePath;
                        String filename = mTmpGalleryPicturePath.substring(mTmpGalleryPicturePath.lastIndexOf("/") + 1);
                    } else {
                        try {
                            InputStream is = getContentResolver().openInputStream(selectedImage);
                            //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                            mTmpGalleryPicturePath = selectedImage.getPath();
                            Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                            imgPayment.setImageBitmap(BitmapFactory.decodeFile(mTmpGalleryPicturePath));
                            cekImg = "1";
                            pathPayment = mTmpGalleryPicturePath;
                            String filename = mTmpGalleryPicturePath.substring(mTmpGalleryPicturePath.lastIndexOf("/") + 1);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Snackbar snackbar = Snackbar.make(linearLayout, "Upload failed", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        });

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    private void confirmPayment() {
        Log.d("param save bank", dbMasterRef + "-" + projectRef + "-" + nupOrderRef + "-" + memberRef + "-" +
                bankRef + "-" + accountName.getText().toString() + "-" +
                accountNumber.getText().toString() + "-" + fileName);

        Map<String, String> fields = new HashMap<>();
        String dbMasterRefProjectCode = dbMasterRef + "#" + General.projectCode;
        fields.put("dbMasterRef", dbMasterRefProjectCode);
        fields.put("projectRef", projectRef);
        fields.put("nupOrderRef", nupOrderRef);
        fields.put("memberRef", memberRef);
        //imgPayment.buildDrawingCache();
        //Bitmap bmap = imgPayment.getDrawingCache();
        fields.put("image", getStringImage(pathPayment));
        fields.put("bankRef", bankRef);
        fields.put("accName", accountName.getText().toString());
        fields.put("accNumber", accountNumber.getText().toString());
        fields.put("fileName", fileName);
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.savePaymentBankTransferSvc(fields);
    }

    public String getStringImage(String path) {
        Bitmap bmp = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void showSaveNupResults(Response<SaveBookingModel> response) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        String bookingNotification = response.body().getBookingNotification();

        if (status == 200) {
            Intent intent = new Intent(NupBankTransferActivity.this, NupFinishActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(NUP_ORDER_REF, nupOrderRef);
            intent.putExtra("paymentType", paymentType);
            intent.putExtra("mylistNup", mylistNup);
            intent.putExtra("bookingNotification", bookingNotification);

            SharedPreferences sharedPreferences = NupBankTransferActivity.this.
                    getSharedPreferences(PREF_NAME, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("isMemberCostumerRef", "");
            editor.putString("isBankRef", "0");
            editor.commit();
            startActivity(intent);
            Log.d("cek result", " " + message);
        } else {
            Log.d("cek result", " " + message);
        }
    }

    public void showSaveNupFailure(Throwable t) {
        Toast.makeText(this, "Proses Gagal, Silahkan Cek Koneksi Anda", Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        SharedPreferences sharedPreferences = NupBankTransferActivity.this.
                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("isBankRef", "0");
        editor.commit();
        Intent intentProjectMenu = new Intent(NupBankTransferActivity.this, ProjectMenuActivity.class);
        intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentProjectMenu);
    }
}
