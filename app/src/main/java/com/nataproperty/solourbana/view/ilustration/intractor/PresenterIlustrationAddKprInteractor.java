package com.nataproperty.solourbana.view.ilustration.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterIlustrationAddKprInteractor {
    void getKprPayment(String dbMasterRef,String projectRef,String clusterRef,String productRef,String unitRef,String termRef,String termNo);

}
