package com.nataproperty.solourbana.view.kpr.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.kpr.model.ResponeCalcUserProject;
import com.nataproperty.solourbana.view.kpr.presenter.CalculationInputProjectPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;


public class CalculationInputProjectActivity extends AppCompatActivity {
    public static final String TAG = "CalculationInputProject";
    public static final String PREF_NAME = "pref";
    public static final String CALC_USER_PROJECT_REF = "calcUserProjectRef";
    public static final String PROJECT = "project";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private CalculationInputProjectPresenter presenter;
    SharedPreferences sharedPreferences;
    String memberRef;
    EditText edtProject, edtCluster, edtProduct, edtBlock, edtUnit, edtPrice;
    AutoCompleteTextView autoProject, autoCluster, autoProduct, autoBlock, autoUnit;
    private ArrayList<String> listProject = new ArrayList<String>();
    private ArrayList<String> listCluster = new ArrayList<String>();
    private ArrayList<String> listProduct = new ArrayList<String>();
    private ArrayList<String> listBlock = new ArrayList<String>();
    Toolbar toolbar;
    TextView title;
    Typeface font;
    Button btnSave;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_input_project);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new CalculationInputProjectPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekProject = edtProject.getText().toString();

                if (cekProject.isEmpty()) {
                    if (cekProject.isEmpty()) {
                        edtProject.setError("enter a project name");
                    } else {
                        edtProject.setError(null);
                    }

                } else {

                    saveNewCalcUserProject();

                }

            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_calculation));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setTypeface(font);
        edtProject = (EditText) findViewById(R.id.edt_project_name);
        edtCluster = (EditText) findViewById(R.id.edt_cluster);
        edtProduct = (EditText) findViewById(R.id.edt_product);
        edtBlock = (EditText) findViewById(R.id.edt_block);
        edtUnit = (EditText) findViewById(R.id.edt_unit);
        edtPrice = (EditText) findViewById(R.id.edt_price);

    }

    private void saveNewCalcUserProject() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        Map<String, String> fields = new HashMap<>();
        fields.put("memberRef", memberRef);
        fields.put("project", edtProject.getText().toString());
        fields.put("cluster", "");
        fields.put("product", "");
        fields.put("block", "");
        fields.put("unit", "");
        fields.put("price", "0");
        presenter.saveNewCalcUserProject(fields);
    }

    public void showResponeResults(Response<ResponeCalcUserProject> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String calcUserProjectRef = response.body().getCalcUserProjectRef();
            if (status == 200) {
                Intent intent = new Intent(CalculationInputProjectActivity.this, CalculationCaraBayarActivity.class);
                intent.putExtra("type", 0);
                intent.putExtra("noteSave", 1);
                intent.putExtra(CALC_USER_PROJECT_REF, calcUserProjectRef);
                intent.putExtra(PROJECT, edtProject.getText().toString());
                startActivity(intent);
                finish();

            }
        }

    }

    public void showResponeFailure(Throwable t) {
        progressDialog.dismiss();
    }

    /*public void saveNewCalcUserProject() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.saveNewCalcUserProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, response);
                    int status = jsonObject.getInt("status");
                    final String calcUserProjectRef = jsonObject.getString("calcUserProjectRef");

                    if (status == 200) {
                        Intent intent = new Intent(CalculationInputProjectActivity.this, CalculationCaraBayarActivity.class);
                        intent.putExtra("type", 0);
                        intent.putExtra("noteSave", 1);
                        intent.putExtra(CALC_USER_PROJECT_REF, calcUserProjectRef);
                        intent.putExtra(PROJECT, edtProject.getText().toString());
                        startActivity(intent);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(CalculationInputProjectActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef.toString());
                params.put("project", edtProject.getText().toString());
                params.put("cluster", "");
                params.put("product", "");
                params.put("block", "");
                params.put("unit", "");
                params.put("price", "0");

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "inputProject");

    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CalculationInputProjectActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
