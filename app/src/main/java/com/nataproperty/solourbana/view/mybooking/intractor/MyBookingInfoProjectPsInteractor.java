package com.nataproperty.solourbana.view.mybooking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface MyBookingInfoProjectPsInteractor {

    void getMemberInfoSvc(String memberRef);

    void getBookingDetailSvc(String dbMasterRef, String projectRef, String bookingRef);

    void getBookingPaymentInfoSvc(String bookingRef);

}
