package com.nataproperty.solourbana.view.profile.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.nup.adapter.BankAdapter;
import com.nataproperty.solourbana.view.nup.model.BankModel;
import com.nataproperty.solourbana.view.profile.presenter.AccountBankPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * Created by UserModel on 6/3/2016.
 */
public class AccountBankActivity extends AppCompatActivity {
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private AccountBankPresenter presenter;
    private ArrayList<BankModel> listBank = new ArrayList<BankModel>();
    private BankAdapter adapterBank;

    ProgressDialog progressDialog;
    String message;
    EditText edtBankBranch, edtAccountName, edtAccountNumber;
    Spinner spnBank;
    Button btnSave;
    String bankRef;
    String memberRef, bankBranch, accName, accNo;
    Typeface font;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface fontLight;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_bank);
        intent = getIntent();
        memberRef = intent.getStringExtra("memberRef");
        bankRef = intent.getStringExtra("bankRef");
        bankBranch = intent.getStringExtra("bankBranch");
        accName = intent.getStringExtra("accName");
        accNo = intent.getStringExtra("accNo");
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new AccountBankPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        Log.d("Accont", "" + bankRef);
        edtBankBranch.setText(bankBranch);
        edtAccountName.setText(accName);
        edtAccountNumber.setText(accNo);
        requestBank();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAccountBank();
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_account_bank));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        edtBankBranch = (EditText) findViewById(R.id.edt_bank_branch);
        edtAccountName = (EditText) findViewById(R.id.edt_account_name);
        edtAccountNumber = (EditText) findViewById(R.id.edt_account_number);
        spnBank = (Spinner) findViewById(R.id.list_bank);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setTypeface(font);
    }

    private void requestBank() {
        presenter.getListBankSvc();
    }

    private void updateAccountBank() {
        String bankBranch = edtBankBranch.getText().toString();
        String accName = edtAccountName.getText().toString();
        String accNo = edtAccountNumber.getText().toString();
        presenter.updateAccountBankSvc(memberRef, bankRef, bankBranch, accName, accNo);
    }

    public void showListBankResults(Response<ArrayList<BankModel>> response) {
        listBank = response.body();
        adapterBank = new BankAdapter(getApplication(), listBank);
        spnBank.setAdapter(adapterBank);
        spnBank.setSelection(Integer.parseInt(bankRef));
        spnBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                bankRef = listBank.get(position).getBankRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showListBankFailure(Throwable t) {

    }

    public void showResponeResults(Response<ResponeModel> response) {
        int status = response.body().getStatus();
        message = response.body().getMessage();
        if (status == 200) {
            finish();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showResponeFailure(Throwable t) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(AccountBankActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
