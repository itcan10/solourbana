package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by nata on 11/28/2016.
 */

public class ListUnitMappingModel {
    long Id;
    String dbMasterReff;
    String unitName;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getDbMasterReff() {
        return dbMasterReff;
    }

    public void setDbMasterReff(String dbMasterReff) {
        this.dbMasterReff = dbMasterReff;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
