package com.nataproperty.solourbana.view.loginregister.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.google.zxing.Result;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.loginregister.presenter.RegisterQRPresenter;
import com.nataproperty.solourbana.view.scanQRCode.model.ScanURLModel;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Response;

public class RegisterCustomerQRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private final static String TAG = "RegisterCustomerQRActivity";
    private ZXingScannerView mScannerView;
    private AlertDialog alertDialog;
    private ServiceRetrofitProject service;
    private RegisterQRPresenter presenter;
    private String birthDate, bookingCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_qrcode);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new RegisterQRPresenter(this, service);
        QrScanner();
    }

    public void QrScanner() {
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {
        request(result.getText());
    }

    private void request(String result) {
        presenter.CheckingDocumentProject(General.projectCode, result);
    }

    public void showScanResults(Response<ScanURLModel> response) {
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            if (status == 200) {
                bookingCode = response.body().getBookingCode();
                birthDate = response.body().getBirthDate();
                String link = response.body().getUrlPrint();
                Intent i = new Intent(RegisterCustomerQRActivity.this, RegisterCustomerActivity.class);
                i.putExtra("bookingCode", bookingCode);
                i.putExtra("birthDate", birthDate);
                i.putExtra("qrCode", true);
                startActivity(i);
                finish();

                BaseApplication.showLog(TAG, "bookingCode:" + bookingCode + " birthDate:" + birthDate + " link:" + link);
            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterCustomerQRActivity.this);
                alertDialogBuilder
                        .setMessage("Data tidak ditemukan")
                        .setCancelable(false)
                        .setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alertDialogDismis();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }
    }

    public void showScanFailure(Throwable t) {

    }

    private void alertDialogDismis() {
        mScannerView.resumeCameraPreview(this);
    }
}
