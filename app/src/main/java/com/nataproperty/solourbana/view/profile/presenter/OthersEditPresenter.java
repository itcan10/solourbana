package com.nataproperty.solourbana.view.profile.presenter;


import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.profile.intractor.OthersEditInteractor;
import com.nataproperty.solourbana.view.profile.model.OthersEditModel;
import com.nataproperty.solourbana.view.profile.ui.OthersEditActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class OthersEditPresenter implements OthersEditInteractor {
    private OthersEditActivity view;
    private ServiceRetrofit service;

    public OthersEditPresenter(OthersEditActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getOtherProfileSvc() {
        Call<OthersEditModel> call = service.getAPI().getOtherProfileSvc();
        call.enqueue(new Callback<OthersEditModel> () {
            @Override
            public void onResponse(Call<OthersEditModel> call, Response<OthersEditModel>  response) {
                view.showListResults(response);
            }

            @Override
            public void onFailure(Call<OthersEditModel>  call, Throwable t) {
                view.showListFailure(t);

            }


        });

    }

    @Override
    public void updateOtherProfileSvc(Map<String, String> fields) {
        Call<ResponeModel> call = service.getAPI().updateOtherProfileSvc(fields);
        call.enqueue(new Callback<ResponeModel> () {
            @Override
            public void onResponse(Call<ResponeModel>  call, Response<ResponeModel>  response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponeModel>  call, Throwable t) {
                view.showResponeFailure(t);

            }


        });
    }
}
