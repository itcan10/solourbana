package com.nataproperty.solourbana.view.logbook.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.logbook.adapter.LogBookSectionAdapter;
import com.nataproperty.solourbana.view.logbook.model.LogBookSectionModel;
import com.nataproperty.solourbana.view.logbook.presenter.LogBookSectionPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;


public class LogBookSectionActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    Toolbar toolbar;
    TextView title;
    ListView listView;
    SharedPreferences sharedPreferences;
    String memberRef, projectRef, dbMasterRef, projectCustomerStatus, projectCustomerStatusName, color, message;
    ProgressDialog progressDialog;
    FloatingActionButton fab;

    LogBookSectionPresenter presenter;
    ServiceRetrofitProject service;
    boolean rxCallInWorks = false;

    LogBookSectionAdapter adapter;
    List<LogBookSectionModel> listLogBookSection = new ArrayList<>();
    Button btnContactBank;

    CoordinatorLayout activityLogBookSection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_book_section);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new LogBookSectionPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        message = intent.getStringExtra("message");

        if (message != null) {
            Snackbar snackbar = Snackbar.make(activityLogBookSection, message, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestSection();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.menu_log_book));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        listView = (ListView) findViewById(R.id.list_log_book_section);
        listView.setOnItemClickListener(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
        btnContactBank = (Button) findViewById(R.id.btn_contact_bank);
        btnContactBank.setOnClickListener(this);
        activityLogBookSection = (CoordinatorLayout) findViewById(R.id.activity_log_book_section);
    }

    private void requestSection() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetSectionContactBankProjectSvc(memberRef, dbMasterRef, projectRef);
    }

    public void showListLogBookSectionResults(Response<ArrayList<LogBookSectionModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful())
            listLogBookSection = response.body();

        initAdapter();
    }

    public void showListLogBookSectionFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new LogBookSectionAdapter(this, listLogBookSection);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(LogBookSectionActivity.this, AddLogBookProjectActivity.class);
                intent.putExtra("dbMasterRef", dbMasterRef);
                intent.putExtra("projectRef", projectRef);
                intent.putExtra("customerRef", "");
                startActivity(intent);
                break;

            case R.id.btn_contact_bank:
                Intent intent1 = new Intent(LogBookSectionActivity.this, LogBookContactBankActivity.class);
                intent1.putExtra("dbMasterRef", dbMasterRef);
                intent1.putExtra("projectRef", projectRef);
                startActivity(intent1);
                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == listView) {
            projectCustomerStatus = listLogBookSection.get(position).getProjectCustomerStatus();
            projectCustomerStatusName = listLogBookSection.get(position).getProjectCustomerStatusName();
            color = listLogBookSection.get(position).getColor();
            Intent i = new Intent(LogBookSectionActivity.this, LogBookProjectActivity.class);
            i.putExtra("dbMasterRef", dbMasterRef);
            i.putExtra("projectRef", projectRef);
            i.putExtra("projectCustomerStatus", projectCustomerStatus);
            i.putExtra("projectCustomerStatusName", projectCustomerStatusName);
            i.putExtra("color", color);
            startActivity(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(LogBookSectionActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
