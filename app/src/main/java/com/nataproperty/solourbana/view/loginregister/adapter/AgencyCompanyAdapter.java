package com.nataproperty.solourbana.view.loginregister.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.loginregister.model.CompanyModel;
import com.nataproperty.solourbana.view.loginregister.ui.RegisterPropertyAgencyActivity;

import java.util.ArrayList;

/**
 * Created by User on 5/12/2016.
 */
public class AgencyCompanyAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private ArrayList<CompanyModel> originalList;
    private ArrayList<CompanyModel> suggestions = new ArrayList<>();
    private Filter filter = new CustomFilter();

    public AgencyCompanyAdapter(Context context, ArrayList<CompanyModel> originalList) {
        this.context = context;
        this.originalList = originalList;
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_list_agency_company, parent, false);
            holder = new ViewHolder();
            holder.autoText = (TextView) convertView.findViewById(R.id.autoText);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.autoText.setText(suggestions.get(position).getCompanyName());
        holder.autoText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RegisterPropertyAgencyActivity.class);
                intent.putExtra("agencyCompanyRef", suggestions.get(position).getAgencyCompanyRef());
                Log.d("agencyCompanyRef", suggestions.get(position).getAgencyCompanyRef());

                intent.putExtra("companyName", suggestions.get(position).getCompanyName());
                intent.putExtra("principleName", suggestions.get(position).getPrincipleName());
                intent.putExtra("companyPhone", suggestions.get(position).getCompanyPhone());
                intent.putExtra("companyMobile", suggestions.get(position).getCompanyHP());
                intent.putExtra("companyAddress", suggestions.get(position).getAddress());
                context.startActivity(intent);

            }
        });

        return convertView;
    }


    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        TextView autoText;
    }

    /**
     * Our Custom Filter Class.
     */
    private class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();

            if (originalList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                for (int i = 0; i < originalList.size(); i++) {
                    if (originalList.get(i).getCompanyName().toLowerCase().contains(constraint)) { // Compare item in original list if it contains constraints.
                        suggestions.add(originalList.get(i)); // If TRUE add item in Suggestions.
                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
