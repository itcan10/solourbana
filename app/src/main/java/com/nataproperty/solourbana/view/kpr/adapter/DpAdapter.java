package com.nataproperty.solourbana.view.kpr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.kpr.model.DpModel;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class DpAdapter extends BaseAdapter  {
    public static final String TAG = "DpAdapter" ;

    private Context context;
    private ArrayList<DpModel> list;

    private ListDpHolder holder;

    public DpAdapter(Context context, ArrayList<DpModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_dp,null);
            holder = new ListDpHolder();
            holder.dp = (TextView) convertView.findViewById(R.id.txt_dp);
            holder.cicil = (TextView) convertView.findViewById(R.id.txt_cicil);
            holder.hari = (TextView) convertView.findViewById(R.id.txt_hari);

            convertView.setTag(holder);
        }else{
            holder = (ListDpHolder) convertView.getTag();
        }

        DpModel dpModel = list.get(position);
        String hari;
        hari = dpModel.getHari();
       // Log.d(TAG,dpModel.getDp());
        holder.dp.setText("DP #"+(position+1)+" "+dpModel.getDp()+"% "+"dimulai "+hari+" hari dari tgl booking fee");

        return convertView;
    }


    private class ListDpHolder {
        TextView dp,cicil,hari;
    }

}
