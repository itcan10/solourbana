package com.nataproperty.solourbana.view.event.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.event.interactor.PresenterEventDetailInteractor;
import com.nataproperty.solourbana.view.event.model.EventDetailModel;
import com.nataproperty.solourbana.view.event.model.EventSchaduleModel;
import com.nataproperty.solourbana.view.event.ui.EventDetailActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class EventDetailPresenter implements PresenterEventDetailInteractor {
    private EventDetailActivity view;
    private ServiceRetrofit service;

    public EventDetailPresenter(EventDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getNewsImageSliderSvc(String contentRef) {
        Call<ArrayList<EventDetailModel>> call = service.getAPI().getNewsImageSliderSvc(contentRef);
        call.enqueue(new Callback<ArrayList<EventDetailModel>>() {
            @Override
            public void onResponse(Call<ArrayList<EventDetailModel>> call, Response<ArrayList<EventDetailModel>> response) {
                view.showListEventImageResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<EventDetailModel>> call, Throwable t) {
                view.showListEventImageFailure(t);

            }

        });
    }

    @Override
    public void getListEventScheduleSvc(String contentRef) {
        Call<ArrayList<EventSchaduleModel>> call = service.getAPI().getListEventScheduleSvc(contentRef);
        call.enqueue(new Callback<ArrayList<EventSchaduleModel>>() {
            @Override
            public void onResponse(Call<ArrayList<EventSchaduleModel>> call, Response<ArrayList<EventSchaduleModel>> response) {
                view.showListEventeScheduleResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<EventSchaduleModel>> call, Throwable t) {
                view.showListEventScheduleFailure(t);

            }

        });
    }

}
