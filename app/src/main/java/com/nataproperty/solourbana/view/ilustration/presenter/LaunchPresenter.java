package com.nataproperty.solourbana.view.ilustration.presenter;


import com.google.android.gms.fitness.data.Subscription;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.before_login.intractor.PresenterLaunchInteractor;
import com.nataproperty.solourbana.view.before_login.model.VersionModel;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LaunchPresenter implements PresenterLaunchInteractor {
    private LaunchActivity view;
    private ServiceRetrofitProject service;
    private Subscription subscription;

    public LaunchPresenter(LaunchActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getVersionApp(String projectCode) {
        Call<VersionModel> call = service.getAPI().getNewVersion(projectCode);
        call.enqueue(new Callback<VersionModel>() {
            @Override
            public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                view.showVersionAppResults(response);
            }

            @Override
            public void onFailure(Call<VersionModel> call, Throwable t) {
                view.showVersionAppFailure(t);

            }


        });

    }

}
