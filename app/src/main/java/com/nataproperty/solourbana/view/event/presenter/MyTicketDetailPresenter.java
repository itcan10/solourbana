package com.nataproperty.solourbana.view.event.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.event.interactor.PresenterMyTicketDetailInteractor;
import com.nataproperty.solourbana.view.event.model.MyTicketDetailModel;
import com.nataproperty.solourbana.view.event.ui.MyTicketDetailActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyTicketDetailPresenter implements PresenterMyTicketDetailInteractor {
    private MyTicketDetailActivity view;
    private ServiceRetrofit service;

    public MyTicketDetailPresenter(MyTicketDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListTiketQRCode(String memberRef,String eventScheduleRef) {
        Call<ArrayList<MyTicketDetailModel>> call = service.getAPI().getListTiketQRCode(memberRef,eventScheduleRef);
        call.enqueue(new Callback<ArrayList<MyTicketDetailModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MyTicketDetailModel>> call, Response<ArrayList<MyTicketDetailModel>> response) {
                view.showListMyTicketDetailResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<MyTicketDetailModel>> call, Throwable t) {
                view.showListMyTicketDetailFailure(t);

            }


        });
    }

}
