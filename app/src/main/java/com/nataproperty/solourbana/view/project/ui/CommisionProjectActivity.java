package com.nataproperty.solourbana.view.project.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.commision.ui.CommisionActivity;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/12/2016.
 */
public class CommisionProjectActivity extends AppCompatActivity {
    public static final String TAG = "CommisionProject";
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String MEMBER_REF = "memberRef";
    public static final String IMAGE_LOGO = "imageLogo";

    SharedPreferences sharedPreferences;

    ImageView imgLogo;
    TextView txtProjectName;
    private TextView txtCommision, txtClosingFee, txtDescription, txtCommisionAmt, txtMemberCode;

    private long dbMasterRef;
    private String memberRef, projectRef, projectName, imageLogo, memberCode, isJoin, countCommision, projectPsRef;
    private String commision, closingFee, description;
    private double commissionAmt;

    private LinearLayout commision_fee, linearLayoutCommision, linearLayoutCommisionAmt;
    private Button btnChangeMemberId, btnCekPayment;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    EditText edtMemberCode;
    TextView txtTermMemberCode, bintang;
    AlertDialog alertDialogJoin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commision_project);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        isJoin = sharedPreferences.getString("isJoin", null);

        projectRef = getIntent().getStringExtra(General.PROJECT_REF);
        dbMasterRef = getIntent().getLongExtra(General.DBMASTER_REF, 0);
        projectName = getIntent().getStringExtra(PROJECT_NAME);
        imageLogo = getIntent().getStringExtra(IMAGE_LOGO);

        initWidget();

        Log.d(TAG, "isJoin " + isJoin);

        if (isJoin.trim().equals("1")) {
            btnChangeMemberId.setVisibility(View.VISIBLE);
        } else {
            btnChangeMemberId.setVisibility(View.GONE);
        }

        btnChangeMemberId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CommisionProjectActivity.this);
                LayoutInflater inflater = CommisionProjectActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_insert_member_code, null);
                dialogBuilder.setView(dialogView);
                edtMemberCode = (EditText) dialogView.findViewById(R.id.edt_member_code);
                txtTermMemberCode = (TextView) dialogView.findViewById(R.id.txt_term_member_code);
                bintang = (TextView) dialogView.findViewById(R.id.bintang);
                dialogBuilder.setMessage("Masukkan Member ID");
                txtTermMemberCode.setVisibility(View.GONE);
                bintang.setVisibility(View.GONE);
                dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //subscribeProject();
                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                alertDialogJoin = dialogBuilder.create();
                alertDialogJoin.setCancelable(false);
                alertDialogJoin.show();
                Button positiveButton = alertDialogJoin.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtMemberCode.getText().toString().equals("")) {
                            edtMemberCode.setError("Harus diisi.");
                        } else {
                            changeMemberID();
                        }
                    }
                });
            }
        });

        requestCommision();

        btnCekPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CommisionProjectActivity.this, CommisionActivity.class);
                intent.putExtra("dbMasterRef", String.valueOf(dbMasterRef));
                intent.putExtra("projectRef", projectRef);
                intent.putExtra("projectPsRef", projectPsRef);
                startActivity(intent);
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_commision));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtCommision = (TextView) findViewById(R.id.txt_commision);
        txtClosingFee = (TextView) findViewById(R.id.txt_closingFee);
        txtDescription = (TextView) findViewById(R.id.txt_description);
        //txtCommisionAmt = (TextView) findViewById(R.id.txt_commissionAmt);
        commision_fee = (LinearLayout) findViewById(R.id.commision_fee);
        linearLayoutCommision = (LinearLayout) findViewById(R.id.linear_commission);
        //linearLayoutCommisionAmt = (LinearLayout) findViewById(R.id.linear_commissionAmt);
        txtMemberCode = (TextView) findViewById(R.id.txt_member_code);
        btnChangeMemberId = (Button) findViewById(R.id.btn_change_member_id);
        btnCekPayment = (Button) findViewById(R.id.btn_cek_payment);
        btnCekPayment.setTypeface(font);
        btnChangeMemberId.setTypeface(font);
    }

    private void requestCommision() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCommision(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        commision = jo.getJSONObject("data").getString("commision");
                        closingFee = jo.getJSONObject("data").getString("closingFee");
                        description = jo.getJSONObject("data").getString("description");
                        commissionAmt = jo.getJSONObject("data").getDouble("commissionAmt");
                        memberCode = jo.getJSONObject("data").getString("memberCode");
                        countCommision = jo.getJSONObject("data").getString("commissionCount");
                        projectPsRef = jo.getJSONObject("data").getString("projectPsRef");

                        int cekCommision;
                        cekCommision = Integer.parseInt(closingFee);
                        DecimalFormat decimalFormat = new DecimalFormat("###,##0");

                        if (cekCommision == 0) {
                            commision_fee.setVisibility(View.GONE);
                        } else {
                            commision_fee.setVisibility(View.VISIBLE);
                            txtClosingFee.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(closingFee))));
                        }

                        if (Double.parseDouble(commision) == 0) {

                        } else {
                            txtCommision.setText(commision + " %");
                        }

                        if (commissionAmt == 0.0) {

                        } else {
                            txtCommision.setText(String.valueOf(decimalFormat.format(commissionAmt)));
                        }

                        txtDescription.setText(Html.fromHtml(description));

                        if (memberCode.trim().equals("")) {
                            txtMemberCode.setText("-");
                        } else {
                            txtMemberCode.setText(memberCode);
                        }

                        if (countCommision.trim().equals("0")) {
                            btnCekPayment.setVisibility(View.GONE);
                        } else {
                            btnCekPayment.setVisibility(View.VISIBLE);
                        }

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(CommisionProjectActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(CommisionProjectActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(DBMASTER_REF, String.valueOf(dbMasterRef));
                params.put(PROJECT_REF, projectRef);
                params.put(MEMBER_REF, memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "getCommision");
    }

    private void changeMemberID() {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.ChangeMemberIDSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Toast.makeText(CommisionProjectActivity.this, message, Toast.LENGTH_LONG).show();
                        alertDialogJoin.dismiss();
                        CommisionProjectActivity.this.recreate();
                    } else if (status == 201) {
                        edtMemberCode.setError(message);
                    } else if (status == 202) {
                        edtMemberCode.setError(message);
                    } else {
                        Toast.makeText(CommisionProjectActivity.this, message, Toast.LENGTH_LONG).show();
                        alertDialogJoin.dismiss();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(CommisionProjectActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(CommisionProjectActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(DBMASTER_REF, String.valueOf(dbMasterRef));
                params.put(PROJECT_REF, projectRef);
                params.put(MEMBER_REF, memberRef);
                params.put("memberCode", edtMemberCode.getText().toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "changeMemberID");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CommisionProjectActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
