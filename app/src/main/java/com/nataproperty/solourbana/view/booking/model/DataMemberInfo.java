package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class DataMemberInfo {
    String name, hP1, phone1, email1, idAddr, ktpid, ktpRef, npwpRef, npwp, birthDate;

    public String getKtpid() {
        return ktpid;
    }

    public void setKtpid(String ktpid) {
        this.ktpid = ktpid;
    }

    public String getKtpRef() {
        return ktpRef;
    }

    public void setKtpRef(String ktpRef) {
        this.ktpRef = ktpRef;
    }

    public String getNpwpRef() {
        return npwpRef;
    }

    public void setNpwpRef(String npwpRef) {
        this.npwpRef = npwpRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String gethP1() {
        return hP1;
    }

    public void sethP1(String hP1) {
        this.hP1 = hP1;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getIdAddr() {
        return idAddr;
    }

    public void setIdAddr(String idAddr) {
        this.idAddr = idAddr;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
