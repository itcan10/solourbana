package com.nataproperty.solourbana.view.nup.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jaredrummler.android.device.DeviceName;
import com.nataproperty.solourbana.BuildConfig;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.booking.model.MemberInfoModel;
import com.nataproperty.solourbana.view.nup.adapter.QtyNupAdapter;
import com.nataproperty.solourbana.view.nup.model.QtyNupModel;
import com.nataproperty.solourbana.view.nup.presenter.NupInformasiPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by UserModel on 5/18/2016.
 */
public class NupInformasiCustomerActivity extends AppCompatActivity {
    public static final String TAG = "NupInformasiCustomer";
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String NUP_AMT = "nupAmt";
    public static final String MEMBER_CUSTOMER_REF = "memberCustomerRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String STATUS = "status";
    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";
    public static final String NPWP = "npwp";
    public static final String QTY = "qty";
    public static final String TOTAL = "total";
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

    public static final String FILE_CODE_NPWP = "fileCodeNpwp";
    public static final String URL_DOC_NPWP = "urlDocNpwp";
    public static final String FILE_CODE_KTP = "fileCodeKtp";
    public static final String URL_DOC_KTP = "urlDocKtp";

    SharedPreferences sharedPreferences;
    //logo
    ImageView imgLogo;
    TextView txtProjectName, txtPriortyPrice, txtTotal;
    EditText editFullname, editEmail, editMobile, editPhone, editAddress, editKtpId , editNpwp , editBirthdate;
    ImageView imgKtp, imgNpwp;
    Button btnUploadKtp, btnUploadNpwp, btnNext;
    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address, npwp, birthdate;
    private String dbMasterRef, projectRef, projectName, memberCustomerRef, nupAmt, projectDescription,
            status, mTmpGalleryPicturePath, manufacturer, memberRef;
    private String type;
    private String ktpImg, npwpImg;
    String defaultQty = "1", qty;
    double gTotal;
    Bitmap bitmapKtp, bitmapNpwp;
    Uri file, uri, selectedImage;
    Toolbar toolbar;
    TextView title;
    Typeface font, fontLight;
    private Spinner spnQty;
    private List<QtyNupModel> listQty = new ArrayList<>();
    private QtyNupAdapter adapter;
    NupInformasiPresenter presenter;
    ServiceRetrofit service;
    ServiceRetrofitProject serviceProject;

    String fileCodeKtpcoeg = "", fileCodeNpwpcoeg = "", urlDocKtpcoeg = "", urlDocNpwpcoeg = "";
    private String imageKtp, imageNpwp, fileCodeKtp, urlDocKtp, fileCodeNpwp, urlDocNpwp, flag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_informasi_customer);
        service = ((BaseApplication) getApplication()).getNetworkService();
        serviceProject = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new NupInformasiPresenter(this, service,serviceProject);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCustomerRef = sharedPreferences.getString("isMemberCostumerRef", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Log.d("Cek Member Costumer", memberCustomerRef);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupAmt = intent.getStringExtra(NUP_AMT);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);
        projectName = intent.getStringExtra(PROJECT_NAME);
        phone1 = intent.getStringExtra(PHONE);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);
        memberCustomerRef = intent.getStringExtra(MEMBER_CUSTOMER_REF);
        npwp = intent.getStringExtra(NPWP);
        fullname = intent.getStringExtra(FULLNAME);
        birthdate = intent.getStringExtra("birthdate");
        email1 = intent.getStringExtra(EMAIL);
        mobile1 = intent.getStringExtra(MOBILE);
        ktpRef = intent.getStringExtra(KTP_REF);
        npwpRef = intent.getStringExtra(NPWP_REF);

        fileCodeNpwp = intent.getStringExtra(FILE_CODE_NPWP);
        fileCodeKtp = intent.getStringExtra(FILE_CODE_KTP);
        urlDocKtp = intent.getStringExtra(URL_DOC_KTP);
        urlDocNpwp = intent.getStringExtra(URL_DOC_NPWP);

        flag = intent.getStringExtra("flag");

        if (flag == null || flag.equals("")) {
            flag = "";
        }


        initWidget();

        Log.d(TAG, "" + phone1 + " " + address + " " + ktpid + " "+npwp);

        if (phone1 != null && address != null && ktpid != null && npwp !=null) {
            editPhone = (EditText) findViewById(R.id.edit_phone);
            editAddress = (EditText) findViewById(R.id.edit_address);
            editKtpId = (EditText) findViewById(R.id.edit_ktp_id);
            editNpwp = (EditText) findViewById(R.id.edit_npwp);
            editPhone.setText(phone1);
            editAddress.setText(address);
            editKtpId.setText(ktpid);
            editNpwp.setText(npwp);
        }

        if (birthdate != null && mobile1 != null && email1 != null) {
            editBirthdate = (EditText) findViewById(R.id.edit_birthdate);
            editEmail = (EditText) findViewById(R.id.edit_email);
            editMobile = (EditText) findViewById(R.id.edit_mobile);

            editBirthdate.setText(birthdate);
            editMobile.setText(mobile1);
            editEmail.setText(email1);
        }
        status = intent.getStringExtra(STATUS);

        Log.d(TAG, " " + dbMasterRef + "-" + projectRef + "-" + memberCustomerRef + "-" + nupAmt);
        DecimalFormat decimalFormat = new DecimalFormat("###,##0");
        txtPriortyPrice.setText("IDR " + String.valueOf(decimalFormat.format(Double.parseDouble(nupAmt))));

//        editQty.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                defaultQty = editQty.getText().toString();
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                Log.d("cek s", " " + s);
//                generateTotal(s.toString());
//            }
//        });

        imgKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                selectImage();
            }
        });

        imgNpwp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "3";
                selectImage();
            }
        });

        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String qty = editQty.getText().toString();
                String cekFullname = editFullname.getText().toString();
                String cekPhone = editPhone.getText().toString();
                String cekMobile = editMobile.getText().toString();
                String cekEmail = editEmail.getText().toString();
                String cekAddress = editAddress.getText().toString();
                String cekKtpId = editKtpId.getText().toString();
                String cekNpwp = editNpwp.getText().toString();

                if (imageKtp == null) {
                    imageKtp = "";
                }
                if (imageNpwp == null) {
                    imageNpwp = "";
                }
                if (ktpRef == null) {
                    ktpRef = "";
                }
                if (npwpRef == null) {
                    npwpRef = "";
                }

                if (urlDocKtp != null) {
                    urlDocKtpcoeg = urlDocKtp;
                }
                if (urlDocNpwp != null) {
                    urlDocNpwpcoeg = urlDocNpwp;
                }

                if (!cekFullname.isEmpty() && !cekPhone.isEmpty() && !cekMobile.isEmpty()
                        && !cekEmail.isEmpty() && !cekAddress.isEmpty() && !cekKtpId.isEmpty() && !ktpRef.equals("")
                        && !npwpRef.equals("") && !cekNpwp.isEmpty()) {

                    Intent intent = new Intent(NupInformasiCustomerActivity.this, NupProductInfoActivity.class);
                    intent.putExtra(MEMBER_CUSTOMER_REF,memberCustomerRef);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_DESCRIPTION, projectDescription);
                    intent.putExtra(PROJECT_NAME, projectName);

                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra("birthdate", editBirthdate.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    intent.putExtra(KTP_REF, ktpRef);
                    intent.putExtra(NPWP_REF, npwpRef);
                    intent.putExtra(NPWP,npwp);

                    //intent.putExtra(QTY, editQty.getText().toString());
                    intent.putExtra(QTY, qty);
                    intent.putExtra(TOTAL, txtTotal.getText().toString());
                    intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                    intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                    intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                    intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                    startActivity(intent);

                    //buat ngecek apakah product info ada isinya
                    //kalo nga ada isinya activty product info di skip
                    GetLookupSurveyNUP();

                } else {
                    if (cekFullname.isEmpty()) {
                        editFullname.setError(getResources().getString(R.string.txt_no_fullname));
                    } else {
                        editFullname.setError(null);
                    }

                    if (cekPhone.isEmpty()) {
                        editPhone.setError(getResources().getString(R.string.txt_no_phone));
                    } else {
                        editPhone.setError(null);
                    }

                    if (cekMobile.isEmpty()) {
                        editMobile.setError(getResources().getString(R.string.txt_no_mobile));
                    } else {
                        editMobile.setError(null);
                    }

                    if (cekEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                        editEmail.setError(getResources().getString(R.string.txt_no_email));
                    } else {
                        editEmail.setError(null);
                    }

                    if (cekAddress.isEmpty()) {
                        editAddress.setError(getResources().getString(R.string.txt_no_address));
                    } else {
                        editAddress.setError(null);
                    }

                    if (cekKtpId.isEmpty()) {
                        editKtpId.setError(getResources().getString(R.string.txt_no_ktp_id));
                    } else {
                        editKtpId.setError(null);
                    }

                    if (ktpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_ktp)), Toast.LENGTH_LONG).show();
                    } else if (npwpRef.equals("")) {
                        Toast.makeText(getApplicationContext(), (getResources().getString(R.string.txt_no_img_npwp)), Toast.LENGTH_LONG).show();
                    }

                    if (cekNpwp.isEmpty()) {
                        editNpwp.setError(getResources().getString(R.string.txt_no_npwp));
                    } else {
                        editNpwp.setError(null);
                    }
                }
            }
        });

        if (memberCustomerRef == null) {
            memberCustomerRef = "";
        }
        email1 = "";
        if (fileCodeKtp != null) {
            fileCodeKtpcoeg = fileCodeKtp;
        }
        if (fileCodeNpwp != null) {
            fileCodeNpwpcoeg = fileCodeNpwp;
        }

        if (urlDocKtp != null) {
            urlDocKtpcoeg = urlDocKtp;
        }
        if (urlDocNpwp != null) {
            urlDocNpwpcoeg = urlDocNpwp;
        }

        if (!urlDocKtpcoeg.equals("")) {
            Glide.with(NupInformasiCustomerActivity.this).load(urlDocKtpcoeg).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgKtp);
        } else {
            Glide.with(NupInformasiCustomerActivity.this).load(WebService.getKtp()).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgKtp);
        }

        if (!urlDocNpwpcoeg.equals("")) {
            Glide.with(NupInformasiCustomerActivity.this).load(urlDocNpwpcoeg).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgNpwp);
        } else {
            Glide.with(NupInformasiCustomerActivity.this).load(WebService.getNpwp()).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgNpwp);
        }

        requesrQtyNup();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_buy_nup));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editBirthdate = (EditText) findViewById(R.id.edit_birthdate);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editMobile = (EditText) findViewById(R.id.edit_mobile);
        editPhone = (EditText) findViewById(R.id.edit_phone);
        editAddress = (EditText) findViewById(R.id.edit_address);
        editKtpId = (EditText) findViewById(R.id.edit_ktp_id);
        editNpwp = (EditText) findViewById(R.id.edit_npwp);

        //editQty = (EditText) findViewById(R.id.edit_qty);

        txtPriortyPrice = (TextView) findViewById(R.id.txt_priority_pass);
        txtTotal = (TextView) findViewById(R.id.txt_total);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnUploadKtp = (Button) findViewById(R.id.btn_upload_ktp);
        btnUploadKtp.setTypeface(font);
        btnUploadNpwp = (Button) findViewById(R.id.btn_upload_npwp);
        btnUploadNpwp.setTypeface(font);
        btnNext = (Button) findViewById(R.id.btn_next);

        spnQty = (Spinner) findViewById(R.id.spn_qty);
    }

    private void GetLookupSurveyNUP() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.GetLookupSurveyNUP(), new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArrayUnitType = new JSONArray(jsonObject.getJSONArray("surveyUnitType").toString());
                    JSONArray jsonArrayPaymentTerm = new JSONArray(jsonObject.getJSONArray("surveyPaymentTerm").toString());
                    JSONArray jsonArrayUnitView = new JSONArray(jsonObject.getJSONArray("surveyUnitView").toString());
                    JSONArray jsonArrayUnitFloor = new JSONArray(jsonObject.getJSONArray("surveyUnitFloor").toString());
                    Log.d(TAG,"length "+jsonArrayUnitType.length()+" "+jsonArrayPaymentTerm.length()+" "+jsonArrayUnitView.length()+" "+jsonArrayUnitFloor.length());

//                    if (jsonArrayUnitType.length()<=1 && jsonArrayPaymentTerm.length()<=1 && jsonArrayUnitView.length()<=1 && jsonArrayUnitType.length()<=1) {
                    if (jsonArrayUnitType.length()<=1 || jsonArrayPaymentTerm.length()<=1 || jsonArrayUnitView.length()<=1 || jsonArrayUnitType.length()<=1) {
                        Intent intent = new Intent(NupInformasiCustomerActivity.this, NupReviewOrderActivity.class);

                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_DESCRIPTION, projectDescription);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(MEMBER_CUSTOMER_REF,memberCustomerRef);

                        intent.putExtra(FULLNAME, editFullname.getText().toString());
                        intent.putExtra("birthdate", editBirthdate.getText().toString());
                        intent.putExtra(EMAIL, editEmail.getText().toString());
                        intent.putExtra(MOBILE, editMobile.getText().toString());
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        intent.putExtra(KTP_REF, ktpRef);
                        intent.putExtra(NPWP_REF, npwpRef);
                        intent.putExtra(NPWP, editNpwp.getText().toString());

                        intent.putExtra(QTY, qty);
                        intent.putExtra(TOTAL, txtTotal.getText().toString());

                        intent.putExtra("surveyUnitTypeRef", "0");
                        intent.putExtra("surveyPaymentTermRef", "0");
                        intent.putExtra("surveyUnitViewRef", "0");
                        intent.putExtra("surveyUnitFloorRef", "0");
                        intent.putExtra("NUPPrice", "0");
                        intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                        intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);

                        intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                        intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);

                        startActivity(intent);
                    }
                    else{
                        Intent intent = new Intent(NupInformasiCustomerActivity.this, NupProductInfoActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_DESCRIPTION, projectDescription);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(MEMBER_CUSTOMER_REF, memberCustomerRef);

                        intent.putExtra(FULLNAME, editFullname.getText().toString());
                        intent.putExtra("birthdate", editBirthdate.getText().toString());
                        intent.putExtra(EMAIL, editEmail.getText().toString());
                        intent.putExtra(MOBILE, editMobile.getText().toString());
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        intent.putExtra(KTP_REF, ktpRef);
                        intent.putExtra(NPWP_REF, npwpRef);
                        intent.putExtra(NPWP, editNpwp.getText().toString());

                        intent.putExtra(QTY, qty);
                        intent.putExtra(TOTAL, txtTotal.getText().toString());
                        intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                        intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                        intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                        intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);

                        startActivity(intent);
                    }

//                  BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }if (error instanceof TimeoutError) {
                            Toast.makeText(NupInformasiCustomerActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupInformasiCustomerActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestLookupSurveyUnitType");

    }

    private void generateTotal(String s) {
        String qtyValue = s.toString();
        if (qtyValue.equals("")) {
            qtyValue = "0";
        }

        double qty = Double.parseDouble(qtyValue.toString());
        double price = Double.parseDouble(nupAmt);
        gTotal = qty * price;
        DecimalFormat decimalFormat = new DecimalFormat("###,##0");
        String totalPrice = String.valueOf(decimalFormat.format(gTotal));
        txtTotal.setText("IDR " + totalPrice);
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPsInfo(memberCustomerRef);

    }

    private void requestPsInfo(final String memberCustomerRef) {
        presenter.getMemberInfoSvc(memberCustomerRef);
    }

    public void showMemberInfoResults(Response<MemberInfoModel> response) {
        if (response.isSuccessful()) {
            int statusGet = response.body().getStatus();
            String message = response.body().getMessage();

            if (statusGet == 200) {
                //reuired
                fullname = response.body().getData().getName();
                birthdate = response.body().getData().getBirthDate();
                ktpid = response.body().getData().getKtpid();
                phone1 = response.body().getData().getPhone1();
                mobile1 = response.body().getData().gethP1();
                email1 = response.body().getData().getEmail1();
                address = response.body().getData().getIdAddr();
                ktpRef = response.body().getData().getKtpRef();
                npwpRef = response.body().getData().getNpwpRef();
                npwp = response.body().getData().getNpwp();

                if (!fullname.isEmpty()) {
                    editFullname.setText(fullname);
                    editFullname.setEnabled(false);
                } else {
                    editFullname.setEnabled(true);
                }

                if (!email1.isEmpty()) {
                    editEmail.setText(email1);
                    editEmail.setEnabled(false);
                } else {
                    editEmail.setEnabled(true);
                }

                if (!ktpid.isEmpty()) {
                    editKtpId.setText(ktpid);
                    editKtpId.setEnabled(false);
                } else {
                    editKtpId.setEnabled(true);
                }

                if (!mobile1.isEmpty()) {
                    editMobile.setText(mobile1);
                    editMobile.setEnabled(false);
                } else {
                    editMobile.setEnabled(true);
                }

                if (!phone1.isEmpty()) {
                    editPhone.setText(phone1);
                    editPhone.setEnabled(false);
                } else {
                    editPhone.setEnabled(true);
                }

                if (!address.isEmpty()) {
                    editAddress.setText(address);
                    editAddress.setEnabled(false);
                } else {
                    editAddress.setEnabled(true);
                }

                if (BuildConfig.DEBUG) {
                    imgKtp.setEnabled(true);
                    imgNpwp.setEnabled(true);
                }
                else {
                    if (ktpRef.equals("")) {
                        imgKtp.setEnabled(true);
                    } else {
                        imgKtp.setEnabled(false);
                    }if (npwpRef.equals("")) {
                        imgNpwp.setEnabled(true);
                    } else {
                        imgNpwp.setEnabled(false);
                    }
                }

                    if (!npwp.equals("")) {
                        editNpwp.setText(npwp);
                        editNpwp.setEnabled(false);
                    } else {
                        editNpwp.setEnabled(true);
                    }
                urlDocKtpcoeg = "1";
                urlDocNpwpcoeg = "1";

                if (!ktpRef.equals("") || ktpRef != null) {
                    Glide.with(NupInformasiCustomerActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgKtp);
                } else {
                    Glide.with(NupInformasiCustomerActivity.this).load(WebService.getKtp()).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgKtp);
                }

                if (!npwpRef.equals("") || npwpRef != null) {
                    Glide.with(NupInformasiCustomerActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgNpwp);
                } else {
                    Glide.with(NupInformasiCustomerActivity.this).load(WebService.getNpwp()).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgNpwp);
                }

//                Glide.with(NupInformasiCustomerActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true).into(imgKtp);
//
//                Glide.with(NupInformasiCustomerActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true).into(imgNpwp);
            }else if (statusGet == 201) {
                editFullname.setText(fullname);
                editBirthdate.setText(birthdate);
                email1 = "";

                if (fileCodeKtp != null) {
                    fileCodeKtpcoeg = fileCodeKtp;
                }
                if (fileCodeNpwp != null) {
                    fileCodeNpwpcoeg = fileCodeNpwp;
                }

                if (urlDocKtp != null) {
                    urlDocKtpcoeg = urlDocKtp;
                }
                if (urlDocNpwp != null) {
                    urlDocNpwpcoeg = urlDocNpwp;
                }

                ktpRef = "1";
                npwpRef = "1";

                if (!urlDocKtpcoeg.equals("")) {
                    Glide.with(NupInformasiCustomerActivity.this).load(urlDocKtpcoeg).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgKtp);
                } else {
                    Glide.with(NupInformasiCustomerActivity.this).load(WebService.getKtp()).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgKtp);
                }

                if (!urlDocNpwpcoeg.equals("")) {
                    Glide.with(NupInformasiCustomerActivity.this).load(urlDocNpwpcoeg).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgNpwp);
                } else {
                    Glide.with(NupInformasiCustomerActivity.this).load(WebService.getNpwp()).diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true).into(imgNpwp);
                }

            }else{
                String error = response.body().getMessage();
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showMemberInfoFailure(Throwable t) {

    }

    private void requesrQtyNup() {
        presenter.GetQtyNUPList(dbMasterRef, projectRef, memberRef);
    }

    public void showListQtyResults(Response<ArrayList<QtyNupModel>> response) {
        if (response.isSuccessful()) {
            listQty = response.body();
            Log.d(TAG, "listQty " + listQty);

            adapter = new QtyNupAdapter(this,listQty);
            spnQty.setAdapter(adapter);

            spnQty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    qty = listQty.get(position).getQtyNUP();
                    generateTotal(qty);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            //
        }
    }

    public void showListQtyFailure(Throwable t) {
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(NupInformasiCustomerActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkCameraPermission()) {
                        ActivityCompat.requestPermissions(NupInformasiCustomerActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                    } else {
                        openIntentCamera();
                    }
                } else if (items[item].equals("Use Existing Foto")) {
                    if (checkWriteExternalPermission()){
                        openIntentFile();
                    } else {
                        ActivityCompat.requestPermissions(NupInformasiCustomerActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);
                    }

                }
            }
        });
        builder.show();
    }

    public boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(NupInformasiCustomerActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = NupInformasiCustomerActivity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void showDialogCekPermission() {
        if (ActivityCompat.checkSelfPermission(NupInformasiCustomerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(NupInformasiCustomerActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(NupInformasiCustomerActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == SELECT_FILE) {
                openIntentFile();
            } else if (requestCode == REQUEST_CAMERA) {
                openIntentCamera();
            }
        } else {
            proceedAfterPermissionDeny();
        }
    }

    private void proceedAfterPermissionAllow() {
        Toast.makeText(getBaseContext(), "We got the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void openIntentFile() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void openIntentCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("picUri", file);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        file = savedInstanceState.getParcelable("picUri");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                //onSelectFromGalleryResult(data);
                //handleGalleryResult(data);
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                    //Toast.makeText(EditProfileActivity.this, "18", Toast.LENGTH_SHORT).show();
                } else {
                    handleGalleryResult19(data);
                    //Toast.makeText(EditProfileActivity.this, "19", Toast.LENGTH_SHORT).show();
                }
            else if (requestCode == REQUEST_CAMERA) {
                //onCaptureImageResult(data);
                if (type != null && type.equals("2")) {
                    Intent intent = new Intent(this, NupKtpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra("birthdate", editBirthdate.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    intent.putExtra(NPWP, editNpwp.getText().toString());

                    if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                    }
                    if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                    }
                    if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                    }
                    if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                    }
                    if (ktpRef.equals("1")) {
                        intent.putExtra("ktpref", "1");
                    }
                    if (npwpRef.equals("1")) {
                        intent.putExtra("npwpref", "1");
                    }
                    intent.putExtra("pathImage", file.getEncodedPath());
                    intent.putExtra("flaq", "1");
                    //i.putExtra("Image", byteArray);
                    startActivityForResult(intent, 20);
                } else if (type != null && type.equals("3")) {
                    Intent intent = new Intent(this, NupNpwpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra("birthdate", editBirthdate.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    intent.putExtra(NPWP, editNpwp.getText().toString());

                    if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                    }
                    if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                    }
                    if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                    }
                    if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                    }
                    if (ktpRef.equals("1")) {
                        intent.putExtra("ktpref", "1");
                    }
                    if (npwpRef.equals("1")) {
                        intent.putExtra("npwpref", "1");
                    }
                    intent.putExtra("pathImage", file.getEncodedPath());
                    intent.putExtra("flaq", "1");
                    //i.putExtra("Image", byteArray);
                    startActivityForResult(intent, 20);
                } else {
                    Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }
            }else if (requestCode == 20) {
                dbMasterRef = data.getStringExtra(DBMASTER_REF);
                projectRef = data.getStringExtra(PROJECT_REF);
                nupAmt = data.getStringExtra(NUP_AMT);
                projectName = data.getStringExtra(PROJECT_NAME);

                fullname = data.getStringExtra(FULLNAME);
                birthdate = data.getStringExtra("birthdate");
                mobile1 = data.getStringExtra(MOBILE);
                phone1 = data.getStringExtra(PHONE);
                email1 = data.getStringExtra(EMAIL);
                address = data.getStringExtra(ADDRESS);
                ktpid = data.getStringExtra(KTP_ID);

                fileCodeNpwp = data.getStringExtra(FILE_CODE_NPWP);
                fileCodeKtp = data.getStringExtra(FILE_CODE_KTP);

                urlDocKtp = data.getStringExtra(URL_DOC_KTP);
                urlDocNpwp = data.getStringExtra(URL_DOC_NPWP);

                ktpRef = data.getStringExtra(KTP_REF);
                npwpRef = data.getStringExtra(NPWP_REF);
            }
        }
    }

    private void handleGalleryResult19(Intent data) {
        uri = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                manufacturer = info.manufacturer;

                mTmpGalleryPicturePath = getRealPathFromURI(NupInformasiCustomerActivity.this, uri);
                if (mTmpGalleryPicturePath == null) {
                    mTmpGalleryPicturePath = getPath(selectedImage);
                }

                Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                if (!mTmpGalleryPicturePath.equals("")) {
                    if (mTmpGalleryPicturePath != null) {
                        if (type != null && type.equals("2")) {
                            Intent intent = new Intent(NupInformasiCustomerActivity.this, NupKtpActivity.class);
                            intent.putExtra(DBMASTER_REF, dbMasterRef);
                            intent.putExtra(PROJECT_REF, projectRef);
                            intent.putExtra(NUP_AMT, nupAmt);
                            intent.putExtra(PROJECT_NAME, projectName);
                            intent.putExtra(FULLNAME, editFullname.getText().toString());
                            intent.putExtra("birthdate", editBirthdate.getText().toString());
                            intent.putExtra(MOBILE, editMobile.getText().toString());
                            intent.putExtra(PHONE, editPhone.getText().toString());
                            intent.putExtra(EMAIL, editEmail.getText().toString());
                            intent.putExtra(ADDRESS, editAddress.getText().toString());
                            intent.putExtra(KTP_ID, editKtpId.getText().toString());
                            intent.putExtra(NPWP, editNpwp.getText().toString());

                            if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                                intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                            }
                            if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                                intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                            }
                            if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                                intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                            }
                            if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                                intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                            }
                            if (ktpRef.equals("1")) {
                                intent.putExtra("ktpref", "1");
                            }
                            if (npwpRef.equals("1")) {
                                intent.putExtra("npwpref", "1");
                            }
                            intent.putExtra("pathImage", mTmpGalleryPicturePath);
                            intent.putExtra("flaq", "1");
                            //i.putExtra("Image", byteArray);
                            startActivityForResult(intent, 20);
                        } else if (type != null && type.equals("3")) {
                            Intent intent = new Intent(NupInformasiCustomerActivity.this, NupNpwpActivity.class);
                            intent.putExtra(DBMASTER_REF, dbMasterRef);
                            intent.putExtra(PROJECT_REF, projectRef);
                            intent.putExtra(NUP_AMT, nupAmt);
                            intent.putExtra(PROJECT_NAME, projectName);
                            intent.putExtra(FULLNAME, editFullname.getText().toString());
                            intent.putExtra("birthdate", editBirthdate.getText().toString());
                            intent.putExtra(MOBILE, editMobile.getText().toString());
                            intent.putExtra(PHONE, editPhone.getText().toString());
                            intent.putExtra(EMAIL, editEmail.getText().toString());
                            intent.putExtra(ADDRESS, editAddress.getText().toString());
                            intent.putExtra(KTP_ID, editKtpId.getText().toString());
                            intent.putExtra(NPWP, editNpwp.getText().toString());

                            if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                                intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                            }
                            if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                                intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                            }
                            if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                                intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                            }
                            if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                                intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                            }
                            if (ktpRef.equals("1")) {
                                intent.putExtra("ktpref", "1");
                            }
                            if (npwpRef.equals("1")) {
                                intent.putExtra("npwpref", "1");
                            }
                            intent.putExtra("pathImage", mTmpGalleryPicturePath);
                            intent.putExtra("flaq", "1");
                            //i.putExtra("Image", byteArray);
                            startActivityForResult(intent, 20);
                        } else {
                            Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        try {
                            InputStream is = getContentResolver().openInputStream(selectedImage);
                            //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                            mTmpGalleryPicturePath = selectedImage.getPath();
                            Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                            if (type != null && type.equals("2")) {
                                Intent intent = new Intent(NupInformasiCustomerActivity.this, NupKtpActivity.class);
                                intent.putExtra(DBMASTER_REF, dbMasterRef);
                                intent.putExtra(PROJECT_REF, projectRef);
                                intent.putExtra(NUP_AMT, nupAmt);
                                intent.putExtra(PROJECT_NAME, projectName);
                                intent.putExtra(FULLNAME, editFullname.getText().toString());
                                intent.putExtra("birthdate", editBirthdate.getText().toString());
                                intent.putExtra(MOBILE, editMobile.getText().toString());
                                intent.putExtra(PHONE, editPhone.getText().toString());
                                intent.putExtra(EMAIL, editEmail.getText().toString());
                                intent.putExtra(ADDRESS, editAddress.getText().toString());
                                intent.putExtra(KTP_ID, editKtpId.getText().toString());
                                intent.putExtra(NPWP, editNpwp.getText().toString());

                                if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                                    intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                                }
                                if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                                    intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                                }
                                if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                                    intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                                }
                                if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                                    intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                                }
                                if (ktpRef.equals("1")) {
                                    intent.putExtra("ktpref", "1");
                                }
                                if (npwpRef.equals("1")) {
                                    intent.putExtra("npwpref", "1");
                                }
                                intent.putExtra("pathImage", file.getEncodedPath());
                                intent.putExtra("flaq", "1");
                                //i.putExtra("Image", byteArray);
                                startActivityForResult(intent, 20);
                            } else if (type != null && type.equals("3")) {
                                Intent intent = new Intent(NupInformasiCustomerActivity.this, NupNpwpActivity.class);
                                intent.putExtra(DBMASTER_REF, dbMasterRef);
                                intent.putExtra(PROJECT_REF, projectRef);
                                intent.putExtra(NUP_AMT, nupAmt);
                                intent.putExtra(PROJECT_NAME, projectName);
                                intent.putExtra(FULLNAME, editFullname.getText().toString());
                                intent.putExtra("birthdate", editBirthdate.getText().toString());
                                intent.putExtra(MOBILE, editMobile.getText().toString());
                                intent.putExtra(PHONE, editPhone.getText().toString());
                                intent.putExtra(EMAIL, editEmail.getText().toString());
                                intent.putExtra(ADDRESS, editAddress.getText().toString());
                                intent.putExtra(KTP_ID, editKtpId.getText().toString());
                                intent.putExtra(NPWP, editNpwp.getText().toString());

                                if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                                    intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                                }
                                if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                                    intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                                }
                                if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                                    intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                                }
                                if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                                    intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                                }
                                if (ktpRef.equals("1")) {
                                    intent.putExtra("ktpref", "1");
                                }
                                if (npwpRef.equals("1")) {
                                    intent.putExtra("npwpref", "1");
                                }
                                intent.putExtra("pathImage", file.getEncodedPath());
                                intent.putExtra("flaq", "1");
                                //i.putExtra("Image", byteArray);
                                startActivityForResult(intent, 20);
                            } else {
                                Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (FileNotFoundException e) {

                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("2")) {
                    Intent intent = new Intent(this, NupKtpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra("birthdate", editBirthdate.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    intent.putExtra(NPWP, editNpwp.getText().toString());

                    if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                    }
                    if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                    }
                    if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                    }
                    if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                    }
                    if (ktpRef.equals("1")) {
                        intent.putExtra("ktpref", "1");
                    }
                    if (npwpRef.equals("1")) {
                        intent.putExtra("npwpref", "1");
                    }
                    intent.putExtra("pathImage", mTmpGalleryPicturePath);
                    intent.putExtra("flaq", "1");
                    //i.putExtra("Image", byteArray);
                    startActivityForResult(intent, 20);
                } else if (type != null && type.equals("3")) {
                    Intent intent = new Intent(this, NupNpwpActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(NUP_AMT, nupAmt);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(FULLNAME, editFullname.getText().toString());
                    intent.putExtra("birthdate", editBirthdate.getText().toString());
                    intent.putExtra(MOBILE, editMobile.getText().toString());
                    intent.putExtra(PHONE, editPhone.getText().toString());
                    intent.putExtra(EMAIL, editEmail.getText().toString());
                    intent.putExtra(ADDRESS, editAddress.getText().toString());
                    intent.putExtra(KTP_ID, editKtpId.getText().toString());
                    intent.putExtra(NPWP, editNpwp.getText().toString());

                    if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                    }
                    if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                        intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                    }
                    if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                    }
                    if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                        intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                    }
                    if (ktpRef.equals("1")) {
                        intent.putExtra("ktpref", "1");
                    }
                    if (npwpRef.equals("1")) {
                        intent.putExtra("npwpref", "1");
                    }
                    intent.putExtra("pathImage", mTmpGalleryPicturePath);
                    intent.putExtra("flaq", "1");
                    //i.putExtra("Image", byteArray);
                    startActivityForResult(intent, 20);
                } else {
                    Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("2")) {
                        Intent intent = new Intent(this, NupKtpActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(FULLNAME, editFullname.getText().toString());
                        intent.putExtra("birthdate", editBirthdate.getText().toString());
                        intent.putExtra(MOBILE, editMobile.getText().toString());
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(EMAIL, editEmail.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        intent.putExtra(NPWP, editNpwp.getText().toString());

                        if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                            intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                        }
                        if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                            intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                        }
                        if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                            intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                        }
                        if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                            intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                        }
                        if (ktpRef.equals("1")) {
                            intent.putExtra("ktpref", "1");
                        }
                        if (npwpRef.equals("1")) {
                            intent.putExtra("npwpref", "1");
                        }
                        intent.putExtra("pathImage", file.getEncodedPath());
                        intent.putExtra("flaq", "1");
                        //i.putExtra("Image", byteArray);
                        startActivityForResult(intent, 20);
                    } else if (type != null && type.equals("3")) {
                        Intent intent = new Intent(this, NupNpwpActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(NUP_AMT, nupAmt);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(FULLNAME, editFullname.getText().toString());
                        intent.putExtra("birthdate", editBirthdate.getText().toString());
                        intent.putExtra(MOBILE, editMobile.getText().toString());
                        intent.putExtra(PHONE, editPhone.getText().toString());
                        intent.putExtra(EMAIL, editEmail.getText().toString());
                        intent.putExtra(ADDRESS, editAddress.getText().toString());
                        intent.putExtra(KTP_ID, editKtpId.getText().toString());
                        intent.putExtra(NPWP, editNpwp.getText().toString());

                        if (fileCodeKtpcoeg != null || fileCodeKtpcoeg.equals("")) {
                            intent.putExtra(FILE_CODE_KTP, fileCodeKtpcoeg);
                        }
                        if (fileCodeNpwpcoeg != null || fileCodeNpwpcoeg.equals("")) {
                            intent.putExtra(FILE_CODE_NPWP, fileCodeNpwpcoeg);
                        }
                        if (urlDocKtpcoeg != null || !urlDocKtpcoeg.equals("")) {
                            intent.putExtra(URL_DOC_KTP, urlDocKtpcoeg);
                        }
                        if (urlDocNpwpcoeg != null || !urlDocNpwpcoeg.equals("")) {
                            intent.putExtra(URL_DOC_NPWP, urlDocNpwpcoeg);
                        }
                        if (ktpRef.equals("1")) {
                            intent.putExtra("ktpref", "1");
                        }
                        if (npwpRef.equals("1")) {
                            intent.putExtra("npwpref", "1");
                        }
                        intent.putExtra("pathImage", file.getEncodedPath());
                        intent.putExtra("flaq", "1");
                        //i.putExtra("Image", byteArray);
                        startActivityForResult(intent, 20);
                    } else {
                        Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(NupInformasiCustomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }


    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupInformasiCustomerActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
