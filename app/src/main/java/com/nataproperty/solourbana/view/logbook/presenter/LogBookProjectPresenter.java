package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.LogBookProjectInteractor;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.ui.LogBookProjectActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LogBookProjectPresenter implements LogBookProjectInteractor {
    private LogBookProjectActivity view;
    private ServiceRetrofitProject service;

    public LogBookProjectPresenter(LogBookProjectActivity view, ServiceRetrofitProject service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetContactBankProjectSvc(String dbMasterRef, String projectRef, String memberRef, String projectCustomerStatusRef, String eventRef, String referralRef, String keyword) {
        Call<ArrayList<LogBookModel>> call = service.getAPI().GetContactBankProjectSvc(dbMasterRef, projectRef, memberRef, projectCustomerStatusRef, eventRef, referralRef,keyword);
        call.enqueue(new Callback<ArrayList<LogBookModel>>() {
            @Override
            public void onResponse(Call<ArrayList<LogBookModel>> call, Response<ArrayList<LogBookModel>> response) {
                view.showListLogBookResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<LogBookModel>> call, Throwable t) {
                view.showListLogBookFailure(t);

            }


        });
    }

}
