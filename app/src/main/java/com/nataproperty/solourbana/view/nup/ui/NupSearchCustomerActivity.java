package com.nataproperty.solourbana.view.nup.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.FixedHoloDatePickerDialog;
import com.nataproperty.solourbana.view.nup.adapter.SearchCustomerAdapter;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.nup.model.SearchCustomerModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by UserModel on 5/18/2016.
 */
public class NupSearchCustomerActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String FULLNAME = "fullname";
    public static final String BIRTHDATE = "birthdate";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String NUP_AMT = "nupAmt";
    public static final String PROJECT_NAME = "projectName";
    public static final String MEMBER_CUSTOMER_REF = "memberCustomerRef";

    SharedPreferences sharedPreferences;

    private List<SearchCustomerModel> listCostumer = new ArrayList<SearchCustomerModel>();
    private SearchCustomerAdapter adapter;

    LinearLayout headerList;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    TextView txtRegisterCustomer, txtNoData;

    EditText editFullname, editBirthdate;
    Button btnSearch, btnRegisterCustomer, btnCustomerNoName;
    ListView listView;

    private String dbMasterRef, projectRef, projectName, memberCustomerRef, nupAmt, projectDescription;

    ProgressDialog progressDialog;
    String message;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_search_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_buy_nup));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        headerList = (LinearLayout) findViewById(R.id.header_list_customer);

        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editBirthdate = (EditText) findViewById(R.id.edit_birthdate);

        txtNoData = (TextView) findViewById(R.id.txt_no_data);
        txtRegisterCustomer = (TextView) findViewById(R.id.txt_register_customer);

        btnSearch = (Button) findViewById(R.id.btn_cari);
        btnRegisterCustomer = (Button) findViewById(R.id.btn_register_customer);
        //btnCustomerNoName = (Button) findViewById(R.id.btn_no_name);

        listView = (ListView) findViewById(R.id.list_search_customer);

        final Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupAmt = intent.getStringExtra(NUP_AMT);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);
        projectName = getIntent().getStringExtra(PROJECT_NAME);

        Log.d("cek search", dbMasterRef + "+" + projectRef);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        editBirthdate.setInputType(InputType.TYPE_NULL);
        editBirthdate.setTextIsSelectable(true);
        editBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPicker();
            }
        });
        editBirthdate.setFocusable(false);

        btnSearch.setTypeface(font);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listCostumer.clear();
                requestCustomer();
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editFullname.getWindowToken(), 0);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                memberCustomerRef = listCostumer.get(position).getMemberCustomerRef();
                Log.d("memberrrr reefff", " " + memberCustomerRef);

                Intent intentInformasi = new Intent(NupSearchCustomerActivity.this, NupInformasiCustomerActivity.class);
                intentInformasi.putExtra(DBMASTER_REF, dbMasterRef);
                intentInformasi.putExtra(PROJECT_REF, projectRef);
                intentInformasi.putExtra(NUP_AMT, nupAmt);
                intentInformasi.putExtra(PROJECT_DESCRIPTION, projectDescription);
                intentInformasi.putExtra(PROJECT_NAME, projectName);
                intentInformasi.putExtra(MEMBER_CUSTOMER_REF,memberCustomerRef);
                intentInformasi.putExtra(FULLNAME, editFullname.getText().toString());
                intentInformasi.putExtra(BIRTHDATE, editBirthdate.getText().toString());
                SharedPreferences sharedPreferences = NupSearchCustomerActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", memberCustomerRef);
                editor.commit();

                intentInformasi.putExtra("status", "1");
                startActivity(intentInformasi);
            }
        });

        txtRegisterCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister = new Intent(NupSearchCustomerActivity.this, NupCreateCustomerActivity.class);
                intentRegister.putExtra(DBMASTER_REF, dbMasterRef);
                intentRegister.putExtra(PROJECT_REF, projectRef);
                intentRegister.putExtra(NUP_AMT, nupAmt);
                intentRegister.putExtra(PROJECT_NAME, projectName);
                startActivity(intentRegister);
            }
        });

        btnRegisterCustomer.setTypeface(font);
        btnRegisterCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister = new Intent(NupSearchCustomerActivity.this, NupCreateCustomerActivity.class);
                intentRegister.putExtra(DBMASTER_REF, dbMasterRef);
                intentRegister.putExtra(PROJECT_REF, projectRef);
                intentRegister.putExtra(NUP_AMT, nupAmt);
                intentRegister.putExtra(PROJECT_NAME, projectName);
                startActivity(intentRegister);
            }
        });

/*        btnCustomerNoName.setVisibility(View.GONE);
        btnCustomerNoName.setTypeface(font);
        btnCustomerNoName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentInformasi = new Intent(NupSearchCustomerActivity.this, NupInformasiCustomerActivity.class);
                intentInformasi.putExtra(DBMASTER_REF, dbMasterRef);
                intentInformasi.putExtra(PROJECT_REF, projectRef);
                intentInformasi.putExtra(NUP_AMT, nupAmt);
                intentInformasi.putExtra(PROJECT_DESCRIPTION, projectDescription);
                intentInformasi.putExtra(PROJECT_NAME, projectName);

                SharedPreferences sharedPreferences = NupSearchCustomerActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", "1");
                editor.commit();

                intentInformasi.putExtra("status", "1");
                startActivity(intentInformasi);
            }
        });*/

    }

    private void dialogPicker() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final Context themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog);
        final DatePickerDialog dialog = new FixedHoloDatePickerDialog(
                themedContext,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dialog.show();
    }

    private void updateLabel(){
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editBirthdate.setText(sdf.format(myCalendar.getTime()));
    }


    public void requestCustomer() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCostumer_v2(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result cluster", response);
                    generateListCostumer(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupSearchCustomerActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupSearchCustomerActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customerName", editFullname.getText().toString());
                params.put("birthDate", editBirthdate.getText().toString());
                params.put("dbMasterRef",dbMasterRef);
                params.put("projectRef",projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "searchCustomer");

    }

    private void generateListCostumer(JSONArray response) {
        //Log.d("Leng", String.valueOf(response.length()));
        if (response.length() == 0) {
//            headerList.setVisibility(View.GONE);
//            txtNoData.setVisibility(View.GONE);
//            listView.setEmptyView(txtNoData);
//            txtRegisterCustomer.setVisibility(View.GONE);
//            btnRegisterCustomer.setVisibility(View.VISIBLE);

            Intent intentRegister = new Intent(NupSearchCustomerActivity.this, NupInformasiCustomerActivity.class);
            intentRegister.putExtra(DBMASTER_REF, dbMasterRef);
            intentRegister.putExtra(PROJECT_REF, projectRef);
            intentRegister.putExtra(NUP_AMT, nupAmt);
            intentRegister.putExtra(PROJECT_NAME, projectName);
            intentRegister.putExtra(FULLNAME, editFullname.getText().toString());
            intentRegister.putExtra(BIRTHDATE, editBirthdate.getText().toString());
            startActivity(intentRegister);

        } else if (response.length() == 1){
            try {
                JSONObject jo = response.getJSONObject(0);
                SearchCustomerModel searchCustomer = new SearchCustomerModel();
                searchCustomer.setMemberCustomerRef(jo.getString("memberCustomerRef"));
                searchCustomer.setCustomerName(jo.getString("customerName"));
                searchCustomer.setBirthDate(jo.getString("birthDate"));
                listCostumer.add(searchCustomer);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            memberCustomerRef = listCostumer.get(0).getMemberCustomerRef();
            Log.d("memberrrr reefff", " " + memberCustomerRef);

            Intent intentInformasi = new Intent(NupSearchCustomerActivity.this, NupInformasiCustomerActivity.class);
            intentInformasi.putExtra(DBMASTER_REF, dbMasterRef);
            intentInformasi.putExtra(PROJECT_REF, projectRef);
            intentInformasi.putExtra(NUP_AMT, nupAmt);
            intentInformasi.putExtra(PROJECT_DESCRIPTION, projectDescription);
            intentInformasi.putExtra(PROJECT_NAME, projectName);
            intentInformasi.putExtra(MEMBER_CUSTOMER_REF,memberCustomerRef);
            intentInformasi.putExtra(FULLNAME, editFullname.getText().toString());
            intentInformasi.putExtra(BIRTHDATE, editBirthdate.getText().toString());
            SharedPreferences sharedPreferences = NupSearchCustomerActivity.this.
                    getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("isMemberCostumerRef", memberCustomerRef);
            editor.commit();

            intentInformasi.putExtra("status", "1");
            startActivity(intentInformasi);

        }else {
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jo = response.getJSONObject(i);
                    SearchCustomerModel searchCustomer = new SearchCustomerModel();
                    searchCustomer.setMemberCustomerRef(jo.getString("memberCustomerRef"));
                    searchCustomer.setCustomerName(jo.getString("customerName"));
                    searchCustomer.setBirthDate(jo.getString("birthDate"));
                    listCostumer.add(searchCustomer);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            headerList.setVisibility(View.VISIBLE);
            btnRegisterCustomer.setVisibility(View.GONE);
            txtNoData.setVisibility(View.GONE);
            adapter = new SearchCustomerAdapter(this, listCostumer);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupSearchCustomerActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
