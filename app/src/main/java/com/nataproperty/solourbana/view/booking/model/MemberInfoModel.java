package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class MemberInfoModel {
    int status;
    String message;
    DataMemberInfo data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataMemberInfo getData() {
        return data;
    }

    public void setData(DataMemberInfo data) {
        this.data = data;
    }
}
