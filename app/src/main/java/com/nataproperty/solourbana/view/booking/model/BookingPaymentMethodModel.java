package com.nataproperty.solourbana.view.booking.model;

import java.util.ArrayList;

/**
 * Created by Nata on 1/17/2017.
 */

public class BookingPaymentMethodModel {
    String paymentType, paymentMethod, isOnline;
    ArrayList<String> data;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }
}
