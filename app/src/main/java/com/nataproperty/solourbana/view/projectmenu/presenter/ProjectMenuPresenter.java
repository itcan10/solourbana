package com.nataproperty.solourbana.view.projectmenu.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.menuitem.model.StatusTokenGCMModel;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingSectionModel;
import com.nataproperty.solourbana.view.mynup.model.MyNupSectionModel;
import com.nataproperty.solourbana.view.project.model.CommisionStatusModel;
import com.nataproperty.solourbana.view.project.model.DetailStatusModel;
import com.nataproperty.solourbana.view.projectmenu.intractor.PresenterProjectMenuInteractor;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ProjectMenuPresenter implements PresenterProjectMenuInteractor {
    private ProjectMenuActivity view;
    private ServiceRetrofit service;
    private ServiceRetrofitProject serviceProject;

    public ProjectMenuPresenter(ProjectMenuActivity view, ServiceRetrofit service, ServiceRetrofitProject serviceProject) {
        this.view = view;
        this.service = service;
        this.serviceProject = serviceProject;
    }

   /* @Override
    public void getProjectDtl(String dbMasterRef, String projectRef) {
        Call<DetailStatusModel> call = service.getAPI().getProjectDetail(dbMasterRef,projectRef);
        call.enqueue(new Callback<DetailStatusModel>() {
            @Override
            public void onResponse(Call<DetailStatusModel> call, Response<DetailStatusModel> response) {
                view.showMenuResults(response);
            }

            @Override
            public void onFailure(Call<DetailStatusModel> call, Throwable t) {
                view.showMenuFailure(t);

            }


        });

    }*/

    @Override
    public void saveGCMTokenProjectSvc(String GCMToken, String memberRef, String projectCode) {
        Call<StatusTokenGCMModel> call = serviceProject.getAPI().saveGCMTokenProjectSvc(GCMToken, memberRef, projectCode);
        call.enqueue(new Callback<StatusTokenGCMModel>() {
            @Override
            public void onResponse(Call<StatusTokenGCMModel> call, Response<StatusTokenGCMModel> responseToken) {
                view.showSendGCMTokenResults(responseToken);
            }

            @Override
            public void onFailure(Call<StatusTokenGCMModel> call, Throwable t) {
                view.showSendGCMTokenFailure(t);

            }

        });

    }

    @Override
    public void getProjectDetailSvc(String projectCode, String memberRef) {
        Call<DetailStatusModel> call = serviceProject.getAPI().getProjectDetailSvc(projectCode, memberRef);
        call.enqueue(new Callback<DetailStatusModel>() {
            @Override
            public void onResponse(Call<DetailStatusModel> call, Response<DetailStatusModel> response) {
                view.showMenuResults(response);
            }

            @Override
            public void onFailure(Call<DetailStatusModel> call, Throwable t) {
                view.showMenuFailure(t);

            }


        });

    }

    @Override
    public void getCommision(String dbMasterRef, String projectRef, String memberRef) {
        Call<CommisionStatusModel> call = service.getAPI().getCommision(dbMasterRef, projectRef, memberRef);
        call.enqueue(new Callback<CommisionStatusModel>() {
            @Override
            public void onResponse(Call<CommisionStatusModel> call, Response<CommisionStatusModel> response) {
                view.showCommisionResults(response);
            }

            @Override
            public void onFailure(Call<CommisionStatusModel> call, Throwable t) {
                view.showCommisionFailure(t);

            }


        });

    }

    @Override
    public void getListNupSection(String memberRef, String projectCode) {
        Call<List<MyNupSectionModel>> call = serviceProject.getAPI().getListNupSection(memberRef,projectCode);
        call.enqueue(new Callback<List<MyNupSectionModel>>() {
            @Override
            public void onResponse(Call<List<MyNupSectionModel>> call, Response<List<MyNupSectionModel>> response) {
                view.showListNupSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<MyNupSectionModel>> call, Throwable t) {
                view.showListNupSectionFailure(t);

            }


        });
    }

    @Override
    public void getListBookingSection(String memberRef, String projectCode) {
        Call<List<MyBookingSectionModel>> call = serviceProject.getAPI().GetSectionMyBookingProject(memberRef,projectCode);
        call.enqueue(new Callback<List<MyBookingSectionModel>>() {
            @Override
            public void onResponse(Call<List<MyBookingSectionModel>> call, Response<List<MyBookingSectionModel>> response) {
                view.showListBookingSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<MyBookingSectionModel>> call, Throwable t) {
                view.showListBookingSectionFailure(t);

            }


        });
    }

}
