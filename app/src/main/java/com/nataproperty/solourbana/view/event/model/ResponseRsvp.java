package com.nataproperty.solourbana.view.event.model;

/**
 * Created by Nata on 3/9/2017.
 */

public class ResponseRsvp {
    int status;
    String message;
    String linkRsvp;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLinkRsvp() {
        return linkRsvp;
    }

    public void setLinkRsvp(String linkRsvp) {
        this.linkRsvp = linkRsvp;
    }
}
