package com.nataproperty.solourbana.view.mynup.presenter;

import com.nataproperty.solourbana.view.mynup.intractor.PresenterMyNupProjectPsInteractor;
import com.nataproperty.solourbana.view.mynup.model.MyNupProjectPsModel;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.mynup.ui.MyNupProjectPsActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyNupProjectPsPresenter implements PresenterMyNupProjectPsInteractor {
    private MyNupProjectPsActivity view;
    private ServiceRetrofit service;

    public MyNupProjectPsPresenter(MyNupProjectPsActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListNupProjectPs(String dbMasterRef,String projectRef,String projectPsRef,String memberRef, String projectSchemeRef) {
        Call<List<MyNupProjectPsModel>> call = service.getAPI().getListNupProjectPs(dbMasterRef,projectRef,projectPsRef,memberRef,projectSchemeRef);
        call.enqueue(new Callback<List<MyNupProjectPsModel>>() {
            @Override
            public void onResponse(Call<List<MyNupProjectPsModel>> call, Response<List<MyNupProjectPsModel>> response) {
                view.showListNupProjectPsResults(response);
            }

            @Override
            public void onFailure(Call<List<MyNupProjectPsModel>> call, Throwable t) {
                view.showListNupProjectPsFailure(t);

            }


        });
    }


}
