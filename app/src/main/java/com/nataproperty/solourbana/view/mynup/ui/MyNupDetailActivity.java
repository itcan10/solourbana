package com.nataproperty.solourbana.view.mynup.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mynup.adapter.NeedAttetionDetailAdapter;
import com.nataproperty.solourbana.view.mynup.adapter.PaidDetailAdapter;
import com.nataproperty.solourbana.view.mynup.adapter.UnPaidDetailAdapter;
import com.nataproperty.solourbana.view.mynup.adapter.UnVerifiedDetailAdapter;
import com.nataproperty.solourbana.view.mynup.model.NeedAttentionModel;
import com.nataproperty.solourbana.view.mynup.model.PaidModel;
import com.nataproperty.solourbana.view.mynup.model.UnPaidNupModel;
import com.nataproperty.solourbana.view.mynup.model.UnVerifiedModel;
import com.nataproperty.solourbana.view.nup.ui.NupPaymentMethodActivity;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyNupDetailActivity extends AppCompatActivity {
    public static final String TAG = "MyNupDetailActivity";
    public static final String DB_MASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String NUP_ORDE_CODE = "nupOrderCode";
    public static final String BUY_DATE = "buyDate";
    public static final String PAYMENT_TYPE_NAME = "paymentTypeName";
    public static final String QTY = "qty";
    public static final String PP_AMT = "ppAmt";
    public static final String TOTAL_AMT = "totalAmt";
    public static final String NUP_ORDER_DETAIL_REF = "nupOrderDetailRef";
    public static final String NUP_ORDER_STATUS = "nupOrderStatus";
    public static final String COSTUMER_NAME = "customerName";
    public static final String PROJECT_NAME = "projectName";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String NUP_NO = "nupNo";
    public static final String TOTAL = "total";
    public static final String STATUS = "status";
    public static final String NUP_AMT = "nupAmt";
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;
    private ArrayList<UnPaidNupModel> listUnPaidNup = new ArrayList<UnPaidNupModel>();
    private ArrayList<UnVerifiedModel> listUnVeriFiedNup = new ArrayList<UnVerifiedModel>();
    private ArrayList<NeedAttentionModel> listNeedAttetionNup = new ArrayList<NeedAttentionModel>();
    private ArrayList<PaidModel> listPaidNup = new ArrayList<PaidModel>();
    private UnPaidDetailAdapter adapterUnPaid;
    private NeedAttetionDetailAdapter adapterNeedAttetionPaid;
    private PaidDetailAdapter adapterPaid;
    private UnVerifiedDetailAdapter adapterUnVerifieedPaid;

    String dbMasterRef, projectRef, nupOrderCode, buyDate, paymentTypeName, qty,
            ppAmt, totalAmt, nupOrderDetailRef, nupOrderStatus,
            customerName, projectName, nupNo, status, nupOrderRef, linkPdf, linkDownload;

    TextView txtNupCode, txtByDate, txtPaymentType, txtPpQty, txtPpAmt, txtTotalAmt;

    Button btnPayment, btnDownload;

    LinearLayout linearUnPaid, linearPaid;
    Toolbar toolbar;
    TextView title;
    Typeface font, fontLight;

    ProgressDialog progressDialog;
    private String fileRef, filename, extension, urlDownload;
    MyListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_nup_detail);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DB_MASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupOrderCode = intent.getStringExtra(NUP_ORDE_CODE);
        buyDate = intent.getStringExtra(BUY_DATE);
        paymentTypeName = intent.getStringExtra(PAYMENT_TYPE_NAME);
        qty = intent.getStringExtra(QTY);
        ppAmt = intent.getStringExtra(PP_AMT);
        totalAmt = intent.getStringExtra(TOTAL_AMT);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        //status nup
        status = intent.getStringExtra(STATUS);

        /**
         * UnPaid
         */
        nupOrderDetailRef = intent.getStringExtra(NUP_ORDER_DETAIL_REF);
        nupOrderStatus = intent.getStringExtra(NUP_ORDER_STATUS);
        customerName = intent.getStringExtra(COSTUMER_NAME);
        projectName = intent.getStringExtra(PROJECT_NAME);

        /**
         * Paid
         */
        nupNo = intent.getStringExtra(NUP_NO);
        linkPdf = intent.getStringExtra("linkPdf");
        linkDownload = intent.getStringExtra("linkDownload");

        initWidget();

        Log.d("cek detail Nup", "" + nupOrderRef);

        txtNupCode.setText(nupOrderCode);
        txtByDate.setText(buyDate);
        txtPaymentType.setText(paymentTypeName);
        txtPpQty.setText(qty);
        txtPpAmt.setText(ppAmt);
        txtTotalAmt.setText(totalAmt);

        /**
         * 1 unpaid --> button payment visible
         * 2 need attetion--> button payment visible
         * 3 un-verifed
         * 4 paid
         */

        if (status.equals("1")) {
            title.setText(getResources().getString(R.string.tab_title_unpaid));
            linearUnPaid.setVisibility(View.VISIBLE);
            linearPaid.setVisibility(View.GONE);
            btnPayment.setVisibility(View.VISIBLE);
            btnDownload.setVisibility(View.GONE);

            generateListUnpaid();

            adapterUnPaid = new UnPaidDetailAdapter(this, listUnPaidNup);
            listView.setAdapter(adapterUnPaid);
            listView.setExpanded(true);

        } else if (status.equals("2")) {
            title.setText(getResources().getString(R.string.tab_title_need_attention));
            linearUnPaid.setVisibility(View.VISIBLE);
            linearPaid.setVisibility(View.GONE);
            btnPayment.setVisibility(View.VISIBLE);
            btnDownload.setVisibility(View.GONE);

            generateListNeedAttetion();

            adapterNeedAttetionPaid = new NeedAttetionDetailAdapter(this, listNeedAttetionNup);
            listView.setAdapter(adapterNeedAttetionPaid);
            listView.setExpanded(true);

        } else if (status.equals("3")) {
            title.setText(getResources().getString(R.string.tab_title_un_verified));
            linearUnPaid.setVisibility(View.GONE);
            linearPaid.setVisibility(View.VISIBLE);
            btnPayment.setVisibility(View.GONE);
            btnDownload.setVisibility(View.GONE);

            generateListUnVerified();

            adapterUnVerifieedPaid = new UnVerifiedDetailAdapter(this, listUnVeriFiedNup);
            listView.setAdapter(adapterUnVerifieedPaid);
            listView.setExpanded(true);

        } else if (status.equals("4")) {
            title.setText(getResources().getString(R.string.tab_title_paid));
            linearUnPaid.setVisibility(View.GONE);
            linearPaid.setVisibility(View.VISIBLE);
            btnPayment.setVisibility(View.GONE);
            btnDownload.setVisibility(View.VISIBLE);

            generateListPaid();

            adapterPaid = new PaidDetailAdapter(this, listPaidNup);
            listView.setAdapter(adapterPaid);
            listView.setExpanded(true);

            //requestDownload();
        }

        btnPayment.setTypeface(font);
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyNupDetailActivity.this, NupPaymentMethodActivity.class);
                intent.putExtra(DB_MASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                intent.putExtra(NUP_AMT, ppAmt);
                intent.putExtra(TOTAL, totalAmt);
                intent.putExtra("mylistNup", true);
                startActivity(intent);
            }
        });

        btnDownload.setTypeface(font);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startDownload(urlDownload);
//                Log.d(TAG, "" + urlDownload);
//                Intent intent = new Intent(MyNupDetailActivity.this, MyNupViewPDFActivity.class);
//                intent.putExtra("linkPdf", linkPdf);
//                intent.putExtra("linkDownload", linkDownload);
//                intent.putExtra("nupOrderCode", nupOrderCode);
//                startActivity(intent);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyNupDetailActivity.this);
//                alertDialogBuilder.setMessage(R.string.txt_cetak_nup_online);
                LayoutInflater inflater = MyNupDetailActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_cetak_nup_online, null);
                alertDialogBuilder.setView(dialogView);
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //startDownload(urlDownload);
                        Log.d(TAG, "" + urlDownload);
                        Intent intent = new Intent(MyNupDetailActivity.this, MyNupViewPDFActivity.class);
                        intent.putExtra("linkPdf", linkPdf);
                        intent.putExtra("linkDownload", linkDownload);
                        intent.putExtra("nupOrderCode", nupOrderCode);
                        startActivity(intent);

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        linearUnPaid = (LinearLayout) findViewById(R.id.linear_un_paid_detail);
        linearPaid = (LinearLayout) findViewById(R.id.linear_paid_detail);
        txtNupCode = (TextView) findViewById(R.id.txt_nup_code);
        txtByDate = (TextView) findViewById(R.id.txt_by_date);
        txtPaymentType = (TextView) findViewById(R.id.txt_payment_type);
        txtPpQty = (TextView) findViewById(R.id.txt_pp_qty);
        txtPpAmt = (TextView) findViewById(R.id.txt_pp_amt);
        txtTotalAmt = (TextView) findViewById(R.id.txt_total_amt);
        btnPayment = (Button) findViewById(R.id.btn_payment);
        btnDownload = (Button) findViewById(R.id.btn_download_pp);

        listView = (MyListView) findViewById(R.id.list_detail_nup);
    }

    private void generateListUnpaid() {
        String[] dataNupDetailRef = nupOrderDetailRef.split(",");
        String[] dataNupStatus = nupOrderStatus.split(",");
        String[] dataNupCostumerName = customerName.split(",");

        for (int i = 0; i < dataNupDetailRef.length; i++) {
            UnPaidNupModel UnPaid = new UnPaidNupModel();
            UnPaid.setNupOrderDetailRef(dataNupDetailRef[i]);
            UnPaid.setNupOrderStatus(dataNupStatus[i]);
            UnPaid.setCustomerName(dataNupCostumerName[i]);

            Log.d("cek generate", "" + dataNupDetailRef[i] + " " + dataNupStatus[i] + " " + dataNupCostumerName[i]);

            listUnPaidNup.add(UnPaid);
        }

    }

    private void generateListUnVerified() {
        String[] dataNupDetailRef = nupOrderDetailRef.split(",");
        String[] dataNupStatus = nupOrderStatus.split(",");
        String[] dataNupCostumerName = customerName.split(",");
        String[] dataNupNo = nupNo.split(",");

        for (int i = 0; i < dataNupDetailRef.length; i++) {
            UnVerifiedModel unVerifiedModel = new UnVerifiedModel();
            unVerifiedModel.setNupOrderDetailRef(dataNupDetailRef[i]);
            unVerifiedModel.setNupOrderStatus(dataNupStatus[i]);
            unVerifiedModel.setCustomerName(dataNupCostumerName[i]);
            unVerifiedModel.setNupNo(dataNupNo[i]);
            Log.d("cek generate", "" + dataNupDetailRef[i] + " " + dataNupStatus[i] + " " + dataNupCostumerName[i]);

            listUnVeriFiedNup.add(unVerifiedModel);
        }

    }

    private void generateListPaid() {
        String[] dataNupDetailRef = nupOrderDetailRef.split(",");
        String[] dataNupStatus = nupOrderStatus.split(",");
        String[] dataNupCostumerName = customerName.split(",");
        String[] dataNupNo = nupNo.split(",");

        for (int i = 0; i < dataNupDetailRef.length; i++) {
            PaidModel Paid = new PaidModel();
            Paid.setNupOrderDetailRef(dataNupDetailRef[i]);
            Paid.setNupOrderStatus(dataNupStatus[i]);
            Paid.setCustomerName(dataNupCostumerName[i]);
            Paid.setNupNo(dataNupNo[i]);

            Log.d("cek generate", "" + dataNupDetailRef[i] + " " + dataNupStatus[i] + " " + dataNupCostumerName[i] + " " + dataNupNo[i]);

            listPaidNup.add(Paid);
        }

    }

    private void generateListNeedAttetion() {
        String[] dataNupDetailRef = nupOrderDetailRef.split(",");
        String[] dataNupStatus = nupOrderStatus.split(",");
        String[] dataNupCostumerName = customerName.split(",");
        //String[] dataNupNo = nupNo.split(",");

        for (int i = 0; i < dataNupDetailRef.length; i++) {
            NeedAttentionModel Paid = new NeedAttentionModel();
            Paid.setNupOrderDetailRef(dataNupDetailRef[i]);
            Paid.setNupOrderStatus(dataNupStatus[i]);
            Paid.setCustomerName(dataNupCostumerName[i]);
            //Paid.setNupNo(dataNupNo[i]);

            Log.d("cek generate", "" + dataNupDetailRef[i] + " " + dataNupStatus[i] + " " + dataNupCostumerName[i]);

            listNeedAttetionNup.add(Paid);
        }

    }

    public void requestDownload() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.generateNUP(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    urlDownload = jo.getString("message");
                    filename = jo.getString("fileName");
                    extension = jo.getString("extension");
                    Log.d(TAG, "urlDownload " + urlDownload + " " + filename + " " + extension);
                    /*if (status==200){
                        startDownload(message);
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(MyNupDetailActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(MyNupDetailActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("nupOrderRef", nupOrderRef);

                Log.d("param", dbMasterRef + " - " + projectRef + " - " + nupOrderRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "insertFacility");

    }

    private void startDownload(String url) {
        if (!url.equals("")) {
            new DownloadFileAsync().execute(url);
            Log.d(TAG, "url " + url);
        } else {
            Toast.makeText(MyNupDetailActivity.this, "No input url", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Downloading file..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            int count;

            try {
                Log.d("filename", "" + filename);

                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                File wallpaperDirectory = new File(android.os.Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                // have the object build the directory structure, if needed.
                wallpaperDirectory.mkdirs();

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(wallpaperDirectory + "/" + filename.toString() + "." + extension.toString());

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;

                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
                notification();
            } catch (Exception e) {
            }
            return null;

        }

        @Override
        protected void onProgressUpdate(String... values) {
            mProgressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }
    }

    public void notification() {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        File file = new File("/" + Environment.getExternalStorageDirectory().getPath()
                + "/nataproperty/" + filename.toString() + "." + extension.toString());

        Log.d("FileUri", Environment.getExternalStorageDirectory().getPath() + "/" + filename.toString() + "." + extension.toString());
        if (extension.toLowerCase().equals("pdf")) {
            //intent.setDataAndType(Uri.fromFile(file), "document/*");
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
                Toast.makeText(MyNupDetailActivity.this, "File tidak bisa dibuka, anda harus install PDF reader", Toast.LENGTH_LONG).show();
            }
        } else if (extension.toLowerCase().equals("jpg")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        } else if (extension.toLowerCase().equals("jpeg")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        } else if (extension.toLowerCase().equals("png")) {
            intent.setDataAndType(Uri.fromFile(file), "image/*");
        } else {
            Toast.makeText(MyNupDetailActivity.this, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
        }
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyNupDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
