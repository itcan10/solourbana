package com.nataproperty.solourbana.view.loginregister.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyEditTextLatoReguler;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/6/2016.
 */
public class RegisterStepOneActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    public static final String PREF_NAME = "pref";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String MESSAGE = "message";
    public static final String NAME = "name";
    public static final String STATUS_GOOGLE_SIGN_IN = "statusGoogleSignIn";
    public static final String MEMBER_REF = "memberRef";
    public static final String BIRTHDATE = "birthDate";
    public static final String PASSWORD = "password";
    public static final String ISACTIVE = "isActive";
    private MyEditTextLatoReguler txtPhone, txtEmail, txtPassword, txtPasswordConfirm;
    private Button btnNext, btnGoogle;
    private String mobile, email, message, statusGoogleSignIn, name, phone, password, confirmPassword,
            memberRef, birthDate, acctName, acctEmail, emailGet, nameGet;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 0;
    private ProgressDialog progress_dialog;
    private GoogleSignInAccount acct;
    private int statusCekLogin, isActive;
    private CheckBox cbShowPassword;
    private Toolbar toolbar;
    private TextView title;
    private Typeface font;
    private boolean isGuest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_step_one);
        initWidget();

        Intent intent = getIntent();
        email = intent.getStringExtra(EMAIL);
        mobile = intent.getStringExtra(MOBILE);
        message = intent.getStringExtra(MESSAGE);
        name = intent.getStringExtra(NAME);
        statusGoogleSignIn = intent.getStringExtra(STATUS_GOOGLE_SIGN_IN);
        memberRef = intent.getStringExtra(MEMBER_REF);
        birthDate = intent.getStringExtra(BIRTHDATE);
        isActive = intent.getIntExtra(ISACTIVE, 0);
        password = intent.getStringExtra(PASSWORD);
        isGuest = intent.getBooleanExtra(General.GUEST, false);

        initCekIntent();
        //GUEST
        if (isGuest){
            btnGoogle.setVisibility(View.GONE);
        } else {
            //is_active untuk membedakan register dari google
            Log.d("isActive", "" + String.valueOf(isActive));
            if (isActive == 1) {
                txtEmail.setEnabled(false);
                btnGoogle.setVisibility(View.GONE);
            } else {
                btnGoogle.setVisibility(View.VISIBLE);
            }
        }


        Log.d("Register one", "" + name + "-" + email + "-" + mobile + "-" + message + "-" + statusGoogleSignIn + "-" + memberRef);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    onLoginFailed();
                    return;
                }

                phone = txtPhone.getText().toString().trim();
                email = txtEmail.getText().toString().trim();
                password = txtPassword.getText().toString().trim();
                confirmPassword = txtPasswordConfirm.getText().toString().trim();

                if (!phone.isEmpty() && !email.isEmpty() && !password.isEmpty()
                        && !confirmPassword.isEmpty() && password.equals(confirmPassword)) {

                    cekEmailRegisterSvc();

                } else {
                    Toast.makeText(getApplicationContext(), "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });

        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gSignIn();
            }
        });

        //Google+
        buidNewGoogleApiClient();
        //setBtnClickListeners();
        progress_dialog = new ProgressDialog(this);
        progress_dialog.setMessage("Register....");

        cbShowPassword.setTypeface(font);
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    txtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    txtPasswordConfirm.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    txtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    txtPasswordConfirm.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_register_one));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtPhone = (MyEditTextLatoReguler) findViewById(R.id.txtPhone);
        txtEmail = (MyEditTextLatoReguler) findViewById(R.id.txtEmail);
        txtPassword = (MyEditTextLatoReguler) findViewById(R.id.txtPassword);
        txtPasswordConfirm = (MyEditTextLatoReguler) findViewById(R.id.txtPasswordConfirm);
        cbShowPassword = (CheckBox) findViewById(R.id.cbShowPassword);
        btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnNext = (Button) findViewById(R.id.btnNext);

        btnGoogle.setTypeface(font);
        btnNext.setTypeface(font);
    }

    private void initCekIntent() {
        if (email == null) {
            email = "";
        } else {
            txtEmail.setText(email);
        }

        if (mobile == null) {
            mobile = "";
        } else {
            txtPhone.setText(mobile);
        }

        if (password == null) {
            password = "";
        } else {
            txtPassword.setText(password);
            txtPasswordConfirm.setText(password);
        }

        if (message == null) {
            message = "";
        }

        if (name == null) {
            name = "";
        }

        if (memberRef == null) {
            statusGoogleSignIn = "200";
        }

        if (birthDate == null) {
            birthDate = "";
        }

    }

    public boolean validate() {
        boolean valid = true;

        String phone = txtPhone.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String passwordConfirm = txtPasswordConfirm.getText().toString().trim();

        if (phone.isEmpty()) {
            txtPhone.setError("enter a mobile phone");
            txtPhone.setFocusableInTouchMode(true);
            txtPhone.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtPhone, InputMethodManager.SHOW_IMPLICIT);
            valid = false;
        } else {
            txtPhone.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txtEmail.setError("enter a valid email address");
            txtEmail.setFocusableInTouchMode(true);
            txtEmail.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtEmail, InputMethodManager.SHOW_IMPLICIT);
            valid = false;
        } else {
            txtEmail.setError(null);
        }

        if (password.isEmpty()) {
            txtPassword.setError("enter a password");
            txtPassword.setFocusableInTouchMode(true);
            txtPassword.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtPassword, InputMethodManager.SHOW_IMPLICIT);
            valid = false;
        } else {
            txtPassword.setError(null);
        }

        if (passwordConfirm.isEmpty()) {
            txtPasswordConfirm.setError("enter a password");
            txtPasswordConfirm.setFocusableInTouchMode(true);
            txtPasswordConfirm.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtPasswordConfirm, InputMethodManager.SHOW_IMPLICIT);
            valid = false;
        } else if (!password.equals(passwordConfirm)) {
            txtPasswordConfirm.setError("password don't match");
            txtPasswordConfirm.setFocusableInTouchMode(true);
            txtPasswordConfirm.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(txtPasswordConfirm, InputMethodManager.SHOW_IMPLICIT);
            valid = false;
        } else {
            txtPasswordConfirm.setError(null);
        }

        return valid;
    }

    public void onLoginFailed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    public void cekEmailRegisterSvc() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.cekEmailRegisterSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result register", response);
                    statusCekLogin = jo.getInt("status");
                    message = jo.getString("message");

                    if (statusCekLogin == 200) {
                        Intent intent = new Intent(RegisterStepOneActivity.this, RegisterStepTwoActivity.class);
                        intent.putExtra("phone", phone);
                        intent.putExtra("email", email);
                        intent.putExtra("password", password);
                        intent.putExtra("confirmPassword", confirmPassword);
                        intent.putExtra(STATUS_GOOGLE_SIGN_IN, statusGoogleSignIn);
                        intent.putExtra(NAME, name);
                        intent.putExtra(BIRTHDATE, birthDate);
                        intent.putExtra(ISACTIVE, 1);
                        intent.putExtra(General.GUEST,isGuest);
                        startActivity(intent);
                    } else if (statusCekLogin == 201) {
                        txtEmail.setError(message);

                        Toast.makeText(RegisterStepOneActivity.this, message, Toast.LENGTH_LONG).show();
                    } else if (statusCekLogin == 206) {
                        cekEmailCostumerPopup();

                    } else {
                        Toast.makeText(RegisterStepOneActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterStepOneActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterStepOneActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", txtEmail.getText().toString().trim());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "cekEmail");

    }

    private void gSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
        progress_dialog.show();
    }

    /*Google+*/
    private void buidNewGoogleApiClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail() //Remove these below according to your needs
                .requestProfile()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {

                progress_dialog.dismiss();

            }
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            getSignInResult(result);
        }
    }

    private void getSignInResult(GoogleSignInResult result) {
        if (result.isSuccess() && result != null) {
            acct = result.getSignInAccount();
            acctName = acct.getDisplayName();
            acctEmail = acct.getEmail();

            if (acctName == null) {
                acctName = "Mr/Mrs VIP";
            }
            Log.d("acctEmail",acctEmail+"");
            registerGoogleSignIn(acctEmail);
            progress_dialog.dismiss();
        } else {
            Toast.makeText(getApplicationContext(), "Get Data failed", Toast.LENGTH_LONG).show();
        }

    }

    public void registerGoogleSignIn(final String acctEmail) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.signInGoogle(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    final String message = jo.getString("message");
                    final String memberRef = jo.getString("memberRef");
                    final String memberType = jo.getString("memberType");
                    if (status == 200) {
                        emailGet = jo.getJSONObject("data").getString("email1");
                        nameGet = jo.getJSONObject("data").getString("name");
                        SharedPreferences sharedPreferences = RegisterStepOneActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", emailGet);
                        editor.putString("isName", nameGet);
                        editor.putString("isMemberType", memberType);
                        editor.commit();
                        Intent intent = new Intent(RegisterStepOneActivity.this, LaunchActivity.class);
                        finish();
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        /*201 = new*/
                    } else if (status == 201) {
                        if (!mGoogleApiClient.isConnected()) {
                            mGoogleApiClient.connect();
                            googleSign();
                        } else {
                            googleSign();
                        }
//                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                                new ResultCallback<Status>() {
//                                    @Override
//                                    public void onResult(Status status) {
//                                        txtEmail.setText(acctEmail);
//                                        txtEmail.setEnabled(false);
//                                        statusGoogleSignIn = "201";
//                                        btnGoogle.setVisibility(View.GONE);
//                                    }
//                                });
                         /*202 = Register dari calculator*/
                    } else if (status == 300) {
                        name = jo.getJSONObject("data").getString("name");
                        mobile = jo.getJSONObject("data").getString("hP1");
                        birthDate = jo.getJSONObject("data").getString("birthDate");
                        final String password = jo.getJSONObject("data").getString("key");
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        txtEmail.setText(acctEmail);
                                        txtEmail.setEnabled(false);

                                        if (mobile == null) {
                                            mobile = "";
                                        } else {
                                            txtPhone.setText(mobile);
                                        }

                                        txtPassword.setText(password);
                                        txtPasswordConfirm.setText(password);
                                        statusGoogleSignIn = "202";
                                        btnGoogle.setVisibility(View.GONE);
                                    }
                                });
                         /*locked*/
                    } else if (status == 400) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Toast.makeText(RegisterStepOneActivity.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else if (status == 206) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        cekEmailCostumerPopup();
                                    }
                                });
                    } else {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {

                                    }
                                });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterStepOneActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterStepOneActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", acctEmail);
                params.put("projectCode", General.projectCode + "#" + General.projectCodeSingleProject);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productImage");

    }

    private void googleSign() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        txtEmail.setText(acctEmail);
                        txtEmail.setEnabled(false);
                        statusGoogleSignIn = "201";
                        btnGoogle.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private void cekEmailCostumerPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegisterStepOneActivity.this);
        LayoutInflater inflater = RegisterStepOneActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_activate_email, null);
        dialogBuilder.setView(dialogView);

        final TextView txtActivate = (TextView) dialogView.findViewById(R.id.txt_activate);
        txtActivate.setText(getResources().getString(R.string.if_email_costumer));

        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass


            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

}
