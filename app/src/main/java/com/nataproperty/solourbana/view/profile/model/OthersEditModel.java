package com.nataproperty.solourbana.view.profile.model;

import java.util.ArrayList;

/**
 * Created by Nata on 3/9/2017.
 */

public class OthersEditModel {
    int status;
    String message;
    ArrayList<NationModel> nation;
    ArrayList<TypeModel> type;
    ArrayList<SexModel> sex;
    ArrayList<RegionModel> religion;
    ArrayList<OccupationModel> occupation;
    ArrayList<MaritalStatusModel> marital;
    ArrayList<JobTitleModel> jobTitle;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<NationModel> getNation() {
        return nation;
    }

    public void setNation(ArrayList<NationModel> nation) {
        this.nation = nation;
    }

    public ArrayList<TypeModel> getType() {
        return type;
    }

    public void setType(ArrayList<TypeModel> type) {
        this.type = type;
    }

    public ArrayList<SexModel> getSex() {
        return sex;
    }

    public void setSex(ArrayList<SexModel> sex) {
        this.sex = sex;
    }

    public ArrayList<RegionModel> getReligion() {
        return religion;
    }

    public void setReligion(ArrayList<RegionModel> religion) {
        this.religion = religion;
    }

    public ArrayList<OccupationModel> getOccupation() {
        return occupation;
    }

    public void setOccupation(ArrayList<OccupationModel> occupation) {
        this.occupation = occupation;
    }

    public ArrayList<MaritalStatusModel> getMarital() {
        return marital;
    }

    public void setMarital(ArrayList<MaritalStatusModel> marital) {
        this.marital = marital;
    }

    public ArrayList<JobTitleModel> getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(ArrayList<JobTitleModel> jobTitle) {
        this.jobTitle = jobTitle;
    }
}
