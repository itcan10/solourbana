package com.nataproperty.solourbana.view.kpr.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nata on 3/27/2018.
 */

public class DataKalkulator {
    String bankName;
    String option;
    String term;
    String installment;
    @SerializedName("float")
    String floatStr;
    String fixYear;
    String status;
    String interest;
    String floatInterest;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getFloatStr() {
        return floatStr;
    }

    public void setFloatStr(String floatStr) {
        this.floatStr = floatStr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFixYear() {
        return fixYear;
    }

    public void setFixYear(String fixYear) {
        this.fixYear = fixYear;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getFloatInterest() {
        return floatInterest;
    }

    public void setFloatInterest(String floatInterest) {
        this.floatInterest = floatInterest;
    }
}
