package com.nataproperty.solourbana.view.mynup.model;

/**
 * Created by UserModel on 6/14/2016.
 */
public class MyNupModel {
    String nupType,nupCount;

    public String getNupType() {
        return nupType;
    }

    public void setNupType(String nupType) {
        this.nupType = nupType;
    }

    public String getNupCount() {
        return nupCount;
    }

    public void setNupCount(String nupCount) {
        this.nupCount = nupCount;
    }
}
