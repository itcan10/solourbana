package com.nataproperty.solourbana.view.event.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.event.model.MyTicketModel;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class MyTikcetAdapter extends BaseAdapter  {
    public static final String TAG = "MyTikcetAdapter" ;

    public static final String PREF_NAME = "pref" ;

    private Context context;
    private ArrayList<MyTicketModel> list;

    private ListMyTicketHolder holder;

    public MyTikcetAdapter(Context context, ArrayList<MyTicketModel> list) {
        this.context = context;
        this.list = list;

    }

    SharedPreferences sharedPreferences;
    private boolean state;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state =  sharedPreferences.getBoolean("isLogin", false);

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_my_ticket,null);
            holder = new ListMyTicketHolder();
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.date = (TextView) convertView.findViewById(R.id.txt_date);
            holder.count = (TextView) convertView.findViewById(R.id.txt_count);

            convertView.setTag(holder);
        }else{
            holder = (ListMyTicketHolder) convertView.getTag();
        }

        MyTicketModel myTicketModel = list.get(position);
        holder.title.setText(myTicketModel.getTitle());
        holder.date.setText(myTicketModel.getEventScheduleDate());
        holder.count.setText(myTicketModel.getCountRsvp()+" Tiket");

        return convertView;
    }


    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListMyTicketHolder {
        TextView title,date,count;
    }

}
