package com.nataproperty.solourbana.view.project.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.SessionManager;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.EndlessScrollListener;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.news.adapter.NewsAdapter;
import com.nataproperty.solourbana.view.news.model.NewsModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewsProjectActivity extends AppCompatActivity {
    public static final String TAG = "NewsActivity";

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<NewsModel> listNews = new ArrayList<NewsModel>();
    private NewsAdapter adapter;

    String dbMasterRef, projectRef, txtTitle, publishDate, developerName, content, contentRef, linkDetail,
            imgSetting, projectName, urlApi, memberRef;

    String keyword = "";
    private int page = 1;
    private int countTotal;

    private LinearLayout linearLayoutNoData;
    private TextView txtNoData;
    boolean isPromo;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sessionManager = new SessionManager(this);

        Display display = getWindowManager().getDefaultDisplay();
        listView = (ListView) findViewById(R.id.list_news);
        linearLayoutNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        txtNoData = (TextView) findViewById(R.id.txt_no_data);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(General.DBMASTER_REF);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        isPromo = intent.getBooleanExtra(General.PROJECT_PROMO, false);

        adapter = new NewsAdapter(this, listNews, display);
        listView.setAdapter(adapter);

        memberRef = sessionManager.getStringFromSP("isMemberRef");

        //jika dia promo
        if (isPromo) {
            urlApi = WebService.getListPromoProjectSvc();
            requestList(page);
            title.setText(getResources().getString(R.string.title_promo));
        } else {
            urlApi = WebService.getNewsProject();
            requestList(page);
            title.setText(getResources().getString(R.string.title_news));
        }

        listView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (isPromo) {
                    urlApi = WebService.getListPromoProjectSvc();
                    requestList(page);
                } else {
                    urlApi = WebService.getNewsProject();
                    requestList(page);
                }

            }
        });

    }


    public void requestList(final int page) {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST, urlApi
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    countTotal = jsonObject.getInt("totalPage");

                    if (countTotal != 0) {
                        if (page <= countTotal) {
                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONArray("data").toString());
                            Log.d("jsonArray", "" + jsonArray.length());
                            generateListNews(jsonArray);
                            linearLayoutNoData.setVisibility(View.GONE);
                        }
                    } else {
                        linearLayoutNoData.setVisibility(View.VISIBLE);
                        if (isPromo){
                            txtNoData.setText("*No promo for this project");
                        } else {
                            txtNoData.setText("*No news for this project");
                        }

                    }


                    Log.d(TAG, "count=" + page + " " + countTotal);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NewsProjectActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NewsProjectActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("keyword", "");
                params.put("pageNo", String.valueOf(page));
                params.put("projectCode", General.projectCode);
                if (isPromo){
                    params.put("memberRef", memberRef);
                }


                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "newsProject");

    }

    private void generateListNews(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                NewsModel News = new NewsModel();
                News.setDbMasterRef(jo.getString("dbMasterRef"));
                News.setProjectRef(jo.getString("projectRef"));
                News.setTitle(jo.getString("title"));
                News.setContentRef(jo.getString("contentRef"));
                News.setPublishDate(jo.getString("publishDate"));
                News.setDeveloperName(jo.getString("developerName"));
                News.setImageHeader(jo.getString("imageHeader"));
                News.setSynopsis(jo.getString("synopsis"));
                News.setContent(jo.getString("content"));
                News.setLinkDetail(jo.getString("linkDetail"));
                News.setImgSetting(jo.getString("imgSetting"));
                News.setLinkShare(jo.getString("linkShare"));
                listNews.add(News);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NewsProjectActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);

    }
}
