package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by "dwist14"
 * on Oct 10/16/2017 16:36.
 * Project : Adhipersadaproperty
 */
public class FloorPlanRespone {
    int status;
    String message;
    FloorPlanData data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FloorPlanData getData() {
        return data;
    }

    public void setData(FloorPlanData data) {
        this.data = data;
    }
}
