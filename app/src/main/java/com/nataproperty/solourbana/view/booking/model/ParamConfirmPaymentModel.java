package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by nata on 7/19/2018.
 */

public class ParamConfirmPaymentModel {
    String dbMasterRef, projectRef, bookingRef, termRef, memberRef, image, fileName, va1, va2;

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getBookingRef() {
        return bookingRef;
    }

    public void setBookingRef(String bookingRef) {
        this.bookingRef = bookingRef;
    }

    public String getTermRef() {
        return termRef;
    }

    public void setTermRef(String termRef) {
        this.termRef = termRef;
    }

    public String getMemberRef() {
        return memberRef;
    }

    public void setMemberRef(String memberRef) {
        this.memberRef = memberRef;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getVa1() {
        return va1;
    }

    public void setVa1(String va1) {
        this.va1 = va1;
    }

    public String getVa2() {
        return va2;
    }

    public void setVa2(String va2) {
        this.va2 = va2;
    }
}
