package com.nataproperty.solourbana.view.loginregister.model;

/**
 * Created by User on 5/11/2016.
 */
public class CompanyModel {
    String agencyCompanyRef;
    String companyName;
    String principleName;
    String companyPhone;
    String companyHP;
    String address;

    public String getAgencyCompanyRef() {
        return agencyCompanyRef;
    }

    public void setAgencyCompanyRef(String agencyCompanyRef) {
        this.agencyCompanyRef = agencyCompanyRef;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPrincipleName() {
        return principleName;
    }

    public void setPrincipleName(String principleName) {
        this.principleName = principleName;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyHP() {
        return companyHP;
    }

    public void setCompanyHP(String companyHP) {
        this.companyHP = companyHP;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
