package com.nataproperty.solourbana.view.logbook.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.adapter.LogBookAdapter;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.presenter.SearchLogBookPresenter;

import java.util.ArrayList;

import retrofit2.Response;

public class SearchLogBookActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private SearchLogBookPresenter presenter;
    ListView listView;
    LogBookAdapter adapter;
    ArrayList<LogBookModel> list = new ArrayList<>();
    Toolbar toolbar;
    TextView title, noData;
    Typeface font;
    SharedPreferences sharedPreferences;
    FloatingActionButton fab;
    String memberRef, keyword = "";
    ProgressDialog progressDialog;
    private EditText edtSearch;
    private TextView txtClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_log_book);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new SearchLogBookPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        edtSearch.setHint("Search..");
        edtSearch.setHintTextColor(getResources().getColor(R.color.colorText));
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edtSearch.getText().toString().trim().equals("")) {
                        Toast.makeText(getApplicationContext(), "Pencarian Tidak Boleh Kosong", Toast.LENGTH_LONG).show();
                    } else {
                        if (list.size() != 0) {
                            list.clear();
                        }
                        keyword = edtSearch.getText().toString();
                        requestLogBook();
                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                        return true;
                    }
                }
                return false;
            }
        });

        txtClose.setOnClickListener(this);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        listView = (ListView) findViewById(R.id.list_log_book);
        noData = (TextView) findViewById(R.id.txt_no_data);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        txtClose = (TextView) findViewById(R.id.txt_close);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_close:
                onBackPressed();
                break;

        }
    }

    private void requestLogBook() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetContactBankSvc(memberRef, General.customerStatusNewRef, keyword, General.projectCode);
    }

    public void showListLogBookResults(Response<ArrayList<LogBookModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            list = response.body();
            if (list.size() > 0) {
                initAdapter();
                noData.setVisibility(View.GONE);
            } else {
                noData.setVisibility(View.VISIBLE);
                listView.setEmptyView(noData);
            }

        }
    }

    public void showListLogBookFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new LogBookAdapter(this, list);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
