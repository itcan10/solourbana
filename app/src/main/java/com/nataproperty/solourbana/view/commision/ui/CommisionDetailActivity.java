package com.nataproperty.solourbana.view.commision.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.commision.adapter.CommisionDetailAdapter;
import com.nataproperty.solourbana.view.commision.model.CommisionDetailModel;
import com.nataproperty.solourbana.view.commision.presenter.CommisionDetailPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class CommisionDetailActivity extends AppCompatActivity {
    private static final String EXTRA_RX = "EXTRA_RX";

    Toolbar toolbar;
    TextView title;
    Typeface font;

    ProgressDialog progressDialog;
    CommisionDetailPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    String memberRef, dbMasterRef, projectRef, projectPsRef, bookingRef;

    MyListView listView;
    CommisionDetailAdapter adapter;
    List<CommisionDetailModel> listCommisionDetail = new ArrayList<>();

    String bookingCode, customerName, projectName, clusterName, blockName, unitName, commisionStatusName,
            comPriceAmt, custPaid, totalAmtCommision, countCommision;
    TextView txtBookingCode, txtCustomerName, txtProjectName, txtClusterName, txtBlockName, txtUnitName,
            txtCommisionStatusName, txtComPriceAmt, txtCustPaid, txtTotalAmtCommision;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commision_detail);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new CommisionDetailPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        projectPsRef = intent.getStringExtra("projectPsRef");
        bookingRef = intent.getStringExtra("bookingRef");
        bookingCode = intent.getStringExtra("bookingCode");
        customerName = intent.getStringExtra("customerName");
        projectName = intent.getStringExtra("projectName");
        clusterName = intent.getStringExtra("clusterName");
        blockName = intent.getStringExtra("blockName");
        unitName = intent.getStringExtra("unitName");
        commisionStatusName = intent.getStringExtra("commisionStatusName");
        comPriceAmt = intent.getStringExtra("comPriceAmt");
        custPaid = intent.getStringExtra("custPaid");
        totalAmtCommision = intent.getStringExtra("totalAmtCommision");
        countCommision = intent.getStringExtra("countCommision");

        txtBookingCode.setText(bookingCode);
        txtCustomerName.setText(customerName);
        txtProjectName.setText(projectName);
        txtClusterName.setText(clusterName);
        txtBlockName.setText(blockName);
        txtUnitName.setText(unitName);
        if (countCommision.equals("0")) {
            txtCommisionStatusName.setText(commisionStatusName);
        } else {
            txtCommisionStatusName.setText(commisionStatusName + " (" + countCommision + ")");
        }
        txtComPriceAmt.setText(comPriceAmt);
        txtCustPaid.setText(custPaid + " %");
        txtTotalAmtCommision.setText(totalAmtCommision);

        requestDetailCommision();
    }

    private void requestDetailCommision() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetListCommissionDetailProjectSvc(dbMasterRef, projectRef, bookingRef, projectPsRef);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Pembayaran Komisi");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (MyListView) findViewById(R.id.list_commision_detail);
        txtBookingCode = (TextView) findViewById(R.id.txt_booking_code);
        txtProjectName = (TextView) findViewById(R.id.txt_property);
        txtClusterName = (TextView) findViewById(R.id.txt_cluster_name);
        txtBlockName = (TextView) findViewById(R.id.txt_block_name);
        txtUnitName = (TextView) findViewById(R.id.txt_unit_no);
        txtComPriceAmt = (TextView) findViewById(R.id.txt_comPriceAmt);
        txtCommisionStatusName = (TextView) findViewById(R.id.txt_commisionStatusName);
        txtTotalAmtCommision = (TextView) findViewById(R.id.txt_totalAmtCommision);
        txtCustPaid = (TextView) findViewById(R.id.txt_custPaid);
        txtCustomerName = (TextView) findViewById(R.id.txt_customer_name);
    }

    public void showListCommisionDetailResults(Response<List<CommisionDetailModel>> response) {
        progressDialog.dismiss();
        listCommisionDetail = response.body();
        initAdapter();
    }

    public void showListCommisionnDetailFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new CommisionDetailAdapter(CommisionDetailActivity.this, listCommisionDetail);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
