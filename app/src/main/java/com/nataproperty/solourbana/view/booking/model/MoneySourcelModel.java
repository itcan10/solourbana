package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by User on 11/3/2016.
 */
public class MoneySourcelModel {
    String moneySource;
    String moneySourceName;

    public String getMoneySource() {
        return moneySource;
    }

    public void setMoneySource(String moneySource) {
        this.moneySource = moneySource;
    }

    public String getMoneySourceName() {
        return moneySourceName;
    }

    public void setMoneySourceName(String moneySourceName) {
        this.moneySourceName = moneySourceName;
    }
}
