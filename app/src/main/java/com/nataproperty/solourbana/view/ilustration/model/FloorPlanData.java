package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by "dwist14"
 * on Oct 10/16/2017 16:38.
 * Project : Adhipersadaproperty
 */
public class FloorPlanData {
    String unitStatus;

    public String getUnitStatus() {
        return unitStatus;
    }

    public void setUnitStatus(String unitStatus) {
        this.unitStatus = unitStatus;
    }
}
