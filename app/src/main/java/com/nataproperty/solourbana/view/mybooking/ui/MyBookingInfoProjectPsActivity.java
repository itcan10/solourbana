package com.nataproperty.solourbana.view.mybooking.ui;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.booking.model.MemberInfoModel;
import com.nataproperty.solourbana.view.mybooking.model.BookingPaymentInfoModel;
import com.nataproperty.solourbana.view.mybooking.presenter.MyBookingInfoProjectPsPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import retrofit2.Response;

/**
 * Created by UserModel on 8/15/2016.
 */
public class MyBookingInfoProjectPsActivity extends AppCompatActivity {

    public static final String PREF_NAME = "pref";
    public static final String TAG = "MyBookingInfoProjectPs";

    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    private static final String EXTRA_RX = "EXTRA_RX";
    Toolbar toolbar;
    TextView title;
    Typeface font;
    MyBookingInfoProjectPsPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    SharedPreferences sharedPreferences;

    TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    TextView txtFullname, txtEmail, txtMobile, txtPhone, txtAddress, txtKtpId, txtQty;
    TextView txtPaymentDate, txtPaymentType, txtPaymentAmt, txtAccountBank, txtAccountName, txtAccountNomor;
    TextView txtProjectName, txtCostumerName, txtCostumerMobile, txtCostumerPhone, txtCostumerAddress, txtCostumerEmail, txtMemberEmail;
    String paymentDate, paymentType, paymentAmt, accountBank, accountName, accountNomor, status = "";

    ImageView imgKtp, imgNpwp;

    String bookingRef, customerName, mobile, phone, email, alamat, ktpId;
    String memberCostumerRef, costumerName, costumerMobile, costumerPhone, costumerEmail, costumerAddress, linkDownload;
    String ktpid, ktpRef, npwpRef;
    String projectBookingRef, dbMasterRef, projectRef, isPayment;
    int waitingVerification;

    LinearLayout linearLayoutWaiting, linearPaymentInfo;
    Button btnDownload, btnDownloadOR, btnIlustrasi;

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;

    private long enqueue;
    private DownloadManager dm;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_info);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyBookingInfoProjectPsPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCostumerRef = sharedPreferences.getString("isMemberCostumerRef", null);
        Intent intent = getIntent();
        linkDownload = intent.getStringExtra("linkDownload");
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        isPayment = intent.getStringExtra("isPayment");
        customerName = intent.getStringExtra("customerName");
        mobile = intent.getStringExtra("mobile");
        phone = intent.getStringExtra("phone");
        alamat = intent.getStringExtra("alamat");
        email = intent.getStringExtra("email");
        ktpId = intent.getStringExtra("ktpId");
        initWidget();

        Log.d(TAG, "param " + memberCostumerRef + " " + dbMasterRef + " " + projectRef + " " + bookingRef + " " + projectBookingRef);
        Log.d(TAG, "ktp " + ktpId);
        Log.d(TAG, "isPayment " + isPayment);

        Glide.with(MyBookingInfoProjectPsActivity.this)
                .load(WebService.getProjectKtp() + memberCostumerRef + "&tipe=" + WebService.projectKtpType + "&dmr=" + dbMasterRef + "&pr=" + projectRef)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(imgKtp);

        Log.d(TAG, "doc ktp " + WebService.getProjectKtp() + memberCostumerRef + "&tipe=" + WebService.projectKtpType + "&dmr=" + dbMasterRef + "&pr=" + projectRef);

        Glide.with(MyBookingInfoProjectPsActivity.this)
                .load(WebService.getProjectNpwp() + memberCostumerRef + "&tipe=" + WebService.projectNPWPType + "&dmr=" + dbMasterRef + "&pr=" + projectRef)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(imgNpwp);

        Log.d(TAG, "doc npwp " + WebService.getProjectKtp() + memberCostumerRef + "&tipe=" + WebService.projectNPWPType + "&dmr=" + dbMasterRef + "&pr=" + projectRef);

        if (isPayment.equals("0")) {
            linearPaymentInfo.setVisibility(View.GONE);
        } else {
            linearPaymentInfo.setVisibility(View.VISIBLE);
        }

        requestMyBookingInfo();
        //requestMemberInfo();
        requestMyBookingDetail();

        btnIlustrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyBookingInfoProjectPsActivity.this, MyBookingDetailActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                intent.putExtra("payTerm", txtPaymentTerms.getText().toString());
                intent.putExtra("total", txtNetPrice.getText().toString());
                startActivity(intent);
            }
        });
    }

    private void initWidget(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        txtPaymentDate = (TextView) findViewById(R.id.txt_payment_date);
        txtPaymentType = (TextView) findViewById(R.id.txt_payment_type);
        txtPaymentAmt = (TextView) findViewById(R.id.txt_payment_amt);
        txtAccountBank = (TextView) findViewById(R.id.txt_account_bank);
        txtAccountName = (TextView) findViewById(R.id.txt_account_name);
        txtAccountNomor = (TextView) findViewById(R.id.txt_account_nomor);

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * informasion customer
         */
        txtFullname = (TextView) findViewById(R.id.txt_fullname);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtMobile = (TextView) findViewById(R.id.txt_mobile);
        txtPhone = (TextView) findViewById(R.id.txt_phone);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtKtpId = (TextView) findViewById(R.id.txt_ktp_id);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        txtFullname.setText(customerName);
        txtMobile.setText(mobile);
        txtPhone.setText(phone);
        txtEmail.setText(email);
        txtAddress.setText(alamat);
        txtKtpId.setText(ktpId);

        linearLayoutWaiting = (LinearLayout) findViewById(R.id.linear_waiting_verification);
        btnIlustrasi = (Button) findViewById(R.id.btn_ilustration);
        btnDownload = (Button) findViewById(R.id.btn_download);
        btnDownloadOR = (Button) findViewById(R.id.btn_download_or);

        linearPaymentInfo = (LinearLayout) findViewById(R.id.linear_payment_info);

        linearLayoutWaiting.setVisibility(View.GONE);
        btnDownload.setVisibility(View.GONE);
        btnDownloadOR.setVisibility(View.GONE);
        btnIlustrasi.setTypeface(font);
    }

    private void requestMyBookingDetail() {
        presenter.getBookingDetailSvc(dbMasterRef, projectRef, projectBookingRef);
    }

    public void showBookingInfoResults(Response<DetailBooking> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            String bookDate = response.body().getBookingInfo().getBookDate();
            String salesReferral = response.body().getBookingInfo().getSalesReferral();
            String salesEvent = response.body().getBookingInfo().getSalesEvent();
            String purpose = response.body().getBookingInfo().getPurpose();
            String salesLocation = response.body().getBookingInfo().getSalesLocation();
            String category = response.body().getUnitInfo().getCategory();
            String detail = response.body().getUnitInfo().getDetail();
            String cluster = response.body().getUnitInfo().getCluster();
            String block = response.body().getUnitInfo().getBlock();
            String product = response.body().getUnitInfo().getProduct();
            String unitName = response.body().getUnitInfo().getUnitName();
            String projectName = response.body().getUnitInfo().getProjectName();
            String area = response.body().getUnitInfo().getArea();
            String total = response.body().getUnitInfo().getTotal();
            String payTerm = response.body().getUnitInfo().getPayTerm();

            txtPropertyName.setText(projectName);
            txtCategoryType.setText(category + " | " + cluster);
            txtProduct.setText(product);
            txtUnitNo.setText(block + " - " + unitName);
            txtArea.setText(Html.fromHtml(area));

            txtPaymentTerms.setText(payTerm);
            txtNetPrice.setText(total);

            Log.d("price", payTerm + " " + total);

        } else {
            Log.d(TAG, "get error");
        }
    }

    public void showBookingInfoFailure(Throwable t) {

    }

    private void requestMyBookingInfo() {
        presenter.getBookingPaymentInfoSvc(bookingRef);
    }

    public void showBookingPaymentInfoResults(Response<BookingPaymentInfoModel> response) {
        paymentType = response.body().getPaymentTypeName();
        paymentDate = response.body().getPaymentDate();
        paymentAmt = response.body().getPaymentAmt();
        accountBank = response.body().getBankName();
        accountName = response.body().getBankAccName();
        accountNomor = response.body().getBankAccNo();

        txtPaymentType.setText(paymentType);
        txtPaymentDate.setText(paymentDate);
        txtPaymentAmt.setText(paymentAmt);
        txtAccountBank.setText(accountBank);
        txtAccountName.setText(accountName);
        txtAccountNomor.setText(accountNomor);
    }

    public void showMemberInfoResults(Response<MemberInfoModel> response) {

    }

    public void showMemberInfoFailure(Throwable t) {

    }

    public void showBookingPaymentInfoFailure(Throwable t) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingInfoProjectPsActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
