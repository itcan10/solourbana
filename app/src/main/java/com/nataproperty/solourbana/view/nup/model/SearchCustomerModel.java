package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by UserModel on 5/19/2016.
 */
public class SearchCustomerModel {
    String memberCustomerRef,customerName,birthDate;

    public String getMemberCustomerRef() {
        return memberCustomerRef;
    }

    public void setMemberCustomerRef(String memberCustomerRef) {
        this.memberCustomerRef = memberCustomerRef;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
