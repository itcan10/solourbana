package com.nataproperty.solourbana.view.logbook.model;

import java.util.ArrayList;

/**
 * Created by Nata on 2/9/2017.
 */

public class ContactProjectModel {
    ArrayList<ListEvent>   listEvent;
    ArrayList<ListReferral>  listReferral;

    public ArrayList<ListEvent> getListEvent() {
        return listEvent;
    }

    public void setListEvent(ArrayList<ListEvent> listEvent) {
        this.listEvent = listEvent;
    }

    public ArrayList<ListReferral> getListReferral() {
        return listReferral;
    }

    public void setListReferral(ArrayList<ListReferral> listReferral) {
        this.listReferral = listReferral;
    }
}
