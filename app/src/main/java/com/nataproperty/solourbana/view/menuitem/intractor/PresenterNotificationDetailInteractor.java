package com.nataproperty.solourbana.view.menuitem.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterNotificationDetailInteractor {
    void getGCMMessageSvc(String gcmMessageRef,String memberRef);

}
