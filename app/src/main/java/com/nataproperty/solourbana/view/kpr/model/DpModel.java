package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by UserModel on 7/12/2016.
 */
public class DpModel {
    String dpNo,dp,cicil,hari;

    public String getDpNo() {
        return dpNo;
    }

    public void setDpNo(String dpNo) {
        this.dpNo = dpNo;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getCicil() {
        return cicil;
    }

    public void setCicil(String cicil) {
        this.cicil = cicil;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }
}
