package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by Nata
 * on Mar 3/30/2017 17:42.
 * Project : CarstenszNew
 */
public class QtyNupModel {
    String qtyNUP;

    public String getQtyNUP() {
        return qtyNUP;
    }

    public void setQtyNUP(String qtyNUP) {
        this.qtyNUP = qtyNUP;
    }
}
