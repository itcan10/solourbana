package com.nataproperty.solourbana.view.project.model;
/**
 * Created by UserModel on 5/15/2016.
 */

public class FeatureModel {
    long  projectFeature;
    String dbMasterRef, projectRef,projectFeatureName;

    public long getProjectFeature() {
        return projectFeature;
    }

    public void setProjectFeature(long projectFeature) {
        this.projectFeature = projectFeature;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectFeatureName() {
        return projectFeatureName;
    }

    public void setProjectFeatureName(String projectFeatureName) {
        this.projectFeatureName = projectFeatureName;
    }
}
