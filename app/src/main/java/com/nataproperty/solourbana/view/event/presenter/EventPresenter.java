package com.nataproperty.solourbana.view.event.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.event.interactor.PresenterEventInteractor;
import com.nataproperty.solourbana.view.event.model.EventModel;
import com.nataproperty.solourbana.view.event.ui.EventActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class EventPresenter implements PresenterEventInteractor {
    private EventActivity view;
    private ServiceRetrofitProject service;

    public EventPresenter(EventActivity view, ServiceRetrofitProject service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListEventSvc(String contentDate, String memberRef, String projectCode) {
        Call<ArrayList<EventModel>> call = service.getAPI().getListEvent(contentDate, memberRef, projectCode);
        call.enqueue(new Callback<ArrayList<EventModel>>() {
            @Override
            public void onResponse(Call<ArrayList<EventModel>> call, Response<ArrayList<EventModel>> response) {
                view.showListEventResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<EventModel>> call, Throwable t) {
                view.showListEventFailure(t);

            }


        });
    }

}
