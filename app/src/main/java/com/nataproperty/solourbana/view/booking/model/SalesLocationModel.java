package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by User on 11/3/2016.
 */
public class SalesLocationModel {
    String salesLocation;
    String salesLocationName;


    public String getSalesLocation() {
        return salesLocation;
    }

    public void setSalesLocation(String salesLocation) {
        this.salesLocation = salesLocation;
    }

    public String getSalesLocationName() {
        return salesLocationName;
    }

    public void setSalesLocationName(String salesLocationName) {
        this.salesLocationName = salesLocationName;
    }
}
