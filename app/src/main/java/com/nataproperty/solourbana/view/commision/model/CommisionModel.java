package com.nataproperty.solourbana.view.commision.model;

/**
 * Created by Nata on 1/4/2017.
 */

public class CommisionModel {
    String bookingRef, dbMasterRef, projectRef, projectPsRef, bookingCode, projectName, clusterName, blockName,
            unitName, commisionStatusName, comPriceAmt, countCommision, totalAmtCommision, custPaid, customerName,
            isShowCommissionCust;

    public String getBookingRef() {
        return bookingRef;
    }

    public void setBookingRef(String bookingRef) {
        this.bookingRef = bookingRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectPsRef() {
        return projectPsRef;
    }

    public void setProjectPsRef(String projectPsRef) {
        this.projectPsRef = projectPsRef;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getCommisionStatusName() {
        return commisionStatusName;
    }

    public void setCommisionStatusName(String commisionStatusName) {
        this.commisionStatusName = commisionStatusName;
    }

    public String getComPriceAmt() {
        return comPriceAmt;
    }

    public void setComPriceAmt(String comPriceAmt) {
        this.comPriceAmt = comPriceAmt;
    }

    public String getCountCommision() {
        return countCommision;
    }

    public void setCountCommision(String countCommision) {
        this.countCommision = countCommision;
    }

    public String getTotalAmtCommision() {
        return totalAmtCommision;
    }

    public void setTotalAmtCommision(String totalAmtCommision) {
        this.totalAmtCommision = totalAmtCommision;
    }

    public String getCustPaid() {
        return custPaid;
    }

    public void setCustPaid(String custPaid) {
        this.custPaid = custPaid;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getIsShowCommissionCust() {
        return isShowCommissionCust;
    }

    public void setIsShowCommissionCust(String isShowCommissionCust) {
        this.isShowCommissionCust = isShowCommissionCust;
    }
}
