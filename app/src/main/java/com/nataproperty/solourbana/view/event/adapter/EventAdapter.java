package com.nataproperty.solourbana.view.event.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.event.model.EventModel;
import com.nataproperty.solourbana.view.event.ui.EventDetailActivity;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class EventAdapter extends BaseAdapter implements Filterable {

    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String CONTENT_TYPE = "contentType";
    public static final String CONTENT_REF = "contentRef";
    public static final String TITLE = "title";
    public static final String SYNOPSIS = "synopsis";
    public static final String SCHADULE = "schedule";
    public static final String LINK_DETAIL = "linkDetail";

    private Context context;
    private ArrayList<EventModel> list;
    public ArrayList<EventModel> orig;

    private ListEventHolder holder;

    long timeInMilliseconds;

    CompactCalendarView compactCalendarView;

    String memberRef, dbMasterRef, projectRef, contentType, contentRef, txtTitle, synopsis, schedule, linkDetail, formattedDate;

    Typeface font;

    public EventAdapter(Context context, ArrayList<EventModel> list, CompactCalendarView compactCalendarView) {
        this.context = context;
        this.list = list;
        this.compactCalendarView = compactCalendarView;
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_event, null);
            holder = new ListEventHolder();
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.schedule = (TextView) convertView.findViewById(R.id.txt_schedule);
            holder.synopsis = (TextView) convertView.findViewById(R.id.txt_synopsis);
            holder.itemList = (LinearLayout) convertView.findViewById(R.id.item_list);
            holder.btnRsvp = (Button) convertView.findViewById(R.id.btn_rsvp);

            convertView.setTag(holder);
        } else {
            holder = (ListEventHolder) convertView.getTag();
        }

        EventModel event = list.get(position);
        holder.title.setText(event.getTitle());
        holder.schedule.setText(event.getSchedule());
        holder.synopsis.setText(event.getSynopsis());
        holder.itemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbMasterRef = list.get(position).getDbMasterRef();
                projectRef = list.get(position).getProjectRef();
                txtTitle = list.get(position).getTitle();
                schedule = list.get(position).getSchedule();
                synopsis = list.get(position).getSynopsis();
                contentType = list.get(position).getContentType();
                contentRef = list.get(position).getContentRef();
                linkDetail = list.get(position).getLinkDetail();

                Intent intent = new Intent(context, EventDetailActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(TITLE, txtTitle);
                intent.putExtra(SYNOPSIS, synopsis);
                intent.putExtra(SCHADULE, schedule);
                intent.putExtra(CONTENT_TYPE, contentType);
                intent.putExtra(CONTENT_REF, contentRef);
                intent.putExtra(LINK_DETAIL, linkDetail);
                context.startActivity(intent);
            }
        });

        holder.btnRsvp.setTypeface(font);
        holder.btnRsvp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbMasterRef = list.get(position).getDbMasterRef();
                projectRef = list.get(position).getProjectRef();
                txtTitle = list.get(position).getTitle();
                schedule = list.get(position).getSchedule();
                synopsis = list.get(position).getSynopsis();
                contentType = list.get(position).getContentType();
                contentRef = list.get(position).getContentRef();
                linkDetail = list.get(position).getLinkDetail();


                Intent intent = new Intent(context, EventDetailActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(TITLE, txtTitle);
                intent.putExtra(SYNOPSIS, synopsis);
                intent.putExtra(SCHADULE, schedule);
                intent.putExtra(CONTENT_TYPE, contentType);
                intent.putExtra(CONTENT_REF, contentRef);
                intent.putExtra(LINK_DETAIL, linkDetail);
                context.startActivity(intent);


            }
        });

        String status = event.getStatus();

        /*status btn RSVP*/
        Log.d("Status", "" + status);
        if (status.equals("202")) {
            holder.btnRsvp.setVisibility(View.GONE);
        } else if (status.equals("205")) {
            holder.btnRsvp.setVisibility(View.GONE);
        } else {
            holder.btnRsvp.setVisibility(View.VISIBLE);
        }

        dbMasterRef = list.get(position).getDbMasterRef();
        //formattedDate = list.get(position).getFormattedDate();
        /*Color*/
        if (status.equals("205")) {
            holder.itemList.setBackgroundResource(R.color.colorWhite);
            holder.title.setTextColor(convertView.getResources().getColor(R.color.colorText));
            holder.schedule.setTextColor(convertView.getResources().getColor(R.color.colorText));
            holder.synopsis.setTextColor(convertView.getResources().getColor(R.color.colorText));
        } else {
            holder.itemList.setBackgroundResource(R.color.colorPrimarySecond);
            holder.title.setTextColor(Color.WHITE);
            holder.schedule.setTextColor(Color.WHITE);
            holder.synopsis.setTextColor(Color.WHITE);
        }


        return convertView;
    }

    private class ListEventHolder {
        TextView title, schedule, synopsis;
        LinearLayout itemList;
        Button btnRsvp;
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<EventModel> results = new ArrayList<EventModel>();
                if (orig == null)
                    orig = list;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final EventModel g : orig) {
                            if (g.getEventDate().toLowerCase().contains(constraint.toString()))
                                results.add(g);
                            else if (g.getSchedule().toLowerCase().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (ArrayList<EventModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}
