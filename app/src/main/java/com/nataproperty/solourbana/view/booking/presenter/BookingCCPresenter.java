package com.nataproperty.solourbana.view.booking.presenter;


import com.google.android.gms.fitness.data.Subscription;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.booking.intractor.PresenterBookingCCInteractor;
import com.nataproperty.solourbana.view.booking.ui.BookingCreditCardActivity;
import com.nataproperty.solourbana.view.kpr.model.PaymentCCStatusModel;
import com.nataproperty.solourbana.view.mybooking.model.BookingDetailStatusModel;
import com.nataproperty.solourbana.view.nup.model.MouthModel;
import com.nataproperty.solourbana.view.nup.model.YearModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class BookingCCPresenter implements PresenterBookingCCInteractor {
    private BookingCreditCardActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public BookingCCPresenter(BookingCreditCardActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getMontList(String month) {
        Call<List<MouthModel>> call = service.getAPI().getMonthList(month);
        call.enqueue(new Callback<List<MouthModel>>() {
            @Override
            public void onResponse(Call<List<MouthModel>> call, Response<List<MouthModel>> response) {
                //view.showMonthResults(response);
            }

            @Override
            public void onFailure(Call<List<MouthModel>> call, Throwable t) {
                //view.showMonthFailure(t);

            }


        });
    }

    @Override
    public void getYearsList(String years) {
        Call<List<YearModel>> call = service.getAPI().getYearsList(years);
        call.enqueue(new Callback<List<YearModel>>() {
            @Override
            public void onResponse(Call<List<YearModel>> call, Response<List<YearModel>> response) {
                //view.showYearsResults(response);
            }

            @Override
            public void onFailure(Call<List<YearModel>> call, Throwable t) {
                //view.showYearsFailure(t);

            }


        });
    }

    @Override
    public void getPayment(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<PaymentCCStatusModel> call = service.getAPI().getPaymentCC(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo);
        call.enqueue(new Callback<PaymentCCStatusModel>() {
            @Override
            public void onResponse(Call<PaymentCCStatusModel> call, Response<PaymentCCStatusModel> response) {
                //view.showPaymentResults(response);
            }

            @Override
            public void onFailure(Call<PaymentCCStatusModel> call, Throwable t) {
                //view.showPaymentFailure(t);

            }


        });
    }

    @Override
    public void getBookingDetail(String dbMasterRef, String projectRef, String bookingRef) {
        Call<BookingDetailStatusModel> call = service.getAPI().getBookingDetail(dbMasterRef,projectRef,bookingRef);
        call.enqueue(new Callback<BookingDetailStatusModel>() {
            @Override
            public void onResponse(Call<BookingDetailStatusModel> call, Response<BookingDetailStatusModel> response) {
                //view.showBookingDtlResults(response);
            }

            @Override
            public void onFailure(Call<BookingDetailStatusModel> call, Throwable t) {
                //view.showBookingDtlFailure(t);

            }


        });
    }
}
