package com.nataproperty.solourbana.view.event.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.event.model.EventSchaduleModel;
import com.nataproperty.solourbana.view.event.ui.EventRsvpActivity;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class EventSchaduleAdapter extends BaseAdapter  {
    public static final String CONTENT_REF = "contentRef" ;
    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String GUEST_NAME = "guest_name";
    public static final String COMPANY_NAME = "company_name";
    public static final String GUEST_PHONE = "guest_phone";
    public static final String GUEST_MOBILE1 = "guest_hp1";
    public static final String GUEST_MOBILE2 = "guest_hp2";
    public static final String GUEST_EMAIL = "guest_email";
    public static final String STATUS = "status";
    public static final String LINK_DETAIL = "linkDetail";

    private Context context;
    private ArrayList<EventSchaduleModel> list;
    private ListEventSchaduleHolder holder;
    Typeface font;
    String linkDetail;

    public EventSchaduleAdapter(Context context, ArrayList<EventSchaduleModel> list,String linkDetail) {
        this.context = context;
        this.list = list;
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
        this.linkDetail = linkDetail;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list_event_schadule,null);
            holder = new ListEventSchaduleHolder();
            holder.eventScheduleDate = (TextView) convertView.findViewById(R.id.txt_event_schedule_date);
            holder.btnRsvp = (Button) convertView.findViewById(R.id.btn_rspv);
            holder.btnRsvp.setTypeface(font);

            convertView.setTag(holder);
        }else{
            holder = (ListEventSchaduleHolder) convertView.getTag();
        }

        final EventSchaduleModel eventSchaduleModel = list.get(position);
        holder.eventScheduleDate.setText(eventSchaduleModel.getEventScheduleDate());

        String status = eventSchaduleModel.getStatus();

        if (status.equals("202")){
            holder.btnRsvp.setVisibility(View.INVISIBLE);
        }else {
            holder.btnRsvp.setVisibility(View.VISIBLE);
        }

        holder.btnRsvp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EventRsvpActivity.class);
                intent.putExtra(CONTENT_REF,eventSchaduleModel.getContentRef());
                intent.putExtra(EVENT_SCHEDULE_REF,eventSchaduleModel.getEventScheduleRef());
                intent.putExtra(STATUS,eventSchaduleModel.getStatus());
                intent.putExtra(LINK_DETAIL,linkDetail);
                context.startActivity(intent);
            }
        });

        Log.d("EventSchaduleAdapter",""+eventSchaduleModel.getEventScheduleDate());
        return convertView;
    }

    private class ListEventSchaduleHolder {
        TextView eventScheduleDate;
        Button btnRsvp;
    }

}
