package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by nata on 12/2/2016.
 */
public class DataKPR {
    private String ref;
    private String insPeriode;
    private String instMonth;
    private String pct;
    private String total;

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getInsPeriode() {
        return insPeriode;
    }

    public void setInsPeriode(String insPeriode) {
        this.insPeriode = insPeriode;
    }

    public String getInstMonth() {
        return instMonth;
    }

    public void setInstMonth(String instMonth) {
        this.instMonth = instMonth;
    }

    public String getPct() {
        return pct;
    }

    public void setPct(String pct) {
        this.pct = pct;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
