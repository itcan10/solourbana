package com.nataproperty.solourbana.view.mybooking.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.mybooking.intractor.PresenterMyBookInteractor;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingModel;
import com.nataproperty.solourbana.view.mybooking.ui.MyBookingActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookPresenter implements PresenterMyBookInteractor {
    private MyBookingActivity view;
    private ServiceRetrofitProject service;

    public MyBookPresenter(MyBookingActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListBook(String memberRef,String projectCode ) {
        Call<List<MyBookingModel>> call = service.getAPI().getListBook(memberRef,projectCode);
        call.enqueue(new Callback<List<MyBookingModel>>() {
            @Override
            public void onResponse(Call<List<MyBookingModel>> call, Response<List<MyBookingModel>> response) {
                view.showListBookResults(response);
            }

            @Override
            public void onFailure(Call<List<MyBookingModel>> call, Throwable t) {
                view.showListBookFailure(t);

            }


        });
    }
}
