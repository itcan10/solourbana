package com.nataproperty.solourbana.view.menuitem.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterNotificationInteractor {
    void getListNotificationSSTSvc(String memberRef, String projectCode);
}
