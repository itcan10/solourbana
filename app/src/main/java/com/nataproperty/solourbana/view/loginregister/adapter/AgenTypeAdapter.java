package com.nataproperty.solourbana.view.loginregister.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.loginregister.model.AgenTypeModel;

import java.util.List;

/**
 * Created by User on 4/17/2016.
 */
public class AgenTypeAdapter extends BaseAdapter {
    private Context context;
    private List<AgenTypeModel> list;
    private ListAgentTypeHolder holder;

    public AgenTypeAdapter(Context context, List<AgenTypeModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_member_type,null);
            holder = new ListAgentTypeHolder();
            holder.memberTypeName = (TextView) convertView.findViewById(R.id.memberTypeName);

            convertView.setTag(holder);
        }else{
            holder = (ListAgentTypeHolder) convertView.getTag();
        }
        AgenTypeModel type = list.get(position);
        holder.memberTypeName.setText(type.getMemberTypeName());

        return convertView;
    }

    private class ListAgentTypeHolder {
        TextView memberTypeName;
    }
}
