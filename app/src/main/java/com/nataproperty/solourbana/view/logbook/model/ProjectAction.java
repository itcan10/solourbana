package com.nataproperty.solourbana.view.logbook.model;

/**
 * Created by Nata on 2/17/2017.
 */

public class ProjectAction {
    String customerProjectAction, customerProjectActionName;

    public String getCustomerProjectAction() {
        return customerProjectAction;
    }

    public void setCustomerProjectAction(String customerProjectAction) {
        this.customerProjectAction = customerProjectAction;
    }

    public String getCustomerProjectActionName() {
        return customerProjectActionName;
    }

    public void setCustomerProjectActionName(String customerProjectActionName) {
        this.customerProjectActionName = customerProjectActionName;
    }
}
