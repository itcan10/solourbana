package com.nataproperty.solourbana.view.mynup.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mynup.adapter.UnPaidAdapter;
import com.nataproperty.solourbana.view.mynup.model.UnPaidNupModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyNupUnpaidActivity extends AppCompatActivity {
    public static final String TAG = "MyNupUnpaidActivity";
    public static final String PREF_NAME = "pref";

    public static final String DB_MASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String NUP_ORDE_CODE = "nupOrderCode";
    public static final String BUY_DATE = "buyDate";
    public static final String PAYMENT_TYPE_NAME = "paymentTypeName";
    public static final String QTY = "qty";
    public static final String PP_AMT = "ppAmt";
    public static final String TOTAL_AMT = "totalAmt";
    public static final String NUP_ORDER_DETAIL_REF = "nupOrderDetailRef";
    public static final String NUP_ORDER_STATUS = "nupOrderStatus";
    public static final String COSTUMER_NAME = "customerName";
    public static final String PROJECT_NAME = "projectName";

    public static final String NUP_ORDER_REF = "nupOrderRef";

    public static final String STATUS = "status";

    String dbMasterRef, projectRef, nupOrderCode, buyDate, paymentTypeName, qty, ppAmt, totalAmt, nupOrderDetailRef, nupOrderStatus,
            customerName, projectName, status, nupOrderRef;

    SharedPreferences sharedPreferences;

    private ArrayList<UnPaidNupModel> listUnpaid = new ArrayList<UnPaidNupModel>();
    private UnPaidAdapter adapter;

    String memberRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_nup_unpaid);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.tab_title_unpaid));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Log.d("cek", "" + memberRef);

        final ListView listView = (ListView) findViewById(R.id.list_nup_project_ps);
        adapter = new UnPaidAdapter(this, listUnpaid);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dbMasterRef = listUnpaid.get(position).getDbMasterRef();
                projectRef = listUnpaid.get(position).getProjectRef();
                nupOrderCode = listUnpaid.get(position).getNupOrderCode();
                buyDate = listUnpaid.get(position).getBuyDate();
                paymentTypeName = listUnpaid.get(position).getPaymentTypeName();
                qty = listUnpaid.get(position).getQty();
                ppAmt = listUnpaid.get(position).getPpAmt();
                totalAmt = listUnpaid.get(position).getTotalAmt();
                nupOrderDetailRef = listUnpaid.get(position).getNupOrderDetailRef();
                nupOrderStatus = listUnpaid.get(position).getNupOrderStatus();
                customerName = listUnpaid.get(position).getCustomerName();
                projectName = listUnpaid.get(position).getProjectName();
                nupOrderRef = listUnpaid.get(position).getNupOrderRef();

                Intent intent = new Intent(MyNupUnpaidActivity.this, MyNupDetailActivity.class);
                intent.putExtra(DB_MASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(NUP_ORDE_CODE, nupOrderCode);
                intent.putExtra(BUY_DATE, buyDate);
                intent.putExtra(PAYMENT_TYPE_NAME, paymentTypeName);
                intent.putExtra(QTY, qty);
                intent.putExtra(PP_AMT, ppAmt);
                intent.putExtra(TOTAL_AMT, totalAmt);
                intent.putExtra(NUP_ORDER_DETAIL_REF, nupOrderDetailRef);
                intent.putExtra(NUP_ORDER_STATUS, nupOrderStatus);
                intent.putExtra(COSTUMER_NAME, customerName);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(NUP_ORDER_REF, nupOrderRef);
                intent.putExtra(STATUS, "1");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        listUnpaid.clear();
        requestListUnpaid();
    }

    public void requestListUnpaid() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getUnpaidNUPForProjectSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result unpaid", response);
                    generateListUnpaid(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(MyNupUnpaidActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(MyNupUnpaidActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("projectCode", General.projectCode);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestListUnpaid");

    }

    private void generateListUnpaid(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                UnPaidNupModel UnPaid = new UnPaidNupModel();
                UnPaid.setDbMasterRef(jo.getString("dbMasterRef"));
                UnPaid.setProjectRef(jo.getString("projectRef"));
                UnPaid.setNupOrderCode(jo.getString("nupOrderCode"));
                UnPaid.setBuyDate(jo.getString("buyDate"));
                UnPaid.setPaymentTypeName(jo.getString("paymentTypeName"));
                UnPaid.setQty(jo.getString("qty"));
                UnPaid.setPpAmt(jo.getString("ppAmt"));
                UnPaid.setTotalAmt(jo.getString("totalAmt"));
                UnPaid.setNupOrderDetailRef(jo.getString("nupOrderDetailRef"));
                UnPaid.setNupOrderStatus(jo.getString("nupOrderStatus"));
                UnPaid.setCustomerName(jo.getString("customerName"));
                UnPaid.setProjectName(jo.getString("projectName"));
                UnPaid.setNupOrderRef(jo.getString("nupOrderRef"));
                listUnpaid.add(UnPaid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyNupUnpaidActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
