package com.nataproperty.solourbana.view.event.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.event.interactor.EventRsvpInteractor;
import com.nataproperty.solourbana.view.event.model.ResponseRsvp;
import com.nataproperty.solourbana.view.event.ui.EventRsvpActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class EventRsvpPresenter implements EventRsvpInteractor {
    private EventRsvpActivity view;
    private ServiceRetrofit service;

    public EventRsvpPresenter(EventRsvpActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void saveRsvpSvc(Map<String, String> fields) {
        Call<ResponseRsvp> call = service.getAPI().saveRsvpSvc(fields);
        call.enqueue(new Callback<ResponseRsvp>() {
            @Override
            public void onResponse(Call<ResponseRsvp> call, Response<ResponseRsvp> response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponseRsvp> call, Throwable t) {
                view.showResponeFailure(t);
            }
        });
    }


}
