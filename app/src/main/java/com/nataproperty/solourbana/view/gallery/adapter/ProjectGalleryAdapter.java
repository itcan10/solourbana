package com.nataproperty.solourbana.view.gallery.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.gallery.model.ProjectGalleryModel;
import com.nataproperty.solourbana.view.gallery.ui.DetailGalleryActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by UserModel on 4/19/2016.
 */
public class ProjectGalleryAdapter extends BaseAdapter {
    public static final String TAG = "NewsGalleryAdapter" ;

    public static final String IMAGE_GALLERY_REF = "imageGalleryRef";
    public static final String POSISION = "position";
    public static final String GROUP_GALLERY_REF = "groupGalleryRef";
    public static final String PROJECT_NAME = "projectName";

    private Context context;
    private List<ProjectGalleryModel> list;
    private ListGalleryHolder holder;

    private Display display;

    private String projectName;

    public ProjectGalleryAdapter(Context context, List<ProjectGalleryModel> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_project_gallery,null);
            holder = new ListGalleryHolder();
            holder.imgNewsGallery = (ImageView) convertView.findViewById(R.id.img_item_list_property_gallery);
            holder.txtCaption = (TextView) convertView.findViewById(R.id.txt_caption);

            convertView.setTag(holder);
        }else{
            holder = (ListGalleryHolder) convertView.getTag();
        }

        ProjectGalleryModel ProjectGalleryModel = list.get(position);

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x/2;
        Double result = width/1.234567901234568;

        ViewGroup.LayoutParams params = holder.imgNewsGallery.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.imgNewsGallery.setLayoutParams(params);
        holder.imgNewsGallery.requestLayout();

//        Glide.with(context).load(ProjectGalleryModel.getImgFile())
//                .skipMemoryCache(true)
//                .into(holder.imgNewsGallery);

        Picasso.with(context)
                .load(ProjectGalleryModel.getImgFile())
                .into(holder.imgNewsGallery);


        Log.d("IMG",String.valueOf(ProjectGalleryModel.getImgFile()));
        holder.txtCaption.setText(ProjectGalleryModel.getTitle());
        holder.imgNewsGallery.setOnClickListener(new OnImageClickListener(position));

        return convertView;
    }

    class OnImageClickListener implements View.OnClickListener {

        int position;

        // constructor
        public OnImageClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
            Intent i = new Intent(context, DetailGalleryActivity.class);
            i.putExtra(IMAGE_GALLERY_REF,list.get(position).getImgGalleryRef());
            i.putExtra(GROUP_GALLERY_REF, list.get(position).getGroupGalleryRef());
            i.putExtra(PROJECT_NAME, list.get(position).getProjectName());
            i.putExtra(POSISION, position);
            context.startActivity(i);
        }

    }

    private class ListGalleryHolder {
        ImageView imgNewsGallery;
        TextView txtCaption;
    }
}
