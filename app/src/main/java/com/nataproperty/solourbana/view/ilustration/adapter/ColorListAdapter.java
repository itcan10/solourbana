package com.nataproperty.solourbana.view.ilustration.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.ilustration.model.ColorModel;

import java.util.List;


public class ColorListAdapter extends RecyclerView.Adapter<ColorListAdapter.BalanceViewHolder> {

    private Context context;
    private List<ColorModel> listBlockDiagramModels;

    public ColorListAdapter(Context context, List<ColorModel> balanceModelList) {
        this.context = context;
        this.listBlockDiagramModels = balanceModelList;
    }

    @Override
    public BalanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_color, parent, false);

        return new BalanceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BalanceViewHolder holder, int position) {
        final ColorModel bm = listBlockDiagramModels.get(position);

        holder.name.setBackgroundColor(Color.parseColor(bm.getColor()));
        holder.name.setText(bm.getColorName());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, bm.getColorName(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listBlockDiagramModels.size();
    }

    static class BalanceViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        public BalanceViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.txt_name);
        }
    }
}
