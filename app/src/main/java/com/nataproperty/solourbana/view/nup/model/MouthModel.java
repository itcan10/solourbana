package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by UserModel on 5/22/2016.
 */
public class MouthModel {
    String mounthRef;
    String mounthName;

    public String getMounthRef() {
        return mounthRef;
    }

    public void setMounthRef(String mounthRef) {
        this.mounthRef = mounthRef;
    }

    public String getMounthName() {
        return mounthName;
    }

    public void setMounthName(String mounthName) {
        this.mounthName = mounthName;
    }
}
