package com.nataproperty.solourbana.view.news.model;

/**
 * Created by UserModel on 6/11/2016.
 */
public class NewsDetailModel {
    String imageSlider,imageRef,contentRef;

    public String getImageRef() {
        return imageRef;
    }

    public void setImageRef(String imageRef) {
        this.imageRef = imageRef;
    }

    public String getContentRef() {
        return contentRef;
    }

    public void setContentRef(String contentRef) {
        this.contentRef = contentRef;
    }

    public String getImageSlider() {
        return imageSlider;
    }

    public void setImageSlider(String imageSlider) {
        this.imageSlider = imageSlider;
    }
}
