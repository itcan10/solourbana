package com.nataproperty.solourbana.view.event.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.event.adapter.MyTicketDetailImageAdapter;
import com.nataproperty.solourbana.view.event.model.MyTicketDetailModel;
import com.nataproperty.solourbana.view.event.presenter.MyTicketDetailPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Response;

public class MyTicketDetailActivity extends AppCompatActivity {
    public static final String TAG = "MyTicketActivity" ;

    public static final String PREF_NAME = "pref" ;

    public static final String TITLE = "title" ;
    public static final String DATE = "date" ;
    public static final String LINK_DETAIL = "linkDetail" ;
    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String EVENT_SCHEDULE_DATE = "eventScheduleDate";
    private static final String EXTRA_RX = "EXTRA_RX";
    SharedPreferences sharedPreferences;

    String memberRef;

    ViewPager viewPager;
    CircleIndicator indicator;
    private List<MyTicketDetailModel> listGallery = new ArrayList<MyTicketDetailModel>();
    private MyTicketDetailImageAdapter adapter;

    String eventScheduleRef,txtTitle,eventScheduleDate;

    LinearLayout linearLayout;
    MyTicketDetailPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;
    ProgressDialog progressDialog;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ticket_detail);

        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyTicketDetailPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );

        eventScheduleRef = getIntent().getStringExtra(EVENT_SCHEDULE_REF);
        eventScheduleDate = getIntent().getStringExtra(EVENT_SCHEDULE_DATE);
        txtTitle = getIntent().getStringExtra(TITLE);

        initWidget();

        listGallery.clear();
        requestPropertyInfo();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_ticket));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        title.setText(txtTitle);

        indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.pager);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/0.8;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        viewPager.setLayoutParams(params);
        viewPager.requestLayout();
    }

    public void requestPropertyInfo() {
        presenter.getListTiketQRCode(memberRef,eventScheduleRef);
    }

    public void showListMyTicketDetailResults(Response<ArrayList<MyTicketDetailModel>> response) {
        listGallery = response.body();
        initAdapterImage();
    }

    public void showListMyTicketDetailFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapterImage(){
        adapter = new MyTicketDetailImageAdapter(this,listGallery,eventScheduleDate);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyTicketDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
