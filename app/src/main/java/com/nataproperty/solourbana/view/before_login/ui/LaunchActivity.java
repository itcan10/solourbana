package com.nataproperty.solourbana.view.before_login.ui;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.before_login.model.VersionModel;
import com.nataproperty.solourbana.view.ilustration.presenter.LaunchPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

/**
 * Created by UserModel on 4/13/2016.
 */
public class LaunchActivity extends AppCompatActivity {
    public static final String TAG = "LaunchActivity";
    public static final String PREF_NAME = "pref";
    boolean ifNotExists = false;
    private static int splashInterval = 1;
    SharedPreferences sharedPreferences;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    boolean state;
    String versionName = "";
    int versionCode = -1;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private LaunchPresenter presenter;
    ProgressDialog progressDialog;
    AlertDialog alertDialog;
    String memberTypeCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new LaunchPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        getVersionInfo();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        requestVersion();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void requestVersion() {
        presenter.getVersionApp(General.projectCode);

    }

    private void getVersionInfo() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.d(TAG, versionName + " " + versionCode);
    }

    public void showVersionAppResults(retrofit2.Response<VersionModel> response) {
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            String versionApp = response.body().getVersionApp();
            memberTypeCode = response.body().getMemberTypeCode(); //isinya link playstore

            Log.d(TAG, "versionApp " + versionApp);

            if (status == 200) {
                if (versionCode < Integer.parseInt(message)) {
                    if (!isFinishing()) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LaunchActivity.this);
                        alertDialogBuilder
                                .setMessage("Update Available v" + versionApp)
                                .setCancelable(false)
                                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(memberTypeCode)));
                                        } catch (ActivityNotFoundException e){
                                            Toast.makeText(LaunchActivity.this, "Buka Play Store, Update Aplikasi "+getString(R.string.app_name),
                                                    Toast.LENGTH_LONG).show();
                                        }

                                    }
                                });
                        alertDialog = alertDialogBuilder.create();
                        if(!(LaunchActivity.this).isFinishing()) {
                            alertDialog.show();
                        }
                    }

                } else {
                    if (state) {
                        Intent intent = new Intent(LaunchActivity.this, ProjectMenuActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(LaunchActivity.this, ViewpagerActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LaunchActivity.this);
            alertDialogBuilder
                    .setMessage("Terjadi Kesalahan")
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            LaunchActivity.this.recreate();
                        }
                    });

            alertDialog = alertDialogBuilder.create();
            if (!(LaunchActivity.this).isFinishing()){
                alertDialog.show();
            }

        }

    }

    public void showVersionAppFailure(Throwable t) {
        if (state) {
            Intent intent = new Intent(LaunchActivity.this, ProjectMenuActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(LaunchActivity.this, ViewpagerActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }


}
