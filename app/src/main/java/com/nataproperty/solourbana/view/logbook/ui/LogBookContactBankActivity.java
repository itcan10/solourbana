package com.nataproperty.solourbana.view.logbook.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.adapter.LogBookContactBankAdapter;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.presenter.LogBookContactBankPresenter;

import java.util.ArrayList;

import retrofit2.Response;

public class LogBookContactBankActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private LogBookContactBankPresenter presenter;
    ListView listView;
    LogBookContactBankAdapter adapter;
    ArrayList<LogBookModel> list = new ArrayList<>();
    Toolbar toolbar;
    TextView title, noData;
    Typeface font;
    SharedPreferences sharedPreferences;
    FloatingActionButton fab;
    String memberRef, keyword = "", customerRef, dbMasterRef, projectRef, countryCode = "", provinceCode = "0", cityCode = "0";
    ProgressDialog progressDialog;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_book_contact_bank);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new LogBookContactBankPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.menu_log_book) + " Contact Bank");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.list_log_book);
        noData = (TextView) findViewById(R.id.txt_no_data);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = getIntent();
        dbMasterRef = i.getStringExtra("dbMasterRef");
        projectRef = i.getStringExtra("projectRef");
        provinceCode = i.getStringExtra("provinceCode");
        cityCode = i.getStringExtra("cityCode");
        keyword = i.getStringExtra("keyword");
        if (provinceCode == null)
            provinceCode = "0";
        if (cityCode == null)
            cityCode = "0";
        if (keyword == null)
            keyword = "";

        requestLogBook();

    }

    private void requestLogBook() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetContactBankTaskSvc(memberRef, dbMasterRef, projectRef, General.customerStatusVerifyRef, keyword, General.coutryCode, provinceCode, cityCode);
        Log.d("param", memberRef + " " + dbMasterRef + " " + General.customerStatusVerifyRef + " " + keyword + " " + General.coutryCode + " " + provinceCode + " " + cityCode);
    }

    public void showListLogBookResults(Response<ArrayList<LogBookModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            list = response.body();
            if (list.size() > 0) {
                initAdapter();
                noData.setVisibility(View.GONE);
            } else {
                noData.setVisibility(View.VISIBLE);
                listView.setEmptyView(noData);
            }

        }
    }

    public void showListLogBookFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new LogBookContactBankAdapter(this, list, dbMasterRef, projectRef);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(LogBookContactBankActivity.this, AddLogBookActivity.class);
                intent.putExtra("customerRef", "");
                startActivity(intent);
                break;

            case R.id.btn_contact_bank:
                Intent intent1 = new Intent(LogBookContactBankActivity.this, LogBookContactBankActivity.class);
                startActivity(intent1);
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == listView) {
            customerRef = list.get(position).getCustomerRef();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LogBookContactBankActivity.this);
            alertDialogBuilder
                    .setMessage("Apakah anda ingin mengubah log book?")
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(LogBookContactBankActivity.this, AddLogBookActivity.class);
                            intent.putExtra("customerRef", customerRef);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_search:
                Intent intent = new Intent(LogBookContactBankActivity.this, FilterLogBookActivity.class);
                intent.putExtra("dbMasterRef", dbMasterRef);
                intent.putExtra("projectRef", projectRef);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }
}
