package com.nataproperty.solourbana.view.report.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;
import com.nataproperty.solourbana.view.report.adapter.ReportProjectAdapter;
import com.nataproperty.solourbana.view.report.model.ReportModel;
import com.nataproperty.solourbana.view.report.presenter.ReportProjectPresenter;

import java.util.ArrayList;

import retrofit2.Response;

public class ReportProjectActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private ReportProjectPresenter presenter;
    ListView listView;
    ReportProjectAdapter adapter;
    ArrayList<ReportModel> list = new ArrayList<>();
    Toolbar toolbar;
    TextView title, noData;
    Typeface font;
    String projectRef, dbMasterRef, projectName, userRef;
    SharedPreferences sharedPreferences;
    String memberRef;
    ProgressDialog progressDialog;
    LinearLayout activityReportProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_project);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new ReportProjectPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent i = getIntent();
        userRef = i.getStringExtra("userRef");
        dbMasterRef = i.getStringExtra(DBMASTER_REF);
        projectRef = i.getStringExtra(PROJECT_REF);
        projectName = i.getStringExtra(PROJECT_NAME);

        requestReport();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.menu_report));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.list_report);
        noData = (TextView) findViewById(R.id.txt_no_data);
        listView.setOnItemClickListener(this);
        activityReportProject = (LinearLayout) findViewById(R.id.activity_report_project);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private void requestReport (){
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetListDashboardReport2(memberRef,dbMasterRef,projectRef);
    }

    public void showListReportResults(Response<ArrayList<ReportModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            list = response.body();
            if (list.size() > 0) {
                initAdapter();
                noData.setVisibility(View.GONE);
            } else {
                noData.setVisibility(View.VISIBLE);
                listView.setEmptyView(noData);
            }
        }
    }

    public void showListReportFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new ReportProjectAdapter(this, list, userRef, dbMasterRef, projectRef);
        listView.setAdapter(adapter);
    }

    public void showSnakBar(String message) {
        Snackbar snackbar = Snackbar.make(activityReportProject, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intent = new Intent(ReportProjectActivity.this, ProjectMenuActivity.class);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intent.putExtra(PROJECT_NAME, projectName);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
