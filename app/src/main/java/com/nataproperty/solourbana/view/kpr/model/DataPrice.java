package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by nata on 12/2/2016.
 */
public class DataPrice {
    private String paymentTerm;
    private String priceInc;
    private String discPercent;
    private String discAmt;
    private String netPrice;
    private String termCondition;

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPriceInc() {
        return priceInc;
    }

    public void setPriceInc(String priceInc) {
        this.priceInc = priceInc;
    }

    public String getDiscPercent() {
        return discPercent;
    }

    public void setDiscPercent(String discPercent) {
        this.discPercent = discPercent;
    }

    public String getDiscAmt() {
        return discAmt;
    }

    public void setDiscAmt(String discAmt) {
        this.discAmt = discAmt;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    public String getTermCondition() {
        return termCondition;
    }

    public void setTermCondition(String termCondition) {
        this.termCondition = termCondition;
    }
}
