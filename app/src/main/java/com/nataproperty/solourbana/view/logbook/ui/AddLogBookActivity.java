package com.nataproperty.solourbana.view.logbook.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.CountryModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerCityAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerCountryAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerProvinceAdapter;
import com.nataproperty.solourbana.view.logbook.model.ContactInfo;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.logbook.presenter.AddLogBookPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class AddLogBookActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private AddLogBookPresenter presenter;
    Toolbar toolbar;
    TextView title;
    Typeface font;
    EditText edtName, edtHp1, edtHp2, edtPhone1, edtPhone2, edtEmail1, edtEmail2, edtAddress, edtPostCode;
    Spinner spnCoutry, spnProvince, spnCity;
    String name, hp1, hp2, phone1, phone2, email1 = "", email2, countryCode, provinceCode, cityCode,
            memberRef, address, postCode, customerRef = "", customerStatusRef, customerStatusName, userRef, countryName,
            provinceName, cityName;
    Button btnSave;

    SpinnerCountryAdapter adapterCountry;
    SpinnerProvinceAdapter adapterProvince;
    SpinnerCityAdapter adapterCity;
    ArrayList<CountryModel> listCounty = new ArrayList<CountryModel>();
    ArrayList<ProvinceModel> listProvince = new ArrayList<ProvinceModel>();
    ArrayList<CityModel> listCity = new ArrayList<CityModel>();

    SharedPreferences sharedPreferences;

    LinearLayout activityAddLogBook;

    ProgressDialog progressDialog;

    int setCountry = 0, setProvince = 0, setCity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_log_book);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new AddLogBookPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        customerRef = intent.getStringExtra("customerRef");
        if (!customerRef.equals(""))
            requestContactInfo();

        requestCountry();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.menu_log_book));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtHp1 = (EditText) findViewById(R.id.edt_hp1);
        edtHp2 = (EditText) findViewById(R.id.edt_hp2);
        edtPhone1 = (EditText) findViewById(R.id.edt_phone1);
        edtPhone2 = (EditText) findViewById(R.id.edt_phone2);
        edtEmail1 = (EditText) findViewById(R.id.edt_email1);
        edtEmail2 = (EditText) findViewById(R.id.edt_email2);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtPostCode = (EditText) findViewById(R.id.edt_post_code);
        spnCoutry = (Spinner) findViewById(R.id.spn_country);
        spnProvince = (Spinner) findViewById(R.id.spn_province);
        spnCity = (Spinner) findViewById(R.id.spn_city);
        btnSave = (Button) findViewById(R.id.btn_save);
        activityAddLogBook = (LinearLayout) findViewById(R.id.activity_add_log_book);
        btnSave.setOnClickListener(this);
    }

    private void requestCountry() {
        presenter.getCountryListSvc();
    }

    public void showListCountryResults(Response<ArrayList<CountryModel>> response) {
        listCounty = response.body();
        initSpinnerCountry();

        if (!customerRef.equals("")) {
            for (int i = 0; i < listCounty.size(); i++) {
                try {
                    String countyCodeSet = listCounty.get(i).getCountryCode();
                    if (countryCode.equals(countyCodeSet)) {
                        setCountry = i;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            spnCoutry.setSelection(setCountry);
        }

    }

    public void showListCountryFailure(Throwable t) {

    }

    private void initSpinnerCountry() {
        adapterCountry = new SpinnerCountryAdapter(this, listCounty);
        spnCoutry.setAdapter(adapterCountry);
        spnCoutry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode = listCounty.get(position).getCountryCode();
                requestProvince(countryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void requestProvince(String countryCode) {
        presenter.getProvinceListSvc(countryCode);
    }

    public void showListProvinceResults(Response<ArrayList<ProvinceModel>> response) {
        listProvince = response.body();
        initSpinnerProvince();
        if (!customerRef.equals("")) {
            for (int i = 0; i < listProvince.size(); i++) {
                try {
                    String provinceCodeSet = listProvince.get(i).getProvinceCode();
                    if (provinceCode.equals(provinceCodeSet)) {
                        setProvince = i;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            spnProvince.setSelection(setProvince);
        }
    }

    public void showListProvinceFailure(Throwable t) {

    }

    private void initSpinnerProvince() {
        adapterProvince = new SpinnerProvinceAdapter(this, listProvince);
        spnProvince.setAdapter(adapterProvince);
        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //countryCode = listCounty.get(position).getCountryCode();
                provinceCode = listProvince.get(position).getProvinceCode();
                requestCity(countryCode, provinceCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void requestCity(String countryCode, String provinceCode) {
        presenter.getCityListSvc(countryCode, provinceCode);
    }

    public void showListCityResults(Response<ArrayList<CityModel>> response) {
        listCity = response.body();
        initSpinnerCity();
        if (!customerRef.equals("")) {
            for (int i = 0; i < listCity.size(); i++) {
                try {
                    String cityCodeSet = listCity.get(i).getCityCode();
                    if (cityCode.equals(cityCodeSet)) {
                        setCity = i;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            spnCity.setSelection(setCity);
        }

    }

    public void showListCityFailure(Throwable t) {

    }

    private void initSpinnerCity() {
        adapterCity = new SpinnerCityAdapter(this, listCity);
        spnCity.setAdapter(adapterCity);
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //countryCode = listCounty.get(position).getCountryCode();
                //provinceCode = listProvince.get(position).getProvinceCode();
                cityCode = listCity.get(position).getCityCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void requestContactInfo() {
        presenter.GetInfoContactBankSvc(memberRef, customerRef, General.projectCode);
    }

    public void showInfoLogBookResults(Response<ContactInfo> response) {
        if (response.isSuccessful()) {
            name = response.body().getName();
            countryCode = response.body().getCountryCode();
            provinceCode = response.body().getProvinceCode();
            cityCode = response.body().getCityCode();
            address = response.body().getAddress();
            postCode = response.body().getPostCode();
            hp1 = response.body().getHp1();
            hp2 = response.body().getHp2();
            phone1 = response.body().getPhone1();
            phone2 = response.body().getPhone2();
            email1 = response.body().getEmail1();
            email2 = response.body().getEmail2();
            customerStatusRef = response.body().getCustomerStatusRef();
            customerStatusName = response.body().getCustomerStatusName();
            userRef = response.body().getUserRef();
            countryName = response.body().getCountryName();
            provinceName = response.body().getProvinceName();
            cityName = response.body().getCityName();

            edtName.setText(name);
            edtAddress.setText(address);
            edtHp1.setText(hp1);
            edtHp2.setText(hp2);
            edtPostCode.setText(postCode);
            edtEmail1.setText(email1);
            edtEmail2.setText(email2);
            edtPhone1.setText(phone1);
            edtPhone2.setText(phone2);
        }

    }

    public void showInfoLogBookFailure(Throwable t) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                if (customerRef.equals("")) {
                    insertContactBankSvc();
                } else {
                    updateContactBankSvc();
                }

                break;
        }
    }


    private void insertContactBankSvc() {
        name = edtName.getText().toString();
        address = edtAddress.getText().toString();
        postCode = edtPostCode.getText().toString();
        hp1 = edtHp1.getText().toString();
        hp2 = edtHp2.getText().toString();
        phone1 = edtPhone1.getText().toString();
        phone2 = edtPhone2.getText().toString();
        email1 = edtEmail1.getText().toString();
        email2 = edtEmail2.getText().toString();

        System.out.printf("email1 ", email1);

        if (name.equals("") || hp1.equals("") || email1.equals("") || (!android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches() && !email1.equals(""))
                || (!android.util.Patterns.EMAIL_ADDRESS.matcher(email2).matches() && !email2.equals(""))) {

            Snackbar snackbar = Snackbar.make(activityAddLogBook, "Silakan lengkapi form.", Snackbar.LENGTH_SHORT);
            snackbar.show();

            if (name.trim().equals("")) {
                edtName.setError(getString(R.string.isEmpty));
            } else {
                edtName.setError(null);
            }

            if (hp1.trim().equals("")) {
                edtHp1.setError(getString(R.string.isEmpty));
            } else {
                edtHp1.setError(null);
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches() && !email1.equals("")) {
                edtEmail1.setError("Email tidak sesuai");
            } else if (email1.trim().equals("")){
                edtEmail1.setError("Email tidak sesuai");
            }
            else {
                edtEmail1.setError(null);
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email2).matches() && !email2.equals("")) {
                edtEmail2.setError("Email tidak sesuai");
            } else {
                edtEmail2.setError(null);
            }

        } else {
            progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
            Map<String, String> fields = new HashMap<>();
            fields.put("memberRef", memberRef);
            fields.put("customerStatus", General.customerStatusNewRef);
            fields.put("customerName", name);
            fields.put("countryCode", countryCode);
            fields.put("provinceCode", provinceCode);
            fields.put("cityCode", cityCode);
            fields.put("address", address);
            fields.put("postCode", postCode);
            fields.put("hp1", hp1);
            fields.put("hp2", hp2);
            fields.put("phone1", phone1);
            fields.put("phone2", phone2);
            fields.put("email1", email1);
            fields.put("email2", email2);
            fields.put("projectCode", General.projectCode);
            presenter.insertContactBankSvc(fields);
        }
    }

    private void updateContactBankSvc() {
        name = edtName.getText().toString();
        address = edtAddress.getText().toString();
        postCode = edtPostCode.getText().toString().trim();
        hp1 = edtHp1.getText().toString().trim();
        hp2 = edtHp2.getText().toString().trim();
        phone1 = edtPhone1.getText().toString().trim();
        phone2 = edtPhone2.getText().toString().trim();
        email1 = edtEmail1.getText().toString().trim();
        email2 = edtEmail2.getText().toString().trim();

        if (name.equals("") || hp1.equals("") || email1.equals("") ||(!android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches() && !email1.equals(""))
                || (!android.util.Patterns.EMAIL_ADDRESS.matcher(email2).matches() && !email2.equals(""))) {

            Snackbar snackbar = Snackbar.make(activityAddLogBook, "Silakan lengkapi form.", Snackbar.LENGTH_SHORT);
            snackbar.show();

            if (name.trim().equals("")) {
                edtName.setError(getString(R.string.isEmpty));
            } else {
                edtName.setError(null);
            }

            if (hp1.trim().equals("")) {
                edtHp1.setError(getString(R.string.isEmpty));
            } else {
                edtHp1.setError(null);
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches() && !email1.equals("")) {
                edtEmail1.setError("Email tidak sesuai");
            } else if (email1.trim().equals("")) {
                edtEmail1.setError(getString(R.string.isEmpty));
            } else {
                edtEmail1.setError(null);
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email2).matches() && !email2.equals("")) {
                edtEmail2.setError("Email tidak sesuai");
            } else {
                edtEmail2.setError(null);
            }

        } else {
            progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
            Map<String, String> fields = new HashMap<>();
            fields.put("memberRef", memberRef);
            fields.put("customerRef", customerRef);
            fields.put("customerStatus", General.customerStatusNewRef);
            fields.put("customerName", name);
            fields.put("countryCode", countryCode);
            fields.put("provinceCode", provinceCode);
            fields.put("cityCode", cityCode);
            fields.put("address", address);
            fields.put("postCode", postCode);
            fields.put("hp1", hp1);
            fields.put("hp2", hp2);
            fields.put("phone1", phone1);
            fields.put("phone2", phone2);
            fields.put("email1", email1);
            fields.put("email2", email2);
            fields.put("projectCode", General.projectCode);
            presenter.updateContactBankSvc(fields);
        }
    }

    public void showResponeResults(Response<ResponeModel> response) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        if (status == 200) {
            finish();
        } else {
            Snackbar snackbar = Snackbar.make(activityAddLogBook, message, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    public void showResponeFailure(Throwable t) {
        progressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(activityAddLogBook, "Terjadi Kesalahan", Snackbar.LENGTH_SHORT);
        snackbar.show();

    }

    public void showResponeUpdateResults(Response<ResponeModel> response) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        if (status == 200) {
            finish();
        } else {
            Snackbar snackbar = Snackbar.make(activityAddLogBook, message, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    public void showResponeUpdateFailure(Throwable t) {
        progressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(activityAddLogBook, "Terjadi Kesalahan", Snackbar.LENGTH_SHORT);
        snackbar.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
