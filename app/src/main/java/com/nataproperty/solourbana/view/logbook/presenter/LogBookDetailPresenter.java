package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.LogBookDetailInteractor;
import com.nataproperty.solourbana.view.logbook.model.TaskModel;
import com.nataproperty.solourbana.view.logbook.ui.LogBookDetailActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LogBookDetailPresenter implements LogBookDetailInteractor {
    private LogBookDetailActivity view;
    private ServiceRetrofitProject service;

    public LogBookDetailPresenter(LogBookDetailActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetContactProjectTaskListSvc(String dbMasterRef, String projectRef, String customerRef) {
        Call<ArrayList<TaskModel>> call = service.getAPI().GetContactProjectTaskListSvc(dbMasterRef,projectRef,customerRef);
        call.enqueue(new Callback<ArrayList<TaskModel>>() {
            @Override
            public void onResponse(Call<ArrayList<TaskModel>> call, Response<ArrayList<TaskModel>> response) {
                view.showListTaskResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<TaskModel>> call, Throwable t) {
                view.showListTaskFailure(t);

            }


        });
    }

}
