package com.nataproperty.solourbana.view.mybooking.ui;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.booking.model.MemberInfoModel;
import com.nataproperty.solourbana.view.mybooking.model.BookingPaymentInfoModel;
import com.nataproperty.solourbana.view.mybooking.presenter.MyBookingInfoPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import retrofit2.Response;

/**
 * Created by UserModel on 8/15/2016.
 */
public class MyBookingInfoActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "MyBookingInfoActivity";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";
    private static final String EXTRA_RX = "EXTRA_RX";
    Toolbar toolbar;
    TextView title;
    SharedPreferences sharedPreferences;
    Typeface font;
    MyBookingInfoPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;
    TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount;
    TextView txtFullname, txtEmail, txtMobile, txtPhone, txtAddress, txtKtpId, txtQty;
    TextView txtPaymentDate, txtPaymentType, txtPaymentAmt, txtAccountBank, txtAccountName,
            txtAccountNomor;
    String paymentDate, paymentType, paymentAmt, accountBank, accountName, accountNomor, status = "", linkOR, linkDownloadOR;
    ImageView imgKtp, imgNpwp;
    String bookingRef;
    String memberCostumerRef, costumerName, costumerMobile, costumerPhone, costumerEmail, costumerAddress,
            linkDownload, linkPdf, bookingCode;
    String ktpid, ktpRef, npwpRef;
    String projectBookingRef, dbMasterRef, projectRef;
    int waitingVerification;
    LinearLayout linearLayoutWaiting, linearLayout;
    Button btnDownload, btnIlustrasi, btnDownloadOR;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;
    private long enqueue;
    private DownloadManager dm;
    BroadcastReceiver receiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_info);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyBookingInfoPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberCostumerRef = sharedPreferences.getString("isMemberCostumerRef", null);
        final Intent intent = getIntent();
        waitingVerification = intent.getIntExtra("waitingVerification", 0);
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        projectBookingRef = intent.getStringExtra(PROJECT_BOOKING_REF);
        bookingCode = intent.getStringExtra("bookingCode");

        linkPdf = intent.getStringExtra("linkPdf");
        linkDownload = intent.getStringExtra("linkDownload");
        linkOR = intent.getStringExtra("linkOR");
        linkDownloadOR = intent.getStringExtra("linkDownloadOR");

        Log.d(TAG, "sp " + linkDownload + " " + linkPdf);
        Log.d(TAG, "or " + linkDownloadOR + " " + linkOR);

        initWidget();

        if (waitingVerification == 1) {
            linearLayoutWaiting.setVisibility(View.VISIBLE);
            btnDownload.setVisibility(View.GONE);
            btnDownloadOR.setVisibility(View.GONE);
        } else {
            linearLayoutWaiting.setVisibility(View.GONE);
            btnDownload.setVisibility(View.VISIBLE);
            btnDownloadOR.setVisibility(View.VISIBLE);
        }

        requestMyBookingInfo();
        requestMemberInfo();
        requestMyBookingDetail();

        //Download manager
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueue);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            Intent i = new Intent();
                            i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
                            startActivity(i);
                            Toast.makeText(MyBookingInfoActivity.this, "Download successed", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(MyBookingInfoActivity.this, "Download failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtPaymentDate = (TextView) findViewById(R.id.txt_payment_date);
        txtPaymentType = (TextView) findViewById(R.id.txt_payment_type);
        txtPaymentAmt = (TextView) findViewById(R.id.txt_payment_amt);
        txtAccountBank = (TextView) findViewById(R.id.txt_account_bank);
        txtAccountName = (TextView) findViewById(R.id.txt_account_name);
        txtAccountNomor = (TextView) findViewById(R.id.txt_account_nomor);

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * informasion customer
         */
        txtFullname = (TextView) findViewById(R.id.txt_fullname);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtMobile = (TextView) findViewById(R.id.txt_mobile);
        txtPhone = (TextView) findViewById(R.id.txt_phone);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtKtpId = (TextView) findViewById(R.id.txt_ktp_id);

        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        linearLayoutWaiting = (LinearLayout) findViewById(R.id.linear_waiting_verification);
        btnIlustrasi = (Button) findViewById(R.id.btn_ilustration);
        btnDownload = (Button) findViewById(R.id.btn_download);
        btnDownloadOR = (Button) findViewById(R.id.btn_download_or);

        btnDownload.setTypeface(font);
        btnDownloadOR.setTypeface(font);
        btnIlustrasi.setTypeface(font);

        btnDownload.setOnClickListener(this);
        btnDownloadOR.setOnClickListener(this);
        btnIlustrasi.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_download:
                //downloadSP true untuk download sp
//                Intent intent1 = new Intent(MyBookingInfoActivity.this, MyBookingViewPDFActivity.class);
//                intent1.putExtra("linkDownload", linkDownload);
//                intent1.putExtra("linkPdf", linkPdf);
//                intent1.putExtra("bookingCode", bookingCode);
//                intent1.putExtra("downloadSP", true);
//                startActivity(intent1);
//                break;

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyBookingInfoActivity.this);
//                alertDialogBuilder.setMessage(R.string.txt_cetak_sp_online);
                LayoutInflater inflater = MyBookingInfoActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_cetak_sp_online, null);
                alertDialogBuilder.setView(dialogView);
                alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //downloadSP true untuk download sp
                        Intent intent1 = new Intent(MyBookingInfoActivity.this, MyBookingViewPDFActivity.class);
                        intent1.putExtra("linkDownload", linkDownload);
                        intent1.putExtra("linkPdf", linkPdf);
                        intent1.putExtra("bookingCode", bookingCode);
                        intent1.putExtra("downloadSP", true);
                        startActivity(intent1);

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;

            case R.id.btn_download_or:
                //downloadSP false untuk download OR
                Intent intent2 = new Intent(MyBookingInfoActivity.this, MyBookingViewPDFActivity.class);
                intent2.putExtra("linkDownloadOR", linkDownloadOR);
                intent2.putExtra("linkOR", linkOR);
                intent2.putExtra("bookingCode", bookingCode);
                intent2.putExtra("downloadSP", false);
                startActivity(intent2);
                break;

            case R.id.btn_ilustration:
                Intent intent = new Intent(MyBookingInfoActivity.this, MyBookingDetailActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                intent.putExtra("payTerm", txtPaymentTerms.getText().toString());
                intent.putExtra("total", txtNetPrice.getText().toString());
                startActivity(intent);
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void requestMyBookingInfo() {
        presenter.getBookingPaymentInfoSvc(bookingRef);
    }

    public void showBookingPaymentInfoResults(Response<BookingPaymentInfoModel> response) {
        paymentType = response.body().getPaymentTypeName();
        paymentDate = response.body().getPaymentDate();
        paymentAmt = response.body().getPaymentAmt();
        accountBank = response.body().getBankName();
        accountName = response.body().getBankAccName();
        accountNomor = response.body().getBankAccNo();

        txtPaymentType.setText(paymentType);
        txtPaymentDate.setText(paymentDate);
        txtPaymentAmt.setText(paymentAmt);
        txtAccountBank.setText(accountBank);
        txtAccountName.setText(accountName);
        txtAccountNomor.setText(accountNomor);
    }

    public void showBookingPaymentInfoFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void requestMemberInfo() {
        presenter.getMemberInfoSvc(memberCostumerRef);
    }

    public void showMemberInfoResults(Response<MemberInfoModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();

        if (status == 200) {
            costumerName = response.body().getData().getName();
            costumerMobile = response.body().getData().gethP1();
            costumerPhone = response.body().getData().getPhone1();
            costumerEmail = response.body().getData().getEmail1();
            costumerAddress = response.body().getData().getIdAddr();
            ktpid = response.body().getData().getKtpid();
            ktpRef = response.body().getData().getKtpRef();
            npwpRef = response.body().getData().getNpwpRef();

            txtFullname.setText(costumerName);
            txtMobile.setText(costumerMobile);
            txtPhone.setText(costumerPhone);
            txtEmail.setText(costumerEmail);
            txtAddress.setText(costumerAddress);
            txtKtpId.setText(ktpid);

            Glide.with(MyBookingInfoActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgKtp);

            Glide.with(MyBookingInfoActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgNpwp);
        } else {
            Log.d("error", " " + message);

        }
    }

    public void showMemberInfoFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void requestMyBookingDetail() {
        presenter.getBookingDetailSvc(dbMasterRef, projectRef, projectBookingRef);
    }

    public void showBookingInfoResults(Response<DetailBooking> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            String bookDate = response.body().getBookingInfo().getBookDate();
            String salesReferral = response.body().getBookingInfo().getSalesReferral();
            String salesEvent = response.body().getBookingInfo().getSalesEvent();
            String purpose = response.body().getBookingInfo().getPurpose();
            String salesLocation = response.body().getBookingInfo().getSalesLocation();
            String category = response.body().getUnitInfo().getCategory();
            String detail = response.body().getUnitInfo().getDetail();
            String cluster = response.body().getUnitInfo().getCluster();
            String block = response.body().getUnitInfo().getBlock();
            String product = response.body().getUnitInfo().getProduct();
            String unitName = response.body().getUnitInfo().getUnitName();
            String projectName = response.body().getUnitInfo().getProjectName();
            String area = response.body().getUnitInfo().getArea();
            String total = response.body().getUnitInfo().getTotal();
            String payTerm = response.body().getUnitInfo().getPayTerm();

                /*txtBookDate.setText(bookDate);
                txtSalesReferral.setText(salesReferral);
                txtSalesEvent.setText(salesEvent);
                txtPurpose.setText(purpose);
                txtSalesLocation.setText(salesLocation);*/

            txtPropertyName.setText(projectName);
            txtCategoryType.setText(category + " | " + cluster);
            txtProduct.setText(product);
            txtUnitNo.setText(block + " - " + unitName);
            txtArea.setText(Html.fromHtml(area));

            txtPaymentTerms.setText(payTerm);
            txtNetPrice.setText(total);

            Log.d("price", payTerm + " " + total);

        } else {
            Log.d(TAG, "get error");
        }
    }

    public void showBookingInfoFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingInfoActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
