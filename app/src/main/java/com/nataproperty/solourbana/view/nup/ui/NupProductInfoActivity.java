package com.nataproperty.solourbana.view.nup.ui;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.nup.adapter.PaymentTypeAdapter;
import com.nataproperty.solourbana.view.nup.adapter.UnitFloorAdapter;
import com.nataproperty.solourbana.view.nup.adapter.UnitTypeAdapter;
import com.nataproperty.solourbana.view.nup.adapter.UnitViewAdapter;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.nup.model.PaymentTermModel;
import com.nataproperty.solourbana.view.nup.model.UnitFloorModel;
import com.nataproperty.solourbana.view.nup.model.UnitTypeModel;
import com.nataproperty.solourbana.view.nup.model.UnitViewModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NupProductInfoActivity extends AppCompatActivity {
    public static final String TAG = "NupProductInfoActivity";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String NUP_AMT = "nupAmt";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String PROJECT_NAME = "projectName";

    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String BIRTHDATE = "birthdate";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";
    public static final String QTY = "qty";
    public static final String TOTAL = "total";

    public static final String FILE_CODE_KTP = "fileCodeKtp";
    public static final String FILE_CODE_NPWP = "fileCodeNpwp";
    public static final String URL_DOC_KTP = "urlDocKtp";
    public static final String URL_DOC_NPWP = "urlDocNpwp";

    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address, qty, total, birthdate,npwp;
    private String dbMasterRef, projectRef, projectName, memberRef, memberCustomerRef, nupAmt, projectDescription, status;
    private String fileCodeKtp, fileCodeNpwp,urlDocKtp, urlDocNpwp,flaq;

    //logo
    private ImageView imgLogo;
    private TextView txtProjectName;

    private List<UnitViewModel> listUnitView = new ArrayList<>();
    private UnitViewAdapter unitViewAdapter;
    private UnitViewModel unitViewModel;
    private String surveyUnitViewRef,surveyUnitViewName;

    private List<UnitFloorModel> listUnitFloor = new ArrayList<>();
    private UnitFloorAdapter unitFloorAdapter;
    private UnitFloorModel unitFloorModel;
    private String surveyUnitFloorRef,surveyUnitFloorName;

    private List<UnitTypeModel> listUnitType = new ArrayList<>();
    private UnitTypeAdapter unitTypeAdapter;
    private UnitTypeModel unitTypeModel;
    private String surveyUnitTypeRef,surveyUnitTypeName;

    private List<PaymentTermModel> listpaymentTerm = new ArrayList<>();
    private PaymentTypeAdapter paymentTypeAdapter;
    private PaymentTermModel paymentTermModel;
    private String surveyPaymentTermRef,surveyPaymentTermName;

    int flagg = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_product_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Survey / Preferensi");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        nupAmt = intent.getStringExtra(NUP_AMT);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);
        projectName = intent.getStringExtra(PROJECT_NAME);

        fullname = intent.getStringExtra(FULLNAME);
        birthdate = intent.getStringExtra(BIRTHDATE);
        mobile1 = intent.getStringExtra(MOBILE);
        phone1 = intent.getStringExtra(PHONE);
        email1 = intent.getStringExtra(EMAIL);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);
        ktpRef = intent.getStringExtra(KTP_REF);
        npwpRef = intent.getStringExtra(NPWP_REF);
        qty = intent.getStringExtra(QTY);
        total = intent.getStringExtra(TOTAL);
        npwp = intent.getStringExtra("npwp");

        fileCodeKtp = intent.getStringExtra(FILE_CODE_KTP);
        fileCodeNpwp = intent.getStringExtra(FILE_CODE_NPWP);
        urlDocKtp = intent.getStringExtra(URL_DOC_KTP);
        urlDocNpwp = intent.getStringExtra(URL_DOC_NPWP);

        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        /*SpinnerUnitType*/
        Spinner spinnerUnitView = (Spinner) findViewById(R.id.spn_unit_view);
        Spinner spinnerUnitFloor = (Spinner) findViewById(R.id.spn_unit_floor);
        Spinner spinnerUnitType = (Spinner) findViewById(R.id.spn_unit_type);
        Spinner spinnerPaymentTerm = (Spinner) findViewById(R.id.spn_payment_term);

        Button btnNext = (Button) findViewById(R.id.btn_next);

        unitViewAdapter = new UnitViewAdapter(this, listUnitView);
        spinnerUnitView.setAdapter(unitViewAdapter);
        spinnerUnitView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (flagg == 1) {
                    surveyUnitViewRef = String.valueOf(listUnitView.get(position).getSurveyUnitViewRef());
                    surveyUnitViewName = String.valueOf(listUnitView.get(position).getSurveyUnitViewName());
                } else {
                    flagg = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        unitFloorAdapter = new UnitFloorAdapter(this, listUnitFloor);
        spinnerUnitFloor.setAdapter(unitFloorAdapter);
        spinnerUnitFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (flagg == 1) {
                    surveyUnitFloorRef = String.valueOf(listUnitFloor.get(position).getSurveyUnitFloorRef());
                    surveyUnitFloorName = String.valueOf(listUnitFloor.get(position).getSurveyUnitFloorName());
                } else {
                    flagg = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        unitTypeAdapter = new UnitTypeAdapter(this, listUnitType);
        spinnerUnitType.setAdapter(unitTypeAdapter);
        spinnerUnitType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (flagg == 1) {
                    surveyUnitTypeRef = String.valueOf(listUnitType.get(position).getSurveyUnitTypeRef());
                    surveyUnitTypeName = String.valueOf(listUnitType.get(position).getSurveyUnitTypeName());
                } else {
                    flagg = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        paymentTypeAdapter = new PaymentTypeAdapter(this, listpaymentTerm);
        spinnerPaymentTerm.setAdapter(paymentTypeAdapter);
        spinnerPaymentTerm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (flagg == 1) {
                    surveyPaymentTermRef = String.valueOf(listpaymentTerm.get(position).getSurveyPaymentTermRef());
                    surveyPaymentTermName = String.valueOf(listpaymentTerm.get(position).getSurveyPaymentTermName());
                } else {
                    flagg = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NupProductInfoActivity.this,NupReviewOrderActivity.class);
                intent.putExtra("memberCustomerRef", memberCustomerRef);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(NUP_AMT, nupAmt);
                intent.putExtra(PROJECT_DESCRIPTION, projectDescription);
                intent.putExtra(PROJECT_NAME, projectName);

                intent.putExtra(FULLNAME, fullname);
                intent.putExtra(BIRTHDATE, birthdate);
                intent.putExtra(EMAIL, email1);
                intent.putExtra(MOBILE, mobile1);
                intent.putExtra(PHONE,phone1);
                intent.putExtra(ADDRESS,address);
                intent.putExtra(KTP_ID, ktpid);
                intent.putExtra(KTP_REF, ktpRef);
                intent.putExtra(NPWP_REF, npwpRef);
                intent.putExtra(QTY, qty);
                intent.putExtra(TOTAL, total);
                intent.putExtra("npwp", npwp);

                intent.putExtra("surveyUnitTypeRef", surveyUnitTypeRef);
                intent.putExtra("surveyPaymentTermRef", surveyPaymentTermRef);
                intent.putExtra("surveyUnitViewRef", surveyUnitViewRef);
                intent.putExtra("surveyUnitFloorRef", surveyUnitFloorRef);
                intent.putExtra(FILE_CODE_NPWP,fileCodeNpwp);
                intent.putExtra(FILE_CODE_KTP, fileCodeKtp);
                intent.putExtra(URL_DOC_NPWP, urlDocNpwp);
                intent.putExtra(URL_DOC_KTP, urlDocKtp);

                startActivity(intent);
            }
        });


        GetLookupSurveyNUP();
    }

    private void GetLookupSurveyNUP() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.GetLookupSurveyNUP(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArrayUnitType = new JSONArray(jsonObject.getJSONArray("surveyUnitType").toString());
                    generateUnitType(jsonArrayUnitType);

                    JSONArray jsonArrayPaymentTerm = new JSONArray(jsonObject.getJSONArray("surveyPaymentTerm").toString());
                    generatePaymentTerm(jsonArrayPaymentTerm);

                    JSONArray jsonArrayUnitView = new JSONArray(jsonObject.getJSONArray("surveyUnitView").toString());
                    generateUnitView(jsonArrayUnitView);

                    JSONArray jsonArrayUnitFloor = new JSONArray(jsonObject.getJSONArray("surveyUnitFloor").toString());
                    generateUnitFloor(jsonArrayUnitFloor);

//                    BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();
                    Log.d(TAG,"length "+jsonArrayUnitType.length()+" "+jsonArrayPaymentTerm.length()+" "+jsonArrayUnitView.length()+" "+jsonArrayUnitFloor.length());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupProductInfoActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupProductInfoActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestLookupSurveyUnitType");

    }

    private void generateUnitType(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                UnitTypeModel unitTypeModel = new UnitTypeModel();
                unitTypeModel.setSurveyUnitTypeRef(jo.getString("surveyUnitTypeRef"));
                unitTypeModel.setSurveyUnitTypeName(jo.getString("surveyUnitTypeName"));
                listUnitType.add(unitTypeModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        unitTypeAdapter.notifyDataSetChanged();

    }

    private void generatePaymentTerm(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonObject1 = response.getJSONObject(i);
                PaymentTermModel paymentTermModel = new PaymentTermModel();
                paymentTermModel.setSurveyPaymentTermRef(jsonObject1.getString("surveyPaymentTermRef"));
                paymentTermModel.setSurveyPaymentTermName(jsonObject1.getString("surveyPaymentTermName"));
                listpaymentTerm.add(paymentTermModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        paymentTypeAdapter.notifyDataSetChanged();

    }

    private void generateUnitView(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                UnitViewModel unitViewModel = new UnitViewModel();
                unitViewModel.setSurveyUnitViewRef(jo.getString("surveyUnitViewRef"));
                unitViewModel.setSurveyUnitViewName(jo.getString("surveyUnitViewName"));
                listUnitView.add(unitViewModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        unitViewAdapter.notifyDataSetChanged();

    }

    private void generateUnitFloor(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonObject = response.getJSONObject(i);
                UnitFloorModel unitFloorModel = new UnitFloorModel();
                unitFloorModel.setSurveyUnitFloorRef(jsonObject.getString("surveyUnitFloorRef"));
                unitFloorModel.setSurveyUnitFloorName(jsonObject.getString("surveyUnitFloorName"));
                listUnitFloor.add(unitFloorModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        unitFloorAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupProductInfoActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
