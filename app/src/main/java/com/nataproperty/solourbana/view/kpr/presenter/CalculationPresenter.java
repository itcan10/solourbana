package com.nataproperty.solourbana.view.kpr.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.kpr.intractor.PresenterCalculationInteractor;
import com.nataproperty.solourbana.view.kpr.model.CalculationModel;
import com.nataproperty.solourbana.view.kpr.ui.CalculationActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CalculationPresenter implements PresenterCalculationInteractor {
    private CalculationActivity view;
    private ServiceRetrofit service;

    public CalculationPresenter(CalculationActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListCal(String memberRef) {
        Call<List<CalculationModel>> call = service.getAPI().getListCal(memberRef);
        call.enqueue(new Callback<List<CalculationModel>>() {
            @Override
            public void onResponse(Call<List<CalculationModel>> call, Response<List<CalculationModel>> response) {
                view.showListCalResults(response);
            }

            @Override
            public void onFailure(Call<List<CalculationModel>> call, Throwable t) {
                view.showListCalFailure(t);

            }


        });
    }

}
