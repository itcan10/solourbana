package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by UserModel on 7/1/2016.
 */
public class CalculationModel {
    String project,cluster,product,block,unit,calcUserProjectRef,price,countTemp;

    public String getCountTemp() {
        return countTemp;
    }

    public void setCountTemp(String countTemp) {
        this.countTemp = countTemp;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCalcUserProjectRef() {
        return calcUserProjectRef;
    }

    public void setCalcUserProjectRef(String calcUserProjectRef) {
        this.calcUserProjectRef = calcUserProjectRef;
    }
}
