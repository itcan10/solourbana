package com.nataproperty.solourbana.view.news.ui;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.view.news.adapter.NewsDetailImageAdapter;
import com.nataproperty.solourbana.view.news.model.NewsDetailModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

public class NewsDetailImageActivity extends AppCompatActivity {
    public static final String TAG = "NewsDetailImageActivity";

    public static final String CONTENT_REF = "contentRef";
    public static final String POSISION = "position";

    ViewPager viewPager;

    private List<NewsDetailModel> listImage = new ArrayList<NewsDetailModel>();
    private NewsDetailImageAdapter adapter;

    String contentRef;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail_image);

        Intent intent = getIntent();
        contentRef = intent.getStringExtra(CONTENT_REF);
        position = intent.getIntExtra(POSISION,0);

        Log.d(TAG,""+contentRef+" "+position);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        listImage.clear();
        requestImage();

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new NewsDetailImageAdapter(this,listImage);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
        viewPager.setCurrentItem(position);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/0.8;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        viewPager.setLayoutParams(params);
        viewPager.requestLayout();
    }

    public void requestImage() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getImageSlider(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result projectttt",response);
                    generateListImage(jsonArray);

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NewsDetailImageActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NewsDetailImageActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("contentRef", contentRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "newsImage");

    }

    private void generateListImage(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                NewsDetailModel newsDetailModel = new NewsDetailModel();
                newsDetailModel.setImageSlider(jo.getString("imageSlider"));

                listImage.add(newsDetailModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();

    }

}
