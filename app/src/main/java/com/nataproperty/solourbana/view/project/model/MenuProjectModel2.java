package com.nataproperty.solourbana.view.project.model;

/**
 * Created by herlambang-nata on 11/2/2016.
 */
public class MenuProjectModel2 {
    long dbMasterRef;
    String projectRef;
    String projectName;
    String projectDescription;
    String countCategory;
    String countCluster;
    String categoryRef;
    String clusterRef;
    String salesStatus;
    String isBooking;
    String isNUP;
    String isShowAvailableUnit;
    Double latitude;
    Double longitude;
    String nupAmt;
    String linkProjectDetail;
    String urlVideo;
    String imageLogo;
    String bookingContact;
    String downloadProjectInfo;
    String isJoin;
    String isWaiting;
    String clusterDescription;
    String priceRange;
    String countPromo;
    String memberCode;
    String isGuest;
    String psCode;


    public String getProjectPhone() {
        return projectPhone;
    }

    public void setProjectPhone(String projectPhone) {
        this.projectPhone = projectPhone;
    }

    public String getProjectEmail() {
        return projectEmail;
    }

    public void setProjectEmail(String projectEmail) {
        this.projectEmail = projectEmail;
    }

    public String getProjectWhatsapp() {
        return projectWhatsapp;
    }

    public void setProjectWhatsapp(String projectWhatsapp) {
        this.projectWhatsapp = projectWhatsapp;
    }

    String projectPhone;
    String projectEmail;
    String projectWhatsapp;

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public String getCountPromo() {
        return countPromo;
    }

    public void setCountPromo(String countPromo) {
        this.countPromo = countPromo;
    }

    public String getClusterDescription() {
        return clusterDescription;
    }

    public void setClusterDescription(String clusterDescription) {
        this.clusterDescription = clusterDescription;
    }

    public String getDownloadProjectInfo() {
        return downloadProjectInfo;
    }

    public void setDownloadProjectInfo(String downloadProjectInfo) {
        this.downloadProjectInfo = downloadProjectInfo;
    }

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getCountCategory() {
        return countCategory;
    }

    public void setCountCategory(String countCategory) {
        this.countCategory = countCategory;
    }

    public String getCountCluster() {
        return countCluster;
    }

    public void setCountCluster(String countCluster) {
        this.countCluster = countCluster;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getSalesStatus() {
        return salesStatus;
    }

    public void setSalesStatus(String salesStatus) {
        this.salesStatus = salesStatus;
    }

    public String getIsBooking() {
        return isBooking;
    }

    public void setIsBooking(String isBooking) {
        this.isBooking = isBooking;
    }

    public String getIsNUP() {
        return isNUP;
    }

    public void setIsNUP(String isNUP) {
        this.isNUP = isNUP;
    }

    public String getIsShowAvailableUnit() {
        return isShowAvailableUnit;
    }

    public void setIsShowAvailableUnit(String isShowAvailableUnit) {
        this.isShowAvailableUnit = isShowAvailableUnit;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getNupAmt() {
        return nupAmt;
    }

    public void setNupAmt(String nupAmt) {
        this.nupAmt = nupAmt;
    }

    public String getLinkProjectDetail() {
        return linkProjectDetail;
    }

    public void setLinkProjectDetail(String linkProjectDetail) {
        this.linkProjectDetail = linkProjectDetail;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getImageLogo() {
        return imageLogo;
    }

    public void setImageLogo(String imageLogo) {
        this.imageLogo = imageLogo;
    }

    public String getBookingContact() {
        return bookingContact;
    }

    public void setBookingContact(String bookingContact) {
        this.bookingContact = bookingContact;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getIsJoin() {
        return isJoin;
    }

    public void setIsJoin(String isJoin) {
        this.isJoin = isJoin;
    }

    public String getIsWaiting() {
        return isWaiting;
    }

    public void setIsWaiting(String isWaiting) {
        this.isWaiting = isWaiting;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getIsGuest() {
        return isGuest;
    }

    public void setIsGuest(String isGuest) {
        this.isGuest = isGuest;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }
}
