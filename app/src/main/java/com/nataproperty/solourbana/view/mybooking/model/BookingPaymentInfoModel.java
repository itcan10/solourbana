package com.nataproperty.solourbana.view.mybooking.model;

/**
 * Created by Nata on 1/23/2017.
 */

public class BookingPaymentInfoModel {
    int status;
    String message, urlSchedule, paymentTypeName, paymentDate, paymentAmt, bankName, bankAccName, bankAccNo, linkDonwload;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrlSchedule() {
        return urlSchedule;
    }

    public void setUrlSchedule(String urlSchedule) {
        this.urlSchedule = urlSchedule;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentAmt() {
        return paymentAmt;
    }

    public void setPaymentAmt(String paymentAmt) {
        this.paymentAmt = paymentAmt;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccName() {
        return bankAccName;
    }

    public void setBankAccName(String bankAccName) {
        this.bankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getLinkDonwload() {
        return linkDonwload;
    }

    public void setLinkDonwload(String linkDonwload) {
        this.linkDonwload = linkDonwload;
    }
}
