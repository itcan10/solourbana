package com.nataproperty.solourbana.view.project.model;

/**
 * Created by herlambang-nata on 10/31/2016.
 */
public class IlustrationProductModel {
    long dbMasterRef;
    String projectRef;
    String categoryRef;
    String clusterRef;
    String clusterDescription;
    String priceRangeStart;
    String priceRangeEnd;
    String isAllowCalculatePrice;
    String isNUP;
    String isBooking;
    String isShowAvailableUnit;
    String isShowDiagramMaticUnit;
    String isShowSitePlan;
    String urlSitePlan;



    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getClusterDescription() {
        return clusterDescription;
    }

    public void setClusterDescription(String clusterDescription) {
        this.clusterDescription = clusterDescription;
    }

    public String getPriceRangeStart() {
        return priceRangeStart;
    }

    public void setPriceRangeStart(String priceRangeStart) {
        this.priceRangeStart = priceRangeStart;
    }

    public String getPriceRangeEnd() {
        return priceRangeEnd;
    }

    public void setPriceRangeEnd(String priceRangeEnd) {
        this.priceRangeEnd = priceRangeEnd;
    }

    public String getIsAllowCalculatePrice() {
        return isAllowCalculatePrice;
    }

    public void setIsAllowCalculatePrice(String isAllowCalculatePrice) {
        this.isAllowCalculatePrice = isAllowCalculatePrice;
    }

    public String getIsNUP() {
        return isNUP;
    }

    public void setIsNUP(String isNUP) {
        this.isNUP = isNUP;
    }

    public String getIsBooking() {
        return isBooking;
    }

    public void setIsBooking(String isBooking) {
        this.isBooking = isBooking;
    }

    public String getIsShowAvailableUnit() {
        return isShowAvailableUnit;
    }

    public void setIsShowAvailableUnit(String isShowAvailableUnit) {
        this.isShowAvailableUnit = isShowAvailableUnit;
    }

    public String getIsShowDiagramMaticUnit() {
        return isShowDiagramMaticUnit;
    }

    public void setIsShowDiagramMaticUnit(String isShowDiagramMaticUnit) {
        this.isShowDiagramMaticUnit = isShowDiagramMaticUnit;
    }

    public String getIsShowSitePlan() {
        return isShowSitePlan;
    }

    public void setIsShowSitePlan(String isShowSitePlan) {
        this.isShowSitePlan = isShowSitePlan;
    }

    public String getUrlSitePlan() {
        return urlSitePlan;
    }

    public void setUrlSitePlan(String urlSitePlan) {
        this.urlSitePlan = urlSitePlan;
    }
}
