package com.nataproperty.solourbana.view.commision.model;

/**
 * Created by Nata on 1/4/2017.
 */

public class CommisionDetailModel {
    String payOrderCode,comPayNo,schedDate,comAmt,pphAmt,ppnAmt,netPaidAmt, paidDate;

    public String getPayOrderCode() {
        return payOrderCode;
    }

    public void setPayOrderCode(String payOrderCode) {
        this.payOrderCode = payOrderCode;
    }

    public String getComPayNo() {
        return comPayNo;
    }

    public void setComPayNo(String comPayNo) {
        this.comPayNo = comPayNo;
    }

    public String getSchedDate() {
        return schedDate;
    }

    public void setSchedDate(String schedDate) {
        this.schedDate = schedDate;
    }

    public String getComAmt() {
        return comAmt;
    }

    public void setComAmt(String comAmt) {
        this.comAmt = comAmt;
    }

    public String getPphAmt() {
        return pphAmt;
    }

    public void setPphAmt(String pphAmt) {
        this.pphAmt = pphAmt;
    }

    public String getPpnAmt() {
        return ppnAmt;
    }

    public void setPpnAmt(String ppnAmt) {
        this.ppnAmt = ppnAmt;
    }

    public String getNetPaidAmt() {
        return netPaidAmt;
    }

    public void setNetPaidAmt(String netPaidAmt) {
        this.netPaidAmt = netPaidAmt;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }
}
