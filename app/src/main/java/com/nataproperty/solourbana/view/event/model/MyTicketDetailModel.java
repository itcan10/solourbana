package com.nataproperty.solourbana.view.event.model;

/**
 * Created by UserModel on 7/28/2016.
 */
public class MyTicketDetailModel {
    String rsvpRef,linkCode,guestName;

    public String getRsvpRef() {
        return rsvpRef;
    }

    public void setRsvpRef(String rsvpRef) {
        this.rsvpRef = rsvpRef;
    }

    public String getLinkCode() {
        return linkCode;
    }

    public void setLinkCode(String linkCode) {
        this.linkCode = linkCode;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

}
