package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.RegionModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class RegionAdapter extends BaseAdapter {
    private Context context;
    private List<RegionModel> list;
    private ListRegionHolder holder;

    public RegionAdapter(Context context, List<RegionModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_region,null);
            holder = new ListRegionHolder();
            holder.regionName = (TextView) convertView.findViewById(R.id.txt_region);

            convertView.setTag(holder);
        }else{
            holder = (ListRegionHolder) convertView.getTag();
        }
        RegionModel nation = list.get(position);
        holder.regionName.setText(nation.getRegionName());

        return convertView;
    }

    private class ListRegionHolder {
        TextView regionName;
    }
}
