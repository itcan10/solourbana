package com.nataproperty.solourbana.view.mybooking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyBookingSectionInteractor {
    void getListBookingSection(String memberRef,String projectCode);

}
