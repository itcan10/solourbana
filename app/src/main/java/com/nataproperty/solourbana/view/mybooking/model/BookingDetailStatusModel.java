package com.nataproperty.solourbana.view.mybooking.model;

/**
 * Created by nata on 11/24/2016.
 */

public class BookingDetailStatusModel {
    int status;
    String message;
    BookingDetailModel bookingInfo;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BookingDetailModel getBookingInfo() {
        return bookingInfo;
    }

    public void setBookingInfo(BookingDetailModel bookingInfo) {
        this.bookingInfo = bookingInfo;
    }
}
