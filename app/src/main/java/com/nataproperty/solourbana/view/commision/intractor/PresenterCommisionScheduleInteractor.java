package com.nataproperty.solourbana.view.commision.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCommisionScheduleInteractor {
    void GetListCommissionCustProjectSvc(String dbMasterRef, String projectRef, String bookingRef, String memberRef);

}
