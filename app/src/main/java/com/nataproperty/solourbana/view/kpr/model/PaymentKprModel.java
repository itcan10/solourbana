package com.nataproperty.solourbana.view.kpr.model;

import com.nataproperty.solourbana.view.ilustration.model.PaymentModel;

import java.util.List;

/**
 * Created by nata on 11/23/2016.
 */
public class PaymentKprModel {
    String paymentTerm;
    String priceInc;
    String discPercent;
    String discAmt;
    String netPrice;
    String termCondition;
    String totalPayment;

    PropertyInfoKprModel propertyInfo;
    List<PaymentModel> dataSchedule;
    List<KprTableModel> dataKPR;

    public List<KprTableModel> getDataKPR() {
        return dataKPR;
    }

    public void setDataKPR(List<KprTableModel> dataKPR) {
        this.dataKPR = dataKPR;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPriceInc() {
        return priceInc;
    }

    public void setPriceInc(String priceInc) {
        this.priceInc = priceInc;
    }

    public String getDiscPercent() {
        return discPercent;
    }

    public void setDiscPercent(String discPercent) {
        this.discPercent = discPercent;
    }

    public String getDiscAmt() {
        return discAmt;
    }

    public void setDiscAmt(String discAmt) {
        this.discAmt = discAmt;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    public String getTermCondition() {
        return termCondition;
    }

    public void setTermCondition(String termCondition) {
        this.termCondition = termCondition;
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public PropertyInfoKprModel getPropertyInfo() {
        return propertyInfo;
    }

    public void setPropertyInfo(PropertyInfoKprModel propertyInfo) {
        this.propertyInfo = propertyInfo;
    }

    public List<PaymentModel> getDataSchedule() {
        return dataSchedule;
    }

    public void setDataSchedule(List<PaymentModel> dataSchedule) {
        this.dataSchedule = dataSchedule;
    }
}
