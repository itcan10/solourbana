package com.nataproperty.solourbana.view.profile.model;

/**
 * Created by UserModel on 4/25/2016.
 */
public class NationModel {

    String nationRef;
    String nationName;
    String nationSortNo;

    public String getNationName() {
        return nationName;
    }

    public void setNationName(String nationName) {
        this.nationName = nationName;
    }

    public String getNationRef() {
        return nationRef;
    }

    public void setNationRef(String nationRef) {
        this.nationRef = nationRef;
    }

    public String getNationSortNo() {
        return nationSortNo;
    }

    public void setNationSortNo(String nationSortNo) {
        this.nationSortNo = nationSortNo;
    }
}
