package com.nataproperty.solourbana.view.logbook.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.adapter.LogBookProjectAdapter;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.presenter.LogBookProjectPresenter;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Response;

public class LogBookProjectActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private LogBookProjectPresenter presenter;
    ListView listView;
    LogBookProjectAdapter adapter;
    ArrayList<LogBookModel> list = new ArrayList<>();
    Toolbar toolbar;
    TextView title, noData;
    Typeface font;
    SharedPreferences sharedPreferences;
    String memberRef, keyword = "", projectRef, dbMasterRef, referralRef = "", eventRef = "",
            projectCustomerStatus, projectCustomerStatusName, name, hp1, email1, address, province,
            city, eventName, referralName, color, customerRef;
    ProgressDialog progressDialog;
    EditText edtsearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_book_project);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new LogBookProjectPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent i = getIntent();
        dbMasterRef = i.getStringExtra(DBMASTER_REF);
        projectRef = i.getStringExtra(PROJECT_REF);
        projectCustomerStatus = i.getStringExtra("projectCustomerStatus");
        projectCustomerStatusName = i.getStringExtra("projectCustomerStatusName");
        color = i.getStringExtra("color");
        requestLogBook();
        initWidget();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.menu_log_book) + " - " + projectCustomerStatusName);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.list_log_book);
        noData = (TextView) findViewById(R.id.txt_no_data);
        listView.setOnItemClickListener(this);
        edtsearch = (EditText) findViewById(R.id.edt_search);

        edtsearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = edtsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void requestLogBook() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetContactBankProjectSvc(dbMasterRef, projectRef, memberRef, projectCustomerStatus, eventRef, referralRef, keyword);
    }

    public void showListLogBookResults(Response<ArrayList<LogBookModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            list = response.body();
            if (list.size() > 0) {
                initAdapter();
                noData.setVisibility(View.GONE);
            } else {
                noData.setVisibility(View.VISIBLE);
                listView.setEmptyView(noData);
            }

        }
    }

    public void showListLogBookFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new LogBookProjectAdapter(this, list, color);
        listView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == listView) {
            name = list.get(position).getName();
            hp1 = list.get(position).getHp1();
            email1 = list.get(position).getEmail1();
            province = list.get(position).getProvinceName();
            city = list.get(position).getCityName();
            address = list.get(position).getAddress();
            projectCustomerStatusName = list.get(position).getProjectCustomerStatusName();
            eventName = list.get(position).getEventName();
            referralName = list.get(position).getReferralName();
            customerRef = list.get(position).getCustomerRef();
            projectCustomerStatus = list.get(position).getProjectCustomerStatus();

            Intent i = new Intent(LogBookProjectActivity.this, LogBookDetailActivity.class);
            i.putExtra("dbMasterRef", dbMasterRef);
            i.putExtra("projectRef", projectRef);
            i.putExtra("customerRef", customerRef);
            i.putExtra("name", name);
            i.putExtra("hp1", hp1);
            i.putExtra("email1", email1);
            i.putExtra("province", province);
            i.putExtra("city", city);
            i.putExtra("address", address);
            i.putExtra("projectCustomerStatusName", projectCustomerStatusName);
            i.putExtra("eventName", eventName);
            i.putExtra("referralName", referralName);
            i.putExtra("projectCustomerStatus", projectCustomerStatus);
            i.putExtra("color", color);
            startActivity(i);
        }

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_search:
                Intent intent = new Intent(LogBookProjectActivity.this, SearchLogBookProjectActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra("projectCustomerStatus", projectCustomerStatus);
                intent.putExtra("color", color);
                startActivity(intent);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
