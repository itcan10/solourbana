package com.nataproperty.solourbana.view.commision.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.commision.model.CommisionScheduleModel;

import java.util.List;

/**
 * Created by UserModel on 4/21/2016.
 */
public class CommisionScheduleAdapter extends BaseAdapter {
    public static final String TAG = "MyNupAdapter";

    private Context context;
    private List<CommisionScheduleModel> list;
    private ListMyNupHolder holder;

    public CommisionScheduleAdapter(Context context, List<CommisionScheduleModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_commision_schedule, null);
            holder = new ListMyNupHolder();
            holder.dueDate = (TextView) convertView.findViewById(R.id.txt_dueDate);
            holder.tipe = (TextView) convertView.findViewById(R.id.txt_tipe);
            holder.jumlah = (TextView) convertView.findViewById(R.id.txt_jumlah);
            holder.outstanding = (TextView) convertView.findViewById(R.id.txt_outstanding);

            convertView.setTag(holder);
        } else {
            holder = (ListMyNupHolder) convertView.getTag();
        }

        CommisionScheduleModel commisionScheduleModel = list.get(position);
        holder.dueDate.setText(commisionScheduleModel.getDueDate());
        holder.tipe.setText(commisionScheduleModel.getTipe());
        holder.jumlah.setText(commisionScheduleModel.getJumlah());
        holder.outstanding.setText(commisionScheduleModel.getOutstanding());

        return convertView;
    }

    public class ListMyNupHolder {
        TextView dueDate, tipe, jumlah, outstanding;

    }
}