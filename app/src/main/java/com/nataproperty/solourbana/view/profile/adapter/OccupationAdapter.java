package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.OccupationModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class OccupationAdapter extends BaseAdapter {
    private Context context;
    private List<OccupationModel> list;
    private ListOccupationHolder holder;

    public OccupationAdapter(Context context, List<OccupationModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_occupation,null);
            holder = new ListOccupationHolder();
            holder.occupationName = (TextView) convertView.findViewById(R.id.txt_occupation);

            convertView.setTag(holder);
        }else{
            holder = (ListOccupationHolder) convertView.getTag();
        }
        OccupationModel nation = list.get(position);
        holder.occupationName.setText(nation.getOccupationName());

        return convertView;
    }

    private class ListOccupationHolder {
        TextView occupationName;
    }
}
