package com.nataproperty.solourbana.view.loginregister.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationAddKPRActivity;
import com.nataproperty.solourbana.view.kpr.model.KprModel;
import com.nataproperty.solourbana.view.loginregister.adapter.AgenTypeAdapter;
import com.nataproperty.solourbana.view.loginregister.model.AgenTypeModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/14/2016.
 */
public class RegisterAgentTypeActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String AGENTY_TYPE = "agentType";
    public static final String AGENTY_TYPE_NAME = "agentTypeName";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String MESSAGE = "message";
    public static final String STATUS_GOOGLE_SIGN_IN = "statusGoogleSignIn";

    private List<AgenTypeModel> listAgentType = new ArrayList<AgenTypeModel>();
    private AgenTypeAdapter adapter;

    private ListView listView;

    String fullname, birthDate, phone, email, password;
    String agentType, agentTypeName;

    SharedPreferences sharedPreferences;

    ProgressDialog progressDialog;
    String message, statusGoogleSignIn;

    private AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_agent_type);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_register_agent_type));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        fullname = intent.getStringExtra("fullname");
        birthDate = intent.getStringExtra("brithDate");
        phone = intent.getStringExtra("phone");
        email = intent.getStringExtra("email");
        password = intent.getStringExtra("password");
        statusGoogleSignIn = intent.getStringExtra(STATUS_GOOGLE_SIGN_IN);

        Log.d("statusGoogleSignIn", "" + statusGoogleSignIn);

        listView = (ListView) findViewById(R.id.list_agent_type);

        adapter = new AgenTypeAdapter(this, listAgentType);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                agentType = listAgentType.get(position).getMemberType();
                agentTypeName = listAgentType.get(position).getMemberTypeName();
                switch (position) {
                    case 0:

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegisterAgentTypeActivity.this, R.style.AlertDialogTheme);
                        LayoutInflater inflater = RegisterAgentTypeActivity.this.getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.dialog_insert_company_code2, null);
                        dialogBuilder.setView(dialogView);

                        final EditText companyCode = (EditText) dialogView.findViewById(R.id.edt_company_code2);

                        dialogBuilder.setMessage("Masukan Company Name");
                        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        });
                        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                //pass
                            }
                        });

                        alertDialog = dialogBuilder.create();
                        alertDialog.show();
                        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                        positiveButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (!companyCode.getText().toString().isEmpty()) {

                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterAgentTypeActivity.this, R.style.AlertDialogTheme);
                                    alertDialogBuilder.setMessage("Apakah anda " + String.valueOf(agentTypeName) + " ? \n \n \n" +
                                            "Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
                                    alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            final String code = companyCode.getText().toString();
                                            registerFinishAgen(code);

                                        }
                                    });
                                    alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
//                                            dialog.dismiss();
                                        }
                                    });
                                    alertDialogBuilder.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(RegisterAgentTypeActivity.this, RegisterTermActivity.class));
                                        }
                                    });
                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                }else {
                                    companyCode.setError("Tidak Boleh Kosong");
                                }


                                }

                        });

//                        AlertDialog.Builder alert = new AlertDialog.Builder(RegisterAgentTypeActivity.this);
//                        alert.setTitle("Masukan Company Name");
//                        alert.setCancelable(false);
//
////                        LinearLayout layout = new LinearLayout(RegisterAgentTypeActivity.this);
////                        layout.setOrientation(LinearLayout.VERTICAL);
////                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
////                                LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
////                        params.setMargins(30, 0, 30, 0);
//
//                        final EditText input = new EditText(RegisterAgentTypeActivity.this);
//
//
//                        alert.setView(input);
//                        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//
//
//                                if (!input.getText().toString().isEmpty()) {
//
//                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterAgentTypeActivity.this);
//                                    alertDialogBuilder.setMessage("Apakah anda " + String.valueOf(agentTypeName) + " ? \n \n \n" +
//                                            "Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
//                                    alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            final String code = input.getText().toString();
//                                            registerFinishAgen(code);
//
//                                        }
//                                    });
//                                    alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
////                                            dialog.dismiss();
//                                        }
//                                    });
//                                    alertDialogBuilder.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            startActivity(new Intent(RegisterAgentTypeActivity.this, RegisterTermActivity.class));
//                                        }
//                                    });
//                                    AlertDialog alertDialog = alertDialogBuilder.create();
//                                    alertDialog.show();
//
//                                }else {
//                                    input.setError("Tidak Boleh Kosong");
//                                }
//
//
//                            }
//                        });
//                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                //Put actions for CANCEL button here, or leave in blank
//                            }
//                        });
//
//                        alert.show();
//                        Intent intentPropertyAgentcy = new Intent(RegisterAgentTypeActivity.this, RegisterPropertyAgencyActivity.class);
//                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
//                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                        editor.putString("isfullname", fullname);
//                        editor.putString("isbirthDate", birthDate);
//                        editor.putString("isphone", phone);
//                        editor.putString("isemail", email);
//                        editor.putString("ispassword", password);
//                        editor.putString("isagentType", agentType);
//                        editor.putString("isagentTypeName", agentTypeName);
//                        editor.putString("statusGoogleSignIn", statusGoogleSignIn);
//                        editor.commit();
//                        startActivity(intentPropertyAgentcy);
                        break;

                    case 1:
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterAgentTypeActivity.this, R.style.AlertDialogTheme);
                        alertDialogBuilder.setMessage("Apakah anda " + String.valueOf(agentTypeName) + " ? \n \n \n" +
                                "Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
                        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                registerFinish();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialogBuilder.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(RegisterAgentTypeActivity.this, RegisterTermActivity.class));
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                        break;

                    case 2:
                        Intent intentInhouse = new Intent(RegisterAgentTypeActivity.this, RegisterInhouseActivity.class);
                        SharedPreferences sharedPreferencesInhouse = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editorInhouse = sharedPreferencesInhouse.edit();
                        editorInhouse.putString("isfullname", fullname);
                        editorInhouse.putString("isbirthDate", birthDate);
                        editorInhouse.putString("isphone", phone);
                        editorInhouse.putString("isemail", email);
                        editorInhouse.putString("ispassword", password);
                        editorInhouse.putString("isagentType", agentType);
                        editorInhouse.putString("isagentTypeName", agentTypeName);
                        editorInhouse.putString("statusGoogleSignIn", statusGoogleSignIn);
                        editorInhouse.commit();
                        startActivity(intentInhouse);
                        break;

                    case 3:
//                        AlertDialog.Builder alertDialogBuilderGuest = new AlertDialog.Builder(RegisterAgentTypeActivity.this);
//                        alertDialogBuilderGuest.setMessage("Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
//                        alertDialogBuilderGuest.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                registerCustomerFinish();
//                            }
//                        });
//                        alertDialogBuilderGuest.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
//                            }
//                        });
//                        alertDialogBuilderGuest.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                startActivity(new Intent(RegisterAgentTypeActivity.this,RegisterTermActivity.class));
//                            }
//                        });
//                        AlertDialog alertDialogGuest = alertDialogBuilderGuest.create();
//                        alertDialogGuest.show();
                        break;

                }
            }
        });

        requestMemberType();
    }

    private void registerFinishAgen(final String code) {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getRegisterAgency(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result ", response);
                    int status = jo.getInt("status");

                    if (status == 200) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isfullname", "");
                        editor.putString("isbirthDate", "");
                        editor.putString("isphone", "");
                        editor.putString("isemail", "");
                        editor.putString("ispassword", "");
                        editor.putString("isagentType", "");
                        editor.putString("isagentTypeName", "");
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();

                        // Launch finish activity
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, RegisterFinishActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else if (status == 201) {
                        message = jo.getString("message");
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, RegisterStepOneActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.putExtra(MOBILE, phone);
                        intent.putExtra(MESSAGE, message);
                        startActivity(intent);
                        finish();

                    } else if (status == 500) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();

                        Intent intent = new Intent(RegisterAgentTypeActivity.this, LaunchActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fullname", fullname);
                params.put("username", email);
                params.put("mobile", phone);
                params.put("agentType", agentType);
                params.put("password", password);
                params.put("birthDate", birthDate);
                params.put("companyName", code);
                params.put("principleName", "");
                params.put("companyPhone", "");
                params.put("companyMobile", "");
                params.put("companyAddress", "");
                params.put("agencyCompanyRef", "");
                params.put("status", statusGoogleSignIn);
                params.put("projectCode", General.projectCode + "#" + General.projectCodeSingleProject);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "registerAgency");
    }

    public void requestMemberType() {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMemberType(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateListMemberType(jsonArray);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("projectCode", General.projectCode);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "getMemberType");

    }

    private void generateListMemberType(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                AgenTypeModel type = new AgenTypeModel();
                type.setMemberType(jo.getString("memberType"));
                type.setMemberTypeName(jo.getString("memberTypeName"));

                listAgentType.add(type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void registerFinish() {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getRegisterSalesAgent(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result register", response);
                    int status = jo.getInt("status");

                    if (status == 200) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, RegisterFinishActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else if (status == 500) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, LaunchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else if (status == 201) {
                        message = jo.getString("message");
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, RegisterStepOneActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.putExtra(MOBILE, phone);
                        intent.putExtra(MESSAGE, message);
                        startActivity(intent);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fullname", fullname);
                params.put("username", email);
                params.put("mobile", phone);
                params.put("agentType", agentType);
                params.put("password", password);
                params.put("birthDate", birthDate);
                params.put("status", statusGoogleSignIn);
                params.put("projectCode", General.projectCode + "#" + General.projectCodeSingleProject);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "registerSales");
    }

    private void registerCustomerFinish() {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getSaveRegisterCustomerSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();

                try {
                    JSONObject jo = new JSONObject(response);
                    String message = jo.getString("message");
                    Log.d("result register", response);
                    int status = jo.getInt("status");

                    if (status == 200) {
                        // generate json ke string
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        // simpan profile user ke dalam shared preferences
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(RegisterAgentTypeActivity.PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();
                        // tampilkan register finish
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, RegisterFinishActivity.class);
                        intent.putExtra(RegisterAgentTypeActivity.EMAIL, email);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), message,
                                Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fullname", fullname);
                params.put("username", email);
                params.put("mobile", phone);
                params.put("password", password);
                params.put("birthDate", birthDate);
                params.put("projectCode", General.projectCode);
                params.put("projectPsRef", "0");
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "saveRegisterCustomer");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
