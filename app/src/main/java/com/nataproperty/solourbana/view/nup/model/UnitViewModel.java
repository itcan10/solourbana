package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by UserModel on 11/3/2016.
 */
public class UnitViewModel {
    String surveyUnitViewRef,surveyUnitViewName;

    public String getSurveyUnitViewRef() {
        return surveyUnitViewRef;
    }

    public void setSurveyUnitViewRef(String surveyUnitViewRef) {
        this.surveyUnitViewRef = surveyUnitViewRef;
    }

    public String getSurveyUnitViewName() {
        return surveyUnitViewName;
    }

    public void setSurveyUnitViewName(String surveyUnitViewName) {
        this.surveyUnitViewName = surveyUnitViewName;
    }
}
