package com.nataproperty.solourbana.view.booking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.booking.model.SalesLocationModel;

import java.util.List;

/**
 * Created by User on 5/15/2016.
 */
public class SalesLocationAdapter extends BaseAdapter {
    private Context context;
    private List<SalesLocationModel> list;
    private ListFeatureHolder holder;

    public SalesLocationAdapter(Context context, List<SalesLocationModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.spinner_item,null);
            holder = new ListFeatureHolder();
            holder.locationName = (TextView) convertView.findViewById(R.id.txt_location_name);

            convertView.setTag(holder);
        }else{
            holder = (ListFeatureHolder) convertView.getTag();
        }

        SalesLocationModel item = list.get(position);
        holder.locationName.setText(item.getSalesLocationName());

        return convertView;

    }

    private class ListFeatureHolder {
        TextView locationName;
    }
}
