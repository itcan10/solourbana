
package com.nataproperty.solourbana.view.kpr.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.NumberTextWatcher;
import com.nataproperty.solourbana.view.kpr.adapter.DpAdapter;
import com.nataproperty.solourbana.view.kpr.adapter.KprAdapter;
import com.nataproperty.solourbana.view.kpr.model.DpModel;
import com.nataproperty.solourbana.view.kpr.model.KprModel;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculationCaraBayarActivity extends AppCompatActivity {
    public static final String TAG = "CalculationCaraBayar";
    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;
    String memberRef;
    public static EditText edtBookingDate, edtBookingFee, edtnumOfInst, edtdaysOfInst;
    TextView txtTotal;
    Spinner spnTypeKpr, spnSpDp;
    String typeRef, dpRef;
    Button btnSave, btnCreateIllustration, btnAddDp, btnAddTahunKpr;
    MyListView listViewDp;
    MyListView listViewKPR;
    private ArrayList<DpModel> listDpNew = new ArrayList<DpModel>();
    private ArrayList<KprModel> listKpr = new ArrayList<KprModel>();
    public ArrayList<String> dpList = new ArrayList<String>();
    public ArrayList<String> cicilList = new ArrayList<String>();
    public ArrayList<String> hariList = new ArrayList<String>();
    public ArrayList<String> tahunKprList = new ArrayList<String>();
    public ArrayList<String> bungaList = new ArrayList<String>();
    private DpAdapter dpAdapter;
    private KprAdapter kprAdapter;
    DpModel dpModel;
    KprModel kprModel;
    LinearLayout linearLayoutJumlah, linearLayoutMulaiHari, linearLayoutTahunKpr;
    String stringListDp;
    String stringListKpr;
    EditText edtPayTermName;
    String calcPayTermRef, calcUserProjectRef, payTermName, installmentType, dpType, bookingFeeDate, bookingFee, numOfInst, daysOfInst;
    String project, cluster, product, block, unit, price, total;
    TextView txtProject, txtCluster, txtProduct, txtBlock, txtUnit, txtPrice;
    AutoCompleteTextView autoProject, autoCluster, autoProduct, autoBlock, autoUnit, autoPrice;
    private ArrayList<String> listCluster = new ArrayList<String>();
    private ArrayList<String> listProduct = new ArrayList<String>();
    private ArrayList<String> listBlock = new ArrayList<String>();
    Typeface font;
    LinearLayout linearNoDataDP, linearNoDataKPR;
    AlertDialog alertDialogDp, alertDialogKPR, alertDialogSave;
    Button btnDeleteDp, btnDeleteKPR;
    int type, spDp, noteSave, hari, hariAkhir;
    CardView cardViewDP, cardViewKPR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_cara_bayar_kpr);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_calculation));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        autoCluster = (AutoCompleteTextView) findViewById(R.id.auto_cluster);
        autoProduct = (AutoCompleteTextView) findViewById(R.id.auto_product);
        autoBlock = (AutoCompleteTextView) findViewById(R.id.auto_block);
        autoUnit = (AutoCompleteTextView) findViewById(R.id.auto_unit);
        autoPrice = (AutoCompleteTextView) findViewById(R.id.auto_price);
        //autoPrice.addTextChangedListener(new NumberTextWatcher(autoPrice));
        txtProject = (TextView) findViewById(R.id.txt_project);
        txtCluster = (TextView) findViewById(R.id.txt_cluster);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtBlock = (TextView) findViewById(R.id.txt_block);
        txtUnit = (TextView) findViewById(R.id.txt_unit);
        txtPrice = (TextView) findViewById(R.id.txt_price);
        spnTypeKpr = (Spinner) findViewById(R.id.spn_type_kpr);
        spnSpDp = (Spinner) findViewById(R.id.spn_dp);
        edtBookingDate = (EditText) findViewById(R.id.edt_booking_date);
        edtBookingFee = (EditText) findViewById(R.id.edt_booking_fee);
        edtBookingFee.addTextChangedListener(new NumberTextWatcher(edtBookingFee));
        edtBookingFee.setText("0");
        edtnumOfInst = (EditText) findViewById(R.id.edt_numOfInst);
        edtdaysOfInst = (EditText) findViewById(R.id.edt_daysOfInst);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnCreateIllustration = (Button) findViewById(R.id.btn_create_illustration);
        btnAddTahunKpr = (Button) findViewById(R.id.btn_add_tahun_kpr);
        btnAddDp = (Button) findViewById(R.id.btn_add_dp);
        listViewDp = (MyListView) findViewById(R.id.list_dp);
        listViewKPR = (MyListView) findViewById(R.id.list_tahun_kpr);
        btnDeleteDp = (Button) findViewById(R.id.btn_delete_dp);
        btnDeleteKPR = (Button) findViewById(R.id.btn_delete_kpr);
        btnDeleteDp.setTypeface(font);
        btnDeleteKPR.setTypeface(font);

        Intent intent = getIntent();
        type = intent.getIntExtra("type", 0);
        calcUserProjectRef = intent.getStringExtra("calcUserProjectRef");
        calcPayTermRef = intent.getStringExtra("calcPayTermRef");
        project = intent.getStringExtra("project");
        numOfInst = intent.getStringExtra("numOfInst");
        noteSave = intent.getIntExtra("noteSave", 0);

        Log.d(TAG, "type " + type);

        /*Kpr / Cicilan Layout*/
        linearLayoutJumlah = (LinearLayout) findViewById(R.id.linear_jumlah);
        linearLayoutMulaiHari = (LinearLayout) findViewById(R.id.linear_mulai_hari);
        linearLayoutTahunKpr = (LinearLayout) findViewById(R.id.linear_tahun_kpr);

        linearNoDataDP = (LinearLayout) findViewById(R.id.linear_no_list_dp);
        linearNoDataKPR = (LinearLayout) findViewById(R.id.linear_no_list_kpr);

        cardViewDP = (CardView) findViewById(R.id.card_view_list_dp);
        cardViewKPR = (CardView) findViewById(R.id.card_view_list_kpr);

        /*TypeCaraBayar
        0 --> Cicilan
        1 --> KPR */
        if (type == 0) {
            linearLayoutJumlah.setVisibility(View.VISIBLE);
            linearLayoutMulaiHari.setVisibility(View.VISIBLE);
            linearLayoutTahunKpr.setVisibility(View.GONE);
            autoPrice.addTextChangedListener(new NumberTextWatcher(autoPrice));
        } else {
            linearLayoutJumlah.setVisibility(View.GONE);
            linearLayoutMulaiHari.setVisibility(View.GONE);
            linearLayoutTahunKpr.setVisibility(View.VISIBLE);
            autoPrice.addTextChangedListener(new NumberTextWatcher(autoPrice));
        }

        Log.d(TAG, "getIntent:" + calcPayTermRef);

        /*Dari Listview/ bikin baru*/
        if (calcPayTermRef != null) {
            /*listview*/
            requestTemplateInfo();
            requestListCluster();

             /*AutoComplate*/
            ArrayAdapter<String> adapterCluster = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listCluster);
            autoCluster.setAdapter(adapterCluster);
            autoCluster.setTypeface(font);
            autoCluster.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    requestListProduct();
                }
            });
            ArrayAdapter<String> adapterProduct = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listProduct);
            autoProduct.setAdapter(adapterProduct);
            autoProduct.setTypeface(font);
            autoProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    requestListBlock();
                }
            });
            ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listBlock);
            autoBlock.setAdapter(adapterBlock);
            autoBlock.setTypeface(font);
            autoUnit.setTypeface(font);
            autoPrice.setTypeface(font);
        } else {
            /*new*/
            requestTemplateInfoWithoutList();

            requestListCluster();

             /*AutoComplate*/
            ArrayAdapter<String> adapterCluster = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listCluster);
            autoCluster.setAdapter(adapterCluster);
            autoCluster.setTypeface(font);
            autoCluster.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    requestListProduct();
                }
            });
            ArrayAdapter<String> adapterProduct = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listProduct);
            autoProduct.setAdapter(adapterProduct);
            autoProduct.setTypeface(font);
            autoProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    requestListBlock();
                }
            });
            ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listBlock);
            autoBlock.setAdapter(adapterBlock);
            autoBlock.setTypeface(font);
            autoUnit.setTypeface(font);
            autoPrice.setTypeface(font);

            Calendar c = Calendar.getInstance();
            int mDate = c.get(Calendar.DATE);
            int mMonth = c.get(Calendar.MONTH);
            int mYear = c.get(Calendar.YEAR);

            edtBookingDate.setText(String.valueOf(mDate) + "/" + String.valueOf(mMonth+1) + "/" + String.valueOf(mYear));
        }

        List<String> listType = new ArrayList<String>();
        listType.add("Cicilan");
        listType.add("KPR/KPA");
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listType);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnTypeKpr.setAdapter(dataAdapter1);
        spnTypeKpr.setSelection(type);
        spnTypeKpr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeRef = String.valueOf(spnTypeKpr.getSelectedItemPosition() + 1);

                if (position == 0) {
                    linearLayoutJumlah.setVisibility(View.VISIBLE);
                    linearLayoutMulaiHari.setVisibility(View.VISIBLE);
                    linearLayoutTahunKpr.setVisibility(View.GONE);
                } else {
                    linearLayoutJumlah.setVisibility(View.GONE);
                    linearLayoutMulaiHari.setVisibility(View.GONE);
                    linearLayoutTahunKpr.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnTypeKpr.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(CalculationCaraBayarActivity.this);
                return false;
            }
        });

        final List<String> list = new ArrayList<String>();
        list.add("DP x Harga");
        list.add("DP x (Harga - Booking Fee)");
        list.add("(DP 1 x Harga) - Booking Fee");
        list.add("(DP x Harga) - (Booking Fee / Jml DP)");
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSpDp.setAdapter(dataAdapter);
        spnSpDp.setSelection(spDp);
        spnSpDp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dpRef = String.valueOf(spnSpDp.getSelectedItemPosition() + 1);
                Log.d(TAG, "dpRef" + dpRef);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnSpDp.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(CalculationCaraBayarActivity.this);
                return false;
            }
        });

        /*default tambah dp*/
        edtBookingFee.setText("0");
        edtBookingDate.setInputType(InputType.TYPE_NULL);
        edtBookingDate.setTextIsSelectable(true);
        edtBookingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "Date Picker");
            }
        });
        edtBookingDate.setFocusable(false);

        /*listDp*/
        dpAdapter = new DpAdapter(this, listDpNew);
        listViewDp.setAdapter(dpAdapter);
        listViewDp.setExpanded(true);

        /*listKPR*/
        kprAdapter = new KprAdapter(this, listKpr);
        listViewKPR.setAdapter(kprAdapter);
        listViewKPR.setExpanded(true);

        btnAddDp.setTypeface(font);
        btnAddDp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CalculationCaraBayarActivity.this);
                LayoutInflater inflater = CalculationCaraBayarActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_add_dp, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);
                final EditText edtDp = (EditText) dialogView.findViewById(R.id.edt_dp);
                final EditText edtCicil = (EditText) dialogView.findViewById(R.id.edt_cicil);
                final EditText edtHari = (EditText) dialogView.findViewById(R.id.edt_hari);

                hari = hariAkhir + 14;
                edtHari.setText(String.valueOf(hari));

                dialogBuilder.setMessage("Tambah Skema DP");
                dialogBuilder.setPositiveButton(getResources().getString(R.string.btn_add), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });

                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                alertDialogDp = dialogBuilder.create();
                alertDialogDp.show();
                alertDialogDp.setCancelable(false);
                Button positiveButton = alertDialogDp.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideSoftKeyboard(CalculationCaraBayarActivity.this);
                        cardViewDP.setFocusableInTouchMode(true);
                        cardViewDP.setFocusable(true);

                        String dp = edtDp.getText().toString();
                        String cicil = edtCicil.getText().toString();
                        String hari = edtHari.getText().toString();

                        if (dp.isEmpty() || cicil.isEmpty() || hari.isEmpty() || dp.equals(".")) {
                            if (dp.isEmpty()) {
                                edtDp.setError("masukkan dp");
                            } else if (dp.equals(".")) {
                                edtDp.setError("dp tidak sesuai");
                            } else {
                                edtDp.setError(null);
                            }
                            if (cicil.isEmpty()) {
                                edtCicil.setError("masukkan cicilan");
                            } else {
                                edtCicil.setError(null);
                            }
                            if (hari.isEmpty()) {
                                edtHari.setError("masukkan mulai hari ke");
                            } else {
                                edtHari.setError(null);
                            }

                        } else {
                            hariAkhir = Integer.parseInt(edtHari.getText().toString());

                            int i;
                            //int day = Integer.parseInt(hari);
                            double dpAkhir = Double.parseDouble(dp) / Double.parseDouble(cicil);
                            DecimalFormat decimalFormatDp = new DecimalFormat("#.##");

                            Log.d(TAG, "dpAkhir " + dpAkhir + " " + decimalFormatDp.format(dpAkhir));
                            for (i = 0; i < Integer.parseInt(cicil); i++) {
                                dpModel = new DpModel();
                                dpModel.setDp(decimalFormatDp.format(dpAkhir));
                                dpModel.setCicil(String.valueOf(i + 1));
                                if (i > 0) {
                                    dpModel.setHari(String.valueOf(Integer.parseInt(hari) + (30 * i)));

                                } else {
                                    dpModel.setHari(hari);
                                }
                                listDpNew.add(dpModel);
                                Log.d("trying", "" + dpModel.getHari());

                                dpList.add(decimalFormatDp.format(dpAkhir));
                                cicilList.add(cicil);
                                if (i > 0) {
                                    hariList.add(String.valueOf(Integer.parseInt(hari) + (30 * i)));

                                    edtdaysOfInst.setText(String.valueOf(Integer.parseInt(hari) + (30 * i) + 30));
                                } else {
                                    hariList.add(hari);

                                    edtdaysOfInst.setText(String.valueOf(Integer.parseInt(hari) + 30));
                                }
                            }

                            Log.d(TAG, dpList.toString());
                            Log.d(TAG, cicilList.toString());
                            Log.d(TAG, hariList.toString());

                            stringListDp = dpList + " # " + cicilList + " # " + hariList;

                            Log.d(TAG, dpList + " # " + cicilList + " # " + hariList + " " + dpList.size());

                            CalculationCaraBayarActivity.this.dpAdapter.notifyDataSetChanged();

                            if (dpList.size() == 0) {
                                linearNoDataDP.setVisibility(View.VISIBLE);
                                listViewDp.setVisibility(View.GONE);
                            } else {
                                linearNoDataDP.setVisibility(View.GONE);
                                listViewDp.setVisibility(View.VISIBLE);
                            }

                            alertDialogDp.dismiss();

                        }

                    }

                });
                edtnumOfInst.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edtnumOfInst, InputMethodManager.SHOW_IMPLICIT);

            }
        });

        btnDeleteDp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder =
                        new AlertDialog.Builder(CalculationCaraBayarActivity.this);
                alertDialogBuilder.setMessage("Apakah anda yakin menghapus semua DP?");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        stringListDp = "";
                        listDpNew.clear();
                        dpList.clear();
                        cicilList.clear();
                        hariList.clear();
                        CalculationCaraBayarActivity.this.dpAdapter.notifyDataSetChanged();
                        linearNoDataDP.setVisibility(View.VISIBLE);

                          /*delete hariakhirdp n mulai hari*/
                        hariAkhir = 0;
                        edtdaysOfInst.setText("");

                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Log.d(TAG, "dpList: " + dpList.size());

        btnAddTahunKpr.setTypeface(font);
        btnAddTahunKpr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CalculationCaraBayarActivity.this);
                LayoutInflater inflater = CalculationCaraBayarActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_add_tahun_kpr, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);
                final EditText edtTahunKpr = (EditText) dialogView.findViewById(R.id.edt_tahun_kpr);
                final EditText edtBunga = (EditText) dialogView.findViewById(R.id.edt_bunga);

                dialogBuilder.setMessage("Tambah Skema KPR");
                dialogBuilder.setPositiveButton(getResources().getString(R.string.btn_add), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                alertDialogKPR = dialogBuilder.create();
                alertDialogKPR.setCancelable(false);
                alertDialogKPR.show();
                Button positiveButton = alertDialogKPR.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hideSoftKeyboard(CalculationCaraBayarActivity.this);
                        cardViewKPR.setFocusableInTouchMode(true);
                        cardViewKPR.setFocusable(true);

                        String tahunKpr = edtTahunKpr.getText().toString();
                        String bunga = edtBunga.getText().toString();

                        if (tahunKpr.isEmpty() || bunga.isEmpty() || bunga.equals(".")) {
                            if (tahunKpr.isEmpty()) {
                                edtTahunKpr.setError("masukkan tahun KPR");
                            } else {
                                edtTahunKpr.setError(null);
                            }
                            if (bunga.isEmpty()) {
                                edtBunga.setError("masukkan bunga");
                            } else if (bunga.equals(".")) {
                                edtBunga.setError("bunga tidak sesuai");
                            } else {
                                edtBunga.setError(null);
                            }

                        } else {
                            int kprAkhir = Integer.parseInt(tahunKpr) * 12;
                            Log.d(TAG, "kprAkhir " + kprAkhir);

                            kprModel = new KprModel();
                            kprModel.setTahunKpr(tahunKpr);
                            kprModel.setBunga(bunga);
                            listKpr.add(kprModel);

                            tahunKprList.add(String.valueOf(kprAkhir));
                            bungaList.add(bunga);

                            Log.d(TAG, "tahunKpr:" + tahunKprList.toString());
                            Log.d(TAG, "bunga:" + bungaList.toString());

                            stringListKpr = tahunKprList + " # " + bungaList;

                            Log.d(TAG, "stringListKpr:" + stringListKpr);

                            CalculationCaraBayarActivity.this.kprAdapter.notifyDataSetChanged();

                            if (tahunKprList.size() == 0) {
                                linearNoDataKPR.setVisibility(View.VISIBLE);
                                listViewKPR.setVisibility(View.GONE);
                            } else {
                                linearNoDataKPR.setVisibility(View.GONE);
                                listViewKPR.setVisibility(View.VISIBLE);
                            }

                            alertDialogKPR.dismiss();
                        }
                    }
                });
            }
        });

        btnDeleteKPR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder =
                        new AlertDialog.Builder(CalculationCaraBayarActivity.this);
                alertDialogBuilder.setMessage("Apakah anda yakin menghapus semua KPR?");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listKpr.clear();
                        tahunKprList.clear();
                        bungaList.clear();
                        CalculationCaraBayarActivity.this.kprAdapter.notifyDataSetChanged();
                        linearNoDataKPR.setVisibility(View.VISIBLE);

                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        btnSave.setTypeface(font);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekPrice = autoPrice.getText().toString();
                String cekBookingFee = edtBookingFee.getText().toString();
                String cekNumOfInst = edtnumOfInst.getText().toString();
                String cekDayOfInst = edtdaysOfInst.getText().toString();

                if (typeRef.equals("1")) {
                    if (cekPrice.isEmpty() || cekBookingFee.isEmpty() || cekNumOfInst.isEmpty() || cekDayOfInst.isEmpty()) {
                        if (cekPrice.isEmpty()) {
                            autoPrice.setError("masukkan harga");
                            autoPrice.setFocusableInTouchMode(true);
                            autoPrice.requestFocus();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(autoPrice, InputMethodManager.SHOW_IMPLICIT);
                        } else {
                            autoPrice.setError(null);
                        }

                        if (cekBookingFee.isEmpty()) {
                            edtBookingFee.setError("masukkan booking fee");
                        } else {
                            edtBookingFee.setError(null);
                        }

                        if (cekNumOfInst.isEmpty()) {
                            edtnumOfInst.setError("masukkan jumlah cicilan");
                        } else {
                            edtnumOfInst.setError(null);
                        }

                        if (cekDayOfInst.isEmpty()) {
                            edtdaysOfInst.setError("masukkan mulai hari ke");
                        } else {
                            edtdaysOfInst.setError(null);
                        }

                       /* if (dpList.size() == 0) {
                            Toast.makeText(CalculationCaraBayarActivity.this, "Tambah Skema Dp", Toast.LENGTH_SHORT).show();
                        }*/

                    } else {

                        popUpSavetemplate();

                    }

                } else {
                    if (cekPrice.isEmpty() || cekBookingFee.isEmpty() || tahunKprList.size() == 0) {
                        if (cekPrice.isEmpty()) {
                            autoPrice.setError("masukkan price");
                        } else {
                            autoPrice.setError(null);
                        }

                        if (cekBookingFee.isEmpty()) {
                            edtBookingFee.setError("masukkan booking fee");
                        } else {
                            edtBookingFee.setError(null);
                        }

                       /* if (dpList.size() == 0) {
                            Toast.makeText(CalculationCaraBayarActivity.this, "Tambah Skema Dp", Toast.LENGTH_SHORT).show();
                        } else */
                        if (tahunKprList.size() == 0) {
                            Toast.makeText(CalculationCaraBayarActivity.this, "Tambah Skema Kpr", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        popUpSavetemplate();

                    }
                }
            }
        });

        btnCreateIllustration.setTypeface(font);
        btnCreateIllustration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekPrice = autoPrice.getText().toString();
                String cekBookingFee = edtBookingFee.getText().toString();
                String cekNumOfInst = edtnumOfInst.getText().toString();
                String cekDayOfInst = edtdaysOfInst.getText().toString();

                if (typeRef.equals("1")) {
                    if (cekPrice.isEmpty() || cekBookingFee.isEmpty() || cekNumOfInst.isEmpty() || cekDayOfInst.isEmpty()) {
                        if (cekPrice.isEmpty()) {
                            autoPrice.setError("masukkan harga");
                        } else {
                            autoPrice.setError(null);
                        }

                        if (cekBookingFee.isEmpty()) {
                            edtBookingFee.setError("masukkan booking fee");
                        } else {
                            edtBookingFee.setError(null);
                        }

                        if (cekNumOfInst.isEmpty()) {
                            edtnumOfInst.setError("masukkan jumlah cicilan");
                        } else {
                            edtnumOfInst.setError(null);
                        }

                        if (cekDayOfInst.isEmpty()) {
                            edtdaysOfInst.setError("masukkan mulai hari ke");
                        } else {
                            edtdaysOfInst.setError(null);
                        }

                        /*if (dpList.size() == 0) {
                            Toast.makeText(CalculationCaraBayarActivity.this, "Tambah Skema Dp", Toast.LENGTH_SHORT).show();
                        }*/

                    } else {
                        craeateIlustrasi();
                    }

                } else {

                    if (cekPrice.isEmpty() || cekBookingFee.isEmpty() || tahunKprList.size() == 0) {
                        if (cekPrice.isEmpty()) {
                            autoPrice.setError("masukkan a harga");
                        } else {
                            autoPrice.setError(null);
                        }

                        if (cekBookingFee.isEmpty()) {
                            edtBookingFee.setError("masukkan a booking fee");
                        } else {
                            edtBookingFee.setError(null);
                        }

                        /*if (dpList.size() == 0) {
                            Toast.makeText(CalculationCaraBayarActivity.this, "Tambah Skema Dp", Toast.LENGTH_SHORT).show();
                        } else */
                        if (tahunKprList.size() == 0) {
                            Toast.makeText(CalculationCaraBayarActivity.this, "Tambah Skema Kpr", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        craeateIlustrasi();
                    }
                }

            }
        });
    }

    public void popUpSavetemplate() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CalculationCaraBayarActivity.this);
        LayoutInflater inflater = CalculationCaraBayarActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_template, null);
        dialogBuilder.setView(dialogView);

        edtPayTermName = (EditText) dialogView.findViewById(R.id.edt_template_name);
        dialogBuilder.setMessage("Masukkan nama template");
        dialogBuilder.setPositiveButton(getResources().getString(R.string.btn_save), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        alertDialogSave = dialogBuilder.create();
        alertDialogSave.show();
        Button positiveButton = alertDialogSave.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekPayTermName = edtPayTermName.getText().toString();

                if (cekPayTermName.isEmpty()) {
                    edtPayTermName.setError("masukkan nama template");
                } else {
                    edtPayTermName.setError(null);
                    if (typeRef.equals("1")) {
                        stringListKpr = "";
                        if (dpList.size() == 0) {
                            stringListDp = "";
                        }
                        saveCaraBayar();
                        alertDialogSave.dismiss();
                    } else {
                        if (dpList.size() == 0) {
                            stringListDp = "";
                        }
                        saveCaraBayar();
                        alertDialogSave.dismiss();
                    }

                }
            }
        });
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), R.style.DatePickerDialogTheme, this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            TextView tv = (TextView) getActivity().findViewById(R.id.txtBirthDate);
            edtBookingDate.setText(day + "/" + (month + 1) + "/" + year);
        }
    }

    public void craeateIlustrasi() {
        if (typeRef.equals("1")) {
            stringListKpr = "";

            if (dpList.size() == 0) {
                stringListDp = "";
            }

            Intent intent = new Intent(CalculationCaraBayarActivity.this, CalculationIlustasiActivity.class);
            intent.putExtra("calcUserProjectRef", calcUserProjectRef.toString());
            intent.putExtra("payTermName", "");
            intent.putExtra("installmentType", typeRef.toString());
            intent.putExtra("DPType", dpRef.toString());
            intent.putExtra("bookingFeeDate", edtBookingDate.getText().toString());
            intent.putExtra("bookingFee", edtBookingFee.getText().toString());
            intent.putExtra("numOfInst", edtnumOfInst.getText().toString());
            intent.putExtra("daysOfInst", edtdaysOfInst.getText().toString());
            intent.putExtra("listDP", stringListDp.toString());
            intent.putExtra("KPRYear", stringListKpr.toString());

            intent.putExtra("project", project.toString());
            intent.putExtra("cluster", autoCluster.getText().toString());
            intent.putExtra("product", autoProduct.getText().toString());
            intent.putExtra("block", autoBlock.getText().toString());
            intent.putExtra("unit", autoUnit.getText().toString());
            intent.putExtra("price", autoPrice.getText().toString());
            intent.putExtra("noteSave", noteSave);
            startActivity(intent);
        } else {
            if (dpList.size() == 0) {
                stringListDp = "";
            }

            Intent intent = new Intent(CalculationCaraBayarActivity.this, CalculationIlustasiActivity.class);
            intent.putExtra("calcUserProjectRef", calcUserProjectRef.toString());
            intent.putExtra("payTermName", "");
            intent.putExtra("installmentType", typeRef.toString());
            intent.putExtra("DPType", dpRef.toString());
            intent.putExtra("bookingFeeDate", edtBookingDate.getText().toString());
            intent.putExtra("bookingFee", edtBookingFee.getText().toString());
            intent.putExtra("numOfInst", edtnumOfInst.getText().toString());
            intent.putExtra("daysOfInst", edtdaysOfInst.getText().toString());
            intent.putExtra("listDP", stringListDp.toString());
            intent.putExtra("KPRYear", stringListKpr.toString());
            Log.d(TAG, "KPRYear " + stringListKpr);

            intent.putExtra("project", project.toString());
            intent.putExtra("cluster", autoCluster.getText().toString());
            intent.putExtra("product", autoProduct.getText().toString());
            intent.putExtra("block", autoBlock.getText().toString());
            intent.putExtra("unit", autoUnit.getText().toString());
            intent.putExtra("price", autoPrice.getText().toString());
            intent.putExtra("noteSave", noteSave);
            startActivity(intent);
        }

    }

    public void saveCaraBayar() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.savePaymentTermCalcUserProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, response);
                    if (jsonObject.getInt("status") == 200) {
                        Intent intent = new Intent(CalculationCaraBayarActivity.this, CalculationDetailActivity.class);
                        intent.putExtra("calcUserProjectRef", calcUserProjectRef);
                        intent.putExtra("project", project);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        Toast.makeText(CalculationCaraBayarActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CalculationCaraBayarActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(CalculationCaraBayarActivity.this,getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef.toString());
                params.put("calcUserProjectRef", calcUserProjectRef.toString());
                params.put("payTermName", edtPayTermName.getText().toString());
                params.put("installmentType", typeRef.toString());
                params.put("DPType", dpRef.toString());
                params.put("bookingFeeDate", edtBookingDate.getText().toString());
                params.put("bookingFee", edtBookingFee.getText().toString().replace(".","").replace(",",""));
                params.put("numOfInst",edtnumOfInst.getText().toString());
                params.put("daysOfInst", edtdaysOfInst.getText().toString());
                params.put("listDP", stringListDp.toString());
                params.put("KPRYear", stringListKpr.toString());
                params.put("project", project.toString());
                params.put("cluster", autoCluster.getText().toString());
                params.put("product", autoProduct.getText().toString());
                params.put("block", autoBlock.getText().toString());
                params.put("unit", autoUnit.getText().toString());
                params.put("price", autoPrice.getText().toString().replace(".","").replace(",",""));
                Log.d("paramSave ",params.toString());
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calcSend");

    }

    public void requestTemplateInfo() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getInfoPayment(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, "user: " + response);

                    project = jsonObject.getString("project");
                    cluster = jsonObject.getString("cluster");
                    product = jsonObject.getString("product");
                    block = jsonObject.getString("block");
                    unit = jsonObject.getString("unit");
                    price = jsonObject.getString("price");

                    Log.d(TAG, "user: " + project + " " + cluster + " " + product + " " + block + " " + unit + " " + price);

                    txtProject.setText(project);
                    txtCluster.setText(cluster);
                    txtProduct.setText(product);
                    txtBlock.setText(block);
                    txtUnit.setText(unit);
                    txtPrice.setText(price);

                    autoCluster.setText(cluster);
                    autoProduct.setText(product);
                    autoBlock.setText(block);
                    autoUnit.setText(unit);

                    if (price.equals("0")) {
                        autoPrice.setText("");
                    } else {
                        autoPrice.setText(price);
                    }

                    calcPayTermRef = jsonObject.getString("calcPayTermRef");
                    calcUserProjectRef = jsonObject.getString("calcUserProjectRef");
                    payTermName = jsonObject.getString("payTermName");
                    installmentType = jsonObject.getString("installmentType");
                    dpType = jsonObject.getString("dpType");
                    bookingFeeDate = jsonObject.getString("bookingFeeDate");
                    bookingFee = jsonObject.getString("bookingFee");

                    numOfInst = jsonObject.getString("numOfInst");
                    daysOfInst = jsonObject.getString("daysOfInst");

                    Log.d(TAG, "cek" + calcPayTermRef + " " + calcUserProjectRef + " " + payTermName + " " + installmentType
                            + " " + dpType + " " + bookingFeeDate + " " + bookingFee + " " + numOfInst + " " + daysOfInst);

                    Log.d("Tanggal", " " + bookingFeeDate);

                    spnTypeKpr.setSelection(Integer.parseInt(installmentType) - 1);
                    spnSpDp.setSelection(Integer.parseInt(dpType) - 1);

                    type = Integer.parseInt(installmentType) - 1;
                    spDp = Integer.parseInt(dpType) - 1;

                    edtBookingFee.setText(bookingFee);
                    edtBookingDate.setText(bookingFeeDate);

                    edtnumOfInst.setText(numOfInst);
                    edtdaysOfInst.setText(daysOfInst);

                    JSONArray jsonArrayDp = new JSONArray(jsonObject.getJSONArray("dataDP").toString());
                    Log.d(TAG, "jsonArrayDp:" + jsonArrayDp.toString());
                    generateDp(jsonArrayDp);

                    JSONArray jsonArrayKpr = new JSONArray(jsonObject.getJSONArray("dataKPR").toString());
                    Log.d(TAG, "jsonArrayKpr:" + jsonArrayKpr.toString());
                    generateKpr(jsonArrayKpr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(CalculationCaraBayarActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("calcPayTermRef", calcPayTermRef.toString());
                params.put("calcUserProjectRef", calcUserProjectRef.toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calCaraBayarInfo");

    }

    private void generateDp(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                DpModel dpModel = new DpModel();
                dpModel.setDpNo(joArray.getString("dpNo"));
                dpModel.setDp(joArray.getString("dpPct"));
                dpModel.setCicil(joArray.getString("numOfInstDP"));
                dpModel.setHari(joArray.getString("daysOfInstDP"));

                listDpNew.add(dpModel);

                dpList.add(joArray.getString("dpPct"));
                cicilList.add(joArray.getString("numOfInstDP"));
                hariList.add(joArray.getString("daysOfInstDP"));

                Log.d(TAG, dpList.toString());
                Log.d(TAG, cicilList.toString());
                Log.d(TAG, hariList.toString());

                stringListDp = dpList + " # " + cicilList + " # " + hariList;

                Log.d(TAG, dpList + " # " + cicilList + " # " + hariList);

                if (dpList.size() == 0) {
                    linearNoDataDP.setVisibility(View.VISIBLE);
                    listViewDp.setVisibility(View.GONE);
                } else {
                    linearNoDataDP.setVisibility(View.GONE);
                    listViewDp.setVisibility(View.VISIBLE);
                }

                hariAkhir = Integer.parseInt(joArray.getString("daysOfInstDP"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        dpAdapter.notifyDataSetChanged();

    }

    private void generateKpr(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject joArray = jsonArray.getJSONObject(i);
                KprModel kprModel = new KprModel();
                int kprAkhir = Integer.parseInt(joArray.getString("kprYear")) / 12;
                kprModel.setTahunKpr(String.valueOf(kprAkhir));
                kprModel.setBunga(joArray.getString("kprPct"));
                listKpr.add(kprModel);

                tahunKprList.add(joArray.getString("kprYear"));
                bungaList.add(joArray.getString("kprPct"));

                Log.d(TAG, "tahunKpr:" + tahunKprList.toString());
                Log.d(TAG, "bunga:" + bungaList.toString());

                stringListKpr = tahunKprList + " # " + bungaList;

                Log.d(TAG, "stringListKpr:" + stringListKpr);

                if (tahunKprList.size() == 0) {
                    linearNoDataKPR.setVisibility(View.VISIBLE);
                    listViewKPR.setVisibility(View.GONE);
                } else {
                    linearNoDataKPR.setVisibility(View.GONE);
                    listViewKPR.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        kprAdapter.notifyDataSetChanged();

    }

    public void requestTemplateInfoWithoutList() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getInfoCalcUserProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, "WithoutList: " + response);
                    int status = jsonObject.getInt("status");
                    if (status == 200) {
                        project = jsonObject.getString("project");
                        cluster = jsonObject.getString("cluster");
                        product = jsonObject.getString("product");
                        block = jsonObject.getString("block");
                        unit = jsonObject.getString("unit");
                        price = (jsonObject.getString("price")).replace(",",".");

                        txtProject.setText(project);
                        txtCluster.setText(cluster);
                        txtProduct.setText(product);
                        txtBlock.setText(block);
                        txtUnit.setText(unit);
                        txtPrice.setText(price);

                        spnSpDp.setSelection(2);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(CalculationCaraBayarActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("calcUserProjectRef", calcUserProjectRef.toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calCaraBayar");

    }

    public void requestListCluster() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListCalcCluster(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            listCluster.add(jo.getString("cluster"));
                            Log.d(TAG, "cluster" + response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();
                        Toast.makeText(CalculationCaraBayarActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("project", project.toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calCuster");

    }

    public void requestListProduct() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListCalcProduct(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            listCluster.add(jo.getString("cluster"));
                            Log.d(TAG, "cluster" + response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();
                        Toast.makeText(CalculationCaraBayarActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("project", project.toString());
                params.put("cluster", autoCluster.getText().toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calProduct");

    }

    public void requestListBlock() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListCalcBlock(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jo = jsonArray.getJSONObject(i);
                            listBlock.add(jo.getString("block"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();
                        Toast.makeText(CalculationCaraBayarActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("project", project.toString());
                params.put("cluster", autoCluster.getText().toString());
                params.put("product", autoProduct.getText().toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calProduct");

    }

    public void hideSoftKeyboard(Activity activity) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(
                    Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CalculationCaraBayarActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
