package com.nataproperty.solourbana.view.nup.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.booking.model.MemberInfoModel;
import com.nataproperty.solourbana.view.nup.intractor.NupInformasiInteractor;
import com.nataproperty.solourbana.view.nup.model.QtyNupModel;
import com.nataproperty.solourbana.view.nup.ui.NupInformasiCustomerActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NupInformasiPresenter implements NupInformasiInteractor {
    private NupInformasiCustomerActivity view;
    private ServiceRetrofit service;
    private ServiceRetrofitProject serviceProject;

    public NupInformasiPresenter(NupInformasiCustomerActivity view, ServiceRetrofit service, ServiceRetrofitProject serviceProject) {
        this.view = view;
        this.service = service;
        this.serviceProject = serviceProject;
    }


    @Override
    public void getMemberInfoSvc(String memberRef) {
        Call<MemberInfoModel> call = service.getAPI().getMemberInfoSvc(memberRef);
        call.enqueue(new Callback<MemberInfoModel>() {
            @Override
            public void onResponse(Call<MemberInfoModel> call, Response<MemberInfoModel> response) {
                view.showMemberInfoResults(response);
            }

            @Override
            public void onFailure(Call<MemberInfoModel> call, Throwable t) {
                view.showMemberInfoFailure(t);

            }


        });
    }

    @Override
    public void GetQtyNUPList(String dbMasterRef, String projectRef, String memberRef) {
        Call<ArrayList<QtyNupModel>> call = serviceProject.getAPI().GetQtyNUPList(dbMasterRef, projectRef, memberRef);
        call.enqueue(new Callback<ArrayList<QtyNupModel>>() {
            @Override
            public void onResponse(Call<ArrayList<QtyNupModel>> call, Response<ArrayList<QtyNupModel>> response) {
                view.showListQtyResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<QtyNupModel>> call, Throwable t) {
                view.showListQtyFailure(t);

            }


        });
    }

}
