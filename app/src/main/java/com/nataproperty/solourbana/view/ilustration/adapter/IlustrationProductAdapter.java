package com.nataproperty.solourbana.view.ilustration.adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.ilustration.model.IlustrationModel;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by UserModel on 5/13/2016.
 */
public class IlustrationProductAdapter extends BaseAdapter {
    private Context context;
    private List<IlustrationModel> list;
    private ListIlustrationHolder holder;

    public IlustrationProductAdapter(Context context, List<IlustrationModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_ilustration,null);
            holder = new ListIlustrationHolder();
            holder.holderUnitNo = (TextView) convertView.findViewById(R.id.txt_unitNo);
            holder.holdeArea = (TextView) convertView.findViewById(R.id.txt_area);
            holder.holderPriceInc = (TextView) convertView.findViewById(R.id.txt_priceInc);
            holder.holderSalesDisc = (TextView) convertView.findViewById(R.id.txt_salesDisc);
            holder.holderNetPrice = (TextView) convertView.findViewById(R.id.txt_netPrice);
            holder.txtNoteItemIlustrasi = (TextView) convertView.findViewById(R.id.txt_note_item_ilustrasi);
            holder.linearLayoutSales = (LinearLayout) convertView.findViewById(R.id.linear_sales_disc);
            holder.linearLayoutNetPrice = (LinearLayout) convertView.findViewById(R.id.linear_net_price);
            holder.linearLayoutNote = (LinearLayout) convertView.findViewById(R.id.linear_note_item_ilustrasi);
            holder.holdertxtProduct = (TextView) convertView.findViewById(R.id.txt_product);
            convertView.setTag(holder);
        }else{
            holder = (ListIlustrationHolder) convertView.getTag();
        }

        IlustrationModel ilustration = list.get(position);
        double salesDisc = Double.parseDouble(ilustration.getSalesDisc());
        double netPrice = Double.parseDouble(ilustration.getNetPrice());
        DecimalFormat decimalFormat = new DecimalFormat("###,##0");
        String decimalFormatSales = String.valueOf(decimalFormat.format(Double.parseDouble(ilustration.getSalesDisc())));
        String decimalFormatNet = String.valueOf(decimalFormat.format(Double.parseDouble(ilustration.getNetPrice())));

        Log.d("CekA",salesDisc+" "+netPrice);

        if (salesDisc==0.0){
            holder.linearLayoutSales.setVisibility(View.GONE);
        } else {
            holder.holderSalesDisc.setText("IDR " + decimalFormatSales);
        }

        if (netPrice==0.0){
            holder.linearLayoutNetPrice.setVisibility(View.GONE);
        } else {
            holder.holderNetPrice.setText("IDR " + decimalFormatNet);
        }

        if (salesDisc==0.0 || netPrice==0.0){
            holder.linearLayoutNote.setVisibility(View.VISIBLE);
        }

        holder.holderUnitNo.setText(ilustration.getUnitNo());
        holder.holdertxtProduct.setText(ilustration.getProductName());
        holder.holdeArea.setText(Html.fromHtml(ilustration.getLandBuild()));
        holder.holderPriceInc.setText(ilustration.getPriceInc());

        return convertView;
    }

    private class ListIlustrationHolder {
        TextView holderUnitNo,holdeArea,holderPriceInc,holderSalesDisc,holderNetPrice,txtNoteItemIlustrasi,holdertxtProduct;
        LinearLayout linearLayoutSales,linearLayoutNetPrice,linearLayoutNote;
    }
}
