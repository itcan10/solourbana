package com.nataproperty.solourbana.view.loginregister.model;

/**
 * Created by User on 4/17/2016.
 */
public class AgenTypeModel {
    public String getMemberTypeName() {
        return memberTypeName;
    }

    public void setMemberTypeName(String memberTypeName) {
        this.memberTypeName = memberTypeName;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    private String memberType;
    private String memberTypeName;
}
