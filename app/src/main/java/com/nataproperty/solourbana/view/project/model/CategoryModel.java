package com.nataproperty.solourbana.view.project.model;

/**
 * Created by UserModel on 5/3/2016.
 */
public class CategoryModel {
    long categoryRef;
    String dbMasterRef;
    String projectRef;
    String projectCategoryType;
    String categoryDescription;
    String isShowAvailableUnit;
    String countCluster;
    String clusterRef;
    String image;
    String isSales;

    public String getIsSales() {
        return isSales;
    }

    public void setIsSales(String isSales) {
        this.isSales = isSales;
    }

    public long getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(long categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectCategoryType() {
        return projectCategoryType;
    }

    public void setProjectCategoryType(String projectCategoryType) {
        this.projectCategoryType = projectCategoryType;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getIsShowAvailableUnit() {
        return isShowAvailableUnit;
    }

    public void setIsShowAvailableUnit(String isShowAvailableUnit) {
        this.isShowAvailableUnit = isShowAvailableUnit;
    }

    public String getCountCluster() {
        return countCluster;
    }

    public void setCountCluster(String countCluster) {
        this.countCluster = countCluster;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
