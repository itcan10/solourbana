package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.MaritalStatusModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class MaritalStatusAdapter extends BaseAdapter {
    private Context context;
    private List<MaritalStatusModel> list;
    private ListMaritalHolder holder;

    public MaritalStatusAdapter(Context context, List<MaritalStatusModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_marital_status,null);
            holder = new ListMaritalHolder();
            holder.maritalName = (TextView) convertView.findViewById(R.id.txt_marital_status);

            convertView.setTag(holder);
        }else{
            holder = (ListMaritalHolder) convertView.getTag();
        }
        MaritalStatusModel nation = list.get(position);
        holder.maritalName.setText(nation.getMaritalStatusName());

        return convertView;
    }

    private class ListMaritalHolder {
        TextView maritalName;
    }
}
