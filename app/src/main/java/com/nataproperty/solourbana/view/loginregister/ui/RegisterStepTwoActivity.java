package com.nataproperty.solourbana.view.loginregister.ui;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.FixedHoloDatePickerDialog;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyEditTextLatoReguler;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Administrator on 4/6/2016.
 */
public class RegisterStepTwoActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String STATUS_GOOGLE_SIGN_IN = "statusGoogleSignIn";
    public static final String NAME = "name";
    public static final String BIRTHDATE = "birthDate";
    public static final String ISACTIVE = "isActive";
    private static final String TAG = RegisterStepTwoActivity.class.getSimpleName();
    private MyEditTextLatoReguler txtFullname, txtPhone, txtEmail, txtPassword, txtPasswordConfirm;
    private MyEditTextLatoReguler txtBrith;
    private Button next;
    private String statusGoogleSignIn, name, birthDate, memberTypeName, projectPsRef;
    private int isActive, loginType;
    private Calendar myCalendar = Calendar.getInstance();
    private Toolbar toolbar;
    private TextView title;
    private Typeface font;
    private boolean isGuest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_step_two);
        initWidget();

        Intent intent = getIntent();
        String phone = intent.getStringExtra("phone");
        String email = intent.getStringExtra("email");
        String password = intent.getStringExtra("password");
        String confirmPassword = intent.getStringExtra("confirmPassword");
        projectPsRef = intent.getStringExtra("projectPsRef");
        statusGoogleSignIn = intent.getStringExtra(STATUS_GOOGLE_SIGN_IN);
        name = intent.getStringExtra(NAME);
        birthDate = intent.getStringExtra(BIRTHDATE);
        isGuest = intent.getBooleanExtra(General.GUEST, false);

        txtFullname.setText(name);
        txtBrith.setText(birthDate);
        txtPhone.setText(phone);
        txtEmail.setText(email);
        txtPassword.setText(password);
        txtPasswordConfirm.setText(confirmPassword);
        isActive = intent.getIntExtra(ISACTIVE, 0);
        loginType = intent.getIntExtra(General.INTENT_CAT_LOGIN, 1); // intent tipe login / register customer atau bukan
        memberTypeName = intent.getStringExtra("memberTypeName");

        txtBrith.setInputType(InputType.TYPE_NULL);
        txtBrith.setTextIsSelectable(true);
        txtBrith.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPicker();
            }
        });
        txtBrith.setFocusable(false);

        if (isGuest){
            next.setText(getResources().getString(R.string.register_guest));
        } else {
            if (loginType == General.LOGIN_CUSTOMER) {
                next.setText(getResources().getString(R.string.register_customer));
            }
        }

        //is_active dari google
        //jika email kosong berarti dari customer daftar dari agent yang udah ada di napro
        Log.d("isActive", "" + String.valueOf(isActive));
        if (email.equals("")) {
            txtEmail.setEnabled(true);
        } else {
            if (isActive == 1) {
                txtEmail.setEnabled(false);
            } else {
                txtEmail.setEnabled(false);
            }
        }

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_register_two));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        txtFullname = (MyEditTextLatoReguler) findViewById(R.id.txtFullname);
        txtBrith = (MyEditTextLatoReguler) findViewById(R.id.txtBirthDate);
        txtPhone = (MyEditTextLatoReguler) findViewById(R.id.txtPhone);
        txtEmail = (MyEditTextLatoReguler) findViewById(R.id.txtEmail);
        txtPassword = (MyEditTextLatoReguler) findViewById(R.id.txtPassword);
        txtPasswordConfirm = (MyEditTextLatoReguler) findViewById(R.id.txtPasswordConfirm);

        next = (Button) findViewById(R.id.btnNext);
        next.setTypeface(font);
        next.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnNext:
                if (isGuest){
                    //projectPsRef untuk Guest
                    projectPsRef = "0";
                    dialogRegisterCustomer(projectPsRef);
                } else {
                    registerAgentCustomerOwner();
                }

                break;
        }
    }

    private void registerAgentCustomerOwner (){
        if (!validate()) {
            onLoginFailed();
            return;
        }

        String fullname = txtFullname.getText().toString().trim();
        String phone = txtPhone.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String passwordConfirm = txtPasswordConfirm.getText().toString().trim();
        String brithDate = txtBrith.getText().toString().trim();

        if (!fullname.isEmpty() && !phone.isEmpty() && !email.isEmpty() && !password.isEmpty()
                && !passwordConfirm.isEmpty() && password.equals(passwordConfirm) && !brithDate.isEmpty()) {

            if (loginType == General.LOGIN_CUSTOMER) {
                // TODO tampilkan dialog untuk kirim data register
                dialogRegisterCustomer(projectPsRef);
            } else {
                Intent intent = new Intent(RegisterStepTwoActivity.this, RegisterAgentTypeActivity.class);
                intent.putExtra("fullname", fullname);
                intent.putExtra("brithDate", brithDate);
                intent.putExtra("phone", phone);
                intent.putExtra("email", email);
                intent.putExtra("password", password);
                intent.putExtra("passwordConfrim", passwordConfirm);
                intent.putExtra(STATUS_GOOGLE_SIGN_IN, statusGoogleSignIn);

                startActivity(intent);

            }

        } else {
            Toast.makeText(getApplicationContext(), "Please enter your details!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * fungsi untuk menampilkan dialog ketika akan register customer
     */
    private void dialogRegisterCustomer(final String projectPsRef) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                registerCustomerFinish(projectPsRef);
            }
        });
        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialogBuilder.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(RegisterStepTwoActivity.this, RegisterTermActivity.class));
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * fungsi untuk kirim register customer
     */
    private void registerCustomerFinish(final String projectPsRef) {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getSaveRegisterCustomerSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();

                try {
                    JSONObject jo = new JSONObject(response);
                    String message = jo.getString("message");
                    Log.d("result register", response);
                    int status = jo.getInt("status");

                    if (status == 200) {
                        // generate json ke string
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        String memberTypeCode = jo.getJSONObject("data").getString("memberTypeCode");
                        // simpan profile user ke dalam shared preferences
                        SharedPreferences sharedPreferences = RegisterStepTwoActivity.this.
                                getSharedPreferences(RegisterAgentTypeActivity.PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.putString("isMemberTypeCode", memberTypeCode);
                        editor.commit();
                        // tampilkan register finish
                        Intent intent = new Intent(RegisterStepTwoActivity.this, RegisterFinishActivity.class);
                        intent.putExtra(RegisterAgentTypeActivity.EMAIL, email);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else {
                        //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        dialogPopUp(message);
                        next.setEnabled(true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterStepTwoActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterStepTwoActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fullname", txtFullname.getText().toString());
                params.put("username", txtEmail.getText().toString());
                params.put("mobile", txtPhone.getText().toString());
                params.put("password", txtPassword.getText().toString());
                params.put("birthDate", txtBrith.getText().toString());
                params.put("projectCode", General.projectCode);
                params.put("projectPsRef", projectPsRef);
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "saveRegisterCustomer");
    }

    private void dialogPopUp(String message) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(RegisterStepTwoActivity.this);
        LayoutInflater inflater = RegisterStepTwoActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_activate_email, null);
        dialogBuilder.setView(dialogView);
        final TextView txtActivate = (TextView) dialogView.findViewById(R.id.txt_activate);
        txtActivate.setText(message);
        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                next.setEnabled(true);
            }
        });
        android.app.AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void dialogPicker() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final Context themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog);
        final DatePickerDialog dialog = new FixedHoloDatePickerDialog(
                themedContext,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dialog.show();
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txtBrith.setText(sdf.format(myCalendar.getTime()));
    }


    public boolean validate() {
        boolean valid = true;

        String fullname = txtFullname.getText().toString().trim();
        String phone = txtPhone.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String passwordConfirm = txtPasswordConfirm.getText().toString().trim();
        String brithDate = txtBrith.getText().toString().trim();

        if (fullname.isEmpty()) {
            txtFullname.setError("enter a fullname");
            valid = false;
        } else {
            txtFullname.setError(null);
        }

        if (phone.isEmpty()) {
            txtPhone.setError("enter a mobile phone");
            valid = false;
        } else {
            txtPhone.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txtEmail.setError("enter a valid email address");
            valid = false;
        } else {
            txtEmail.setError(null);
        }

        if (password.isEmpty()) {
            txtPassword.setError("enter a password");
            valid = false;
        } else {
            txtPassword.setError(null);
        }

        if (passwordConfirm.isEmpty()) {
            txtPasswordConfirm.setError("enter a password");
            valid = false;
        } else if (!password.equals(passwordConfirm)) {
            txtPasswordConfirm.setError("password don't match");
            valid = false;
        } else {
            txtPasswordConfirm.setError(null);
        }

        if (brithDate.isEmpty()) {
            txtBrith.setError("enter a birthdate");
            valid = false;
        } else {
            txtBrith.setError(null);
        }

        return valid;
    }

    public void onLoginFailed() {
        //Toast.makeText(getBaseContext(), "Register failed", Toast.LENGTH_LONG).show();

        next.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        next.setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}

