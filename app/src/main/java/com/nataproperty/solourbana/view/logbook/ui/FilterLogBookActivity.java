package com.nataproperty.solourbana.view.logbook.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerCityAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerProvinceAdapter;
import com.nataproperty.solourbana.view.logbook.presenter.LogBookFilterPresenter;

import java.util.ArrayList;

import retrofit2.Response;

public class FilterLogBookActivity extends AppCompatActivity {
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private LogBookFilterPresenter presenter;

    Toolbar toolbar;
    Typeface font;
    TextView title;
    Spinner spinnerProvinsi, spinnerCity;
    Button btnNext;

    ArrayList<ProvinceModel> listProvince = new ArrayList<ProvinceModel>();
    ArrayList<CityModel> listCity = new ArrayList<CityModel>();
    SpinnerCityAdapter adapterCity;
    SpinnerProvinceAdapter adapterProvince;
    String countryCode, provinceCode, cityCode, dbMasterRef,projectRef;
    EditText edtKeyword;
    String keyword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_log_book);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new LogBookFilterPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        requestProvince();

        Intent i = getIntent();
        dbMasterRef = i.getStringExtra("dbMasterRef");
        projectRef = i.getStringExtra("projectRef");

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterLogBookActivity.this,LogBookContactBankActivity.class);
                intent.putExtra("dbMasterRef",dbMasterRef);
                intent.putExtra("projectRef",projectRef);
                intent.putExtra("provinceCode", provinceCode);
                intent.putExtra("cityCode", cityCode);
                intent.putExtra("keyword", edtKeyword.getText().toString());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }



    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        title.setText("Search Log Book");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        spinnerProvinsi = (Spinner) findViewById(R.id.spn_province);
        spinnerCity = (Spinner) findViewById(R.id.spn_city);
        btnNext = (Button) findViewById(R.id.btn_next);
        edtKeyword = (EditText) findViewById(R.id.edt_keyword);
    }

    private void requestProvince() {
        presenter.getProvinceListAllSvc(General.coutryCode);
    }

    public void showListProvinceResults(Response<ArrayList<ProvinceModel>> response) {
        listProvince = response.body();
        initSpinnerProvince();
    }

    public void showListProvinceFailure(Throwable t) {

    }

    private void initSpinnerProvince() {
        adapterProvince = new SpinnerProvinceAdapter(this, listProvince);
        spinnerProvinsi.setAdapter(adapterProvince);
        spinnerProvinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvince.get(position).getProvinceCode();
                requestCity(General.coutryCode, provinceCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void requestCity(String countryCode, String provinceCode) {
        presenter.getCityListAllSvc(countryCode, provinceCode);
    }

    public void showListCityResults(Response<ArrayList<CityModel>> response) {
        listCity = response.body();
        initSpinnerCity();
    }

    public void showListCityFailure(Throwable t) {

    }

    private void initSpinnerCity() {
        adapterCity = new SpinnerCityAdapter(this, listCity);
        spinnerCity.setAdapter(adapterCity);
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listCity.get(position).getCityCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
