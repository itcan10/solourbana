package com.nataproperty.solourbana.view.ilustration.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterDescriptionFragmentInteractor {
    void getListingDetail(String listingRef,String memberRef);
    void sendToFriend(String email,String memberRef,String listingRef);
    void sendNotif(String agencyCompanyRef,String listingRef,String memberRef);
    void rxUnSubscribe();

}
