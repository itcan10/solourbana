package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class DataPrice {
    String paymentTerm, priceInc, discPercent,discAmt,netPrice,termCondition,urlPaymentIlustration,bookingTermAndCondition;

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPriceInc() {
        return priceInc;
    }

    public void setPriceInc(String priceInc) {
        this.priceInc = priceInc;
    }

    public String getDiscPercent() {
        return discPercent;
    }

    public void setDiscPercent(String discPercent) {
        this.discPercent = discPercent;
    }

    public String getDiscAmt() {
        return discAmt;
    }

    public void setDiscAmt(String discAmt) {
        this.discAmt = discAmt;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    public String getTermCondition() {
        return termCondition;
    }

    public void setTermCondition(String termCondition) {
        this.termCondition = termCondition;
    }

    public String getUrlPaymentIlustration() {
        return urlPaymentIlustration;
    }

    public void setUrlPaymentIlustration(String urlPaymentIlustration) {
        this.urlPaymentIlustration = urlPaymentIlustration;
    }

    public String getBookingTermAndCondition() {
        return bookingTermAndCondition;
    }

    public void setBookingTermAndCondition(String bookingTermAndCondition) {
        this.bookingTermAndCondition = bookingTermAndCondition;
    }
}
