package com.nataproperty.solourbana.view.menuitem.model;

/**
 * Created by UserModel on 6/28/2016.
 */
public class NotificationModel {
    String gcmMessageRef,dbMasterRef,projectRef,gcmTitle,gcmMessage,gcmMessageTypeRef,inputTime,linkDetail;

    public String getGcmMessageRef() {
        return gcmMessageRef;
    }

    public void setGcmMessageRef(String gcmMessageRef) {
        this.gcmMessageRef = gcmMessageRef;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getGcmTitle() {
        return gcmTitle;
    }

    public void setGcmTitle(String gcmTitle) {
        this.gcmTitle = gcmTitle;
    }

    public String getGcmMessage() {
        return gcmMessage;
    }

    public void setGcmMessage(String gcmMessage) {
        this.gcmMessage = gcmMessage;
    }

    public String getGcmMessageTypeRef() {
        return gcmMessageTypeRef;
    }

    public void setGcmMessageTypeRef(String gcmMessageTypeRef) {
        this.gcmMessageTypeRef = gcmMessageTypeRef;
    }

    public String getInputTime() {
        return inputTime;
    }

    public void setInputTime(String inputTime) {
        this.inputTime = inputTime;
    }

    public String getLinkDetail() {
        return linkDetail;
    }

    public void setLinkDetail(String linkDetail) {
        this.linkDetail = linkDetail;
    }
}
