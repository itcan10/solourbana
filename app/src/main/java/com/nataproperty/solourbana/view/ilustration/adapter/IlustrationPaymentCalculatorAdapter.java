package com.nataproperty.solourbana.view.ilustration.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.ilustration.model.PaymentModel;

import java.util.List;

/**
 * Created by UserModel on 5/14/2016.
 */
public class IlustrationPaymentCalculatorAdapter extends BaseAdapter {
    private Context context;
    private List<PaymentModel> list;
    private ListPaymentHolder holder;

    public IlustrationPaymentCalculatorAdapter(Context context, List<PaymentModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_payment_schedule,null);
            holder = new ListPaymentHolder();
            holder.schedule = (TextView) convertView.findViewById(R.id.txt_schedule);
            holder.dueDate = (TextView) convertView.findViewById(R.id.txt_duedate);
            holder.amount = (TextView) convertView.findViewById(R.id.txt_amount);

            convertView.setTag(holder);
        }else{
            holder = (ListPaymentHolder) convertView.getTag();
        }

        PaymentModel payment = list.get(position);
        holder.schedule.setText(payment.getSchedule());
        holder.dueDate.setText(payment.getDueDate());
        holder.amount.setText(payment.getAmount());

        return convertView;
    }

    private class ListPaymentHolder {
        TextView schedule,dueDate,amount;
    }
}
