package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by nata on 12/2/2016.
 */
public class PropertyInfo {

    private String propertys;
    private String category;
    private String product;
    private String unit;
    private String area;
    private String priceInc;
    private String bookingFee;
    private String specialEnquiries;

    public String getPropertys() {
        return propertys;
    }

    public void setPropertys(String propertys) {
        this.propertys = propertys;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPriceInc() {
        return priceInc;
    }

    public void setPriceInc(String priceInc) {
        this.priceInc = priceInc;
    }

    public String getBookingFee() {
        return bookingFee;
    }

    public void setBookingFee(String bookingFee) {
        this.bookingFee = bookingFee;
    }

    public String getSpecialEnquiries() {
        return specialEnquiries;
    }

    public void setSpecialEnquiries(String specialEnquiries) {
        this.specialEnquiries = specialEnquiries;
    }
}
