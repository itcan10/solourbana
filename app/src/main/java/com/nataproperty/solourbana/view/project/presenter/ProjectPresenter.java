package com.nataproperty.solourbana.view.project.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.project.intractor.PresenterProjectInteractor;
import com.nataproperty.solourbana.view.project.model.LocationModel;
import com.nataproperty.solourbana.view.project.model.ProjectModelNew;
import com.nataproperty.solourbana.view.project.ui.ProjectActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ProjectPresenter implements PresenterProjectInteractor {
    private ProjectActivity view;
    private ServiceRetrofitProject service;

    public ProjectPresenter(ProjectActivity view, ServiceRetrofitProject service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void getLocation(String memberRef, String projectCode) {
        Call<List<LocationModel>> call = service.getAPI().getLocation(memberRef,projectCode);
        call.enqueue(new Callback<List<LocationModel>>() {
            @Override
            public void onResponse(Call<List<LocationModel>> call, Response<List<LocationModel>> response) {
                view.showLocationResults(response);
            }

            @Override
            public void onFailure(Call<List<LocationModel>> call, Throwable t) {
                view.showLocationFailure(t);

            }


        });
    }

    @Override
    public void getListProject(String memberRef, String locationRef, String projectCode) {
        Call<List<ProjectModelNew>> call = service.getAPI().getListProject(memberRef,locationRef,projectCode);
        call.enqueue(new Callback<List<ProjectModelNew>>() {
            @Override
            public void onResponse(Call<List<ProjectModelNew>> call, Response<List<ProjectModelNew>> response) {
                view.showListProjectResults(response);
            }

            @Override
            public void onFailure(Call<List<ProjectModelNew>> call, Throwable t) {
                view.showListProjectFailure(t);

            }


        });
    }
}
