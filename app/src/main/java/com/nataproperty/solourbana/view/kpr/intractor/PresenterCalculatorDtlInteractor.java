package com.nataproperty.solourbana.view.kpr.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCalculatorDtlInteractor {
    void getListTemplate(String project, String memberRef);

}
