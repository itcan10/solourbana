package com.nataproperty.solourbana.view.mainmenu.ui;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.gcm.GCMRegistrationIntentService;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.mainmenu.adapter.MainMenuAdapter;
import com.nataproperty.solourbana.view.mainmenu.model.MainMenuModel;
import com.nataproperty.solourbana.view.mainmenu.model.UserModel;
import com.nataproperty.solourbana.view.mainmenu.presenter.MainMenuPresenter;
import com.nataproperty.solourbana.view.menuitem.model.StatusTokenGCMModel;
import com.nataproperty.solourbana.view.menuitem.ui.AboutUsActivity;
import com.nataproperty.solourbana.view.menuitem.ui.ChangePasswordActivity;
import com.nataproperty.solourbana.view.menuitem.ui.ContactUsActivity;
import com.nataproperty.solourbana.view.menuitem.ui.NotificationActivity;
import com.nataproperty.solourbana.view.profile.ui.SectionProfileActivity;
import com.nataproperty.solourbana.view.profile.ui.SettingNotificationActivity;
import com.nataproperty.solourbana.view.report.model.CheckAvailableReportModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by UserModel on 4/21/2016.
 */
public class MainMenuActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, View.OnClickListener {
    public static final String TAG = "MainMenuActivity";
    public static final String PREF_NAME = "pref";
    public static final String MEMBER_REF = "memberRef";
    public static final String GCM_TOKEN = "GCMToken";
    public static boolean OFF_LINE_MODE = false;
    public static boolean TICKET = true;
    private CoordinatorLayout coordinatorLayout;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private MainMenuPresenter presenter;
    ProgressDialog progressDialog;
    Context context;
    SharedPreferences sharedPreferences;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView nameProfile, memberTypeName;
    private ImageView imgProfile;
    private static final int RC_SIGN_IN = 0;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    private Tracker mTracker;
    String email, name, memberRef, memberType, memberTypeCode, dbMasterRef = "", projectRef = "", userRef;
    boolean state, isReport;
    AlertDialog alertDialogSave;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    AlertDialog alertDialog;
    RecyclerView recyclerViewMenu;

    private MainMenuAdapter mainMenuAdapter;
    // list icon menu
    int menuIcon[] = new int[]{
            R.drawable.selector_menu_all_project,
            R.drawable.selector_menu_my_listing,
            R.drawable.selector_menu_download,
            R.drawable.selector_menu_commision,
            R.drawable.selector_menu_nup,
            R.drawable.selector_menu_mybooking,
            R.drawable.selector_menu_news,
            R.drawable.selector_menu_event,
            R.drawable.selector_menu_ticket,
            R.drawable.selector_menu_calculator,
//            R.drawable.selector_menu_log_book,
//            R.drawable.selector_menu_report
    };

    // list title menu
    int menuTitle[] = new int[]{
            R.string.menu_all_project,
            R.string.menu_mylisting,
            R.string.menu_download,
            R.string.menu_commision,
            R.string.menu_mynup,
            R.string.menu_mybooking,
            R.string.menu_news,
            R.string.menu_event,
            R.string.menu_ticket,
            R.string.menu_calculator,
//            R.string.menu_log_book,
//            R.string.menu_report
    };
    List<MainMenuModel> menulist = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new MainMenuPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        context = this;
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        email = sharedPreferences.getString("isEmail", null);
        name = sharedPreferences.getString("isName", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberType = sharedPreferences.getString("isMemberType", null);
        memberTypeCode = sharedPreferences.getString("isMemberTypeCode", "");

        //googleAnalitic
        BaseApplication application = (BaseApplication) getApplication();
        //mTracker = application.getDefaultTracker();

        animationButton();
        textSet();
        serviceGCM();
        buidNewGoogleApiClient();
        requestCekReport();
        //logBookSetup();

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, SectionProfileActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name = sharedPreferences.getString("isName", null);
        nameProfile.setText(name);
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
        Glide.with(context).load(WebService.getProfile() + memberRef)
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.drawable.profile_image)
                .placeholder(R.drawable.profile_image)
                .into(imgProfile);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.app_name));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.icon_mini_carstenz);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        nameProfile = (TextView) findViewById(R.id.profilName);
        imgProfile = (ImageView) findViewById(R.id.profileImage);
        memberTypeName = (TextView) findViewById(R.id.memberTypeName);
        recyclerViewMenu = (RecyclerView) findViewById(R.id.listMenu);

        mainMenuAdapter = new MainMenuAdapter(this, menulist);
        recyclerViewMenu.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewMenu.setHasFixedSize(true);
        recyclerViewMenu.setAdapter(mainMenuAdapter);

        for (int i = 0; i < menuIcon.length; i++) {
            MainMenuModel mm = new MainMenuModel();
            mm.setMenuIcon(menuIcon[i]);
            mm.setMenuTitle(getResources().getString(menuTitle[i]));

            menulist.add(mm);
        }
        mainMenuAdapter.notifyDataSetChanged();

    }

    private void logBookSetup() {
        if (memberTypeCode.equals(General.inhouse)) {
            MainMenuModel mm = new MainMenuModel();
            mm.setMenuIcon(R.drawable.selector_menu_log_book);
            mm.setMenuTitle(getResources().getString(R.string.menu_log_book));
            menulist.add(mm);
        }
        mainMenuAdapter.notifyDataSetChanged();
    }

    private void textSet() {
        nameProfile.setText(name);
        memberTypeName.setText(memberType);
    }

    private void animationButton() {
        stopService(new Intent(getApplicationContext(), GCMRegistrationIntentService.class));
    }

    private void serviceGCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    //Registration success
                    String token = intent.getStringExtra("token");
                    //Log.d("GCMToken", token);
                    saveGCMTokenToServer(token);

                } else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)) {

                } else {

                }
            }
        };

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                //So notification
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);

        }
    }

    //savegcmtoken
    private void saveGCMTokenToServer(final String GCMToken) {
        presenter.sendGCMToken(GCMToken, memberRef);

    }

    public void showSendGCMTokenResults(retrofit2.Response<StatusTokenGCMModel> responseToken) {
        if (responseToken.isSuccessful()) {
            int Status = responseToken.body().getStatus();
            String message = responseToken.body().getMessage();
            if (Status == 200) {
                Log.d("saveToken", message);
            } else {
                Log.d("saveToken", message);
            }
        }
    }

    public void showSendGCMTokenFailure(Throwable t) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "OFFLINE MODE", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(MainMenuActivity.this, LaunchActivity.class));
                        finish();
                    }
                });

        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_profile:
                startActivity(new Intent(MainMenuActivity.this, SectionProfileActivity.class));
                return true;

            case R.id.btn_change_password:
                startActivity(new Intent(MainMenuActivity.this, ChangePasswordActivity.class));
                return true;

            case R.id.btn_logout:
                dialogLogout();
                return true;

            case R.id.btn_contact_us:
                startActivity(new Intent(MainMenuActivity.this, ContactUsActivity.class));
                return true;

            case R.id.btn_about_us:
                startActivity(new Intent(MainMenuActivity.this, AboutUsActivity.class));
                return true;

            case R.id.btn_setting:
                startActivity(new Intent(MainMenuActivity.this, SettingNotificationActivity.class));
                return true;

            case R.id.btn_notification:
                startActivity(new Intent(MainMenuActivity.this, NotificationActivity.class));
                return true;

            case R.id.btn_exit:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dialogLogout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah anda ingin logout?");
        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPreferences sharedPreferences = MainMenuActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", false);
                        editor.putString("isEmail", "");
                        editor.putString("isName", "");
                        editor.putString("isMemberRef", "");
                        editor.putString("isMemberType", "");
                        editor.commit();
                        // logout fb
                        LoginManager.getInstance().logOut();

                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(MainMenuActivity.this, LaunchActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();

                                    }
                                });
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void buidNewGoogleApiClient() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, MainMenuActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    //Report
    private void requestCekReport() {
        presenter.CheckAvailableReportSvc(General.projectCode, dbMasterRef, projectRef, memberRef);
    }

    public void showCheckAvailableReportResults(Response<CheckAvailableReportModel> response) {
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            int isView = response.body().getIsView();
            userRef = response.body().getUserRef();
            Log.d("cek", status + " " + isView + " "+ userRef);
            UserModel.getInstance().setUserRef(userRef);

            if (status == 200 && isView > 0 && memberTypeCode.equals(General.inhouse)) {
                // berhasil memunculkan report
                MainMenuModel mm = new MainMenuModel();
                mm.setMenuIcon(R.drawable.selector_menu_report);
                mm.setMenuTitle(getResources().getString(R.string.menu_report));
                menulist.add(mm);
                mainMenuAdapter.notifyDataSetChanged();
            }
        }
    }

    public void showCheckAvailableReportFailure(Throwable t) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profileImage:
                startActivity(new Intent(MainMenuActivity.this, SectionProfileActivity.class));
                break;
        }

    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

        if (alertDialogSave != null && alertDialogSave.isShowing()) {
            alertDialogSave.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

        if (alertDialogSave != null && alertDialogSave.isShowing()) {
            alertDialogSave.dismiss();
        }
    }

}
