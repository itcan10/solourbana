package com.nataproperty.solourbana.view.project.model;

/**
 * Created by UserModel on 5/10/2016.
 */
public class ProductDetailModel {
    String dbMasterRef,projectRef,productRef,dbMasterProjectProductFileRef,querystring,title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public String getDbMasterProjectProductFileRef() {
        return dbMasterProjectProductFileRef;
    }

    public void setDbMasterProjectProductFileRef(String dbMasterProjectProductFileRef) {
        this.dbMasterProjectProductFileRef = dbMasterProjectProductFileRef;
    }

    public String getQuerystring() {
        return querystring;
    }

    public void setQuerystring(String querystring) {
        this.querystring = querystring;
    }
}
