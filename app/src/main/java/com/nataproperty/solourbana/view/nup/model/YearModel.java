package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by UserModel on 5/22/2016.
 */
public class YearModel {
    String year;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
