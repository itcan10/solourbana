package com.nataproperty.solourbana.view.booking.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.booking.intractor.PresenterBookingFinishInteractor;
import com.nataproperty.solourbana.view.booking.model.MemberInfoModel;
import com.nataproperty.solourbana.view.booking.model.PaymentModel;
import com.nataproperty.solourbana.view.booking.ui.BookingFinishActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class BookingFinishPresenter implements PresenterBookingFinishInteractor {
    private BookingFinishActivity view;
    private ServiceRetrofit service;

    public BookingFinishPresenter(BookingFinishActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getIlustrationPaymentSvc(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<PaymentModel> call = service.getAPI().getIlustrationPaymentSvc(dbMasterRef,projectRef, clusterRef,  productRef,  unitRef,  termRef,  termNo);
        call.enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
                view.showPaymentModelResults(response);
            }

            @Override
            public void onFailure(Call<PaymentModel> call, Throwable t) {
                view.showPaymentModelFailure(t);

            }


        });
    }

    @Override
    public void getMemberInfoSvc(String memberRef) {
        Call<MemberInfoModel> call = service.getAPI().getMemberInfoSvc(memberRef);
        call.enqueue(new Callback<MemberInfoModel>() {
            @Override
            public void onResponse(Call<MemberInfoModel> call, Response<MemberInfoModel> response) {
                view.showMemberInfoResults(response);
            }

            @Override
            public void onFailure(Call<MemberInfoModel> call, Throwable t) {
                view.showMemberInfoFailure(t);

            }


        });
    }

}
