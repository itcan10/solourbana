package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.nup.model.AccountBankModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class AccountBankAdapter extends BaseAdapter {
    private Context context;
    private List<AccountBankModel> list;
    private ListBankHolder holder;

    public AccountBankAdapter(Context context, List<AccountBankModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_account_bank,null);

            holder = new ListBankHolder();
            holder.bankName = (TextView) convertView.findViewById(R.id.txt_bank_name);
            holder.accName = (TextView) convertView.findViewById(R.id.txt_acc_bank);
            holder.accNo = (TextView) convertView.findViewById(R.id.txt_no_bank);

            convertView.setTag(holder);
        }else{
            holder = (ListBankHolder) convertView.getTag();
        }

        AccountBankModel account = list.get(position);
        holder.bankName.setText(account.getBankName());
        holder.accName.setText(account.getAccName());
        holder.accNo.setText(account.getAccNo());

        return convertView;

    }

    private class ListBankHolder {
        TextView bankName,accName,accNo;
    }
}
