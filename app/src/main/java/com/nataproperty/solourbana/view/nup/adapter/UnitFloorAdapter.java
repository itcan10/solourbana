package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.nup.model.UnitFloorModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class UnitFloorAdapter extends BaseAdapter {
    private Context context;
    private List<UnitFloorModel> list;
    private ListFeatureHolder holder;

    public UnitFloorAdapter(Context context, List<UnitFloorModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.spinner_item,null);
            holder = new ListFeatureHolder();
            holder.locationName = (TextView) convertView.findViewById(R.id.txt_location_name);

            convertView.setTag(holder);
        }else{
            holder = (ListFeatureHolder) convertView.getTag();
        }

        UnitFloorModel locationModel = list.get(position);
        holder.locationName.setText(locationModel.getSurveyUnitFloorName());

        return convertView;

    }

    private class ListFeatureHolder {
        TextView locationName;
    }
}
