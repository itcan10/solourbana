package com.nataproperty.solourbana.view.event.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.event.model.ResponseRsvp;
import com.nataproperty.solourbana.view.event.presenter.EventRsvpPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class EventRsvpActivity extends AppCompatActivity {
    public static final String TAG = "EventRsvpActivity";
    public static final String CONTENT_REF = "contentRef";
    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String GUEST_NAME = "guest_name";
    public static final String COMPANY_NAME = "company_name";
    public static final String GUEST_PHONE = "guest_phone";
    public static final String GUEST_MOBILE1 = "guest_hp1";
    public static final String GUEST_MOBILE2 = "guest_hp2";
    public static final String GUEST_EMAIL = "guest_email";
    public static final String STATUS = "status";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private EventRsvpPresenter presenter;
    ProgressDialog progressDialog;
    String message;
    int getStatus;
    EditText edtGuestName, edtCompanyName, edtPhone, edtMobile1, edtMobile2, edtEmail;
    Button btnSubmit;
    String contentRef, eventScheduleRef, status, linkDetail, memberRef, email, name;
    SharedPreferences sharedPreferences;
    Toolbar toolbar;
    Typeface font;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_rsvp);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new EventRsvpPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        contentRef = intent.getStringExtra(CONTENT_REF);
        eventScheduleRef = intent.getStringExtra(EVENT_SCHEDULE_REF);
        status = intent.getStringExtra(STATUS);
        //linkDetail = intent.getStringExtra(LINK_DETAIL);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        email = sharedPreferences.getString("isEmail", null);
        name = sharedPreferences.getString("isName", null);
        initWidget();

        Log.d(TAG, contentRef + " " + eventScheduleRef + " " + status);

        edtGuestName.setText(name);
        edtEmail.setText(email);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekGuest = edtGuestName.getText().toString().trim();
                String cekPhone = edtPhone.getText().toString().trim();
                String cekMobile1 = edtMobile1.getText().toString().trim();
                String cekEmail = edtEmail.getText().toString().trim();

                if (cekGuest.isEmpty() || cekMobile1.isEmpty() || cekEmail.isEmpty()) {

                    if (cekGuest.isEmpty()) {
                        edtGuestName.setError("Guest name is required");
                    } else {
                        edtGuestName.setError(null);
                    }
                   /* if (cekPhone.isEmpty()){
                        edtPhone.setError("Phone is required");
                    }else {
                        edtPhone.setError(null);
                    }*/
                    if (cekMobile1.isEmpty()) {
                        edtMobile1.setError("Mobile 1 is required");
                    } else {
                        edtMobile1.setError(null);
                    }
                    if (cekEmail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(cekEmail).matches()) {
                        edtEmail.setError("Email is required");
                    } else {
                        edtEmail.setError(null);
                    }

                } else {

                    registerRsvp();
                }

            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_rsvp));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edtGuestName = (EditText) findViewById(R.id.edt_guest_name);
        edtCompanyName = (EditText) findViewById(R.id.edt_company_name);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        edtMobile1 = (EditText) findViewById(R.id.edt_mobile1);
        edtMobile2 = (EditText) findViewById(R.id.edt_mobile2);
        edtEmail = (EditText) findViewById(R.id.edt_email);

        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnSubmit.setTypeface(font);
    }

    private void registerRsvp() {
        String guest_name = edtGuestName.getText().toString();
        String company_name = edtCompanyName.getText().toString();
        String guest_phone = edtPhone.getText().toString();
        String guest_hp1 = edtMobile1.getText().toString();
        String guest_hp2 = edtMobile2.getText().toString();
        String guest_email = edtEmail.getText().toString();

        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        Map<String, String> fields = new HashMap<>();
        fields.put("memberRef", memberRef);
        fields.put("contentRef", contentRef);
        fields.put("eventScheduleRef", eventScheduleRef);
        fields.put("guest_name", guest_name);
        fields.put("company_name", company_name);
        fields.put("guest_phone", guest_phone);
        fields.put("guest_hp1", guest_hp1);
        fields.put("guest_hp2", guest_hp2);
        fields.put("guest_email", guest_email);
        fields.put("status", status);
        presenter.saveRsvpSvc(fields);
    }

    public void showResponeResults(Response<ResponseRsvp> response) {
        progressDialog.dismiss();
        getStatus = response.body().getStatus();
        message = response.body().getMessage();
        linkDetail = response.body().getLinkRsvp();

        if (getStatus == 200) {
            Intent intent = new Intent(EventRsvpActivity.this, EventRsvpFinishActivity.class);
            intent.putExtra(CONTENT_REF, contentRef);
            intent.putExtra(LINK_DETAIL, linkDetail);
            startActivity(intent);
            finish();
        } else if (getStatus == 201) {
            Intent intent = new Intent(EventRsvpActivity.this, EventRsvpFinishActivity.class);
            intent.putExtra(CONTENT_REF, contentRef);
            intent.putExtra(LINK_DETAIL, linkDetail);
            startActivity(intent);
            finish();
        } else if (getStatus == 202) {
            edtEmail.setError(message);
            btnSubmit.setEnabled(true);
        } else {

        }
    }

    public void showResponeFailure(Throwable t) {
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EventRsvpActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
