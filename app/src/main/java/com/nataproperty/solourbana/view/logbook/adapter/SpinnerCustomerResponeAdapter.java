package com.nataproperty.solourbana.view.logbook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.logbook.model.CustomerResponse;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/17/2016.
 */
public class SpinnerCustomerResponeAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<CustomerResponse> list;
    private ListCountryHolder holder;

    public SpinnerCustomerResponeAdapter(Context context, ArrayList<CustomerResponse> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_spinner_with_icon,null);
            holder = new ListCountryHolder();
            holder.countryName = (TextView) convertView.findViewById(R.id.name);
            holder.iconResponse = (ImageView) convertView.findViewById(R.id.icon_response);
            convertView.setTag(holder);
        }else{
            holder = (ListCountryHolder) convertView.getTag();
        }
        CustomerResponse customerResponse = list.get(position);
        holder.countryName.setText(customerResponse.getCustomerResponseStatusName());
        Glide.with(context).load(customerResponse.getIconResponse()).into(holder.iconResponse);
        return convertView;
    }

    private class ListCountryHolder {
        TextView countryName;
        ImageView iconResponse;
    }
}
