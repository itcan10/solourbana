package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class DetailBooking {
    int status;
    String message;
    BookingInfo bookingInfo;
    UnitInfoModel unitInfo;

    public BookingInfo getBookingInfo() {
        return bookingInfo;
    }

    public void setBookingInfo(BookingInfo bookingInfo) {
        this.bookingInfo = bookingInfo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UnitInfoModel getUnitInfo() {
        return unitInfo;
    }

    public void setUnitInfo(UnitInfoModel unitInfo) {
        this.unitInfo = unitInfo;
    }
}
