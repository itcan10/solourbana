package com.nataproperty.solourbana.view.commision.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.commision.intractor.PresenterCommisionSectionInteractor;
import com.nataproperty.solourbana.view.commision.model.CommisionSectionModel;
import com.nataproperty.solourbana.view.commision.ui.CommisionSectionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CommisionSectionPresenter implements PresenterCommisionSectionInteractor {
    private CommisionSectionActivity view;
    private ServiceRetrofitProject service;

    public CommisionSectionPresenter(CommisionSectionActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListCommisionSection(String memberRef,String projectCode) {
        Call<List<CommisionSectionModel>> call = service.getAPI().getListCommisionSection(memberRef,projectCode);
        call.enqueue(new Callback<List<CommisionSectionModel>>() {
            @Override
            public void onResponse(Call<List<CommisionSectionModel>> call, Response<List<CommisionSectionModel>> response) {
                view.showListCommisionSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<CommisionSectionModel>> call, Throwable t) {
                view.showListCommisionSectionFailure(t);

            }


        });
    }

}
