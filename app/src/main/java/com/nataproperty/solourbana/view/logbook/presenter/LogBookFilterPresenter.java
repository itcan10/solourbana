package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.LogBookFilterInteractor;
import com.nataproperty.solourbana.view.logbook.ui.FilterLogBookActivity;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LogBookFilterPresenter implements LogBookFilterInteractor {
    private FilterLogBookActivity view;
    private ServiceRetrofitProject service;

    public LogBookFilterPresenter(FilterLogBookActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getProvinceListAllSvc(String countryCode) {
        Call<ArrayList<ProvinceModel>> call = service.getAPI().getProvinceListAllSvc(countryCode);
        call.enqueue(new Callback<ArrayList<ProvinceModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ProvinceModel>> call, Response<ArrayList<ProvinceModel>> response) {
                view.showListProvinceResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<ProvinceModel>> call, Throwable t) {
                view.showListProvinceFailure(t);

            }


        });
    }

    @Override
    public void getCityListAllSvc(String countryCode, String provinceCode) {
        Call<ArrayList<CityModel>> call = service.getAPI().getCityListAllSvc(countryCode,provinceCode);
        call.enqueue(new Callback<ArrayList<CityModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CityModel>> call, Response<ArrayList<CityModel>> response) {
                view.showListCityResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<CityModel>> call, Throwable t) {
                view.showListCityFailure(t);

            }


        });
    }

}
