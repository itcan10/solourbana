package com.nataproperty.solourbana.view.commision.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.commision.intractor.PresenterCommisionScheduleInteractor;
import com.nataproperty.solourbana.view.commision.model.CommisionScheduleModel;
import com.nataproperty.solourbana.view.commision.ui.CommisionScheduleActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CommisionSchedulePresenter implements PresenterCommisionScheduleInteractor {
    private CommisionScheduleActivity view;
    private ServiceRetrofit service;

    public CommisionSchedulePresenter(CommisionScheduleActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetListCommissionCustProjectSvc(String dbMasterRef, String projectRef, String bookingRef, String memberRef) {
        Call<List<CommisionScheduleModel>> call = service.getAPI().GetListCommissionCustProjectSvc(dbMasterRef,projectRef,bookingRef,memberRef);
        call.enqueue(new Callback<List<CommisionScheduleModel>>() {
            @Override
            public void onResponse(Call<List<CommisionScheduleModel>> call, Response<List<CommisionScheduleModel>> response) {
                view.showListCommisionScheduleResults(response);
            }

            @Override
            public void onFailure(Call<List<CommisionScheduleModel>> call, Throwable t) {
                view.showListCommisionnScheduleFailure(t);

            }


        });
    }

}
