package com.nataproperty.solourbana.view.profile.ui;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

public class SettingNotificationActivity extends AppCompatActivity implements
        CompoundButton.OnCheckedChangeListener {
    public static final String PREF_NAME = "pref";
    Toolbar toolbar;
    TextView title;
    Typeface font;
    SwitchCompat switchCompatNata, switchCompatCobroke;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    boolean notificationNata, notificationCobroke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_notification);
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        notificationNata = sharedPreferences.getBoolean("isNotificationNata", true);
        notificationCobroke = sharedPreferences.getBoolean("isNotificationCobroke", true);

        switchCompatNata.setChecked(notificationNata);
        switchCompatCobroke.setChecked(notificationCobroke);

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Mute Notif");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        switchCompatNata = (SwitchCompat) findViewById(R.id
                .switch_compat_nata);
        switchCompatNata.setOnCheckedChangeListener(this);

        switchCompatCobroke = (SwitchCompat) findViewById(R.id
                .switch_compat_co_broke);
        switchCompatCobroke.setOnCheckedChangeListener(this);
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switch_compat_nata:
                Log.d("switch ", isChecked + "");
                sharedPreferences = SettingNotificationActivity.this.getSharedPreferences(PREF_NAME, 0);
                editor = sharedPreferences.edit();
                editor.putBoolean("isNotificationNata", isChecked);
                editor.commit();
                break;
            case R.id.switch_compat_co_broke:
                Log.d("switch ", isChecked + "c");
                sharedPreferences = SettingNotificationActivity.this.getSharedPreferences(PREF_NAME, 0);
                editor = sharedPreferences.edit();
                editor.putBoolean("isNotificationCobroke", isChecked);
                editor.commit();
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
