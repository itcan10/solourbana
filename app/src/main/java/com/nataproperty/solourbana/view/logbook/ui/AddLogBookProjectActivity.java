package com.nataproperty.solourbana.view.logbook.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.adapter.ListEventAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.ListReferralAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerCityAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerCountryAdapter;
import com.nataproperty.solourbana.view.logbook.adapter.SpinnerProvinceAdapter;
import com.nataproperty.solourbana.view.logbook.model.ContactProjectModel;
import com.nataproperty.solourbana.view.logbook.model.ListEvent;
import com.nataproperty.solourbana.view.logbook.model.ListReferral;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.logbook.presenter.AddLogBookProjectPresenter;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.CountryModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

public class AddLogBookProjectActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    private final int REQUEST_CODE=99;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private AddLogBookProjectPresenter presenter;
    Toolbar toolbar;
    TextView title;
    Typeface font;
    EditText edtName, edtHp1, edtHp2, edtPhone1, edtPhone2, edtEmail1, edtEmail2, edtAddress, edtPostCode;
    Spinner spnCoutry, spnProvince, spnCity, spnEvent, spnReferral;
    String name, hp1, hp2, phone1, phone2, email1 = "", email2, countryCode, provinceCode, cityCode, memberRef,
            address, postCode, eventRef, referralRef, dbMasterRef, projectRef, customerRef = "", customerStatus, customerName;
    Button btnBookPhone, btnSave;

    SpinnerCountryAdapter adapterCountry;
    SpinnerProvinceAdapter adapterProvince;
    SpinnerCityAdapter adapterCity;
    ListEventAdapter adapterEvent;
    ListReferralAdapter adapterReferral;
    ArrayList<CountryModel> listCounty = new ArrayList<CountryModel>();
    ArrayList<ProvinceModel> listProvince = new ArrayList<ProvinceModel>();
    ArrayList<CityModel> listCity = new ArrayList<CityModel>();
    ArrayList<ListEvent> listEvent = new ArrayList<ListEvent>();
    ArrayList<ListReferral> listReferral = new ArrayList<ListReferral>();

    SharedPreferences sharedPreferences;

    LinearLayout activityAddLogBook;

    ProgressDialog progressDialog;
    Intent intent;

    int setCountry = 0, setProvince = 0, setCity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_log_book_project);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new AddLogBookProjectPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);

        requestCountry();

        customerRef = intent.getStringExtra("customerRef");
        if (!customerRef.equals("")){
            addToProject();
        }

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.menu_log_book));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtHp1 = (EditText) findViewById(R.id.edt_hp1);
        edtHp2 = (EditText) findViewById(R.id.edt_hp2);
        edtPhone1 = (EditText) findViewById(R.id.edt_phone1);
        edtPhone2 = (EditText) findViewById(R.id.edt_phone2);
        edtEmail1 = (EditText) findViewById(R.id.edt_email1);
        edtEmail2 = (EditText) findViewById(R.id.edt_email2);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtPostCode = (EditText) findViewById(R.id.edt_post_code);
        spnCoutry = (Spinner) findViewById(R.id.spn_country);
        spnProvince = (Spinner) findViewById(R.id.spn_province);
        spnCity = (Spinner) findViewById(R.id.spn_city);
        spnEvent = (Spinner) findViewById(R.id.spn_event);
        spnReferral = (Spinner) findViewById(R.id.spn_referral);
        btnSave = (Button) findViewById(R.id.btn_save);
        activityAddLogBook = (LinearLayout) findViewById(R.id.activity_add_log_book);
        btnBookPhone = (Button) findViewById(R.id.btn_book_phone);
        btnBookPhone.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    private void addToProject() {
        customerStatus = intent.getStringExtra("customerStatus");
        customerName = intent.getStringExtra("customerName");
        countryCode = intent.getStringExtra("countryCode");
        provinceCode = intent.getStringExtra("provinceCode");
        cityCode = intent.getStringExtra("cityCode");
        postCode = intent.getStringExtra("postCode");
        hp1 = intent.getStringExtra("hp1");
        hp2 = intent.getStringExtra("hp2");
        phone1 = intent.getStringExtra("phone1");
        phone2 = intent.getStringExtra("phone2");
        email1 = intent.getStringExtra("email1");
        email2 = intent.getStringExtra("email2");

        edtName.setText(customerName);
        edtHp1.setText(hp1);
        edtHp2.setText(hp2);
        edtPhone1.setText(phone1);
        edtPhone2.setText(phone2);
        edtEmail1.setText(email1);
        edtEmail2.setText(email2);
        edtAddress.setText(address);
        edtPostCode.setText(postCode);

        edtName.setEnabled(false);
        edtHp1.setEnabled(false);
        edtHp2.setEnabled(false);
        edtPhone1.setEnabled(false);
        edtPhone2.setEnabled(false);
        edtEmail1.setEnabled(false);
        edtEmail2.setEnabled(false);
        edtAddress.setEnabled(false);
        edtPostCode.setEnabled(false);
        spnCoutry.setEnabled(false);
        spnProvince.setEnabled(false);
        spnCity.setEnabled(false);
    }

    private void requestCountry() {
        presenter.getCountryListSvc();
        presenter.GetLookupContactProjectSvc(dbMasterRef, projectRef, memberRef);
    }

    public void showListContactProjectResults(Response<ContactProjectModel> response) {
        listEvent = response.body().getListEvent();
        listReferral = response.body().getListReferral();
        initAdapter();
    }

    public void showListContactProjectFailure(Throwable t) {

    }

    private void initAdapter() {
        adapterEvent = new ListEventAdapter(this, listEvent);
        spnEvent.setAdapter(adapterEvent);
        adapterReferral = new ListReferralAdapter(this, listReferral);
        spnReferral.setAdapter(adapterReferral);
        spnEvent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                eventRef = listEvent.get(position).getEventRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnReferral.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                referralRef = listReferral.get(position).getReferralRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showListCountryResults(Response<ArrayList<CountryModel>> response) {
        listCounty = response.body();
        initSpinnerCountry();

        if (!customerRef.equals("")) {
            for (int i = 0; i < listCounty.size(); i++) {
                try {
                    String countyCodeSet = listCounty.get(i).getCountryCode();
                    if (countryCode.equals(countyCodeSet)) {
                        setCountry = i;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            spnCoutry.setSelection(setCountry);
        }
    }

    public void showListCountryFailure(Throwable t) {

    }

    private void initSpinnerCountry() {
        adapterCountry = new SpinnerCountryAdapter(this, listCounty);
        spnCoutry.setAdapter(adapterCountry);
        spnCoutry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode = listCounty.get(position).getCountryCode();
                requestProvince(countryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void requestProvince(String countryCode) {
        presenter.getProvinceListSvc(countryCode);
    }

    public void showListProvinceResults(Response<ArrayList<ProvinceModel>> response) {
        listProvince = response.body();
        initSpinnerProvince();

        if (!customerRef.equals("")) {
            for (int i = 0; i < listProvince.size(); i++) {
                try {
                    String provinceCodeSet = listProvince.get(i).getProvinceCode();
                    if (provinceCode.equals(provinceCodeSet)) {
                        setProvince = i;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            spnProvince.setSelection(setProvince);
        }
    }

    public void showListProvinceFailure(Throwable t) {

    }

    private void initSpinnerProvince() {
        adapterProvince = new SpinnerProvinceAdapter(this, listProvince);
        spnProvince.setAdapter(adapterProvince);
        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //countryCode = listCounty.get(position).getCountryCode();
                provinceCode = listProvince.get(position).getProvinceCode();
                requestCity(countryCode, provinceCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void requestCity(String countryCode, String provinceCode) {
        presenter.getCityListSvc(countryCode, provinceCode);
    }

    public void showListCityResults(Response<ArrayList<CityModel>> response) {
        listCity = response.body();
        initSpinnerCity();

        if (!customerRef.equals("")) {
            for (int i = 0; i < listCity.size(); i++) {
                try {
                    String cityCodeSet = listCity.get(i).getCityCode();
                    if (cityCode.equals(cityCodeSet)) {
                        setCity = i;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            spnCity.setSelection(setCity);
        }
    }

    public void showListCityFailure(Throwable t) {

    }

    private void initSpinnerCity() {
        adapterCity = new SpinnerCityAdapter(this, listCity);
        spnCity.setAdapter(adapterCity);
        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //countryCode = listCounty.get(position).getCountryCode();
                //provinceCode = listProvince.get(position).getProvinceCode();
                cityCode = listCity.get(position).getCityCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getContactPhone() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddLogBookProjectActivity.this);
            alertDialogBuilder.setMessage("Apakah anda ingin mengambil dari phone book?");
            alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, REQUEST_CODE);
                }
            });
            alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                getContactPhone();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
//                        String hasMime = c.getString(c.getColumnIndex(ContactsContract.Data.MIMETYPE));
                        String num = "", email = "", addressData = "", zipCode = "";
                        if (Integer.valueOf(hasNumber) == 1) {
                            //      NAMA
                            edtName.setText(hasName);

                            //      PHONE NUMBER
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                if (numbers.getCount() > 1) {
                                    if (numbers.getPosition() == 0) edtHp1.append(num);
                                    if (numbers.getPosition() == 1) edtHp2.append(num);
                                    if (numbers.getPosition() == 2) edtPhone1.append(num);
                                    if (numbers.getPosition() == 3) edtPhone2.append(num);
                                } else {
                                    edtHp1.append(num);
                                }
                            }
                            numbers.close();

                            //      EMAIL
                            Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
                            while (emailCursor.moveToNext()) {
                                email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                if (emailCursor.getCount() > 1) {
                                    if (emailCursor.getPosition() == 0) edtEmail1.append(email);
                                    if (emailCursor.getPosition() == 1) edtEmail2.append(email);
                                } else {
                                    edtEmail1.append(email);
                                }
                            }
                            emailCursor.close();

                            //      ADDRESS
                            Cursor addressCursor = getContentResolver().query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null, ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = " + contactId, null, null);
                            while (addressCursor.moveToNext()) {
                                addressData = addressCursor.getString(addressCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                                zipCode = addressCursor.getString(addressCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
                                edtAddress.append(addressData);
                                edtPostCode.append(zipCode);
                            }
                            addressCursor.close();
                        }
                    }
                    break;
                }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                AlertDialog alertDialog = new AlertDialog.Builder(AddLogBookProjectActivity.this).create();
                alertDialog.setTitle("Are you sure to save data?");
                alertDialog.setMessage("Data yang sudah tersimpan tidak dapat di edit kembali.");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                insertContactBankSvc();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;

            case R.id.btn_book_phone:
                getContactPhone();
                break;
        }
    }

    private void insertContactBankSvc() {
        name = edtName.getText().toString();
        address = edtAddress.getText().toString();
        postCode = edtPostCode.getText().toString();
        hp1 = edtHp1.getText().toString();
        hp2 = edtHp2.getText().toString();
        phone1 = edtPhone1.getText().toString();
        phone2 = edtPhone2.getText().toString();
        email1 = edtEmail1.getText().toString();
        email2 = edtEmail2.getText().toString();

        if (name.equals("") || hp1.equals("") || email1.equals("") || (!android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches() && !email1.equals(""))
                || (!android.util.Patterns.EMAIL_ADDRESS.matcher(email2).matches() && !email2.equals(""))) {
            Log.d("param",name+" "+hp1+" "+email1+" "+email2);
            Snackbar snackbar = Snackbar.make(activityAddLogBook, "Silakan lengkapi form.", Snackbar.LENGTH_SHORT);
            snackbar.show();

            if (name.trim().equals("")) {
                edtName.setError(getString(R.string.isEmpty));
            } else {
                edtName.setError(null);
            }

            if (hp1.trim().equals("")) {
                edtHp1.setError(getString(R.string.isEmpty));
            } else {
                edtHp1.setError(null);
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches() && !email1.equals("")) {
                edtEmail1.setError("Email tidak sesuai");
            }else if (email1.trim().equals("")) {
                edtEmail1.setError(getString(R.string.isEmpty));
            }
            else {
                edtEmail1.setError(null);
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email2).matches() && !email2.equals("")) {
                edtEmail2.setError("Email tidak sesuai");
            } else {
                edtEmail2.setError(null);
            }

        } else {
            progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
            Map<String, String> fields = new HashMap<>();
            fields.put("dbMasterRef", dbMasterRef);
            fields.put("projectRef", projectRef);
            fields.put("memberRef", memberRef);
            fields.put("customerStatus", General.customerStatusVerifyRef);
            fields.put("customerName", name);
            fields.put("countryCode", countryCode);
            fields.put("provinceCode", provinceCode);
            fields.put("cityCode", cityCode);
            fields.put("address", address);
            fields.put("postCode", postCode);
            fields.put("hp1", hp1);
            fields.put("hp2", hp2);
            fields.put("phone1", phone1);
            fields.put("phone2", phone2);
            fields.put("email1", email1);
            fields.put("email2", email2);
            fields.put("projectCustomerStatus", General.projectCustomerStatus);
            fields.put("eventRef", eventRef);
            fields.put("referralRef", referralRef);
            fields.put("customerRef", customerRef);
            presenter.insertContactBankProjectSvc(fields);
        }
    }

    public void showResponeResults(Response<ResponeModel> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            if (status == 200) {
                finish();
            } else {
                Snackbar snackbar = Snackbar.make(activityAddLogBook, message, Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        } else {
            Snackbar snackbar = Snackbar.make(activityAddLogBook, "Save failed", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

    }

    public void showResponeFailure(Throwable t) {
        progressDialog.dismiss();
        Snackbar snackbar = Snackbar.make(activityAddLogBook, "Terjadi Kesalahan", Snackbar.LENGTH_SHORT);
        snackbar.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
