package com.nataproperty.solourbana.view.mynup.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyNupDetailProjectPsInteractor {
    void getListNupDetailProjectPs(String dbMasterRef, String projectRef, String projectPsRef, String NUPRef, String memberRef, String projectSchemeRef);

}
