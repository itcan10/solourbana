package com.nataproperty.solourbana.view.event.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.event.model.MyTicketDetailModel;

import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by UserModel on 5/11/2016.
 */
public class MyTicketDetailImageAdapter extends PagerAdapter {
    public static final String TAG = "NewsDetailImageAdapter";

    Context context;
    private List<MyTicketDetailModel> list;
    PhotoViewAttacher attacher;
    String eventScheduleDate;

    public MyTicketDetailImageAdapter(Context context, List<MyTicketDetailModel> list, String eventScheduleDate){
        this.context = context;
        this.list = list;
        this.eventScheduleDate = eventScheduleDate;
    }

    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_my_ticket_detail_image, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.image_news_detail_image);
        TextView txtGuestName = (TextView) viewItem.findViewById(R.id.txt_guest_name);
        TextView txtDate = (TextView) viewItem.findViewById(R.id.txt_date);

        MyTicketDetailModel image = list.get(position);

        Glide.with(context).load(image.getLinkCode()).into(imageView);
        txtGuestName.setText(image.getGuestName());
        txtDate.setText(eventScheduleDate);

        //zoom
        /*attacher = new PhotoViewAttacher(imageView);
        attacher.update();*/

        Log.d(TAG,""+image.getLinkCode());

        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }


    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}
