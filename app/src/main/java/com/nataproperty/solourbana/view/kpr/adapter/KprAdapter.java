package com.nataproperty.solourbana.view.kpr.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.kpr.model.KprModel;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class KprAdapter extends BaseAdapter  {
    public static final String TAG = "KprAdapter" ;

    private Context context;
    private ArrayList<KprModel> list;

    private ListKprHolder holder;

    public KprAdapter(Context context, ArrayList<KprModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_kpr,null);
            holder = new ListKprHolder();
            holder.tahunKpr = (TextView) convertView.findViewById(R.id.txt_tahun_kpr);
            holder.bunga = (TextView) convertView.findViewById(R.id.txt_bunga);

            convertView.setTag(holder);
        }else{
            holder = (ListKprHolder) convertView.getTag();
        }

        KprModel kprModel = list.get(position);
        holder.tahunKpr.setText("Tahun KPR #"+(position+1)+" "+kprModel.getBunga()+" % "+"selama "+kprModel.getTahunKpr()+" tahun");

        return convertView;
    }


    private class ListKprHolder {
        TextView tahunKpr,bunga;
    }

}
