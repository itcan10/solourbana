package com.nataproperty.solourbana.view.report.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.report.model.ReportModel;
import com.nataproperty.solourbana.view.report.model.ResponGenerateReportModel;
import com.nataproperty.solourbana.view.report.presenter.ReportGenerateProjectPresenter;
import com.nataproperty.solourbana.view.report.ui.ReportProjectActivity;

import java.util.ArrayList;

import retrofit2.Response;

import static com.nataproperty.solourbana.R.id.report;

/**
 * Created by UserModel on 4/19/2016.
 */
public class ReportProjectAdapter extends BaseAdapter {
    public static final String TAG = "NotificationAdapter";
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private ReportGenerateProjectPresenter presenter;

    private Context context;
    private ArrayList<ReportModel> list;
    private ListNotificationHolder holder;
    private String userRef, dbMasterRef, projectRef, memberRef, link;
    SharedPreferences sharedPreferences;

    public ReportProjectAdapter(Context context, ArrayList<ReportModel> list,
                                String userRef, String dbMasterRef, String projectRef) {
        this.context = context;
        this.list = list;
        this.userRef = userRef;
        this.dbMasterRef = dbMasterRef;
        this.projectRef = projectRef;
        service = ((BaseApplication) context.getApplicationContext()).getNetworkServiceProject();
        presenter = new ReportGenerateProjectPresenter(this, service);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_report, null);
            holder = new ListNotificationHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.report = (ImageView) convertView.findViewById(report);
            holder.openWebview = (ImageView) convertView.findViewById(R.id.open_webview);
            convertView.setTag(holder);
        } else {
            holder = (ListNotificationHolder) convertView.getTag();
        }

        final ReportModel report = list.get(position);
        holder.name.setText(report.getName());
        holder.openWebview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.GenerateDashboardReportSvc("", dbMasterRef, projectRef, memberRef, userRef,report.getAspx());
            }
        });

        /*holder.openWebview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReportDetailActivity.class);
                intent.putExtra("userRef", userRef);
                intent.putExtra("dbMasterRef", dbMasterRef);
                intent.putExtra("projectRef", projectRef);
                context.startActivity(intent);
            }
        });*/


        return convertView;
    }


    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void showGenerateResults(Response<ResponGenerateReportModel> response) {
        if (response.isSuccessful()){
            link = response.body().getUrlReport();
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(link));
            context.startActivity(i);
        } else {
            ((ReportProjectActivity)context).showSnakBar("Terjadi kesalahan, coba lagi");
        }

    }

    public void showGenerateFailure(Throwable t) {
        ((ReportProjectActivity)context).showSnakBar("Tidak ada koneksi, coba lagi");
    }

    private class ListNotificationHolder {
        TextView name;
        ImageView report, openWebview;
    }

}
