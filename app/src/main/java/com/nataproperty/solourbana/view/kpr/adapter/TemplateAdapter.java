package com.nataproperty.solourbana.view.kpr.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.view.kpr.model.TemplateModel;
import com.nataproperty.solourbana.view.kpr.ui.CalculationIlustasiActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by UserModel on 4/19/2016.
 */
public class TemplateAdapter extends BaseAdapter {
    public static final String TAG = "TemplateAdapter";

    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;
    String memberRef;

    private Context context;
    private List<TemplateModel> list;

    private ListTemplateHolder holder;

    TemplateModel templateModel;
    String installmentType;

    public TemplateAdapter(Context context, List<TemplateModel> list) {
        this.context = context;
        this.list = list;

    }

    String calcUserProjectRef,calcPayTermRef,numOfInst;
    String project, cluster, block, product, unit, price, type, nilaiKpr,payTermName,dpType,bookingFeeDate,
            daysOfInst,bookingFee, listDP, KPRYear;

    String stringListDp;
    String stringListKpr;
    public ArrayList<String> dpList = new ArrayList<String>();
    public ArrayList<String> cicilList = new ArrayList<String>();
    public ArrayList<String> hargaList = new ArrayList<String>();

    public ArrayList<String> tahunKprList = new ArrayList<String>();
    public ArrayList<String> bungaList = new ArrayList<String>();


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_template, null);
            holder = new ListTemplateHolder();
            holder.title = (TextView) convertView.findViewById(R.id.txt_project_name);
            holder.cluster = (TextView) convertView.findViewById(R.id.txt_cluster);
            //  holder.product = (TextView) convertView.findViewById(R.id.txt_product);
            holder.type = (TextView) convertView.findViewById(R.id.txt_type);
            holder.rubish = (ImageView) convertView.findViewById(R.id.btn_rubish);
            holder.ilustrasi = (ImageView) convertView.findViewById(R.id.btn_illustrasi);

            convertView.setTag(holder);
        } else {
            holder = (ListTemplateHolder) convertView.getTag();
        }

        templateModel = list.get(position);

        if (templateModel.getInstallmentType().equals("1")) {
            installmentType = "Cicilan";
        } else {
            installmentType = "KPR/KPA";
        }

        if (templateModel.getCluster().equals("") && templateModel.getProduct().equals("")) {
            cluster = "";
        } else {
            cluster = "[ " + templateModel.getCluster() + " " + templateModel.getProduct() + " ]";
        }

        holder.title.setText(templateModel.getPayTermName());
        holder.cluster.setText(cluster);
        //holder.product.setText(templateModel.getProduct());
        holder.type.setText(installmentType);

        holder.rubish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcUserProjectRef = list.get(position).getCalcUserProjectRef();
                calcPayTermRef = list.get(position).getCalcPayTermRef();

                AlertDialog.Builder alertDialogBuilder =
                        new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Apakah anda yakin menghapus template?");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteTemplate(calcUserProjectRef,calcPayTermRef);
                        list.remove(position);
                        notifyDataSetChanged();
                        notifyDataSetInvalidated();

                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        holder.ilustrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcPayTermRef = list.get(position).getCalcPayTermRef();
                calcUserProjectRef = list.get(position).getCalcUserProjectRef();

                requestTemplateInfo();

            }
        });

        return convertView;
    }


    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListTemplateHolder {
        TextView title, cluster, product, type;
        ImageView rubish,ilustrasi;
    }

    public void deleteTemplate(final String calcUserProjectRef, final String calcPayTermRef) {
//        BaseApplication.getInstance().startLoader(context);
        LoadingBar.startLoader(context);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteTemplateCalcUserProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    if (status == 200) {
                        notifyDataSetChanged();
                    }

                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(context, context.getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context, context.getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("calcUserProjectRef", calcUserProjectRef.toString());
                params.put("calcPayTermRef", calcPayTermRef.toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "delete");

    }

    public void requestTemplateInfo() {
//        BaseApplication.getInstance().startLoader(context);
        LoadingBar.startLoader(context);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getInfoPayment(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, "user: " + response);

                    String project,block,unit,price,payTermName,dpType,bookingFeeDate,bookingFee,daysOfInst,stringListKpr;
                    String dataDP;

                    project = jsonObject.getString("project");
                    cluster = jsonObject.getString("cluster");
                    product = jsonObject.getString("product");
                    block = jsonObject.getString("block");
                    unit = jsonObject.getString("unit");
                    price = jsonObject.getString("price");

                    calcPayTermRef = jsonObject.getString("calcPayTermRef");
                    calcUserProjectRef = jsonObject.getString("calcUserProjectRef");
                    payTermName = jsonObject.getString("payTermName");
                    installmentType = jsonObject.getString("installmentType");
                    dpType = jsonObject.getString("dpType");
                    bookingFeeDate = jsonObject.getString("bookingFeeDate");
                    bookingFee = jsonObject.getString("bookingFee");
                    dataDP = jsonObject.getString("dpString").toString();
                    stringListKpr = jsonObject.getString("kprString").toString();

                    numOfInst = jsonObject.getString("numOfInst");
                    daysOfInst = jsonObject.getString("daysOfInst");

                    Log.d("Z-stringListKpr:",""+stringListKpr);
                    Log.d("Z-dataDP:",""+dataDP);
                    Log.d("Z-installmentType:",""+installmentType+"-"+installmentType);
                    if (installmentType.equals("1")) {
                        stringListKpr = "";
                        Intent intent = new Intent(context, CalculationIlustasiActivity.class);
                        intent.putExtra("calcUserProjectRef", calcUserProjectRef.toString());
                        intent.putExtra("payTermName", payTermName);
                        intent.putExtra("installmentType", String.valueOf(Integer.parseInt(installmentType)));
                        intent.putExtra("DPType", dpType.toString());
                        intent.putExtra("bookingFeeDate", bookingFeeDate.toString());
                        intent.putExtra("bookingFee", bookingFee.toString());
                        intent.putExtra("numOfInst", numOfInst.toString());
                        intent.putExtra("daysOfInst", daysOfInst.toString());
                        intent.putExtra("listDP", dataDP.toString());
                        intent.putExtra("KPRYear", stringListKpr.toString());

                        intent.putExtra("project", project.toString());
                        intent.putExtra("cluster", cluster.toString());
                        intent.putExtra("product", product.toString());
                        intent.putExtra("block", block.toString());
                        intent.putExtra("unit", unit.toString());
                        intent.putExtra("price", price.toString());
                        context.startActivity(intent);
                    }else{
                        Intent intent = new Intent(context, CalculationIlustasiActivity.class);
                        intent.putExtra("calcUserProjectRef", calcUserProjectRef.toString());
                        intent.putExtra("payTermName", payTermName);
                        intent.putExtra("installmentType", String.valueOf(Integer.parseInt(installmentType)));
                        intent.putExtra("DPType", dpType.toString());
                        intent.putExtra("bookingFeeDate", bookingFeeDate.toString());
                        intent.putExtra("bookingFee", bookingFee.toString());
                        intent.putExtra("numOfInst", numOfInst.toString());
                        intent.putExtra("daysOfInst", daysOfInst.toString());
                        intent.putExtra("listDP", dataDP.toString());
                        intent.putExtra("KPRYear", stringListKpr.toString());

                        intent.putExtra("project", project.toString());
                        intent.putExtra("cluster", cluster.toString());
                        intent.putExtra("product", product.toString());
                        intent.putExtra("block", block.toString());
                        intent.putExtra("unit", unit.toString());
                        intent.putExtra("price", price.toString());
                        context.startActivity(intent);
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(context, "error connection", Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("calcPayTermRef", calcPayTermRef.toString());
                params.put("calcUserProjectRef", calcUserProjectRef.toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "calCaraBayarInfo");

    }

}
