package com.nataproperty.solourbana.view.profile.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jaredrummler.android.device.DeviceName;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by UserModel on 4/22/2016.
 */
public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = "EditProfileActivity";

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    public static final String PREF_NAME = "pref";
    public static final String KEY_MEMBERREF = "memberRef";
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private EditText editFullname, editKtpId, editBirthPlace, editMobile1, editMobile2,
            editEmail1, editEmail2, editBirthdate, editNpwp;
    private TextView btnDeleteKtp, btnDeleteNpwp;
    private ImageView imgKtp, imgNpwp;
    private CircleImageView imgProfile;
    private Button btnSave;
    SharedPreferences sharedPreferences;
    String memberRef, email, fullname, ktpid, birthPlace, mobile1, mobile2, email1, email2, birthdate, npwp, ktpRef, npwpRef,
            type, manufacturer, mTmpGalleryPicturePath;
    Uri file, uri, selectedImage;
    static EditProfileActivity editProfileActivity;
    Toolbar toolbar;
    Typeface font;
    TextView title;
    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        email = sharedPreferences.getString("isEmail", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        email = sharedPreferences.getString("isEmail", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        requestPsInfo(memberRef);
        Glide.with(EditProfileActivity.this)
                .load(WebService.getProfile() + memberRef)
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(imgProfile);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_profile));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        editProfileActivity = this;
        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editKtpId = (EditText) findViewById(R.id.edit_ktpid);
        editBirthPlace = (EditText) findViewById(R.id.edit_birthplace);
        editMobile1 = (EditText) findViewById(R.id.edit_mobile1);
        editMobile2 = (EditText) findViewById(R.id.edit_mobile2);
        editEmail1 = (EditText) findViewById(R.id.edit_email1);
        editEmail2 = (EditText) findViewById(R.id.edit_email2);
        editBirthdate = (EditText) findViewById(R.id.edit_birthdate);
        editNpwp = (EditText) findViewById(R.id.edit_npwp);

        imgProfile = (CircleImageView) findViewById(R.id.profileImage);
        imgKtp = (ImageView) findViewById(R.id.img_ktp);
        imgNpwp = (ImageView) findViewById(R.id.img_npwp);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDeleteKtp = (TextView) findViewById(R.id.btn_delete_ktp);
        btnDeleteNpwp = (TextView) findViewById(R.id.btn_delete_npwp);

        btnSave.setTypeface(font);
        imgProfile.setOnClickListener(this);
        imgKtp.setOnClickListener(this);
        imgNpwp.setOnClickListener(this);
        editBirthdate.setOnClickListener(this);
        btnDeleteKtp.setOnClickListener(this);
        btnDeleteNpwp.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        editBirthdate.setInputType(InputType.TYPE_NULL);
        editBirthdate.setTextIsSelectable(true);
        editBirthdate.setFocusable(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profileImage:
                type = "1";
                selectImage();
                break;
            case R.id.img_ktp:
                type = "2";
                selectImage();
                break;
            case R.id.img_npwp:
                type = "3";
                selectImage();
                break;
            case R.id.edit_birthdate:
                dialogPicker();
                break;
            case R.id.btn_delete_ktp:
                AlertDialog.Builder alertDialogBuilderKtp = new AlertDialog.Builder(EditProfileActivity.this);
                alertDialogBuilderKtp.setMessage(R.string.deleteKtp);
                alertDialogBuilderKtp.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteKtp();
                    }
                });
                alertDialogBuilderKtp.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialogKtp = alertDialogBuilderKtp.create();
                alertDialogKtp.show();
                break;
            case R.id.btn_delete_npwp:
                AlertDialog.Builder alertDialogBuilderNpwp = new AlertDialog.Builder(EditProfileActivity.this);
                alertDialogBuilderNpwp.setMessage(R.string.deleteNpwp);
                alertDialogBuilderNpwp.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteNpwp();
                    }
                });
                alertDialogBuilderNpwp.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilderNpwp.create();
                alertDialog.show();
                break;
            case R.id.btnSave:
                String birthdate = editBirthdate.getText().toString();
                String fullname = editFullname.getText().toString();
                String email1 = editEmail1.getText().toString();

                if (!birthdate.isEmpty() && !fullname.isEmpty() && !email1.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                    updateProfile();
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } else {
                    if (birthdate.isEmpty()) {
                        editBirthdate.setError("enter a birthdate");
                        editBirthdate.setFocusableInTouchMode(true);
                        editBirthdate.requestFocus();
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } else {
                        editBirthdate.setError(null);
                    }

                    if (fullname.isEmpty()) {
                        editFullname.setError("entar a fullname");
                        editFullname.setFocusableInTouchMode(true);
                        editFullname.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editFullname, InputMethodManager.SHOW_IMPLICIT);
                    } else {
                        editFullname.setError(null);
                    }

                    if (email1.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {
                        editEmail1.setError("enter a valid email");
                        editEmail1.setFocusableInTouchMode(true);
                        editEmail1.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editEmail1, InputMethodManager.SHOW_IMPLICIT);
                    } else {
                        editEmail1.setError(null);
                    }

                }
                break;
        }
    }

    private void dialogPicker() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        new DatePickerDialog(EditProfileActivity.this, R.style.DatePickerDialogTheme, date,
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editBirthdate.setText(sdf.format(myCalendar.getTime()));
    }

    public void updateProfile() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.profileReuired(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result", response);
                    int status = jo.getInt("status");
                    if (status == 200) {
                        finish();
                        SharedPreferences sharedPreferences = EditProfileActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isName", editFullname.getText().toString());
                        editor.commit();
                    } else if (status == 201) {

                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("fullname", editFullname.getText().toString());
                params.put("KTPID", editKtpId.getText().toString().trim());
                params.put("birthPlace", editBirthPlace.getText().toString());
                params.put("birthDate", editBirthdate.getText().toString());
                params.put("mobile1", editMobile1.getText().toString().trim());
                params.put("mobile2", editMobile2.getText().toString().trim());
                params.put("email1", editEmail1.getText().toString().trim());
                params.put("email2", editEmail2.getText().toString().trim());
                params.put("NPWP", editNpwp.getText().toString().trim());
                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "profileReuired");

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(EditProfileActivity.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkCameraPermission()) {
                        Toast.makeText(EditProfileActivity.this, "Enable camera permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                    } else {
                        openIntentCamera();
                    }

                } else if (items[item].equals("Use Existing Foto")) {
                    if (checkWriteExternalPermission()) {
                        openIntentFile();
                        /*Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image*//*");
                        intent.putExtra("type", type);
                        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                        startActivityForResult(intent, SELECT_FILE);*/
                    } else {
                        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);
                    }


                }
            }
        });
        builder.show();

    }

    public boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = EditProfileActivity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void showDialogCekPermission() {
        if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    EXTERNAL_STORAGE_PERMISSION_CONSTANT);
//            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == SELECT_FILE) {
                openIntentFile();
            } else if (requestCode == REQUEST_CAMERA) {
                openIntentCamera();
            }
        } else {
            proceedAfterPermissionDeny();
        }
    }


    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void openIntentFile() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void openIntentCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("picUri", file);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        file = savedInstanceState.getParcelable("picUri");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                //onSelectFromGalleryResult(data);
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                } else {
                    handleGalleryResult19(data);
                }

            else if (requestCode == REQUEST_CAMERA) {
                if (type != null && type.equals("1")) {
                    Intent i = new Intent(this, EditProfileImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else if (type != null && type.equals("2")) {
                    Intent i = new Intent(this, EditKtpImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else if (type != null && type.equals("3")) {
                    Intent i = new Intent(this, EditNpwpImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else {
                    Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }
            }
            //onCaptureImageResult(data);

        }
    }


    private void handleGalleryResult19(Intent data) {
        uri = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                manufacturer = info.manufacturer;

                mTmpGalleryPicturePath = getRealPathFromURI(EditProfileActivity.this, uri);
                if (mTmpGalleryPicturePath == null) {
                    mTmpGalleryPicturePath = getPath(selectedImage);
                }

                Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                if (!mTmpGalleryPicturePath.equals("")) {
                    if (mTmpGalleryPicturePath != null) {
                        if (type != null && type.equals("1")) {
                            Intent i = new Intent(EditProfileActivity.this, EditProfileImageActivity.class);
                            i.putExtra("pathImage", mTmpGalleryPicturePath);
                            startActivity(i);
                        } else if (type != null && type.equals("2")) {
                            Intent i = new Intent(EditProfileActivity.this, EditKtpImageActivity.class);
                            i.putExtra("pathImage", mTmpGalleryPicturePath);
                            startActivity(i);
                        } else if (type != null && type.equals("3")) {
                            Intent i = new Intent(EditProfileActivity.this, EditNpwpImageActivity.class);
                            i.putExtra("pathImage", mTmpGalleryPicturePath);
                            startActivity(i);
                        } else {
                            Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        try {
                            InputStream is = getContentResolver().openInputStream(selectedImage);
                            //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                            mTmpGalleryPicturePath = selectedImage.getPath();
                            Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                            if (type != null && type.equals("1")) {
                                Intent i = new Intent(EditProfileActivity.this, EditProfileImageActivity.class);
                                i.putExtra("pathImage", mTmpGalleryPicturePath);
                                startActivity(i);
                            } else if (type != null && type.equals("2")) {
                                Intent i = new Intent(EditProfileActivity.this, EditKtpImageActivity.class);
                                i.putExtra("pathImage", mTmpGalleryPicturePath);
                                startActivity(i);
                            } else if (type != null && type.equals("3")) {
                                Intent i = new Intent(EditProfileActivity.this, EditNpwpImageActivity.class);
                                i.putExtra("pathImage", mTmpGalleryPicturePath);
                                startActivity(i);
                            } else {
                                Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("1")) {
                    Intent i = new Intent(this, EditProfileImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else if (type != null && type.equals("2")) {
                    Intent i = new Intent(this, EditKtpImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else if (type != null && type.equals("3")) {
                    Intent i = new Intent(this, EditNpwpImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else {
                    Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("1")) {
                        Intent i = new Intent(this, EditProfileImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else if (type != null && type.equals("2")) {
                        Intent i = new Intent(this, EditKtpImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else if (type != null && type.equals("3")) {
                        Intent i = new Intent(this, EditNpwpImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else {
                        Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(EditProfileActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }


    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    /**
     * request info member
     */
    private void requestPsInfo(final String memberRef) {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMemberInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                Log.d("cek", "response psInfo " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        fullname = jo.getJSONObject("data").getString("name");
                        ktpid = jo.getJSONObject("data").getString("ktpid");
                        birthPlace = jo.getJSONObject("data").getString("birthPlace");
                        mobile1 = jo.getJSONObject("data").getString("hP1");
                        mobile2 = jo.getJSONObject("data").getString("hP2");
                        email1 = jo.getJSONObject("data").getString("email1");
                        email2 = jo.getJSONObject("data").getString("email2");
                        birthdate = jo.getJSONObject("data").getString("birthDate");
                        npwp = jo.getJSONObject("data").getString("npwp");
                        ktpRef = jo.getJSONObject("data").getString("ktpRef");
                        npwpRef = jo.getJSONObject("data").getString("npwpRef");

                        //Log.d(TAG, ktpid + " " + birthPlace + " " + birthPlace + " " + mobile1 + " " + mobile2
                        //+ " " + email1 + " " + email2 + " " + birthdate);

                        SharedPreferences sharedPreferences = EditProfileActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isName", fullname);
                        editor.commit();

                        editFullname.setText(fullname);
                        editKtpId.setText(ktpid);
                        editBirthPlace.setText(birthPlace);
                        editMobile1.setText(mobile1);
                        editMobile2.setText(mobile2);
                        editEmail1.setText(email1);
                        editEmail2.setText(email2);
                        editBirthdate.setText(birthdate);
                        editNpwp.setText(npwp);

                        Glide.with(EditProfileActivity.this).load(WebService.getKtp() + ktpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgKtp);

                        Glide.with(EditProfileActivity.this).load(WebService.getNpwp() + npwpRef).diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true).into(imgNpwp);

                        if (ktpRef.equals("")) {
                            btnDeleteKtp.setVisibility(View.GONE);
                        } else {
                            btnDeleteKtp.setVisibility(View.VISIBLE);
                        }

                        if (npwpRef.equals("")) {
                            btnDeleteNpwp.setVisibility(View.GONE);
                        } else {
                            btnDeleteNpwp.setVisibility(View.VISIBLE);
                        }

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(getIntent());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        Toast.makeText(EditProfileActivity.this, "error connection", Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_MEMBERREF, memberRef);
                //Log.d("TAG Member Ref", memberRef);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "psInfo");
    }

    public void deleteKtp() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteKtp(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result ktpRef", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {

                        finish();
                        startActivity(getIntent());

                    } else {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("ktpRef", ktpRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteKtp");

    }

    public void deleteNpwp() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteNpwp(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result ktpRef", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {

                        finish();
                        startActivity(getIntent());

                    } else {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("npwpRef", npwpRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteNpwp");

    }

    public static EditProfileActivity getInstance() {
        return editProfileActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EditProfileActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
