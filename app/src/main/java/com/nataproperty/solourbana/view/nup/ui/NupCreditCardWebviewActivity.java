package com.nataproperty.solourbana.view.nup.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;

public class NupCreditCardWebviewActivity extends AppCompatActivity {
    public static final String TAG = "NupCreditCardWebview";
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String TOTAL = "total";
    private String nupOrderRef, projectRef, dbMasterRef, projectName, total, memberRef, link, titlePayment, paymentType = "";
    private SharedPreferences sharedPreferences;
    private WebView webView;
    private Toolbar toolbar;
    private TextView title;
    private Typeface font, fontLight;
    private ProgressBar progressBar;
    private boolean mylistNup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_credit_card_webview);
        intWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        total = intent.getStringExtra(TOTAL);
        link = intent.getStringExtra("link");
        titlePayment = intent.getStringExtra("titlePayment");
        paymentType = intent.getStringExtra("paymentType");
        mylistNup = intent.getBooleanExtra("mylistNup", false);
        Log.d(TAG, "paymentType " + paymentType);

        title.setText(titlePayment);
        initWebview();
    }

    private void intWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progress1);
    }

    private void initWebview() {
        webView.setWebViewClient(new myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100) {
                    progressBar.setVisibility(View.INVISIBLE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        webView.loadUrl(link);
        Log.d("lingWebView", "" + link);
        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void intentFinish() {
            Intent intent = new Intent(NupCreditCardWebviewActivity.this, NupFinishActivity.class);
            intent.putExtra(DBMASTER_REF, dbMasterRef);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(NUP_ORDER_REF, nupOrderRef);
            intent.putExtra("paymentType", paymentType);
            intent.putExtra("mylistNup",mylistNup);

            SharedPreferences sharedPreferences = NupCreditCardWebviewActivity.this.
                    getSharedPreferences(PREF_NAME, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("isMemberCostumerRef", "");
            editor.commit();

            startActivity(intent);
            finish();
        }

        @JavascriptInterface
        public void intentPaymentMethod() {
            Toast.makeText(mContext, "Failed to Pay", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
