package com.nataproperty.solourbana.view.profile.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface OthersEditInteractor {
    void getOtherProfileSvc();

    void updateOtherProfileSvc(Map<String, String> fields);

}
