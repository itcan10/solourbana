package com.nataproperty.solourbana.view.nup.model;

public class JsonInSaveOrderNupModel {

    private String projectCode;
    private String dbMasterRef;
    private String projectRef;
    private String agentMemberRef;
    private String NUPQty;
    private String surveyUnitTypeRef;
    private String surveyPaymentTermRef;
    private String surveyUnitViewRef;
    private String surveyUnitFloorRef;
    private String unitRefList;

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getAgentMemberRef() {
        return agentMemberRef;
    }

    public void setAgentMemberRef(String agentMemberRef) {
        this.agentMemberRef = agentMemberRef;
    }

    public String getNUPQty() {
        return NUPQty;
    }

    public void setNUPQty(String NUPQty) {
        this.NUPQty = NUPQty;
    }

    public String getSurveyUnitTypeRef() {
        return surveyUnitTypeRef;
    }

    public void setSurveyUnitTypeRef(String surveyUnitTypeRef) {
        this.surveyUnitTypeRef = surveyUnitTypeRef;
    }

    public String getSurveyPaymentTermRef() {
        return surveyPaymentTermRef;
    }

    public void setSurveyPaymentTermRef(String surveyPaymentTermRef) {
        this.surveyPaymentTermRef = surveyPaymentTermRef;
    }

    public String getSurveyUnitViewRef() {
        return surveyUnitViewRef;
    }

    public void setSurveyUnitViewRef(String surveyUnitViewRef) {
        this.surveyUnitViewRef = surveyUnitViewRef;
    }

    public String getSurveyUnitFloorRef() {
        return surveyUnitFloorRef;
    }

    public void setSurveyUnitFloorRef(String surveyUnitFloorRef) {
        this.surveyUnitFloorRef = surveyUnitFloorRef;
    }

    public String getUnitRefList() {
        return unitRefList;
    }

    public void setUnitRefList(String unitRefList) {
        this.unitRefList = unitRefList;
    }
}
