package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.view.nup.model.NupVirtualAccountModel;
import com.nataproperty.solourbana.R;

import java.util.ArrayList;

/**
 * Created by nata on 7/17/2018.
 */

public class AccountVirtualAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<? extends NupVirtualAccountModel> list;
    private ListVirtualAccountHolder holder;

    public AccountVirtualAdapter(Context context, ArrayList<? extends NupVirtualAccountModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list_virtual_account,null);
            holder = new ListVirtualAccountHolder();
            holder.txtVA = (TextView) convertView.findViewById(R.id.txt_number_va);
            convertView.setTag(holder);
        } else {
            holder = (ListVirtualAccountHolder) convertView.getTag();
        }

        NupVirtualAccountModel vaModel = list.get(position);
        holder.txtVA.setText(vaModel.getNoVA());

        return convertView;
    }


    private class ListVirtualAccountHolder {
        TextView txtVA;
    }
}
