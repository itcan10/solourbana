package com.nataproperty.solourbana.view.projectmenu.model;

/**
 * Created by Nata
 * on Mar 3/22/2017 14:41.
 * Project : wika
 */
public class ProjectVareable {
    long dbMasterRef;
    String  projectRef, linkDetail, urlVideo, downloadProjectInfo, categoryRef, clusterRef, isShowAvailableUnit,
            isNUP, isBooking, nupAmt, imageLogo, countCategory, countCluster, userRef, salesStatus, projectName;
    double latitude, longitude;

    private static ProjectVareable ourInstance = new ProjectVareable();

    public static ProjectVareable getInstance() {
        return ourInstance;
    }

    private ProjectVareable() {
    }

    public String getLinkDetail() {
        return linkDetail;
    }

    public void setLinkDetail(String linkDetail) {
        this.linkDetail = linkDetail;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getDownloadProjectInfo() {
        return downloadProjectInfo;
    }

    public void setDownloadProjectInfo(String downloadProjectInfo) {
        this.downloadProjectInfo = downloadProjectInfo;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static ProjectVareable getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(ProjectVareable ourInstance) {
        ProjectVareable.ourInstance = ourInstance;
    }

    public String getCategoryRef() {
        return categoryRef;
    }

    public void setCategoryRef(String categoryRef) {
        this.categoryRef = categoryRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getIsShowAvailableUnit() {
        return isShowAvailableUnit;
    }

    public void setIsShowAvailableUnit(String isShowAvailableUnit) {
        this.isShowAvailableUnit = isShowAvailableUnit;
    }

    public String getIsNUP() {
        return isNUP;
    }

    public void setIsNUP(String isNUP) {
        this.isNUP = isNUP;
    }

    public String getIsBooking() {
        return isBooking;
    }

    public void setIsBooking(String isBooking) {
        this.isBooking = isBooking;
    }

    public String getNupAmt() {
        return nupAmt;
    }

    public void setNupAmt(String nupAmt) {
        this.nupAmt = nupAmt;
    }

    public String getImageLogo() {
        return imageLogo;
    }

    public void setImageLogo(String imageLogo) {
        this.imageLogo = imageLogo;
    }

    public String getCountCategory() {
        return countCategory;
    }

    public void setCountCategory(String countCategory) {
        this.countCategory = countCategory;
    }

    public String getCountCluster() {
        return countCluster;
    }

    public void setCountCluster(String countCluster) {
        this.countCluster = countCluster;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }

    public String getSalesStatus() {
        return salesStatus;
    }

    public void setSalesStatus(String salesStatus) {
        this.salesStatus = salesStatus;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public long getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(long dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }
}
