package com.nataproperty.solourbana.view.mybooking.ui;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mybooking.adapter.MyBookingProjectPsAdapter;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingModel;
import com.nataproperty.solourbana.view.mybooking.presenter.MyBookingProjectPsPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by UserModel on 6/14/2016.
 */
public class MyBookingProjectPsActivity extends AppCompatActivity {
    public static final String TAG = "MyBookingProjectPs";

    public static final String PREF_NAME = "pref";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String BOOKING_REF = "bookingRef";
    public static final String MEMBER_REF = "memberRef";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private MyBookingProjectPsPresenter presenter;
//    ProgressDialog progressDialog;

    private List<MyBookingModel> listMyBooking = new ArrayList<>();
    private MyBookingProjectPsAdapter adapter;
    private MyListView listView;
    private SharedPreferences sharedPreferences;
    private String memberRef, dbMasterRef, projectRef, bookingRef, projectPsRef, memberCustomerRef;
    ProgressDialog progressDialog;
    String result, projectBookingRef, isPayment, customerName, mobile, phone, email, alamat, ktpId;
    LinearLayout itemListNoData;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyBookingProjectPsPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        projectPsRef = intent.getStringExtra("projectPsRef");

        Log.d(TAG, "param " + dbMasterRef + " " + projectRef + " " + projectPsRef);

        initWidget();

        requestMyBooking();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dbMasterRef = listMyBooking.get(position).getDbMasterRef();
                projectRef = listMyBooking.get(position).getProjectRef();
                bookingRef = listMyBooking.get(position).getBookingRef();
                memberCustomerRef = listMyBooking.get(position).getMemberCostumerRef();
                projectBookingRef = listMyBooking.get(position).getProjectBookingRef();
                isPayment = listMyBooking.get(position).getIsPayment();
                customerName = listMyBooking.get(position).getCustomerName();
                mobile = listMyBooking.get(position).getMobile();
                phone = listMyBooking.get(position).getPhone();
                email = listMyBooking.get(position).getEmail();
                alamat = listMyBooking.get(position).getAlamat();
                ktpId = listMyBooking.get(position).getKtpId();

                Intent intent = new Intent(MyBookingProjectPsActivity.this, MyBookingInfoProjectPsActivity.class);
                intent.putExtra(BOOKING_REF, bookingRef);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(PROJECT_BOOKING_REF, projectBookingRef);
                intent.putExtra("isPayment", isPayment);
                intent.putExtra("customerName", customerName);
                intent.putExtra("mobile", mobile);
                intent.putExtra("phone", phone);
                intent.putExtra("email", email);
                intent.putExtra("alamat", alamat);
                intent.putExtra("ktpId", ktpId);

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", memberCustomerRef);
                editor.commit();
                startActivity(intent);
            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        itemListNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        listView = (MyListView) findViewById(R.id.list_mybooking);
    }

    public void requestMyBooking() {

        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListBookingProjectPs(dbMasterRef, projectRef, projectPsRef);
    }

    public void showListBookingProjectPsResults(retrofit2.Response<List<MyBookingModel>> response) {
        progressDialog.dismiss();
        listMyBooking = response.body();
        initAdapter();
    }

    private void initAdapter() {
        adapter = new MyBookingProjectPsAdapter(MyBookingProjectPsActivity.this, listMyBooking);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showListBookingProjectPsFailure(Throwable t) {
        progressDialog.dismiss();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingProjectPsActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

}
