package com.nataproperty.solourbana.view.event.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterEventInteractor {
    void getListEventSvc(String contentDate, String memberRef, String projectCode);

}
