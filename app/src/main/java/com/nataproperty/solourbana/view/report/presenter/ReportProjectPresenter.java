package com.nataproperty.solourbana.view.report.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.report.intractor.ReportProjectInteractor;
import com.nataproperty.solourbana.view.report.model.ReportModel;
import com.nataproperty.solourbana.view.report.ui.ReportProjectActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ReportProjectPresenter implements ReportProjectInteractor {
    private ReportProjectActivity view;
    private ServiceRetrofitProject service;

    public ReportProjectPresenter(ReportProjectActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void GetListDashboardReport2(String memberRef,String dbMasterRef,String projectRef) {
        Call<ArrayList<ReportModel>> call = service.getAPI().GetListDashboardReport2(memberRef,dbMasterRef,projectRef);
        call.enqueue(new Callback<ArrayList<ReportModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ReportModel>> call, Response<ArrayList<ReportModel>> response) {
                view.showListReportResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<ReportModel>> call, Throwable t) {
                view.showListReportFailure(t);

            }


        });
    }
}
