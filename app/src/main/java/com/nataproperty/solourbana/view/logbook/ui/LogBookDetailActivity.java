package com.nataproperty.solourbana.view.logbook.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.NonScrollExpandableListView;
import com.nataproperty.solourbana.view.logbook.adapter.TaskAdapter;
import com.nataproperty.solourbana.view.logbook.model.TaskModel;
import com.nataproperty.solourbana.view.logbook.presenter.LogBookDetailPresenter;

import java.util.ArrayList;

import retrofit2.Response;

public class LogBookDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String EXTRA_RX = "EXTRA_RX";

    private ServiceRetrofitProject service;
    private boolean rxCallInWorks = false;
    private LogBookDetailPresenter presenter;

    TextView txtName, txtHp, txtEmail, txtAddress, txtProjectCustomerStatusName, txtEventName, txtReferralName;
    String name, hp, email, address, projectCustomerStatusName, eventName, referralName, province, city, dbMasterRef,
            projectRef, customerRef = "", color, projectCustomerStatus;
    Toolbar toolbar;
    TextView title, noData;
    Typeface font;
    FloatingActionButton fab;

    TaskAdapter adapter;
    ArrayList<TaskModel> list = new ArrayList<>();
    NonScrollExpandableListView expandableListView;

    LinearLayout itemList;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_book_detail);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new LogBookDetailPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        Intent i = getIntent();
        dbMasterRef = i.getStringExtra("dbMasterRef");
        projectRef = i.getStringExtra("projectRef");
        customerRef = i.getStringExtra("customerRef");
        name = i.getStringExtra("name");
        hp = i.getStringExtra("hp1");
        email = i.getStringExtra("email1");
        address = i.getStringExtra("address");
        city = i.getStringExtra("city");
        province = i.getStringExtra("province");
        projectCustomerStatusName = i.getStringExtra("projectCustomerStatusName");
        eventName = i.getStringExtra("eventName");
        referralName = i.getStringExtra("referralName");
        color = i.getStringExtra("color");
        projectCustomerStatus = i.getStringExtra("projectCustomerStatus");

        Log.d("customerRef", " " + customerRef);
        if (projectCustomerStatus.equals(General.projectCustomerStatusFinish))
            fab.setVisibility(View.GONE);

        setText();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        requestTask();
    }

    private void setText() {
        txtName.setText(name);
        txtHp.setText(hp);
        if (email.equals("")) {
            txtEmail.setText("-");
        } else {
            txtEmail.setText(email);
        }
        if (address.trim().equals("")) {
            txtAddress.setText(city + ", " + province);
        } else {
            txtAddress.setText(address + ", " + city + ", " + province);
        }
        txtProjectCustomerStatusName.setText(projectCustomerStatusName);
        txtEventName.setText(eventName);
        txtReferralName.setText(referralName);
        itemList.setBackgroundColor(Color.parseColor(color));
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.menu_log_book) + " Detail");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title.setTypeface(font);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtName = (TextView) findViewById(R.id.txt_name);
        txtHp = (TextView) findViewById(R.id.txt_hp);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtAddress = (TextView) findViewById(R.id.txt_address);
        txtProjectCustomerStatusName = (TextView) findViewById(R.id.txt_projectCustomerStatusName);
        txtEventName = (TextView) findViewById(R.id.txt_eventName);
        txtReferralName = (TextView) findViewById(R.id.txt_referralName);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
        expandableListView = (NonScrollExpandableListView) findViewById(R.id.list_task);
        noData = (TextView) findViewById(R.id.txt_no_data);
        itemList = (LinearLayout) findViewById(R.id.item_list);
    }

    private void requestTask() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetContactProjectTaskListSvc(dbMasterRef, projectRef, customerRef);
    }

    public void showListTaskResults(Response<ArrayList<TaskModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            list = response.body();
            if (list.size() > 0) {
                initAdapter();
                noData.setVisibility(View.GONE);
            } else {
                noData.setVisibility(View.VISIBLE);
                expandableListView.setEmptyView(noData);
            }

        }
    }

    public void showListTaskFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new TaskAdapter(this, list, dbMasterRef, projectRef, customerRef, projectCustomerStatus, name);
        expandableListView.setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent i = new Intent(LogBookDetailActivity.this, AddTaskActivity.class);
                i.putExtra("dbMasterRef", dbMasterRef);
                i.putExtra("projectRef", projectRef);
                i.putExtra("customerRef", customerRef);
                i.putExtra("projectCustomerStatus", projectCustomerStatus);
                i.putExtra("name", name);
                i.putExtra("logProjectCustomerStatusTaskRef", "");
                i.putExtra("parentLogProjectCustomerStatusTaskRef", "0");
                startActivity(i);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }


}
