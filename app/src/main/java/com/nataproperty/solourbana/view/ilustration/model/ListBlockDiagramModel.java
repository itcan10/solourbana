package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by nata on 7/9/2018.
 */

public class ListBlockDiagramModel {
    String blockName;
    String unitMaping;
    String productRefList;
    String unitStatusList;
    String unitRefList;
    String productNameList;
    String viewFloorPlan;
    String color ;
    String statusColor;
    String statusColorName;
    String viewFloorPlanUrl;

    public String getStatusColorName() {
        return statusColorName;
    }

    public void setStatusColorName(String statusColorName) {
        this.statusColorName = statusColorName;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public String getViewFloorPlan() {
        return viewFloorPlan;
    }

    public void setViewFloorPlan(String viewFloorPlan) {
        this.viewFloorPlan = viewFloorPlan;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getUnitMaping() {
        return unitMaping;
    }

    public void setUnitMaping(String unitMaping) {
        this.unitMaping = unitMaping;
    }

    public String getProductRefList() {
        return productRefList;
    }

    public void setProductRefList(String productRefList) {
        this.productRefList = productRefList;
    }

    public String getUnitStatusList() {
        return unitStatusList;
    }

    public void setUnitStatusList(String unitStatusList) {
        this.unitStatusList = unitStatusList;
    }

    public String getUnitRefList() {
        return unitRefList;
    }

    public void setUnitRefList(String unitRefList) {
        this.unitRefList = unitRefList;
    }

    public String getProductNameList() {
        return productNameList;
    }

    public void setProductNameList(String productNameList) {
        this.productNameList = productNameList;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getViewFloorPlanUrl() {
        return viewFloorPlanUrl;
    }

    public void setViewFloorPlanUrl(String viewFloorPlanUrl) {
        this.viewFloorPlanUrl = viewFloorPlanUrl;
    }
}
