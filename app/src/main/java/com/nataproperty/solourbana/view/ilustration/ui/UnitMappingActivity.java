package com.nataproperty.solourbana.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.SessionManager;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.MyGridView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.ilustration.adapter.ColorListAdapter;
import com.nataproperty.solourbana.view.ilustration.adapter.NewUnitMappingAdapter;
import com.nataproperty.solourbana.view.ilustration.model.ColorModel;
import com.nataproperty.solourbana.view.ilustration.model.MapingModelNew;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by UserModel on 5/30/2016.
 */
public class UnitMappingActivity extends AppCompatActivity {
    private List<MapingModelNew> listUnitMapping = new ArrayList<>();
    private NewUnitMappingAdapter adapterUnit;
    ProgressDialog progressDialog;
    ImageView imgLogo;
    TextView txtProjectName;
    MyGridView listUnit, listView;
    String projectRef, dbMasterRef, categoryRef, clusterRef, projectName, blockName, unitRef,
            productRef, unitStatus, isBooking, isShowAvailableUnit, unitMaping, unitRefList,
            color, statusColor, statusColorName, unitStatusList, productNameList, titleProduct, productRefList;
    String memberRef, color1, color2, viewFloor;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Point size;
    Display display;
    Integer width;
    Double result;
    SessionManager sessionManager;
    RecyclerView listColor;
    Button btnFloorPlan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_mapping);
        sessionManager = new SessionManager(this);

        initWidget();

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(General.DBMASTER_REF);
        projectRef = intent.getStringExtra(General.PROJECT_REF);
        categoryRef = intent.getStringExtra(General.CATEGORY_REF);
        clusterRef = intent.getStringExtra(General.CLUSTER_REF);
        projectName = intent.getStringExtra(General.PROJECT_NAME);
        blockName = intent.getStringExtra(General.BLOCK_NAME);
        isBooking = intent.getStringExtra(General.IS_BOOKING);
        productRef = intent.getStringExtra(General.PRODUCT_REF);
        isShowAvailableUnit = intent.getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);


        //intent berupa aray
        unitMaping = intent.getStringExtra(General.UNIT_MAPPING);
        unitRefList = intent.getStringExtra(General.UNIT_REF_LIST);
        color = intent.getStringExtra(General.COLOR);
        unitStatusList = intent.getStringExtra(General.UNIT_STATUS_LIST);
        productRefList = intent.getStringExtra(General.PRODUCT_REF_LIST);
        productNameList = intent.getStringExtra(General.PRODUCT_NAME_LIST);
        statusColor = intent.getStringExtra(General.STATUS_COLOR);
        statusColorName = intent.getStringExtra(General.STATUS_COLOR_NAME);
        titleProduct = intent.getStringExtra(General.TITLE_PRODUCT);

        viewFloor = intent.getStringExtra(General.VIEW_FLOOR_PLAN);

        memberRef = sessionManager.getStringFromSP(General.IS_MEMBER_REF);
        Log.d("cek unit", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef + " " + blockName);

        Log.d("TAG", "productRef " + productRef);

        txtProjectName.setText(projectName);

        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);
        //requestDiagramColor();
        initData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                unitRef = listUnitMapping.get(position).getUnitRef();
                productRef = listUnitMapping.get(position).getProductRef();
                unitStatus = listUnitMapping.get(position).getUnitStatus();

                if (unitStatus.equals("A")) {
                    Intent intent = new Intent(UnitMappingActivity.this, IlustrationPaymentTermActivity.class);
                    intent.putExtra(General.DBMASTER_REF, dbMasterRef);
                    intent.putExtra(General.PROJECT_REF, projectRef);
                    intent.putExtra(General.CLUSTER_REF, clusterRef);
                    intent.putExtra(General.CATEGORY_REF, categoryRef);
                    intent.putExtra(General.PROJECT_NAME, projectName);
                    intent.putExtra(General.PRODUCT_REF, productRef);
                    intent.putExtra(General.UNIT_REF, unitRef);
                    intent.putExtra(General.IS_BOOKING, isBooking);
                    startActivity(intent);

                } else if (unitStatus.equals("S")) {
                    Toast.makeText(UnitMappingActivity.this, getString(R.string.unit_sold).replace("@unit", listUnitMapping.get(position).getUnitName()),
                            Toast.LENGTH_SHORT).show();
                } else {
                    if (!unitStatus.trim().equals("")) {
                        Toast.makeText(UnitMappingActivity.this, getString(R.string.unit_not_available), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        if (!viewFloor.trim().equals("")) {
            btnFloorPlan.setVisibility(View.VISIBLE);
        }

        btnFloorPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UnitMappingActivity.this, FloorPlanActivity.class);
                intent.putExtra(General.VIEW_FLOOR_PLAN, viewFloor);
                startActivity(intent);
            }
        });
    }

    private void initData() {
        List<String> unitMapings = Arrays.asList(unitMaping.split(","));
        List<String> unitRefLists = Arrays.asList(unitRefList.split(","));
        List<String> unitStatusLists = Arrays.asList(unitStatusList.split(","));
        List<String> productNameLists = Arrays.asList(productNameList.split(","));
        List<String> productRefLists = Arrays.asList(productRefList.split(","));
        List<String> colors = Arrays.asList(color.split(","));

        List<String> statusColors = Arrays.asList(statusColor.split(","));
        List<String> statusColorNames = Arrays.asList(statusColorName.split(","));

        for (int x = 0; x < unitMapings.size() - 1; x++) {
            if (!productRef.equals("")) {
                if (!unitMapings.get(x).toString().trim().equals("")) {
                    MapingModelNew mm = new MapingModelNew();
                    Log.d("TAG", "productRef " + productRefLists.get(x));
                    if (productRef.equals(productRefLists.get(x))) {
                        mm.setColor(colors.get(x));
                        mm.setUnitName(unitMapings.get(x));
                        mm.setProductName(productNameLists.get(x));
                        mm.setUnitRef(unitRefLists.get(x));
                        mm.setUnitStatus(unitStatusLists.get(x));
                        mm.setProductRef(productRefLists.get(x));
                        listUnitMapping.add(mm);
                    }

                }

            } else {
                if (!unitMapings.get(x).toString().trim().equals("")) {
                    MapingModelNew mm = new MapingModelNew();
                    Log.d("TAG", "productRef " + productRefLists.get(x));
                    mm.setColor(colors.get(x));
                    mm.setUnitName(unitMapings.get(x));
                    mm.setProductName(productNameLists.get(x));
                    mm.setUnitRef(unitRefLists.get(x));
                    mm.setUnitStatus(unitStatusLists.get(x));
                    mm.setProductRef(productRefLists.get(x));
                    listUnitMapping.add(mm);

                }
            }
        }

        initAdapter();

        List<ColorModel> list = new ArrayList<>();
        for (int x = 0; x < statusColors.size(); x++) {
            if (!statusColors.get(x).toString().trim().toLowerCase().equals("#ffffff") &&
                    !statusColors.get(x).toString().trim().toLowerCase().equals("")) {
                ColorModel c = new ColorModel();
                c.setColor(statusColors.get(x));
                c.setColorName(statusColorNames.get(x));
                list.add(c);
            }
        }

//        if (!productRef.equals("")){
//            ColorModel c = new ColorModel();
//            c.setColor("#c7c7c7");
//            c.setColorName(titleProduct);
//            list.add(c);
//        }

        ColorListAdapter adapter = new ColorListAdapter(getApplicationContext(), list);
        listColor.setAdapter(adapter);
    }

    private void initAdapter() {
        adapterUnit = new NewUnitMappingAdapter(this, listUnitMapping);
        listView.setAdapter(adapterUnit);
        listView.setExpanded(true);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText(getResources().getString(R.string.title_illustrastion_unit_mapping));
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        listView = (MyGridView) findViewById(R.id.list_unit_mapping);

        listColor = (RecyclerView) findViewById(R.id.list_color);

        btnFloorPlan = (Button) findViewById(R.id.btn_floor_plan);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        listColor.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
