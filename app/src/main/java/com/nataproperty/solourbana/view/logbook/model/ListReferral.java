package com.nataproperty.solourbana.view.logbook.model;

/**
 * Created by Nata on 2/9/2017.
 */

public class ListReferral {
    String referralRef,referralName;

    public String getReferralRef() {
        return referralRef;
    }

    public void setReferralRef(String referralRef) {
        this.referralRef = referralRef;
    }

    public String getReferralName() {
        return referralName;
    }

    public void setReferralName(String referralName) {
        this.referralName = referralName;
    }
}
