package com.nataproperty.solourbana.view.mynup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.mynup.model.UnPaidNupModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class UnPaidDetailAdapter extends BaseAdapter {
    private Context context;
    private List<UnPaidNupModel> list;
    private ListUnPaidHolder holder;

    public UnPaidDetailAdapter(Context context, List<UnPaidNupModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_un_paid_detail,null);
            holder = new ListUnPaidHolder();
            holder.orderStatus = (TextView) convertView.findViewById(R.id.txt_order_status);
            holder.costumerName = (TextView) convertView.findViewById(R.id.txt_costumer_name);

            convertView.setTag(holder);
        }else{
            holder = (ListUnPaidHolder) convertView.getTag();
        }

        UnPaidNupModel unPaidDetail = list.get(position);

        holder.orderStatus.setText(unPaidDetail.getNupOrderStatus());
        holder.costumerName.setText(unPaidDetail.getCustomerName());

        Log.d("cek nup adapter",unPaidDetail.getNupOrderStatus());

        return convertView;
    }

    private class ListUnPaidHolder {
        TextView orderStatus,costumerName;
    }
}
