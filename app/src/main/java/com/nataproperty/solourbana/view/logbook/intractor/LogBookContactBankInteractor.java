package com.nataproperty.solourbana.view.logbook.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface LogBookContactBankInteractor {
    void GetContactBankTaskSvc(String memberRef,String dbMasterRef,String projectRef, String customerStatusRef, String keyword
            , String countryCode, String provinceCode, String cityCode);

}
