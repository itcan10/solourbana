package com.nataproperty.solourbana.view.ilustration.ui;

import android.util.Log;

import java.util.Iterator;
import java.util.List;

/**
 * Created by UserModel on 6/3/2016.
 */
public class SampleObject {

    String[] hdrStr;
    List<String> header;

    public SampleObject(List<String> hdr){


        this.header = hdr;
        Iterator<String> iter = header.iterator();
        hdrStr = new String[header.size()];
        int i =0;
        while( iter.hasNext() )
        {

            String str = ( String ) iter.next();

            this.hdrStr[i] = str;
            Log.d("hdr",""+this.hdrStr[i]);
            i = i+1;
        }
    }
}
