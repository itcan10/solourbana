package com.nataproperty.solourbana.view.mynup.model;

/**
 * Created by Nata on 1/3/2017.
 */

public class MyNupSectionModel {
    String sectionName,dbMasterRef,projectRef,nupCount,projectPsRef,projectSchemeRef;

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getNupCount() {
        return nupCount;
    }

    public void setNupCount(String nupCount) {
        this.nupCount = nupCount;
    }

    public String getProjectPsRef() {
        return projectPsRef;
    }

    public void setProjectPsRef(String projectPsRef) {
        this.projectPsRef = projectPsRef;
    }

    public String getProjectSchemeRef() {
        return projectSchemeRef;
    }

    public void setProjectSchemeRef(String projectSchemeRef) {
        this.projectSchemeRef = projectSchemeRef;
    }
}
