package com.nataproperty.solourbana.view.booking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterBookingFinishInteractor {

    void getIlustrationPaymentSvc(String dbMasterRef, String projectRef, String clusterRef, String productRef,
                                  String unitRef, String termRef, String termNo);

    void getMemberInfoSvc(String memberRef);


}
