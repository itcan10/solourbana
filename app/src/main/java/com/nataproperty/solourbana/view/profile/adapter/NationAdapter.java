package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.NationModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class NationAdapter extends BaseAdapter {
    private Context context;
    private List<NationModel> list;
    private ListNationHolder holder;

    public NationAdapter(Context context, List<NationModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_nation,null);
            holder = new ListNationHolder();
            holder.nationName = (TextView) convertView.findViewById(R.id.txt_nation);

            convertView.setTag(holder);
        }else{
            holder = (ListNationHolder) convertView.getTag();
        }
        NationModel nation = list.get(position);
        holder.nationName.setText(nation.getNationName());

        return convertView;
    }

    private class ListNationHolder {
        TextView nationName;
    }
}
