package com.nataproperty.solourbana.view.nup.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/18/2016.
 */
public class NupTermActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref" ;

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String PROJECT_NAME = "projectName";
    public static final String NUP_AMT = "nupAmt";

    ImageView imgTerm;
    TextView txtTerm,txtNupOnline;
    Button btnNext;
    CheckBox cbTerm;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    HtmlTextView text;

    SharedPreferences sharedPreferences;

    private String dbMasterRef,projectRef,projectName,termAndCondition,projectDescription,nupAmt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_trem);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_buy_nup));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        projectRef = getIntent().getStringExtra(PROJECT_REF);
        dbMasterRef = getIntent().getStringExtra(DBMASTER_REF);
        projectName = getIntent().getStringExtra(PROJECT_NAME);
        projectDescription = getIntent().getStringExtra(PROJECT_DESCRIPTION);
        nupAmt = getIntent().getStringExtra(NUP_AMT);

        imgTerm = (ImageView) findViewById(R.id.img_term);
        cbTerm =(CheckBox) findViewById(R.id.cb_term);
        cbTerm.setTypeface(font);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnNext.setTypeface(font);
        txtNupOnline = (TextView) findViewById(R.id.txt_nup_online);
        text = (HtmlTextView) findViewById(R.id.html_text);

        //logo
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this).load(WebService.getProjectImage()+dbMasterRef+
                        "&pr="+projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout)findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/1.233333333333333;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        requestTermAndConditionNup();

        //imgterm
        LinearLayout rPageTerm = (LinearLayout)findViewById(R.id.rPageTerm);

        Display displayTerm = getWindowManager().getDefaultDisplay();
        Point sizeTerm = new Point();
        display.getSize(sizeTerm);
        Integer widthTerm = sizeTerm.x;
        Double resultTerm = width/1.555555555555556;
        Log.d("screen width", resultTerm.toString()+"--"+Math.round(resultTerm));

        ViewGroup.LayoutParams paramsTerm = rPageTerm.getLayoutParams();
        paramsTerm.width = widthTerm;
        paramsTerm.height = resultTerm.intValue() ;
        rPageTerm.setLayoutParams(paramsTerm);
        rPageTerm.requestLayout();

        Glide.with(NupTermActivity.this)
                .load("http://www.nataproperty.com/assets/images/other/how_to_get_priortiy_pass.jpg").into(imgTerm);
        text.setTypeface(fontLight);
        text.setTextColor(Color.BLACK);

        btnNext.setEnabled(false);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NupTermActivity.this,NupSearchCustomerActivity.class);
                intent.putExtra(DBMASTER_REF,dbMasterRef);
                intent.putExtra(PROJECT_REF,projectRef);
                intent.putExtra(PROJECT_DESCRIPTION,projectDescription);
                intent.putExtra(PROJECT_NAME,projectName);
                intent.putExtra(NUP_AMT,nupAmt);
                startActivity(intent);
            }
        });

    }

    public void requestTermAndConditionNup() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getTermAndCondition(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result nup",response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    termAndCondition  = jo.getString("termAndCondition");

                    text.setHtmlFromString(termAndCondition, new HtmlTextView.RemoteImageGetter());

                    if (status==200){
                        txtNupOnline.setVisibility(View.VISIBLE);
                        text.setVisibility(View.VISIBLE);
                        cbTerm.setVisibility(View.VISIBLE);
                        cbTerm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(!isChecked){
                                    btnNext.setEnabled(false);
                                }else {
                                    btnNext.setEnabled(true);
                                }
                            }
                        });
                    }else if (status==201){
                        txtNupOnline.setVisibility(View.GONE);
                        text.setVisibility(View.GONE);
                        cbTerm.setVisibility(View.GONE);
                        btnNext.setEnabled(true);
                    }else {
                        Log.d("error"," "+message);
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(NupTermActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(NupTermActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "nupTerm");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupTermActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
