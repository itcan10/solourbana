package com.nataproperty.solourbana.view.project.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserModel on 5/16/2016.
 */
public class ValidateInhouseCode extends AppCompatActivity {
    EditText edtInhouseCode, edtMemberCode;
    Button btnValidate;

    String memberRef, dbMasterRef, projectRef, memberCode;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_inhouse);
        initWidget();

        Intent intent = getIntent();
        memberRef = intent.getStringExtra("memberRef");
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        memberCode = intent.getStringExtra("memberCode");

        if (memberCode!=null) {
            edtMemberCode.setText(memberCode);
        }

        Log.d("validate inhouse", " " + memberRef + "-" + dbMasterRef + "-" + projectRef);

        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekInhouseCode = edtInhouseCode.getText().toString();
                if (cekInhouseCode.equals("")) {
                    edtInhouseCode.setError("Harus diisi.");
                } else {
                    edtInhouseCode.setError(null);
                    subscribeProjectInhouse();
                }
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_inhouse_code));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        edtInhouseCode = (EditText) findViewById(R.id.txt_inhouse_code);
        edtMemberCode = (EditText) findViewById(R.id.txt_member_code);
        btnValidate = (Button) findViewById(R.id.btn_validate);
        btnValidate.setTypeface(font);

    }

    public void subscribeProjectInhouse() {
        LoadingBar.startLoader(ValidateInhouseCode.this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.subscribeProjectInhouse(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LoadingBar.stopLoader();
                    JSONObject jo = new JSONObject(response);
                    Log.d("subscribeProjectInhouse", "" + response.toString());
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Toast.makeText(ValidateInhouseCode.this, message, Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(ValidateInhouseCode.this, ProjectMenuActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (status == 201) {
                        Toast.makeText(ValidateInhouseCode.this, message, Toast.LENGTH_LONG).show();
                        btnValidate.setEnabled(true);
                    } else if (status == 202) {
                        edtMemberCode.setError(message);
                        btnValidate.setEnabled(true);
                    } else if (status == 203) {
                        edtMemberCode.setError(message);
                        btnValidate.setEnabled(true);
                    } else if (status == 204) {
                        edtInhouseCode.setError(message);
                        btnValidate.setEnabled(true);
                    } else {
                        Toast.makeText(ValidateInhouseCode.this, message, Toast.LENGTH_LONG).show();
                        btnValidate.setEnabled(true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(ValidateInhouseCode.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ValidateInhouseCode.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("dbMasterRef", dbMasterRef.toString());
                params.put("projectRef", projectRef.toString());
                params.put("inhouseCode", edtInhouseCode.getText().toString());
                params.put("memberCode", edtMemberCode.getText().toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "inhouseCode");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
