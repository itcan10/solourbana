package com.nataproperty.solourbana.view.ilustration.model;

/**
 * Created by "dwist14"
 * on Aug 8/4/2017 15:13.
 * Project : TangCity
 */
public class ColorModel {
    String color;
    String colorName;

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
