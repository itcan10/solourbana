package com.nataproperty.solourbana.view.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.view.project.model.DownloadModel;

import java.io.File;
import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class DownloadAdapter extends BaseAdapter {
    private Context context;
    private List<DownloadModel> list;
    private ListDownloadHolder holder;

    public DownloadAdapter(Context context, List<DownloadModel> list) {
        this.context = context;
        this.list = list;

    }

    private String fileRef, filename, extension;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_download, null);
            holder = new ListDownloadHolder();
            holder.fileName = (TextView) convertView.findViewById(R.id.txt_fileName);
            //holder.extension = (TextView) convertView.findViewById(R.id.txt_extension);
            holder.fileJPG = (ImageView) convertView.findViewById(R.id.ic_file_jpg);
            holder.filePNG = (ImageView) convertView.findViewById(R.id.ic_file_png);
            holder.filePDF = (ImageView) convertView.findViewById(R.id.ic_file_pdf);
            holder.shared = (ImageView) convertView.findViewById(R.id.ic_action_shared);

            convertView.setTag(holder);
        } else {
            holder = (ListDownloadHolder) convertView.getTag();
        }
        DownloadModel download = list.get(position);
        holder.fileName.setText(download.getFileName());
        //holder.extension.setText(download.getExtension());

        if (download.getExtension().toLowerCase().equals("jpg")) {
            holder.fileJPG.setVisibility(View.VISIBLE);
            holder.filePNG.setVisibility(View.GONE);
            holder.filePDF.setVisibility(View.GONE);
        } else if (download.getExtension().toLowerCase().equals("png")) {
            holder.fileJPG.setVisibility(View.GONE);
            holder.filePNG.setVisibility(View.VISIBLE);
            holder.filePDF.setVisibility(View.GONE);
        } else if (download.getExtension().toLowerCase().equals("pdf")) {
            holder.fileJPG.setVisibility(View.GONE);
            holder.filePNG.setVisibility(View.GONE);
            holder.filePDF.setVisibility(View.VISIBLE);
        } else {
            holder.fileJPG.setVisibility(View.GONE);
            holder.filePNG.setVisibility(View.GONE);
            holder.filePDF.setVisibility(View.INVISIBLE);
        }

        final DownloadModel downloadModel = list.get(position);

        final String url = WebService.URL_DOWNLOAD + "?is=fileDownload" +
                "&dr=" + downloadModel.getDbMasterRef().toString() +
                "&pr=" + downloadModel.getProjectRef().toString() +
                "&fr=" + downloadModel.getFileRef().toString() +
                "&fn=" + downloadModel.getFileName().toString().replace(" ", "%20") +
                "&ex=." + downloadModel.getExtension().toString();
        Log.d("urldAdapter", url);

        holder.shared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileRef = downloadModel.getFileRef();
                filename = downloadModel.getFileName();
                extension = downloadModel.getExtension();

                String path = Environment.getExternalStorageDirectory().getPath() + "/nataproperty/"
                        + filename.toString() + "." + extension.toString();
                File f = new File(path);
                if (f.exists()) {
                    if (extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png")) {
                        try {
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("image/*");
                            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                                    "/nataproperty/" + downloadModel.getFileName().toString() + "." + downloadModel.getExtension().toString();
                            File imageFileToShare = new File(imagePath);
                            Uri uri = Uri.fromFile(imageFileToShare);
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            context.startActivity(Intent.createChooser(share, "Share Image!"));
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "File tidak bisa dibagikan", Toast.LENGTH_SHORT).show();
                        }

                    } else if (extension.equals("pdf")) {
                        try {
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("application/pdf");
                            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                                    "/nataproperty/" + downloadModel.getFileName().toString() + "." + downloadModel.getExtension().toString();
                            File imageFileToShare = new File(imagePath);
                            Uri uri = Uri.fromFile(imageFileToShare);
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            context.startActivity(Intent.createChooser(share, "Share Document!"));

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "File tidak bisa dibagikan", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "File tidak bisa dibuka", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "File harus didownload terlebih dahulu", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return convertView;
    }

    private class ListDownloadHolder {
        TextView fileName, extension;
        ImageView fileJPG, filePNG, filePDF, shared;
    }
}
