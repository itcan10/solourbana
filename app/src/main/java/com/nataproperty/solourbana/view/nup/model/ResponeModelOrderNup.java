package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by Nata on 2/9/2017.
 */

public class ResponeModelOrderNup {
    int status;
    String message;
    String nupOrderRef;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNupOrderRef() {
        return nupOrderRef;
    }

    public void setNupOrderRef(String nupOrderRef) {
        this.nupOrderRef = nupOrderRef;
    }
}
