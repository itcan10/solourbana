package com.nataproperty.solourbana.view.mynup.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mynup.adapter.MyNupProjectPsAdapter;
import com.nataproperty.solourbana.view.mynup.model.MyNupProjectPsModel;
import com.nataproperty.solourbana.view.mynup.presenter.MyNupProjectPsPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class MyNupProjectPsActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    private static final String TAG = "MyNupProjectPs";

    Toolbar toolbar;
    TextView title;
    ListView listView;
    SharedPreferences sharedPreferences;
    String memberRef, projectRef, dbMasterRef, projectPsRef, projectSchemeRef, nupRef, projectName, ppNup, ppName, totalAmt;
    ProgressDialog progressDialog;

    MyNupProjectPsPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    MyNupProjectPsAdapter adapter;
    List<MyNupProjectPsModel> listMyNupProjectPs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_nup_unpaid);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyNupProjectPsPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        projectPsRef = intent.getStringExtra("projectPsRef");
        projectSchemeRef = intent.getStringExtra("projectSchemeRef");

        Log.d(TAG, "param " + dbMasterRef + " " + projectRef + " " + projectPsRef);

        requestMyNup();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                nupRef = listMyNupProjectPs.get(position).getNupRef();
                projectName = listMyNupProjectPs.get(position).getProjectName();
                ppNup = listMyNupProjectPs.get(position).getNupNo();
                ppName = listMyNupProjectPs.get(position).getNupName();
                totalAmt = listMyNupProjectPs.get(position).getAmount();

                Intent intent = new Intent(MyNupProjectPsActivity.this, MyNupDetailProjectPsActivity.class);
                intent.putExtra("nupRef", nupRef);
                intent.putExtra("dbMasterRef", dbMasterRef);
                intent.putExtra("projectRef", projectRef);
                intent.putExtra("projectPsRef", projectPsRef);
                intent.putExtra("projectName", projectName);
                intent.putExtra("ppNup", ppNup);
                intent.putExtra("ppName", ppName);
                intent.putExtra("totalAmt", totalAmt);
                intent.putExtra("projectSchemeRef", projectSchemeRef);

                startActivity(intent);
            }
        });

    }

    private void requestMyNup() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListNupProjectPs(dbMasterRef, projectRef, projectPsRef, memberRef,projectSchemeRef);
    }


    public void showListNupProjectPsResults(Response<List<MyNupProjectPsModel>> response) {
        progressDialog.dismiss();
        listMyNupProjectPs = response.body();
        listMyNupProjectPs.size();
        Log.d(TAG, "size " + listMyNupProjectPs.size());
        initAdapter();
    }

    public void showListNupProjectPsFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(MyNupProjectPsActivity.this, getString(R.string.error_connection), Toast.LENGTH_SHORT).show();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("My NUP");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        listView = (ListView) findViewById(R.id.list_nup_project_ps);
    }


    private void initAdapter() {
        adapter = new MyNupProjectPsAdapter(MyNupProjectPsActivity.this, listMyNupProjectPs);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyNupProjectPsActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    protected void onStop() {
        super.onStop();

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
