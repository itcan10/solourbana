package com.nataproperty.solourbana.view.ilustration.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.ilustration.intractor.PresenterIlustrationAddKprInteractor;
import com.nataproperty.solourbana.view.ilustration.model.StatusIlustrationAddKprModel;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationAddKPRActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationAddKprPresenter implements PresenterIlustrationAddKprInteractor {
    private IlustrationAddKPRActivity view;
    private ServiceRetrofit service;

    public IlustrationAddKprPresenter(IlustrationAddKPRActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getKprPayment(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<StatusIlustrationAddKprModel> call = service.getAPI().getIlustrationAddKpr(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo);
        call.enqueue(new Callback<StatusIlustrationAddKprModel>() {
            @Override
            public void onResponse(Call<StatusIlustrationAddKprModel> call, Response<StatusIlustrationAddKprModel> response) {
                view.showKprPaymentResults(response);
            }

            @Override
            public void onFailure(Call<StatusIlustrationAddKprModel> call, Throwable t) {
                view.showKprPaymentFailure(t);

            }


        });

    }

}
