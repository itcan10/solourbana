package com.nataproperty.solourbana.view.project.model;

/**
 * Created by nata on 11/17/2016.
 */

public class ChatRoomModel {
    long idChat;
    String memberRefSender;
    String memberRefReceiver;
    String chatMessage;
    String status;
    String sendTime;
    String readMessage;

    public long getIdChat() {
        return idChat;
    }

    public void setIdChat(long idChat) {
        this.idChat = idChat;
    }

    public String getMemberRefSender() {
        return memberRefSender;
    }

    public void setMemberRefSender(String memberRefSender) {
        this.memberRefSender = memberRefSender;
    }

    public String getMemberRefReceiver() {
        return memberRefReceiver;
    }

    public void setMemberRefReceiver(String memberRefReceiver) {
        this.memberRefReceiver = memberRefReceiver;
    }

    public String getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(String chatMessage) {
        this.chatMessage = chatMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getReadMessage() {
        return readMessage;
    }

    public void setReadMessage(String readMessage) {
        this.readMessage = readMessage;
    }
}
