package com.nataproperty.solourbana.view.event.ui;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.event.adapter.EventDetailAdapter;
import com.nataproperty.solourbana.view.event.adapter.EventSchaduleAdapter;
import com.nataproperty.solourbana.view.event.model.EventDetailModel;
import com.nataproperty.solourbana.view.event.model.EventSchaduleModel;
import com.nataproperty.solourbana.view.event.presenter.EventDetailPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Response;

public class EventDetailActivity extends AppCompatActivity {
    public static final String TAG = "EventDetailActivity";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String CONTENT_TYPE = "contentType";
    public static final String CONTENT_REF = "contentRef";
    public static final String TITLE = "title";
    public static final String SYNOPSIS = "synopsis";
    public static final String SCHADULE = "schedule";
    public static final String LINK_DETAIL = "linkDetail";

    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String GUEST_NAME = "guest_name";
    public static final String COMPANY_NAME = "company_name";
    public static final String GUEST_PHONE = "guest_phone";
    public static final String GUEST_MOBILE1 = "guest_hp1";
    public static final String GUEST_MOBILE2 = "guest_hp2";
    public static final String GUEST_EMAIL = "guest_email";
    public static final String STATUS = "status";

    private static final String EXTRA_RX = "EXTRA_RX";

    String dbMasterRef, projectRef, contentType, contentRef, txtTitle, synopsis, schedule, linkDetail;
    String eventScheduleRef, guest_name, company_name, guest_phone, guest_hp1, guest_hp2, guest_email, status;

    private List<EventDetailModel> listImage = new ArrayList<EventDetailModel>();
    private ArrayList<EventSchaduleModel> listSchadule = new ArrayList<EventSchaduleModel>();
    private EventDetailAdapter adapter;
    private EventSchaduleAdapter adapterSchadule;
    MyListView listView;
    ViewPager viewPager;
    CircleIndicator indicator;

    TextView txtTitleEvent, txtSchedule;
    String titleEvent, schadule;

    WebView webView;

    static EventDetailActivity eventDetailActivity;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    LinearLayout linearLayout;
    RelativeLayout rPage;
    Display display;

    EventDetailPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        txtTitle = intent.getStringExtra(TITLE);
        schedule = intent.getStringExtra(SCHADULE);
        synopsis = intent.getStringExtra(SYNOPSIS);
        contentType = intent.getStringExtra(CONTENT_TYPE);
        contentRef = intent.getStringExtra(CONTENT_REF);
        linkDetail = intent.getStringExtra(LINK_DETAIL);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new EventDetailPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        eventDetailActivity = this;

        Log.d(TAG, "Get " + linkDetail + " " + contentRef);

        requestImage();
        requestListEventSchadule();

        txtTitleEvent.setText(txtTitle);
        txtSchedule.setText(schedule);

        webView = (WebView) findViewById(R.id.web_content);
        webView.loadUrl(linkDetail);

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_event));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager) findViewById(R.id.viewpager_event_detail);

        txtTitleEvent = (TextView) findViewById(R.id.txt_title);
        txtSchedule = (TextView) findViewById(R.id.txt_schedule);
        listView = (MyListView) findViewById(R.id.list_event_schadule);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.78125;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
    }

    private void requestImage() {
        presenter.getNewsImageSliderSvc(contentRef);
    }

    private void requestListEventSchadule() {
        presenter.getListEventScheduleSvc(contentRef);
    }

    public void showListEventImageResults(Response<ArrayList<EventDetailModel>> response) {
        listImage = response.body();
        initAdapterImage();
    }

    public void showListEventImageFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapterImage() {
        adapter = new EventDetailAdapter(this, listImage);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
    }

    public void showListEventeScheduleResults(Response<ArrayList<EventSchaduleModel>> response) {
        listSchadule = response.body();
        initAdapterSchedule();
    }

    public void showListEventScheduleFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapterSchedule() {
        adapterSchadule = new EventSchaduleAdapter(this, listSchadule, linkDetail);
        listView.setAdapter(adapterSchadule);
        listView.setExpanded(true);
    }

    public static EventDetailActivity getInstance() {
        return eventDetailActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EventDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


}
