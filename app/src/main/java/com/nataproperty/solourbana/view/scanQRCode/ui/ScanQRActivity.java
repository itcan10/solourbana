package com.nataproperty.solourbana.view.scanQRCode.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.zxing.Result;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.scanQRCode.model.ScanURLModel;
import com.nataproperty.solourbana.view.scanQRCode.presenter.ScanQRPresenter;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Response;

public class ScanQRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private AlertDialog alertDialog;
    private ServiceRetrofitProject service;
    private ScanQRPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new ScanQRPresenter(this, service);
        QrScanner();
    }

    public void QrScanner() {
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

    @Override
    public void handleResult(Result result) {
        // Do something with the result here

        Log.e("handler", result.getText()); // Prints scan results
        Log.e("handler", result.getBarcodeFormat().toString()); // Prints the scan format (qrcode)
        //bookingCode = result.getText();
        // show the scanner result into dialog box.
        /*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ScanQRActivity.this);
        alertDialogBuilder
                .setTitle("Scan Result")
                .setMessage(bookingCode)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestBookingInfo(bookingCode);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        alertDialogDismis();
                    }
                });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();*/

        requestBookingInfo(result.getText());

    }

    private void requestBookingInfo(String bookingCode) {
        presenter.CheckingDocumentProject(General.projectCode, bookingCode);
    }

    private void alertDialogDismis() {
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    public void showScanResults(Response<ScanURLModel> response, String bookingCode) {
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            BaseApplication.showLog("show log","status "+response.toString());
            if (status == 200) {
//                Intent i = new Intent(ScanQRActivity.this, ScanQRDetailActivity.class);
                Intent i = new Intent(ScanQRActivity.this, ScanQRCodeWebviewActivity.class);
                i.putExtra("link", response.body().getUrlPrint());
                startActivity(i);
                finish();
            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ScanQRActivity.this);
                alertDialogBuilder
                        .setMessage("Data tidak ditemukan")
                        .setCancelable(false)
                        .setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alertDialogDismis();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }
    }

    public void showScanFailure(Throwable t) {

    }
}
