package com.nataproperty.solourbana.view.kpr.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.view.kpr.model.CalculationModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by UserModel on 4/19/2016.
 */
public class CalculationAdapter extends BaseAdapter  {
    public static final String TAG = "NotificationAdapter" ;

    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;
    String memberRef;

    private Context context;
    private List<CalculationModel> list;

    private ListCalculationHolder holder;

    CalculationModel calculationModel;

    public CalculationAdapter(Context context, List<CalculationModel> list) {
        this.context = context;
        this.list = list;

    }

    String project;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_calculation,null);
            holder = new ListCalculationHolder();
            holder.title = (TextView) convertView.findViewById(R.id.txt_project_name);
            holder.rubish = (ImageView) convertView.findViewById(R.id.btn_rubish);
            holder.countTemp = (TextView) convertView.findViewById(R.id.txt_countTemp);

            convertView.setTag(holder);
        }else{
            holder = (ListCalculationHolder) convertView.getTag();
        }

        calculationModel = list.get(position);
        holder.title.setText(calculationModel.getProject());
        holder.countTemp.setText(calculationModel.getCountTemp());
        holder.rubish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                project = list.get(position).getProject();
                Log.d("project",""+project);
                AlertDialog.Builder alertDialogBuilder =
                        new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Apakah anda yakin menghapus project?");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteProject(project);
                        list.remove(position);
                        notifyDataSetChanged();
                        notifyDataSetInvalidated();

                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        return convertView;
    }


    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListCalculationHolder {
        TextView title,countTemp;
        ImageView rubish;
    }

    public void deleteProject(final String project) {
//        BaseApplication.getInstance().startLoader(context);
        LoadingBar.startLoader(context);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteProjectCalcUserProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d(TAG, response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    if (status == 200) {
                        notifyDataSetChanged();
                    }

                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(context, context.getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context, context.getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("project", project.toString());
                params.put("memberRef", memberRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "delete");

    }

}
