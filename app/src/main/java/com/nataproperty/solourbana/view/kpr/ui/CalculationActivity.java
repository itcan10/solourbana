package com.nataproperty.solourbana.view.kpr.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.kpr.adapter.CalculationAdapter;
import com.nataproperty.solourbana.view.kpr.model.CalculationModel;
import com.nataproperty.solourbana.view.kpr.presenter.CalculationPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

public class CalculationActivity extends AppCompatActivity {
    public static final String TAG = "CalculationActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private CalculationPresenter presenter;
    ProgressDialog progressDialog;

    public static final String PREF_NAME = "pref";
    SharedPreferences sharedPreferences;
    String memberRef;

    Button btnAdd;
    MyListView listView;

    LinearLayout linearList, linearNoList;

    private List<CalculationModel> listCalculation = new ArrayList<>();
    private CalculationAdapter adapter;
    Toolbar toolbar;
    TextView title;
    Typeface font;

    String calcUserProjectRef,project,cluster,product,unit,price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new CalculationPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Log.d(TAG,"isMemberRef "+memberRef);
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                project = listCalculation.get(position).getProject();
                calcUserProjectRef = listCalculation.get(position).getCalcUserProjectRef();
                Intent intent = new Intent(CalculationActivity.this,CalculationDetailActivity.class);
                intent.putExtra("project",project);
                intent.putExtra("calcUserProjectRef",calcUserProjectRef);
                startActivity(intent);
            }
        });


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CalculationActivity.this, CalculationInputProjectActivity.class);
                startActivity(intent);
            }
        });



    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_calculation));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnAdd = (Button) findViewById(R.id.btn_add);
        listView = (MyListView) findViewById(R.id.list_calculation);

        linearList = (LinearLayout) findViewById(R.id.linear_list_calculation);
        linearNoList = (LinearLayout) findViewById(R.id.linear_no_list_calculation);
        btnAdd.setTypeface(font);


    }

    @Override
    protected void onResume() {
        super.onResume();
//        listCalculation.clear();
        requestListCaculation();
    }


    public void requestListCaculation() {
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListCal(memberRef.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CalculationActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showListCalResults(retrofit2.Response<List<CalculationModel>> response) {
        progressDialog.dismiss();
        listCalculation=response.body();
        if(listCalculation.size()!=0){
            linearNoList.setVisibility(View.GONE);
            linearList.setVisibility(View.VISIBLE);
        }else {
        linearNoList.setVisibility(View.VISIBLE);
        linearList.setVisibility(View.GONE);
        }
        initAdapter();
    }

    private void initAdapter() {
        adapter = new CalculationAdapter(this,listCalculation);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showListCalFailure(Throwable t) {
        progressDialog.dismiss();

    }
}
