package com.nataproperty.solourbana.view.commision.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.commision.adapter.CommisionScheduleAdapter;
import com.nataproperty.solourbana.view.commision.model.CommisionScheduleModel;
import com.nataproperty.solourbana.view.commision.presenter.CommisionSchedulePresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class CommisionScheduleActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    Toolbar toolbar;
    TextView title;
    Typeface font;

    ProgressDialog progressDialog;
    CommisionSchedulePresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    String memberRef, dbMasterRef, projectRef, projectPsRef, bookingRef;

    MyListView listView;
    CommisionScheduleAdapter adapter;
    List<CommisionScheduleModel> listCommisionSchedule = new ArrayList<>();

    String bookingCode, customerName, projectName, clusterName, blockName, unitName, commisionStatusName,
            comPriceAmt, custPaid, totalAmtCommision, countCommision;
    TextView txtBookingCode, txtCustomerName, txtProjectName, txtClusterName, txtBlockName, txtUnitName,
            txtCommisionStatusName, txtComPriceAmt, txtCustPaid, txtTotalAmtCommision;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commision_schedule);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new CommisionSchedulePresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra("dbMasterRef");
        projectRef = intent.getStringExtra("projectRef");
        projectPsRef = intent.getStringExtra("projectPsRef");
        bookingRef = intent.getStringExtra("bookingRef");
        bookingCode = intent.getStringExtra("bookingCode");
        customerName = intent.getStringExtra("customerName");
        projectName = intent.getStringExtra("projectName");
        clusterName = intent.getStringExtra("clusterName");
        blockName = intent.getStringExtra("blockName");
        unitName = intent.getStringExtra("unitName");
        commisionStatusName = intent.getStringExtra("commisionStatusName");
        comPriceAmt = intent.getStringExtra("comPriceAmt");
        custPaid = intent.getStringExtra("custPaid");
        totalAmtCommision = intent.getStringExtra("totalAmtCommision");
        countCommision = intent.getStringExtra("countCommision");

        txtBookingCode.setText(bookingCode);
        txtCustomerName.setText(customerName);
        txtProjectName.setText(projectName);
        txtClusterName.setText(clusterName);
        txtBlockName.setText(blockName);
        txtUnitName.setText(unitName);
        if (countCommision.equals("0")){
            txtCommisionStatusName.setText(commisionStatusName);
        }else {
            txtCommisionStatusName.setText(commisionStatusName + " (" + countCommision + ")");
        }
        txtComPriceAmt.setText(comPriceAmt);
        txtCustPaid.setText(custPaid + " %");
        txtTotalAmtCommision.setText(totalAmtCommision);

        requestCommisionSchedule();
    }

    private void requestCommisionSchedule() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.GetListCommissionCustProjectSvc(dbMasterRef, projectRef, bookingRef, memberRef);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Schedule & Pembayaran Customer");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (MyListView) findViewById(R.id.list_commision_schedule);
        txtBookingCode = (TextView) findViewById(R.id.txt_booking_code);
        txtProjectName = (TextView) findViewById(R.id.txt_property);
        txtClusterName = (TextView) findViewById(R.id.txt_cluster_name);
        txtBlockName = (TextView) findViewById(R.id.txt_block_name);
        txtUnitName = (TextView) findViewById(R.id.txt_unit_no);
        txtComPriceAmt = (TextView) findViewById(R.id.txt_comPriceAmt);
        txtCommisionStatusName = (TextView) findViewById(R.id.txt_commisionStatusName);
        txtTotalAmtCommision = (TextView) findViewById(R.id.txt_totalAmtCommision);
        txtCustPaid = (TextView) findViewById(R.id.txt_custPaid);
        txtCustomerName = (TextView) findViewById(R.id.txt_customer_name);
    }

    public void showListCommisionScheduleResults(Response<List<CommisionScheduleModel>> response) {
        progressDialog.dismiss();
        listCommisionSchedule = response.body();
        initAdapter();
    }

    public void showListCommisionnScheduleFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new CommisionScheduleAdapter(CommisionScheduleActivity.this, listCommisionSchedule);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
