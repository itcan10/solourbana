package com.nataproperty.solourbana.view.event.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.event.adapter.MyTikcetAdapter;
import com.nataproperty.solourbana.view.event.model.MyTicketModel;
import com.nataproperty.solourbana.view.event.presenter.MyTicketPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;

import retrofit2.Response;

public class MyTicketActivity extends AppCompatActivity {
    public static final String TAG = "MyTicketActivity";

    public static final String PREF_NAME = "pref";

    public static final String TITLE = "title";
    public static final String EVENT_SCHEDULE_REF = "eventScheduleRef";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String EVENT_SCHEDULE_DATE = "eventScheduleDate";
    private static final String EXTRA_RX = "EXTRA_RX";

    SharedPreferences sharedPreferences;

    ListView listView;

    private ArrayList<MyTicketModel> listMyTicket = new ArrayList<MyTicketModel>();
    private MyTikcetAdapter adapter;

    String memberRef, linkDetail, titleGcm, date, eventScheduleRef, txtTitle, eventScheduleDate;

    LinearLayout linearNoData, linearLayout;
    Toolbar toolbar;
    TextView title;
    Typeface font;

    MyTicketPresenter presenter;
    ServiceRetrofitProject service;
    boolean rxCallInWorks = false;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ticket);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new MyTicketPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                eventScheduleRef = listMyTicket.get(position).getEventScheduleRef();
                txtTitle = listMyTicket.get(position).getTitle();
                eventScheduleDate = listMyTicket.get(position).getEventScheduleDate();
                Intent intent = new Intent(MyTicketActivity.this, MyTicketDetailActivity.class);
                intent.putExtra(EVENT_SCHEDULE_REF, eventScheduleRef);
                intent.putExtra(TITLE, txtTitle);
                intent.putExtra(EVENT_SCHEDULE_DATE, eventScheduleDate);
                startActivity(intent);
            }
        });

        requestListMyTicket();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_ticket));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        listView = (ListView) findViewById(R.id.list_myticket);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
    }

    private void requestListMyTicket() {
        presenter.getListTiketRsvp(memberRef, General.projectCode);
    }


    public void showListMyTicketResults(Response<ArrayList<MyTicketModel>> response) {
        listMyTicket = response.body();
        if (listMyTicket.size() != 0) {
            initAdapter();
            linearNoData.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            linearNoData.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
    }

    public void showListTicketFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapter() {
        adapter = new MyTikcetAdapter(this, listMyTicket);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyTicketActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
