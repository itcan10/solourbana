package com.nataproperty.solourbana.view.nup.ui;

import android.support.v7.app.AppCompatActivity;

//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.util.EntityUtils;

/**
 * Created by UserModel on 5/21/2016.
 */
public class NupCreditCardActivity extends AppCompatActivity {
    /*public static final String PREF_NAME = "pref" ;
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String TOTAL = "total";

    private List<MouthModel> listMounth = new ArrayList<MouthModel>();
    private MounthAdapter adapterMounth;
    private List<YearModel> listYear = new ArrayList<YearModel>();
    private YearAdapter adapterYear;

    SharedPreferences sharedPreferences;

    Spinner spnMounth,spnYear;

    ImageView imgProject;
    TextView txtProjectName,txtCostumerName,txtOrderCode,txtPriortyPass,txtAmount,txtEmail,txtAmountTotal,txtTotal;
    EditText editCardNumber,editSecurityCard,editExDate;

    Button btnPayment;

    String costumerName,orderCode,priorityPass,amount,projectName,totalAmount,total;
    String email;
    String nupOrderRef,projectRef,dbMasterRef;
    String monthRef,year;

    //veritrans
    AlertDialog dialog3ds;
    ProgressDialog sendServerProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_credit_card);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Credit Card");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        total = intent.getStringExtra(TOTAL);

        imgProject = (ImageView) findViewById(R.id.image_project);

        RelativeLayout rPage = (RelativeLayout)findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/1.233333333333333;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtTotal = (TextView) findViewById(R.id.txt_total);
        txtTotal.setText(total);

        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtCostumerName = (TextView) findViewById(R.id.txt_customer_name);
        txtOrderCode = (TextView) findViewById(R.id.txt_order_code);
        txtPriortyPass = (TextView) findViewById(R.id.txt_priorty_pass);
        txtAmount = (TextView) findViewById(R.id.txt_amount);
        txtAmountTotal = (TextView) findViewById(R.id.txt_amount_total);
        txtEmail = (TextView) findViewById(R.id.member_email);

        editCardNumber = (EditText) findViewById(R.id.edit_card_number);
        editSecurityCard = (EditText) findViewById(R.id.edit_security_card);
       // editExDate = (EditText) findViewById(R.id.edit_ex_date);

        spnMounth = (Spinner) findViewById(R.id.list_mounth);
        spnYear =  (Spinner) findViewById(R.id.list_year);

        btnPayment = (Button) findViewById(R.id.btn_payment);
        btnPayment.setTypeface(font);
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekCardNumber = editCardNumber.getText().toString().toString();
                String cekSecurityCard = editSecurityCard.getText().toString().toString();
                String cekMount = monthRef;
                String cekYear = year;

                if (!cekCardNumber.isEmpty() && editCardNumber.length()==16 && !cekSecurityCard.isEmpty() && !cekMount.equals("0") &&
                        !cekYear.equals("Not Defined")){

                    paymentCreditcard();

                }else {
                    if (editCardNumber.getText().toString().toString().equals("")){
                        editCardNumber.setError("Credit card is empty");
                    }else if(editCardNumber.length()!=16){
                        editCardNumber.setError("Credit card not valid");
                    }else {
                        editCardNumber.setError(null);
                    }
                    Log.d("lengt"," "+ editCardNumber.length());

                    if (cekSecurityCard.isEmpty()){
                        editSecurityCard.setError("Security card is empty");
                    }else {
                        editSecurityCard.setError(null);
                    }

                    if (cekMount.equals("0")){
                        Toast.makeText(NupCreditCardActivity.this,"Select month expired date",Toast.LENGTH_SHORT).show();
                    }else if (cekYear.equals("Not Defined")){
                        Toast.makeText(NupCreditCardActivity.this,"Select year expired date",Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });

        requestMounth();

        requestYear();

        requestOrderInfo();

        adapterMounth = new MounthAdapter(this,listMounth);
        spnMounth.setAdapter(adapterMounth);
        spnMounth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthRef = listMounth.get(position).getMounthRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterYear = new YearAdapter(this,listYear);
        spnYear.setAdapter(adapterYear);
        spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                year = listYear.get(position).getYear();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void requestOrderInfo (){
        String url = WebService.getOrderInfo();
        String urlPostParameter ="&dbMasterRef="+dbMasterRef.toString()+
                "&projectRef="+projectRef.toString()+
                "&nupOrderRef="+nupOrderRef.toString();                ;
        String result = Network.PostHttp(url, urlPostParameter);
        try {
            JSONObject jo = new JSONObject(result);
            Log.d("result nup review",result);
            int status = jo.getInt("status");
            String message = jo.getString("message");

            if (status==200){
                projectName = jo.getJSONObject("data").getString("projectName");
                orderCode = jo.getJSONObject("data").getString("nupOrderCode");
                costumerName = jo.getJSONObject("data").getString("customerName");
                priorityPass = jo.getJSONObject("data").getString("quantity");
                amount = jo.getJSONObject("data").getString("amount");
                totalAmount = jo.getJSONObject("data").getString("totalAmount");

                txtProjectName.setText(projectName);
                txtCostumerName.setText(costumerName);
                txtOrderCode.setText(orderCode);
                txtPriortyPass.setText(priorityPass);
                DecimalFormat decimalFormat= new DecimalFormat("###,##0");
                txtAmount.setText("IDR "+ String.valueOf(decimalFormat.format(Double.parseDouble(amount))));
                txtAmountTotal.setText("IDR "+ String.valueOf(decimalFormat.format(Double.parseDouble(totalAmount))));
              *//*  txtAmount.setText("IDR "+amount);
                txtAmountTotal.setText("IDR "+totalAmount);*//*

                Glide.with(this)
                        .load(WebService.getProjectImage()+dbMasterRef+
                                "&pr="+projectRef).into(imgProject);

            }else {
                Log.d("error"," "+message);

            }

        }catch (JSONException e){

        }

    }

    private void requestMounth (){
        String url = WebService.getMounth();
        String urlPostParameter ="";
        String result = Network.PostHttp(url, urlPostParameter);
        Log.d("result mounth"," "+result);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListMounth(jsonArray);

        }catch (JSONException e){

        }
    }

    private void generateListMounth(JSONArray response){
        for (int i = 0; i < response.length();i++){
            try {
                JSONObject jo = response.getJSONObject(i);
                MouthModel mouth = new MouthModel();
                mouth.setMounthRef(jo.getString("monthRef"));
                mouth.setMounthName(jo.getString("monthName"));

                listMounth.add(mouth);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        adapterMounth.notifyDataSetChanged();
    }

    private void requestYear (){
        String url = WebService.getYear();
        String urlPostParameter ="";
        String result = Network.PostHttp(url, urlPostParameter);
        Log.d("result year"," "+result);
        try {
            JSONArray jsonArray = new JSONArray(result);

            generateListYear(jsonArray);

        }catch (JSONException e){

        }
    }

    private void generateListYear(JSONArray response){
        for (int i = 0; i < response.length();i++){
            try {
                JSONObject jo = response.getJSONObject(i);
                YearModel year = new YearModel();
                year.setYear(jo.getString("year"));

                listYear.add(year);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
   //     adapterYear.notifyDataSetChanged();
    }

    private void paymentCreditcard(){
        //set environment
//        VTConfig.VT_IsProduction = WebService.VT_IsProduction;
//        //set client key
//        VTConfig.CLIENT_KEY = WebService.CLIENT_KEY;
//
//        VTDirect vtDirect = new VTDirect();
//        VTCardDetails cardDetails = null;
//        cardDetails = CardFactory(true);
//        vtDirect.setCard_details(cardDetails);
//
//        final ProgressDialog loadingDialog = ProgressDialog.show(NupCreditCardActivity.this,"","Loading, Please Wait...",true);
//        vtDirect.getToken(new ITokenCallback() {
//            @Override
//            public void onSuccess(VTToken vtToken) {
//                loadingDialog.cancel();
//                if(vtToken.getRedirect_url() != null){
//                    //show it to user using webview
//                    Log.d("VtLog",vtToken.getToken_id());
//
//                    CustomWebView webView = new CustomWebView(NupCreditCardActivity.this);
//                    webView.getSettings().setJavaScriptEnabled(true);
//                    webView.setOnTouchListener(new View.OnTouchListener() {
//                        @Override
//                        public boolean onTouch(View v, MotionEvent event) {
//                            switch (event.getAction()) {
//                                case MotionEvent.ACTION_DOWN:
//                                case MotionEvent.ACTION_UP:
//                                    if (!v.hasFocus()) {
//                                        v.requestFocus();
//                                    }
//                                    break;
//                            }
//                            return false;
//                        }
//                    });
//                    webView.setWebChromeClient(new WebChromeClient());
//                    webView.setWebViewClient(new VtWebViewClient(vtToken.getToken_id(),Integer.parseInt(totalAmount)+"",nupOrderRef));
//                    Log.d("total"," "+totalAmount);
//                    webView.loadUrl(vtToken.getRedirect_url());
//
//                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(NupCreditCardActivity.this);
//                    dialog3ds = alertBuilder.create();
//
//                    dialog3ds.setTitle("3D Secure Veritrans");
//                    dialog3ds.setView(webView);
//                    webView.requestFocus(View.FOCUS_DOWN);
//                    alertBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.dismiss();
//                        }
//                    });
//
//                    dialog3ds.show();
//
//                }
//            }
//
//            @Override
//            public void onError(Exception e) {
//                loadingDialog.cancel();
//                Toast.makeText(NupCreditCardActivity.this,e.getMessage(),Toast.LENGTH_SHORT);
//            }
//        });

    }

//    private VTCardDetails CardFactory(boolean secure){
//        VTCardDetails cardDetails = new VTCardDetails();
//        cardDetails.setCard_number(editCardNumber.getText().toString());
//        cardDetails.setCard_cvv(editSecurityCard.getText().toString());
//        cardDetails.setCard_exp_month(Integer.parseInt(monthRef));
//        cardDetails.setCard_exp_year(Integer.parseInt(year));
//        cardDetails.setSecure(secure);
//        cardDetails.setGross_amount(Integer.parseInt(totalAmount)+"");
//        return cardDetails;
//    }

    private class VtWebViewClient extends WebViewClient {
        String token;
        String price;
        String nupOrderRef;

        public VtWebViewClient(String token, String price,String nupOrderRef){
            this.token = token;
            this.price = price;
            this.nupOrderRef = nupOrderRef;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d("VtUrl",url);
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            Log.d("VtLog", url);

            if (url.startsWith(Constants.getPaymentApiUrl() + "/callback/")) {
                //send token to server
//                SendTokenAsync sendTokenAsync = new SendTokenAsync();
//                sendTokenAsync.execute(token,price,nupOrderRef);
                //close web dialog
                dialog3ds.dismiss();
                //show loading dialog
                sendServerProgress = ProgressDialog.show(NupCreditCardActivity.this,"","Sending Data to Server. Please Wait...",true);

            } else {
                if (url.startsWith(Constants.getPaymentApiUrl() + "/redirect/") || url.contains("3dsecure")) {
                *//* Do nothing *//*
                } else {
                    if (dialog3ds != null) {
                        dialog3ds.dismiss();
                    }
                }
            }
        }
    }

//    private class SendTokenAsync extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... params) {
//            HttpClient httpClient = new DefaultHttpClient();
//            HttpPost httpPost = new HttpPost(WebService.getVeritransNUP());
//            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
//            Log.d("cek param"," "+params[0]+"*"+params[1]);
//            try {
//                nameValuePairs.add(new BasicNameValuePair("tokenid", URLEncoder.encode(params[0],"UTF-8")));
//                nameValuePairs.add(new BasicNameValuePair("price",params[1]));
//                nameValuePairs.add(new BasicNameValuePair("nupOrderRef",params[2]));
//                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//                HttpResponse response = httpClient.execute(httpPost);
//                String jsonResponse = EntityUtils.toString(response.getEntity()).toString();
//                Log.d("VtLog Json", jsonResponse);
//                return jsonResponse;
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            return "";
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            //dismiss dialog
//            if(dialog3ds != null){
//                dialog3ds.dismiss();
//            }
//            if(sendServerProgress.isShowing()){
//                sendServerProgress.dismiss();
//            }
//
//            try{
//                VTRestResponse response = new Gson().fromJson(s,VTRestResponse.class);
//                Log.d("status",""+response.toString());
//
//                if(response.status.equalsIgnoreCase("succeed")){
//                    Toast.makeText(NupCreditCardActivity.this,"Success To Add payment: "+response.data.order_id,Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(NupCreditCardActivity.this,NupFinishActivity.class);
//                    intent.putExtra(DBMASTER_REF,dbMasterRef);
//                    intent.putExtra(PROJECT_REF,projectRef);
//                    intent.putExtra(NUP_ORDER_REF,nupOrderRef);
//
//                    SharedPreferences sharedPreferences = NupCreditCardActivity.this.
//                            getSharedPreferences(PREF_NAME, 0);
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putString("isMemberCostumerRef","");
//                    editor.commit();
//
//                    startActivity(intent);
//                    finish();
//                }else{
//                    Toast.makeText(NupCreditCardActivity.this,"Failed to Pay",Toast.LENGTH_SHORT).show();
//                }
//
//            }catch (Exception ex){
//                Toast.makeText(NupCreditCardActivity.this,"Failed to Pay ex",Toast.LENGTH_LONG).show();
//                Log.d("respon",ex.getMessage());
//            }
//        }
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }*/

}
