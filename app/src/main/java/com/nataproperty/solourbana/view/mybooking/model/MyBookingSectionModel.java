package com.nataproperty.solourbana.view.mybooking.model;

/**
 * Created by Nata on 1/3/2017.
 */

public class MyBookingSectionModel {
    String sectionName,dbMasterRef,projectRef,bookingCount,projectPsRef;

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getBookingCount() {
        return bookingCount;
    }

    public void setBookingCount(String bookingCount) {
        this.bookingCount = bookingCount;
    }

    public String getProjectPsRef() {
        return projectPsRef;
    }

    public void setProjectPsRef(String projectPsRef) {
        this.projectPsRef = projectPsRef;
    }
}
