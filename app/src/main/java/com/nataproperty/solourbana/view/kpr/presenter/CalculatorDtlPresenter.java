package com.nataproperty.solourbana.view.kpr.presenter;


import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.kpr.intractor.PresenterCalculatorDtlInteractor;
import com.nataproperty.solourbana.view.kpr.model.TemplateModel;
import com.nataproperty.solourbana.view.kpr.ui.CalculationDetailActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CalculatorDtlPresenter implements PresenterCalculatorDtlInteractor {
    private CalculationDetailActivity view;
    private ServiceRetrofit service;

    public CalculatorDtlPresenter(CalculationDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListTemplate(String project, String memberRef) {
        Call<List<TemplateModel>> call = service.getAPI().getListTemplate(project,memberRef);
        call.enqueue(new Callback<List<TemplateModel>>() {
            @Override
            public void onResponse(Call<List<TemplateModel>> call, Response<List<TemplateModel>> response) {
                view.showListTemplateResults(response);
            }

            @Override
            public void onFailure(Call<List<TemplateModel>> call, Throwable t) {
                view.showListTemplateFailure(t);

            }


        });
    }

}
