package com.nataproperty.solourbana.view.event.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.event.adapter.EventAdapter;
import com.nataproperty.solourbana.view.event.model.EventModel;
import com.nataproperty.solourbana.view.event.presenter.EventPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

public class EventActivity extends AppCompatActivity {
    public static final String TAG = "EventActivity";
    public static final String PREF_NAME = "pref";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String CONTENT_TYPE = "contentType";
    public static final String CONTENT_REF = "contentRef";
    public static final String TITLE = "title";
    public static final String SYNOPSIS = "synopsis";
    public static final String SCHADULE = "schedule";
    public static final String LINK_DETAIL = "linkDetail";
    private static final String EXTRA_RX = "EXTRA_RX";

    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());

    private ArrayList<EventModel> listEvent = new ArrayList<EventModel>();
    private EventAdapter adapter;

    CompactCalendarView compactCalendarView;
    long timeInMilliseconds;
    TextView txtMonth, txtNoDataMonth, txtNoDataDay;
    String dbMasterRef, projectRef, contentType, contentRef, txtTitle, synopsis, schedule, linkDetail;
    String formattedDate;
    MyListView listView;
    String givenDateString, memberRef;
    SharedPreferences sharedPreferences;
    LinearLayout linearLayout;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    EventPresenter presenter;
    ServiceRetrofitProject service;
    boolean rxCallInWorks = false;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new EventPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                String newText = dateformat.format(dateClicked);
                Log.d(TAG, "Date: " + dateformat.format(dateClicked));

                compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorBlockDiagramMatic));

                Filter filter = adapter.getFilter();
                if (newText.isEmpty()) {
                    filter.filter("");
                    txtNoDataMonth.setVisibility(View.GONE);
                    txtNoDataDay.setVisibility(View.VISIBLE);
                } else {
                    filter.filter(newText);
                    Log.d(TAG, "Date New Text: " + newText);
                }

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
                txtMonth.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                SimpleDateFormat dateformat = new SimpleDateFormat("MM/yyyy");
                Log.d(TAG, "Month: " + dateformat.format(firstDayOfNewMonth));

                formattedDate = dateformat.format(firstDayOfNewMonth);

                listEvent.clear();
                compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.colorWhite));
                compactCalendarView.removeAllEvents();
                requestEvent();

                Filter filter = adapter.getFilter();
                if (formattedDate.isEmpty()) {
                    filter.filter("");
                } else {
                    filter.filter(formattedDate);
                    Log.d(TAG, "Date New Text: " + formattedDate);
                }

            }
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/yyyy");
        formattedDate = df.format(c.getTime());
        Log.d(TAG, "Month: " + formattedDate);

        txtMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filter filter = adapter.getFilter();
                filter.filter("");
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_event));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtMonth = (TextView) findViewById(R.id.txt_month);
        txtNoDataMonth = (TextView) findViewById(R.id.txt_no_list_event_month);
        txtNoDataDay = (TextView) findViewById(R.id.txt_no_list_event_day);
        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        txtMonth.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        listView = (MyListView) findViewById(R.id.list_event);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        listEvent.clear();
        compactCalendarView.removeAllEvents();
        requestEvent();

    }

    private void requestEvent() {
        presenter.getListEventSvc(formattedDate, memberRef, General.projectCode);
    }

    public void showListEventResults(Response<ArrayList<EventModel>> response) {
        listEvent = response.body();
        List<Event> list = new ArrayList<Event>();
        initAdapter();
        for (int i = 0; i < listEvent.size(); i++) {
            givenDateString = listEvent.get(i).getEventDate();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date mDate = sdf.parse(givenDateString);
                timeInMilliseconds = mDate.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Event event = new Event(Color.RED, timeInMilliseconds);
            list.add(event);
        }
        compactCalendarView.addEvents(list);
        if (listEvent.size() == 0) {
            txtNoDataMonth.setVisibility(View.VISIBLE);
        } else {
            txtNoDataMonth.setVisibility(View.GONE);
        }
    }

    public void showListEventFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapter() {
        adapter = new EventAdapter(this, listEvent, compactCalendarView);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(EventActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
