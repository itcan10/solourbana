package com.nataproperty.solourbana.view.profile.presenter;


import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.nup.model.BankModel;
import com.nataproperty.solourbana.view.profile.intractor.AccountBankInteractor;
import com.nataproperty.solourbana.view.profile.ui.AccountBankActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class AccountBankPresenter implements AccountBankInteractor {
    private AccountBankActivity view;
    private ServiceRetrofit service;

    public AccountBankPresenter(AccountBankActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListBankSvc() {
        Call<ArrayList<BankModel>> call = service.getAPI().getListBankSvc();
        call.enqueue(new Callback<ArrayList<BankModel>> () {
            @Override
            public void onResponse(Call<ArrayList<BankModel>>  call, Response<ArrayList<BankModel>>  response) {
                view.showListBankResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<BankModel>>  call, Throwable t) {
                view.showListBankFailure(t);

            }


        });

    }

    @Override
    public void updateAccountBankSvc(String memberRef, String bankRef, String bankBranch, String accName, String accNo) {
        Call<ResponeModel> call = service.getAPI().updateAccountBankSvc(memberRef,bankRef,bankBranch,accName,accNo);
        call.enqueue(new Callback<ResponeModel> () {
            @Override
            public void onResponse(Call<ResponeModel>  call, Response<ResponeModel>  responseToken) {
                view.showResponeResults(responseToken);
            }

            @Override
            public void onFailure(Call<ResponeModel>  call, Throwable t) {
                view.showResponeFailure(t);

            }


        });
    }
}
