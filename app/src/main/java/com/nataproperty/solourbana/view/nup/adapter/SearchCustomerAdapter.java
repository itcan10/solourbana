package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.nup.model.SearchCustomerModel;

import java.util.List;

/**
 * Created by UserModel on 5/19/2016.
 */
public class SearchCustomerAdapter extends BaseAdapter {
    private Context context;
    private List<SearchCustomerModel> list;
    private ListSearchCustomerHolder holder;

    public SearchCustomerAdapter(Context context, List<SearchCustomerModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_search_customer,null);
            holder = new ListSearchCustomerHolder();
            holder.customerName = (TextView) convertView.findViewById(R.id.txt_customer_name);
            holder.birthDate = (TextView) convertView.findViewById(R.id.txt_birthDate);

            convertView.setTag(holder);
        }else{
            holder = (ListSearchCustomerHolder) convertView.getTag();
        }

        SearchCustomerModel searchCustomer = list.get(position);
        holder.customerName.setText(searchCustomer.getCustomerName());
        holder.birthDate.setText(searchCustomer.getBirthDate());

        return convertView;
    }

    private class ListSearchCustomerHolder {
        TextView customerName,birthDate;

    }
}
