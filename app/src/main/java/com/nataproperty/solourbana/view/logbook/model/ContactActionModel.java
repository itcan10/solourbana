package com.nataproperty.solourbana.view.logbook.model;

import java.util.ArrayList;

/**
 * Created by Nata on 2/16/2017.
 */

public class ContactActionModel {
    ArrayList<ProjectAction> projectAction;
    ArrayList <CustomerResponse> customerResponse;

    public ArrayList<ProjectAction> getProjectAction() {
        return projectAction;
    }

    public void setProjectAction(ArrayList<ProjectAction> projectAction) {
        this.projectAction = projectAction;
    }

    public ArrayList<CustomerResponse> getCustomerResponse() {
        return customerResponse;
    }

    public void setCustomerResponse(ArrayList<CustomerResponse> customerResponse) {
        this.customerResponse = customerResponse;
    }
}
