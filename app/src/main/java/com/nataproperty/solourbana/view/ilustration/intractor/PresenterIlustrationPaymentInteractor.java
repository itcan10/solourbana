package com.nataproperty.solourbana.view.ilustration.intractor;

import com.nataproperty.solourbana.view.ilustration.model.GenerateKalkulatorKPRParam;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterIlustrationPaymentInteractor {
    void getPayment(String dbMasterRef,String projectRef,String clusterRef,String productRef,String unitRef,String termRef,String termNo,String discRoom,String memberRef);
    void getPaymentKpr(GenerateKalkulatorKPRParam generateKalkulatorKPRParam);

}
