package com.nataproperty.solourbana.view.nup.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface NupInformasiInteractor {

    void GetQtyNUPList(String dbMasterRef, String projectRef, String memberRef);

    void getMemberInfoSvc(String memberRef);


}
