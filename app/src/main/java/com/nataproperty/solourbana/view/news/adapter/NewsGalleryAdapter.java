package com.nataproperty.solourbana.view.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.news.model.NewsDetailModel;
import com.nataproperty.solourbana.view.news.ui.NewsDetailImageActivity;

import java.util.List;

/**
 * Created by UserModel on 4/19/2016.
 */
public class NewsGalleryAdapter extends BaseAdapter {
    public static final String TAG = "NewsGalleryAdapter" ;

    public static final String PRODUCT_REF = "productRef";
    public static final String CONTENT_REF = "contentRef";
    public static final String IMAGE_REF = "imageRef";
    public static final String POSISION = "position";

    private Context context;
    private List<NewsDetailModel> list;
    private ListNewsDetailHolder holder;

    private Display display;

    public NewsGalleryAdapter(Context context, List<NewsDetailModel> list,Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_news_gallery,null);
            holder = new ListNewsDetailHolder();
            holder.imgNewsGallery = (ImageView) convertView.findViewById(R.id.img_item_list_news_gallery);

            convertView.setTag(holder);
        }else{
            holder = (ListNewsDetailHolder) convertView.getTag();
        }

        NewsDetailModel newsDetailModel = list.get(position);

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x/2;
        Double result = width/1.78125;

        ViewGroup.LayoutParams params = holder.imgNewsGallery.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.imgNewsGallery.setLayoutParams(params);
        holder.imgNewsGallery.requestLayout();

        Glide.with(context).load(newsDetailModel.getImageSlider())
                .into(holder.imgNewsGallery);
        Log.d(TAG,newsDetailModel.getImageSlider());

        holder.imgNewsGallery.setOnClickListener(new OnImageClickListener(position));

        return convertView;
    }

    class OnImageClickListener implements View.OnClickListener {

        int position;

        // constructor
        public OnImageClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
            Intent i = new Intent(context, NewsDetailImageActivity.class);
            i.putExtra(CONTENT_REF,list.get(position).getContentRef());
            i.putExtra(POSISION, position);
            context.startActivity(i);
        }

    }

    private class ListNewsDetailHolder {
        ImageView imgNewsGallery;
    }
}
