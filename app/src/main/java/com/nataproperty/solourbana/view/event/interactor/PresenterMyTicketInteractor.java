package com.nataproperty.solourbana.view.event.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyTicketInteractor {
    void getListTiketRsvp(String memberRef, String projectCode);

}
