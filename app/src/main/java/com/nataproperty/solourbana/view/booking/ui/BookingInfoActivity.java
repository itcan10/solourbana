package com.nataproperty.solourbana.view.booking.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.LoadingBar;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.booking.adapter.MoneySourceAdapter;
import com.nataproperty.solourbana.view.booking.adapter.SalesEventAdapter;
import com.nataproperty.solourbana.view.booking.adapter.SalesLocationAdapter;
import com.nataproperty.solourbana.view.booking.adapter.SalesPurposeAdapter;
import com.nataproperty.solourbana.view.booking.adapter.SalesReferalAdapter;
import com.nataproperty.solourbana.view.booking.model.MoneySourcelModel;
import com.nataproperty.solourbana.view.booking.model.SalesEventModel;
import com.nataproperty.solourbana.view.booking.model.SalesLocationModel;
import com.nataproperty.solourbana.view.booking.model.SalesPurposeModel;
import com.nataproperty.solourbana.view.booking.model.SalesReferalModel;
import com.nataproperty.solourbana.view.profile.adapter.CityAdapter;
import com.nataproperty.solourbana.view.profile.adapter.CountryAdapter;
import com.nataproperty.solourbana.view.profile.adapter.ProvinceAdapter;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.CountryModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookingInfoActivity extends AppCompatActivity {

    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String KTP_REF = "ktpRef";
    public static final String NPWP_REF = "npwpRef";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String KTP_ID = "ktpId";
    public static final String PROJECT_BOOKING_REF = "projectBookingRef";

    private String fullname, mobile1, phone1, email1, ktpid, ktpRef, npwpRef, address,va1, va2;
    private String dbMasterRef, projectRef, projectName, memberRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo, npwpid;

    //logo
    private ImageView imgLogo;
    private TextView txtProjectName;
    private LinearLayout choiceSalesEvent, choiceSalesLocation, choiceSalesReferral, choiceSalesPurpose, choiceMoneySource;
    private Spinner spinnerSalesEvent, spinnerSalesLocation, spinnerSalesReferral, spinnerSalesPurpose, spinnerMoneySource,spinnerCity,spinnerCountry,spinnerProvince;
    private Button btnNext;
    private EditText edtNote;

    private List<SalesEventModel> listSalesEvent = new ArrayList<>();
    private SalesEventAdapter salesEventAdapter;
    private String salesEvent, salesEventName;

    private List<SalesLocationModel> listSalesLocation = new ArrayList<>();
    private SalesLocationAdapter salesLocationAdapter;
    private String salesLocation, salesLocationName;

    private List<SalesReferalModel> listSalesReferal = new ArrayList<>();
    private SalesReferalAdapter salesReferalAdapter;
    private String salesReferral, salesReferralName;

    private List<MoneySourcelModel> listMoneySource = new ArrayList<>();
    private MoneySourceAdapter moneySourceAdapter;
    private String moneySourceName, moneySource;

    private List<SalesPurposeModel> listSalesPurpose = new ArrayList<>();
    private SalesPurposeAdapter salesPurposeAdapter;
    private String salesPurpose, salesPurposeName;
    SharedPreferences sharedPreferences;

    private List<CountryModel> listCounty = new ArrayList<CountryModel>();
    private CountryAdapter adapterCountry;
    private List<ProvinceModel> listProvince = new ArrayList<ProvinceModel>();
    private ProvinceAdapter adapterProvince;
    private List<CityModel> listCity = new ArrayList<CityModel>();
    private CityAdapter adapterCity;
    //private String idCountryCode = "", idProvinceCode = "", idCityCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Informasi Booking");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);

        fullname = intent.getStringExtra(FULLNAME);
        mobile1 = intent.getStringExtra(MOBILE);
        phone1 = intent.getStringExtra(PHONE);
        email1 = intent.getStringExtra(EMAIL);
        address = intent.getStringExtra(ADDRESS);
        ktpid = intent.getStringExtra(KTP_ID);
        ktpRef = intent.getStringExtra(KTP_REF);
        npwpid = intent.getStringExtra("NPWP");
        npwpRef = intent.getStringExtra(NPWP_REF);

        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");

//        idCountryCode = intent.getStringExtra("idCountryCode");
//        idProvinceCode = intent.getStringExtra("idProvinceCode");
//        idCityCode = intent.getStringExtra("idCityCode");

        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        choiceSalesEvent = (LinearLayout) findViewById(R.id.choice_sales_event);
        choiceSalesLocation = (LinearLayout) findViewById(R.id.choice_sales_location);
        choiceSalesReferral = (LinearLayout) findViewById(R.id.choice_sales_referal);
        choiceSalesPurpose = (LinearLayout) findViewById(R.id.choice_sales_purpose);
        choiceMoneySource = (LinearLayout) findViewById(R.id.choice_money_source);

        spinnerSalesEvent = (Spinner) findViewById(R.id.spn_sales_event);
        spinnerSalesLocation = (Spinner) findViewById(R.id.spn_sales_location);
        spinnerSalesReferral = (Spinner) findViewById(R.id.spn_sales_referral);
        spinnerSalesPurpose = (Spinner) findViewById(R.id.spn_sales_purpose);
        spinnerMoneySource = (Spinner) findViewById(R.id.spn_money_Source);

        edtNote = (EditText) findViewById(R.id.edit_note);
        btnNext = (Button) findViewById(R.id.btn_next);

        spinnerCountry = (Spinner) findViewById(R.id.spn_country);
        spinnerProvince = (Spinner) findViewById(R.id.spn_province);
        spinnerCity = (Spinner) findViewById(R.id.spn_city);

        salesEventAdapter = new SalesEventAdapter(this, listSalesEvent);
        spinnerSalesEvent.setAdapter(salesEventAdapter);
        spinnerSalesEvent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salesEvent = String.valueOf(listSalesEvent.get(position).getSalesEvent());
                salesEventName = String.valueOf(listSalesEvent.get(position).getSalesEventName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        salesLocationAdapter = new SalesLocationAdapter(this, listSalesLocation);
        spinnerSalesLocation.setAdapter(salesLocationAdapter);
        spinnerSalesLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salesLocation = String.valueOf(listSalesLocation.get(position).getSalesLocation());
                salesLocationName = String.valueOf(listSalesLocation.get(position).getSalesLocationName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        salesReferalAdapter = new SalesReferalAdapter(this, listSalesReferal);
        spinnerSalesReferral.setAdapter(salesReferalAdapter);
        spinnerSalesReferral.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salesReferral = String.valueOf(listSalesReferal.get(position).getSalesReferral());
                salesReferralName = String.valueOf(listSalesReferal.get(position).getSalesReferralName());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        salesPurposeAdapter = new SalesPurposeAdapter(this, listSalesPurpose);
        spinnerSalesPurpose.setAdapter(salesPurposeAdapter);
        spinnerSalesPurpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salesPurpose = String.valueOf(listSalesPurpose.get(position).getSalesPurpose());
                salesPurposeName = String.valueOf(listSalesPurpose.get(position).getSalesPurposeName());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        moneySourceAdapter = new MoneySourceAdapter(this, listMoneySource);
        spinnerMoneySource.setAdapter(moneySourceAdapter);
        spinnerMoneySource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                moneySource = String.valueOf(listMoneySource.get(position).getMoneySource());
                moneySourceName = String.valueOf(listMoneySource.get(position).getMoneySourceName());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BookingInfoActivity.this, BookingReviewOrderActivity.class);
                intent.putExtra(DBMASTER_REF, dbMasterRef);
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CATEGORY_REF, categoryRef);
                intent.putExtra(CLUSTER_REF, clusterRef);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(UNIT_REF, unitRef);
                intent.putExtra(TERM_REF, termRef);
                intent.putExtra(TERM_NO, termNo);
                intent.putExtra(PROJECT_NAME, projectName);

                intent.putExtra(FULLNAME, fullname);
                intent.putExtra(EMAIL, email1);
                intent.putExtra(MOBILE, mobile1);
                intent.putExtra(PHONE, phone1);
                intent.putExtra(ADDRESS, address);
                intent.putExtra(KTP_ID, ktpid);
                intent.putExtra(NPWP_REF, npwpRef);
                intent.putExtra(KTP_REF, ktpRef);

                intent.putExtra("NPWP", npwpid);
                intent.putExtra("va1", va1);
                intent.putExtra("va2", va2);

                intent.putExtra("salesEvent", salesEvent);
                intent.putExtra("salesEventName", salesEventName);
                intent.putExtra("salesLocation", salesLocation);
                intent.putExtra("salesLocationName", salesLocationName);
                intent.putExtra("salesReferral", salesReferral);
                intent.putExtra("salesReferralName", salesReferralName);
                intent.putExtra("salesPurpose", salesPurpose);
                intent.putExtra("salesPurposeName", salesPurposeName);
                intent.putExtra("moneySource", moneySource);
                intent.putExtra("moneySourceName", moneySourceName);
//                intent.putExtra("idCountryCode", idCountryCode);
//                intent.putExtra("idProvinceCode", idProvinceCode);
//                intent.putExtra("idCityCode", idCityCode);
                intent.putExtra("note", edtNote.getText().toString());
                startActivity(intent);
            }
        });

        getBookingInfo();
    }

    private void getBookingInfo() {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getLookupAdditionalInfoBooking(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    if (status == 200) {
                        JSONArray jsonSalesEvent = new JSONArray(jsonObject.getJSONArray("salesEvent").toString());
                        generateSalesEvent(jsonSalesEvent);

                        JSONArray jsonSalesLocation = new JSONArray(jsonObject.getJSONArray("salesLocation").toString());
                        generateSalesLocation(jsonSalesLocation);

                        JSONArray jsonSalesReferral = new JSONArray(jsonObject.getJSONArray("salesReferral").toString());
                        generateSalesReferral(jsonSalesReferral);

                        JSONArray jsonSalesPurpose = new JSONArray(jsonObject.getJSONArray("salesPurpose").toString());
                        generateSalesPurpose(jsonSalesPurpose);

                        JSONArray jsonMoneySource = new JSONArray(jsonObject.getJSONArray("moneySource").toString());
                        generateMoneySource(jsonMoneySource);
                    } else {
                        Toast.makeText(BookingInfoActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    LoadingBar.stopLoader();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingInfoActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingInfoActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("memberRef", memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "requestLookupSurveyUnitType");

    }

    private void generateSalesEvent(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonSalesEvent = response.getJSONObject(i);
                SalesEventModel salesEventModel = new SalesEventModel();
                salesEventModel.setSalesEvent(jsonSalesEvent.getString("salesEvent"));
                salesEventModel.setSalesEventName(jsonSalesEvent.getString("salesEventName"));
                listSalesEvent.add(salesEventModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        salesEventAdapter.notifyDataSetChanged();
        if (response.length() <= 1) {
            choiceSalesEvent.setVisibility(View.GONE);
            salesEventName = "";
        }
    }

    private void generateSalesLocation(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonSalesLocation = response.getJSONObject(i);
                SalesLocationModel salesLocationModel = new SalesLocationModel();
                salesLocationModel.setSalesLocation(jsonSalesLocation.getString("salesLocation"));
                salesLocationModel.setSalesLocationName(jsonSalesLocation.getString("salesLocationName"));
                listSalesLocation.add(salesLocationModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        salesLocationAdapter.notifyDataSetChanged();
        if (response.length() <= 1) {
            choiceSalesLocation.setVisibility(View.GONE);
            salesLocationName = "";
        }
    }

    private void generateSalesReferral(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonSalesReferral = response.getJSONObject(i);
                SalesReferalModel salesReferalModel = new SalesReferalModel();
                salesReferalModel.setSalesReferral(jsonSalesReferral.getString("salesReferral"));
                salesReferalModel.setSalesReferralName(jsonSalesReferral.getString("salesReferralName"));
                listSalesReferal.add(salesReferalModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        salesReferalAdapter.notifyDataSetChanged();
        if (response.length() <= 1) {
            choiceSalesReferral.setVisibility(View.GONE);
            salesReferralName = "";
        }
    }

    private void generateSalesPurpose(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonSalesPurpose = response.getJSONObject(i);
                SalesPurposeModel salesPurposeModel = new SalesPurposeModel();
                salesPurposeModel.setSalesPurpose(jsonSalesPurpose.getString("salesPurpose"));
                salesPurposeModel.setSalesPurposeName(jsonSalesPurpose.getString("salesPurposeName"));
                listSalesPurpose.add(salesPurposeModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        salesPurposeAdapter.notifyDataSetChanged();
        if (response.length() <= 1) {
            choiceSalesPurpose.setVisibility(View.GONE);
            salesPurposeName = "";
        }
    }

    private void generateMoneySource(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jsonMoneySource = response.getJSONObject(i);
                MoneySourcelModel moneySourcelModel = new MoneySourcelModel();
                moneySourcelModel.setMoneySource(jsonMoneySource.getString("moneySource"));
                moneySourcelModel.setMoneySourceName(jsonMoneySource.getString("moneySourceName"));
                listMoneySource.add(moneySourcelModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        moneySourceAdapter.notifyDataSetChanged();
        if (response.length() <= 1) {
            choiceMoneySource.setVisibility(View.GONE);
            moneySourceName = "";
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
