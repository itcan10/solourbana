package com.nataproperty.solourbana.view.ilustration.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;


public class FloorPlanActivity extends AppCompatActivity {
    String viewFloorPlan;
    PhotoView imageView;
    PhotoViewAttacher attacher;
    Toolbar toolbar;
    private DisplayMetrics mMetrics;
    ProgressBar progressBar;
    boolean shouldLoadAgain = false;
    TextView txtNoData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imageView = (PhotoView) findViewById(R.id.image);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtNoData = (TextView) findViewById(R.id.txt_no_data);

        viewFloorPlan = getIntent().getStringExtra(General.VIEW_FLOOR_PLAN);

        Log.d("Tag", "viewFloorPlan " + viewFloorPlan);

        attacher = new PhotoViewAttacher(imageView);
        attacher.update();

        loadPicture(viewFloorPlan, shouldLoadAgain);

    }

    private void loadPicture(String photoUrl, final Boolean shouldLoadAgain) {
        progressBar.setVisibility(View.VISIBLE);
        txtNoData.setVisibility(View.GONE);
        Glide.with(this)
                .load(photoUrl)
                .error(R.drawable.bg_black)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .crossFade()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        txtNoData.setVisibility(View.VISIBLE);
                        if (shouldLoadAgain)
                            loadPicture(viewFloorPlan, false);
                        return false;


                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        txtNoData.setVisibility(View.GONE);
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_close, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
