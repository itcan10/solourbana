package com.nataproperty.solourbana.view.ilustration.presenter;

import android.util.Log;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.ilustration.intractor.PresenterIlustrationPaymentInteractor;
import com.nataproperty.solourbana.view.ilustration.model.GenerateKalkulatorKPRParam;
import com.nataproperty.solourbana.view.ilustration.ui.IlustrationPaymentActivity;
import com.nataproperty.solourbana.view.kpr.model.PaymentCCStatusModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationPaymentPresenter implements PresenterIlustrationPaymentInteractor {
    private IlustrationPaymentActivity view;
    private ServiceRetrofit service;

    public IlustrationPaymentPresenter(IlustrationPaymentActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }
    @Override
    public void getPayment(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo,String discRoomPrice,String memberRef) {
        Call<PaymentCCStatusModel> call = service.getAPI().getIlustrationPaymentIncDiscSvc(dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo,discRoomPrice,memberRef);
        call.enqueue(new Callback<PaymentCCStatusModel>() {
            @Override
            public void onResponse(Call<PaymentCCStatusModel> call, Response<PaymentCCStatusModel> response) {
                view.showPaymentResults(response);
            }

            @Override
            public void onFailure(Call<PaymentCCStatusModel> call, Throwable t) {
                view.showPaymentFailure(t);

            }


        });
    }

    @Override
    public void getPaymentKpr(GenerateKalkulatorKPRParam generateKalkulatorKPRParam) {
        Call<PaymentCCStatusModel> call = service.getAPI().generateKalkulatorKPR(Utils.modelToJson(generateKalkulatorKPRParam));
        Log.d("Json_IN",Utils.modelToJson(generateKalkulatorKPRParam));
        call.enqueue(new Callback<PaymentCCStatusModel>() {
            @Override
            public void onResponse(Call<PaymentCCStatusModel> call, Response<PaymentCCStatusModel> response) {
                view.showPaymentKprResults(response);
            }

            @Override
            public void onFailure(Call<PaymentCCStatusModel> call, Throwable t) {
                view.showPaymentKprFailure(t);

            }


        });
    }



}
