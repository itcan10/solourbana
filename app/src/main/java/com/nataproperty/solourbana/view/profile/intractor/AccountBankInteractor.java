package com.nataproperty.solourbana.view.profile.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface AccountBankInteractor {
    void getListBankSvc();

    void updateAccountBankSvc(String memberRef, String bankRef, String bankBranch, String accName, String accNo);

}
