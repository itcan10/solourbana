package com.nataproperty.solourbana.view.ilustration.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface FloorPlanInteractor {
    void getUnitStatusSVC(String jsonIn, String unitRef, String productRef);

}
