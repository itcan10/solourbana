package com.nataproperty.solourbana.view.mybooking.presenter;

import com.nataproperty.solourbana.view.mybooking.intractor.PresenterMyBookingProjectPsInteractor;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingModel;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.mybooking.ui.MyBookingProjectPsActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookingProjectPsPresenter implements PresenterMyBookingProjectPsInteractor {
    private MyBookingProjectPsActivity view;
    private ServiceRetrofit service;

    public MyBookingProjectPsPresenter(MyBookingProjectPsActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListBookingProjectPs(String dbMasterRef, String projectRef, String projectPsRef) {
        Call<List<MyBookingModel>> call = service.getAPI().getListBookingProjectPs(dbMasterRef,projectRef,projectPsRef);
        call.enqueue(new Callback<List<MyBookingModel>>() {
            @Override
            public void onResponse(Call<List<MyBookingModel>> call, Response<List<MyBookingModel>> response) {
                view.showListBookingProjectPsResults(response);
            }

            @Override
            public void onFailure(Call<List<MyBookingModel>> call, Throwable t) {
                view.showListBookingProjectPsFailure(t);

            }


        });
    }

}
