package com.nataproperty.solourbana.view.nup.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class VeritransNupVTModel {
    int status;
    String message, redirectUrl;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
