package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.booking.model.BookingPaymentMethodModel;

import java.util.List;

/**
 * Created by UserModel on 5/14/2016.
 */
public class NupPaymentMethodAdapter extends BaseAdapter {
    public static final String TAG = "NupPaymentMethod";
    private Context context;
    private List<BookingPaymentMethodModel> list;
    private ListPaymentHolder holder;

    public NupPaymentMethodAdapter(Context context, List<BookingPaymentMethodModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_booking_payment_method, null);
            holder = new ListPaymentHolder();
            holder.paymentMethod = (TextView) convertView.findViewById(R.id.txt_payment_method);
            holder.paymentMethodTerm = (TextView) convertView.findViewById(R.id.txt_payment_method_term);
            holder.bulet = (TextView) convertView.findViewById(R.id.txt_bulet);
            convertView.setTag(holder);
        } else {
            holder = (ListPaymentHolder) convertView.getTag();
        }

        BookingPaymentMethodModel bookingPaymentMethodModel = list.get(position);
        holder.paymentMethod.setText(bookingPaymentMethodModel.getPaymentMethod());
        StringBuilder builder = new StringBuilder();
        for (String details : bookingPaymentMethodModel.getData()) {
            builder.append("•");
            builder.append(details + "\n");
        }
        holder.paymentMethodTerm.setText(builder.toString());

        return convertView;
    }

    private class ListPaymentHolder {
        TextView paymentMethod, paymentMethodTerm, bulet;
    }
}
