package com.nataproperty.solourbana.view.profile.model;

/**
 * Created by UserModel on 5/15/2016.
 */
public class TypeModel {
    String typeRef;
    String typeName;
    String typeSortNo;

    public String getTypeRef() {
        return typeRef;
    }

    public void setTypeRef(String typeRef) {
        this.typeRef = typeRef;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeSortNo() {
        return typeSortNo;
    }

    public void setTypeSortNo(String typeSortNo) {
        this.typeSortNo = typeSortNo;
    }
}
