package com.nataproperty.solourbana.view.before_login.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.view.loginregister.ui.LoginActivity;
import com.nataproperty.solourbana.view.scanQRCode.ui.ScanQRActivity;


/**
 * Created by User on 4/27/2016.
 */
public class ViewpagerActivity extends Activity {
    private Button btn_customer;
    private Button btn_agent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);

        btn_agent = (Button) findViewById(R.id.btn_agent);
        btn_customer = (Button) findViewById(R.id.btn_customer);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");

        btn_agent.setTypeface(font);
        btn_agent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewpagerActivity.this, LoginActivity.class)
                        .putExtra(General.INTENT_CAT_LOGIN, General.LOGIN_GENERAL));
            }
        });

        btn_customer.setTypeface(font);
        btn_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewpagerActivity.this, LoginActivity.class)
                        .putExtra(General.INTENT_CAT_LOGIN, General.LOGIN_CUSTOMER);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(ViewpagerActivity.this, ScanQRActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getBaseContext(), "We don't have the Camera Permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}
