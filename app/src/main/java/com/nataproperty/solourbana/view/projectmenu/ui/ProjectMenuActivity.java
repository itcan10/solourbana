package com.nataproperty.solourbana.view.projectmenu.ui;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.config.SessionManager;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.gcm.GCMRegistrationIntentService;
import com.nataproperty.solourbana.helper.GridItemDivider;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.helper.Utils;
import com.nataproperty.solourbana.view.before_login.ui.LaunchActivity;
import com.nataproperty.solourbana.view.menuitem.model.StatusTokenGCMModel;
import com.nataproperty.solourbana.view.menuitem.ui.AboutUsActivity;
import com.nataproperty.solourbana.view.menuitem.ui.ChangePasswordActivity;
import com.nataproperty.solourbana.view.menuitem.ui.ContactUsActivity;
import com.nataproperty.solourbana.view.menuitem.ui.NotificationActivity;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingSectionModel;
import com.nataproperty.solourbana.view.mynup.model.MyNupSectionModel;
import com.nataproperty.solourbana.view.profile.ui.SectionProfileActivity;
import com.nataproperty.solourbana.view.profile.ui.SettingNotificationActivity;
import com.nataproperty.solourbana.view.project.model.CommisionStatusModel;
import com.nataproperty.solourbana.view.project.model.DetailStatusModel;
import com.nataproperty.solourbana.view.project.ui.ValidateInhouseCode;
import com.nataproperty.solourbana.view.projectmenu.adapter.ProjectMenuAdapter;
import com.nataproperty.solourbana.view.projectmenu.model.ProjectMenuModel;
import com.nataproperty.solourbana.view.projectmenu.model.ProjectVareable;
import com.nataproperty.solourbana.view.projectmenu.presenter.ProjectMenuPresenter;
import com.nataproperty.solourbana.view.scanQRCode.ui.ScanQRActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.numColumns;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.nataproperty.solourbana.config.General.IS_GUEST;
import static com.nataproperty.solourbana.config.General.MEMBER_CODE;
import static com.nataproperty.solourbana.config.General.PS_CODE;

/**
 * Created by UserModel on 5/3/2016.
 */
public class ProjectMenuActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    public static final String TAG = "ProjectMenuActivity";
    public static final String PREF_NAME = "pref";
    private String bookingCountNew;
    private ImageView imageView;
    private Context context;
    private long dbMasterRef;
    private String projectRef, projectName, locationName, locationRef, sublocationName, sublocationRef;
    private String countCategory, countCluster, salesStatus, isBooking, isNUP, isShowAvailableUnit;
    private String categoryRef, clusterRef, memberCode, projectDescription;
    private String nupAmt, linkDetail, bookingContact = "", downloadProjectInfo, clusterName, priceRange, countPromo;
    private String projectWA,projectEmail,projectPhone;
    public String isGuest, psCode;
    private double latitude, longitude;
    private ServiceRetrofit service;
    private ServiceRetrofitProject serviceProject;
    private ProjectMenuPresenter presenter;
    private ProgressDialog progressDialog;
    private static ProjectMenuActivity projectMenuActivity;
    private String imageLogo, urlVideo = "", memberRef, memberTypeCode, name, memberType;
    public String userRef;
    private ImageView btnJoin, btnJoined, btnWaiting;
    private SharedPreferences sharedPreferences;
    private String commision, closingFee, description, isWaiting = "", isJoin = "";
    private double commissionAmt;
    private Toolbar toolbar;
    private MyTextViewLatoReguler title;
    private Typeface font;
    private RelativeLayout rPage;
    private Display display;
    private Integer width;
    private Double result, widthButton, heightButton;
    private EditText edtMemberCode;
    private TextView txtTermMemberCode;
    private AlertDialog alertDialogJoin, b;
    private LinearLayout linearLayout;
    private RecyclerView recyclerViewMenu;
    private ProjectMenuAdapter projectMenuAdapter;
    private TextView txtName, txtMemberTypeName;

    List<MyNupSectionModel> listMyNupSection = new ArrayList<>();
    Integer nupCount = 0;
    List<MyBookingSectionModel> listMyBookingSection = new ArrayList<>();
    Integer bookingCount = 0;

    private Integer flagg = 0;

    // list icon menu
    int menuIcon[] = new int[]{
            R.drawable.selector_project_menu_information,
            R.drawable.selector_project_menu_search_unit,
            R.drawable.selector_project_menu_download,
            R.drawable.selector_project_menu_nup,
            R.drawable.selector_project_menu_ilustration,
            R.drawable.selector_project_menu_commision,
            R.drawable.selector_project_menu_gallery,
            R.drawable.selector_project_menu_news,
            R.drawable.selector_project_menu_promo,
            R.drawable.selector_project_menu_event,
            R.drawable.selector_project_menu_my_nup,
            R.drawable.selector_project_menu_my_booking,
            R.drawable.selector_project_menu_qrcode,
//            R.drawable.selector_project_menu_ticket
            //tambah image ticket edit selector
    };

    // list title menu
    String menuTitle[] = new String[]{
            General.PROJECT_INFORMASI,
            General.PROJECT_SEARCH_UNIT,
            General.PROJECT_DOWNLOAD,
            General.PROJECT_NUP,
            General.PROJECT_BOOKING,
            General.PROJECT_KOMISI,
            General.PROJECT_GALLERY,
            General.PROJECT_NEWS,
            General.PROJECT_PROMO,
            General.PROJECT_EVENT,
            General.PROJECT_MYNUP,
            General.PROJECT_MYBOOKING,
            General.PROJECT_QRCODE,
//            General.PROJECT_TICKET
    };

    int menuName[] = new int[]{
            R.string.projectInformasi,
            R.string.projectProduct,
            R.string.projectDownload,
            R.string.projectNup,
            R.string.projectBooking,
            R.string.projectKomisi,
            R.string.projectGallery,
            R.string.projectNews,
            R.string.projectPromo,
            R.string.projectEvent,
            R.string.projectMyNup,
            R.string.projectMyBooking,
            R.string.projectScanQr,
            R.string.projectTicket
    };
    private List<ProjectMenuModel> menulist = new ArrayList<>();
    private RecyclerViewHeader header;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projek_menu);
        service = ((BaseApplication) getApplication()).getNetworkService();
        serviceProject = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new ProjectMenuPresenter(this, service, serviceProject);
        sessionManager = new SessionManager(this);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        projectMenuActivity = this;
        context = this;

        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", "");
        name = sharedPreferences.getString("isName", "");
        memberTypeCode = sharedPreferences.getString("isMemberTypeCode", "");
        initWidget();
        requestProjectDetail();
        initAdapter();
        komisi();
        logBookSetup();
        buidNewGoogleApiClient();
        serviceGCM();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));

        if (flagg==1){

            name = sharedPreferences.getString("isName", "");
            memberType = sharedPreferences.getString("isMemberType", "");
            bookingCountNew = sharedPreferences.getString("bookingCount", "");

            sessionManager = new SessionManager(this);
            HashMap<String, String> projectSession = sessionManager.getProjectSession();
            isGuest = projectSession.get(IS_GUEST);
            psCode = projectSession.get(PS_CODE);
            memberCode = projectSession.get(MEMBER_CODE);

            if (memberTypeCode.equals(General.customer)) {
                if (bookingCountNew.equals("0")) {
                    txtMemberTypeName.setText("Guest");
                    txtName.setText(name+" ("+memberCode+")");
                } else {
                    txtMemberTypeName.setText("Customer");
                    txtName.setText(name+" ("+psCode+")");
                }
            } else { // login Agent
                txtMemberTypeName.setText(memberType);
                txtName.setText(name+" ("+memberCode+")");
            }
        }
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setIcon(R.drawable.icon_mini_carstenz);
        imageView = (ImageView) findViewById(R.id.image_projek);

        title.setText(getString(R.string.app_name_singkat));

        rPage = (RelativeLayout) findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;

        widthButton = size.x / 3.0;
        heightButton = widthButton / 1.0;
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        txtName = (TextView) findViewById(R.id.name);
        txtMemberTypeName = (TextView) findViewById(R.id.memberTypeName);

        btnJoin = (ImageView) findViewById(R.id.btn_join);
        btnJoined = (ImageView) findViewById(R.id.btn_joined);
        btnWaiting = (ImageView) findViewById(R.id.btn_waiting);
        //btnUnJoin = (ImageView) findViewById(R.id.ic_unjoined);

        recyclerViewMenu = (RecyclerView) findViewById(R.id.listMenu);
    }

    private void komisi() {
        if (memberTypeCode.equals(General.customer)) {
//            menulist.remove(5);
//            projectMenuAdapter.notifyDataSetChanged();
            remove(General.PROJECT_KOMISI);
            projectMenuAdapter.notifyDataSetChanged();
        }

    }

    //lookbook blm kepakai
    private void logBookSetup() {
        if (memberTypeCode.equals(General.inhouse)) {
            ProjectMenuModel mm;
            mm = new ProjectMenuModel();
            mm.setMenuIcon(R.drawable.selector_project_menu_log_book);
            mm.setMenuTitle(General.PROJECT_LOOK_BOOK);
            mm.setMenuName(R.string.projectLookBook);

            menulist.add(mm);
        }
        projectMenuAdapter.notifyDataSetChanged();
    }

    private void initAdapter() {
        projectMenuAdapter = new ProjectMenuAdapter(this, menulist, display);
        recyclerViewMenu.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewMenu.setHasFixedSize(true);
        recyclerViewMenu.setAdapter(projectMenuAdapter);

        for (int i = 0; i < menuIcon.length; i++) {
            ProjectMenuModel projectMenuModel = new ProjectMenuModel();
            projectMenuModel.setMenuIcon(menuIcon[i]);
            projectMenuModel.setMenuTitle(menuTitle[i]);
            projectMenuModel.setMenuName(menuName[i]);

            menulist.add(projectMenuModel);
        }
        projectMenuAdapter.notifyDataSetChanged();

        header = (RecyclerViewHeader) findViewById(R.id.header);
        header.attachTo(recyclerViewMenu);
        recyclerViewMenu.addItemDecoration(new GridItemDivider(numColumns, Utils.dpToPx(this, 8), false));
    }

    public static ProjectMenuActivity getInstance() {
        return projectMenuActivity;
    }

    private void requestProjectDetail() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        //untuk ambil data baru dari project
        presenter.getProjectDetailSvc(General.projectCode, memberRef);
    }

    private void requestCommision() {
        presenter.getCommision(String.valueOf(dbMasterRef), projectRef, memberRef);
    }

    public void showMenuResults(retrofit2.Response<DetailStatusModel> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            if (status == 200) {
                dbMasterRef = response.body().getData().getDbMasterRef();
                projectRef = response.body().getData().getProjectRef();
                projectName = response.body().getData().getProjectName();
                isJoin = response.body().getData().getIsJoin();
                isWaiting = response.body().getData().getIsWaiting();
                projectDescription = response.body().getData().getProjectDescription();
                countCategory = response.body().getData().getCountCategory();
                countCluster = response.body().getData().getCountCluster();
                categoryRef = response.body().getData().getCategoryRef();
                clusterRef = response.body().getData().getClusterRef();
                salesStatus = response.body().getData().getSalesStatus();
                isBooking = response.body().getData().getIsBooking();
                isNUP = response.body().getData().getIsNUP();
                isShowAvailableUnit = response.body().getData().getIsShowAvailableUnit();
                latitude = response.body().getData().getLatitude();
                longitude = response.body().getData().getLongitude();
                nupAmt = response.body().getData().getNupAmt();
                linkDetail = response.body().getData().getLinkProjectDetail();
                urlVideo = response.body().getData().getUrlVideo();
                imageLogo = response.body().getData().getImageLogo();
                bookingContact = response.body().getData().getBookingContact();
                downloadProjectInfo = response.body().getData().getDownloadProjectInfo();
                clusterName = response.body().getData().getClusterDescription();
                priceRange = response.body().getData().getPriceRange();
                countPromo = response.body().getData().getCountPromo();
                memberCode = response.body().getData().getMemberCode();
                txtName.setText(name+" ("+memberCode+")");
                projectWA = response.body().getData().getProjectWhatsapp();
                projectEmail = response.body().getData().getProjectEmail();
                projectPhone = response.body().getData().getProjectPhone();
                isGuest = response.body().getData().getIsGuest();
                psCode = response.body().getData().getPsCode();

                //ambil dbmaster ref sama projectref
                Glide.with(this)
                        .load(imageLogo)
                        .skipMemoryCache(true)
                        .into(imageView);

                requestCommision();
                requestSectionNup();
                requestSectionBooking();

                joinSetup();

                sessionManager.sessionProject(isJoin, isWaiting, String.valueOf(dbMasterRef), projectRef, projectName, countCategory, countCluster, clusterRef, salesStatus,
                        isBooking, isNUP, isShowAvailableUnit, Double.toString(latitude), Double.toString(longitude), nupAmt,
                        linkDetail, urlVideo, imageLogo, bookingContact, downloadProjectInfo, clusterName, categoryRef, priceRange,projectWA,projectEmail,projectPhone,
                        psCode,memberCode);

                if (salesStatus.equals(General.salesStatusOpen)) {
                    if (isBooking.equals("0")) {
                        remove(General.PROJECT_BOOKING);
                    }

                    if (isNUP.equals("0")) {
                        remove(General.PROJECT_NUP);
                    }
                } else {
                    for (int i = 0; i < menulist.size(); i++) {
                        if (menulist.get(i).getMenuTitle().equals(General.PROJECT_BOOKING)) { //change here
                            menulist.remove(i);
                            projectMenuAdapter.notifyDataSetChanged();
                            Log.d("TAG", "i " + i);
                            break;
                        }
                    }

                    for (int j = 0; j < menulist.size(); j++) {
                        if (menulist.get(j).getMenuTitle().equals(General.PROJECT_NUP)) { //change here
                            menulist.remove(j);
                            projectMenuAdapter.notifyDataSetChanged();
                            Log.d("TAG", "i " + j);
                            break;
                        }
                    }
                }

                if (countPromo.trim().equals("0")) {
                    remove(General.PROJECT_PROMO);
                }

            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                ProjectMenuActivity.this.recreate();
            }

        } else {
            //jika data tidak dapat dari service
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProjectMenuActivity.this);
            alertDialogBuilder
                    .setMessage("Terjadi kesalahan, silahkan coba lagi")
                    .setCancelable(false)
                    .setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(ProjectMenuActivity.this, LaunchActivity.class));
                            finish();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            if (!(ProjectMenuActivity.this).isFinishing()) {
                alertDialog.show();
            }
        }

    }

    public void showMenuFailure(Throwable t) {
        progressDialog.dismiss();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProjectMenuActivity.this);
        alertDialogBuilder
                .setMessage("Terjadi kesalahan koneksi, silahkan coba lagi")
                .setCancelable(false)
                .setPositiveButton("Coba Lagi", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(ProjectMenuActivity.this, LaunchActivity.class));
                        finish();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        if (!(ProjectMenuActivity.this).isFinishing()) {
            alertDialog.show();
        }
    }

    public void requestSectionNup() {
//        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListNupSection(memberRef.toString(), General.projectCode);
    }

    public void showListNupSectionResults(retrofit2.Response<List<MyNupSectionModel>> response) {
//        progressDialog.dismiss();
        listMyNupSection = response.body();

        for(Integer i = 0;i<listMyNupSection.size(); i++){
            nupCount = nupCount + Integer.parseInt(listMyNupSection.get(i).getNupCount());
        }

        if(nupCount == 0){
            remove(General.PROJECT_MYNUP);
            projectMenuAdapter.notifyDataSetChanged();
        }
    }

    public void showListNupSectionFailure(Throwable t) {
//        progressDialog.dismiss();
        Toast.makeText(ProjectMenuActivity.this, getString(R.string.error_connection), Toast.LENGTH_SHORT).show();
    }

    public void requestSectionBooking() {
        Log.d("TAG",memberRef);
//        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListBookingSection(memberRef.toString(), General.projectCode);
    }

    public void showListBookingSectionResults(retrofit2.Response<List<MyBookingSectionModel>> response) {
//        progressDialog.dismiss();
        listMyBookingSection = response.body();

        for(Integer i = 0;i<listMyBookingSection.size(); i++){
            bookingCount = bookingCount + Integer.parseInt(listMyBookingSection.get(i).getBookingCount());
        }

        if(bookingCount == 0){
            remove(General.PROJECT_MYBOOKING);
            projectMenuAdapter.notifyDataSetChanged();
        }

        bookingCountNew = Integer.toString(bookingCount);
        SharedPreferences sharedPreferences = ProjectMenuActivity.this.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("bookingCount", bookingCountNew);
        editor.commit();

        flagg = 1;

        if (memberTypeCode.equals(General.customer)) {
            if (bookingCount == 0) {
                txtMemberTypeName.setText("Guest");
                txtName.setText(name+" ("+memberCode+")");
            } else {
                txtMemberTypeName.setText("Customer");
                txtName.setText(name+" ("+psCode+")");
            }
        } else { // login Agent
            memberType = sharedPreferences.getString("isMemberType", "");
            txtMemberTypeName.setText(memberType);
            txtName.setText(name+" ("+memberCode+")");
        }
    }

    public void showListBookingSectionFailure(Throwable t) {
//        progressDialog.dismiss();
        Toast.makeText(ProjectMenuActivity.this, getString(R.string.error_connection), Toast.LENGTH_SHORT).show();
    }

    private void joinSetup() {
        if (isJoin.equals("0")) {
            if (isWaiting.equals("1")) {
                btnJoined.setVisibility(GONE);
                btnWaiting.setVisibility(VISIBLE);
                btnJoin.setVisibility(GONE);
            } else {
                btnJoined.setVisibility(GONE);
                btnWaiting.setVisibility(GONE);
                btnJoin.setVisibility(VISIBLE);
            }

        } else {
            btnJoined.setVisibility(VISIBLE);
            btnWaiting.setVisibility(GONE);
            btnJoin.setVisibility(GONE);
        }

        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinProject();
            }
        });

       /* btnUnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unSubscribe(String.valueOf(dbMasterRef), projectRef);
            }
        });*/
    }

    public void dialogNotJoin(String statusJoin) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProjectMenuActivity.this);
        LayoutInflater inflater = ProjectMenuActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView textView = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Notifikasi");
        textView.setText(statusJoin);
        dialogBuilder.setPositiveButton("GO TO PROJECT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        b = dialogBuilder.create();
        b.show();
    }

    public void showCommisionResults(retrofit2.Response<CommisionStatusModel> response) {
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            if (status == 200) {
                commision = response.body().getData().getCommision();
                closingFee = response.body().getData().getClosingFee();
                description = response.body().getData().getDescription();
                commissionAmt = response.body().getData().getCommissionAmt();
                if (Double.parseDouble(commision) == 0) {
                    if (commissionAmt == 0.0) {
                        remove(General.PROJECT_KOMISI);
                        projectMenuAdapter.notifyDataSetChanged();
                    }
                } else if (commissionAmt == 0.0) {
                    if (Double.parseDouble(commision) == 0) {
                        remove(General.PROJECT_KOMISI);
                        projectMenuAdapter.notifyDataSetChanged();
                    }
                }

                int statusReport = response.body().getDataReport().getStatus();
                int isView = response.body().getDataReport().getIsView();
                userRef = response.body().getDataReport().getUserRef();
                projectMenuAdapter.notifyDataSetChanged();
                ProjectVareable.getInstance().setUserRef(userRef);

                BaseApplication.showLog(TAG, "statusReport:" + statusReport + " isView:" + isView + " userRef" + userRef);
                //Add btn report
//                if (statusReport == 200 && isView > 0 && memberTypeCode.equals(General.inhouse) && isJoin.equals("1")) {
//                    ProjectMenuModel mm = new ProjectMenuModel();
//                    mm.setMenuIcon(R.drawable.selector_project_menu_report);
//                    mm.setMenuTitle(General.PROJECT_REPORT);
//                    mm.setMenuName(R.string.projectReport);
//                    menulist.add(mm);
//                    projectMenuAdapter.notifyDataSetChanged();
//                }

            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

        }

    }

    public void showCommisionFailure(Throwable t) {
        //btnInformation.setEnabled(false);
    }

    private void joinProject() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProjectMenuActivity.this);
        LayoutInflater inflater = ProjectMenuActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_insert_member_code, null);
        dialogBuilder.setView(dialogView);
        edtMemberCode = (EditText) dialogView.findViewById(R.id.edt_member_code);
        txtTermMemberCode = (TextView) dialogView.findViewById(R.id.txt_term_member_code);
        dialogBuilder.setMessage("Masukkan Member ID");
        txtTermMemberCode.setText("Kosongkan jika tidak memiliki Member ID di " + projectName);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //subscribeProject();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        alertDialogJoin = dialogBuilder.create();
        alertDialogJoin.setCancelable(false);
        alertDialogJoin.show();
        Button positiveButton = alertDialogJoin.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memberCode = edtMemberCode.getText().toString();
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                final StringRequest request = new StringRequest(Request.Method.POST,
                        WebService.subscribeProject(), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.dismiss();
                            JSONObject jo = new JSONObject(response);
                            int status = jo.getInt("status");
                            String message = jo.getString("message");

                            if (status == 200) {
                                alertDialogJoin.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                ProjectMenuActivity.this.recreate();
                            } else if (status == 202) {
                                alertDialogJoin.dismiss();
                                Intent intent = new Intent(context, ValidateInhouseCode.class);
                                intent.putExtra("memberRef", memberRef);
                                intent.putExtra("dbMasterRef", String.valueOf(dbMasterRef));
                                intent.putExtra("projectRef", projectRef);
                                intent.putExtra("memberCode", edtMemberCode.getText().toString());
                                context.startActivity(intent);
                            } else if (status == 203) {
                                alertDialogJoin.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                ProjectMenuActivity.this.recreate();

                            } else if (status == 204) {
                                edtMemberCode.setError(message);
                            } else {
                                String error = jo.getString("message");
                                Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                NetworkResponse networkResponse = error.networkResponse;
                                if (networkResponse != null) {
                                    Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                }
                                if (error instanceof TimeoutError) {
                                    Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                                    Log.d("Volley", "TimeoutError");
                                } else if (error instanceof NoConnectionError) {
                                    Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                                    Log.d("Volley", "NoConnectionError");
                                } else if (error instanceof AuthFailureError) {
                                    Log.d("Volley", "AuthFailureError");
                                } else if (error instanceof ServerError) {
                                    Log.d("Volley", "ServerError");
                                } else if (error instanceof NetworkError) {
                                    Log.d("Volley", "NetworkError");
                                } else if (error instanceof ParseError) {
                                    Log.d("Volley", "ParseError");
                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("memberRef", memberRef);
                        params.put("dbMasterRef", String.valueOf(dbMasterRef));
                        params.put("projectRef", projectRef);
                        params.put("memberCode", edtMemberCode.getText().toString());

                        return params;
                    }
                };

                BaseApplication.getInstance().addToRequestQueue(request, "project");

            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_profile:
                startActivity(new Intent(ProjectMenuActivity.this, SectionProfileActivity.class));
                return true;

            case R.id.btn_change_password:
                startActivity(new Intent(ProjectMenuActivity.this, ChangePasswordActivity.class));
                return true;

            case R.id.btn_logout:
                dialogLogout();
                return true;

            case R.id.btn_contact_us:
                startActivity(new Intent(ProjectMenuActivity.this, ContactUsActivity.class));
                return true;

            case R.id.btn_about_us:
                startActivity(new Intent(ProjectMenuActivity.this, AboutUsActivity.class));
                return true;

            case R.id.btn_setting:
                startActivity(new Intent(ProjectMenuActivity.this, SettingNotificationActivity.class));
                return true;

            case R.id.btn_notification:
                startActivity(new Intent(ProjectMenuActivity.this, NotificationActivity.class));
                return true;

            case R.id.btn_exit:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dialogLogout() {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah anda ingin logout?");
        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPreferences sharedPreferences = ProjectMenuActivity.this.
                                getSharedPreferences(General.PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", false);
                        editor.putString("isEmail", "");
                        editor.putString("isName", "");
                        editor.putString("isMemberRef", "");
                        editor.putString("isMemberType", "");
                        editor.commit();

                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(ProjectMenuActivity.this, LaunchActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();

                                    }
                                });
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (alertDialogJoin != null && alertDialogJoin.isShowing()) {
            alertDialogJoin.dismiss();
        }

        if (b != null && b.isShowing()) {
            b.dismiss();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    private void buidNewGoogleApiClient() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, ProjectMenuActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void cekPermissionCamera() {
        if (ContextCompat.checkSelfPermission(ProjectMenuActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ProjectMenuActivity.this, new String[]{Manifest.permission.CAMERA}, 0);
        } else {
            startActivity(new Intent(ProjectMenuActivity.this, ScanQRActivity.class));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(ProjectMenuActivity.this, ScanQRActivity.class));
            } else {

            }
        }
    }

    private void serviceGCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    //Registration success
                    String token = intent.getStringExtra("token");
                    Log.d("GCMToken", token);
                    saveGCMTokenToServer(token);

                } else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)) {

                } else {

                }
            }
        };

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                //So notification
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);

        }
    }

    //savegcmtoken
    private void saveGCMTokenToServer(final String GCMToken) {
        presenter.saveGCMTokenProjectSvc(GCMToken, memberRef, General.projectCode);
        Log.d("GCMToken", GCMToken);
    }

    public void showSendGCMTokenResults(retrofit2.Response<StatusTokenGCMModel> responseToken) {
        if (responseToken.isSuccessful()) {
            int Status = responseToken.body().getStatus();
            String message = responseToken.body().getMessage();
            if (Status == 200) {
                Log.d("saveToken", message);
            } else {
                Log.d("saveToken", message);
            }
        }
    }

    public void showSendGCMTokenFailure(Throwable t) {
        Snackbar snackbar = Snackbar
                .make(linearLayout, "OFFLINE MODE", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(ProjectMenuActivity.this, LaunchActivity.class));
                        finish();
                    }
                });

        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    private void remove(String value) {
        for (int i = 0; i < menulist.size(); i++) {
            if (menulist.get(i).getMenuTitle().equals(value)) { //change here
                menulist.remove(i);
                projectMenuAdapter.notifyDataSetChanged();
                break;
            }
        }
    }
}