package com.nataproperty.solourbana.view.mybooking.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.mybooking.adapter.MyBookingSectionAdapter;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingSectionModel;
import com.nataproperty.solourbana.view.mybooking.presenter.MyBookingSectionPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class MyBookingSectionActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    Toolbar toolbar;
    TextView title;
    ListView listView;
    SharedPreferences sharedPreferences;
    String memberRef,projectRef,dbMasterRef,projectPsRef;
    ProgressDialog progressDialog;

    MyBookingSectionPresenter presenter;
    ServiceRetrofitProject service;
    boolean rxCallInWorks = false;

    MyBookingSectionAdapter adapter;
    List<MyBookingSectionModel> listMyBookingSection = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_section);
        service = ((BaseApplication) getApplication()).getNetworkServiceProject();
        presenter = new MyBookingSectionPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        requestSectionBooking();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                projectRef = listMyBookingSection.get(position).getProjectRef();
                dbMasterRef = listMyBookingSection.get(position).getDbMasterRef();
                projectPsRef = listMyBookingSection.get(position).getProjectPsRef();

                if (projectRef.equals("0")){
                    startActivity(new Intent(MyBookingSectionActivity.this,MyBookingActivity.class));
                } else {
                    Intent intent = new Intent(MyBookingSectionActivity.this,MyBookingProjectPsActivity.class);
                    intent.putExtra("dbMasterRef",dbMasterRef);
                    intent.putExtra("projectRef",projectRef);
                    intent.putExtra("projectPsRef",projectPsRef);
                    startActivity(intent);
                }

            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("My Booking");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        listView = (ListView) findViewById(R.id.list_mybooking_section);
    }

    public void requestSectionBooking() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListBookingSection(memberRef.toString(), General.projectCode);
    }

    public void showListBookingSectionResults(Response<List<MyBookingSectionModel>> response) {
        progressDialog.dismiss();
        listMyBookingSection = response.body();
        initAdapter();

    }

    public void showListBookingSectionFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(MyBookingSectionActivity.this, getString(R.string.error_connection), Toast.LENGTH_SHORT).show();
    }

    private void initAdapter() {
        adapter = new MyBookingSectionAdapter(MyBookingSectionActivity.this, listMyBookingSection);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingSectionActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
