package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by User on 11/3/2016.
 */
public class SalesEventModel {
    String salesEvent;
    String salesEventName;

    public String getSalesEvent() {
        return salesEvent;
    }

    public void setSalesEvent(String salesEvent) {
        this.salesEvent = salesEvent;
    }

    public String getSalesEventName() {
        return salesEventName;
    }

    public void setSalesEventName(String salesEventName) {
        this.salesEventName = salesEventName;
    }
}
