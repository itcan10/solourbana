package com.nataproperty.solourbana.view.kpr.model;

/**
 * Created by nata on 11/23/2016.
 */

public class PaymentKprStatusModel {
    int status;
    String message;
    PaymentKprModel dataPrice;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PaymentKprModel getDataPrice() {
        return dataPrice;
    }

    public void setDataPrice(PaymentKprModel dataPrice) {
        this.dataPrice = dataPrice;
    }
}
