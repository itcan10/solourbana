package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.AddLogBookInteractor;
import com.nataproperty.solourbana.view.logbook.model.ContactInfo;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.logbook.ui.AddLogBookActivity;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.CountryModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class AddLogBookPresenter implements AddLogBookInteractor {
    private AddLogBookActivity view;
    private ServiceRetrofitProject service;

    public AddLogBookPresenter(AddLogBookActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getCountryListSvc() {
        Call<ArrayList<CountryModel>> call = service.getAPI().getCountryListSvc();
        call.enqueue(new Callback<ArrayList<CountryModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CountryModel>> call, Response<ArrayList<CountryModel>> response) {
                view.showListCountryResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<CountryModel>> call, Throwable t) {
                view.showListCountryFailure(t);

            }


        });
    }

    @Override
    public void getProvinceListSvc(String countryCode) {
        Call<ArrayList<ProvinceModel>> call = service.getAPI().getProvinceListSvc(countryCode);
        call.enqueue(new Callback<ArrayList<ProvinceModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ProvinceModel>> call, Response<ArrayList<ProvinceModel>> response) {
                view.showListProvinceResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<ProvinceModel>> call, Throwable t) {
                view.showListProvinceFailure(t);

            }


        });
    }

    @Override
    public void getCityListSvc(String countryCode, String provinceCode) {
        Call<ArrayList<CityModel>> call = service.getAPI().getCityListSvc(countryCode,provinceCode);
        call.enqueue(new Callback<ArrayList<CityModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CityModel>> call, Response<ArrayList<CityModel>> response) {
                view.showListCityResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<CityModel>> call, Throwable t) {
                view.showListCityFailure(t);

            }


        });
    }

    @Override
    public void insertContactBankSvc(Map<String, String> fields) {
        Call<ResponeModel> call = service.getAPI().insertContactBankSvc(fields);
        call.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, Response<ResponeModel> response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.showResponeFailure(t);
            }
        });
    }

    @Override
    public void updateContactBankSvc(Map<String, String> fields) {
        Call<ResponeModel> call = service.getAPI().updateContactBankSvc(fields);
        call.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, Response<ResponeModel> response) {
                view.showResponeUpdateResults(response);
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.showResponeUpdateFailure(t);
            }
        });
    }

    @Override
    public void GetInfoContactBankSvc(String memberRef, String customerRef, String projectCode) {
        Call<ContactInfo> call = service.getAPI().GetInfoContactBankSvc(memberRef,customerRef,projectCode);
        call.enqueue(new Callback<ContactInfo>() {
            @Override
            public void onResponse(Call<ContactInfo> call, Response<ContactInfo> response) {
                view.showInfoLogBookResults(response);
            }

            @Override
            public void onFailure(Call<ContactInfo> call, Throwable t) {
                view.showInfoLogBookFailure(t);

            }


        });

    }

}
