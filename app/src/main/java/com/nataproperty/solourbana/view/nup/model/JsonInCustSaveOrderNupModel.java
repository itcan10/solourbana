package com.nataproperty.solourbana.view.nup.model;

public class JsonInCustSaveOrderNupModel {

    private String customerMemberRef;
    private String customerName;
    private String customerBirthDate;
    private String customerNIK;
    private String customerHP;
    private String customerPHONE;
    private String customerEMAIL;
    private String customerADDRESS;
    private String customerNPWP;
    private String fileCodeKtp;
    private String fileCodeNpwp;

    public String getCustomerMemberRef() {
        return customerMemberRef;
    }

    public void setCustomerMemberRef(String customerMemberRef) {
        this.customerMemberRef = customerMemberRef;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerBirthDate() {
        return customerBirthDate;
    }

    public void setCustomerBirthDate(String customerBirthDate) {
        this.customerBirthDate = customerBirthDate;
    }

    public String getCustomerNIK() {
        return customerNIK;
    }

    public void setCustomerNIK(String customerNIK) {
        this.customerNIK = customerNIK;
    }

    public String getCustomerHP() {
        return customerHP;
    }

    public void setCustomerHP(String customerHP) {
        this.customerHP = customerHP;
    }

    public String getCustomerPHONE() {
        return customerPHONE;
    }

    public void setCustomerPHONE(String customerPHONE) {
        this.customerPHONE = customerPHONE;
    }

    public String getCustomerEMAIL() {
        return customerEMAIL;
    }

    public void setCustomerEMAIL(String customerEMAIL) {
        this.customerEMAIL = customerEMAIL;
    }

    public String getCustomerADDRESS() {
        return customerADDRESS;
    }

    public void setCustomerADDRESS(String customerADDRESS) {
        this.customerADDRESS = customerADDRESS;
    }

    public String getCustomerNPWP() {
        return customerNPWP;
    }

    public void setCustomerNPWP(String customerNPWP) {
        this.customerNPWP = customerNPWP;
    }

    public String getFileCodeKtp() {
        return fileCodeKtp;
    }

    public void setFileCodeKtp(String fileCodeKtp) {
        this.fileCodeKtp = fileCodeKtp;
    }

    public String getFileCodeNpwp() {
        return fileCodeNpwp;
    }

    public void setFileCodeNpwp(String fileCodeNpwp) {
        this.fileCodeNpwp = fileCodeNpwp;
    }
}
