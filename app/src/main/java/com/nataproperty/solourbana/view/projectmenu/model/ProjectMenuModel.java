package com.nataproperty.solourbana.view.projectmenu.model;

/**
 * Created by Nata
 * on Mar 3/16/17 15:03.
 * Project : wikaAppsAndroid
 */

public class ProjectMenuModel {

    private int menuIcon;
    private String menuTitle;
    private int menuName;

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public int getMenuName() {
        return menuName;
    }

    public void setMenuName(int menuName) {
        this.menuName = menuName;
    }
}
