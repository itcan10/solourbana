package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class PaymentModel {
    int status;
    String message;
    DataPrice dataPrice;
    String totalPayment;
    PropertyInfo propertyInfo;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataPrice getDataPrice() {
        return dataPrice;
    }

    public void setDataPrice(DataPrice dataPrice) {
        this.dataPrice = dataPrice;
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public PropertyInfo getPropertyInfo() {
        return propertyInfo;
    }

    public void setPropertyInfo(PropertyInfo propertyInfo) {
        this.propertyInfo = propertyInfo;
    }
}
