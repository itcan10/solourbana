package com.nataproperty.solourbana.view.kpr.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.helper.MyListView;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.kpr.adapter.TemplateAdapter;
import com.nataproperty.solourbana.view.kpr.model.TemplateModel;
import com.nataproperty.solourbana.view.kpr.presenter.CalculatorDtlPresenter;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

public class CalculationDetailActivity extends AppCompatActivity {
    public static final String TAG = "CalculationDetail";

    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private CalculatorDtlPresenter presenter;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    String memberRef;

    TextView txtProjectName;
    MyListView listView;
    Button btnAddTemplate;

    private List<TemplateModel> listTemplate = new ArrayList<>();
    private TemplateAdapter adapter;
    String calcPayTermRef, numOfInst;

    String calcUserProjectRef, project, cluster, product, unit, price;

    LinearLayout linearList, linearNoList;
    Toolbar toolbar;
    TextView title;
    Typeface font;


    static CalculationDetailActivity calculationDetailActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_detail);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new CalculatorDtlPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        calculationDetailActivity = this;
        Intent intent = getIntent();
        calcUserProjectRef = intent.getStringExtra("calcUserProjectRef");
        project = intent.getStringExtra("project");
        cluster = intent.getStringExtra("cluster");
        product = intent.getStringExtra("product");
        unit = intent.getStringExtra("unit");
        price = intent.getStringExtra("price");
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Log.d(TAG, "calcUserProjectRef:" + calcUserProjectRef);
        btnAddTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCaraBayar();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                calcPayTermRef = listTemplate.get(position).getCalcPayTermRef();
                calcUserProjectRef = listTemplate.get(position).getCalcUserProjectRef();
                numOfInst = listTemplate.get(position).getInstallmentType();
                Intent intent = new Intent(CalculationDetailActivity.this, CalculationCaraBayarActivity.class);
                intent.putExtra("calcPayTermRef", calcPayTermRef);
                intent.putExtra("calcUserProjectRef", calcUserProjectRef);
                intent.putExtra("project", project);
                intent.putExtra("type", Integer.parseInt(numOfInst) - 1);
                startActivity(intent);
            }
        });

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_calculation));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        linearNoList = (LinearLayout) findViewById(R.id.linear_no_list_calculation);

        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        listView = (MyListView) findViewById(R.id.list_template);
        btnAddTemplate = (Button) findViewById(R.id.btn_add_template);

        txtProjectName.setText(project);
        btnAddTemplate.setTypeface(font);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        listTemplate.clear();
        requestTemplate();

    }

    public static CalculationDetailActivity getInstance() {
        return calculationDetailActivity;
    }

    private void selectCaraBayar() {
        //final CharSequence[] items = {"Cicilan", "KPR"};
        Intent intent = new Intent(CalculationDetailActivity.this, CalculationCaraBayarActivity.class);
        intent.putExtra("type", 0);
        intent.putExtra("calcUserProjectRef", calcUserProjectRef);
        intent.putExtra("project", project);
        intent.putExtra("noteSave", 1);
        startActivity(intent);
    }


    public void requestTemplate() {

        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListTemplate(project.toString(), memberRef.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CalculationDetailActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showListTemplateResults(retrofit2.Response<List<TemplateModel>> response) {
        progressDialog.dismiss();
        listTemplate = response.body();
        if (listTemplate.size() != 0) {
            linearNoList.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            linearNoList.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
        initAdapter();
    }

    private void initAdapter() {
        adapter = new TemplateAdapter(this, listTemplate);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showListTemplateFailure(Throwable t) {
        progressDialog.dismiss();

    }
}
