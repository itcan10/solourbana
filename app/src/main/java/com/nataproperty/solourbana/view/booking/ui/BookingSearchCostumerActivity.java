package com.nataproperty.solourbana.view.booking.ui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.helper.FixedHoloDatePickerDialog;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.nup.adapter.SearchCustomerAdapter;
import com.nataproperty.solourbana.view.nup.model.SearchCustomerModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by UserModel on 5/23/2016.
 */
public class BookingSearchCostumerActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PROJECT_NAME = "projectName";
    private TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    private TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount, txtTotal;

    SharedPreferences sharedPreferences;

    //logo
    ImageView imgLogo;
    TextView txtProjectName;

    String dbMasterRef, projectRef, categoryRef, clusterRef, productRef, unitRef, termRef, termNo;
    String propertys, category, product, unit, area, priceInc;
    String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition, total;
    String memberCustomerRef,va1, va2;

    //search customer
    private List<SearchCustomerModel> listCostumer = new ArrayList<SearchCustomerModel>();
    private SearchCustomerAdapter adapter;

    LinearLayout headerList;

    TextView txtRegisterCustomer, txtNoData;

    EditText editFullname, editBirthdate;
    Button btnSearch, btnRegisterCustomer;
    ListView listView;
    Typeface font;
    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_search_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        va1 = intent.getStringExtra("va1");
        va2 = intent.getStringExtra("va2");

        RelativeLayout rPage = (RelativeLayout) findViewById(R.id.rPage);

        final Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * search costmer
         */
        headerList = (LinearLayout) findViewById(R.id.header_list_customer);

        editFullname = (EditText) findViewById(R.id.edit_fullname);
        editBirthdate = (EditText) findViewById(R.id.edit_birthdate);

        txtNoData = (TextView) findViewById(R.id.txt_no_data);
        txtRegisterCustomer = (TextView) findViewById(R.id.txt_register_customer);

        btnSearch = (Button) findViewById(R.id.btn_search);
        btnRegisterCustomer = (Button) findViewById(R.id.btn_register_customer);

        listView = (ListView) findViewById(R.id.list_search_customer);

        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        //edtBirthdate
        editBirthdate.setInputType(InputType.TYPE_NULL);
        editBirthdate.setTextIsSelectable(true);
        editBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialogPicker();
            }
        });
        editBirthdate.setFocusable(false);

        btnSearch.setTypeface(font);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listCostumer.clear();
                requestCustomer();
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editFullname.getWindowToken(), 0);
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                memberCustomerRef = listCostumer.get(position).getMemberCustomerRef();

                Log.d("memberrrr reefff", " " + memberCustomerRef);
                Intent intentInformasi = new Intent(BookingSearchCostumerActivity.this, BookingInformasiCustomerActivity.class);
                intentInformasi.putExtra(DBMASTER_REF, dbMasterRef);
                intentInformasi.putExtra(PROJECT_REF, projectRef);
                intentInformasi.putExtra(CATEGORY_REF, categoryRef);
                intentInformasi.putExtra(CLUSTER_REF, clusterRef);
                intentInformasi.putExtra(PRODUCT_REF, productRef);
                intentInformasi.putExtra(UNIT_REF, unitRef);
                intentInformasi.putExtra(TERM_REF, termRef);
                intentInformasi.putExtra(TERM_NO, termNo);
                intentInformasi.putExtra(PROJECT_NAME, propertys);
                intentInformasi.putExtra("va1", va1);
                intentInformasi.putExtra("va2", va2);

                //intentInformasi.putExtra(MEMBER_CUSTOMER_REF,memberCustomerRef);
                SharedPreferences sharedPreferences = BookingSearchCostumerActivity.this.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isMemberCostumerRef", memberCustomerRef);
                editor.commit();

                intentInformasi.putExtra("status", "1");
                startActivity(intentInformasi);
            }
        });

        btnRegisterCustomer.setTypeface(font);
        btnRegisterCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister = new Intent(BookingSearchCostumerActivity.this, BookingCreateCustomerActivity.class);
                intentRegister.putExtra(DBMASTER_REF, dbMasterRef);
                intentRegister.putExtra(PROJECT_REF, projectRef);
                intentRegister.putExtra(PROJECT_NAME, propertys);
                intentRegister.putExtra(CLUSTER_REF, clusterRef);
                intentRegister.putExtra(PRODUCT_REF, productRef);
                intentRegister.putExtra(UNIT_REF, unitRef);
                intentRegister.putExtra(TERM_REF, termRef);
                intentRegister.putExtra(TERM_NO, termNo);
                intentRegister.putExtra("va1", va1);
                intentRegister.putExtra("va2", va2);
                startActivity(intentRegister);
            }
        });


        txtRegisterCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister = new Intent(BookingSearchCostumerActivity.this, BookingCreateCustomerActivity.class);
                intentRegister.putExtra(DBMASTER_REF, dbMasterRef);
                intentRegister.putExtra(PROJECT_REF, projectRef);
                intentRegister.putExtra(PROJECT_NAME, propertys);
                intentRegister.putExtra(CLUSTER_REF, clusterRef);
                intentRegister.putExtra(PRODUCT_REF, productRef);
                intentRegister.putExtra(UNIT_REF, unitRef);
                intentRegister.putExtra(TERM_REF, termRef);
                intentRegister.putExtra(TERM_NO, termNo);
                intentRegister.putExtra("va1", va1);
                intentRegister.putExtra("va2", va2);
                startActivity(intentRegister);
            }
        });

        requestPayment();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //requestPayment();

    }

    private void dialogPicker() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final Context themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog);
        final DatePickerDialog dialog = new FixedHoloDatePickerDialog(
                themedContext,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dialog.show();
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editBirthdate.setText(sdf.format(myCalendar.getTime()));
    }

    public void requestPayment() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getPayment(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    Log.d("Cek payment info", response);
                    if (status == 200) {
                        paymentTerm = jo.getJSONObject("dataPrice").getString("paymentTerm");
                        priceIncVat = jo.getJSONObject("dataPrice").getString("priceInc");
                        discPercent = jo.getJSONObject("dataPrice").getString("discPercent");
                        discAmt = jo.getJSONObject("dataPrice").getString("discAmt");
                        netPrice = jo.getJSONObject("dataPrice").getString("netPrice");
                        termCondition = jo.getJSONObject("dataPrice").getString("termCondition");

                        propertys = jo.getJSONObject("propertyInfo").getString("propertys");
                        category = jo.getJSONObject("propertyInfo").getString("category");
                        product = jo.getJSONObject("propertyInfo").getString("product");
                        unit = jo.getJSONObject("propertyInfo").getString("unit");
                        area = jo.getJSONObject("propertyInfo").getString("landBuild");
                        priceInc = jo.getJSONObject("propertyInfo").getString("priceInc");

                        txtPropertyName.setText(propertys);
                        txtCategoryType.setText(category);
                        txtProduct.setText(product);
                        txtUnitNo.setText(unit);
                        txtArea.setText(Html.fromHtml(area));
                        txtEstimate.setText(priceInc);

                        txtPaymentTerms.setText(paymentTerm);
                        txtPriceInc.setText(priceIncVat);
                        txtDisconutPersen.setText(discPercent);
                        txtdiscount.setText(discAmt);
                        txtNetPrice.setText(netPrice);

                        txtProjectName.setText(propertys);

                    } else {

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingSearchCostumerActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingSearchCostumerActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dbMasterRef", dbMasterRef);
                params.put("projectRef", projectRef);
                params.put("clusterRef", clusterRef);
                params.put("productRef", productRef);
                params.put("unitRef", unitRef);
                params.put("termRef", termRef);
                params.put("termNo", termNo);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "paymentInfo");

    }

    public void requestCustomer() {
        //BaseApplication.getInstance().startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCostumer(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("result cluster", response);
                    generateListCostumer(jsonArray);

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //BaseApplication.getInstance().stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);

                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingSearchCostumerActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingSearchCostumerActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customerName", editFullname.getText().toString());
                params.put("birthDate", editBirthdate.getText().toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "searchCustomer");

    }

    private void generateListCostumer(JSONArray response) {
        Log.d("Leng", String.valueOf(response.length()));
        if (response.length() == 0) {
            Intent intentRegister = new Intent(BookingSearchCostumerActivity.this, BookingCreateCustomerActivity.class);
            intentRegister.putExtra(DBMASTER_REF, dbMasterRef);
            intentRegister.putExtra(PROJECT_REF, projectRef);
            intentRegister.putExtra(PROJECT_NAME, propertys);
            intentRegister.putExtra(CLUSTER_REF, clusterRef);
            intentRegister.putExtra(PRODUCT_REF, productRef);
            intentRegister.putExtra(UNIT_REF, unitRef);
            intentRegister.putExtra(TERM_REF, termRef);
            intentRegister.putExtra(TERM_NO, termNo);
            intentRegister.putExtra("va1", va1);
            intentRegister.putExtra("va2", va2);
            intentRegister.putExtra("customerName", editFullname.getText().toString());
            intentRegister.putExtra("birthDate", editBirthdate.getText().toString());
            startActivity(intentRegister);

        } else if(response.length() == 1){
            try {
                JSONObject jo = response.getJSONObject(0);
                SearchCustomerModel searchCustomer = new SearchCustomerModel();
                searchCustomer.setMemberCustomerRef(jo.getString("memberCustomerRef"));
                searchCustomer.setCustomerName(jo.getString("customerName"));
                searchCustomer.setBirthDate(jo.getString("birthDate"));
                listCostumer.add(searchCustomer);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            memberCustomerRef = listCostumer.get(0).getMemberCustomerRef();
            Log.d("memberrrr reefff", " " + memberCustomerRef);
            Intent intentInformasi = new Intent(BookingSearchCostumerActivity.this, BookingInformasiCustomerActivity.class);
            intentInformasi.putExtra(DBMASTER_REF, dbMasterRef);
            intentInformasi.putExtra(PROJECT_REF, projectRef);
            intentInformasi.putExtra(CATEGORY_REF, categoryRef);
            intentInformasi.putExtra(CLUSTER_REF, clusterRef);
            intentInformasi.putExtra(PRODUCT_REF, productRef);
            intentInformasi.putExtra(UNIT_REF, unitRef);
            intentInformasi.putExtra(TERM_REF, termRef);
            intentInformasi.putExtra(TERM_NO, termNo);
            intentInformasi.putExtra(PROJECT_NAME, propertys);
            intentInformasi.putExtra("va1", va1);
            intentInformasi.putExtra("va2", va2);
            SharedPreferences sharedPreferences = BookingSearchCostumerActivity.this.
                    getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("isMemberCostumerRef", memberCustomerRef);
            editor.commit();

            intentInformasi.putExtra("status", "1");
            startActivity(intentInformasi);
        }else {
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jo = response.getJSONObject(i);
                    SearchCustomerModel searchCustomer = new SearchCustomerModel();
                    searchCustomer.setMemberCustomerRef(jo.getString("memberCustomerRef"));
                    searchCustomer.setCustomerName(jo.getString("customerName"));
                    searchCustomer.setBirthDate(jo.getString("birthDate"));

                    listCostumer.add(searchCustomer);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            headerList.setVisibility(View.VISIBLE);
            btnRegisterCustomer.setVisibility(View.GONE);
            txtNoData.setVisibility(View.GONE);
            adapter = new SearchCustomerAdapter(this, listCostumer);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
