package com.nataproperty.solourbana.view.menuitem.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.menuitem.intractor.PresenterNotificationDetailInteractor;
import com.nataproperty.solourbana.view.menuitem.model.NotificationDetailModel;
import com.nataproperty.solourbana.view.menuitem.ui.NotificationDetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NotificationDetailPresenter implements PresenterNotificationDetailInteractor {
    private NotificationDetailActivity view;
    private ServiceRetrofit service;

    public NotificationDetailPresenter(NotificationDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getGCMMessageSvc(String gcmMessageRef,String memberRef) {
        Call<NotificationDetailModel> call = service.getAPI().getGCMMessageSvc(gcmMessageRef,memberRef);
        call.enqueue(new Callback<NotificationDetailModel>() {
            @Override
            public void onResponse(Call<NotificationDetailModel> call, Response<NotificationDetailModel> response) {
                view.showListNotificationDetailResults(response);
            }

            @Override
            public void onFailure(Call<NotificationDetailModel> call, Throwable t) {
                view.showListNotificationDetailFailure(t);

            }


        });
    }

}
