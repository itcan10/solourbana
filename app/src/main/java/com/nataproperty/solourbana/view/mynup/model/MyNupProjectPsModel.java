package com.nataproperty.solourbana.view.mynup.model;

/**
 * Created by Nata on 1/9/2017.
 */

public class MyNupProjectPsModel {
    String projectName, dbMasterRef, projectRef, nupPsCode, qty, nupNo, nupName, amount, nupRef,
            buyDate,nupStatusName;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getNupPsCode() {
        return nupPsCode;
    }

    public void setNupPsCode(String nupPsCode) {
        this.nupPsCode = nupPsCode;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getNupNo() {
        return nupNo;
    }

    public void setNupNo(String nupNo) {
        this.nupNo = nupNo;
    }

    public String getNupName() {
        return nupName;
    }

    public void setNupName(String nupName) {
        this.nupName = nupName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getNupRef() {
        return nupRef;
    }

    public void setNupRef(String nupRef) {
        this.nupRef = nupRef;
    }

    public String getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(String buyDate) {
        this.buyDate = buyDate;
    }

    public String getNupStatusName() {
        return nupStatusName;
    }

    public void setNupStatusName(String nupStatusName) {
        this.nupStatusName = nupStatusName;
    }
}
