package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.nup.model.QtyNupModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class QtyNupAdapter extends BaseAdapter {
    private Context context;
    private List<QtyNupModel> list;
    private ListBankHolder holder;

    public QtyNupAdapter(Context context, List<QtyNupModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_bank,null);
            holder = new ListBankHolder();
            holder.bankName = (TextView) convertView.findViewById(R.id.txt_bank);

            convertView.setTag(holder);
        }else{
            holder = (ListBankHolder) convertView.getTag();
        }

        QtyNupModel feature = list.get(position);
        holder.bankName.setText(feature.getQtyNUP());

        return convertView;

    }

    private class ListBankHolder {
        TextView bankName;
    }
}
