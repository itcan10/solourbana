package com.nataproperty.solourbana.view.project.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.config.BaseApplication;
import com.nataproperty.solourbana.config.General;
import com.nataproperty.solourbana.config.WebService;
import com.nataproperty.solourbana.view.project.model.ProjectModelNew;
import com.nataproperty.solourbana.view.project.ui.ProjectActivity;
import com.nataproperty.solourbana.view.project.ui.ValidateInhouseCode;
import com.nataproperty.solourbana.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by UserModel on 10/28/2016.
 */
public class RVProjectAdapter extends RecyclerView.Adapter<RVProjectAdapter.ProjectRequestHolder> {
    List<ProjectModelNew> projectModels;
    private Context context;
    private Display display;
    ProjectModelNew projectsModel;

    Integer position = 0; //Need to declare because onbind is executed after oncreate, and idk how to get position on oncreate

    public RVProjectAdapter(Context context, List<ProjectModelNew> projectModels1, Display display) {
        this.context = context;
        this.display = display;
        this.projectModels = projectModels1;
    }

    public static class ProjectRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cv;
        TextView projectName, locationName, subLocationName;
        ImageView projectImg, btnJoin, btnJoined, btnWaiting;
        Context context;
        SharedPreferences sharedPreferences;
        private boolean state;
        private String memberRef, dbMasterRef, projectRef, memberCode = "";
        EditText edtMemberCode;
        TextView txtTermMemberCode;
        AlertDialog alertDialogJoin;
        ProgressDialog progressDialog;

        ProjectRequestHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            cv = (CardView) itemView.findViewById(R.id.cv);
            projectName = (TextView) itemView.findViewById(R.id.projectName);
            locationName = (TextView) itemView.findViewById(R.id.locationName);
            subLocationName = (TextView) itemView.findViewById(R.id.subLocationName);
            projectImg = (ImageView) itemView.findViewById(R.id.imgProject);
            btnJoin = (ImageView) itemView.findViewById(R.id.btn_join);
            btnJoined = (ImageView) itemView.findViewById(R.id.btn_joined);
            btnWaiting = (ImageView) itemView.findViewById(R.id.btn_waiting);
        }

        @Override
        public void onClick(final View v) {

        }

    }

    @Override
    public ProjectRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.include_card_projectrequest, parent, false);
        final ProjectRequestHolder holder = new ProjectRequestHolder(v);
        ProjectRequestHolder crh = new ProjectRequestHolder(v);

        return crh;
    }

    @Override
    public void onBindViewHolder(final ProjectRequestHolder holder, final int position) {
        this.position = position + 1;
        holder.sharedPreferences = context.getSharedPreferences(General.PREF_NAME, 0);
        holder.state = holder.sharedPreferences.getBoolean("isLogin", false);
        holder.memberRef = holder.sharedPreferences.getString("isMemberRef", null);

        final ProjectModelNew project = projectModels.get(position);
        holder.projectName.setText(project.getProjectName().toUpperCase());
        holder.locationName.setText(project.getLocationName());
        holder.subLocationName.setText(project.getSubLocationName());
        //Log.d("RVProject", project.getIsJoin());

        if (project.getIsJoin().equals("0")) {
            if (project.getIsWaiting() != null) {
                if (project.getIsWaiting().equals("1")) {
                    holder.btnJoin.setVisibility(View.GONE);
                    holder.btnJoined.setVisibility(View.GONE);
                    holder.btnWaiting.setVisibility(View.VISIBLE);
                } else {
                    holder.btnJoin.setVisibility(View.VISIBLE);
                    holder.btnJoined.setVisibility(View.GONE);
                    holder.btnWaiting.setVisibility(View.GONE);
                }
            } else {
                holder.btnJoined.setVisibility(View.GONE);
                holder.btnJoin.setVisibility(View.GONE);
                holder.btnWaiting.setVisibility(View.GONE);
            }
        } else {
            holder.btnJoined.setVisibility(View.VISIBLE);
            holder.btnJoin.setVisibility(View.GONE);
            holder.btnWaiting.setVisibility(View.GONE);
        }

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = holder.projectImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        holder.projectImg.setLayoutParams(params);
        holder.projectImg.requestLayout();

        holder.projectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProjectMenuActivity.class);
                intent.putExtra(General.PROJECT_REF, project.getProjectRef());
                intent.putExtra(General.DBMASTER_REF, project.getDbMasterRef());
                intent.putExtra(General.PROJECT_NAME, project.getProjectName());
                intent.putExtra(General.LOCATION_NAME, project.getLocationName());
                intent.putExtra(General.LOCATION_REF, project.getLocationRef());
                intent.putExtra(General.SUBLOCATION_NAME, project.getSubLocationName());
                intent.putExtra(General.SUBLOCATION_REF, project.getSublocationRef());
                SharedPreferences sharedPreferences = context.
                        getSharedPreferences(General.PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isWaiting", project.getIsWaiting());
                editor.putString("isJoin", project.getIsJoin());
                editor.commit();
                //context.startActivity(intent);
                ((Activity) context).startActivityForResult(intent,1);
            }
        });

        holder.btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View dialogView = inflater.inflate(R.layout.dialog_insert_member_code, null);
                dialogBuilder.setView(dialogView);
                holder.edtMemberCode = (EditText) dialogView.findViewById(R.id.edt_member_code);
                holder.txtTermMemberCode = (TextView) dialogView.findViewById(R.id.txt_term_member_code);
                dialogBuilder.setMessage("Masukkan Member ID");
                holder.txtTermMemberCode.setText("Kosongkan jika tidak memiliki Member ID di " + project.getProjectName());
                dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //subscribeProject();
                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                holder.alertDialogJoin = dialogBuilder.create();
                holder.alertDialogJoin.setCancelable(false);
                holder.alertDialogJoin.show();
                Button positiveButton = holder.alertDialogJoin.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.memberCode = holder.edtMemberCode.getText().toString();
                        holder.progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                        final StringRequest request = new StringRequest(Request.Method.POST,
                                WebService.subscribeProject(), new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    holder.progressDialog.dismiss();
                                    JSONObject jo = new JSONObject(response);
                                    int status = jo.getInt("status");
                                    String message = jo.getString("message");

                                    if (status == 200) {
                                        holder.alertDialogJoin.dismiss();
                                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(context, ProjectActivity.class);
                                        ((Activity) context).recreate();
                                        //context.startActivity(intent);
                                    } else if (status == 202) {
                                        holder.alertDialogJoin.dismiss();
                                        Intent intent = new Intent(context, ValidateInhouseCode.class);
                                        intent.putExtra("memberRef", holder.memberRef);
                                        intent.putExtra("dbMasterRef", String.valueOf(project.getDbMasterRef()));
                                        intent.putExtra("projectRef", project.getProjectRef());
                                        intent.putExtra("memberCode", holder.edtMemberCode.getText().toString());
                                        context.startActivity(intent);
                                    } else if (status == 203) {
                                        holder.alertDialogJoin.dismiss();
                                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(context, ProjectActivity.class);
                                        ((Activity) context).recreate();
                                        //context.startActivity(intent);
                                    }  else if (status == 204) {
                                        holder.edtMemberCode.setError(message);
                                    } else {
                                        String error = jo.getString("message");
                                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        holder.progressDialog.dismiss();
                                        NetworkResponse networkResponse = error.networkResponse;
                                        if (networkResponse != null) {
                                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                        }
                                        if (error instanceof TimeoutError) {
                                            Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                                            Log.d("Volley", "TimeoutError");
                                        } else if (error instanceof NoConnectionError) {
                                            Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                                            Log.d("Volley", "NoConnectionError");
                                        } else if (error instanceof AuthFailureError) {
                                            Log.d("Volley", "AuthFailureError");
                                        } else if (error instanceof ServerError) {
                                            Log.d("Volley", "ServerError");
                                        } else if (error instanceof NetworkError) {
                                            Log.d("Volley", "NetworkError");
                                        } else if (error instanceof ParseError) {
                                            Log.d("Volley", "ParseError");
                                        }
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("memberRef", holder.memberRef);
                                params.put("dbMasterRef", String.valueOf(project.getDbMasterRef()));
                                params.put("projectRef", project.getProjectRef());
                                params.put("memberCode", holder.edtMemberCode.getText().toString());

                                return params;
                            }
                        };

                        BaseApplication.getInstance().addToRequestQueue(request, "project");

                    }

                });

            }


        });

    }

    @Override
    public int getItemCount() {
        return projectModels.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}
