package com.nataproperty.solourbana.view.logbook.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofitProject;
import com.nataproperty.solourbana.view.logbook.intractor.LogBookInteractor;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.ui.LogBookActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class LogBookPresenter implements LogBookInteractor {
    private LogBookActivity view;
    private ServiceRetrofitProject service;

    public LogBookPresenter(LogBookActivity view, ServiceRetrofitProject service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetContactBankSvc(String memberRef, String customerStatusRef, String keyword, String projectCode) {
        Call<ArrayList<LogBookModel>> call = service.getAPI().GetContactBankSvc(memberRef,customerStatusRef,keyword,projectCode);
        call.enqueue(new Callback<ArrayList<LogBookModel>>() {
            @Override
            public void onResponse(Call<ArrayList<LogBookModel>> call, Response<ArrayList<LogBookModel>> response) {
                view.showListLogBookResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<LogBookModel>> call, Throwable t) {
                view.showListLogBookFailure(t);

            }


        });
    }
}
