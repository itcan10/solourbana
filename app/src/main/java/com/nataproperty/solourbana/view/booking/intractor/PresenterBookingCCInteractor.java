package com.nataproperty.solourbana.view.booking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterBookingCCInteractor {
    void getMontList(String month);
    void getYearsList(String years);
    void getPayment(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo);
    void getBookingDetail(String dbMasterRef, String projectRef, String bookingRef);
}
