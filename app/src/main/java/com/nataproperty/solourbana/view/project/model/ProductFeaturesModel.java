package com.nataproperty.solourbana.view.project.model;

/**
 * Created by UserModel on 5/11/2016.
 */
public class ProductFeaturesModel {
    String dbMasterRef,projectRef,projectfeature,featureName;

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectfeature() {
        return projectfeature;
    }

    public void setProjectfeature(String projectfeature) {
        this.projectfeature = projectfeature;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }
}
