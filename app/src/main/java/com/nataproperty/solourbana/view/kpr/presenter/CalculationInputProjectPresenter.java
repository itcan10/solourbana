package com.nataproperty.solourbana.view.kpr.presenter;

import com.nataproperty.solourbana.config.ServiceRetrofit;
import com.nataproperty.solourbana.view.kpr.intractor.CalculationInputProjectInteractor;
import com.nataproperty.solourbana.view.kpr.model.ResponeCalcUserProject;
import com.nataproperty.solourbana.view.kpr.ui.CalculationInputProjectActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CalculationInputProjectPresenter implements CalculationInputProjectInteractor {
    private CalculationInputProjectActivity view;
    private ServiceRetrofit service;

    public CalculationInputProjectPresenter(CalculationInputProjectActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void saveNewCalcUserProject(Map<String, String> fields) {
        Call<ResponeCalcUserProject> call = service.getAPI().saveNewCalcUserProject(fields);
        call.enqueue(new Callback<ResponeCalcUserProject> () {
            @Override
            public void onResponse(Call<ResponeCalcUserProject>  call, Response<ResponeCalcUserProject>  response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponeCalcUserProject>  call, Throwable t) {
                view.showResponeFailure(t);

            }


        });
    }

}
