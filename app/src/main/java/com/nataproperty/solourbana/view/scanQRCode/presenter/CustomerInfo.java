package com.nataproperty.solourbana.view.scanQRCode.presenter;

/**
 * Created by Nata on 3/14/2017.
 */

public class CustomerInfo {
    String customerName, customerHp, customerPhone, customerEmail, customerAddr, customerKTP, customerNPWP, bookingCode;

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerHp() {
        return customerHp;
    }

    public void setCustomerHp(String customerHp) {
        this.customerHp = customerHp;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerAddr() {
        return customerAddr;
    }

    public void setCustomerAddr(String customerAddr) {
        this.customerAddr = customerAddr;
    }

    public String getCustomerKTP() {
        return customerKTP;
    }

    public void setCustomerKTP(String customerKTP) {
        this.customerKTP = customerKTP;
    }

    public String getCustomerNPWP() {
        return customerNPWP;
    }

    public void setCustomerNPWP(String customerNPWP) {
        this.customerNPWP = customerNPWP;
    }
}
