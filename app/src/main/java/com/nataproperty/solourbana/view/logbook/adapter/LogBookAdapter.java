package com.nataproperty.solourbana.view.logbook.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class LogBookAdapter extends BaseAdapter  {
    public static final String TAG = "NotificationAdapter" ;

    public static final String PREF_NAME = "pref" ;

    private Context context;
    private ArrayList<LogBookModel> list;

    private ListNotificationHolder holder;

    public LogBookAdapter(Context context, ArrayList<LogBookModel> list) {
        this.context = context;
        this.list = list;

    }

    SharedPreferences sharedPreferences;
    private boolean state;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state =  sharedPreferences.getBoolean("isLogin", false);

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_log_book,null);
            holder = new ListNotificationHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.hp = (TextView) convertView.findViewById(R.id.txt_hp);
            holder.email = (TextView) convertView.findViewById(R.id.txt_email);
            holder.address = (TextView) convertView.findViewById(R.id.txt_address);

            convertView.setTag(holder);
        }else{
            holder = (ListNotificationHolder) convertView.getTag();
        }

        LogBookModel logBookModel = list.get(position);
        holder.name.setText(logBookModel.getName());
        holder.hp.setText(logBookModel.getHp1());
        if (logBookModel.getEmail1().equals("")){
            holder.email.setText("-");
        } else {
            holder.email.setText(logBookModel.getEmail1());
        }

        if (logBookModel.getAddress().trim().equals("")){
            holder.address.setText(logBookModel.getCityName()+", "+logBookModel.getProvinceName());
        } else {
            holder.address.setText(logBookModel.getAddress()+", "+logBookModel.getCityName()+", "+logBookModel.getProvinceName());
        }


        return convertView;
    }


    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListNotificationHolder {
        TextView name,hp,email,address;
    }

}
