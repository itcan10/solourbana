package com.nataproperty.solourbana.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.nup.model.YearModel;

import java.util.List;

/**
 * Created by UserModel on 5/15/2016.
 */
public class YearAdapter extends BaseAdapter {
    private Context context;
    private List<YearModel> list;
    private ListYearHolder holder;

    public YearAdapter(Context context, List<YearModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_year,null);
            holder = new ListYearHolder();
            holder.yearName = (TextView) convertView.findViewById(R.id.txt_year);

            convertView.setTag(holder);
        }else{
            holder = (ListYearHolder) convertView.getTag();
        }

        YearModel feature = list.get(position);
        holder.yearName.setText(feature.getYear());

       /* Picasso.with(context)
                .load("http://192.168.10.180/andro/wf/support/displayImage.aspx?is=cluster&dr=5&pr=1&cr=1&quot;).into(holder.clusterImg);*/

        return convertView;

    }

    private class ListYearHolder {
        TextView yearName;
    }
}
