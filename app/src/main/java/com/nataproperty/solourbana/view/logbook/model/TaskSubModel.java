package com.nataproperty.solourbana.view.logbook.model;

/**
 * Created by Nata on 2/16/2017.
 */

public class TaskSubModel {
    String logProjectCustomerStatusTaskRef, customerProjectActionName, number, projectCustomerStatusName, scheduleDate,
            descriptionTask, finishDate, customerResponseStatusName, descriptionResult, customerProjectAction,
            parentLogProjectCustomerStatusTaskRef, scheduleDateValue, newToProgress, progressToInterest, progressToNotInterest, interestToFinish, notInterestToFinish,
            iconResponse;

    public String getNewToProgress() {
        return newToProgress;
    }

    public void setNewToProgress(String newToProgress) {
        this.newToProgress = newToProgress;
    }

    public String getProgressToInterest() {
        return progressToInterest;
    }

    public void setProgressToInterest(String progressToInterest) {
        this.progressToInterest = progressToInterest;
    }

    public String getProgressToNotInterest() {
        return progressToNotInterest;
    }

    public void setProgressToNotInterest(String progressToNotInterest) {
        this.progressToNotInterest = progressToNotInterest;
    }

    public String getInterestToFinish() {
        return interestToFinish;
    }

    public void setInterestToFinish(String interestToFinish) {
        this.interestToFinish = interestToFinish;
    }

    public String getNotInterestToFinish() {
        return notInterestToFinish;
    }

    public void setNotInterestToFinish(String notInterestToFinish) {
        this.notInterestToFinish = notInterestToFinish;
    }

    public String getIconResponse() {
        return iconResponse;
    }

    public void setIconResponse(String iconResponse) {
        this.iconResponse = iconResponse;
    }

    public String getScheduleDateValue() {
        return scheduleDateValue;
    }

    public void setScheduleDateValue(String scheduleDateValue) {
        this.scheduleDateValue = scheduleDateValue;
    }

    public String getCustomerProjectAction() {
        return customerProjectAction;
    }

    public void setCustomerProjectAction(String customerProjectAction) {
        this.customerProjectAction = customerProjectAction;
    }

    public String getParentLogProjectCustomerStatusTaskRef() {
        return parentLogProjectCustomerStatusTaskRef;
    }

    public void setParentLogProjectCustomerStatusTaskRef(String parentLogProjectCustomerStatusTaskRef) {
        this.parentLogProjectCustomerStatusTaskRef = parentLogProjectCustomerStatusTaskRef;
    }

    public String getDescriptionResult() {
        return descriptionResult;
    }

    public void setDescriptionResult(String descriptionResult) {
        this.descriptionResult = descriptionResult;
    }

    public String getLogProjectCustomerStatusTaskRef() {
        return logProjectCustomerStatusTaskRef;
    }

    public void setLogProjectCustomerStatusTaskRef(String logProjectCustomerStatusTaskRef) {
        this.logProjectCustomerStatusTaskRef = logProjectCustomerStatusTaskRef;
    }

    public String getCustomerProjectActionName() {
        return customerProjectActionName;
    }

    public void setCustomerProjectActionName(String customerProjectActionName) {
        this.customerProjectActionName = customerProjectActionName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getProjectCustomerStatusName() {
        return projectCustomerStatusName;
    }

    public void setProjectCustomerStatusName(String projectCustomerStatusName) {
        this.projectCustomerStatusName = projectCustomerStatusName;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getDescriptionTask() {
        return descriptionTask;
    }

    public void setDescriptionTask(String descriptionTask) {
        this.descriptionTask = descriptionTask;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getCustomerResponseStatusName() {
        return customerResponseStatusName;
    }

    public void setCustomerResponseStatusName(String customerResponseStatusName) {
        this.customerResponseStatusName = customerResponseStatusName;
    }
}
