package com.nataproperty.solourbana.view.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.profile.model.CityModel;

import java.util.List;

/**
 * Created by UserModel on 4/17/2016.
 */
public class CityAdapter extends BaseAdapter {
    private Context context;
    private List<CityModel> list;
    private ListCityHolder holder;

    public CityAdapter(Context context, List<CityModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_city,null);
            holder = new ListCityHolder();
            holder.provinceName = (TextView) convertView.findViewById(R.id.cityName);

            convertView.setTag(holder);
        }else{
            holder = (ListCityHolder) convertView.getTag();
        }
        CityModel city = list.get(position);
        holder.provinceName.setText(city.getCityName());

        return convertView;
    }

    private class ListCityHolder {
        TextView provinceName;
    }
}
