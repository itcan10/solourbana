package com.nataproperty.solourbana.view.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.view.project.model.ProjectModel2;

import java.util.ArrayList;

/**
 * Created by UserModel on 4/19/2016.
 */
public class ListDownloadProjectAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<ProjectModel2> list;
    private ListProjectHolder holder;

    public ListDownloadProjectAdapter(Context context, ArrayList<ProjectModel2> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_project_download,null);
            holder = new ListProjectHolder();
            holder.projectName = (TextView) convertView.findViewById(R.id.projectName);
            holder.projectImg = (ImageView) convertView.findViewById(R.id.imgProject);

            convertView.setTag(holder);
        }else{
            holder = (ListProjectHolder) convertView.getTag();
        }

        ProjectModel2 project = list.get(position);
        holder.projectName.setText(project.getProjectName().toUpperCase());

        Glide.with(context)
                .load(project.getImage()).into(holder.projectImg);

        return convertView;
    }

    private class ListProjectHolder {
        TextView projectName;
        ImageView projectImg;
    }

}
