package com.nataproperty.solourbana.view.scanQRCode.model;


import com.nataproperty.solourbana.view.booking.model.UnitInfoModel;
import com.nataproperty.solourbana.view.scanQRCode.presenter.CustomerInfo;

/**
 * Created by Nata on 3/14/2017.
 */

public class ScanQRModel {
    int status;
    String message;
    BookingInfo bookingInfo;
    UnitInfoModel unitInfo;
    CustomerInfo custInfo;
    DealCloserInfo dealCloserInfo;

    public DealCloserInfo getDealCloserInfo() {
        return dealCloserInfo;
    }

    public void setDealCloserInfo(DealCloserInfo dealCloserInfo) {
        this.dealCloserInfo = dealCloserInfo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BookingInfo getBookingInfo() {
        return bookingInfo;
    }

    public void setBookingInfo(BookingInfo bookingInfo) {
        this.bookingInfo = bookingInfo;
    }

    public UnitInfoModel getUnitInfo() {
        return unitInfo;
    }

    public void setUnitInfo(UnitInfoModel unitInfo) {
        this.unitInfo = unitInfo;
    }

    public CustomerInfo getCustInfo() {
        return custInfo;
    }

    public void setCustInfo(CustomerInfo custInfo) {
        this.custInfo = custInfo;
    }
}
