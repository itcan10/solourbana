package com.nataproperty.solourbana.view.logbook.model;

/**
 * Created by Nata on 2/9/2017.
 */

public class ResponeModel {
    int status;
    String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
