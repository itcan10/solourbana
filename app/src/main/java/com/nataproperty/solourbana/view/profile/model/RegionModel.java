package com.nataproperty.solourbana.view.profile.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by UserModel on 5/15/2016.
 */
public class RegionModel {
    @SerializedName("religionRef")
    String regionRef;
    @SerializedName("religionName")
    String regionName;
    @SerializedName("religionSortNo")
    String regionSortNo;

    public String getRegionRef() {
        return regionRef;
    }

    public void setRegionRef(String regionRef) {
        this.regionRef = regionRef;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionSortNo() {
        return regionSortNo;
    }

    public void setRegionSortNo(String regionSortNo) {
        this.regionSortNo = regionSortNo;
    }
}
