package com.nataproperty.solourbana.view.commision.model;

/**
 * Created by Nata on 1/4/2017.
 */

public class CommisionSectionModel {
    String sectionName, dbMasterRef, projectRef, commissionCount, projectPsRef;

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getCommissionCount() {
        return commissionCount;
    }

    public void setCommissionCount(String commissionCount) {
        this.commissionCount = commissionCount;
    }

    public String getProjectPsRef() {
        return projectPsRef;
    }

    public void setProjectPsRef(String projectPsRef) {
        this.projectPsRef = projectPsRef;
    }
}
