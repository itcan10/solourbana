package com.nataproperty.solourbana.view.mainmenu.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nataproperty.solourbana.R;
import com.nataproperty.solourbana.helper.MyTextViewLatoReguler;
import com.nataproperty.solourbana.view.commision.ui.CommisionSectionActivity;
import com.nataproperty.solourbana.view.event.ui.EventActivity;
import com.nataproperty.solourbana.view.event.ui.MyTicketActivity;
import com.nataproperty.solourbana.view.kpr.ui.CalculationActivity;
import com.nataproperty.solourbana.view.logbook.ui.LogBookActivity;
import com.nataproperty.solourbana.view.mainmenu.model.MainMenuModel;
import com.nataproperty.solourbana.view.mainmenu.model.UserModel;
import com.nataproperty.solourbana.view.mybooking.ui.MyBookingActivity;
import com.nataproperty.solourbana.view.mynup.ui.MyNupActivity;
import com.nataproperty.solourbana.view.news.ui.NewsActivity;
import com.nataproperty.solourbana.view.project.ui.DownloadActivity;
import com.nataproperty.solourbana.view.project.ui.MyListingActivity;
import com.nataproperty.solourbana.view.project.ui.ProjectActivity;
import com.nataproperty.solourbana.view.report.ui.ReportActivity;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by arifcebe
 * on Mar 3/16/17 14:59.
 * Project : wikaAppsAndroid
 */

public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.MainMenuHolder> {
    public static final String TAG = "MainMenuAdapter";
    private List<MainMenuModel> listMenu;
    private Context context;
    private String usrRef;

    public MainMenuAdapter(Context context, List<MainMenuModel> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @Override
    public MainMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_main_menu, null);

        return new MainMenuHolder(view);
    }

    @Override
    public void onBindViewHolder(MainMenuHolder holder, int position) {

        holder.onBindHolder(listMenu.get(position), position);
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class MainMenuHolder extends RecyclerView.ViewHolder {

        CircleImageView menuIcon;
        MyTextViewLatoReguler menuTitle;
        RelativeLayout menuLayout;

        public MainMenuHolder(View itemView) {
            super(itemView);

            menuIcon = (CircleImageView) itemView.findViewById(R.id.btn_main_menu);
            menuTitle = (MyTextViewLatoReguler) itemView.findViewById(R.id.btn_main_menu_title);
            menuLayout = (RelativeLayout) itemView.findViewById(R.id.main_menu_layout_item);
        }

        public void onBindHolder(final MainMenuModel menuModel, final int position) {

            if (position % 2 == 0) {
                menuLayout.setGravity(Gravity.RIGHT);
                menuLayout.setPadding(24, 24, 100, 24);
            } else {
                menuLayout.setGravity(Gravity.LEFT);
                menuLayout.setPadding(100,24,24,24);
            }

            menuTitle.setText(menuModel.getMenuTitle());
            menuIcon.setBackground(context.getResources().getDrawable(menuModel.getMenuIcon()));
            menuIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = null;
                    switch (position) {
                        case 0:
                            intent = new Intent(context, ProjectActivity.class);
                            break;
                        case 1:
                            intent = new Intent(context, MyListingActivity.class);
                            break;
                        case 2:
                            intent = new Intent(context, DownloadActivity.class);
                            break;
                        case 3:
                            intent = new Intent(context, CommisionSectionActivity.class);
                            break;
                        case 4:
                            intent = new Intent(context, MyNupActivity.class);
                            break;
                        case 5:
                            intent = new Intent(context, MyBookingActivity.class);
                            break;
                        case 6:
                            intent = new Intent(context, NewsActivity.class);
                            break;
                        case 7:
                            intent = new Intent(context, EventActivity.class);
                            break;
                        case 8:
                            intent = new Intent(context, MyTicketActivity.class);
                            break;
                        case 9:
                            intent = new Intent(context, CalculationActivity.class);
                            break;
                        case 10:
                            intent = new Intent(context, LogBookActivity.class);
                            break;
                        case 11:
                            usrRef = UserModel.getInstance().getUserRef();
                            intent = new Intent(context, ReportActivity.class);
                            intent.putExtra("userRef", usrRef);
                            break;

                    }

                    context.startActivity(intent);

                }
            });
        }
    }

}
