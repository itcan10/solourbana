package com.nataproperty.solourbana.view.booking.model;

/**
 * Created by Nata on 1/18/2017.
 */

public class PropertyInfo {
    String propertys,category,product,unit,area,landBuild,priceInc,bookingFee,specialEnquiries,bookingContact;

    public String getPropertys() {
        return propertys;
    }

    public void setPropertys(String propertys) {
        this.propertys = propertys;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLandBuild() {
        return landBuild;
    }

    public void setLandBuild(String landBuild) {
        this.landBuild = landBuild;
    }

    public String getPriceInc() {
        return priceInc;
    }

    public void setPriceInc(String priceInc) {
        this.priceInc = priceInc;
    }

    public String getBookingFee() {
        return bookingFee;
    }

    public void setBookingFee(String bookingFee) {
        this.bookingFee = bookingFee;
    }

    public String getSpecialEnquiries() {
        return specialEnquiries;
    }

    public void setSpecialEnquiries(String specialEnquiries) {
        this.specialEnquiries = specialEnquiries;
    }

    public String getBookingContact() {
        return bookingContact;
    }

    public void setBookingContact(String bookingContact) {
        this.bookingContact = bookingContact;
    }
}
