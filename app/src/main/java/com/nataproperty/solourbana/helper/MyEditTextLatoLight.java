package com.nataproperty.solourbana.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by UserModel on 4/29/2016.
 */
public class MyEditTextLatoLight extends EditText {
    private Context context;
    private AttributeSet attrs;
    private int defStyle;

    public MyEditTextLatoLight(Context context) {
        super(context);
        this.context=context;
        init();
    }

    public MyEditTextLatoLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        this.attrs=attrs;
        init();
    }

    public MyEditTextLatoLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context=context;
        this.attrs=attrs;
        this.defStyle=defStyle;
        init();
    }

    private void init() {
        Typeface font=Typeface.createFromAsset(getContext().getAssets(),"fonts/Lato-Light.ttf");
        this.setTypeface(font);
    }
    @Override
    public void setTypeface(Typeface tf, int style) {
        tf=Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Light.ttf");
        super.setTypeface(tf, style);
    }

    @Override
    public void setTypeface(Typeface tf) {
        tf=Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Light.ttf");
        super.setTypeface(tf);
    }
}
