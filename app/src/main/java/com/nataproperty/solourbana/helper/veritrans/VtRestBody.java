package com.nataproperty.solourbana.helper.veritrans;

/**
 * Created by UserModel on 5/22/2016.
 */
public class VtRestBody {
    public String status_code;
    public String status_message;
    public String transaction_id;
    public String masked_card;
    public String order_id;
    public String payment_type;

    //TODO: Add any parameter you need
}
