package com.nataproperty.solourbana.helper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by nata on 11/14/2016.
 */
public class LoadingBar {
    private static AlertDialog dialog;

    public static void startLoader(Context context){

        dialog = new ProgressDialog(context);
        dialog.setMessage("Mohon Tunggu...");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.show();
    }
    public static void stopLoader(){
        dialog.dismiss();
    }
}
