package com.nataproperty.solourbana.helper;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Utils {

    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    /**
     * format harga, dikasih separator ribuan
     *
     * @param currency  value currency
     * @return
     */
    public static String currencyFormat(String currency) {
        if (currency == null) {
            currency = "0";
        }
        double currPrice = Double.valueOf(currency);
        DecimalFormat format = new DecimalFormat("#,###");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    /**
     * method untuk conversi dari model ke dalam json object
     *
     * @param loginParam parameter yang akan diconversi, dalam bentuk class model
     * @return String Json.
     */
    public static String modelToJson(Object loginParam) {
        Gson gson = new Gson();
        String jsonIn = gson.toJson(loginParam);
        Log.d("utils", "json in " + jsonIn);
        return jsonIn;
    }
}
