package com.nataproperty.solourbana.helper.veritrans;

import android.content.Context;
import android.view.MotionEvent;
import android.webkit.WebView;

/**
 * Created by UserModel on 5/22/2016.
 */
public class CustomWebView extends WebView {
    public CustomWebView(Context context)
    {
        super(context);
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
                if (!hasFocus())
                    requestFocus();
                break;
        }

        return super.onTouchEvent(event);
    }
}
