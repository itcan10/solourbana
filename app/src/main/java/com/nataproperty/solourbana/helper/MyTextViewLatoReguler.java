package com.nataproperty.solourbana.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by UserModel on 4/29/2016.
 */
public class MyTextViewLatoReguler extends TextView {
    public MyTextViewLatoReguler(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewLatoReguler(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewLatoReguler(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        setTypeface(tf ,1);

    }
}
