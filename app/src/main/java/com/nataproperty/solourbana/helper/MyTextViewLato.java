package com.nataproperty.solourbana.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by UserModel on 4/29/2016.
 */
public class MyTextViewLato extends TextView {
    public MyTextViewLato(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewLato(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewLato(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Black.ttf");
        setTypeface(tf ,1);

    }
}
