package com.nataproperty.solourbana.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by UserModel on 4/29/2016.
 */
public class MyTextViewLatoBold extends TextView {
    public MyTextViewLatoBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewLatoBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewLatoBold(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Bold.ttf");
        setTypeface(tf ,1);

    }
}
