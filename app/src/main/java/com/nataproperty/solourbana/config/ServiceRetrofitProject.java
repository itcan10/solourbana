package com.nataproperty.solourbana.config;


import android.database.Observable;
import android.util.LruCache;

import com.nataproperty.solourbana.view.before_login.model.VersionModel;
import com.nataproperty.solourbana.view.commision.model.CommisionSectionModel;
import com.nataproperty.solourbana.view.event.model.EventModel;
import com.nataproperty.solourbana.view.event.model.MyTicketModel;
import com.nataproperty.solourbana.view.logbook.model.ContactActionModel;
import com.nataproperty.solourbana.view.logbook.model.ContactInfo;
import com.nataproperty.solourbana.view.logbook.model.ContactProjectModel;
import com.nataproperty.solourbana.view.logbook.model.LogBookModel;
import com.nataproperty.solourbana.view.logbook.model.LogBookSectionModel;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.logbook.model.TaskModel;
import com.nataproperty.solourbana.view.menuitem.model.ContactUsModel;
import com.nataproperty.solourbana.view.menuitem.model.NotificationModel;
import com.nataproperty.solourbana.view.menuitem.model.StatusTokenGCMModel;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingModel;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingSectionModel;
import com.nataproperty.solourbana.view.mynup.model.MyNupSectionModel;
import com.nataproperty.solourbana.view.nup.model.QtyNupModel;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.CountryModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;
import com.nataproperty.solourbana.view.project.model.DetailStatusModel;
import com.nataproperty.solourbana.view.project.model.LocationModel;
import com.nataproperty.solourbana.view.project.model.ProjectModelNew;
import com.nataproperty.solourbana.view.report.model.CheckAvailableReportModel;
import com.nataproperty.solourbana.view.report.model.ReportModel;
import com.nataproperty.solourbana.view.report.model.ResponGenerateReportModel;
import com.nataproperty.solourbana.view.scanQRCode.model.ScanURLModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by nata on 11/22/2016.
 */

public class ServiceRetrofitProject {
    private NetworkAPI networkAPI;
    private OkHttpClient okHttpClient;
    private LruCache<Class<?>, Observable<?>> apiObservables;

    public ServiceRetrofitProject() {
        this(WebService.URL_PROJECT);
    }

    public ServiceRetrofitProject(String baseUrl) {
        okHttpClient = buildClient();
        apiObservables = new LruCache<>(10);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }

    /**
     * Method to return the API interface.
     *
     * @return
     */
    public NetworkAPI getAPI() {
        return networkAPI;
    }


    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     *
     * @return
     */
    public OkHttpClient buildClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                // Do anything with response here
                //if we ant to grab a specific cookie or something..
                return response;
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }
        });

        return builder.build();
    }

    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache() {
        apiObservables.evictAll();
    }

    /**
     * all the Service alls to use for the retrofit requests.
     */
    public interface NetworkAPI {
        @FormUrlEncoded
        @POST("getAppVersionProjectSvc")
        Call<VersionModel> getNewVersion(@Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("GetLocationLookupProjectSvc")
        Call<List<LocationModel>> getLocation(@Field("memberRef") String memberRef,
                                              @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("getListProjectForProjectSvc")
        Call<List<ProjectModelNew>> getListProject(@Field("memberRef") String memberRef,
                                                   @Field("locationRef") String locationRef,
                                                   @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("getListEventProjectSvc")
        Call<ArrayList<EventModel>> getListEvent(@Field("contentDate") String contentDate,
                                                 @Field("memberRef") String memberRef,
                                                 @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("getListBookingForProjectSvc")
        Call<List<MyBookingModel>> getListBook(@Field("memberRef") String memberRef,
                                               @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("GetSectionMyNUPForProject")
        Call<List<MyNupSectionModel>> getListNupSection(@Field("memberRef") String memberRef,
                                                        @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("GetSectionMyBookingProject")
        Call<List<MyBookingSectionModel>> GetSectionMyBookingProject(@Field("memberRef") String memberRef,
                                                                     @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("GetSectionCommissionForProject")
        Call<List<CommisionSectionModel>> getListCommisionSection(@Field("memberRef") String memberRef,
                                                                  @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("getListTiketRsvpForProject")
        Call<ArrayList<MyTicketModel>> getListTiketRsvp(@Field("memberRef") String memberRef,
                                                        @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("getListNotificationForProjectSvc")
        Call<ArrayList<NotificationModel>> getListNotificationSSTSvc(@Field("memberRef") String memberRef,
                                                                     @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("GetContactBankSvc")
        Call<ArrayList<LogBookModel>> GetContactBankSvc(@Field("memberRef") String memberRef,
                                                        @Field("customerStatusRef") String customerStatusRef,
                                                        @Field("keyword") String keyword,
                                                        @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("GetContactBankProjectSvc")
        Call<ArrayList<LogBookModel>> GetContactBankProjectSvc(@Field("dbMasterRef") String dbMasterRef,
                                                               @Field("projectRef") String projectRef,
                                                               @Field("memberRef") String memberRef,
                                                               @Field("projectCustomerStatusRef") String projectCustomerStatusRef,
                                                               @Field("eventRef") String eventRef,
                                                               @Field("referralRef") String referralRef,
                                                               @Field("keyword") String keyword);

        @FormUrlEncoded
        @POST("GetLookupContactProjectSvc")
        Call<ContactProjectModel> GetLookupContactProjectSvc(@Field("dbMasterRef") String dbMasterRef,
                                                             @Field("projectRef") String projectRef,
                                                             @Field("memberRef") String memberRef);

        @GET("getCountryListSvc")
        Call<ArrayList<CountryModel>> getCountryListSvc();

        @FormUrlEncoded
        @POST("getProvinceListSvc")
        Call<ArrayList<ProvinceModel>> getProvinceListSvc(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListSvc")
        Call<ArrayList<CityModel>> getCityListSvc(@Field("countryCode") String countryCode,
                                                  @Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("insertContactBankSvc")
        Call<ResponeModel> insertContactBankSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("insertContactBankProjectSvc")
        Call<ResponeModel> insertContactBankProjectSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("GetInfoContactBankSvc")
        Call<ContactInfo> GetInfoContactBankSvc(@Field("memberRef") String memberRef,
                                                @Field("customerRef") String customerRef,
                                                @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("updateContactBankSvc")
        Call<ResponeModel> updateContactBankSvc(@FieldMap Map<String, String> fields);


        @FormUrlEncoded
        @POST("GetSectionContactBankProjectSvc")
        Call<ArrayList<LogBookSectionModel>> GetSectionContactBankProjectSvc(@Field("memberRef") String memberRef,
                                                                             @Field("dbMasterRef") String dbMasterRef,
                                                                             @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("GetContactBankTaskSvc")
        Call<ArrayList<LogBookModel>> GetContactBankTaskSvc(@Field("memberRef") String memberRef,
                                                            @Field("dbMasterRef") String dbMasterRef,
                                                            @Field("projectRef") String projectRef,
                                                            @Field("customerStatusRef") String customerStatusRef,
                                                            @Field("keyword") String keyword,
                                                            @Field("countryCode") String countryCode,
                                                            @Field("provinceCode") String provinceCode,
                                                            @Field("cityCode") String cityCode);

        @FormUrlEncoded
        @POST("GetLookupProjectAction")
        Call<ContactActionModel> GetLookupProjectAction(@Field("dbMasterRef") String dbMasterRef,
                                                        @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("GetContactProjectTaskListSvc")
        Call<ArrayList<TaskModel>> GetContactProjectTaskListSvc(@Field("dbMasterRef") String dbMasterRef,
                                                                @Field("projectRef") String projectRef,
                                                                @Field("customerRef") String customerRef);

        @FormUrlEncoded
        @POST("insertTaskContactBankSvc")
        Call<ResponeModel> insertTaskContactBankSvc(@FieldMap Map<String, String> fields);


        @FormUrlEncoded
        @POST("getProvinceListAllSvc")
        Call<ArrayList<ProvinceModel>> getProvinceListAllSvc(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListAllSvc")
        Call<ArrayList<CityModel>> getCityListAllSvc(@Field("countryCode") String countryCode,
                                                     @Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("saveGCMTokenForProjectSvc")
        Call<StatusTokenGCMModel> postGCMToken(@Field("GCMToken") String GCMToken,
                                               @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("CheckAvailableReportSvc")
        Call<CheckAvailableReportModel> CheckAvailableReportSvc(@Field("projectCode") String projectCode,
                                                                @Field("dbMasterRef") String dbMasterRef,
                                                                @Field("projectRef") String projectRef,
                                                                @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetListDashboardReport")
        Call<ArrayList<ReportModel>> GetListDashboardReport(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetListDashboardReport_v2")
        Call<ArrayList<ReportModel>> GetListDashboardReport2(@Field("memberRef") String memberRef,
                                                            @Field("dbMasterRef") String dbMasterRef,
                                                            @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("GenerateDashboardReportSvc")
        Call<ResponGenerateReportModel> GenerateDashboardReportSvc(@Field("projectCode") String projectCode,
                                                                   @Field("dbMasterRef") String dbMasterRef,
                                                                   @Field("projectRef") String projectRef,
                                                                   @Field("memberRef") String memberRef,
                                                                   @Field("userRef") String userRef);

        @FormUrlEncoded
        @POST("GenerateDashboardReportSvc_v2")
        Call<ResponGenerateReportModel> GenerateDashboardReportSvc2(@Field("projectCode") String projectCode,
                                                                   @Field("dbMasterRef") String dbMasterRef,
                                                                   @Field("projectRef") String projectRef,
                                                                   @Field("memberRef") String memberRef,
                                                                   @Field("userRef") String userRef,
                                                                   @Field("aspx") String aspx);

        @FormUrlEncoded
        @POST("getProjectDetailSvc")
        Call<DetailStatusModel> getProjectDetailSvc(@Field("projectCode") String projectCode,
                                                    @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("CheckingDocumentProject")
        Call<ScanURLModel> GenerateDocumentProjectQRCode(@Field("projectCode") String projectCode,
                                                         @Field("bookingCode") String bookingCode);

        @FormUrlEncoded
        @POST("saveGCMTokenProjectSvc")
        Call<StatusTokenGCMModel> saveGCMTokenProjectSvc(@Field("GCMToken") String GCMToken,
                                               @Field("memberRef") String memberRef,
                                               @Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("GetQtyNUPList")
        Call<ArrayList<QtyNupModel>> GetQtyNUPList(@Field("dbMasterRef") String dbMasterRef,
                                                   @Field("projectRef") String projectRef,
                                                   @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("saveFeedbackProjectSvc")
        Call<ContactUsModel> saveFeedbackProjectSvc(@Field("contactName") String contactName,
                                                    @Field("contactEmail") String contactEmail,
                                                    @Field("contactSubject") String contactSubject,
                                                    @Field("contactMessage") String contactMessage,
                                                    @Field("projectCode") String projectCode);

    }

}
