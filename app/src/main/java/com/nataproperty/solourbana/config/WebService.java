package com.nataproperty.solourbana.config;

/**
 * Created by Administrator on 3/31/2016.
 */
public class WebService {
//    public static final String BASE_URL = "http://192.168.10.229/services/";
//    public static final String BASE_URL = "http://172.20.10.4/services/";
    public static final String BASE_URL = "http://www.nataproperty.com/services/";
    public static final String URL = BASE_URL + "AppsServices.asmx/";
    public static final String URL_PROJECT = BASE_URL + "AppsServicesProject.asmx/";
    public static final String URL_IMAGE = BASE_URL + "Assets/support/displayImage.aspx";
    public static final String URL_DOWNLOAD = BASE_URL + "Assets/support/displayAttachment.aspx";

    //Notification
    public static final String notificationDowloadProject = "1";
    public static final String notificationNews = "2";
    public static final String notificationNewProject = "3";
    public static final String notificationGeneral = "4";
    public static final String notificationUpdate = "5";
    public static final String notificationEventDetail = "8";
    public static final String notificationApprovalProject = "AP";

    //PaymentMethod
    public static final String updatePaymentTypeBankTransfer = "7";

    //projectDoc
    public static final String projectKtpType = "1";
    public static final String projectNPWPType = "4";

    //Verison
    private static String version = "getAppVersionSvc";

    //Project
    private static String project = "getListProjectSvc";
    private static String category = "getListCategorySvc";
    private static String cluster = "getListClusterSvc";
    private static String product = "getListProductSvc";
    private static String productDetail = "getProductDetailSvc";
    private static String productListImage = "getListProductImageSvc";
    private static String feature = "getListFeatureSvc";
    private static String commision = "getProjectCommisionSvc";
    private static String ChangeMemberIDSvc = "ChangeMemberIDSvc";
    private static String subscribe = "subscribeProjectMemberCodeSvc";
    private static String UnSubscribe = "unSubscribe";
    private static String subscribeProjectInhouse = "subscribeProjectInhouseSvc";

    private static String projectImage = "?is=project&dr=";
    private static String categoryImage = "?is=category&dr=";
    private static String clusterImage = "?is=cluster&dr=";
    private static String productImage = "?is=product&dr=";
    private static String logoProject = "?is=projectlogo&dr=";

    private static String profileImage = "?is=profile&dt=5&mr=";
    private static String login = "cekLogin";
    private static String cekLoginCustomer = "cekLoginCustomer";
    private static String registerAgency = "registerAgency";
    private static String registerSalesAgent = "registerSalesAgent";
    private static String signInGoogle = "cekRegisterByGoogle";
    private static String registerCustomerSvc = "RegisterCustomerSvc";
    private static String saveRegisterCustomerSvc = "SaveRegisterCustomerSvc";
    private static String registerFacebook = "cekRegisterByFacebook";

    private static String companyCode = "getAgencyCompanyInfoByCode"; //companycode
    private static String agencyCompany = "getListAgencyCompanySvc"; //company
    private static String forgotPassword = "forgotPassword";
    private static String aboutMe = "updateAboutMeSvc";
    private static String ktp = "?is=ktp&dr=";
    private static String npwp = "?is=npwp&dr=";
    private static String projectKtp = "?is=projectktp&ps=";
    private static String projectNpwp = "?is=projectnpwp&ps=";
    private static String deleteKtp = "deleteKTPSvc";
    private static String deleteNpwp = "deleteNPWPSvc";
    private static String cekEmailRegisterSvc = "cekEmailRegisterSvc";

    private static String memberType = "getMemberTypeProject";
    //addr
    private static String country = "getCountryListSvc";
    private static String province = "getProvinceListSvc";
    private static String city = "getCityListSvc";

    private static String memberInfo = "getMemberInfoSvc";
    private static String SyncPsCodeSvc = "SyncPsCodeSvc";

    private static String profileReuired = "updateRequiredProfileSvc";
    private static String idAddress = "updateIDAddressSvc";
    private static String changePassword = "changePasswordSvc";
    private static String upload = "updateImageProfileSvc";
    private static String updateImageKtpSvc = "updateImageKtpSvc";
    private static String updateImageNpwpSvc = "updateImageNpwpSvc";
    private static String updateImageOrderSvc = "updateImageOrderSvc";

    //ilustration
    private static String ilustration = "getListPaymentIlustrationSvc";
    private static String clusterInfo = "getClusterInfoSvc";
    private static String paymentTerm = "getIlustrationPaymentTermSvc_v2";
    private static String payment = "getIlustrationPaymentSvc";
    private static String download = "getListDownloadSvc";

    //nup
    private static String termAndConditionNUP = "getTermAndConditionNUPSvc";
    private static String costumer = "searchCustomerSvc";
    private static String costumer_v2 = "searchCustomerSvc_v2";
    private static String costumerRegister = "registerNewCustomerSvc";
    private static String saveOrderNup = "saveOrderNUPSvc";
    private static String getOrderInfo = "getOrderInfoSvc";
    private static String year = "getListYearSvc";

    private static String GetLookupSurveyNUP = "GetLookupSurveyNUPSvc";

    //booking
    private static String termAndConditionBooking = "getTermAndConditionBookingSvc";
    private static String saveOrderBooking = "saveOrderBookingSvc";
    private static String getLookupAdditionalInfoBooking = "GetLookupAdditionalInfoBookingSvc";

    //My Nup
    private static String generateNUP = "generateNUPSvc";

    //News
    private static String listNews = "getListNewsSvc";
    private static String listNewsProject = "getListNewsProjectSvc";
    private static String getListPromoProjectSvc = "getListPromoProjectSvc";
    private static String newsDetail = "getNewsImageSliderSvc";

    //GCM
    private static String getGCMMessageSvc = "getGCMMessageSvc";

    //calculate
    private static String savePaymentTermCalcUserProject = "savePaymentTermCalcUserProject";
    private static String getInfoPayment = "getInfoPaymentTermCalcUser";
    private static String getListCalcUserProjectIlustration = "getListCalcUserProjectIlustration";
    private static String getListCalcCluster = "getListCalcCluster";
    private static String getListCalcProduct = "getListCalcProduct";
    private static String getListCalcBlock = "getListCalcBlock";
    private static String getInfoCalcUserProject = "getInfoCalcUserProject";
    private static String deleteTemplateCalcUserProject = "deleteTemplateCalcUserProject";
    private static String deleteProjectCalcUserProject = "deleteProjectCalcUserProject";
    private static String sendToFriendCalcUserProject = "sendToFriendCalcUserProject";

    /*Gallery*/
    private static String getListGroupGallery = "getListGroupGallerySvc";
    private static String getListProjectGallery = "getListProjectGallerySvc";

    ///Project
    private static String registerInhouseForProject = "registerInhouseForProject";
    private static String getListNewsPagingProjectSvc = "getListNewsPagingProjectSvc";
    private static String getListNewsProjectSvc = "getListNewsForProjectSvc";
    private static String getListProjectForProjectSvc = "getListProjectForProjectSvc";
    private static String getListTypeNUPForProject = "getListTypeNUPForProject";
    private static String getUnpaidNUPForProjectSvc = "getUnpaidNUPForProjectSvc";
    private static String getNeedAttentionNUPForProjectSvc = "getNeedAttentionNUPForProjectSvc";
    private static String getWaitingVerificationForProjectSvc = "getWaitingVerificationForProjectSvc";
    private static String getPaidNUPForProjectSvc = "getPaidNUPForProjectSvc";
    private static String getMyListingForProjectSvc = "getMyListingForProjectSvc";
    private static String getListProjectKeywordForProjectSvc = "getListProjectKeywordForProjectSvc";
    private static String sendToFriendForProject = "sendToFriendForProject";

    //VersionCode
    public static String getVersionCode() {
        return URL + version;
    }

    public static String getLogin() {
        return URL_PROJECT + login;
    }

    public static String getLoginCustomer() {
        return URL_PROJECT + cekLoginCustomer;
    }

    public static String getRegisterAgency() {
        return URL_PROJECT + registerAgency;
    }

    public static String getRegisterSalesAgent() {
        return URL_PROJECT + registerSalesAgent;
    }

    public static String signInGoogle() {
        return URL_PROJECT + signInGoogle;
    }

    public static String getCompanyCode() {
        return URL + companyCode;
    }

    public static String getAgencyCompany() {
        return URL + agencyCompany;
    }

    public static String getForgotPassword() {
        return URL + forgotPassword;
    }

    public static String getMemberType() {
        return URL_PROJECT + memberType;
    }

    public static String cekEmailRegisterSvc() {
        return URL + cekEmailRegisterSvc;
    }

    public static String getMemberInfo() {
        return URL + memberInfo;
    }

    public static String SyncPsCodeSvc() {
        return URL + SyncPsCodeSvc;
    }

    public static String getProfile() {
        return URL_IMAGE + profileImage;
    }

    public static String profileReuired() {
        return URL + profileReuired;
    }

    public static String idAddress() {
        return URL + idAddress;
    }

    public static String changePassword() {
        return URL + changePassword;
    }

    public static String getAboutMe() {
        return URL + aboutMe;
    }

    public static String getKtp() {
        return URL_IMAGE + ktp;
    }

    public static String getNpwp() {
        return URL_IMAGE + npwp;
    }

    public static String getProjectKtp() {
        return URL_IMAGE + projectKtp;
    }

    public static String getProjectNpwp() {
        return URL_IMAGE + projectNpwp;
    }

    public static String deleteKtp() {
        return URL + deleteKtp;
    }

    public static String deleteNpwp() {
        return URL + deleteNpwp;
    }

    public static String updateImageProfileSvc() {
        return URL + upload;
    }

    public static String updateImageKtpSvc() {
        return URL + updateImageKtpSvc;
    }

    public static String updateImageNpwpSvc() {
        return URL + updateImageNpwpSvc;
    }

    public static String getUpdateImageOrderSvc(){
        return URL + updateImageOrderSvc;
    }

    public static String getCountry() {
        return URL + country;
    }

    public static String getProvince() {
        return URL + province;
    }

    public static String getCity() {
        return URL + city;
    }

    public static String getProject() {
        return URL + project;
    }

    public static String getCategory() {
        return URL + category;
    }

    public static String getCluster() {
        return URL + cluster;
    }

    public static String getProduct() {
        return URL + product;
    }

    public static String getProductDetail() {
        return URL + productDetail;
    }

    public static String getListProductImage() {
        return URL + productListImage;
    }

    public static String getFeatureList() {
        return URL + feature;
    }

    //ilustration
    public static String getIlustration() {
        return URL + ilustration;
    }

    public static String getPaymentTerm() {
        return URL + paymentTerm;
    }

    public static String getPayment() {
        return URL + payment;
    }

    public static String getListDownload() {
        return URL + download;
    }

    public static String getCommision() {
        return URL + commision;
    }

    public static String ChangeMemberIDSvc() {
        return URL + ChangeMemberIDSvc;
    }

    public static String subscribeProject() {
        return URL + subscribe;
    }

    public static String subscribeProjectInhouse() {
        return URL + subscribeProjectInhouse;
    }

    public static String unSubscribeProject() {
        return URL + UnSubscribe;
    }

    //Nup
    public static String getTermAndCondition() {
        return URL + termAndConditionNUP;
    }

    public static String getCostumer() {
        return URL + costumer;
    }

    public static String getCostumer_v2(){
        return URL + costumer_v2;
    }

    public static String getCostumerRegister() {
        return URL + costumerRegister;
    }

    public static String saveOrderNup() {
        return URL + saveOrderNup;
    }

    public static String getOrderInfo() {
        return URL + getOrderInfo;
    }

    public static String getYear() {
        return URL + year;
    }

    public static String GetLookupSurveyNUP() {
        return URL + GetLookupSurveyNUP;
    }

    //Booking
    public static String getTermAndConditionBooking() {
        return URL + termAndConditionBooking;
    }

    public static String saveOrderBooking() {
        return URL + saveOrderBooking;
    }

    public static String getLookupAdditionalInfoBooking() {
        return URL + getLookupAdditionalInfoBooking;
    }

    public static String generateNUP() {
        return URL + generateNUP;
    }

    //News
    public static String getNews() {
        return URL + listNews;
    }

    public static String getNewsProject() {
        return URL_PROJECT + listNewsProject;
    }

    public static String getListPromoProjectSvc() {
        return URL_PROJECT + getListPromoProjectSvc;
    }

    public static String getImageSlider() {
        return URL + newsDetail;
    }

    public static String getGCMMessage() {
        return URL + getGCMMessageSvc;
    }

    public static String savePaymentTermCalcUserProject() {
        return URL + savePaymentTermCalcUserProject;
    }

    public static String getInfoPayment() {
        return URL + getInfoPayment;
    }

    public static String getListCalcUserProjectIlustration() {
        return URL + getListCalcUserProjectIlustration;
    }

    public static String getListCalcCluster() {
        return URL + getListCalcCluster;
    }

    public static String getListCalcProduct() {
        return URL + getListCalcProduct;
    }

    public static String getListCalcBlock() {
        return URL + getListCalcBlock;
    }

    public static String getInfoCalcUserProject() {
        return URL + getInfoCalcUserProject;
    }

    public static String deleteTemplateCalcUserProject() {
        return URL + deleteTemplateCalcUserProject;
    }

    public static String deleteProjectCalcUserProject() {
        return URL + deleteProjectCalcUserProject;
    }

    public static String sendToFriendCalcUserProject() {
        return URL + sendToFriendCalcUserProject;
    }

    //image
    public static String getProjectImage() {
        return URL_IMAGE + projectImage;
    }

    public static String getCategoryImage() {
        return URL_IMAGE + categoryImage;
    }

    public static String getClusterImage() {
        return URL_IMAGE + clusterImage;
    }

    public static String getProductImage() {
        return URL_IMAGE + productImage;
    }

    public static String getProductImageDetail() {
        return URL_IMAGE;
    }

    //gallery
    public static String getListGroupGallery() {
        return URL + getListGroupGallery;
    }

    public static String getListProjectGallery() {
        return URL + getListProjectGallery;
    }


    ///PROJECT
    public static String registerInhouseForProject() {
        return URL_PROJECT + registerInhouseForProject;
    }

    public static String getListNewsPagingProjectSvc() {
        return URL_PROJECT + getListNewsPagingProjectSvc;
    }

    public static String getListNewsProjectSvc() {
        return URL_PROJECT + getListNewsProjectSvc;
    }

    public static String getListProjectForProjectSvc() {
        return URL_PROJECT + getListProjectForProjectSvc;
    }

    public static String getListTypeNUPForProject() {
        return URL_PROJECT + getListTypeNUPForProject;
    }

    public static String getUnpaidNUPForProjectSvc() {
        return URL_PROJECT + getUnpaidNUPForProjectSvc;
    }

    public static String getNeedAttentionNUPForProjectSvc() {
        return URL_PROJECT + getNeedAttentionNUPForProjectSvc;
    }

    public static String getWaitingVerificationForProjectSvc() {
        return URL_PROJECT + getWaitingVerificationForProjectSvc;
    }

    public static String getPaidNUPForProjectSvc() {
        return URL_PROJECT + getPaidNUPForProjectSvc;
    }

    public static String getMyListingForProjectSvc() {
        return URL_PROJECT + getMyListingForProjectSvc;
    }

    public static String getListProjectKeywordForProjectSvc() {
        return URL_PROJECT + getListProjectKeywordForProjectSvc;
    }

    public static String sendToFriendForProject() {
        return URL_PROJECT + sendToFriendForProject;
    }

    public static String getRegisterCustomerSvc() {
        return URL_PROJECT + registerCustomerSvc;
    }

    public static String getSaveRegisterCustomerSvc() {
        return URL_PROJECT + saveRegisterCustomerSvc;
    }

    public static String getRegisterFacebook() {
        return URL_PROJECT + registerFacebook;
    }
}
