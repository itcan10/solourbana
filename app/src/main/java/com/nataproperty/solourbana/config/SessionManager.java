package com.nataproperty.solourbana.config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

import static com.nataproperty.solourbana.config.General.CLUSTER_NAME;


public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static SessionManager instance;

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(General.PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static SessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    /**
     * Create login session
     **/
    public void sessionProject(String isJoin, String isWaiting, String dbMasterRef, String projectRef, String projectName ,String countCategory,
                               String countCluster, String clusterRef,
                               String salesStatus, String isBooking, String isNUP,
                               String isShowAvailableUnit, String latitude, String longitude,
                               String nupAmt, String linkDetail, String urlVideo, String imageLogo,
                               String bookingContact, String downloadProjectInfo, String clusterName,
                               String categoryRef, String priceRange,String projectWA,String projectEmail,String projectPhone,String psCode, String memberCode) {
        editor.putString(General.IS_JOIN, isJoin);
        editor.putString(General.IS_WAITING, isWaiting);
        editor.putString(General.DBMASTER_REF, dbMasterRef);
        editor.putString(General.PROJECT_REF, projectRef);
        editor.putString(General.PROJECT_NAME, projectName);
        editor.putString(General.COUNT_CATEGORY, countCategory);
        editor.putString(General.COUNT_CLUSTER, countCluster);
        editor.putString(General.CLUSTER_REF, clusterRef);
        editor.putString(General.SALES_STATUS, salesStatus);
        editor.putString(General.IS_BOOKING, isBooking);
        editor.putString(General.IS_NUP, isNUP);
        editor.putString(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
        editor.putString(General.LATITUDE, latitude);
        editor.putString(General.LONGITUDE, longitude);
        editor.putString(General.URL_VIDEO, urlVideo);
        editor.putString(General.IMAGE_LOGO, imageLogo);
        editor.putString(General.BOOKING_CONTACT, bookingContact);
        editor.putString(General.LINK_DETAIL, linkDetail);
        editor.putString(General.NUP_AMT, nupAmt);
        editor.putString(General.DOWNLOAD_PROJECT_INFO, downloadProjectInfo);
        editor.putString(General.CLUSTER_NAME, clusterName);
        editor.putString(General.CATEGORY_REF, categoryRef);
        editor.putString(General.PRICE_RANGE, priceRange);
        editor.putString(General.PROJECT_WA, projectWA);
        editor.putString(General.PROJECT_EMAIL, projectEmail);
        editor.putString(General.PROJECT_PHONE, projectPhone);
        editor.putString(General.PS_CODE, psCode);
        editor.putString(General.MEMBER_CODE, memberCode);
        editor.commit();
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getProjectSession() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(General.DBMASTER_REF, pref.getString(General.DBMASTER_REF, "0"));
        user.put(General.PROJECT_REF, pref.getString(General.PROJECT_REF, ""));
        user.put(General.PROJECT_NAME, pref.getString(General.PROJECT_NAME, ""));
        user.put(General.COUNT_CATEGORY, pref.getString(General.COUNT_CATEGORY, ""));
        user.put(General.COUNT_CLUSTER, pref.getString(General.COUNT_CLUSTER, ""));
        user.put(General.CLUSTER_REF, pref.getString(General.CLUSTER_REF, ""));
        user.put(General.SALES_STATUS, pref.getString(General.SALES_STATUS, ""));
        user.put(General.IS_BOOKING, pref.getString(General.IS_BOOKING, ""));
        user.put(General.IS_NUP, pref.getString(General.IS_NUP, ""));
        user.put(General.IS_SHOW_AVAILABLE_UNIT, pref.getString(General.IS_SHOW_AVAILABLE_UNIT, ""));
        user.put(General.LATITUDE, pref.getString(General.LATITUDE, null));
        user.put(General.LONGITUDE, pref.getString(General.LONGITUDE, ""));
        user.put(General.URL_VIDEO, pref.getString(General.URL_VIDEO, ""));
        user.put(General.IMAGE_LOGO, pref.getString(General.IMAGE_LOGO, ""));
        user.put(General.BOOKING_CONTACT, pref.getString(General.BOOKING_CONTACT, ""));
        user.put(General.LINK_DETAIL, pref.getString(General.LINK_DETAIL, ""));
        user.put(General.NUP_AMT, pref.getString(General.NUP_AMT, null));
        user.put(General.DOWNLOAD_PROJECT_INFO, pref.getString(General.DOWNLOAD_PROJECT_INFO, ""));
        user.put(CLUSTER_NAME, pref.getString(CLUSTER_NAME, ""));
        user.put(General.CATEGORY_REF, pref.getString(General.CATEGORY_REF, ""));
        user.put(General.PRICE_RANGE, pref.getString(General.PRICE_RANGE, ""));
        user.put(General.PROJECT_WA, pref.getString(General.PROJECT_WA, ""));
        user.put(General.PROJECT_EMAIL, pref.getString(General.PROJECT_EMAIL, ""));
        user.put(General.PROJECT_PHONE, pref.getString(General.PROJECT_PHONE, ""));
        user.put(General.PS_CODE, pref.getString(General.PS_CODE, ""));
        user.put(General.MEMBER_CODE, pref.getString(General.MEMBER_CODE, ""));
        return user;
    }

    /**
     * Quick check for login
     **/

    public String getStringFromSP(String valName) {
        return pref.getString(valName, "");
    }

    public void setStringToSP(String valName, String value) {
        editor.putString(valName, value);
        editor.commit();
    }

    public void setIntToSP(String valName, int value) {
        editor.putInt(valName, value);
        editor.commit();
    }

    public int getIntFromSP(String valName) {
        return pref.getInt(valName, 0);
    }

}
