package com.nataproperty.solourbana.config;

/**
 * Created by User on 5/13/2016.
 * <p>
 * class untuk menempatkan variable static yang bersifat public
 */
public class General {

    public static final String DEVELOPER_KEY = "AIzaSyD3APAJoaSZTqBaH57P4OCfrh2rzxxUrsQ";

    public static final String projectCode = "17"; //17 solourbana
    public static final String projectCodeSingleProject = "SP";
    public static final String customerStatusNewRef = "1";
    public static final String customerStatusVerifyRef = "3";
    public static final String projectCustomerStatus = "1";
    public static final String coutryCode = "INA";

    public static final String docTypeKtp = "1";
    public static final String docTypeNpwp = "4";

    public static final String projectCustomerStatusNew = "1";
    public static final String projectCustomerStatusProgress = "2";
    public static final String projectCustomerStatusNotInterest = "3";
    public static final String projectCustomerStatusInterest = "4";
    public static final String projectCustomerStatusFinish = "5";

    //untuk cek open sales agent
    public static final String salesStatusOpen = "2";

    // test doangan
    //tambahan latan
    //ini untuk booking review order activity
    public static final String bookingSource = "5";

    //    response error code, variabel ini ditentukan oleh developer.
//    dipanggil ketika error response network
    public static final int NETWROK_ERROR_JSON = 1;
    public static final int NETWORK_ERROR_CONNCECTION = 2;
    public static final int NETWORK_ERROR_PERMISSION_ACCESS = 3;

    //latlong untuk lokasi marketing di contact us
    public static final double LATITUDE_CODE = -7.5669723;
    public static final double LONGITUDE_CODE = 110.8343205;

    // state identifikasi login, apakah login customer atau login kategori user selain customer
    public static final int LOGIN_GENERAL = 1;
    public static final int LOGIN_CUSTOMER = 2;
    public static final String INTENT_CAT_LOGIN = "kategori_login";

    //untuk set variable di project menu
    public static final String PROJECT_INFORMASI = "projectInformasi";
    public static final String PROJECT_SEARCH_UNIT = "projectSearchUnit";
    public static final String PROJECT_DOWNLOAD = "projectDownload";
    public static final String PROJECT_NUP = "projectNup";
    public static final String PROJECT_BOOKING = "projectBooking";
    public static final String PROJECT_KOMISI = "projectKomisi";
    public static final String PROJECT_GALLERY = "projectGallery";
    public static final String PROJECT_NEWS = "projectNews";
    public static final String PROJECT_PROMO = "projectPromo";
    public static final String PROJECT_LOOK_BOOK = "projectLookBook";
    public static final String PROJECT_REPORT = "projectReport";
    public static final String PROJECT_MYNUP = "projectMyNup";
    public static final String PROJECT_EVENT = "projectEvent";
    public static final String PROJECT_MYBOOKING = "projectMyBooking";
    public static final String PROJECT_TICKET = "projectTicet";
    public static final String PROJECT_QRCODE = "projectQrCode";

    //MemberType
    public static final String customer = "1";
    public static final String propertyAgency = "2";
    public static final String independent = "3";
    public static final String inhouse = "4";

    //whatapss
    public static final String PACKAGE_WHATSHAPP = "com.whatsapp";

    //set key untuk guest
    public static final String GUEST = "guest";
    public static final String  IS_GUEST = "isGuest";
    public static final String  PS_CODE = "psCode";
    public static final String  MEMBER_CODE = "memberCode";

    public static final String IS_MEMBER_REF = "isMemberRef";

    public static final String PREF_NAME = "pref";
    public static final String MEMBER_REF = "memberRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";
    public static final String IMAGE_LOGO = "imageLogo";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String CLUSTER_NAME = "clusterName";
    public static final String PRODUCT_REF = "productRef";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String IS_NUP = "isNUP";
    public static final String NUP_AMT = "nupAmt";
    public static final String IS_BOOKING = "isBooking";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    public static final String TITLE_PRODUCT = "titleProduct";
    public static final String BATHROOM = "bathroom";
    public static final String BEDROOM = "bedroom";
    public static final String CLUSTER_DESCRIPTION = "clusterDescription";
    public static final String URL_VIDEO = "urlVideo";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PRICE_RANGE = "priceRange";
    public static final String POSISION = "posision";


    //tambahan latan untuk project detail
    public static final String PROJECT_WA = "projectWA";
    public static final String PROJECT_EMAIL = "projectEmail";
    public static final String PROJECT_PHONE = "projectPhone";

    //set sharedpref
    public static final String IS_WAITING = "isWaiting";
    public static final String IS_JOIN = "isJoin";

    public static final String COUNT_CATEGORY = "countCategory";
    public static final String COUNT_CLUSTER = "countCluster";
    public static final String SALES_STATUS = "salesStatus";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String BOOKING_CONTACT = "bookingContact";
    public static final String DOWNLOAD_PROJECT_INFO = "downloadProjectInfo";
    public static final String BLOCK_NAME = "blockName";

    public static final String UNIT_MAPPING = "unitMapping";
    public static final String UNIT_REF_LIST = "unitRefList";
    public static final String COLOR = "color";
    public static final String STATUS_COLOR = "statusColor";
    public static final String STATUS_COLOR_NAME = "statusColorName";
    public static final String UNIT_STATUS_LIST = "unitStatusList";
    public static final String PRODUCT_NAME_LIST = "productNameList";
    public static final String PRODUCT_REF_LIST = "productRefList";
    public static final String VIEW_FLOOR_PLAN = "viewFloorPlan";
    public static final String VIEW_FLOOR_PLAN_URL = "viewFloorPlanUrl";

}
