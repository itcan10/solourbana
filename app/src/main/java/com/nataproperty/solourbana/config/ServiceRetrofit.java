package com.nataproperty.solourbana.config;


import android.database.Observable;
import android.util.LruCache;

import com.nataproperty.solourbana.view.before_login.model.VersionModel;
import com.nataproperty.solourbana.view.booking.model.BookingPaymentMethodModel;
import com.nataproperty.solourbana.view.booking.model.DetailBooking;
import com.nataproperty.solourbana.view.booking.model.MemberInfoModel;
import com.nataproperty.solourbana.view.booking.model.PaymentModel;
import com.nataproperty.solourbana.view.booking.model.SaveBookingModel;
import com.nataproperty.solourbana.view.booking.model.VeritransBookingVTModel;
import com.nataproperty.solourbana.view.commision.model.CommisionDetailModel;
import com.nataproperty.solourbana.view.commision.model.CommisionModel;
import com.nataproperty.solourbana.view.commision.model.CommisionScheduleModel;
import com.nataproperty.solourbana.view.commision.model.CommisionSectionModel;
import com.nataproperty.solourbana.view.event.model.EventDetailModel;
import com.nataproperty.solourbana.view.event.model.EventModel;
import com.nataproperty.solourbana.view.event.model.EventSchaduleModel;
import com.nataproperty.solourbana.view.event.model.MyTicketDetailModel;
import com.nataproperty.solourbana.view.event.model.MyTicketModel;
import com.nataproperty.solourbana.view.event.model.ResponseRsvp;
import com.nataproperty.solourbana.view.ilustration.model.BlockMappingModel;
import com.nataproperty.solourbana.view.ilustration.model.DiagramColor;
import com.nataproperty.solourbana.view.ilustration.model.FloorPlanRespone;
import com.nataproperty.solourbana.view.ilustration.model.IlustrationModel;
import com.nataproperty.solourbana.view.ilustration.model.IlustrationStatusModel;
import com.nataproperty.solourbana.view.ilustration.model.ListBlockDiagramModel;
import com.nataproperty.solourbana.view.ilustration.model.ListUnitMappingModel;
import com.nataproperty.solourbana.view.ilustration.model.MapingModelNew;
import com.nataproperty.solourbana.view.ilustration.model.StatusIlustrationAddKprModel;
import com.nataproperty.solourbana.view.kpr.model.CalculationModel;
import com.nataproperty.solourbana.view.kpr.model.PaymentCCStatusModel;
import com.nataproperty.solourbana.view.kpr.model.PaymentKprStatusModel;
import com.nataproperty.solourbana.view.kpr.model.ResponeCalcUserProject;
import com.nataproperty.solourbana.view.kpr.model.TemplateModel;
import com.nataproperty.solourbana.view.logbook.model.ResponeModel;
import com.nataproperty.solourbana.view.menuitem.model.ContactUsModel;
import com.nataproperty.solourbana.view.menuitem.model.NotificationDetailModel;
import com.nataproperty.solourbana.view.menuitem.model.NotificationModel;
import com.nataproperty.solourbana.view.menuitem.model.StatusTokenGCMModel;
import com.nataproperty.solourbana.view.mybooking.model.BookingDetailStatusModel;
import com.nataproperty.solourbana.view.mybooking.model.BookingPaymentInfoModel;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingModel;
import com.nataproperty.solourbana.view.mybooking.model.MyBookingSectionModel;
import com.nataproperty.solourbana.view.mybooking.model.PaymentSchaduleModel;
import com.nataproperty.solourbana.view.mynup.model.MyNupDetailProjectPsModel;
import com.nataproperty.solourbana.view.mynup.model.MyNupProjectPsModel;
import com.nataproperty.solourbana.view.mynup.model.MyNupSectionModel;
import com.nataproperty.solourbana.view.nup.model.AccountBankModel;
import com.nataproperty.solourbana.view.nup.model.BankModel;
import com.nataproperty.solourbana.view.nup.model.MouthModel;
import com.nataproperty.solourbana.view.nup.model.ResponeModelOrderNup;
import com.nataproperty.solourbana.view.nup.model.VeritransNupVTModel;
import com.nataproperty.solourbana.view.nup.model.YearModel;
import com.nataproperty.solourbana.view.profile.model.CityModel;
import com.nataproperty.solourbana.view.profile.model.OthersEditModel;
import com.nataproperty.solourbana.view.profile.model.ProvinceModel;
import com.nataproperty.solourbana.view.profile.model.SubLocationModel;
import com.nataproperty.solourbana.view.project.model.CategoryModel;
import com.nataproperty.solourbana.view.project.model.ChatRoomModel;
import com.nataproperty.solourbana.view.project.model.CommisionStatusModel;
import com.nataproperty.solourbana.view.project.model.DetailStatusModel;
import com.nataproperty.solourbana.view.project.model.LocationModel;
import com.nataproperty.solourbana.view.project.model.ProjectModelNew;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by nata on 11/22/2016.
 */

public class ServiceRetrofit {
    private NetworkAPI networkAPI;
    private OkHttpClient okHttpClient;
    private LruCache<Class<?>, Observable<?>> apiObservables;

    public ServiceRetrofit() {
        this(WebService.URL);
    }

    public ServiceRetrofit(String baseUrl) {
        okHttpClient = buildClient();
        apiObservables = new LruCache<>(10);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }

    /**
     * Method to return the API interface.
     *
     * @return
     */
    public NetworkAPI getAPI() {
        return networkAPI;
    }


    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     *
     * @return
     */
    public OkHttpClient buildClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                // Do anything with response here
                //if we ant to grab a specific cookie or something..
                return response;
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }
        });

        return builder.build();
    }

    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache() {
        apiObservables.evictAll();
    }

    /**
     * all the Service alls to use for the retrofit requests.
     */
    public interface NetworkAPI {
        @FormUrlEncoded
        @POST("getListMonthSvc")
        Call<List<MouthModel>> getMonthList(@Field("urlPostParameter") String urlPostParameter);

        @FormUrlEncoded
        @POST("getListYearSvc")
        Call<List<YearModel>> getYearsList(@Field("urlPostParameter") String urlPostParameter);

        @FormUrlEncoded
        @POST("getIlustrationPaymentIncDiscSvc")
        Call<PaymentCCStatusModel> getPaymentCC(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef,
                                                @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef,
                                                @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getIlustrationPaymentIncDiscSvc_v2")
        Call<PaymentCCStatusModel> getIlustrationPaymentIncDiscSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef,
                                                @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef,
                                                @Field("termNo") String termNo,@Field("discRoomPrice") String discRoomPrice,
                                                                   @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getBookingDetailSvc")
        Call<BookingDetailStatusModel> getBookingDetail(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("bookingRef") String bookingRef);

        @FormUrlEncoded
        @POST("getAppVersionProjectSvc")
        Call<VersionModel> getNewVersion(@Field("projectCode") String projectCode);

        @FormUrlEncoded
        @POST("getDiagramColorSvc")
        Call<DiagramColor> getDiagramColour(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getDiagramColorNewSvc")
        Call<DiagramColor> getDiagramColour(@Field("memberRef") String memberRef,@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getListUnitMapingSvc")
        Call<List<MapingModelNew>> getUnitMapping(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef,
                                                  @Field("clusterRef") String clusterRef, @Field("blockName") String blockName);

        @FormUrlEncoded
        @POST("getProjectDetailSvc")
        Call<DetailStatusModel> getProjectDetail(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getProjectCommisionSvc")
        Call<CommisionStatusModel> getCommision(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetLocationLookupSvc")
        Call<List<LocationModel>> getLocation(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListProjectSvc")
        Call<List<ProjectModelNew>> getListProject(@Field("memberRef") String memberRef, @Field("locationRef") String locationRef);

        @FormUrlEncoded
        @POST("getListMyAgencyChatInsertSvc")
        Call<List<ChatRoomModel>> getMessaging(@Field("memberRefSender") String memberRefSender, @Field("memberRefReceiver") String memberRefReceiver, @Field("chatMessage") String chatMessage);

        @FormUrlEncoded
        @POST("getListPaymentIlustrationSvc")
        Call<List<IlustrationModel>> getIlustrationPayment(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef, @Field("keyword") String keyword);


        @FormUrlEncoded
        @POST("getListPaymentIlustrationSvc_v2")
        Call<List<IlustrationModel>> getIlustrationPayment2(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef,
                                                           @Field("keyword") String keyword,@Field("productRef") String productRef);


        @FormUrlEncoded
        @POST("getIlustrationPaymentSvc")
        Call<StatusIlustrationAddKprModel> getIlustrationAddKpr(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef
                , @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getListCategorySvc")
        Call<List<CategoryModel>> getIlusCategory(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getIlustrationPaymentKPRIncDiscSvc")
        Call<PaymentCCStatusModel> getIlusPaymentKpr(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef
                , @Field("termNo") String termNo, @Field("KPRYear") String KPRYear,@Field("discRoomPrice") String discRoomPrice);

        @FormUrlEncoded
        @POST("getIlustrationPaymentTermSvc")
        Call<PaymentKprStatusModel> getIlusPaymentTerm(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef);

        @FormUrlEncoded
        @POST("getListBlockSvc")
        Call<List<BlockMappingModel>> getListBlok(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef
                , @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListCalcUserProject")
        Call<List<CalculationModel>> getListCal(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListBookingSvc")
        Call<List<MyBookingModel>> getListBook(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetSectionMyBooking")
        Call<List<MyBookingSectionModel>> getListBookingSection(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListBookingProjectSvc")
        Call<List<MyBookingModel>> getListBookingProjectPs(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("projectPsRef") String projectPsRef);

        @FormUrlEncoded
        @POST("getListTemplateProjectSvc")
        Call<List<TemplateModel>> getListTemplate(@Field("project") String project, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getProvinceListSvc")
        Call<List<ProvinceModel>> getListProvinsi(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListSvc")
        Call<List<CityModel>> getListCity(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("getSubLocationListSvc")
        Call<List<SubLocationModel>> getSubLoc(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode);

        @FormUrlEncoded
        @POST("getProvinceListListingSvc")
        Call<List<ProvinceModel>> getListProvinsiListListing(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListListingSvc")
        Call<List<CityModel>> getListCityListListing(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("getSubLocationListListingSvc")
        Call<List<SubLocationModel>> getSubLocListListing(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode);


        @FormUrlEncoded
        @POST("GetLocationLookupListingSvc")
        Call<List<LocationModel>> getLocationListing(@Field("agencyCompanyRef") String agencyCompanyRef);

        @GET("GetLocationLookupIsCobrokeSvc")
        Call<List<LocationModel>> getLocationListingCobroke();

        //commision
        @FormUrlEncoded
        @POST("GetSectionCommission")
        Call<List<CommisionSectionModel>> getListCommisionSection(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetListCommissionProjectSvc")
        Call<List<CommisionModel>> GetListCommissionProjectSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef")
                String projectRef, @Field("projectPsRef") String projectPsRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetCommissionProjectDetailSvc")
        Call<List<CommisionDetailModel>> GetListCommissionDetailProjectSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef")
                String projectRef, @Field("bookingRef") String bookingRef, @Field("projectPsRef") String projectPsRef);

        @FormUrlEncoded
        @POST("GetListCommissionCustProjectSvc")
        Call<List<CommisionScheduleModel>> GetListCommissionCustProjectSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef")
                String projectRef, @Field("bookingRef") String bookingRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetSectionMyNUPProject")
        Call<List<MyNupSectionModel>> getListNupSection(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetListNUPProjectSVC")
        Call<List<MyNupProjectPsModel>> getListNupProjectPs(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                            @Field("projectPsRef") String projectPsRef, @Field("memberRef") String memberRef,
                                                            @Field("schemeRef") String projectSchemeRef);

        @FormUrlEncoded
        @POST("GetNUPInfoProject")
        Call<MyNupDetailProjectPsModel> GetNUPInfoProject(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                          @Field("projectPsRef") String NUPRef, @Field("NUPRef") String projectPsRef,
                                                          @Field("memberRef") String memberRef, @Field("schemeRef") String projectSchemeRef);

        @FormUrlEncoded
        @POST("getListEventSvc")
        Call<ArrayList<EventModel>> getListEvent(@Field("contentDate") String contentDate, @Field("memberRef") String memberRef);
//        @GET("/maps/api/place/autocomplete/json?key=AIzaSyDTkSXoyn7JcS7BTSTY_tSUnV4YCzBFdZw&components=country:id&language=id&types=geocode")
//        public void findPlaces(@Query("input") String input, Callback<GooglePlaceResult> callback);

        @FormUrlEncoded
        @POST("getNewsImageSliderSvc")
        Call<ArrayList<EventDetailModel>> getNewsImageSliderSvc(@Field("contentRef") String contentRef);

        @FormUrlEncoded
        @POST("getListEventScheduleSvc")
        Call<ArrayList<EventSchaduleModel>> getListEventScheduleSvc(@Field("contentRef") String contentRef);

        @FormUrlEncoded
        @POST("getListTiketRsvp")
        Call<ArrayList<MyTicketModel>> getListTiketRsvp(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListTiketQRCode")
        Call<ArrayList<MyTicketDetailModel>> getListTiketQRCode(@Field("memberRef") String memberRef,
                                                                @Field("eventScheduleRef") String eventScheduleRef);

        @FormUrlEncoded
        @POST("getListNotificationSSTSvc")
        Call<ArrayList<NotificationModel>> getListNotificationSSTSvc(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getGCMMessageSvc")
        Call<NotificationDetailModel> getGCMMessageSvc(@Field("GCMMessageRef") String GCMMessageRef,
                                                       @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetPaymentMethodSvc")
        Call<List<BookingPaymentMethodModel>> GetPaymentMethodSvc(@Field("dbMasterRef") String dbMasterRef,
                                                                  @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("veritransBookingVTSvc")
        Call<VeritransBookingVTModel> veritransBookingVTSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                            @Field("memberRef") String memberRef, @Field("bookingRef") String bookingRef,
                                                            @Field("termRef") String termRef, @Field("paymentType") String paymentType);

        @FormUrlEncoded
        @POST("getIlustrationPaymentSvc")
        Call<PaymentModel> getIlustrationPaymentSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                    @Field("clusterRef") String clusterRef, @Field("productRef") String productRef,
                                                    @Field("unitRef") String unitRef, @Field("termRef") String termRef,
                                                    @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getBookingDetailSvc")
        Call<DetailBooking> getBookingDetailSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                @Field("bookingRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListPaymentScheduleSvc")
        Call<ArrayList<PaymentSchaduleModel>> getListPaymentScheduleSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                                        @Field("bookingRef") String clusterRef);

        @FormUrlEncoded
        @POST("getBookingPaymentInfoSvc")
        Call<BookingPaymentInfoModel> getBookingPaymentInfoSvc(@Field("bookingRef") String bookingRef);

        @FormUrlEncoded
        @POST("getMemberInfoSvc")
        Call<MemberInfoModel> getMemberInfoSvc(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getProjectAccountBank")
        Call<ArrayList<AccountBankModel>> getProjectAccountBank(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @GET("getListBankSvc")
        Call<ArrayList<BankModel>> getListBankSvc();

        @FormUrlEncoded
        @POST("savePaymentBookingBankTransferSvc")
        Call<SaveBookingModel> savePaymentBookingBankTransferSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("savePaymentBookingVASvc")
        Call<SaveBookingModel> savePaymentBookingVASvc(@Field("JSON_IN") String jsonIn);

        @FormUrlEncoded
        @POST("updatePaymentTypeOrderSvc")
        Call<VeritransNupVTModel> updatePaymentTypeOrderSvc(@Field("nupOrderRef") String nupOrderRef,
                                                            @Field("paymentType") String paymentType,
                                                            @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("updatePaymentTypeOrderVTSvc")
        Call<VeritransNupVTModel> updatePaymentTypeOrderVTSvc(@Field("nupOrderRef") String nupOrderRef,
                                                              @Field("paymentType") String paymentType,
                                                              @Field("dbMasterRef") String dbMasterRef,
                                                              @Field("projectRef") String projectRef,
                                                              @Field("memberRef") String memberRef,
                                                              @Field("nupAmt") String nupAmt,
                                                              @Field("total") String total);

        @FormUrlEncoded
        @POST("getListUnitSvc")
        Call<List<ListUnitMappingModel>> getListUnitDiagram(@Field("dbMasterRef") String dbMasterRef,
                                                            @Field("projectRef") String projectRef,
                                                            @Field("categoryRef") String categoryRef,
                                                            @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListBlockSvc")
        Call<List<ListBlockDiagramModel>> getListBlockDiagram(@Field("dbMasterRef") String dbMasterRef,
                                                              @Field("projectRef") String projectRef,
                                                              @Field("categoryRef") String categoryRef,
                                                              @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getClusterInfoSvc")
        Call<IlustrationStatusModel> getClusterInfo(@Field("dbMasterRef") String dbMasterRef,
                                                    @Field("projectRef") String projectRef,
                                                    @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getClusterInfoSvc_v2")
        Call<IlustrationStatusModel> getClusterInfo_V2(@Field("JSON_IN") String jsonIn);


        @FormUrlEncoded
        @POST("saveGCMTokenSvc")
        Call<StatusTokenGCMModel> postGCMToken(@Field("GCMToken") String GCMToken,
                                               @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("savePaymentBankTransferSvc")
        Call<SaveBookingModel> savePaymentBankTransferSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("saveFeedbackSvc")
        Call<ContactUsModel> saveFeedbackUser(@Field("contactName") String contactName,
                                              @Field("contactEmail") String contactEmail,
                                              @Field("contactSubject") String contactSubject,
                                              @Field("contactMessage") String contactMessage);

        @FormUrlEncoded
        @POST("updateAccountBankSvc")
        Call<ResponeModel> updateAccountBankSvc(@Field("memberRef") String memberRef,
                                                @Field("bankRef") String bankRef,
                                                @Field("bankBranch") String bankBranch,
                                                @Field("accName") String accName,
                                                @Field("accNo") String accNo);

        @GET("getOtherProfileSvc")
        Call<OthersEditModel> getOtherProfileSvc();

        @FormUrlEncoded
        @POST("updateOtherProfileSvc")
        Call<ResponeModel> updateOtherProfileSvc(@FieldMap Map<String, String> fields);


        @FormUrlEncoded
        @POST("saveNewCalcUserProject")
        Call<ResponeCalcUserProject> saveNewCalcUserProject(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("saveRsvpSvc")
        Call<ResponseRsvp> saveRsvpSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("getListBlockProductColorSvc")
        Call<List<ListBlockDiagramModel>> getListBlockProductColorSvc(@Field("dbMasterRef") String dbMasterRef,
                                                                      @Field("projectRef") String projectRef,
                                                                      @Field("categoryRef") String categoryRef,
                                                                      @Field("clusterRef") String clusterRef,
                                                                      @Field("productRefParam") String productRefParam,
                                                                      @Field("memberRef") String memberRef);


        @FormUrlEncoded
        @POST("GenerateKalkulatorKPR")
        Call<PaymentCCStatusModel> generateKalkulatorKPR(@Field("JSON_IN") String jsonIn);

        @FormUrlEncoded
        @POST("getUnitStatusSVC")
        Call<FloorPlanRespone> getUnitStatusSVC(@Field("JSON_IN") String jsonIn);

        @FormUrlEncoded
        @POST("saveOrderNUPSvc_v2")
        Call<ResponeModelOrderNup> saveOrderNup_v2(@Field("JSON_IN") String jsonIn,
                                                   @Field("JSON_IN_CUST") String jsonInCust);


    }

}
