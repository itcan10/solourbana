package com.nataproperty.solourbana.config;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.Tracker;
import com.nataproperty.solourbana.BuildConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;


/**
 * Created by Administrator on 3/31/2016.
 */
public class BaseApplication extends Application {
    public static final String TAG = BaseApplication.class.getSimpleName();
    private Tracker mTracker;
    private static final int TIMEOUT_MS = 40000;
    private ServiceRetrofit networkService;
    private ServiceRetrofitProject networkServiceProject;
    private RequestQueue requestQueue;
    private static BaseApplication instance;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            //Fabric.with(this, new Crashlytics());
        } else {
            Fabric.with(this, new Crashlytics());
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.nataproperty.solourbana",  // replace with your unique package name
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        networkService = new ServiceRetrofit();
        networkServiceProject = new ServiceRetrofitProject();
        instance = this;
    }

    public ServiceRetrofit getNetworkService() {
        return networkService;
    }

    public ServiceRetrofitProject getNetworkServiceProject() {
        return networkServiceProject;
    }

    public static synchronized BaseApplication getInstance() {
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag) {
        //Utils.TRACE("BaseApplication", "addToRequestQueue : " + request.getUrl());
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        // set retry policy
        request.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        getRequestQueue().add(request);
    }

    public void cancelPendingRequest(Object tag) {
        if (requestQueue != null)
            requestQueue.cancelAll(tag);
    }

    public static void showLog(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }

//    public void startLoader(Context context){
//        dialog = new ProgressDialog(context);
//        dialog.setMessage("Loading..");
//        dialog.setCancelable(false);
//        dialog.setInverseBackgroundForced(false);
//        dialog.show();
//    }

//    public DaoSession getDaoSession() {
//        return daoSession;
//    }

//    public void stopLoader(){
//        if(dialog !=null && dialog.isShowing()) {
//            dialog.dismiss();
//        }
//    }

//    synchronized public Tracker getDefaultTracker() {
//        if (mTracker == null) {
//            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
//            mTracker = analytics.newTracker(R.xml.global_tracker);
//        }
//        return mTracker;
//    }
}
